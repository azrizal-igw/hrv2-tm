<?php 

namespace IhrV2\Contracts\Letter;

interface LetterInterface {

	public function getLetterTypeList();
	public function getLetterInfo($id);
	public function getProcessNewLetter($data, $uid, $sitecode);
	public function getUserInfo($uid, $sitecode);
	public function getLetterAll($uid, $sitecode);
}