<?php 

namespace IhrV2\Contracts\Letter;

interface DbLetterInterface {


	public function dbInsertLetter($data, $uid, $sitecode);
	public function dbInsertLetterAttach($letter_id, $uid, $info);


}