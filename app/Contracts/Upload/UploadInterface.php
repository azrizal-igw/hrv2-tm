<?php 

namespace IhrV2\Contracts\Upload;

interface UploadInterface {

	public function uploadFileLeaveRep($file, $rep_id, $user_id);
	public function uploadFileLeave($file, $id, $uid);

}

