<?php 

namespace IhrV2\Contracts\Mcmc;

interface DbMcmcInterface {

    public function dbInsertAggStaffDetail($sitecode, $icno, $name, $no, $email, $position, $status);
    public function dbUpdateSendDateAggStaffDetail($id);
    public function dbUpdateErrorAggStaffDetail($i, $err);
    public function dbInsertAggSiteDetail($i, $state, $phase);
    public function dbUpdateAggSiteDetail($i, $state, $phase, $sitecode);
    public function dbUpdateSendDateAttendance($i);
    public function dbUpdateErrorAttendance($att, $err);
    public function dbUpdateErrorEmptyAttendance($att);
    public function dbInsertAggStaffAttendance($i, $in, $out, $remark);
    public function dbUpdateAggStaffAttendance($i, $in, $out, $remark);
    public function dbInsertProcessTable($i, $date, $type, $remark);
    public function dbUpdateProcessTable($i, $remark, $type);
    public function dbUpdateProcessTableRemark($i, $remark);
    public function dbUpdateProcessTableInComplete($icno, $sitecode, $date, $remark);

}

