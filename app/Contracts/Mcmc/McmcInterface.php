<?php

namespace IhrV2\Contracts\Mcmc;

interface McmcInterface {

    public function getTokenAndPath();
    public function getCallBack($name);
    public function getSiteWithStatePhase();
    public function getAggSiteBySitecode($sitecode);
    public function getAggStaffDetailInactive($sitecode, $icno, $position);
    public function getAggStaffDetail($date);
    public function getAggStaffAttendance($date);
    public function getAttendanceRemark($i);
    public function getAggStaffAttendanceByICDate($i);
    public function getAttendanceProcessTable($date);
    public function getAttendanceProcessTableBySitecode($sitecode);
    public function getAttProcessBySdRange($from, $to, $sitecode);
    public function getAttProcessByDateRange($from, $to);
    public function getPublicHolidayList($date, $year);
    public function getOffDateList($state_id, $types, $date, $year);
    public function getPartTimerList();
    public function getLeaveDateList($date);
    public function getRemarkManualList($date);
    public function getUserStaffActive();
    public function getUserAttBySitecode($sitecode);
    public function getAttendanceTime($i, $date);
    public function getAttendanceTimeIn($i, $date);
    public function getAttendanceTimeInSitecode($i);
    public function getAttendanceTimeOut($i, $date);
    public function getAttendanceTimeOutSitecode($i);
    public function getStateID($sitecode);
    public function getPublicHoliday($date, $state_id);
    public function getUserJob($icno);
    public function getUserJobActive($icno, $sitecode);
    public function getCheckProcessTable($i, $date);
    public function getCheckAttProcessTable($icno, $sitecode, $date);
    public function getCheckProcessTableEmpty($icno, $sitecode, $date);
    public function getCheckRemarkBoth($arr);
    public function getCheckRemark($x, $y, $r = null);
    public function getCheckTime($time);
    public function getOffDay($state_id, $types, $date);
    public function getAttendanceDateToProcess();
    public function getAttendanceDateToExport();
    public function getAttendanceDateToTransfer();
    public function getCheckAggDate($date);
    public function getRemarkAll($date);
    public function getAttInOut($icno, $sitecode, $date, $remark_1st);
    public function getProcessAtt($user, $date);
    public function getProAttDate($date);
    public function getProAttSiteDate($sitecode, $start_date, $end_date);
    public function getProAttStartEnd($start_date, $end_date);
    public function getExportAtt($i);
    public function getExpAttDate($date);
    public function getExpAttSiteDate($sitecode, $start_date, $end_date);
    public function getExpAttStartEnd($start_date, $end_date);
    public function getExpSite();
    public function getChkAttFail($start, $end);
    public function getAggAttFail($start, $end);
}
