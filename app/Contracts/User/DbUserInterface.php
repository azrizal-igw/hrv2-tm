<?php 

namespace IhrV2\Contracts\User;

interface DbUserInterface {

	public function dbUpdateAuthContact($uid, $data);
	public function dbUpdateStatusContract($user_id);
	public function dbInsertUserContract($i);
	public function dbUpdateUserContract($i);
	public function dbUpdateLeaveBalance($uid, $id, $total_al);
	public function dbInsertLeaveBalance($i);
    public function dbInsertUser($data);
	public function dbInsertUserEmployment($data, $uid);
	public function dbUpdateUserEmployment($data, $id);
	public function dbDeleteUserEmployment();
	public function dbInsertUserSkill($data, $uid);
	public function dbUpdateUserSkill($data, $id);
	public function dbDeleteUserSkill();
	public function dbInsertUserReference($data, $uid);
	public function dbUpdateUserReference($data, $id);
	public function dbDeleteUserReference();
	public function dbUploadUserPhoto($data);
	public function dbRemoveUserPhoto();
	public function dbInsertUserLanguage($data, $uid);
	public function dbUpdateUserLanguage($data, $id);
	public function dbDeleteUserLanguage();
    public function dbInsertUserJob($data, $uid);
    public function dbUpdateUserJob($data, $id);
    public function dbDeleteUserJob();
	public function dbInsertUserFamily($data);
	public function dbUpdateUserFamily($data, $id);
	public function dbDeleteUserFamily();
	public function dbInsertUserEmergency($data, $uid);
	public function dbUpdateUserEmergency($data, $id);
	public function dbDeleteUserEmergency();
	public function dbInsertUserEducation($data, $uid);
	public function dbUpdateUserEducation($data, $id);
	public function dbDeleteUserEducation();

}

