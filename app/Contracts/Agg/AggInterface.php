<?php namespace IhrV2\Contracts\Agg;

interface AggInterface {


	public function getAttProExpExe($i);
	public function getListProcessType();
	public function getListTypes();
	public function getMyCommAtt($arr);
	public function getProcessName($id);
	public function getStatusListAll();
	public function getTypeName($id);
	public function getMyCommAttDetail($id);
	public function getSiteSync($status);
	public function getStaffDetailList($arr, $codes);
	public function getStaffStatusList();
	public function getStaffPositionList();
	public function getAggStaffDetail($id);

}
