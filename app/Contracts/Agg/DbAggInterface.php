<?php namespace IhrV2\Contracts\Agg;

interface DbAggInterface {

    public function dbInsertSiteHistory($i);
    public function dbUpdateSite($i, $state, $phase);
    public function dbInsertSite($i, $state, $phase);

}
