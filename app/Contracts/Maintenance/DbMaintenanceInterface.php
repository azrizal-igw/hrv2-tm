<?php 

namespace IhrV2\Contracts\Maintenance;

interface DbMaintenanceInterface {

	public function dbUpdateRegion($i, $id);
	public function dbInsertSite($i);
	public function dbUpdateSite($i, $id);
	public function dbDeleteSite();
	public function dbInsertPublicHoliday($i);
	public function dbUpdatePublicHoliday($i, $id);

}

