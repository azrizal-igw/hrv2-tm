<?php 

namespace IhrV2\Contracts\Maintenance;

interface MaintenanceInterface {

	public function getAddOffDay($i);
	public function validateDate($date);
	public function DaysInMonth($m, $y);
	public function getLeaveOffGroups();
	public function getLeaveOffTypes();
	public function getLeaveOffYears();

}

