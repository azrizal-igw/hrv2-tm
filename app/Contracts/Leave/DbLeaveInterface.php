<?php 

namespace IhrV2\Contracts\Leave;

interface DbLeaveInterface {

	public function dbUpdateLeaveRepHistory($id, $uid);
	public function dbInsertLeaveRepAttachment($rep_id, $user_id, $i);
	public function dbInsertLeaveRepApplication($i, $report_to);
	public function dbInsertLeaveRepHistory($rep_id);
	public function dbProcessLeaveRepHistory($rep_id, $uid, $data);
	public function dbInsertLeaveRepApprove($rep_id, $uid, $no_day, $rid);
	public function dbUpdateLeaveRepApprove($rep_id, $uid);
	public function dbUpdateLeaveApprove($id, $uid);
	public function dbUpdateLeaveAttachment($id, $uid);
	public function dbUpdateLeaveRepAttachment($id, $uid);
	public function dbInsertLeaveApplication($i);
	public function dbInsertLeaveDate($user_id, $leave_id, $date, $type_id);
	public function dbInsertLeaveHistory($user_id, $leave_id);
	public function dbInsertLeaveHistoryArray($i);
	public function dbInsertLeaveApprove($i);
	public function dbUpdateLeaveHistory($user_id, $leave_id);
	public function dbInsertLeaveAttachment($leave_id, $user_id, $i);
	public function dbUpdateLeaveBalance($user_id, $leave_type_id, $contract_id, $bal);
	public function dbInsertLeaveBalance($user_id, $leave_type_id, $entitled, $contract_id);
	public function dbInsertLeaveAdd($i, $user_id, $sitecode);

}

