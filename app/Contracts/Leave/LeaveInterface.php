<?php

namespace IhrV2\Contracts\Leave;

interface LeaveInterface {

    public function getLeaveInfo($id, $uid);
    public function getReimburseDeductList();
    public function getApplyLeaveRep($data);
    public function getLeaveRepAttachment($id, $uid);
    public function getLeaveAttachment($id, $uid);
    public function getUpdateLeaveRepAttachment($data, $id, $uid);
    public function getUpdateLeaveAttachment($data, $id);
    public function getUploadLeaveAttachment($data, $id, $uid);
    public function getCheckApplyLeave($leave_type_id, $user_id, $sitecode);
    public function getApplyLeave($data);
    public function getOffDayTypeID($date, $year, $group_id);
    public function getOffDayGroup($state_id);
    public function getOffDateList($state_id, $types, $year);
    public function getPublicDateList($state_id, $year);
    public function getApproveLeave($id, $uid, $sitecode, $data);
    public function getProcessLeave($data, $id, $uid, $sitecode, $action_by);
    public function getLeaveDetail($leave_id);
    public function getLeaveRepDetail($id);
    public function getTotalLeavePending();
    public function getLeaveStatusTotal($i);
    public function getLeaveRepStatusTotal($uid, $sitecode, $date_from, $date_to);
    public function getLeaveRepEntitled($uid, $sitecode, $contract_id, $date_from, $date_to);
    public function getTotalLeaveDate($id, $uid);
    public function getCalculateBalance($total1, $total2);
    public function getCheckIfTally($total1, $total2);
    public function getLeaveYear();
    public function getLeaveMonth();
    public function getLeaveType();
    public function getLeaveTypeAll();
    public function getLeaveTypeWithPrefix();
    public function getLeaveTypeName($id);
    public function getTotalAL($from_date, $to_date, $total);
    public function CheckIfExpired($date);
    public function DateRange($first, $last, $step = '+1 day', $output_format = 'Y-m-d');
    public function LeaveCompareDate($from, $to, $half);
    public function LeaveHalfDay($from, $to, $half);
    public function getUserStateID($sitecode);
    public function getLeaveTaken($uid, $sitecode, $leave_type_id, $date_from, $date_to);
    public function getLeaveBalance($user_id, $leave_type_id, $contract_id);
    public function getLeaveStatusList();
    public function getLeaveTypeByID($leave_type_id);
    public function getProcessLeaveRep($data, $id, $uid, $sitecode, $action_by);
    public function getLeavePendingAll($codes);
    public function getLeaveRepPendingAll($codes);

}
