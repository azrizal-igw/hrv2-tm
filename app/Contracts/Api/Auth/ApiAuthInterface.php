<?php namespace IhrV2\Contracts\Api\Auth;

interface ApiAuthInterface {


	public function getApiLogin($i);
	public function getApiToken();
	public function getApiUser($i);
	public function getApiLogout();


}

