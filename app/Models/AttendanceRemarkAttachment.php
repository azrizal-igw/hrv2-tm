<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceRemarkAttachment extends Model
{

	protected $table = 'attendance_remark_attachments';


    protected $fillable = [
    	'att_remark_id',
    	'user_id',
    	'filename',
    	'ext',
    	'size',
    	'thumb_name',
        'att_other_id',
    	'status'
    ];

}
