<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    //

	protected $table = 'phases';


    protected $fillable = [
    	'id',
    	'name',
    	'name_tm',
    	'name_both',
    	'status'
    ];



    
}
