<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveAddAttachment extends Model
{
	protected $table = 'leave_add_attachments';

    protected $fillable = [
    	'user_id',
    	'leave_add_id',
    	'filename',
    	'ext',
    	'size',
    	'thumb_name',
    	'status'
    ];
}
