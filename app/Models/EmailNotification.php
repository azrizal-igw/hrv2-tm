<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class EmailNotification extends Model
{
	protected $table = 'email_notifications';

    // get email type
    // --------------
    public function TypeName() {
        return $this->belongsTo('IhrV2\Models\EmailType', 'type_id');
    }

    // get action by
    // -------------
    public function ActionByUser() {
        return $this->belongsTo('IhrV2\User', 'created_by');
    }

    protected $fillable = [
        'name',
        'email',
        'type_id',
        'status',
        'created_by'
    ];


}
