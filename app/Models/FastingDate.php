<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class FastingDate extends Model
{
	protected $table = 'fasting_dates';


    protected $fillable = [
    	'year',
    	'start_date',
    	'end_date',
    	'start_time',
    	'end_time',
    	'action_date',
    	'action_by',
    	'updated_by',
    	'active'
    ];

    // lists of states
    // ---------------
    public function ListStates()
    {
        return $this->hasMany('IhrV2\Models\FastingDateState', 'fasting_date_id');
    }

    // get action by
    // -------------
    public function ActionByUser() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    public function getAvailabilityAttribute()
    {
        if ($this->active == 1) {
            return '<span class="text-primary">Active</span>';
        }
        else if ($this->active == 2) {
            return '<span class="text-danger">Cancel</span>';
        }
        else {
            return '<span class="text-default">Unknown</span>';            
        }
    }



}
