<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveApprove extends Model
{
    //
	protected $table = 'leave_approves';


    protected $fillable = [
    	'user_id',
    	'leave_id',
        'leave_type_id',
        'contract_id',
        'date_action',
        'action_by',
    	'date_value',
    	'flag',
        'leave_app_id'
    ];



	public function LeaveInfo() {
		return $this->belongsTo('IhrV2\Models\LeaveApplication', 'leave_id');
	}	
	
    public function LeaveUserInfo() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    public function LeaveTypeInfo() {
        return $this->belongsTo('IhrV2\Models\LeaveType', 'leave_type_id');
    }

    public function LeaveHistoryApprove() {
        return $this->hasOne('IhrV2\Models\LeaveHistory', 'leave_id', 'leave_id')->where('flag', 1)->where('status', 2);
    }   

    public function LeaveDate() {
        return $this->hasMany('IhrV2\Models\LeaveDate', 'leave_id', 'leave_id');
    }   

    
}
