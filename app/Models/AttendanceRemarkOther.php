<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceRemarkOther extends Model
{
	protected $table = 'attendance_remark_others';


    protected $fillable = [
    	'user_id',
    	'icno',
    	'sitecode',
    	'date',
    	'notes',
    	'action_date',
    	'action_by',
    	'active',
        'updated_by'
    ];


    // get attachment
    // --------------
    public function AttAttachment() {
        return $this->hasOne('IhrV2\Models\AttendanceRemarkAttachment', 'att_other_id')->where('status', 1);
    }

    // get user info
    // -------------
    public function AttUserInfo() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    // get updated by info
    // -------------------
    public function AttUpdatedByInfo() {
        return $this->belongsTo('IhrV2\User', 'updated_by');
    }

    // get user job
    // ------------
    public function AttUserJob() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id', 'user_id')->where('flag', 1);        
    }

    // get site info
    // --------------
    public function AttSiteInfo() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    public function getStatusAttribute() {
        if ($this->active == 1) {
            return '<span class="text-primary">ACTIVE</span>';
        } else if ($this->active == 2) {
            return '<span class="text-danger">CANCEL</span>';
        } else {
            return '<span class="text-default">UNKNOWN</span>';
        }
    }    

}
