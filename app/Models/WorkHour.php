<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;
use IhrV2\Contracts\Maintenance\MaintenanceInterface;

class WorkHour extends Model
{
    protected $table = 'work_hours';

    protected $fillable = [
    	'user_id',
    	'sitecode',
    	'contract_id',
        'date_from',
        'date_to',
    	'time_in',
    	'time_out',
        'days',
    	'reason',
        'partner',
    	'created_by',
    	'updated_by',
    	'status'
    ];

    // get staff info
    // --------------
    public function StaffInfo() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    // get site info
    // -------------
    public function SiteInfo() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    // get created by detail
    // ---------------------
    public function UserActionBy() {
        return $this->belongsTo('IhrV2\User', 'created_by');
    }

    // get contract info
    // -----------------
    public function ContractInfo() {
        return $this->belongsTo('IhrV2\Models\UserContract', 'contract_id');
    }

    // get off dates
    // -------------
    public function OffDates()
    {
        return $this->hasMany('IhrV2\Models\WorkHourOffDate', 'work_hour_id')->where('status', 1)->orderBy('off_date', 'asc');
    }

    // work hour attachment
    // --------------------
    public function TheAttachment() {
        return $this->hasOne('IhrV2\Models\WorkHourAttachment', 'work_hour_id')->where('status', 1);
    }

    // get status name
    // ---------------
    public function getAvailAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">ACTIVE</span>';
        } else {
            return '<span class="text-danger">INACTIVE</span>';
        }
    }

    // get partner status
    // ------------------
    public function getPartnerStatusAttribute() {
        if ($this->partner != 0) {
            return '<span class="text-primary">YES</span>';
        } else {
            return '<span class="text-danger">NO</span>';
        }
    }

    // get name of day to be off
    // -------------------------
    public function getDayNameAttribute() {
        $exp = explode(':', $this->days);
        $days = \IhrV2\Models\Day::whereIn('id', $exp)->get();
        return $days;
    }



}
