<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model {

    protected $table = 'sites';

    // public $timestamps = false;

    protected $fillable = [
        'code',
        'name',
        'full_name',
        'tm_name',
        'both_name',
        'old_name',
        'phase_id',
        'region_id',
        'street1',
        'street2',
        'postal_code',
        'city',
        'district',
        'state_id',
        'address_label',
        'email',
        'website',
        'latitude',
        'longitude',
        'start',
        'end',
        'bandwidth',
        'backhaul',
        'location_id',
        'status',
        'date_modified',
        'created_at',
        'updated_at'
    ];

    public function UserJobActive() {
        return $this->hasMany('IhrV2\Models\UserJob', 'sitecode', 'code')->where('status', 1);
    }

    // user job active/inactive & position m/am
    // ----------------------------------------
    public function UserJobSite() {
        return $this->hasMany('IhrV2\Models\UserJob', 'sitecode', 'code')->where('flag', 1)->whereIn('position_id', array(4,5));
    }

    // public function UserJobActive2() {
    //     return $this->hasMany('IhrV2\Models\UserJob', 'sitecode', 'code')->where('flag', 1);
    // }

    public function PhaseName() {
        return $this->belongsTo('IhrV2\Models\Phase', 'phase_id');
    }

    public function StateName() {
        return $this->belongsTo('IhrV2\Models\State', 'state_id', 'code');
    }

    public function RegionName() {
        return $this->belongsTo('IhrV2\Models\Region', 'region_id');
    }

    public function RegionManager() {
        return $this->belongsTo('IhrV2\Models\RegionUser', 'region_id', 'region_id');
    }

    public function StatusName() {
        return $this->belongsTo('IhrV2\Models\Status', 'status');
    }

    // get region manager id
    // ---------------------
    public function RegionManagerInfo() {
        return $this->hasOne('IhrV2\Models\RegionUser', 'region_id', 'region_id');
    }

    public function getAvailabilityAttribute() {

        if (!empty($this->status)) {

            // status active
            // -------------
            if ($this->status == 1) {
                return '<span class="text-primary">' . strtoupper($this->StatusName->name) . '</span>';
            }

            // status inactive
            // ---------------
            else if ($this->status == 2) {
                return '<span class="text-danger">' . strtoupper($this->StatusName->name) . '</span>';
            }

            // status unknown
            // --------------
            else {
                return '<span class="text-default">Unknown</span>';
            }
        }

        // status null
        // -----------
        else {
            return '-';
        }

    }

    public function scopeOfSort($query, $sort) {
        foreach ($sort as $column => $direction) {
            $query->orderBy($column, $direction);
        }
        return $query;
    }

}
