<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LetterAttachment extends Model
{
	protected $table = 'letter_attachments';


    protected $fillable = [
    	'letter_id',
    	'user_id',
    	'filename',
    	'ext',
    	'size',
    	'thumb_name',
    	'status'
    ];


}
