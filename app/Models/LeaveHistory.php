<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveHistory extends Model
{
    //

	protected $table = 'leave_histories';


    protected $fillable = [
    	'user_id',
    	'leave_id',
    	'action_date',
    	'action_by',
    	'action_remark',
    	'status',
    	'flag'
    ];

	public function LeaveStatusName() {
		return $this->belongsTo('IhrV2\Models\LeaveStatus', 'status');
	}

	public function LeaveUserApplyName() {
		return $this->belongsTo('IhrV2\User', 'user_id');
	}

	public function LeaveActionByName() {
		return $this->belongsTo('IhrV2\User', 'action_by');
	}


	public function LeaveApplicationName() {
		return $this->belongsTo('IhrV2\Models\LeaveApplication', 'leave_id');
	}

    public function getAvailabilityAttribute()
    {
        if (in_array($this->status, array(2,6))) {
            return 'primary';
        }
        else if (in_array($this->status, array(1))) {
            return 'danger';
        }
        else if (in_array($this->status, array(4))) {
            return 'warning';
        }        
        else if (in_array($this->status, array(3,5,7,8))) {
            return 'muted';
        }        
        else {
            return 'info';            
        }
    }




	    
}
