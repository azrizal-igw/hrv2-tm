<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveRepApprove extends Model
{
	protected $table = 'leave_rep_approves';



    protected $fillable = [
    	'user_id',
    	'leave_rep_id',
    	'date_action',
        'action_by',
    	'total_day',
    	'flag'
    ];


	public function LeaveRepInfo() {
		return $this->belongsTo('IhrV2\Models\LeaveRepApplication', 'leave_rep_id');
	}	


}
