<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOffDateState extends Model
{
	protected $table = 'leave_off_date_states';


    protected $fillable = [
    	'date_id',
    	'state_id',
    ];

    public function LeaveOffDate() {
    	return $this->belongsTo('IhrV2\Models\LeaveOffDate', 'date_id');
    }


    public function LeaveOffStateName() {
    	return $this->belongsTo('IhrV2\Models\State', 'state_id', 'code');
    }




}
