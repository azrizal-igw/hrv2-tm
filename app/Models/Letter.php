<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model {
    protected $table = 'letters';

    protected $fillable = [
        'letter_type_id',
        'user_id',
        'letter_date',
        'reminder_no',
        'action_date',
        'action_by',
        'notes',
        'report_to',
        'updated_by',
        'sitecode',
        'status'
    ];

    public function LetterTypeName() {
        return $this->belongsTo('IhrV2\Models\LetterType', 'letter_type_id');
    }

    public function LetterUserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    public function LetterUserJob() {
        return $this->belongsTo('IhrV2\Models\UserJob', 'user_id', 'user_id')->where('flag', 1);
    }

    public function LetterActionBy() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    public function LetterUpdatedBy() {
        return $this->belongsTo('IhrV2\User', 'updated_by');
    }

    public function LetterReportTo() {
        return $this->belongsTo('IhrV2\User', 'report_to');
    }

    public function LetterSiteName() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    public function LetterAttachment() {
        return $this->hasOne('IhrV2\Models\LetterAttachment', 'letter_id')->where('status', 1);
    }

    public function getAvailabilityAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">ACTIVE</span>';
        } else if ($this->status == 2) {
            return '<span class="text-danger">CANCEL</span>';
        } else {
            return '<span class="text-default">UNKNOWN</span>';
        }
    }

}
