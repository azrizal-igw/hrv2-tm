<?php

namespace IhrV2\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //

	protected $table = 'attendance_log_records';

    public $timestamps = false;

    protected $fillable = [
        'att_log_id',
        'att_mykad_ic',
        'att_password',
        'location_id',
        'att_record_type',
        'att_input_type',
        'att_log_time',
        'att_userstamp',
        'ip_add',
        'att_timestamp',
        'att_sys_status' 
    ];

    // get only time from datetime attendance
    // --------------------------------------
    protected $appends = array('att_time_only');
    public function getAttTimeOnlyAttribute(){
        return Carbon::parse($this->attributes['att_log_time'])->format('g:i:s A');
    }

	public function UserAttendanceDetail() {
		return $this->belongsTo('IhrV2\User', 'att_mykad_ic', 'icno')->where('status', 1);
	}	

    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'att_mykad_ic', 'icno', 'sitecode');
    }   

	public function UserAttendanceSite() {
		return $this->belongsTo('IhrV2\Models\Site', 'location_id', 'code');
	}	

    // get only latest attendance remark
    public function UserAttendanceRemark() {
        return $this->hasOne('IhrV2\Models\AttendanceRemark', 'att_log_id')->where('active', 1);
    }

    // get attendance gps
    public function UserAttendanceGps() {
        return $this->hasOne('IhrV2\Models\AttendanceGps', 'att_log_id', 'att_log_id');
    }

    // get attendance gps (active)
    public function UserAttGpsActive() {
        return $this->hasOne('IhrV2\Models\AttendanceGps', 'att_log_id', 'att_log_id')->where('status', 1);
    }

    // get attendance manual (active)
    public function UserAttManualActive() {
        return $this->hasOne('IhrV2\Models\AttendanceManual', 'att_id', 'att_log_id')->where('active', 1);
    }

    // get punch name
    // --------------
    public function getPunchNameAttribute() {
        if ($this->att_record_type == 0) {
            $name = '<span class="text-primary">IN</span>';
        }
        else {
            $name = '<span class="text-danger">OUT</span>';
        }
        return $name;
    }

    // public function UserAttendanceRemark() {
    //     return $this->hasOne('IhrV2\Models\AttendanceRemark');
    // }


}
