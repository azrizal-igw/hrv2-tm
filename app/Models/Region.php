<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

	protected $table = 'regions';


	// public $timestamps = false;



	protected $fillable = [
		'name',
		'name_eng',
	];


	// public function RegionManager() {
	// 	return $this->belongsTo('IhrV2\User', 'report_to', 'id');
	// }


	public function RegionManagerJob() {
		return $this->belongsTo('IhrV2\Models\UserJob', 'report_to', 'user_id')->where('status', 1);
	}

	public function ListState() {
		return $this->hasMany('IhrV2\Models\State', 'region_id');
	}


    // has one region manager
    // ----------------------
    public function RegionManager() {
        return $this->hasOne('IhrV2\Models\RegionUser', 'region_id')->where('status', 1);
    }








}
