<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeavePublicState extends Model
{
    protected $table = 'leave_public_states';
    

    protected $fillable = [
    	'leave_public_id',
    	'year',
    	'date',
    	'state_id',
        'status'
    ];



	public function StateName() {
		return $this->belongsTo('IhrV2\Models\State', 'state_id', 'code');
	}	

	public function PublicName() {
		return $this->belongsTo('IhrV2\Models\LeavePublic', 'leave_public_id')->where('status', 1);
	}	


    public function PublicNameByDate() {
        return $this->belongsTo('IhrV2\Models\LeavePublic', 'date', 'date')->where('status', 1);
    }   



    
}
