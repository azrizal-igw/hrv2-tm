<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceProcessTable extends Model
{


	protected $table = 'attendance_process_table';


    protected $fillable = [
    	'att_mykad_ic',
    	'att_password',
    	'location_id',
    	'att_record_type',
    	'att_input_type',
    	'att_log_time_in',
    	'att_log_time_out',
    	'att_userstamp',
    	'ip_add',
    	'att_timestamp',
    	'att_sys_status',
    	'remark',
        'att_date',
        'att_process_date'
    ];






}
