<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeaveApplication extends Model
{
	protected $table = 'leave_applications';

    protected $fillable = [
        'user_id', 
        'contract_id',
        'leave_type_id', 
        'date_apply', 
        'report_to', 
        'date_from', 
        'date_to',
        'is_half_day',        
        'desc',
        'sitecode',
        'active',
        'leave_id',
        'leave_details_id'
    ];

    // get relate job
    // --------------
    public function LeaveUserJob() {
    	return $this->hasOne('IhrV2\Models\UserJob', 'user_id', 'user_id')->where('status', 1);
    }

    // get relate contract
    // -------------------
    public function LeaveUserContract() {
    	return $this->belongsTo('IhrV2\Models\UserContract', 'contract_id');
    }

    public function LeaveContractCurrent() {
    	return $this->belongsTo('IhrV2\Models\UserContract', 'contract_id')->where('status', 1);
    }

    public function LeaveUserContractActive() {
    	return $this->hasOne('IhrV2\Models\UserContract', 'user_id', 'user_id')->where('status', 1);
    }

    public function setDateApplyAttribute($date)
    {
    	// $this->attributes['date_apply'] = Carbon::createFromFormat('Y-m-d', $date); // with date and time
    	$this->attributes['date_apply'] = Carbon::parse($date); // only date
    }

	public function LeaveUserDetail() {
		return $this->belongsTo('IhrV2\User', 'user_id');
	}

	public function LeaveSiteName() {
		return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
	}	

	// belongs to
	public function LeaveTypeName() {
		return $this->belongsTo('IhrV2\Models\LeaveType', 'leave_type_id');
	}

	public function LeaveReportToName() {
		return $this->belongsTo('IhrV2\User', 'report_to');
	}

	// leave attachment
	public function LeaveLatestAttachment() {
		return $this->hasOne('IhrV2\Models\LeaveAttachment', 'leave_id')->where('status', 1);
	}

	// get only latest history
	public function LeaveLatestHistory() {
		return $this->hasOne('IhrV2\Models\LeaveHistory', 'leave_id')->where('flag', 1);
	}

	// get only latest history
	public function LeaveLatestHistory2() {
		return $this->hasOne('IhrV2\Models\LeaveHistory', 'leave_id')->where('status', 1)->where('flag', 1);
	}

	// get previous history
	public function LeavePrevHistory() {
		return $this->hasMany('IhrV2\Models\LeaveHistory', 'leave_id')->where('flag', 0)->orderBy('id', 'desc');
	}

	public function LeavePending() {
		return $this->hasOne('IhrV2\Models\LeaveHistory', 'leave_id')->where('flag', 1)->where('status', 1);
	}

	public function LeaveApprove() {
		return $this->hasOne('IhrV2\Models\LeaveApprove', 'leave_id')->where('flag', 1);
	}	

	// hasmany
	public function LeaveAllHistory() {
		return $this->hasMany('IhrV2\Models\LeaveHistory', 'leave_id');
	}

	public function LeaveDateAll() {
		return $this->hasMany('IhrV2\Models\LeaveDate', 'leave_id');
	}

	// 
	public function LeaveDate() {
		// return $this->hasMany('IhrV2\Models\LeaveDate', 'leave_id')->where('leave_type', 1)->where('status', '=', 1);
		return $this->hasMany('IhrV2\Models\LeaveDate', 'leave_id')->where('status', 1);
	}

	public function LeaveDateAvail() {
		// return $this->hasMany('IhrV2\Models\LeaveDate', 'leave_id')->where('leave_type', 1)->where('status', '=', 1);
		return $this->hasMany('IhrV2\Models\LeaveDate', 'leave_id')->where('status', 1)->where('leave_type', 1);
	}


	public function LeaveHistoryApprove()
	{
		return $this->hasOne('IhrV2\Models\LeaveHistory', 'leave_id')->where('flag', 1)->where('status', 2);		
	}


	// public function leave_apply($data) 
	// {
	// 	$db_leave = new DbLeaveRepository;
		
	// 	// dd('test');
	// }	






}
