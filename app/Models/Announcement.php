<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    

	protected $table = 'announcements';


    protected $fillable = [
    	'user_id',
    	'apply_date',
    	'message',
    	'type_id',
    	'status',
    	'order'
    ];


	public function CreatedBy() {
		return $this->belongsTo('IhrV2\User', 'user_id');
	}	

    public function getAvailabilityAttribute()
    {
        if ($this->status == 1) {
            return '<span class="text-primary">ACTIVE</span>';
        }
        else if ($this->status == 2) {
            return '<span class="text-danger">CANCEL</span>';
        }
        else {
            return '<span class="text-default">UNKNOWN</span>';            
        }
    }


    public function getTypeNameAttribute()
    {
        if ($this->type_id == 1) {
            return 'Site';
        }
        else {
            return 'Admin';            
        }
    }




}
