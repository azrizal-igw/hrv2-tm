<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AggSiteDetailHistory extends Model {
    protected $connection = 'mysql2';

    protected $table = 'mycommSiteDetail_history';

    protected $fillable = [
        'REF_ID',
        'SITE_NAME',
        'STATE',
        'ADDRESS1',
        'ADDRESS2',
        'ADDRESS3',
        'POSTCODE',
        'CITY',
        'SERVICE_PROVIDER',
        'UST',
        'PHASE',
        'LOA_DATE',
        'COMMENCEMENT_DATE',
        'SAT_COMPLETION_DATE',
        'BACKHAUL',
        'CONTRACT_DURATION_MONTH',
        'NO_TEL',
        'TOTAL_REGISTERED_MEMBER',
        'TOTAL_ACTIVE_MEMBER',
        'PI1M _SPEED_MBPS',
        'HUB_SPEED_MBPS',
        'SITE_STATUS',
        'X',
        'Y',
        'REMARK',
        'WEBSITE',
        'date_modified',
        'BANDWIDTH',
        'TECHNOLOGY',
    ];
}
