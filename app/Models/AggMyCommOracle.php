<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AggMyCommOracle extends Model
{
    protected $connection = 'mysql2';
    public $timestamps = false;

    protected $table = 'mycommoracle';

    protected $fillable = [
        'f4',
        'f17',
    ];

}
