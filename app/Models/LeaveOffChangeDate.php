<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOffChangeDate extends Model {
    protected $table = 'leave_off_change_dates';

    protected $fillable = [
        'off_change_id',
        'type_id',
        'time_id',
        'off_date',
        'status',
    ];

    // off type name
    // -------------
    public function OffTypeName() {
        return $this->belongsTo('IhrV2\Models\LeaveOffType', 'type_id');
    }

    // time type name
    // --------------
    public function TimeTypeName() {
        return $this->belongsTo('IhrV2\Models\LeaveTimeType', 'time_id');
    }

}
