<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AggMyCommPI1MUsers2017d extends Model
{
    protected $connection = 'mysql2';
    public $timestamps = false;

    protected $table = 'mycommpi1musers2017d';

    protected $fillable = [
        'member_ic',
        'pi1m_refid',
    ];
}
