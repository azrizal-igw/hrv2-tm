<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{

	protected $table = 'leave_types';


    protected $fillable = [
    	'name',
    	'code',
    	'total',
    	'color_code',
    	'attachment',
    	'bypass',
    ];




}
