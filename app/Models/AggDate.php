<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AggDate extends Model
{
	protected $connection = 'mysql2';


	protected $table = 'mycommDates';



	protected $fillable = [
		'date',
		'staff_attendance',
		'staff_detail',
		'process',
		'export',
		'transfer'
	];

}
