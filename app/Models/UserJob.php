<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class UserJob extends Model
{
    //


	protected $table = 'user_jobs';




    protected $fillable = [
        'user_id',
        'staff_id',
        'join_date',
        'position_id',
        'phase_id',
        'region_id',
        'notes',
        'sitecode', 
        'resign_date',
        'notification',
        'status',
        'flag',
        'updated_by',
        'date_modified'
    ];

    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    public function SiteName() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    public function PositionName() {
        return $this->belongsTo('IhrV2\Models\Position', 'position_id');
    }
    
    public function PhaseName() {
        return $this->belongsTo('IhrV2\Models\Phase', 'phase_id');
    }

    public function RegionName() {
        return $this->belongsTo('IhrV2\Models\Region', 'region_id');
    }

    public function StatusName() {
        return $this->belongsTo('IhrV2\Models\Status', 'status');
    }

    public function UpdatedBy() {
        return $this->belongsTo('IhrV2\User', 'updated_by');
    }

    // display status color
    // --------------------
    public function getStatusColorAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">'.strtoupper($this->StatusName->name).'</span>';
        }
        else {
            return '<span class="text-danger">'.strtoupper($this->StatusName->name).'</span>';
        }        
    }

    // display status color without reference table
    // --------------------------------------------
    public function getAvailabilityAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">ACTIVE</span>';
        }
        else {
            return '<span class="text-danger">INACTIVE</span>';
        }        
    }



    
}
