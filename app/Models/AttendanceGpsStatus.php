<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceGpsStatus extends Model
{
    protected $table = 'attendance_gps_status';
}
