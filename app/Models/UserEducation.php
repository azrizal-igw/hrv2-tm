<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    //

	protected $table = 'user_educations';


    protected $fillable = [
        'user_id',
        'year_from',
        'year_to',
        'name_education',
        'result'
    ];


    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }




  


}
