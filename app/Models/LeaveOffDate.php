<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOffDate extends Model
{
	protected $table = 'leave_off_dates';

    protected $fillable = [
        'action_date',
        'action_by',
    	'year',
    	'group_id',
    	'type_id',
    	'off_date',
        'updated_by',
    	'status'
    ];

	public function ListStates()
	{
	    return $this->hasMany('IhrV2\Models\LeaveOffDateState', 'date_id');
	}

    public function OffTypeName() {
        return $this->belongsTo('IhrV2\Models\LeaveOffType', 'type_id');
    }

    public function OffStateAll() {
        return $this->hasMany('IhrV2\Models\LeaveOffDateState', 'date_id');
    }

    public function OffDayState()
    {
        return $this->hasOne('IhrV2\Models\LeaveOffDateState', 'date_id');
    }

    public function StatusName() {
        return $this->belongsTo('IhrV2\Models\Status', 'status');
    }

    public function ActionBy() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    public function getAvailabilityAttribute()
    {
        if ($this->status == 1) {
            return '<span class="text-primary">Active</span>';
        }
        else if ($this->status == 2) {
            return '<span class="text-danger">Cancel</span>';
        }
        else {
            return '<span class="text-default">Unknown</span>';            
        }
    }



}
