<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveStatus extends Model
{
    //

	protected $table = 'leave_status';


    public function getNoteAttribute()
    {
    	// leave pending
    	// -------------
        if ($this->id == 1) {
            return 'Note: This Leave is waiting for Approval from Region Manager (RM).';
        }

        // leave approved
        // --------------
        else if ($this->id == 2) {
            return 'Note: This Leave is Approved.';
        }

        // leave rejected
        // --------------
        else if ($this->id == 3) {
            return 'Note: This Leave is Rejected. Please contact Regional Officer for Further Information.';
        }

        // apply cancel
        // ------------
        else if ($this->id == 4) {
        	return 'Note: This Leave already Approve but Canceled by Site Supervisor. Awaits Approval from Regional Officer.';
        }

        // leave cancel
        // ------------
        else if ($this->id == 5) {
        	return 'Note: This Leave already Cancel by Site Supervisor.';
        }

        // approved cancel
        // ---------------
        else if ($this->id == 6) {
        	return 'Note: This Leave is Approved for Apply Cancel.';
        }

        // rejected cancel
        // ---------------
        else if ($this->id == 7) {        
            return 'Note: This Leave is Rejected for Apply Cancel.';
        }

        // leave canceled 
        // --------------
        else if ($this->id == 8) {
            return 'Note: This Leave is Canceled.';
        }                  
        else {
            return 'Unknown';
        }
    }



}



    
