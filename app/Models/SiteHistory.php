<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class SiteHistory extends Model
{
	protected $table = 'site_histories';

	// public $timestamps = false;


    protected $fillable = [
    	'code',
    	'name',
    	'full_name',
        'tm_name',
        'both_name',
        'old_name',
    	'phase_id',
    	'region_id',
    	'street1',
    	'street2',
    	'postal_code',
    	'city',
    	'district',
    	'state_id',
    	'address_label',
    	'email',
        'website',
    	'latitude',
    	'longitude',
        'start',
        'end',
    	'bandwidth',
    	'backhaul',
        'location_id',
        'status',
        'date_modified',
    	'created_at',
    	'updated_at'
    ];
}
