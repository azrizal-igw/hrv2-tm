<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveAttachment extends Model
{
    //


	protected $table = 'leave_attachments';
	

    
    protected $fillable = [
    	'user_id',
    	'leave_id',
    	'filename',
    	'ext',
    	'size',
    	'thumb_name',
    	'status'
    ];




    public function LeaveDetail() {
        return $this->belongsTo('IhrV2\Models\LeaveApplication', 'leave_id');
    }

    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }


	    
}
