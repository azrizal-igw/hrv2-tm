<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class RegionUser extends Model
{
    //

	protected $table = 'region_users';



	protected $fillable = [
		'region_id',
		'user_id',
		'codes',
		'includes',
		'exceptions',
		'status'
	];



	public function RegionManagerDetail() {
		return $this->belongsTo('IhrV2\User', 'user_id');
	}


	public function RegionName() {
		return $this->belongsTo('IhrV2\Models\Region', 'region_id');
	}


}


