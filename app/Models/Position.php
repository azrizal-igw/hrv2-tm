<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    //

	protected $table = 'positions';


    protected $fillable = [
    	'id',
    	'name',
    	'salary',
    	'group_id'
    ];


	public function GroupName() {
		return $this->belongsTo('IhrV2\Models\Group', 'group_id');
	}	
	




    
}
