<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceManual extends Model {

    protected $table = 'attendance_manual';

    protected $fillable = [
        'att_id',
        'icno',
        'user_id',
        'type_id',
        'att_log_time',
        'sitecode',
        'remark',
        'action_by',
        'action_date',
        'updated_by',
        'active'
    ];

    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'icno', 'icno');
    }

    public function UserInfo() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    public function UserSite() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    public function UserActionBy() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    public function UserUpdatedBy() {
        return $this->belongsTo('IhrV2\User', 'updated_by');
    }

    public function AttendanceDetail() {
        return $this->belongsTo('IhrV2\Models\Attendance', 'att_id', 'att_log_id');
    }

    // get user job
    // ------------
    public function UserJob() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id', 'user_id')->where('flag', 1);
    }

    // get attachment
    // --------------
    public function AttManualAttachment() {
        return $this->hasOne('IhrV2\Models\AttendanceManualAttachment', 'att_manual_id')->where('status', 1);
    }
}
