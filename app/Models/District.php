<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    //
	protected $table = 'districts';



    public function StateName() {
        return $this->belongsTo('IhrV2\Models\State', 'state_id');
    }   



}
