<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class UserHistory extends Model
{
    protected $table = 'user_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'staff_id',
        'prev_staff_id',
        'email',
        'password',
        'api_token',
        'group_id',
        'is_admin',
        'icno',
        'permanent_street_1',
        'permanent_street_2',
        'permanent_postcode',
        'permanent_city',
        'permanent_state',
        'correspondence_street_1',
        'correspondence_street_2',
        'correspondence_postcode',
        'correspondence_city',
        'correspondence_state',
        'telno1',
        'telno2',
        'hpno',
        'hpno2',
        'faxno',
        'website',
        'personal_email',
        'work_email',
        'gender_id',
        'marital_id',
        'height',
        'weight',
        'dob',
        'pob',
        'nationality_id',
        'race_id',
        'religion_id',
        'partner_name',
        'partner_phone',
        'child_no',
        'sitecode',
        'itaxno',
        'epfno',
        'socsono',
        'bankname',
        'bankno',
        'status',
        'remember_token',
        'sid',
        'action_date',
        'last_login',
        'date_created',
        'date_modified',
        'imei',
        'device',
        'edit',
        'mobile',
        'created_at',
        'updated_at',
    ];
}
