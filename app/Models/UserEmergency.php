<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class UserEmergency extends Model
{
    //

	protected $table = 'user_emergency_contacts';



    protected $fillable = [
        'user_id',
        'name',
        'relation',
        'address',
        'telno'
    ];

    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }









    
}
