<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveAdd extends Model
{
	protected $table = 'leave_add';

    protected $fillable = [
        'contract_id',
        'user_id', 
        'leave_type_id', 
        'type_id',
        'action_date',
        'action_by',
        'total',
        'reason',
        'sitecode', 
        'status',
        'updated_by'
    ];

    // get relate contract
    // -------------------
    public function LeaveUserContract() {
        return $this->belongsTo('IhrV2\Models\UserContract', 'contract_id');
    }

    public function LeaveUserContractActive() {
        return $this->hasOne('IhrV2\Models\UserContract', 'user_id', 'user_id')->where('status', 1);
    }

    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    public function LeaveTypeName() {
        return $this->belongsTo('IhrV2\Models\LeaveType', 'leave_type_id');
    }

    public function ActionByDetail() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    public function LeaveAddAttachment() {
        return $this->hasOne('IhrV2\Models\LeaveAddAttachment', 'leave_add_id')->where('status', 1);
    }


}
