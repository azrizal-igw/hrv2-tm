<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceRemarkStatus extends Model
{
    protected $table = 'attendance_remark_status';
}
