<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class UserPhoto extends Model {
    //

    protected $table = 'user_photos';

    protected $fillable = [
        'user_id',
        'sitecode',
        'action_date',
        'action_by',
        'photo',
        'photo_thumb',
        'ext',
        'size',
        'active'
    ];

    // get user info
    // -------------
    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    // get user job
    // ------------
    public function UserJob() {
        return $this->belongsTo('IhrV2\Models\UserJob', 'user_id', 'user_id')->where('flag', 1);
    }

    // get site info
    // --------------
    public function SiteDetail() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    // get photo pending
    // -----------------
    public function PhotoPending() {
        return $this->hasOne('IhrV2\Models\UserPhotoHistory', 'photo_id')->where('status', 1)->where('flag', 1);
    }

    // get photo approved
    // ------------------
    public function PhotoApproved() {
        return $this->hasOne('IhrV2\Models\UserPhotoHistory', 'photo_id')->where('status', 2)->where('flag', 1);
    }

    // get photo latest status
    // -----------------------
    public function PhotoLatestStatus() {
        return $this->hasOne('IhrV2\Models\UserPhotoHistory', 'photo_id')->where('flag', 1);
    }

}
