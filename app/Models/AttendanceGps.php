<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceGps extends Model
{
    protected $table = 'attendance_gps';

    protected $fillable = [
        'att_log_id',
        'user_id',
        'sitecode',
        'latitude',
        'longitude',
        'qr_code',
        'datetime',
        'in_out',
        'location',
        'remark',
        'flag',
        'imei',
        'version',
        'build_no',
        'model',
        'transfer',
        'accuracy',
        'ip_address',
        'updated_by',
        'status'
    ];

    // get attendance status
    // ---------------------
    public function AttGpsStatus() {
        return $this->belongsTo('IhrV2\Models\AttendanceGpsStatus', 'flag');
    }

    // get user info
    // -------------
    public function AttGpsUser() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    // get user job
    // ------------
    public function AttGpsUserJob() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id', 'user_id')->where('flag', 1);        
    }

    // get site info
    // --------------
    public function AttGpsSite() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }


}
