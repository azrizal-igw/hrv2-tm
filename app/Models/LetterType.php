<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LetterType extends Model
{
	protected $table = 'letter_types';


    protected $fillable = [
    	'name'
    ];


}
