<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class UserPhotoHistory extends Model
{


	protected $table = 'user_photo_histories';


    protected $fillable = [
    	'photo_id',
    	'user_id',
    	'action_date',
    	'action_by',
    	'remark',
    	'status',
    	'flag'
    ];

    // get status name
    // ---------------
    public function PhotoStatusName() {
        return $this->belongsTo('IhrV2\Models\UserPhotoStatus', 'status');
    }

    // get action by info
    // ------------------
    public function PhotoActionBy() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    // get status name by color
    // ------------------------
    public function getAvailabilityAttribute()
    {
        $status = $this->PhotoStatusName->name;
        if ($this->status == 1) {
            $color = 'danger';
        }
        else if ($this->status == 2) {
            $color = 'primary';
        }
        else {
            $color = 'default';
        }
        return '<span class="text-'.$color.'">'.$status.'</span>'; 
    }

}
