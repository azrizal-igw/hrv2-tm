<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceApp extends Model
{
	protected $table = 'attendance_app';

	protected $fillable = [
		'user_id',
		'version',
		'build_no',
		'server1',
		'server2',
		'server3',
		'server4',
		'server5',
		'status'
	];

    public function getAvailabilityAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">ACTIVE</span>';
        } else if ($this->status == 2) {
            return '<span class="text-danger">INACTIVE</span>';
        } else {
            return '<span class="text-default">UNKNOWN</span>';
        }
    }

}
