<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceRemarkHistory extends Model
{


	protected $table = 'attendance_remark_histories';


    protected $fillable = [
    	'att_remark_id',
        'user_id',
        'action_date',
        'action_by',
        'action_remark',
    	'status',
    	'flag'
    ];

    // get status name
    // ---------------
    public function AttRemarkStatusName() {
        return $this->belongsTo('IhrV2\Models\AttendanceRemarkStatus', 'status');
    }

    // get action by name
    // ------------------
    public function AttRemarkActionBy() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    // get status color
    // ----------------
    public function getStatusColorAttribute() {

        // status pending
        // --------------
        if ($this->status == 1) {
            return '<span class="text-danger">'.$this->AttRemarkStatusName->name.'</span>';
        } 

        // status approved
        // ---------------
        else if ($this->status == 2) {
            return '<span class="text-primary">'.$this->AttRemarkStatusName->name.'</span>';
        } 

        // status updated
        // --------------
        else if ($this->status == 3) {
            return '<span class="text-info">'.$this->AttRemarkStatusName->name.'</span>';
        } 

        // status canceled
        // ---------------
        else if (in_array($this->status, array(4,5))) {
            return '<span class="text-default">'.$this->AttRemarkStatusName->name.'</span>';
        } 
        else {
            return '<span class="text-default">Unknown</span>';
        }
    }


}
