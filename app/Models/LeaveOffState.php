<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOffState extends Model
{



	protected $table = 'leave_off_states';


    protected $fillable = [
    	'group_id',
    	'state_id'
    ];



	public function StateName() {
		return $this->belongsTo('IhrV2\Models\State', 'state_id', 'code');
	}	



}
