<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOffChangeAttachment extends Model {

    // table name
    // ----------
    protected $table = 'leave_off_change_attachments';

    // field name
    // ----------
    protected $fillable = [
        'user_id',
        'off_change_id',
        'filename',
        'ext',
        'size',
        'thumb_name',
        'status',
    ];

}
