<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveRepHistory extends Model
{
    //

	protected $table = 'leave_rep_histories';


    protected $fillable = [
    	'user_id',
    	'leave_rep_id',
    	'action_date',
    	'action_by',
    	'action_remark',
    	'status',
    	'flag'
    ];

	public function LeaveRepStatusName() {
		return $this->belongsTo('IhrV2\Models\LeaveStatus', 'status');
	}

	public function LeaveActionByName() {
		return $this->belongsTo('IhrV2\User', 'action_by');
	}

    public function LeaveRepInfo() {
        return $this->belongsTo('IhrV2\Models\LeaveRepApplication', 'leave_rep_id');
    }

    public function getAvailabilityAttribute()
    {
        if (in_array($this->status, array(2,6))) {
            return 'primary';
        }
        else if (in_array($this->status, array(1,4))) {
            return 'danger';
        }
        else if (in_array($this->status, array(3,5,7))) {
            return 'muted';
        }        
        else {
            return 'info';            
        }
    }
    
}
