<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOffChange extends Model {

    // table name
    // ----------
    protected $table = 'leave_off_changes';

    // field name
    // ----------
    protected $fillable = [
        'user_id',
        'sitecode',
        'start_date',
        'end_date',
        'reason',
        'created_by',
        'updated_by',
        'status',
    ];

    // get off dates
    // -------------
    public function LeaveAllOffDate() {
        return $this->hasMany('IhrV2\Models\LeaveOffChangeDate', 'off_change_id')->where('status', 1)->orderBy('id', 'desc');
    }

    // get site detail
    // ---------------
    public function SiteDetail() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    // get created by
    // --------------
    public function CreatedByDetail() {
        return $this->belongsTo('IhrV2\User', 'created_by');
    }

    // has attachment
    // --------------
    public function Attachment() {
        return $this->hasOne('IhrV2\Models\LeaveOffChangeAttachment', 'off_change_id')->where('status', 1);
    }

}
