<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class UserNoPartner extends Model
{
    protected $table = 'user_no_partners';

    protected $fillable = [
        'user_id',
        'sitecode',
        'contract_id',
        'date_from',
        'date_to',
        'status',
        'created_by',
        'updated_by'
    ];
}
