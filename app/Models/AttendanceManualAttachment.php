<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceManualAttachment extends Model {

    protected $table = 'attendance_manual_attachments';

    protected $fillable = [
        'att_manual_id',
        'user_id',
        'filename',
        'ext',
        'size',
        'thumb_name',
        'status'
    ];
}
