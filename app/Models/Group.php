<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
	protected $table = 'groups';


    protected $fillable = [
    	'name',
    	'prefix',
    ];





	public function ListPosition() {
		return $this->hasMany('\IhrV2\Models\Position', 'group_id');
	}



}
