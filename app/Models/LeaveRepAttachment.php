<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveRepAttachment extends Model
{
    //

	protected $table = 'leave_rep_attachments';


    protected $fillable = [
    	'user_id',
    	'leave_rep_id',
    	'filename',
    	'ext',
    	'size',
    	'thumb_name',
    	'status'
    ];




    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }




}
