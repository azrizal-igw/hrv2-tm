<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceGpsLog extends Model
{
    protected $table = 'attendance_gps_logs';

    protected $fillable = [
    	'user_id',
    	'sitecode',
    	'filename',
    	'ext',
    	'size',
    	'status'
    ];
}
