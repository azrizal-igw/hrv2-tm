<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveTimeType extends Model {

    protected $table = 'leave_time_types';

    protected $fillable = [
        'name',
    ];

}
