<?php

namespace IhrV2\Models\Training;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
	protected $table = 'trainings';

    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'location',        
    	'description',
        'status',        
        'created_by',
    	'updated_by',
    ];

    // created by
    // ----------
    public function CreatedBy() {
        return $this->belongsTo('IhrV2\User', 'created_by');
    }    

    public function getAvailabilityAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">ACTIVE</span>';
        } else if ($this->status == 2) {
            return '<span class="text-danger">CANCEL</span>';
        } else {
            return '<span class="text-default">UNKNOWN</span>';
        }
    }
}
