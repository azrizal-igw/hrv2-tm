<?php

namespace IhrV2\Models\Training;

use Illuminate\Database\Eloquent\Model;

class TrainingAttendee extends Model
{
	protected $table = 'training_attendees';

    protected $fillable = [
    	'training_id',
    	'date_id',
    	'user_id',
    	'sitecode',
    	'status'
    ];
}
