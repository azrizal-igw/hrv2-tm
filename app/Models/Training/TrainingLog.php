<?php

namespace IhrV2\Models\Training;

use Illuminate\Database\Eloquent\Model;

class TrainingLog extends Model
{
	protected $table = 'training_logs';

    protected $fillable = [
    	'user_id',
    	'sitecode',
    	'filename',
    	'ext',
    	'size',
    	'status'
    ];
}
