<?php

namespace IhrV2\Models\Training;

use Illuminate\Database\Eloquent\Model;

class TrainingAttendanceLog extends Model
{
	protected $table = 'training_attendance_logs';

    protected $fillable = [
    	'training_id',
    	'user_id',
        'sitecode',
    	'latitude',
    	'longitude',
    	'qr_code',
    	'datetime',
    	'location',
    	'remark',
    	'flag',
    	'imei',
    	'version',
    	'build_no',
        'model'
    ];
}
