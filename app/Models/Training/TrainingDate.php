<?php

namespace IhrV2\Models\Training;

use Illuminate\Database\Eloquent\Model;

class TrainingDate extends Model
{
	protected $table = 'training_dates';

    protected $fillable = [
    	'training_id',
    	'code',
    	'date',
    	'slot',
    	'status'
    ];
}
