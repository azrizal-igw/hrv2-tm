<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeavePublic extends Model
{
    


    protected $table = 'leave_public';


    protected $fillable = [
    	'year',
    	'desc',
    	'date',
    	'status',
        'date_modified',
        'updated_by'
    ];



    // excludes states (perlis, penang, kl, labuan, putrajaya)
    // -------------------------------------------------------
	public function LeavePublicState() {
        $excludes = array(508, 509, 514, 515, 516);
		return $this->hasMany('IhrV2\Models\LeavePublicState', 'leave_public_id')->where('status', 1)->whereNotIn('state_id', $excludes);
	}


    public function StatusName() {
        return $this->belongsTo('IhrV2\Models\Status', 'status');
    }


}
