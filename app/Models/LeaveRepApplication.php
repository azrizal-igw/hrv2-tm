<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveRepApplication extends Model
{
    //

    protected $table = 'leave_rep_applications';

    protected $fillable = [
        'user_id',
        'contract_id',
        'date_apply',
        'no_day',
        'year_month',
        'date_work',
        'report_to',
        'instructed_by',
        'location',
        'reason',
        'notes',
        'sitecode',
        'active',
        'leave_rep_id',
    ];

    // get relate contract
    // -------------------
    public function LeaveRepContract()
    {
        return $this->belongsTo('IhrV2\Models\UserContract', 'contract_id');
    }

    public function LeaveRepContractCurrent()
    {
        return $this->belongsTo('IhrV2\Models\UserContract', 'contract_id')->where('status', 1);
    }

    public function LeaveRepContractActive()
    {
        return $this->hasOne('IhrV2\Models\UserContract', 'user_id', 'user_id')->where('status', 1);
    }

    // get status pending
    // ------------------
    public function LeaveRepPending()
    {
        return $this->hasOne('IhrV2\Models\LeaveRepHistory', 'leave_rep_id')->where('flag', 1)->where('status', 1);
    }

    // get status approve
    // ------------------
    public function LeaveRepApprove()
    {
        return $this->hasOne('IhrV2\Models\LeaveRepApprove', 'leave_rep_id')->where('flag', 1);
    }

    // get only latest history
    // -----------------------
    public function LeaveRepLatestHistory()
    {
        return $this->hasOne('IhrV2\Models\LeaveRepHistory', 'leave_rep_id')->where('flag', 1);
    }

    public function LeaveRepUserDetail()
    {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    public function LeaveRepSiteName()
    {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    // get previous history
    public function LeaveRepPrevHistory()
    {
        return $this->hasMany('IhrV2\Models\LeaveRepHistory', 'leave_rep_id')->where('flag', 0)->orderBy('id', 'desc');
    }

    // leave attachment
    public function LeaveRepLatestAttachment()
    {
        return $this->hasOne('IhrV2\Models\LeaveRepAttachment', 'leave_rep_id')->where('status', 1);
    }

    // reporting officer
    public function LeaveRepReportToName()
    {
        return $this->belongsTo('IhrV2\User', 'report_to');
    }

    // user jobs
    public function LeaveRepUserJob()
    {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id', 'user_id');
    }

}
