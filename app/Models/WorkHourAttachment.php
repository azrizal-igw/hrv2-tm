<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class WorkHourAttachment extends Model
{
    protected $table = 'work_hour_attachments';

    protected $fillable = [
    	'work_hour_id',
    	'user_id',
    	'filename',
    	'ext',
    	'size',
    	'thumb_name',
    	'status'
    ];    
}
