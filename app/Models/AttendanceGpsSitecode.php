<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AttendanceGpsSitecode extends Model
{
    protected $table = 'attendance_gps_sitecodes';

    protected $fillable = [
    	'sitecode',
    	'status',
    	'created_by',
    	'updated_by'
    ];
}
