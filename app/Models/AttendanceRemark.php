<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;
use IhrV2\Repositories\LeaveRepository;

class AttendanceRemark extends Model
{

	protected $table = 'attendance_remarks';


    protected $fillable = [
        'user_id',
    	'icno',
        'sitecode',
        'date',
        'status_id',
        'other_id',
        'notes',
        'action_by',
        'action_date',
        'active'
    ];


    // // get attendance record
    // // ---------------------
    // public function AttInfo() {
    //     return $this->hasOne('IhrV2\Models\Attendance', 'att_remark_id')->where('status', 1);
    // }

    // get updatee by info
    // -------------------
    public function UserUpdatedBy() {
        return $this->belongsTo('IhrV2\User', 'updated_by');
    }

    // get latest history
    // ------------------
    public function AttPendingStatus() {
        return $this->hasOne('IhrV2\Models\AttendanceRemarkHistory', 'att_remark_id')->where('status', 1)->where('flag', 1);
    }

    public function AttApplyRemark() {
        return $this->hasOne('IhrV2\Models\AttendanceRemarkHistory', 'att_remark_id')->where('status', 1);
    }

    public function AttPendingHistory() {
        return $this->hasOne('IhrV2\Models\AttendanceRemarkHistory', 'att_remark_id')->where('status', 1);
    }

    // get attachment
    // --------------
    public function AttActiveAttachment() {
        return $this->hasOne('IhrV2\Models\AttendanceRemarkAttachment', 'att_remark_id')->where('status', 1);
    }

    // get all history
    // ---------------
    public function AttAllStatus() {
        return $this->hasMany('IhrV2\Models\AttendanceRemarkHistory', 'att_remark_id')->orderBy('id', 'desc');
    }

    // get active remark
    // ------------------
    public function AttActiveStatus() {
        return $this->hasOne('IhrV2\Models\AttendanceRemarkHistory', 'att_remark_id')->where('flag', 1);
    }    

    // get approve remark
    // ------------------
    public function AttApproveStatus() {
        return $this->hasOne('IhrV2\Models\AttendanceRemarkHistory', 'att_remark_id')->where('status', 2)->where('flag', 1);
    }

    // get status name
    // ---------------
    public function AttStatusName() {
        return $this->belongsTo('IhrV2\Models\AttendanceStatus', 'status_id');
    }

    // get other status name
    // ---------------------
    public function AttOtherName() {
        return $this->belongsTo('IhrV2\Models\AttendanceStatus', 'other_id');
    }

    // get user info
    // -------------
    public function AttUserInfo() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    // get action by
    // -------------
    public function AttActionBy() {
        return $this->belongsTo('IhrV2\User', 'action_by');
    }

    // get user job
    // ------------
    public function AttUserJob() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id', 'user_id')->where('flag', 1);        
    }

    // get site info
    // --------------
    public function AttSiteInfo() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }







}




