<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class WorkHourOffDate extends Model
{
    protected $table = 'work_hour_off_dates';

    protected $fillable = [
    	'work_hour_id',
    	'off_date',
    	'status'
    ];

}
