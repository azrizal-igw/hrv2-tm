<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class FastingDateState extends Model
{
	protected $table = 'fasting_date_states';


    protected $fillable = [
    	'fasting_date_id',
    	'state_id',
    	'status',
    ];


    public function StateName() {
    	return $this->belongsTo('IhrV2\Models\State', 'state_id', 'code');
    }

}
