<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AggStaffAttendance extends Model
{

    protected $connection = 'mysql2';

    protected $table = 'mycommStaffAttendance';

    protected $fillable = [
        'PI1M_REFID',
        'SERVICE_PROVIDER',
        'Staff_IC',
        'CheckIn_Date',
        'CheckIn_Time',
        'CheckOut_Time',
        'CheckIn_Status',
        'Date',
        'Send_Date',
        'Error_Message',
    ];

    public function UserDetail()
    {
        return $this->belongsTo('IhrV2\User', 'Staff_IC', 'icno');
    }

    public function SiteDetail()
    {
        return $this->belongsTo('IhrV2\Models\Site', 'PI1M_REFID', 'code');
    }
}
