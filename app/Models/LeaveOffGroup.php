<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOffGroup extends Model
{


	protected $table = 'leave_off_groups';


    protected $fillable = [
        'name'
    ];


	// public function OffList()
	// {
	//     return $this->belongsToMany('IhrV2\Models\LeaveOffDate', 'off_group_id', 'group_id');
	// }


    public function LeaveOffDates()
    {
    	return $this->hasMany('IhrV2\Models\LeaveOffDate', 'group_id')->where('status', 1)->orderBy('id', 'asc');
    }


    public function LeaveOffStates()
    {
        return $this->hasMany('IhrV2\Models\LeaveOffState', 'group_id')->orderBy('id', 'asc');
    }


    public function LeaveOffDateStates()
    {
        return $this->belongsToMany('IhrV2\Models\LeaveOffDateState');
    }

}
