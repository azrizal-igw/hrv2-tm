<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveDate extends Model
{

	protected $table = 'leave_dates';

    protected $fillable = [
    	'user_id',
    	'leave_id',
    	'leave_date',
    	'leave_type',
    	'status'
    ];

    // get user detail
    // ---------------
    public function LeaveUserDetail() {
    	return $this->belongsTo('IhrV2\User', 'user_id');
    }

    // get leave info
    // --------------
    public function LeaveInfo() {
        return $this->belongsTo('IhrV2\Models\LeaveApplication', 'leave_id');
    }       

    // get AL leave info
    // -----------------
    public function LeaveInfoAL() {
        return $this->belongsTo('IhrV2\Models\LeaveApplication', 'leave_id')->where('leave_type_id', 1);
    }  

    // get RL leave info
    // -----------------
    public function LeaveInfoRL() {
        return $this->belongsTo('IhrV2\Models\LeaveApplication', 'leave_id')->where('leave_type_id', 6);
    }  

    // get leave approves
    // ------------------
    public function LeaveApproveOne() {
        return $this->hasOne('IhrV2\Models\LeaveApprove', 'leave_id', 'leave_id')->where('flag', 1);
    }

    // get leave approves AL/RL
    // ------------------------
    public function LeaveApproveALRL() {
        return $this->hasOne('IhrV2\Models\LeaveApprove', 'leave_id', 'leave_id')->where('flag', 1)->whereIn('leave_type_id', array(1,6));
    }

    // get leave history
    // -----------------
    public function LeaveHistoryOne() {
        return $this->hasOne('IhrV2\Models\LeaveHistory', 'leave_id', 'leave_id');
    }

    // get leave approve of annual and unplan
    // --------------------------------------
    public function LeaveApproveAnnual() {
        return $this->hasOne('IhrV2\Models\LeaveApprove', 'leave_id', 'leave_id')
        ->whereIn('leave_type_id', array(1,13,8)) // annual/unplan/maternity
        ->where('flag', 1);
    }

}
