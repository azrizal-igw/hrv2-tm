<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserContract extends Model
{
   

	protected $table = 'user_contracts';



    protected $fillable = [
    	'user_id',
    	'date_from',
    	'date_to',
    	'salary',
    	'status_contract_id',
    	'sitecode',
    	'status',
        'flag',
        'updated_by',
        'date_modified'
    ];


    public function SiteName() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }   

	public function ContractName() {
		return $this->belongsTo('IhrV2\Models\UserContractStatus', 'status_contract_id');
	}

    public function UserDetail() {
        return $this->belongsTo('IhrV2\User', 'user_id');
    }

    public function LeaveApplication() {
        return $this->hasMany('IhrV2\Models\LeaveApplication', 'contract_id');
    }

    public function StatusName() {
        return $this->belongsTo('IhrV2\Models\Status', 'status');
    }
    
    public function UpdatedBy() {
        return $this->belongsTo('IhrV2\User', 'updated_by');
    }

    // display status color
    // --------------------
    public function getStatusColorAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">'.strtoupper($this->StatusName->name).'</span>';
        }
        else {
            return '<span class="text-danger">'.strtoupper($this->StatusName->name).'</span>';
        }        
    }

    // get status contract by checking the date
    // ----------------------------------------
    public function getStatusContractAttribute() {

        // compare date today and end date
        // -------------------------------
        if (Carbon::now()->gt(Carbon::parse($this->date_to))) {
            return '<span class="text-danger">ENDED</span>';
        } 
        else {
            return '<span class="text-primary">ACTIVE</span>';
        }        
    }
    
    // get total duration
    // ------------------
    public function getDurationAttribute() {
        if ($this->date_from != '0000-00-00' && $this->date_to != '0000-00-00') {
            return Carbon::parse($this->date_from)->diffInDays(Carbon::parse($this->date_to)) + 1;
        }
        else {
            return '-';
        }
    }    

    // get total remaining days
    // ------------------------
    public function getRemainDayAttribute() {
        $date_to = Carbon::parse($this->date_to);
        $today = Carbon::parse(Carbon::now());
        if (Carbon::parse($this->date_to)->gt(Carbon::now()) && $this->date_to != '0000-00-00') {
            return $today->diffInDays($date_to);
            // return Carbon::parse(Carbon::now())->diffInDays(Carbon::parse($this->date_to))->format('Y-m-d');
        }
        else {
            return '-';
        }
    }

}
