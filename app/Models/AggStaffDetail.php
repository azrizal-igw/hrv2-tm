<?php

namespace IhrV2\Models;

use Illuminate\Database\Eloquent\Model;

class AggStaffDetail extends Model
{
    

    protected $connection = 'mysql2';


	protected $table = 'mycommStaffDetail';



	protected $fillable = [
		'PI1M_REFID',
		'SERVICE_PROVIDER',
		'Staff_IC',
		'Staff_Name',
		'Contact_Number',
		'Contact_Email',
		'Position',
		'Status',
		'Send_Date',
		'Error_Message'
	];



	// public function StatusName() {
	// 	return $this->belongsTo('IhrV2\Models\Status', 'id', 'Status');
	// }	


	public function SiteDetail() {
		return $this->belongsTo('IhrV2\Models\Site', 'PI1M_REFID', 'code');
	}	


}
