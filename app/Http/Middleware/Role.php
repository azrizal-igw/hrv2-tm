<?php

namespace IhrV2\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

// use Illuminate\Http\RedirectResponse;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next)
    // {
    //     return $next($request);
    // }

    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->user()->group_id != 3) {

            // permission middleware based on group id
            // ---------------------------------------
            $roles = array_except(func_get_args(), [0, 1]);
            foreach ($roles as $role) {
                if ($role == $this->auth->user()->group_id) {
                    return $next($request);
                }
            }
            return response()->view('errors.code', [
                'header' => 'Page Not Found',
                'msg' => 'Sorry, the page you are looking for is not permissible.',
            ], 404);
        } else {
            abort(404);
        }

    }

}
