<?php

namespace IhrV2\Http\Requests\Leave\RL;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'no_day' => 'required',
            'month' => 'required',
            'year' => 'required',
            'date_work' => 'required',
            'rep_file' => 'required|min:1|max:2000|mimes:jpeg,jpg,png' 
        ];
    }

    public function messages()
    {
        return [
            'no_day.required' => 'Please select No. of Days.',
            'month.required' => 'Please select Month.',
            'year.required' => 'Please select Year.',
            'date_work.required' => 'Please select Work/Start Date.',
            'rep_file.required' => 'Please select Attachment',
            'rep_file.min' => 'File attachment is too small.',
            'rep_file.max' => 'File attachment is too large. Please resize it.',
            'rep_file.mimes' => 'File attachment is not allowed.'            
        ];
    } 
}
