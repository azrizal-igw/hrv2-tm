<?php

namespace IhrV2\Http\Requests\Leave;

use IhrV2\Http\Requests\Request;

class Select extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'leave_type_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'leave_type_id.required' => 'Please select Leave Type.',
        ];
    } 

}
