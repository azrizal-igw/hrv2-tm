<?php

namespace IhrV2\Http\Requests\Leave;

use IhrV2\Http\Requests\Request;
use Session;
use IhrV2\Models\LeaveType;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        // get lists of leave types
        // ------------------------
        $types = LeaveType::where('attachment', 1)->pluck('id')->toArray();

        // check if leave must have attachment
        // medical/compassionate/calamity/hospitalization/maternity/paternity/umrah/haji/married/unplan/prolong
        // ----------------------------------------------------------------------------------------------------
        if (in_array(Session::get('leave_type_id'), $types)) {
            $required = 'required';
        }
        else {
            $required = null;
        }

        return [
            // 'desc' => 'required|min:50',
            'desc' => 'required',
            'date_from' => 'required',
            'date_to' => 'required|sometimes',
            'leave_file' => "$required|min:1|max:2000|mimes:jpeg,jpg,png"
        ];
    }

    public function messages()
    {
        return [
            'desc.required' => 'Please insert Description.',
            // 'desc.min' => 'Description must long 50 characters and above.',
            'date_from.required' => 'Please select Start Date.',
            'date_to.required' => 'Please select End Date.',
            'leave_file.min' => 'File attachment is too small.',
            'leave_file.max' => 'File attachment is too large. Please resize it.',
            'leave_file.mimes' => 'File attachment is not allowed.',
            'leave_file.required' => 'File Attachment is required.'
        ];
    } 

}
