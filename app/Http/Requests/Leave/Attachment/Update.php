<?php

namespace IhrV2\Http\Requests\Leave\Attachment;

use IhrV2\Http\Requests\Request;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'leave_file' => 'required|min:1|max:2000|mimes:jpeg,jpg,png,bmp,gif'
        ];
    }

    public function messages()
    {
        return [
            'leave_file.required' => 'Please select File.',
            'leave_file.min' => 'File attachment is too small.',
            'leave_file.max' => 'File attachment is too large. Please resize it.',
            'leave_file.mimes' => 'File attachment is not allowed.'
        ];
    } 
}
