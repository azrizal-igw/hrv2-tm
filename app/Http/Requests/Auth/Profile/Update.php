<?php

namespace IhrV2\Http\Requests\Auth\Profile;

use IhrV2\Http\Requests\Request;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'marital_id' => 'required',
            'correspondence_street_1' => 'required',
            'correspondence_street_2' => 'required',
            'correspondence_postcode' => 'required',
            'correspondence_city' => 'required',
            'correspondence_state' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'marital_id.required' => 'Please select Marital Status.',
            'correspondence_street_1.required' => 'Please insert Street 1.',
            'correspondence_street_2.required' => 'Please insert Street 2.',
            'correspondence_postcode.required' => 'Please insert Postcode.',
            'correspondence_city.required' => 'Please insert City.',
            'correspondence_state.required' => 'Please select State.',
        ];
    } 
}
