<?php

namespace IhrV2\Http\Requests\Mod\Attendance\Manual;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'icno' => 'required|digits:12',            
            'sitecode' => 'required|size:7|string',
            'type_id' => 'required',
            'datetime' => 'required',
            'remark' => 'required',
            'att_file' => 'min:1|max:2000|mimes:jpeg,jpg,png'
        ];
    }

    public function messages()
    {
        return [
            'icno.required' => 'Please insert IC No.',
            'sitecode.required' => 'Please insert Sitecode.',
            'sitecode.size' => 'Sitecode length must equal 7.',
            'type_id.required' => 'Please select Type Punch.',
            'datetime.required' => 'Please select Date Time.',
            'remark.required' => 'Please insert Remark.',
            'att_file.required' => 'File Attachment is required.',
            'att_file.min' => 'File attachment is too small.',
            'att_file.max' => 'File attachment is too large. Please resize it.',
            'att_file.mimes' => 'File attachment is not allowed.',
        ];
    } 
}
