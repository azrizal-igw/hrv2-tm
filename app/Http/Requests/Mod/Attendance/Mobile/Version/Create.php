<?php

namespace IhrV2\Http\Requests\Mod\Attendance\Mobile\Version;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'version' => 'required',
            'build_no' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'version.required' => 'Version is required.',
            'build_no.required' => 'Build No. is required.'
        ];
    } 
}
