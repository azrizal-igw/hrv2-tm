<?php

namespace IhrV2\Http\Requests\Mod\Attendance\Remark;

use IhrV2\Http\Requests\Request;

class Multiple extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function response(array $errors){
        return \Redirect::back()->withErrors($errors)->withInput([
            'dates' => session()->get('dates'),
            'att_status' => request()->get('att_status'),
            'att_notes' => request()->get('att_notes')
        ]);
    }

    public function rules()
    {
        return [
            'att_status' => 'required',            
            'att_notes' => 'required',
            'att_file' => 'required|min:1|max:2000|mimes:jpeg,jpg,png'
        ];
    }

    public function messages()
    {
        return [
            'att_status.required' => 'Please select Remark Status.',
            'att_notes.required' => 'Please insert Notes.',
            // 'att_notes.between' => 'Notes length must between 5 to 250 characters.',
            'att_file.required' => 'File Attachment is required.',
            'att_file.min' => 'File attachment is too small.',
            'att_file.max' => 'File attachment is too large. Please resize it.',
            'att_file.mimes' => 'File attachment is not allowed.',
        ];
    } 
}
