<?php

namespace IhrV2\Http\Requests\Mod\Attendance\Remark;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'remark' => 'required|sometimes',            
            'att_status' => 'required|sometimes',            
            // 'att_notes' => 'required|between:5,250',
            'att_notes' => 'required|sometimes',
            'att_file' => 'required|min:1|max:2000|mimes:jpeg,jpg,png|sometimes'
        ];
    }

    public function messages()
    {
        return [
            'att_status.required' => 'Please select Remark Status.',
            'att_notes.required' => 'Please insert Notes.',
            // 'att_notes.between' => 'Notes length must between 5 to 250 characters.',
            'att_file.required' => 'File Attachment is required.',
            'att_file.min' => 'File attachment is too small.',
            'att_file.max' => 'File attachment is too large. Please resize it.',
            'att_file.mimes' => 'File attachment is not allowed.',
        ];
    } 
}
