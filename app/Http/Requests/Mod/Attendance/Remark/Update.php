<?php

namespace IhrV2\Http\Requests\Mod\Attendance\Remark;

use IhrV2\Http\Requests\Request;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'att_status' => 'required',            
            'att_notes' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'att_status.required' => 'Please select Remark Status.',
            'att_notes.required' => 'Please insert Notes.',
        ];
    } 
}
