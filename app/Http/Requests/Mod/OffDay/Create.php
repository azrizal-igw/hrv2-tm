<?php

namespace IhrV2\Http\Requests\Mod\OffDay;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'off_state' => 'required',
            'off_type' => 'required',
            'off_date_start' => 'required',
            'off_date_end' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'off_state.required' => 'Please select State.',
            'off_type.required' => 'Please select Leave Type.',
            'off_date_start.required' => 'Please select Start Date.',
            'off_date_end.required' => 'Please select End Date.',
        ];
    } 
}
