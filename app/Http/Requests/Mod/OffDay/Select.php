<?php

namespace IhrV2\Http\Requests\Mod\OffDay;

use IhrV2\Http\Requests\Request;

class Select extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'off_year' => 'required',
            'off_group' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'off_year.required' => 'Please select Year.',
            'off_group.required' => 'Please select Group.',
        ];
    } 
}
