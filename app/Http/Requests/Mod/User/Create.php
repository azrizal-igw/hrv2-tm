<?php

namespace IhrV2\Http\Requests\Mod\User;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() 
    {
        return true;
    }

    public function rules() {
        return [
            'name'     => 'required',
            'icno'     => 'required|unique:users|digits:12',
            'email'    => 'required|email',
            'hpno'     => 'required|numeric',
            'staff_id' => 'required',
            'position_id' => 'required',
            // 'region_id' => 'required',
        ];
    }

    public function messages() {
        return [
            'name.required'     => 'Please insert Name.',
            'icno.required'     => 'Please insert IC No.',
            'icno.digits'       => 'The NRIC must be 12 digits.',
            'email.required'    => 'Please insert Email Address.',
            'hpno.required'     => 'Please insert Mobile No.',
            'staff_id.required' => 'Please insert Staff ID.',
            'position_id.required' => 'Please select Position.',
            'region_id.required' => 'Please select Region.',
        ];
    }
}
