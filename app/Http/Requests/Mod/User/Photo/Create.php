<?php

namespace IhrV2\Http\Requests\Mod\User\Photo;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'photo_file' => 'required|min:1|max:100|mimes:jpeg,jpg,png'

        ];
    }

    public function messages()
    {
        return [
            'photo_file.min' => 'Photo is too small.',
            'photo_file.max' => 'Photo is too large. Please resize it.',
            'photo_file.mimes' => 'Photo type is not allowed.',
            'photo_file.required' => 'Photo is required.'    
        ];
    } 
}
