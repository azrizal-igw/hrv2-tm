<?php

namespace IhrV2\Http\Requests\Mod\User\Photo;

use IhrV2\Http\Requests\Request;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'remark' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_photo.required' => 'Please insert remark.',       
        ];
    } 
}
