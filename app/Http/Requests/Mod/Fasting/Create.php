<?php

namespace IhrV2\Http\Requests\Mod\Fasting;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'year' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'state' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'year.required' => 'Please select Year',
            'start_date.required' => 'Please select Start Date',
            'end_date.required' => 'Please select End Date',
            'start_time.required' => 'Please select Start Time',
            'end_time.required' => 'Please select End Time',
            'state.required' => 'Please select State',
        ];
    } 
}
