<?php

namespace IhrV2\Http\Requests\Mod\Email\Notification;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'type_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please insert Name.',
            'email.required' => 'Please insert Email.',
            'type_id.required' => 'Please select Email Type.',
        ];
    } 
}
