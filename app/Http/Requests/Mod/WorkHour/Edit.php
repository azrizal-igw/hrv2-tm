<?php

namespace IhrV2\Http\Requests\Mod\WorkHour;

use IhrV2\Http\Requests\Request;

class Edit extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required',
            'end_date' => 'required',
            'time_in' => 'required',
            'time_out' => 'required',
            'reason' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'start_date.required' => 'Please select Start Date.',
            'end_date.required' => 'Please select End Date.',
            'time_in.required' => 'Please select Time In.',
            'time_out.required' => 'Please select Time Out.',
            'reason.required' => 'Please insert Reason.',        
        ];
    } 
}
