<?php

namespace IhrV2\Http\Requests\Mod\WorkHour;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_id' => 'required',
            'time_in' => 'required',
            'time_out' => 'required',
            'day' => 'required',
            'reason' => 'required',
            'file' => 'required|min:1|max:2000|mimes:jpeg,jpg,png',
        ];
    }

    public function messages()
    {
        return [
            'contract_id.required' => 'Please select Contract.',
            'time_in.required' => 'Please select Time In.',
            'time_out.required' => 'Please select Time Out.',
            'day.required' => 'Please select Off Day.',
            'reason.required' => 'Please insert Reason.',
            'file.min' => 'File attachment is too small.',
            'file.max' => 'File attachment is too large. Please resize it.',
            'file.mimes' => 'File attachment is not allowed.',
            'file.required' => 'File Attachment is required.'            
        ];
    } 
}
