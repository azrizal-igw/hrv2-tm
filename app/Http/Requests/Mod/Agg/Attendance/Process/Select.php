<?php

namespace IhrV2\Http\Requests\Mod\Agg\Attendance\Process;

use IhrV2\Http\Requests\Request;

class Select extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'type_id' => 'required',            
            'by_id' => 'required',            
        ];
    }


    public function messages()
    {
        return [
            'type_id.required' => 'Please select Type.',
            'by_id.required' => 'Please select By Process.',
        ];
    } 
}
