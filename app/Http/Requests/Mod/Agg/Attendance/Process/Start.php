<?php

namespace IhrV2\Http\Requests\Mod\Agg\Attendance\Process;

use IhrV2\Http\Requests\Request;

class Start extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'start_date' => 'required',            
            'end_date' => 'required',            
        ];
    }

    public function messages()
    {
        return [
            'start_date.required' => 'Please select Start Date.',
            'end_date.required' => 'Please select End Date.',
        ];
    } 
}
