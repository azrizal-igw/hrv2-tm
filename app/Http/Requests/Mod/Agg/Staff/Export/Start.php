<?php

namespace IhrV2\Http\Requests\Mod\Agg\Staff\Export;

use IhrV2\Http\Requests\Request;

class Start extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'status_id' => 'required',            
        ];
    }

    public function messages()
    {
        return [
            'status_id.required' => 'Please select Status.',
        ];
    } 
}
