<?php

namespace IhrV2\Http\Requests\Mod\Agg\Staff\Manual;

use IhrV2\Http\Requests\Request;

class Send extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chk_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'chk_id.required' => 'Please select record.',
        ];
    } 


}
