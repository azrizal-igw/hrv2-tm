<?php

namespace IhrV2\Http\Requests\Mod\Agg\Staff\Manual;

use IhrV2\Http\Requests\Request;

class Search extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'icno' => 'required|digits:12',
        ];
    }




    public function messages()
    {
        return [
            'icno.required' => 'Please insert IC No.',
            'icno.digits' => 'IC No. length must equal to 12.',
        ];
    } 
}
