<?php

namespace IhrV2\Http\Requests\Mod\Leave\ReimburseDeduct;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type_id' => 'required',
            'leave_type_id' => 'required',
            'total' => 'required',
            'reason' => 'required',
            'leave_file' => 'required|min:1|max:2000|mimes:jpeg,jpg,png,bmp,gif'
        ];
    }

    public function messages()
    {
        return [
            'type_id.required' => 'Please select Type',
            'leave_type_id.required' => 'Please select Leave Type.',
            'total.required' => 'Please insert Total Day.',
            'reason.required' => 'Please insert Reason.',
            'leave_file.required' => 'Please select Attachment.',
            'leave_file.min' => 'File attachment is too small.',
            'leave_file.max' => 'File attachment is too large. Please resize it.',
            'leave_file.mimes' => 'File attachment is not allowed.'            
        ];
    } 
}
