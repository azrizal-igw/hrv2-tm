<?php

namespace IhrV2\Http\Requests\Mod\Leave\Backdated;

use IhrV2\Http\Requests\Request;

class Select extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function response(array $errors){
        return \Redirect::back()->withErrors($errors)->with([
            'message' => 'Please select Contract.',
            'label' => 'alert alert-danger alert-dismissible'
        ]);
    }

    public function rules()
    {
        return [
            'chk_contract' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'chk_contract.required' => 'Please select Contract.',
        ];
    } 
}
