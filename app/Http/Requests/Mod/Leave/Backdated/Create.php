<?php

namespace IhrV2\Http\Requests\Mod\Leave\Backdated;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'leave_type_id' => 'required',
            'date_from' => 'required',
            'no_day' => 'required|integer',
            'desc' => 'required',
            'leave_file' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'leave_type_id.required' => 'Please select Leave Type.',
            'date_from.required' => 'Please select Start Date.',
            'no_day.required' => 'Please insert Total Day.',
            'no_day.integer' => 'Total Day must number.',
            'desc.required' => 'Please insert Description.',
            'leave_file.required' => 'File Attachment is required.',
            'leave_file.min' => 'File attachment is too small.',
            'leave_file.max' => 'File attachment is too large. Please resize it.',
            'leave_file.mimes' => 'File attachment is not allowed.',
        ];
    } 
}
