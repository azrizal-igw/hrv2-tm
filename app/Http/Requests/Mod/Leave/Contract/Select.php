<?php

namespace IhrV2\Http\Requests\Mod\Leave\Contract;

use IhrV2\Http\Requests\Request;

class Select extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'contract_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'contract_id.required' => 'Please select Contract.',        
        ];
    } 
}
