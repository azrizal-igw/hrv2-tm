<?php

namespace IhrV2\Http\Requests\Mod\Leave\RL;

use IhrV2\Http\Requests\Request;

class Remark extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'remark' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'remark.required' => 'Please insert Remark.',
        ];
    } 
}
