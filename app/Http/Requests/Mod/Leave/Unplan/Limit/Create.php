<?php

namespace IhrV2\Http\Requests\Mod\Leave\Unplan\Limit;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'total' => 'required',
            'reason' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'total.required' => 'Please select Total.',
            'reason.required' => 'Please insert Reason.',           
        ];
    } 
}
