<?php

namespace IhrV2\Http\Requests\Mod\Training;

use IhrV2\Http\Requests\Request;

class Select extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chk_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'chk_id.required' => 'Please select Staff.',       
        ];
    } 
}
