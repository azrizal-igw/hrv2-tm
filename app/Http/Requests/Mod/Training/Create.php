<?php

namespace IhrV2\Http\Requests\Mod\Training;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'location' => 'required',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please insert Name.',     
            'date_from.required' => 'Please select Start Date.',     
            'date_to.required' => 'Please select End Date.',     
            'location.required' => 'Please insert Location.',        
            'description.required' => 'Please insert Description.',     
        ];
    } 
}
