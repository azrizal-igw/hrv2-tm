<?php

namespace IhrV2\Http\Requests\Mod\Letter;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'letter_type_id' => 'required',
            'reminder_no' => 'required',
            'letter_date' => 'required',
            'notes' => 'required',
            'letter_file' => 'required|min:1|max:2000|mimes:pdf,doc,docx',
        ];
    }

    public function messages()
    {
        return [
            'letter_type_id.required' => 'Please select Letter Type',
            'reminder_no.required' => 'Please select Reminder No.',
            'letter_date.required' => 'Please select Letter Date.',
            'notes.required' => 'Please insert Notes.',
            'letter_file.required' => 'Please select Attachment.',
            'letter_file.min' => 'File attachment is too small.',
            'letter_file.max' => 'File attachment is too large. Please resize it.',
            'letter_file.mimes' => 'File attachment is not allowed.',
            'letter_file.required' => 'File Attachment is required.'            
        ];
    } 
}
