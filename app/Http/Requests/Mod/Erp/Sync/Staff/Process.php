<?php

namespace IhrV2\Http\Requests\Mod\Erp\Sync\Staff;

use IhrV2\Http\Requests\Request;

class Process extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'type_id.required' => 'Please select Process.',     
        ];
    } 
}
