<?php

namespace IhrV2\Http\Requests\Mod\PublicHoliday;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'date' => 'required',
            'desc' => 'required',
            'state_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please select Date.',
            'desc.required' => 'Please insert Description.',
            'state_id.required' => 'Please select State.',
        ];
    } 
}
