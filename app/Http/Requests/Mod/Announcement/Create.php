<?php

namespace IhrV2\Http\Requests\Mod\Announcement;

use IhrV2\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type_id' => 'required',            
            'message' => 'required',
            'order' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'type_id.required' => 'Please select Type.',
            'message.required' => 'Please insert Message.',
            'order.required' => 'Please insert Order.',
        ];
    } 
}
