<?php

/*
|--------------------------------------------------------------------------
| API Update Record
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api/update/user'], function()
{
	Route::get('/job', [
		'as'    => 'api.update.user.job',		
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiUpdateController@getApiUpdateUserJob'
	]);
	Route::get('/contract', [
		'as'    => 'api.update.user.contract',		
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiUpdateController@getApiUpdateUserContract'
	]); 	 
});

