<?php

/*
|--------------------------------------------------------------------------
| API Leave (Approve/Reject by Email)
|--------------------------------------------------------------------------
*/ 

Route::group(['prefix' => 'api/leave'], function()
{
	Route::get('/approve/{url}', [
		'as'    => 'api.leave.approve',		
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiLeaveController@getLeaveApprove'
	]); 
	Route::get('/reject/{url}', [
		'as'    => 'api.leave.reject',
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiLeaveController@getLeaveReject'
	]); 	
});

/*
|--------------------------------------------------------------------------
| API RL Request (Approve/Reject by Email)
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api/rl-request'], function()
{
	Route::get('/approve/{url}', [
		'as'    => 'api.rl-request.approve',		
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiLeaveController@getLeaveRepApprove'
	]); 
	Route::get('/reject/{url}', [
		'as'    => 'api.rl-request.reject',
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiLeaveController@getLeaveRepReject'
	]); 	
});


