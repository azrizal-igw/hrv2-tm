<?php

// need api_token at last url

/*
|--------------------------------------------------------------------------
| Attendance 
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/mobile/attendance/version'], function () {
    Route::get('/', array(
        'uses' => 'IhrV2\Http\Controllers\Api\Attendance\ApiAttendanceController@getApiAttVersion'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Api\Attendance\ApiAttendanceController@postApiAttVersion'
    ));    
});


