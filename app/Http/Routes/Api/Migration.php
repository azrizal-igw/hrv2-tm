<?php

/*
|--------------------------------------------------------------------------
| API Migration
|--------------------------------------------------------------------------
*/ 

Route::group(['prefix' => 'api/migration'], function()
{
	Route::get('/staff_personal_info/update/sid', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateUserID',
	]);	
	Route::get('/staff_site_location/update/location_id', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateLocationID',
	]);	
	// replacement leave
	Route::get('/leave_rep_application/insert', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateLeaveRepApplication',
	]);	
	Route::get('/leave_rep_approved/insert', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateLeaveRepApproved',
	]);	
	// reimburse / deduct
	Route::get('/sleave_add/insert/{year}', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateLeaveAdd',
	]);	
	// leave
	Route::get('/sleave_detail/insert/{year}', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateLeaveDetail',
	]);	
	Route::get('/sleave_approved/insert/{year}', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateLeaveApproved',
	]);	
	// update 
	Route::get('/history/insert/{year}', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateUpdateStatus',
	]);	
	Route::get('/sleave_approved/update/rl', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateUpdateRL',
	]);
	Route::get('/attendance/insert/{date}', [
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiMigrationController@getMigrateRemainAttendance',
	]);
});


