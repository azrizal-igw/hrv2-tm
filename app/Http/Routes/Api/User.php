<?php

/*
|--------------------------------------------------------------------------
| API User Photo (Approve/Reject by Email)
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api/user/photo'], function()
{
	Route::get('/approve/{url}', [
		'as'    => 'api.user.photo.approve',		
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiUserController@getUserPhotoApprove'
	]); 
	Route::get('/reject/{url}', [
		'as'    => 'api.user.photo.reject',
	    'uses' => 'IhrV2\Http\Controllers\Api\ApiUserController@getUserPhotoReject'
	]); 	
});

