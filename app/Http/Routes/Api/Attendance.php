<?php

// need api_token at last url

/*
|--------------------------------------------------------------------------
| Attendance Summary
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/attendance/summary'], function () {
    Route::get('/by/date/{date}', array(
        'uses' => 'IhrV2\Http\Controllers\Api\ApiAttendanceController@getChkAttSummary'
    ));
});


