<?php

/*
|--------------------------------------------------------------------------
| API ERP
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/sync'], function () {

    // sync staff
    // ----------
    Route::get('/staff/by/active/{record}/{skip}', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncStaffActive',
    ]);
    Route::get('/staff/by/contract/{record}/{skip}', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncStaffContract',
    ]);
    Route::get('/staff/by/other/{group}', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncStaffOther',
    ]);

    // sync site
    // ---------
    Route::get('/site', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncSite',
    ]);
    Route::get('/phase', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncPhase',
    ]);
    Route::get('/position', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncPosition',
    ]);

    // sync state|region|public holiday
    // --------------------------------
    Route::get('/state', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncState',
    ]);
    Route::get('/region', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncRegion',
    ]);
    Route::get('/public-holiday/{year?}', [
        'uses' => 'IhrV2\Http\Controllers\Api\ApiSyncController@getSyncPublicHoliday',
    ]);
});
