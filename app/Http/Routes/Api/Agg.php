<?php

// need api_token at last url

/*
|--------------------------------------------------------------------------
| API Callback
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/mcmc/callback'], function () {
    Route::get('/token', array(
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getTokenID'
    ));
    Route::get('/staffdetail/all', array(
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getCallStaffAll'
    ));
    Route::get('/staffattendance/all', array(
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getCallAttAll'
    ));
});

/*
|--------------------------------------------------------------------------
| API Process
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/mcmc/process'], function () {

    // staff attendance
    // ----------------
    Route::get('/staffattendance/by/date/{date}', array(
        'as'   => 'api.agg.att.process.date',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getProAttDate'
    ));
    Route::get('/staffattendance/by/sitecode/start/end/{sitecode}/{start}/{end}', array(
        'as'   => 'api.agg.att.process.sitecode.start.end',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getProAttSiteDate'
    ));
    Route::get('/staffattendance/by/start/end/{start}/{end}', array(
        'as'   => 'api.agg.att.process.start.end',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getProAttStartEnd'
    ));
});

/*
|--------------------------------------------------------------------------
| API Export
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/mcmc/export'], function () {

    // export staff attendance
    // -----------------------
    Route::get('/staffattendance/by/date/{date}', array(
        'as'   => 'api.agg.att.export.date',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getExpAttDate'
    ));
    Route::get('/staffattendance/by/sitecode/start/end/{sitecode}/{start}/{end}', array(
        'as'   => 'api.agg.att.export.sitecode.start.end',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getExpAttSiteDate'
    ));
    Route::get('/staffattendance/by/start/end/{start}/{end}', array(
        'as'   => 'api.agg.att.export.start.end',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getExpAttStartEnd'
    ));

    // export staff
    // ------------
    Route::get('/staffdetail/by/status/start/end/{status}/{start}/{end}', array(
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getExpStaff'
    ));

    // export site
    // -----------
    Route::get('/sitedetail', array(
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getExpSite'
    ));
});

/*
|--------------------------------------------------------------------------
| API Reset
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/mcmc/reset'], function () {

    // staff attendance
    // ----------------
    Route::get('/staffattendance/by/date/{date}', array(
        'as'   => 'agg.att.reset.date',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getResetAttDate'
    ));
    Route::get('/staffattendance/by/start/end/{start}/{end}', array(
        'as'   => 'agg.att.reset.start.end',
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getResetAttStartEnd'
    ));
});

/*
|--------------------------------------------------------------------------
| API Check
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'api/mcmc/check'], function () {

    // daily check attendance fail transfer
    // ------------------------------------
    Route::get('/staffattendance/fail/by/start/end/{start}/{end}', array(
        'uses' => 'IhrV2\Http\Controllers\Api\ApiMcmcController@getChkAttFail'
    ));

});
