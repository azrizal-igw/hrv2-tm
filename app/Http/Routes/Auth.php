<?php

// view profile
// ------------
Route::get('/auth/profile', [
    'as' => 'auth.profile', 
    'uses' => 'HomeController@showProfile'
]); 

// edit profile
// ------------
Route::get('/auth/profile/edit', [
    'as' => 'auth.profile.edit', 
    'uses' => 'HomeController@editProfile'
]); 

// update profile
// --------------
Route::post('/auth/profile/edit', [
    'uses' => 'HomeController@postProfile'
]); 

// edit photo
// ----------
Route::get('/auth/photo', [
    'as' => 'auth.photo', 
    'uses' => 'HomeController@showPhoto'
]);
Route::post('/auth/photo', [
    'uses' => 'HomeController@postPhoto'
]);

// change password
// ---------------
Route::get('/auth/password', [
    'as' => 'auth.password', 
    'uses' => 'HomeController@editPassword'
]); 	
Route::post('auth/password', [
    'uses' => 'HomeController@updatePassword'
]);	

