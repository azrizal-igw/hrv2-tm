<?php

/*
|--------------------------------------------------------------------------
| Modul Attendance
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin
| 2 = HR
| 4 = RM
| 5 = Reporting
| 6 = DRM
| 7 = Clerk RM
| 8 = Developer
 */

Route::group(['prefix' => 'mod/attendance', 'middleware' => 'role:1,2,4,5,6,7,8'], function () 
{
    // lists of sites
    // --------------
    Route::get('/', array(
        'as'   => 'mod.attendance.site',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttSite'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttSite'
    ));

    // daily user attendance
    // ---------------------
    Route::get('/user/{icno}/{sitecode}', array(
        'as'   => 'mod.attendance.user',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttUser'
    ));

    // search user attendance
    // ----------------------
    Route::post('/user/{icno}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttUser'
    ));

    // view user attendance
    // --------------------
    Route::get('/user/view/{uid}/{icno}/{sitecode}/{date}', array(
        'as'   => 'mod.attendance.user.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttUserView'
    ));

    // create/approve/cancel/reject/edit remark
    // ----------------------------------------
    Route::post('/user/view/{uid}/{icno}/{sitecode}/{date}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttUserView'
    ));

    // view attendance punch in/out 
    // ----------------------------
    Route::get('/user/view/punch/{type}/{uid}/{icno}/{sitecode}/{date}', array(
        'as'   => 'mod.attendance.user.view.punch',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttUserViewPunch'
    ));

    // edit remark attendance
    // ----------------------
    Route::get('/user/edit/{uid}/{icno}/{sitecode}/{date}', array(
        'as'   => 'mod.attendance.user.edit',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttUserEdit'
    ));

    // update remark attendance
    // ------------------------
    Route::post('/user/edit/{uid}/{icno}/{sitecode}/{date}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttUserEdit'
    ));    

    /*
    |-------------------------------------------------------
    | Attendance Remark
    |-------------------------------------------------------
     */

    Route::group(['prefix' => 'remark', 'middleware' => 'role:1,2,4,5,6,7'], function () 
    {
        // lists attendance remark
        // -----------------------
        Route::get('/', array(
            'as'   => 'mod.attendance.remark',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttRemark'
        ));

        // search attendance remark
        // ------------------------
        Route::post('/', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttRemark'
        ));

        // view attendance remark
        // ----------------------
        Route::get('/view/{id}/{uid}/{icno}/{sitecode}', array(
            'middleware' => 'role:1,2,4,5,6,7',
            'as'         => 'mod.attendance.remark.view',
            'uses'       => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttRemarkView'
        ));

        // approve/cancel/edit/reject attendance remark
        // --------------------------------------------
        Route::post('/view/{id}/{uid}/{icno}/{sitecode}', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttRemarkView'
        ));

        // edit attendance remark
        // ----------------------
        Route::get('/edit/{id}/{uid}/{icno}/{sitecode}', array(
            'middleware' => 'role:1,2,4,6,7',
            'as'         => 'mod.attendance.remark.edit',
            'uses'       => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttRemarkEdit'
        ));

        // update attendance remark
        // ------------------------
        Route::post('/edit/{id}/{uid}/{icno}/{sitecode}', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttRemarkEdit'
        ));

        // set multiple remark
        // -------------------
        Route::get('/set/{icno}/{sitecode}', array(
            'as'   => 'mod.attendance.remark.set',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttRemarkSet'
        ));

        // save multiple remark
        // --------------------
        Route::post('/set/{icno}/{sitecode}', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttRemarkSet'
        ));                       
    });

    /*
    |-------------------------------------------------------
    | Attendance Manual
    |-------------------------------------------------------
     */

    Route::group(['prefix' => 'manual', 'middleware' => 'role:1,2,4,5,7'], function () 
    {
        // lists attendance manual
        // -----------------------
        Route::get('/', array(
            'as'   => 'mod.attendance.manual',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttManual'
        ));

        // search attendance manual
        // ------------------------
        Route::post('/', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttManual'
        ));

        // view attendance manual
        // ----------------------
        Route::get('/view/{id}/{icno}/{sitecode}', array(
            'as'   => 'mod.attendance.manual.view',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttManualView'
        ));

        // cancel attendance manual
        // ------------------------
        Route::post('/view/{id}/{icno}/{sitecode}', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttManualView'
        ));

        // create attendance manual
        // ------------------------
        Route::get('/create', array(
            'as'   => 'mod.attendance.manual.create',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@showAttManualCreate'
        ));

        // save attendance manual
        // ----------------------
        Route::post('/create', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttManualCreate'
        ));
    });

    /*
    |-------------------------------------------------------
    | Attendance Time Slip
    |-------------------------------------------------------
     */

    Route::group(['prefix' => 'timeslip'], function () 
    {
        // display all attendance timeslip
        // -------------------------------
        Route::get('/', array(
            'as'   => 'mod.attendance.timeslip',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@getTimeSlipIndex'
        ));

        // search attendance timeslip
        // --------------------------
        Route::post('/', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postTimeSlipIndex'
        ));

        // view attendance timeslip
        // ------------------------
        Route::get('/view/{id}/{uid}/{sitecode}', array(
            'as'   => 'mod.attendance.timeslip.view',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@getTimeSlipView'
        ));

        // cancel attendance timeslip
        // --------------------------
        Route::post('/view/{id}/{uid}/{sitecode}', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postTimeSlipView'
        ));                  
    });

    /*
    |-------------------------------------------------------
    | Attendance Mobile
    |-------------------------------------------------------
     */

    Route::group(['prefix' => 'mobile', 'middleware' => 'role:1,2,4,5,7,8'], function () 
    {
        // display all attendance mobile
        // -----------------------------
        Route::get('/', array(
            'as'   => 'mod.attendance.mobile',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@getAttMobileIndex'
        ));

        // search attendance mobile
        // ------------------------
        Route::post('/', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttMobileIndex'
        ));   

        // view attendance mobile
        // ----------------------
        Route::get('/view/{id}/{uid}/{sitecode}', array(
            'as'   => 'mod.attendance.mobile.view',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@getAttMobileView'
        ));

        // cancel mobile attendance
        // ------------------------
        Route::post('/view/{id}/{uid}/{sitecode}', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAttMobileView'
        ));

    });

    /*
    |-------------------------------------------------------
    | Attendance Mobile Version
    |-------------------------------------------------------
     */

    Route::group(['prefix' => 'mobile/version', 'middleware' => 'role:1,2,8'], function () 
    {
        // lists version
        // -------------
        Route::get('/', array(
            'as'   => 'mod.attendance.mobile.version',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@getAppVersion'
        ));

        // save version
        // ------------
        Route::post('/', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@postAppVersion'
        ));

        // edit version
        // ------------
        Route::get('/edit/{id}', array(
            'as'   => 'mod.attendance.mobile.version.edit',
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@editAppVersion'
        )); 

        // update version
        // --------------
        Route::post('/edit/{id}', array(
            'uses' => 'IhrV2\Http\Controllers\Mod\ModAttendanceController@updateAppVersion'
        ));               
    });


});
