<?php

/*
|--------------------------------------------------------------------------
| Modul Letter 
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin 
| 2 = HR 
| 4 = RM 
| 5 = Reporting 
| 6 = DRM 
| 7 = Clerk RM
*/  




// staff letter
// ------------
Route::group(['prefix' => 'mod/letter', 'middleware' => 'role:1,2,4,5,7'], function()
{
	// lists of letter
	// ---------------
	Route::get('/', [
		'as' => 'mod.letter.index',
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModLetterController@showLetterIndex'
	]);	

	// search letter
	// -------------
	Route::post('/', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModLetterController@postLetterIndex'
	]);	


	// lists of user letter
	// --------------------
	Route::get('/user/{uid}/{sitecode}', [
		'as' => 'mod.letter.user.index',
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModLetterController@showLetterUser'
	]);	

	// proses user letter
	// ------------------
	Route::post('/user/{uid}/{sitecode}', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModLetterController@postLetterUser'
	]);	

	// view user letter
	// --------------------
	Route::get('/user/view/{id}/{uid}/{sitecode}', [
		'as' => 'mod.letter.user.view',
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModLetterController@showLetterUserView'
	]);	

	// cancel user letter
	// -------------------
	Route::post('/user/view/{id}/{uid}/{sitecode}', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModLetterController@postLetterUserView'
	]);	


});







