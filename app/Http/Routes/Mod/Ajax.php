<?php

/*
|--------------------------------------------------------------------------
| Ajax Block 
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin 
| 2 = HR 
| 4 = RM 
| 5 = Reporting 
| 6 = DRM 
| 7 = Clerk RM
*/ 

Route::group(['prefix' => 'mod/ajax', 'middleware' => 'role:1,2,4,7'], function()
{
	// block
	// -----
	Route::get('/block/announcement', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModAjaxController@getAjaxModAnnouncement'
	]);	
	Route::get('/block/staff-info', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModAjaxController@getAjaxModStaffInfo'
	]);
	Route::get('/block/leave-info', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModAjaxController@getAjaxModLeaveInfo'
	]);
	Route::get('/block/rl-info', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModAjaxController@getAjaxModRLRequest'
	]);
	Route::get('/block/attendance', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModAjaxController@getAjaxModAttendance'
	]);
	Route::get('/block/calendar', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModAjaxController@getAjaxModCalendar'
	]);


});







