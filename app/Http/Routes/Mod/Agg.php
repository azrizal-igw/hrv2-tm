<?php

/*
|--------------------------------------------------------------------------
| Modul Agg
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin
| 2 = HR
| 4 = RM
| 5 = Reporting
| 6 = DRM
| 7 = Clerk RM
 */

// staff attendance
// ----------------
Route::group(['prefix' => 'mod/agg/attendance', 'middleware' => 'role:1,2'], function () 
{
    // page to select process type
    // ---------------------------
    Route::get('/select', [
        'as'   => 'mod.agg.attendance.select',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getAttSelectType'
    ]);
    Route::post('/select', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postAttSelectType'
    ]);

    // process/export attendance
    // -------------------------
    Route::get('/start', [
        'as'   => 'mod.agg.attendance.start',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getAttTypeStart'
    ]);
    Route::post('/start', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postAttTypeStart'
    ]);

    // check transfer attendance
    // -------------------------
    Route::get('/check', [
        'as'   => 'mod.agg.attendance.check',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getAttCheck'
    ]);
    Route::post('/check', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postAttCheck'
    ]);
    Route::get('/check/view/{id}', [
        'as'   => 'mod.agg.attendance.check.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getAttCheckView'
    ]);
});

// staff detail
// ------------
Route::group(['prefix' => 'mod/agg/staff', 'middleware' => 'role:1,2'], function () 
{
    // export staff detail to aggregator
    // ---------------------------------
    Route::get('/export', [
        'as'   => 'mod.agg.staff.export',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffExport'
    ]);
    Route::post('/export', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postStaffExport'
    ]);

    // check staff detail at mcmc
    // --------------------------
    Route::get('/select', [
        'as'   => 'mod.agg.staff.select',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffSelect'
    ]);

    // lists staff detail at aggregator
    // --------------------------------
    Route::get('/list', [
        'as'   => 'mod.agg.staff.list',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffList'
    ]);
    Route::post('/list', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postStaffList'
    ]);

    // view staff detail at aggregator
    // -------------------------------
    Route::get('/list/view/{id}', [
        'as'   => 'mod.agg.staff.list.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffListView'
    ]);

    // lists of updated manual
    // -----------------------
    Route::get('/manual', [
        'as'   => 'mod.agg.staff.manual.list',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffManualIndex'
    ]);
    Route::post('/manual', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postStaffManualIndex'
    ]);

    // view updated manual info
    // ------------------------
    Route::get('/manual/view/{id}', [
        'as'   => 'mod.agg.staff.manual.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffManualView'
    ]);

    // process to export to agg
    // ------------------------
    Route::post('/manual/view/{id}', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postStaffManualView'
    ]);

    // manual send
    // -----------
    Route::get('/manual/search', [
        'as'   => 'mod.agg.staff.manual.search',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffManualSearch'
    ]);
    Route::post('/manual/search', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postStaffManualSearch'
    ]);
    Route::get('/manual/send', [
        'as'   => 'mod.agg.staff.manual.send',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getStaffManualSend'
    ]);
    Route::post('/manual/send', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postStaffManualSend'
    ]);    
});

// site detail
// -----------
Route::group(['prefix' => 'mod/agg/site', 'middleware' => 'role:1,2'], function () 
{
    // sync site detail
    // ----------------
    Route::get('/sync', [
        'as'   => 'mod.agg.site.sync',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getSiteSync'
    ]);
    Route::post('/sync', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postSiteSync'
    ]);
});


// trigger the transfer process
// ----------------------------
Route::get('mod/agg/transfer', [
    'as'   => 'mod.agg.transfer',
    'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@getAggTransfer'
]);

Route::post('mod/agg/transfer', [
    'uses' => 'IhrV2\Http\Controllers\Mod\ModAggController@postAggTransfer'
]);



