<?php

// training attendance
// -------------------
Route::group(['prefix' => 'mod/training', 'middleware' => 'role:1,2,8'], function () 
{
	// lists all training
	// ------------------
    Route::get('/', [
        'as'   => 'mod.training.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingIndex'
    ]);

    // search training
    // ---------------
    Route::post('/', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@postTrainingIndex'
    ]);

    // view training
    // -------------
    Route::get('/view/{id}', [
        'as'   => 'mod.training.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingIndex'
    ]);

    // view training
    // -------------
    Route::get('/popup/code/{id}', [
        'as'   => 'mod.training.popup.code',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingPopupCode'
    ]);

    // create new training
    // -------------------
    Route::get('/create', [
        'as'   => 'mod.training.create',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingCreate'
    ]);

    // save new training
    // -----------------
    Route::post('/create', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@postTrainingCreate'
    ]);

    // display lists of selected staff by session
    // ------------------------------------------
    Route::get('/create/session', array(
        'as'   => 'mod.training.session',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingSession'
    ));

    // remove selected trainees by session
    // -----------------------------------
    Route::post('/create/remove', [
        'as'   => 'mod.training.remove',        
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@postTrainingRemove'
    ]);

    // popup lists of staff
    // --------------------
    Route::get('/create/popup', [
        'as'   => 'mod.training.popup',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingPopup'
    ]);

    // display lists of staff
    // ----------------------
    Route::post('/create/popup/user', [
        'as'   => 'mod.training.popup.user',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingUser'
    ]);


    // Route::post('/create/select', [
    //     'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@postTrainingCreateUser'
    // ]);








    // edit training
    // -------------
    Route::get('/edit/{id}', [
        'as'   => 'mod.training.edit',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@getTrainingEdit'
    ]);

    // update training
    // ---------------
    Route::post('/edit/{id}', [
        'uses' => 'IhrV2\Http\Controllers\Mod\ModTrainingController@postTrainingEdit'
    ]);





});




