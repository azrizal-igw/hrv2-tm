<?php

/*
|--------------------------------------------------------------------------
| Modul Update
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin
| 2 = HR
| 4 = RM
| 5 = Reporting
| 6 = DRM
| 7 = Clerk RM
 */

Route::group(['prefix' => 'mod/update', 'middleware' => 'role:1,2'], function () {

    // user job
    // --------
    Route::get('/user/job', [
        'as' => 'mod.update.user.job',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getUpdateUserJob',
    ]);

    // to update value att_sys_status of status IHRV2 before this
    // ----------------------------------------------------------
    Route::get('/attendance/status', [
        'as' => 'mod.update.attendance.status',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getUpdateAttStatus',
    ]);

    // to update column remark at table attendance_manual
    // --------------------------------------------------
    Route::get('/attendance/manual', [
        'as' => 'mod.update.attendance.manual',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getUpdateAttManualRemark',
    ]);

    Route::get('/attendance/status/mobile', [
        'as' => 'mod.update.attendance.status.mobile',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getUpdateAttStatusMobile',
    ]); 

    // update user_id at attendance_manual
    // -----------------------------------
    Route::get('/attendance/manual/userid', [
        'as' => 'mod.update.attendance.manual.userid',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getUpdateAttManualUserID',
    ]);

    // delete attendance of cancel attendance manual
    // ---------------------------------------------
    Route::get('/attendance/manual/cancel', [
        'as' => 'mod.update.attendance.manual.cancel',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getDeleteAttManualCancel',
    ]);  

    // update attendance gps sitecode
    // ------------------------------
    Route::get('/attendance/gps/sitecode', [
        'as' => 'mod.update.attendance.gps.sitecode',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getUpdateAttGpsSitecode',
    ]);

    // update attendance gps transfer
    // ------------------------------
    Route::get('/attendance/gps/transfer', [
        'as' => 'mod.update.attendance.gps.transfer',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUpdateController@getUpdateAttGpsTransfer',
    ]);                        
});

