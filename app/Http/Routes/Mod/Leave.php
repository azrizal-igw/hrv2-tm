<?php

/*
|--------------------------------------------------------------------------
| Modul Leave
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin
| 2 = HR
| 4 = RM
| 5 = Reporting
| 6 = DRM
| 7 = Clerk RM
 */

Route::group(['prefix' => 'mod/leave', 'middleware' => 'role:1,2,4,7,5'], function () {

    // print pdf
    // ---------
    Route::get('/print/pdf/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.print.pdf',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getLeavePrintPdf'
    ));

    // all leave
    // ---------
    Route::get('/', array(
        'as'   => 'mod.leave.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveIndex'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveIndex'
    ));

    // reimburse unplan limit
    // ----------------------
    Route::get('/unplan/limit/{uid}/{sitecode}', array(
        'middleware' => 'role:1,2',
        'as'         => 'mod.leave.unplan.limit',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getReimUnplanLimit'
    ));

    Route::post('/unplan/limit/{uid}/{sitecode}', array(
        'middleware' => 'role:1,2',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postReimUnplanLimit'
    ));

    // view reimburse unplan limit
    // ---------------------------
    Route::get('/unplan/limit/view/{id}/{uid}/{sitecode}', array(
        'middleware' => 'role:1,2',
        'as'         => 'mod.leave.unplan.limit.view',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getReimUnplanLimitView'
    ));

    // cancel reimburse unplan limit
    // -----------------------------
    Route::post('/unplan/limit/view{id}/{uid}/{sitecode}', array(
        'middleware' => 'role:1,2',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postReimUnplanLimitView'
    ));

    // view leave
    // ----------
    Route::get('/view/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveView'
    ));
    Route::post('/view/{id}/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveView'
    ));

    // edit attachment
    // ---------------
    Route::get('/attachment/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.attachment',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveAttachmentEdit'
    ));
    Route::post('/attachment/{id}/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveAttachmentEdit'
    ));

    // leave pending
    // -------------
    Route::get('/pending', array(
        'as'   => 'mod.leave.pending.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeavePending'
    ));
    Route::post('/pending', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeavePending'
    ));

    // user lists
    // ----------
    Route::get('/user', array(
        'as'   => 'mod.leave.user.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getLeaveUserIndex'
    ));
    Route::post('/user', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveUserIndex'
    ));

    // leave summary
    // -------------
    Route::get('/user/summary/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.summary',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getLeaveUserSummary'
    ));
    Route::post('/user/summary/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveUserSummary'
    ));

    // leave list from summary
    // -----------------------
    Route::get('/list/{uid}/{sitecode}/{leave_type_id}/{type}/{contract_id}', array(
        'as'   => 'mod.leave.user.list',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getLeaveUserList'
    ));

    // lists of user leaves
    // --------------------
    Route::get('/user/leave/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.leave',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getLeaveUserLeave'
    ));

    // search lists of user leaves by contract
    // ---------------------------------------
    Route::post('/user/leave/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveUserLeave'
    ));

    // list replacement request
    // ------------------------
    Route::get('/user/rl-request/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.replacement',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getLeaveUserRepIndex'
    ));

    // search previous replacement request
    // -----------------------------------
    Route::post('/user/rl-request/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveUserRepIndex'
    ));

    // apply backdated leave
    // ---------------------
    Route::get('user/select/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.select',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveUserSelect'
    ));

    Route::post('user/select/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveUserSelect'
    ));

    Route::get('user/create/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.create',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveUserCreate'
    ));

    Route::post('user/create/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveUserCreate'
    ));

    // list reimburse/deduct leave
    // ---------------------------
    Route::get('/user/reimburse-deduct/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.reimburse.deduct',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getLeaveReimburseDeduct'
    ));

    Route::post('/user/reimburse-deduct/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveReimburseDeduct'
    ));

    // view reimburse/deduct leave
    // ---------------------------
    Route::get('/user/reimburse-deduct/view/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.reimburse.deduct.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getReimburseDeductView'
    ));

    // leave of inactive user
    // ----------------------
    Route::get('/inactive/summary/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.inactive.summary',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getInactiveSummary'
    ));
    Route::get('/inactive/leave/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.inactive.leave',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getInactiveLeave'
    ));
    Route::get('/inactive/leave/view/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.inactive.leave.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getInactiveLeaveView'
    ));
    Route::get('/inactive/leave/list/{uid}/{sitecode}/{leave_type_id}/{type}', array(
        'as'   => 'mod.leave.inactive.leave.list',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getInactiveLeaveList'
    ));
    Route::get('/inactive/rl-request/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.inactive.rl-request',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getInactiveRL'
    ));
    Route::get('/inactive/rl-request/view/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.inactive.rl-request.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@getInactiveRLView'
    ));

});

/*
|--------------------------------------------------------------------------
| Modul RL Request
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'mod/leave/rl-request', 'middleware' => 'role:1,2,4,7,5'], function () {

    // lists of RL request
    // -------------------
    Route::get('/', array(
        'as'   => 'mod.leave.replacement.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveRepIndex'
    ));

    // search RL request
    // -----------------
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveRepIndex'
    ));

    // view RL request
    // ---------------
    Route::get('/view/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.replacement.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveRepView'
    ));

    // view popup RL request
    // ---------------------
    Route::get('/view/popup/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.replacement.view.popup',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveRepViewPopup'
    ));

    // approve/reject RL request
    // -------------------------
    Route::post('/view/{id}/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveRepView'
    ));

    // edit attachment RL request
    // --------------------------
    Route::get('/attachment/edit/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.replacement.attachment.edit',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showRepLeaveAttachmentEdit'
    ));

    // upload attachment RL request
    // ----------------------------
    Route::post('/attachment/edit/{id}/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postRepLeaveAttachmentEdit'
    ));

    // lists pending RL request
    // ------------------------
    Route::get('/pending', array(
        'as'   => 'mod.leave.replacement.pending',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@showLeaveRepPending'
    ));

    // search pending RL request
    // -------------------------
    Route::post('/pending', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModLeaveController@postLeaveRepPending'
    ));
});
