<?php

Route::group(['prefix' => 'mod/report', 'middleware' => 'role:1,2'], function()
{
	Route::get('/staff', [
		'as' => 'mod.report.staff',
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModReportController@getReportStaff'
	]);	
	Route::post('/staff', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModReportController@postReportStaff'
	]);			
});
