<?php

/*
|--------------------------------------------------------------------------
| Modul User
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin
| 2 = HR
| 4 = RM
| 5 = Reporting
| 6 = DRM
| 7 = Clerk RM
 */

Route::group(['prefix' => 'mod/user', 'middleware' => 'role:1,2,4,5,7,10'], function () {

    // lists all user
    // --------------
    Route::get('/', array(
        'as'   => 'mod.user.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserIndex'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserIndex'
    ));

    // select group
    // ------------
    Route::get('/select', array(
        'middleware' => 'role:1,2',
        'as'         => 'mod.user.select',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserSelect'
    ));
    Route::post('/select', array(
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserSelect'
    ));

    // create user
    // -----------
    Route::get('/create', array(
        'middleware' => 'role:1,2',
        'as'         => 'mod.user.create',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserCreate'
    ));

    // save new user
    // -------------
    Route::post('/create', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserCreate'
    ));

    // user password
    // -------------
    Route::get('/password/{id}/{token}', array(
        'middleware' => 'role:1,2,4,5',
        'as'         => 'mod.user.password',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPassword'
    ));
    Route::post('/password/{id}/{token}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@updateUserPassword'
    ));

    // user photo
    // ----------
    Route::get('/photo', array(
        'as'   => 'mod.user.photo',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPhoto'
    ));
    Route::post('/photo', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserPhoto'
    ));

    // view photo
    // ----------
    Route::get('/photo/view/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.user.photo.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPhotoDetail'
    ));
    Route::post('/photo/view/{id}/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserPhotoDetail'
    ));

    Route::get('/photo/remark', array(
        'as'   => 'mod.user.photo.remark',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPhotoRemark'
    ));

    // display other user
    // ------------------
    Route::get('/other', array(
        'middleware' => 'role:1,2,5',
        'as'         => 'mod.user.other',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserOther'
    ));
    Route::post('/other', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserOther'
    ));

    // reset imei device
    // -----------------
    Route::get('/reset/{id}/{token}', array(
        'middleware' => 'role:1,2',
        'as'         => 'mod.user.reset',
        'uses'       => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserResetImei'
    ));
    Route::post('/reset/{id}/{token}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserResetImei'
    ));

});

/*
|--------------------------------------------------------------------------
| Modul User Tabs
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'mod/user/view', 'middleware' => 'role:1,2,4,5,7,10'], function () {

    // user view
    // ---------
    Route::get('/{id}/{token}', array(
        'as'   => 'mod.user.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserView'
    ));
    Route::post('/{id}/{token}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserView'
    ));

    // personal tab
    // ------------
    Route::get('/personal/{id}/{token}', array(
        'as'   => 'mod.user.view.personal',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserViewPersonal'
    ));

    // job tab
    // -------
    Route::get('/job/{id}/{token}', array(
        'as'   => 'mod.user.view.job',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserViewJob'
    ));

    // contract tab
    // ------------
    Route::get('/contract/{id}/{token}', array(
        'as'   => 'mod.user.view.contract',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserViewContract'
    ));

    Route::get('/contract/prev/api/{id}/{token}', array(
        'as'   => 'mod.user.contract.prev.api',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPrevContract'
    ));

    // family tab
    // ----------
    Route::get('/family/{id}/{token}', array(
        'as'   => 'mod.user.view.family',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserViewFamily'
    )); 

    // family lists by json
    // --------------------
    Route::get('/family/json/{id}/{token}', array(
        'as'   => 'mod.user.view.family.json',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserFamilyJson'
    ));    

    // education tab
    // -------------
    Route::get('/education/{id}/{token}', array(
        'as'   => 'mod.user.view.education',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserViewEducation'
    ));

    // emergency tab
    // -------------
    Route::get('/emergency/{id}/{token}', array(
        'as'   => 'mod.user.view.emergency',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserViewEmergency'
    ));

    // photo tab
    // ---------
    Route::get('/photo/{id}/{token}', array(
        'as'   => 'mod.user.view.photo',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserViewPhoto'
    ));

    // upload photo at photo tab
    // -------------------------
    Route::post('/photo/{id}/{token}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserViewPhoto'
    ));

    // popup letter
    // ------------
    Route::get('/popup/letter/{id}', array(
        'as'   => 'mod.user.view.popup.letter',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPopupLetter'
    ));

    // popup job
    // ---------
    Route::get('/popup/job/{id}/{uid}/{token}', array(
        'as'   => 'mod.user.view.popup.job',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPopupJob'
    ));
    Route::post('/popup/job/{id}/{uid}/{token}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserPopupJob'
    ));

    // popup contract
    // --------------
    Route::get('/popup/contract/{id}/{uid}/{token}', array(
        'as'   => 'mod.user.view.popup.contract',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@showUserPopupContract'
    ));

    Route::post('/popup/contract/{id}/{uid}/{token}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserPopupContract'
    ));

    // Route::post('/popup/contract/{id}/{uid}/{token}', array(
    //     'uses' => 'IhrV2\Http\Controllers\Mod\ModUserController@postUserPopupContract'
    // ));

});

/*
|--------------------------------------------------------------------------
| Modul User No Partners 
|--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'mod/user/no-partner', 'middleware' => 'role:1,2'], function () {

    // view no partners
    // ----------------
    Route::get('/view/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.partner.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showWorkHourUser'
    ));
    Route::post('/view/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postWorkHourUser'
    ));

});

