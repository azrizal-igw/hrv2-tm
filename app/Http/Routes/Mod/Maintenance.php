<?php

/*
|--------------------------------------------------------------------------
| Modul Maintenance
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin
| 2 = HR
| 4 = RM
| 5 = Reporting
| 6 = DRM
| 7 = Clerk RM
 */

/*
|--------------------------------------------------------------------------
| Module Site
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'mod/site', 'middleware' => 'role:1,2'], function () {
    Route::get('/', array(
        'as'   => 'mod.site.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showSiteIndex'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postSiteIndex'
    ));

    // view site
    // ---------
    Route::get('/view/{id}', array(
        'as'   => 'mod.site.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showSiteView'
    ));

    // cancel site
    // -----------
    Route::post('/view/{id}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postSiteView'
    ));  
});

/*
|--------------------------------------------------------------------------
| Modul Public Holiday
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'mod/public-holiday', 'middleware' => 'role:1,2,4,5,7'], function () {
    Route::get('/', array(
        'as'   => 'mod.public-holiday.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showPublicHoliday'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postPublicHoliday'
    ));
    Route::get('/view/{id}', array(
        'as'   => 'mod.public-holiday.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showPublicHolidayView'
    ));

    // cancel public holiday
    // ---------------------
    Route::post('/view/{id}', array(
        'middleware' => 'role:1,2',        
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postPublicHolidayView'
    ));     

    // manual public holiday
    // ---------------------    
    Route::get('/create', array(
        'as'   => 'mod.public-holiday.create',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showPublicHolidayAdd'
    ));
    Route::post('/create', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postPublicHolidayAdd'
    ));
    Route::get('/edit/{id}', array(
        'middleware' => 'role:1,2',        
        'as'   => 'mod.public-holiday.edit',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showPublicHolidayEdit'
    ));
    Route::post('/edit/{id}', array(
        'middleware' => 'role:1,2',        
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postPublicHolidayEdit'
    ));  
});

/*
|--------------------------------------------------------------------------
| Modul Off Days
|--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'mod/off-day', 'middleware' => 'role:1,2,4,5,7'], function () {

    // display off day
    // ---------------
    Route::get('/', array(
        'as'   => 'mod.off-day.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showOffDayIndex'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postOffDayIndex'
    ));

    // view off day
    // ------------
    Route::get('/view/{id}', array(
        'as'   => 'mod.off-day.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showOffDayView'
    ));

    // cancel off day
    // --------------
    Route::post('/view/{id}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postOffDayView'
    ));

    // add off day
    // -----------
    Route::get('/select', array(
        'middleware' => 'role:1,2',                
        'as'   => 'mod.off-day.select',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showOffDaySelect'
    ));
    Route::post('/select', array(
        'middleware' => 'role:1,2',                
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postOffDaySelect'
    ));
    Route::get('/create', array(
        'middleware' => 'role:1,2',                
        'as'   => 'mod.off-day.create',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showOffDayAdd'
    ));
    Route::post('/create', array(
        'middleware' => 'role:1,2',                
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postOffDayAdd'
    ));

    // edit off day
    // ------------
    Route::get('/edit/{id}', array(
        'middleware' => 'role:1,2',                
        'as'   => 'mod.off-day.edit',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showOffDayEdit'
    ));

    Route::post('/edit/{id}', array(
        'middleware' => 'role:1,2',                
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postOffDayEdit'
    ));
});

/*
|--------------------------------------------------------------------------
| Modul Calendar
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'mod/calendar', 'middleware' => 'role:1,2,4,5,7'], function () {

    // display calendar
    // ----------------
    Route::get('/', array(
        'as'   => 'mod.calendar.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showCalendarIndex'
    ));
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postCalendarIndex'
    ));

    // view date calendar
    // ------------------
    Route::get('/view/{group}/{date}', array(
        'as'   => 'mod.calendar.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showCalendarView'
    ));
    Route::post('/view/{group}/{date}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postCalendarView'
    ));
});

/*
|--------------------------------------------------------------------------
| Modul Fasting
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'mod/fasting', 'middleware' => 'role:1,2,4,5,7'], function () {

    // list fasting
    // ------------
    Route::get('/', array(
        'as'   => 'mod.fasting.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showFastingIndex'
    ));

    // search fasting
    // --------------
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postFastingIndex'
    ));

    // view fasting
    // ------------
    Route::get('/view/{id}', array(
        'as'   => 'mod.fasting.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showFastingView'
    ));

    // cancel fasting
    // --------------
    Route::post('/view/{id}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postFastingView'
    ));

    // add fasting
    // -----------
    Route::get('/create', array(
        'as'   => 'mod.fasting.create',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showFastingCreate'
    ));
    Route::post('/create', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postFastingCreate'
    ));
});

/*
|--------------------------------------------------------------------------
| Module Announcement
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'mod/announcement', 'middleware' => 'role:1'], function () {

    // list announcement
    // -----------------
    Route::get('/', array(
        'as'   => 'mod.announcement.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showAnnoIndex'
    ));

    // search announcement
    // -------------------
    Route::post('/', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postAnnoIndex'
    ));

    // view announcement
    // -----------------
    Route::get('/view/{id}', array(
        'as'   => 'mod.announcement.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showAnnoView'
    ));

    // cancel announcement
    // -------------------
    Route::post('/view/{id}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postAnnoView'
    ));

    // create announcement
    // -------------------
    Route::get('/create', array(
        'as'   => 'mod.announcement.create',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showAnnoCreate'
    ));
    Route::post('/create', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postAnnoCreate'
    ));

});

/*
|--------------------------------------------------------------------------
| Modul Download
|--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'mod/download', 'middleware' => 'role:1,2'], function () {

    // download page
    // -------------
    Route::get('/', array(
        'as'   => 'mod.download.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showDownload'
    ));
});

/*
|--------------------------------------------------------------------------
| Modul Change 
|--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'mod/change', 'middleware' => 'role:1,2'], function () {

    // lists work hour
    // ---------------
    Route::get('/work-hour', array(
        'as'   => 'mod.work-hour.index',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showWorkHourIndex'
    ));
    Route::post('/work-hour', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postWorkHourIndex'
    ));
    Route::get('/work-hour/view/{id}', array(
        'as'   => 'mod.work-hour.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showWorkHourView'
    ));
    Route::get('/work-hour/edit/{id}', array(
        'as'   => 'mod.work-hour.edit',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showWorkHourEdit'
    ));
    Route::post('/work-hour/edit/{id}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postWorkHourEdit'
    ));        
});

/*
|--------------------------------------------------------------------------
| Modul Leave 
|--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'mod/leave/user', 'middleware' => 'role:1,2'], function () {

    // view & set work hour
    // --------------------
    Route::get('/work-hour/view/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.work-hour.view',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showWorkHourUser'
    ));
    Route::post('/work-hour/view/{uid}/{sitecode}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postWorkHourUser'
    ));

    // detail work hour
    // ----------------
    Route::get('/work-hour/view/popup/{id}/{uid}/{sitecode}', array(
        'as'   => 'mod.leave.user.work-hour.view.popup',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showWorkHourUserPopup'
    ));
});


/*
|--------------------------------------------------------------------------
| Modul Email Notification 
|--------------------------------------------------------------------------
 */
Route::group(['prefix' => 'mod/email', 'middleware' => 'role:1,2'], function () {

    // lists of email notification
    // ---------------------------
    Route::get('/notification', array(
        'as'   => 'mod.email.notification',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showEmailNotification'
    ));
    Route::post('/notification', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postEmailNotification'
    ));

    // create new email notification
    // -----------------------------
    Route::get('/notification/create', array(
        'as'   => 'mod.email.notification.create',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showEmailNotiCreate'
    ));
    Route::post('/notification/create', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postEmailNotiCreate'
    ));

    // edit email notification
    // -----------------------
    Route::get('/notification/edit/{id}', array(
        'as'   => 'mod.email.notification.edit',
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@showEmailNotiEdit'
    ));
    Route::post('/notification/edit/{id}', array(
        'uses' => 'IhrV2\Http\Controllers\Mod\ModMaintenanceController@postEmailNotiEdit'
    ));

});

