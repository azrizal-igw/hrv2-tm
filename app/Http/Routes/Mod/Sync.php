<?php

/*
|--------------------------------------------------------------------------
| Modul Erp 
|--------------------------------------------------------------------------
| Group ID
| 1 = Admin 
| 2 = HR 
| 4 = RM 
| 5 = Reporting 
| 6 = DRM 
| 7 = Clerk RM
*/  

Route::group(['prefix' => 'mod/sync', 'middleware' => 'role:1,2'], function()
{
	// sync staff/contract
	// -------------------
	Route::get('/staff', [
		'as' => 'mod.sync.staff',
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModSyncController@getSyncStaff'
	]);	
	Route::post('/staff', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModSyncController@postSyncStaff'
	]);

	// sync site
	// ---------
	Route::get('/site', [
		'as' => 'mod.sync.site',
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModSyncController@getSyncSite'
	]);	
	Route::post('/site', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModSyncController@postSyncSite'
	]);

	// sync public holiday
	// -------------------
	Route::get('/public-holiday', [
		'as' => 'mod.sync.public-holiday',
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModSyncController@getSyncPublicHoliday'
	]);	
	Route::post('/public-holiday', [
		'uses'   => 'IhrV2\Http\Controllers\Mod\ModSyncController@postSyncPublicHoliday'
	]);		
});
