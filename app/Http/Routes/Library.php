<?php

Route::group(['prefix' => 'lib'], function () 
{
    // display general image
    // ---------------------
    Route::get('/images/{slug}', [
        'as'   => 'lib.image',
        'uses' => 'LibraryController@showImage'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Leave
    |--------------------------------------------------------------------------
     */

    // display leave attachment
    // ------------------------
    Route::get('/files/leave/{slug}', array(
        'as'   => 'lib.file.leave',
        'uses' => 'LibraryController@showLeaveAttachment'
    ));

    // display leave attachment thumbnail
    // ----------------------------------
    Route::get('/files/leave/thumb/{slug}', array(
        'as'   => 'lib.file.leave.thumb',
        'uses' => 'LibraryController@showLeaveAttachmentThumb'
    ));

    /*
    |--------------------------------------------------------------------------
    | Attendance Remark
    |--------------------------------------------------------------------------
     */

    // display attendance remark
    // -------------------------
    Route::get('/files/attendance/remark/{slug}', array(
        'as'   => 'lib.file.attendance.remark',
        'uses' => 'LibraryController@showAttRemAttach'
    ));

    // display attendance remark thumbnail
    // -----------------------------------
    Route::get('/files/attendance/remark/thumb/{slug}', array(
        'as'   => 'lib.file.attendance.remark.thumb',
        'uses' => 'LibraryController@showAttRemAttachThumb'
    ));

    /*
    |--------------------------------------------------------------------------
    | Attendance Manual
    |--------------------------------------------------------------------------
     */

    // display attendance manual
    // -------------------------
    Route::get('/files/attendance/manual/{slug}', array(
        'as'   => 'lib.file.attendance.manual',
        'uses' => 'LibraryController@showAttManualAttach'
    ));

    // display attendance manual thumbnail
    // -----------------------------------
    Route::get('/files/attendance/manual/thumb/{slug}', array(
        'as'   => 'lib.file.attendance.manual.thumb',
        'uses' => 'LibraryController@showAttManualAttachThumb'
    ));

    /*
    |--------------------------------------------------------------------------
    | Staff Photo
    |--------------------------------------------------------------------------
     */

    // display user photo
    // ------------------
    Route::get('/files/user/{slug}', array(
        'as'   => 'lib.file.user',
        'uses' => 'LibraryController@showUserPhoto'
    ));

    // display user photo thumbnail
    // ----------------------------
    Route::get('/files/user/thumb/{slug}', array(
        'as'   => 'lib.file.user.thumb',
        'uses' => 'LibraryController@showUserPhotoThumb'
    ));

    /*
    |--------------------------------------------------------------------------
    | Letter
    |--------------------------------------------------------------------------
     */

    // display letter attachment
    // -------------------------
    Route::get('/files/letter/{slug}', array(
        'as'   => 'lib.file.letter',
        'uses' => 'LibraryController@showLetterAttachment'
    ));

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
     */

    // display misc attachment
    // -----------------------
    Route::get('/files/misc/{slug}', array(
        'as'   => 'lib.file.misc',
        'uses' => 'LibraryController@showMiscAttachment'
    ));

    // display misc attachment thumbnail
    // ---------------------------------
    Route::get('/files/misc/thumb/{slug}', array(
        'as'   => 'lib.file.misc.thumb',
        'uses' => 'LibraryController@showMiscAttachmentThumb'
    ));

    /*
    |--------------------------------------------------------------------------
    | Training
    |--------------------------------------------------------------------------
     */

    // display training attachment
    // ---------------------------
    Route::get('/files/training/{slug}', array(
        'as'   => 'lib.file.training',
        'uses' => 'LibraryController@showTrainingAttachment'
    ));

    // display training attachment thumbnail
    // -------------------------------------
    Route::get('/files/training/thumb/{slug}', array(
        'as'   => 'lib.file.training.thumb',
        'uses' => 'LibraryController@showTrainingAttachmentThumb'
    ));

    /*
    |--------------------------------------------------------------------------
    | Attendance Mobile Log
    |--------------------------------------------------------------------------
     */

    // display log file
    // ----------------
    Route::get('/files/attendance/log/{slug}', array(
        'as'   => 'lib.file.att.log',
        'uses' => 'LibraryController@showAttGpsLog'
    ));

});
