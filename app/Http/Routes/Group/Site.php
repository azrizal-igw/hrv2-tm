<?php


/*
|--------------------------------------------------------------------------
| Module Leave 
|--------------------------------------------------------------------------
*/ 


Route::group(['prefix' => 'leave'], function()
{
	// add leave   
	// ---------      
	Route::get('/select', array(
		'as'    => 'sv.leave.select',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveSelect',
	));   
	Route::post('/select', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postLeaveSelect',
	));    
	Route::get('/create', array(
		'as'    => 'sv.leave.create',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveCreate',
	));   
	Route::post('/create', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@storeLeaveCreate',
	));  

	Route::get('/', array(
		'as'    => 'sv.leave.index',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveIndex',
	)); 

	Route::post('/', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postLeaveIndex',
	));    

	Route::get('/view/{id}', array(
		'as'    => 'sv.leave.view',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveView',
	));  

	Route::post('/view/{id}', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postLeaveView',
	)); 

	Route::get('/summary', array(
		'as'    => 'sv.leave.summary',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveSummary',
	)); 

	// modal dialog on leave summary
	// -----------------------------
	Route::get('/list/{leave_type_id}/{type}', array(
		'as'    => 'sv.leave.list',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveList',
	)); 

	// calendar
	// --------
	Route::get('/calendar', array(
		'as'    => 'sv.leave.calendar',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showCalendar',
	));	
	Route::post('/calendar', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postCalendar',
	));
	Route::get('/calendar/{date}', array(
		'as'    => 'sv.leave.calendar.date',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showCalendarView',
	));			

	// edit attachment
	// ---------------
	Route::get('/attachment/{id}', array(
		'as'    => 'sv.leave.attachment.edit',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveAttachment',
	)); 
	Route::post('/attachment/{id}', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postLeaveAttachment',
	)); 	

});

/*
|--------------------------------------------------------------------------
| Module RL Request 
|--------------------------------------------------------------------------
*/ 

Route::group(['prefix' => 'leave/rl-request'], function()
{
	Route::get('/', array(
		'as'    => 'sv.leave.replacement.index',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveRepIndex',
	));          
	Route::post('/', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postLeaveRepIndex',
	));      
	Route::get('/create', array(
		'as'    => 'sv.leave.replacement.create',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveRepCreate',
	));   
	Route::post('/create', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@storeLeaveRepCreate',
	));               
	Route::get('/view/{id}', array(
		'as'    => 'sv.leave.replacement.view',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showLeaveRepView',
	));   
	Route::post('/view/{id}', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postLeaveRepView',
	));  

	// edit attachment
	// ---------------
	Route::get('/attachment/{id}', array(
		'as'    => 'sv.leave.replacement.attachment.edit',
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@showRLAttachment',
	)); 
	Route::post('/attachment/{id}', array(
		'uses'   => 'IhrV2\Http\Controllers\LeaveController@postRLAttachment',
	)); 
});

/*
|--------------------------------------------------------------------------
| Module Attendance 
|--------------------------------------------------------------------------
*/ 
Route::group(['prefix' => 'attendance'], function()
{
	// daily attendance
	// ----------------
	Route::get('/daily', array(
		'as'    => 'sv.attendance.daily',
		'uses'   => 'IhrV2\Http\Controllers\AttendanceController@showAttDaily',
	));

	Route::get('/daily/map/{id}', array(
		'as'    => 'sv.attendance.daily.map',
		'uses'   => 'IhrV2\Http\Controllers\AttendanceController@showAttDailyMap',
	));

	// search attendance
	// -----------------
	Route::post('/daily', array(
		'uses'   => 'IhrV2\Http\Controllers\AttendanceController@postAttDaily',
	));

	// view attendance
	// ---------------
	Route::get('/daily/view/{date}', array(
		'as'    => 'sv.attendance.daily.view',
		'uses'   => 'IhrV2\Http\Controllers\AttendanceController@showDailyView',
	));

	// submit new remark/time slip
	// ---------------------------
	Route::post('/daily/view/{date}', array(
		'uses'   => 'IhrV2\Http\Controllers\AttendanceController@postDailyView',
	));	

	// set attendance remark (one/multiple)
	// ------------------------------------
	Route::get('/daily/set/remark', array(
		'as'    => 'sv.attendance.daily.set',
		'uses'   => 'IhrV2\Http\Controllers\AttendanceController@showDailyChecked',
	));
	Route::post('/daily/set/remark', array(
		'uses'   => 'IhrV2\Http\Controllers\AttendanceController@postDailyChecked',
	));
									
});


/*
|--------------------------------------------------------------------------
| Module Letter 
|--------------------------------------------------------------------------
*/ 
Route::group(['prefix' => 'letter'], function()
{
	Route::get('/', array(
		'as'    => 'sv.letter.index',
		'uses'   => 'IhrV2\Http\Controllers\LetterController@showLetterIndex',
	));
	Route::get('/view/{id}', array(
		'as'    => 'sv.letter.view',
		'uses'   => 'IhrV2\Http\Controllers\LetterController@showLetterView',
	));
	Route::get('/popup/{id}', array(
		'as'    => 'sv.letter.popup',
		'uses'   => 'IhrV2\Http\Controllers\LetterController@showLetterPopup',
	));						
});





