<?php

// ajax route
// ----------
Route::group(['prefix' => 'ajax'], function()
{
    Route::get('/block/announcement', [
        'uses'   => 'AjaxController@getAjaxAnnouncement',
    ]);     
    Route::get('/block/staff-info', [
        'uses'   => 'AjaxController@getAjaxStaffApplication',
    ]); 
    Route::get('/block/leave-info', [
        'uses'   => 'AjaxController@getAjaxLeaveApplication',
    ]);
    Route::get('/block/rl-info', [
        'uses'   => 'AjaxController@getAjaxRLRequest',
    ]);
    Route::get('/block/attendance', [
        'uses'   => 'AjaxController@getAjaxAttendance',
    ]);
    Route::get('/block/calendar', [
        'uses'   => 'AjaxController@getAjaxCalendar',
    ]);  
});



