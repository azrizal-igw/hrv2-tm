<?php

/*
|--------------------------------------------------------------------------
| API Mobile
|--------------------------------------------------------------------------
 */


Route::post('/api/mobile/login', 'Api\Auth\ApiAuthController@getApiLogin');

Route::group(['prefix' => 'api/mobile', 'middleware' => 'jwt.auth'], function () 
{
	// authentication
	// --------------
    Route::post('/user', 'Api\Auth\ApiAuthController@getApiUser');
    Route::post('/token', 'Api\Auth\ApiAuthController@getApiToken');
    Route::post('/logout', 'Api\Auth\ApiAuthController@getApiLogout');
    Route::post('/device', 'Api\Auth\ApiAuthController@getApiDevice');

    // attendance
    // ----------
    Route::post('/attendance/create', 'Api\Attendance\ApiAttendanceController@getApiAttCreate');
    Route::post('/attendance/check', 'Api\Attendance\ApiAttendanceController@getApiAttCheck');

    // attendance sync
    // ---------------
    Route::post('/attendance/sync', 'Api\Attendance\ApiAttendanceController@getApiAttSync');    

    // attendance file upload (receive log file from apps)
    // ---------------------------------------------------
    Route::post('/attendance/log/upload', 'Api\Attendance\ApiAttendanceController@getApiAttLogUpload');    

    // attendance check sitecode
    // -------------------------
    Route::post('/attendance/check/sitecode', 'Api\Attendance\ApiAttendanceController@getApiChkSitecode');    

    // training
    // --------
    Route::post('/training/attendance/create', 'Api\Training\ApiTrainingController@getApiTtgAttCreate');
    Route::post('/training/attendance/active', 'Api\Training\ApiTrainingController@getApiTtgAttActive');
    
});


