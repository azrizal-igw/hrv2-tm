<?php

use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// initial page
Route::get('/', 'Auth\AuthController@showLoginForm');

// authentication page
Route::auth();

// view home
Route::get('/home', [
    'as' => 'home',
    'uses' => 'HomeController@showHome',
]);

Route::get('message', [
    'as' => 'message',
    'uses' => 'HomeController@showMessage',
]);

Route::get('info/decompose','\Lubusin\Decomposer\Controllers\DecomposerController@index');

Route::get('info/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

