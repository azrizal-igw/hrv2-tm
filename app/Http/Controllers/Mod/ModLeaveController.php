<?php namespace IhrV2\Http\Controllers\Mod;

use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;
use PDF;

class ModLeaveController extends Controller {
    protected $leave_repo;
    protected $user_repo;

    public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo) {
        $this->middleware('auth');
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
    }

    // all leave
    // ---------
    public function showLeaveIndex() {
        $index = $this->leave_repo->getLeaveIndex();
        return $index;
    }

    // search leave
    // ------------
    public function postLeaveIndex(Request $request) {
        $index = $this->leave_repo->getLeaveIndex($request);
        return $index;
    }

    // manage user leave index
    // -----------------------
    public function getLeaveUserIndex() {
        $index = $this->leave_repo->getLeaveUserIndex();
        return $index;
    }

    // search manage user leave
    // ------------------------
    public function postLeaveUserIndex(Request $request) {
        $index = $this->leave_repo->getLeaveUserIndex($request);
        return $index;
    }

    // lists RL Request
    // ----------------
    public function showLeaveRepIndex() {
        $index = $this->leave_repo->getLeaveRepIndex();
        return $index;
    }

    // search replacement leave
    // ------------------------
    public function postLeaveRepIndex(Request $request) {
        $index = $this->leave_repo->getLeaveRepIndex($request);
        return $index;
    }

    // display all leaves of inactive staff
    // ------------------------------------
    public function getInactiveLeave($uid, $sitecode) 
    {
        // get user info
        // -------------
        $user = $this->user_repo->getUserContractInactive($uid, $sitecode);
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Manage',
            'child-a' => route('mod.leave.user.index'),
            'sub'     => 'Staff Leave',
            'sub-a'   => route('mod.leave.inactive.leave', array($uid, $sitecode)),
            'icon'    => 'grid',
            'title'   => $user->name
        );
        $status = 0;

        // user exist
        // ----------
        if (!empty($user)) {
            $status = 1;

            // set array
            // ---------
            $arr = array($uid, $sitecode, $user->UserLastContract->date_from, $user->UserLastContract->date_to);

            // select distinct leave type
            // --------------------------
            $types = $this->leave_repo->getLeaveTypeDistinct($arr);

            // query leave application
            // -----------------------
            $leaves = $this->leave_repo->getLeaveListInactive($arr);
        }
        return View('modules.leave.inactive.leave', compact('header', 'status', 'user', 'leaves', 'types'));
    }

    // popup list of leaves (inactive staff)
    // -------------------------------------
    public function getInactiveLeaveList($uid, $sitecode, $leave_type_id, $type) {
    }

    public function getInactiveLeaveView() {
    }

    // display all rl request of inactive staff
    // ----------------------------------------
    public function getInactiveRL($uid, $sitecode) {
        $user = $this->user_repo->getUserContractInactive($uid, $sitecode);
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Manage',
            'child-a' => route('mod.leave.user.index'),
            'sub'     => 'RL Request',
            'sub-a'   => route('mod.leave.inactive.rl-request', array($uid, $sitecode)),
            'icon'    => 'list',
            'title'   => $user->name
        );
        $status = 0;

        // user exist
        // ----------
        if (!empty($user)) {
            $status = 1;

            // query replacement leave
            // -----------------------
            $arr = array($uid, $sitecode, $user->UserLastContract->date_from, $user->UserLastContract->date_to);
            $leaves = $this->leave_repo->getLeaveRepInactive($arr);
        }
        return View('modules.leave.inactive.rl', compact('header', 'user', 'leaves', 'status'));
    }

    public function getInactiveRLView() {
    }

    // leave summary of inactive user
    // ------------------------------
    public function getInactiveSummary($uid, $sitecode) {
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Leave',
            'sub'     => 'Summary',
            'sub-a'   => route('mod.leave.inactive.summary', array($uid, $sitecode)),
            'icon'    => 'calculator',
            'title'   => 'Summary'
        );

        // check user & contract
        // ---------------------
        $user = \IhrV2\User::with(array(
            'UserCurrentJob',
            'UserLastContract',
            'SiteNameInfo'
        ))->where('id', $uid)->first();

        // get contract info
        // -----------------
        $contract_id = $user->UserLastContract->id;
        $date_from = $user->UserLastContract->date_from;
        $date_to = $user->UserLastContract->date_to;
        $join_date = $user->UserCurrentJob->join_date;

        // get contract duration
        // ---------------------
        $duration = $this->leave_repo->DateRange($date_from, $date_to);

        // get summary lists
        // -----------------
        $lists = $this->leave_repo->getLeaveSummaryEach($uid, $sitecode, $contract_id, $date_from, $date_to);
        return View('modules.leave.inactive.summary', compact('header', 'user', 'duration', 'lists'));
    }

    // print pdf leave
    // ---------------
    public function getLeavePrintPdf(Request $request, $id, $uid, $sitecode) 
    {
        // get approved unpaid leave
        // -------------------------
        $leave = $this->leave_repo->getCheckLeaveApprove($uid, $id);

        // get total taken unpaid current year
        // -----------------------------------
        $total = $this->leave_repo->getSumUnpaidYear($uid, $sitecode, 12, date('Y'));

        // pass variables at pdf view
        // --------------------------
        $pdf = PDF::loadView('modules.leave.pdf.unpaid', compact('sitecode', 'leave', 'total'));
        $filename = date('YmdHis') . '_' . $uid . '_' . str_random(20) . '.pdf';
        return $pdf->download($filename);
    }

    // reimburse or deduct leave
    // -------------------------
    public function getLeaveReimburseDeduct($uid, $sitecode) 
    {
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Manage',
            'child-a' => route('mod.leave.user.index'),
            'sub'     => 'Reimburse/Deduct Leave',
            'icon'    => 'note',
            'title'   => 'Reimburse/Deduct Leave'
        );

        // get user info
        // -------------
        $user = $this->leave_repo->getCheckUserActive($uid, $sitecode);

        // get contract info
        // -----------------
        $contract = $user['user']['user_latest_contract'];
        $date_from = $contract['date_from'];
        $date_to = $contract['date_to'];

        // get job info
        // ------------
        $job = $user['user']['user_latest_job'];
        $position = $job['position_name']['name'];

        // get site info
        // -------------
        $site = $user['user']['site_name_info'];

        // get rm info
        // -----------
        $chk_rm = $this->user_repo->getRegionBySitecode($sitecode);
        $rm = $chk_rm['region_manager_info']['region_manager_detail']['name'];

        // get leave add
        // -------------
        $arr = array($uid, $sitecode, $date_from, $date_to);
        $ext = $this->leave_repo->getLeaveAddAll($arr);

        // drop down lists for reimburse/deduct
        // ------------------------------------
        $types = $this->leave_repo->getReimburseDeductList();

        // get leave types (except unplan)
        // -------------------------------
        $leave_types = $this->leave_repo->getLeaveTypeNotIn(array(13));

        // lists of day
        // ------------
        $days = array('' => '[Day]') + array('0.5' => 'Half Day', 1 => 1, 2 => 2);

        // get display info
        // ----------------
        $info = array(
            'uid'       => $uid,
            'sitecode'  => $sitecode,
            'name'      => $user['user']['name'],
            'icno'      => $user['user']['icno'],
            'position'  => $position,
            'sitename'  => $site['name'],
            'date_from' => $date_from,
            'date_to'   => $date_to,
            'rm'        => $rm
        );
        return View('modules.leave.user.reimburse-deduct.index', compact('header', 'info', 'ext', 'types', 'leave_types', 'days'));
    }

    // insert reimburse/deduct leave
    // -----------------------------
    public function postLeaveReimburseDeduct(Requests\Mod\Leave\ReimburseDeduct\Create $request, $uid, $sitecode) 
    {
        // process reimburse/deduct
        // ------------------------
        $this->leave_repo->getProcessReimburseDeduct($request->all(), $uid, $sitecode);

        // return message
        // --------------
        $type = ($request->type_id == 1) ? 'Reimburse' : 'Deduct';

        // return result
        // -------------
        return redirect()->route('mod.leave.user.reimburse.deduct', array($uid, $sitecode))->with([
            'message' => $type . ' successfully added.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // lists of user leaves
    // --------------------
    public function getLeaveUserLeave($uid, $sitecode) 
    {
        // get user info
        // -------------
        $user = $this->leave_repo->getCheckUserActive($uid, $sitecode);
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Leave',
            'child-a' => route('mod.leave.index'),
            'sub'     => 'Staff',
            'icon'    => 'grid',
            'title'   => $user['user']['name']
        );

        // get contract info
        // -----------------
        $contract = $user['user']['user_latest_contract'];
        $contract_id = $contract['id'];
        $date_from = $contract['date_from'];
        $date_to = $contract['date_to'];

        // set array
        // ---------
        $arr = array($uid, $sitecode, $date_from, $date_to);

        // select distinct leave type
        // --------------------------
        $types = $this->leave_repo->getLeaveTypeDistinct($arr);

        // query leave application
        // -----------------------
        $leaves = $this->leave_repo->getLeaveListAll($arr);

        // get lists of contracts
        // ----------------------
        $contracts = $this->user_repo->getUserContractDrop($uid, $sitecode);
        return View('modules.leave.user.leave', compact('header', 'status', 'arr', 'leaves', 'types', 'contracts', 'contract_id'));
    }

    // search user leaves by previous contract
    // ---------------------------------------
    public function postLeaveUserLeave(Requests\Mod\Leave\Contract\Select $request, $uid, $sitecode) 
    {
        // get user info
        // -------------
        $user = $this->user_repo->getUserCheckIDSitecode($uid, $sitecode);
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Manage',
            'child-a' => route('mod.leave.user.index'),
            'sub'     => 'Staff Leave',
            'icon'    => 'grid',
            'title'   => $user->name
        );

        // get contract info
        // -----------------
        $contract = $this->user_repo->getUserContractByID($request->contract_id);
        $contract_id = $contract->id;

        // set array
        // ---------
        $arr = array($uid, $sitecode, $request->contract_id);

        // select distinct leave type
        // --------------------------
        $types = $this->leave_repo->getLeaveTypeInactive($arr);

        // query leave application
        // -----------------------
        $leaves = $this->leave_repo->getLeaveAllInactive($arr);

        // get lists of contracts
        // ----------------------
        $contracts = $this->user_repo->getUserContractDrop($uid, $sitecode);
        return View('modules.leave.user.leave', compact('header', 'status', 'arr', 'leaves', 'types', 'contracts', 'contract_id'));
    }

    // display popup list of leaves from summary
    // -----------------------------------------
    public function getLeaveUserList($uid, $sitecode, $leave_type_id, $type, $contract_id) 
    {
        // get contract info
        // -----------------
        $contract = $this->user_repo->getUserContractByUser($uid, $sitecode, $contract_id);
        $date_from = $contract->date_from;
        $date_to = $contract->date_to;

        // if click taken
        // --------------
        if ($type == 1) {
            $name = 'Leave Taken';
            $arr = array($uid, $sitecode, $leave_type_id, $date_from, $date_to);
            $leaves = $this->leave_repo->getLeaveTakenList2($arr)->toArray();
        }

        // click entitled for replacement request
        // --------------------------------------
        elseif ($type == 2) {
            $name = 'Entitled';
            $arr = array($uid, $sitecode, $date_from, $date_to);
            $leaves = $this->leave_repo->getLeaveRepApproveList2($arr)->toArray();
        }

        // click deduct leave
        // ------------------
        elseif ($type == 3) {
            $name = 'Deduct Leave';
            $arr = array($uid, $sitecode, $leave_type_id, 2, $date_from, $date_to);
            $leaves = $this->leave_repo->getLeaveReimburseDeductList($arr)->toArray();
        }

        // click reimburse leave
        // ---------------------
        elseif ($type == 4) {
            $name = 'Reimburse Leave';
            $arr = array($uid, $sitecode, $leave_type_id, 1, $date_from, $date_to);
            $leaves = $this->leave_repo->getLeaveReimburseDeductList($arr)->toArray();
        }
        $leave_type = \IhrV2\Models\LeaveType::find($leave_type_id);
        return View('modules.leave.user.list', compact('uid', 'sitecode', 'leaves', 'leave_type', 'name', 'type'));
    }

    // list all RL request (by user)
    // -----------------------------
    public function getLeaveUserRepIndex($uid, $sitecode) {
        $index = $this->leave_repo->getLeaveUserRepIndex($request = null, $uid, $sitecode);
        return $index;
    }

    // search all RL request by user
    // -----------------------------
    public function postLeaveUserRepIndex(Request $request, $uid, $sitecode) {
        $index = $this->leave_repo->getLeaveUserRepIndex($request, $uid, $sitecode);
        return $index;
    }

    // leave summary user
    // ------------------
    public function getLeaveUserSummary($uid, $sitecode) {
        $index = $this->leave_repo->getLeaveSummaryUser($request = null, $uid, $sitecode);
        return $index;
    }

    // search site attendance
    // ----------------------
    public function postLeaveUserSummary(Requests\Leave\Summary\Select $request, $uid, $sitecode) {
        $index = $this->leave_repo->getLeaveSummaryUser($request, $uid, $sitecode);
        return $index;
    }

    // manage reimburse unplan limit
    // -----------------------------
    public function getReimUnplanLimit($uid, $sitecode) {
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Manage',
            'child-a' => route('mod.leave.user.index'),
            'sub'     => 'Unplan Limit',
            'icon'    => 'speedometer',
            'title'   => 'Unplan Limit'
        );

        // get user info
        // -------------
        $user = $this->leave_repo->getCheckUserActive($uid, $sitecode);

        // get contract info
        // -----------------
        $contract = $user['user']['user_latest_contract'];
        $date_from = $contract['date_from'];
        $date_to = $contract['date_to'];

        // get job info
        // ------------
        $job = $user['user']['user_latest_job'];
        $position = $job['position_name']['name'];

        // get site info
        // -------------
        $site = $user['user']['site_name_info'];

        // get rm info
        // -----------
        $chk_rm = $this->user_repo->getRegionBySitecode($sitecode);
        $rm = $chk_rm['region_manager_info']['region_manager_detail']['name'];

        // get display info
        // ----------------
        $info = array(
            'uid'         => $uid,
            'sitecode'    => $sitecode,
            'name'        => $user['user']['name'],
            'icno'        => $user['user']['icno'],
            'position'    => $position,
            'sitename'    => $site['name'],
            'date_from'   => $date_from,
            'date_to'     => $date_to,
            'rm'          => $rm,
            'contract_id' => $contract['id']
        );

        // get total reimburse unplan limit
        // --------------------------------
        $arr = array($uid, $sitecode, $date_from, $date_to);
        $reimburse = $this->leave_repo->getReimburseUnplanLimit($arr);

        // get lists of total
        // ------------------
        $total = array('' => '[Total]', 1 => '1', 2 => '2');
        return view('modules.leave.user.unplan-limit.index', compact('header', 'info', 'reimburse', 'total'));
    }

    // view reimburse unplan limit
    // ---------------------------
    public function getReimUnplanLimitView($id, $uid, $sitecode) {
    }

    // view reimburse/deduct leave
    // ---------------------------
    public function getReimburseDeductView($id, $uid, $sitecode) 
    {
        $rd = $this->leave_repo->getReimburseDeductDetail($id, $uid, $sitecode);
        $type_name = ($rd->type_id == 1) ? 'Reimburse' : 'Deduct';
        return View('modules.leave.user.reimburse-deduct.view', compact('rd', 'type_name'));
    }

    // update leave attachment
    // -----------------------
    public function postLeaveAttachmentEdit(Requests\Mod\Leave\Attachment\Upload $request, $id, $uid, $sitecode) 
    {
        $save = $this->leave_repo->getUploadLeaveAttachment($request->all(), $id, $uid);
        return redirect()->route('mod.leave.attachment', array($id, $uid, $sitecode))->with([
            'message' => $save[1],
            'label'   => 'alert alert-' . $save[0] . ' alert-dismissible'
        ]);
    }

    // approve (2) / reject (3) RL Request
    // approve (6) / reject (7) apply cancel RL request
    // -------------------------------------------------
    public function postLeaveRepView(Requests\Mod\Leave\RL\Remark $request, $id, $uid, $sitecode) 
    {
        // process request
        // ---------------
        $save = $this->leave_repo->getProcessLeaveRep($request->all(), $id, $uid, $sitecode, auth()->user()->id);

        // return result
        // -------------
        return redirect()->route('mod.leave.replacement.view', array($id, $uid, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-'.$save[1].' alert-dismissible'
        ]);
    }

    // save new reimburse unplan limit
    // -------------------------------
    public function postReimUnplanLimit(Requests\Mod\Leave\Unplan\Limit\Create $request, $uid, $sitecode) 
    {
        // save new reimburse
        // ------------------
        $save = $this->leave_repo->getProReimburseUnplan($request->all(), $uid, $sitecode);

        // return message
        // --------------

        return redirect()->route('mod.leave.unplan.limit', array($uid, $sitecode))->with([
            'message' => 'Reimburse successfully added.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // cancel reimburse unplan limit
    // -----------------------------
    public function postReimUnplanLimitView() 
    {
    
    }

    // save replacement leave attachment
    // ---------------------------------
    public function postRepLeaveAttachmentEdit(Requests\Mod\Leave\RL\Attachment\Upload $request, $id, $uid, $sitecode) 
    {
        $save = $this->leave_repo->getUpdateLeaveRepAttachment($request->all(), $id, $uid);

        return redirect()->route('mod.leave.replacement.view', array($id, $uid, $sitecode))->with([
            'message' => 'Attachment successfully uploaded.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // edit leave attachment
    // ---------------------
    public function showLeaveAttachmentEdit($id, $uid, $sitecode) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Leave',
            'child-a'  => route('mod.leave.index'),
            'sub'   => 'View',
            'sub-a' => route('mod.leave.view', array($id, $uid, $sitecode)),
            'cat1'   => 'Attachment',
            'icon'   => 'paper-clip',
            'title'  => 'Attachment'
        );

        // set user info into array
        // ------------------------
        $arr = array($id, $uid, $sitecode);

        // get current attachment
        // ----------------------
        $detail = $this->leave_repo->getLeaveAttachmentActive($id, $uid);

        // get inactive attachment
        // -----------------------
        $inactive = $this->leave_repo->getLeaveAttachmentInactive($id, $uid);
        return View('modules.leave.attachment', compact('header', 'arr', 'detail', 'inactive'));
    }

    // view RL request
    // ---------------
    public function showLeaveRepView($id, $uid, $sitecode) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'RL Request',
            'child-a'  => route('mod.leave.replacement.index'),
            'sub' => 'Staff',
            'sub-a' => route('mod.leave.user.replacement', array($uid, $sitecode)),
            'cat1'   => 'View',
            'icon'   => 'info',
            'title'  => 'View'
        );
        $detail = $this->leave_repo->getLeaveRepByUser($id, $uid, $sitecode);
        $history = $this->leave_repo->getLeaveRepByHistory($id, $uid, $sitecode, $detail->contract_id);
        $user = array('id' => $id, 'uid' => $uid, 'sitecode' => $sitecode);
        return View('modules.rl-request.view', compact('header', 'detail', 'history', 'user'));
    }

    // view RL request at popup
    // ------------------------
    public function showLeaveRepViewPopup($id, $uid, $sitecode)
    {
        $detail = $this->leave_repo->getLeaveRepByUser($id, $uid, $sitecode);
        return View('modules.rl-request.popup.view', compact('detail'));
    }

    // page to apply backdated leave
    // -----------------------------
    public function showLeaveUserCreate(Request $request, $uid, $sitecode) 
    {
        // select contract
        // ---------------
        if (old('chk_contract')) {

            // save leave type session
            // -----------------------
            session()->put('chk_contract', old('chk_contract'));
            $chk_contract = session()->get('chk_contract');

            // get user info
            // -------------
            $user = $this->leave_repo->getCheckUserActive($uid, $sitecode);
            $header = array(
                'parent'  => 'Leave Application',
                'child'   => 'Manage',
                'child-a' => route('mod.leave.user.index'),
                'sub'     => 'Backdated Leave',
                'icon'    => 'plus',
                'title'   => $user['user']['name']
            );

            // get contract info
            // -----------------
            $contract = $this->user_repo->getUserContractByID($chk_contract);

            // get lists of leave types
            // ------------------------
            $leave_types = $this->leave_repo->getLeaveTypeIn(array(2, 3, 4, 5, 7, 8, 9, 10, 11, 12));

            // get array info
            // --------------
            $user = array('uid' => $uid, 'sitecode' => $sitecode);
            return view('modules.leave.user.create', compact('header', 'contract', 'leave_types', 'user'));
        } 

        // not selected any contract
        // -------------------------
        else {
            return redirect()->route('mod.leave.user.select', array($uid, $sitecode));
        }
    }

    // process backdated leave
    // -----------------------
    public function postLeaveUserCreate(Requests\Mod\Leave\Backdated\Create $request, $uid, $sitecode) 
    {
        // check and process leave
        // -----------------------
        $save = $this->leave_repo->getApplyBackdatedLeave($request->all(), $uid, $sitecode);

        // return message
        // --------------
        return redirect()->route('mod.leave.user.leave', array($uid, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // create new leave
    // ----------------
    public function showLeaveUserSelect($uid, $sitecode) 
    {
        // user must active if want apply leave of backdated contract
        // ----------------------------------------------------------
        $user = $this->user_repo->getUserActive($uid, $sitecode);

        // user is active
        // --------------
        if (!empty($user)) {

            // set header
            // ----------
            $header = array(
                'parent'  => 'Leave Application',
                'child'   => 'Manage',
                'child-a' => route('mod.leave.user.index'),
                'sub'     => 'Backdated Leave',
                'icon'    => 'plus',
                'title'   => $user->name
            );

            // get lists of contract
            // ---------------------
            $contracts = $this->user_repo->getUserContractLists($uid, $sitecode);
            return view('modules.leave.user.select', compact('header', 'contracts'));
        } 
        else {
            return redirect()->route('mod.leave.user.index')->with([
                'message' => 'Staff is Inactive. No need to apply leave.',
                'label'   => 'alert alert-danger alert-dismissible'
            ]);
        }
    }

    // process selected contract
    // -------------------------
    public function postLeaveUserSelect(Requests\Mod\Leave\Backdated\Select $request, $uid, $sitecode) 
    {
        return redirect()->route('mod.leave.user.create', array($uid, $sitecode))->withInput();
    }

    // view leave detail
    // -----------------
    public function showLeaveView($id, $uid, $sitecode) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Leave',
            'child-a'  => route('mod.leave.index'),
            'sub' => 'Staff',
            'sub-a' => route('mod.leave.user.leave', array($uid, $sitecode)),
            'cat1'   => 'View',
            'icon'   => 'info',
            'title'  => 'View'
        );

        // initial values
        // --------------
        $status = 0;
        $entitled = 0;
        $taken = 0;
        $balance = 0;

        // get leave info
        // --------------
        $detail = $this->leave_repo->getLeaveByUser($id, $uid, $sitecode);
        $leave_type_id = $detail->leave_type_id;

        // get total entitled/taken/balance (annual/unplan leave)
        // ------------------------------------------------------
        if (in_array($leave_type_id, array(1,13))) {

            // check if still pending
            // ----------------------
            if ($detail->LeaveLatestHistory->status == 1) {

                // leave type is annual/unplan
                // revert leave_type_id to 1 if unplan 
                // -----------------------------------
                $status = 1;
                $leave_type_id = 1;

                // get user info & contract
                // ------------------------
                $user = $this->user_repo->getUserByIDSitecode($uid, $sitecode);
                $contract = $user->UserLatestContract;

                // get leave entitled
                // ------------------
                $entitled = $this->leave_repo->getEntitledLeave($uid, $sitecode, $leave_type_id, $contract);

                // get leave taken
                // ---------------
                $taken = $this->leave_repo->getTakenLeave($uid, $sitecode, $leave_type_id, $contract);

                // get leave balance
                // -----------------
                $balance = $this->leave_repo->getCalculateBalance($entitled, $taken);
            }
        }

        // set total leave
        // ---------------
        $total = array('status' => $status, 'entitled' => $entitled, 'taken' => $taken, 'balance' => $balance);
        return View('modules.leave.view', compact('header', 'detail', 'total'));
    }

    // approve or cancel leave
    // -----------------------
    public function postLeaveView(Request $request, $id, $uid, $sitecode) 
    {
        // get action by
        // -------------
        $action_by = auth()->user()->id;

        // process leave
        // -------------
        $save = $this->leave_repo->getProcessLeave($request->all(), $id, $uid, $sitecode, $action_by);

        // return output
        // -------------
        return redirect()->route('mod.leave.view', array($id, $uid, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // edit replacement leave attachment
    // ---------------------------------
    public function showRepLeaveAttachmentEdit($id, $uid, $sitecode) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'RL Request',
            'child-a'  => route('mod.leave.replacement.index'),
            'sub' => 'Staff',
            'sub-a' => route('mod.leave.user.replacement', array($uid, $sitecode)),
            'cat1'   => 'View',
            'cat1-a' => route('mod.leave.replacement.view', array($id, $uid, $sitecode)),
            'cat2'   => 'Attachment',
            'icon'   => 'paper-clip',
            'title'  => 'Attachment'
        );

        // get attachment info
        // -------------------
        $detail = $this->leave_repo->getLeaveRepAttachment($id, $uid);

        // get inactive attachment
        // -----------------------
        $inactive = $this->leave_repo->getInactiveRLRepAttachment($id, $uid);
        return View('modules.rl-request.attachment', compact('header', 'detail', 'inactive'));
    }
}



