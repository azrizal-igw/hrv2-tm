<?php
namespace IhrV2\Http\Controllers\Mod;

use IhrV2\Contracts\Agg\AggInterface;
use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\Mcmc\DbMcmcInterface as DbMcmcRepository;
use IhrV2\Contracts\Mcmc\McmcInterface as McmcRepository;
use IhrV2\Contracts\User\DbUserInterface as DbUserRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ModAggController extends Controller {
    protected $mcmc_repo;
    protected $db_mcmc_repo;
    protected $user_repo;
    protected $leave_repo;
    protected $db_user_repo;
    protected $agg_repo;

    public function __construct(McmcRepository $mcmc_repo, DbMcmcRepository $db_mcmc_repo, UserRepository $user_repo, LeaveRepository $leave_repo, DbUserRepository $db_user_repo, AggInterface $agg_repo) {
        $this->mcmc_repo = $mcmc_repo;
        $this->db_mcmc_repo = $db_mcmc_repo;
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->db_user_repo = $db_user_repo;
        $this->agg_repo = $agg_repo;
    }

    // get url transfer attendance
    // ---------------------------
    public function getAttUrlTransfer($type) 
    {
        // get the url
        // -----------
        $url = ($type == 1) ? env('AGG_URL_STAFF') : env('AGG_URL_ATTENDANCE');

        // trigger the url
        // ---------------
        $client = new Client();
        $request = $client->get($url);
        $response = $request->getStatusCode();
        if ($response == 200) {
            $msg = array('Transfer is in Progress.', 'success');
        }
        else {
            $msg = array('Transfer is Failed. Please try again.', 'danger');
        }
        return $msg;
    }

    // get url transfer staff detail
    // -----------------------------
    public function getStaffUrlTransfer() {
        $url_staff = env('AGG_URL_STAFF');
        $client = new Client();
        $request = $client->get($url_staff);
        $response = $request->getStatusCode();
        if ($response == 200) {
            $msg = array('Transfer Staff Detail is in Progress.', 'success');
        }
        else {
            $msg = array('Transfer Staff Detail is Fail.', 'danger');
        }
        return $msg;
    }

    // lists staff detail at mcmc
    // --------------------------
    public function getStaffSelect() {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Staff',
            'sub'    => 'Call API',
            'sub-a'  => route('mod.agg.staff.select'),
            'icon'   => 'screen-desktop',
            'title'  => 'Call API'
        );
        $api = $this->mcmc_repo->getCallBack('usp_im.v_pi1m_cms_staff_details');
        $json = json_decode($api, true);
        return view('modules.agg.staff.select', compact('header', 'json'));
    }

    // lists staff detail at aggregator
    // --------------------------------
    public function getStaffList() {
        $index = $this->agg_repo->getStaffTransfer();
        return $index;
    }

    // search lists staff detail at aggregator
    // ---------------------------------------
    public function postStaffList(Request $request) {
        $index = $this->agg_repo->getStaffTransfer($request);
        return $index;
    }

    // view staff detail at aggregator
    // -------------------------------
    public function getStaffListView($id) {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Staff',
            'sub'    => 'Check Transfer',
            'sub-a'  => route('mod.agg.staff.list'),
            'cat1'   => 'View Detail',
            'icon'   => 'info',
            'title'  => 'View Detail'
        );

        // get staff detail aggregator
        // ---------------------------
        $detail = $this->agg_repo->getAggStaffDetail($id);
        return view('modules.agg.staff.agg.view', compact('header', 'detail'));
    }

    // lists of updated manual
    // -----------------------
    public function getStaffManualIndex() {
        $index = $this->agg_repo->getStaffManual();
        return $index;
    }

    // search lists updated manual
    // ---------------------------
    public function postStaffManualIndex(Request $request) {
        $index = $this->agg_repo->getStaffManual($request);
        return $index;
    }

    // view user manual info
    // ---------------------
    public function getStaffManualView($id) {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Staff Detail',
            'sub'    => 'Lists Manual',
            'sub-a'  => route('mod.agg.staff.manual.list'),
            'cat1'   => 'View Updated Manual',
            'icon'   => 'info',
            'title'  => 'View Updated Manual'
        );

        // get user manual info
        // --------------------
        $detail = $this->user_repo->getUserManualDetail($id);

        // check if already export to agg
        // ------------------------------
        $status = 0;
        if (!empty($detail->Export_Date) && $detail->Export_Status == 1) {
            $status = 1;
        }
        return view('modules.agg.staff.manual.view', compact('header', 'detail', 'status'));
    }

    // process to export data to agg db
    // --------------------------------
    public function postStaffManualView(Request $request, $id) {

        // get user manual info
        // --------------------
        $detail = $this->user_repo->getUserManualDetail($id);

        // export to agg
        // -------------
        $this->db_user_repo->dbInsertAggStaffDetail($detail);

        // update export info
        // ------------------
        $this->db_user_repo->dbUpdateUserManual($id);

        // return message
        // --------------
        return redirect()->back()->with([
            'message' => 'Record successfully Exported! Need process to transfer the Data to MCMC.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // manual send - search icno
    // -------------------------
    public function getStaffManualSearch() {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Staff',
            'sub'    => 'Manual',
            'cat1'   => 'Send',
            'icon'   => 'note',
            'title'  => 'Send'
        );
        return view('modules.agg.staff.manual.search', compact('header'));
    }

    // manual send - search icno direct page
    // -------------------------------------
    public function postStaffManualSearch(Requests\Mod\Agg\Staff\Manual\Search $request) 
    {
        return redirect()->route('mod.agg.staff.manual.send')->withInput();
    }

    // manual send - select which user record
    // --------------------------------------
    public function getStaffManualSend()
    {
        // have keyin icno
        // ---------------
        if (old('icno')) {

            // set session
            // -----------
            session()->put('icno', old('icno'));
            $icno = session()->get('icno');

            // set header
            // ----------
            $header = array(
                'parent' => 'Aggregator',
                'child'  => 'Staff',
                'sub'    => 'Manual',
                'cat1'   => 'Send',
                'icon'   => 'note',
                'title'  => 'Send'
            ); 

            /// check user record
            /// -----------------
            $users = $this->user_repo->getUserByIcnoAll($icno);
            if (count($users) > 0) {
                $positions = array('MANAGER' => 'MANAGER', 'ASSISTANT MANAGER' => 'ASSISTANT MANAGER');
                $status = array('ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE');
                return view('modules.agg.staff.manual.send', compact('header', 'icno', 'users', 'positions', 'status'));
            }
            else {
                return redirect()->route('mod.agg.staff.manual.search')->with([
                    'message' => 'Record not found',
                    'label'   => 'alert alert-danger alert-dismissible'
                ]);
            }                
        }
        else {
            return redirect()->route('mod.agg.staff.manual.search');
        }     
    }

    // manual send - add record to agg db
    // ----------------------------------
    public function postStaffManualSend(Requests\Mod\Agg\Staff\Manual\Send $request)
    {
        // get selected id
        // ---------------
        $id = $request->chk_id;

        // get user info
        // -------------
        $user = $this->user_repo->getUserByID($request->chk_id);

        // set array values
        // ----------------
        $arr = array(
            'PI1M_REFID' => $user->sitecode,
            'SERVICE_PROVIDER' => 'TM',
            'Staff_IC' => $user->icno,
            'Staff_Name' => $user->name,
            'Contact_Number' => $request->hpno[$id],
            'Contact_Email' => $request->email[$id],
            'Position' => $request->position_name[$id],
            'Status' => $request->status_name[$id],
        );

        // send record to agg
        // ------------------
        $this->db_user_repo->dbInsertAggStaffDirect($arr);
        return redirect()->route('mod.agg.staff.manual.search')->with([
            'message' => 'Record successfully Send to Aggregator.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);        
    }

    // export staff detail
    // -------------------
    public function getStaffExport() 
    {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Staff',
            'sub'    => 'Export/Transfer',
            'icon'   => 'action-redo',
            'title'  => 'Export/Transfer'
        );
        $status = array('' => '[Status]', 1 => 'ACTIVE', 2 => 'INACTIVE');
        return view('modules.agg.staff.export', compact('header', 'status'));
    }

    // export staff detail into agg db
    // -------------------------------
    public function postStaffExport(Requests\Mod\Agg\Staff\Export\Start $request) 
    {
        // export staff active
        // -------------------
        if ($request->status_id == 1) {
            $label = 'Active';
            $this->mcmc_repo->getExpStaff(1, $request->start, $request->end);
        }

        // export staff inactive
        // ---------------------
        else {
            $label = 'Inactive';
            $this->mcmc_repo->getExpStaff(2, $request->start, $request->end);
        }

        // transfer to mcmc
        // ----------------
        $msg = $this->getStaffUrlTransfer();

        // return result
        // -------------
        return redirect()->route('mod.agg.staff.export')->with([
            'message' => 'Staff ' . $label . ' successfully Exported and '.$msg[0],
            'label'   => 'alert alert-'.$msg[1].' alert-dismissible'
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Modul Staff Attendance
    |--------------------------------------------------------------------------
     */

    // select process type staff attendance
    // ------------------------------------
    public function getAttSelectType() 
    {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Attendance',
            'sub'    => 'Process/Export/Reset',
            'icon'   => 'wrench',
            'title'  => 'Process/Export/Reset'
        );
        $types = $this->agg_repo->getListTypes();
        $by_lists = $this->agg_repo->getListProcessType();
        return view('modules.agg.attendance.select', compact('header', 'types', 'by_lists'));
    }

    // get process type
    // ----------------
    public function postAttSelectType(Requests\Mod\Agg\Attendance\Process\Select $request) 
    {
        return redirect()->route('mod.agg.attendance.start')->withInput();
    }

    // select process type
    // -------------------
    public function getAttTypeStart() 
    {
        // have value
        // ----------
        if (!empty(old('type_id')) && !empty(old('by_id'))) {

            // get process name
            // ----------------
            $by_name = $this->agg_repo->getProcessName(old('by_id'));
            $type_name = $this->agg_repo->getTypeName(old('type_id'));

            // get header
            // ----------
            $header = array(
                'parent' => 'Aggregator',
                'child'  => 'Attendance',
                'sub'    => 'Process/Export Attendance',
                'sub-a'  => route('mod.agg.attendance.select'),
                'cat'    => $type_name,
                'icon'   => 'refresh',
                'title'  => $type_name
            );
            $type_id = old('type_id');
            $by_id = old('by_id');

            // get all sites
            // -------------
            $sites = $this->user_repo->getListSiteByCode();
            return view('modules.agg.attendance.start', compact('header', 'by_name', 'type_name', 'type_id', 'by_id', 'sites'));
        }

        // record not exist
        // ----------------
        else {
            return redirect()->route('mod.agg.attendance.select');
        }
    }

    // start process
    // -------------
    public function postAttTypeStart(Requests\Mod\Agg\Attendance\Process\Start $request) 
    {
        // execute process/export attendance
        // ---------------------------------
        $exe = $this->agg_repo->getAttProExpExe($request->all());

        // return message
        // --------------
        return redirect()->route('mod.agg.attendance.select')->with([
            'message' => $exe[0],
            'label'   => 'alert alert-' . $exe[1] . ' alert-dismissible'
        ]);
    }

    // check transfer attendance
    // -------------------------
    public function getAttCheck() {
        $index = $this->agg_repo->getAttendanceCheck();
        return $index;
    }

    // search transfer attendance
    // --------------------------
    public function postAttCheck(Request $request) {
        $index = $this->agg_repo->getAttendanceCheck($request);
        return $index;
    }

    // view transfer attendance
    // ------------------------
    public function getAttCheckView($id) {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Attendance',
            'sub'    => 'Check Transfer',
            'icon'   => 'info',
            'title'  => 'Check Transfer'
        );
        $detail = $this->agg_repo->getMyCommAttDetail($id);
        return view('modules.agg.attendance.check.view', compact('header', 'detail'));
    }

    // sync site detail
    // ----------------
    public function getSiteSync() {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Site Detail',
            'sub'    => 'Sync Site Detail',
            'icon'   => 'refresh',
            'title'  => 'Sync Site Detail'
        );
        $status = array('' => '[Status]', 1 => 'ACTIVE', 2 => 'INACTIVE');
        return view('modules.agg.site.sync', compact('header', 'status'));
    }

    // process sync site detail
    // ------------------------
    public function postSiteSync(Requests\Mod\Agg\Site\Export\Start $request) 
    {
        if ($request->status_id == 1) {
            $label = 'Active';
            $resp = $this->agg_repo->getSiteSync(1);
        } 
        else {
            $label = 'inactive';
            $resp = $this->agg_repo->getSiteSync(2);
        }

        // set text message
        // ----------------
        $text = "Start: " . $resp['start'] . "<br>";
        $text .= "End: " . $resp['end'] . "<br>";
        $text .= "Total: " . $resp['total'] . "<br>";
        $text .= "Insert: " . $resp['insert'] . "<br>";
        $text .= "Update: " . $resp['update'];

        // return result
        // -------------
        return redirect()->route('mod.agg.site.sync')->with([
            'text'    => $text,
            'message' => 'Site ' . $label . ' successfully Synced.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // page to send the aggregator record to mcmc
    // ------------------------------------------
    public function getAggTransfer()
    {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Transfer',
            'icon'   => 'action-redo',
            'title'  => 'Transfer'
        );
        $types = array('' => '[Type]', 1 => 'Staff', 2 => 'Attendance');
        return view('modules.agg.transfer', compact('header', 'types'));        
    }

    // send the record
    // ---------------
    public function postAggTransfer(Requests\Mod\Agg\Transfer $request)
    {
        // trigger the send process
        // ------------------------
        $msg = $this->getAttUrlTransfer($request->type_id);

        // return result
        // -------------
        return redirect()->route('mod.agg.transfer')->with([
            'message' => $msg[0],
            'label'   => 'alert alert-'.$msg[1].' alert-dismissible'
        ]);
    }


}



