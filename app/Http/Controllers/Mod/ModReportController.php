<?php

namespace IhrV2\Http\Controllers\Mod;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Repositories\Report\ReportRepository;

class ModReportController extends Controller
{
	protected $report_repo;
    public function __construct(ReportRepository $report_repo) {
        $this->middleware('auth');
        $this->report_repo = $report_repo;
    }

    public function getReportStaff()
    {
        $index = $this->report_repo->getReportStaff();
        return $index;    	
    }

    public function postReportStaff(Request $request)
    {
        $index = $this->report_repo->getReportStaff($request);
        return $index;         
    }
}
