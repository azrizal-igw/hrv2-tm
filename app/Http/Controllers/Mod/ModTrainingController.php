<?php

namespace IhrV2\Http\Controllers\Mod;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Contracts\Training\TrainingInterface;
use IhrV2\Contracts\User\UserInterface;
use QrCode;

class ModTrainingController extends Controller
{
    protected $training;
    protected $user;

    public function __construct(TrainingInterface $training, UserInterface $user) {
        $this->middleware('auth');
        $this->training = $training;
        $this->user = $user;
    }

    // show all training
    // -----------------
    public function getTrainingIndex() {
        $index = $this->training->getTrainingIndex();
        return $index;
    }

    // search training
    // ---------------
    public function postTrainingIndex(Request $request) {
        $index = $this->training->getTrainingIndex($request);
        return $index;
    }

    // create new training
    // -------------------
    public function getTrainingCreate()
    {
        $header = array(
            'parent'  => 'Training Attendance',
            'child'   => 'Add',
            'icon'    => 'plus',
            'title'   => 'Add'
        );
        return view('modules.training.create', compact('header'));
    }

    // get lists of selected trainees (by session)
    // -------------------------------------------
    public function getTrainingSession()
    {
        $json['list'] = $this->training->getTrainingSession();        
        return response()->json($json);        
    }

    // remove selected trainees (by session)
    // ------------------------------------
    public function postTrainingRemove(Request $request)
    {
        $json['list'] = $this->training->postTrainingRemove($request);        
        return response()->json($json);  
    }

    // insert new training
    // -------------------
    public function postTrainingCreate(Requests\Mod\Training\Create $request)
    {
        // save training
        // -------------
        $save = $this->training->getTrainingSave($request->all());

        // return output
        // -------------
        return redirect()->route('mod.training.create')->with([
            'message' => $save[1],
            'label'   => 'alert alert-' . $save[0] . ' alert-dismissible'
        ]);        
    }

    // popup lists of user
    // -------------------
    public function getTrainingPopup()
    {
        // get lists sites
        // ---------------
        $chk = $this->user->getListSiteNCode();
        $sites = $chk['sites'];

        // get lists regions
        // -----------------
        $regions = $this->user->getRegionList();

        // get lists phases
        // ----------------
        $phases = $this->user->getPhaseList();   

        // get lists of active users
        // -------------------------
        // $users = $this->training->getStaffActiveAll2();
        return View('modules.training.popup.user', compact('sites', 'regions', 'phases'));
    }

    // lists of users
    // --------------
    public function getTrainingUser(Request $request)
    {
        $json['list'] = $this->training->getTrainingUser($request->all());
        return response()->json($json); 
    }

    // put selected user to the session and back to create training
    // ------------------------------------------------------------
    public function postTrainingCreateUser(Request $request)
    {
        $index = $this->training->getTrainingCreateUser($request);
        return $index;
    }

    // view training qrcode
    // --------------------
    public function getTrainingPopupCode($id) {
        $training = array();
        $training['code'] = QrCode::size(600)->generate($id);
        return View('modules.training.popup.code', compact('training'));
    }


}



