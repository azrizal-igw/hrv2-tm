<?php namespace IhrV2\Http\Controllers\Mod;

use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Maintenance\DbMaintenanceInterface;
use IhrV2\Contracts\Maintenance\MaintenanceInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;
use Session;

class ModMaintenanceController extends Controller {

    protected $user_repo;
    protected $leave_repo;
    protected $db_mtn_repo;
    protected $mtn_repo;

    public function __construct(UserInterface $user_repo, LeaveInterface $leave_repo, DbMaintenanceInterface $db_mtn_repo, MaintenanceInterface $mtn_repo) {
        $this->middleware('auth');
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->db_mtn_repo = $db_mtn_repo;
        $this->mtn_repo = $mtn_repo;
    }

    // lists of sites
    // --------------
    public function showSiteIndex() {
        $index = $this->mtn_repo->getSiteIndex();
        return $index;
    }

    // search site
    // -----------
    public function postSiteIndex(Request $request) {
        $index = $this->mtn_repo->getSiteIndex($request);
        return $index;
    }

    // view detail site
    // ----------------
    public function showSiteView($id) {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Site',
            'child-a' => route('mod.site.index'),
            'sub' => 'View',
            'icon'    => 'info',
            'title'   => 'View'
        );
        $detail = $this->mtn_repo->getSiteInfo($id);
        return View('modules.site.view', compact('header', 'detail'));
    }

    // cancel the site
    // ---------------
    public function postSiteView(Request $request, $id) {
        $update = $this->db_mtn_repo->dbCancelSite($id);
        return redirect()->route('mod.site.index')->with([
            'message' => 'Site successfully cancel.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // lists public holiday
    // --------------------
    public function showPublicHoliday() {
        $index = $this->mtn_repo->getPublicIndex();
        return $index;
    }

    // search public holiday
    // ---------------------
    public function postPublicHoliday(Request $request) {
        $index = $this->mtn_repo->getPublicIndex($request);
        return $index;
    }

    // view public holiday
    // -------------------
    public function showPublicHolidayView($id) 
    {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Public Holiday',
            'child-a' => route('mod.public-holiday.index'),
            'sub'     => 'View',
            'icon'    => 'info',
            'title'   => 'View'
        );
        $public = $this->mtn_repo->getLeavePublicOne($id);
        return View('modules.public-holiday.view', compact('header', 'public'));
    }

    // cancel public holiday
    // ---------------------
    public function postPublicHolidayView(Request $request, $id) {
        $cancel = $this->mtn_repo->getPublicHolidayCancel($id);
        return redirect()->route('mod.public-holiday.index')->with([
            'message' => $cancel[0],
            'label'   => 'alert alert-'.$cancel[1].' alert-dismissible'
        ]);
    }

    // add new public holiday
    // ----------------------
    public function showPublicHolidayAdd() {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Public Holiday',
            'sub'     => 'Manual',
            'icon'    => 'note',
            'title'   => 'Manual'
        );
        $states = $this->mtn_repo->getStateAll();
        return View('modules.public-holiday.create', compact('header', 'states'));
    }

    // save new public holiday
    // -----------------------
    public function postPublicHolidayAdd(Requests\Mod\PublicHoliday\Create $request) 
    {
        $save = $this->mtn_repo->getPublicHolidayCreate($request->all());
        return redirect()->route('mod.public-holiday.index')->with([
            'message' => $save[0],
            'label'   => 'alert alert-'.$save[1].' alert-dismissible'
        ]);
    }

    // edit public holiday
    // -------------------
    public function showPublicHolidayEdit($id) {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Public Holiday',
            'child-a' => route('mod.public-holiday.index'),
            'sub'     => 'View',
            'sub-a'   => route('mod.public-holiday.view', array($id)),
            'cat1'     => 'Edit',
            'icon'    => 'event',
            'title'   => 'Edit'
        );

        // get leave public info
        // ---------------------
        $public = $this->mtn_repo->getLeavePublicByID($id);

        // get leave public states
        // -----------------------
        $public_states = $this->mtn_repo->getLeavePublicState($public->id)->lists('state_id')->toArray();

        // get all states
        // --------------
        $states = $this->mtn_repo->getStateAll();
        return View('modules.public-holiday.edit', compact('header', 'public', 'public_states', 'states'));
    }

    // update public holiday
    // ---------------------
    public function postPublicHolidayEdit(Requests\Mod\PublicHoliday\Update $request, $id) 
    {
        $save = $this->mtn_repo->getPublicHolidayEdit($request->all(), $id);
        return redirect()->route('mod.public-holiday.index')->with([
            'message' => $save[0],
            'label'   => 'alert alert-'.$save[1].' alert-dismissible'
        ]);
    }

    // show calendar
    // -------------
    public function showCalendarIndex() {
        $index = $this->mtn_repo->getCalendar();
        return $index;
    }

    // search lists of off day
    // -----------------------
    public function postCalendarIndex(Request $request) {
        $index = $this->mtn_repo->getCalendar($request);
        return $index;
    }

    // view detail date
    // ----------------
    public function showCalendarView($group_id, $date) {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Calendar',
            'child-a'  => route('mod.calendar.index'),
            'sub'    => 'View',
            'icon'   => 'info',
            'title'  => 'View'
        );

        // get off day
        // -----------
        $off = $this->mtn_repo->getOffDayDetail($group_id, $date);

        // get public holiday
        // ------------------
        $public = $this->mtn_repo->getPublicDetail($date);
        return view('modules.calendar.view', compact('header', 'off', 'public'));
    }

    // cancel the off day/public holiday
    // ---------------------------------
    public function postCalendarView(Request $request, $group_id, $date) {
        dd('in progress');

        // cancel off day/public holiday
        // -----------------------------
        $cancel = $this->mtn_repo->getCancelOffDay($request->all());

        // return success cancel
        // ---------------------
        return redirect()->route('mod.calendar.view', array($group_id, $date))->with([
            'message' => 'Record successfully Cancel.',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // calendar list
    // -------------
    public function showCalendarList()
    {
        dd(0);
    }

    // show off day
    // ------------
    public function showOffDaySelect() {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Off Day',
            'child-a' => route('mod.off-day.index'),
            'sub'    => 'Add',
            'icon'   => 'plus',
            'title'  => 'Add'
        );
        $off_groups = $this->mtn_repo->getLeaveOffGroup();
        return view('modules.off-day.select', compact('header', 'off_groups'));
    }

    // select group off day
    // --------------------
    public function postOffDaySelect(Requests\Mod\OffDay\Select $request) {
        return redirect()->route('mod.off-day.create')->withInput();
    }

    // add off day form
    // ----------------
    public function showOffDayAdd(Request $request) {
        if (!empty(old('off_year')) && !empty(old('off_group'))) {
            $header = array(
                'parent' => 'Maintenance',
                'child'  => 'Off Day',
                'child-a' => route('mod.off-day.index'),
                'sub'    => 'Add',
                'icon'   => 'plus',
                'title'  => 'Add'
            );

            // get input from page
            // -------------------
            $off_year = old('off_year');
            $off_group = old('off_group');

            // query off day info
            // ------------------
            $group = $this->mtn_repo->getOffGroupID($off_group);
            $off_types = $this->mtn_repo->getLeaveOffTypes();
            $states = $this->mtn_repo->getLeaveOffState($off_group);
            return View('modules.off-day.create', compact('header', 'off_year', 'off_group', 'group', 'off_types', 'states'));
        } else {
            return redirect()->route('mod.off-day.select');
        }
    }

    // save new off day
    // ----------------
    public function postOffDayAdd(Requests\Mod\OffDay\Create $request) 
    {
        $save = $this->mtn_repo->getAddOffDay($request->all());
        return redirect()->route('mod.off-day.select')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // display lists of off day
    // ------------------------
    public function showOffDayIndex() {
        $index = $this->mtn_repo->getOffDay();
        return $index;
    }

    // search lists of off day
    // -----------------------
    public function postOffDayIndex(Request $request) {
        $index = $this->mtn_repo->getOffDay($request);
        return $index;
    }

    // view off day
    // ------------
    public function showOffDayView($id) {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Off Day',
            'child-a' => route('mod.off-day.index'),
            'sub'    => 'View',
            'icon'   => 'info',
            'title'  => 'View'
        );

        // get off/rest day info
        // ---------------------
        $off = $this->mtn_repo->getOffDayByID($id)->toArray();
        return view('modules.off-day.view', compact('header', 'off'));
    }

    // cancel off day
    // --------------
    public function postOffDayView(Request $request, $id) {
        $save = $this->mtn_repo->getOffDayCancel($id);
        return redirect()->route('mod.off-day.index')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);        
    }

    // edit off day
    // ------------
    public function showOffDayEdit($id) {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Off Day',
            'child-a' => route('mod.off-day.index'),
            'sub'    => 'View',
            'sub-a'  => route('mod.off-day.view', array($id)),
            'cat1'   => 'Edit',
            'icon'   => 'note',
            'title'  => 'Edit'
        );

        // get off day info
        // ----------------
        $off = $this->mtn_repo->getLeaveOffDayByID($id);

        // get off day states
        // ------------------
        $off_states = $this->mtn_repo->getOffDayState($off->id)->lists('state_id')->toArray();

        // get all states
        // --------------
        $states = $this->mtn_repo->getStateAll();        
        return view('modules.off-day.edit', compact('header', 'off', 'off_states', 'states'));        
    }

    // update off day
    // --------------
    public function postOffDayEdit(Request $request, $id) {
        $save = $this->mtn_repo->getOffDayEdit($request->all(), $id);
        return redirect()->route('mod.off-day.view', array($id))->with([
            'message' => $save[0],
            'label'   => 'alert alert-'.$save[1].' alert-dismissible'
        ]);
    }

    // display all fasting
    // -------------------
    public function showFastingIndex() {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Fasting',
            'icon'   => 'list',
            'title'  => 'Fasting'
        );

        // check session year
        // ------------------
        if (session()->has('year')) {
            $year = session()->get('year');
        } else {
            $year = date('Y');
        }

        // get search sessions
        // -------------------
        $sessions = array('year' => $year);

        // get lists of fasting
        // --------------------
        $fast = $this->mtn_repo->getFastingList($year);
        return view('modules.fasting.index', compact('header', 'sessions', 'fast'));
    }

    // search fasting
    // --------------
    public function postFastingIndex(Request $request) {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Fasting Time',
            'icon'   => 'event',
            'title'  => 'Fasting Time'
        );

        // reset searching
        // ---------------
        $reset = 0;
        if (isset($request->reset)) {
            $reset = 1;
            session()->forget('year');
            return redirect()->route('mod.fasting.index');
        }

        // session year
        // ------------
        if (!empty($request->year) && $reset == 0) {
            session()->put('year', $request->year);
            $year = session()->get('year');
        } else {
            session()->forget('year');
            $year = null;
        }
        // get search sessions
        // -------------------
        $sessions = array('year' => $year);

        // get lists of fasting
        // --------------------
        $fast = $this->mtn_repo->getFastingList($year);
        return view('modules.fasting.index', compact('header', 'sessions', 'fast'));
    }

    // view fasting
    // ------------
    public function showFastingView($id) {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Fasting Time',
            'child-a' => route('mod.fasting.index'),
            'sub'     => 'View Fasting',
            'icon'    => 'info',
            'title'   => 'View Fasting'
        );

        // get fasting info
        // ----------------
        $detail = $this->mtn_repo->getFastingByID($id);
        return view('modules.fasting.view', compact('header', 'detail'));
    }

    // cancel fasting
    // --------------
    public function postFastingView(Request $request, $id) {

        // update status
        // -------------
        $cancel = $this->mtn_repo->getFastingCancel($id);

        // redirect to list page
        // ---------------------
        return redirect()->route('mod.fasting.index')->with([
            'message' => $cancel[0],
            'label'   => 'alert alert-' . $cancel[1] . ' alert-dismissible'
        ]);
    }

    // add new fasting
    // ---------------
    public function showFastingCreate() 
    {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Fasting',
            'child-a' => route('mod.fasting.index'),
            'sub'     => 'Add',
            'icon'    => 'plus',
            'title'   => 'Add'
        );

        // get lists of states
        // -------------------
        $states = $this->mtn_repo->getStateAll();
        return view('modules.fasting.create', compact('header', 'states'));
    }

    // create fasting
    // --------------
    public function postFastingCreate(Requests\Mod\Fasting\Create $request) 
    {
        // save fasting
        // ------------
        $save = $this->mtn_repo->getFastingCreate($request->all());

        // redirect to list page
        // ---------------------
        return redirect()->route('mod.fasting.index')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // lists announcement
    // ------------------
    public function showAnnoIndex() {
        $index = $this->mtn_repo->getAnnoIndex();
        return $index;
    }

    // search announcement
    // -------------------
    public function postAnnoIndex(Request $request) {
        $index = $this->mtn_repo->getAnnoIndex($request);
        return $index;
    }

    // view announcement
    // -----------------
    public function showAnnoView($id) {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Announcement',
            'child-a' => route('mod.announcement.index'),
            'sub'     => 'View',
            'icon'    => 'info',
            'title'   => 'View'
        );
        $detail = $this->mtn_repo->getAnnoByID($id);
        return view('modules.announcement.view', compact('header', 'detail'));
    }

    // cancel announcement
    // -------------------
    public function postAnnoView(Request $request, $id) {

    }

    // create announcement
    // -------------------
    public function showAnnoCreate() {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Announcement',
            'child-a' => route('mod.announcement.index'),
            'sub'     => 'Add',
            'icon'    => 'plus',
            'title'   => 'Add'
        );
        return view('modules.announcement.create', compact('header'));
    }

    // process new announcement
    // ------------------------
    public function postAnnoCreate(Requests\Mod\Announcement\Create $request) 
    {
        // save new announcement
        // ---------------------
        $save = $this->mtn_repo->getProcessNewAnno($request->all());

        // return message
        // --------------
        return redirect()->route('mod.announcement.index')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // show download page
    // ------------------
    public function showDownload() {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Download',
            'icon'   => 'arrow-down-circle',
            'title'  => 'Download'
        );
        return view('modules.download.index', compact('header'));
    }

    // lists of set work hour
    // ----------------------
    public function showWorkHourIndex()
    {
        $index = $this->mtn_repo->getWorkHourIndex();
        return $index;
    }

    // search set work hour
    // --------------------
    public function postWorkHourIndex(Request $request)
    {
        $index = $this->mtn_repo->getWorkHourIndex($request);
        return $index;
    }

    // view work hour
    // --------------
    public function showWorkHourView($id)
    {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Work Hour',
            'child-a' => route('mod.work-hour.index'),
            'sub'     => 'View',
            'icon'    => 'hourglass',
            'title'   => 'View'
        );
        $work_hour = $this->mtn_repo->getWorkHourByID($id);
        return View('modules.work-hour.view', compact('header', 'work_hour'));
    }

    // edit work hour
    // --------------
    public function showWorkHourEdit($id)
    {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Work Hour',
            'child-a' => route('mod.work-hour.index'),
            'sub'     => 'Edit',
            'icon'    => 'hourglass',
            'title'   => 'Edit'
        );
        $work_hour = $this->mtn_repo->getWorkHourByID($id);
        $exp_days = explode(':', $work_hour->days);
        $days = $this->mtn_repo->getDayAll()->toArray();
        return View('modules.work-hour.edit', compact('header', 'work_hour', 'exp_days', 'days'));
    }

    // update work hour
    // ----------------
    public function postWorkHourEdit(Requests\Mod\WorkHour\Edit $request, $id)
    {
        // update work hour
        // ----------------
        $save = $this->mtn_repo->getWorkHourEdit($request->all(), $id);

        // return message
        // --------------
        return redirect()->route('mod.work-hour.index')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);        
    }

    // create new working hour
    // -----------------------
    public function showWorkHourUser($uid, $sitecode)
    {
        // header info
        // -----------
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Work Hour',
            'sub'    => 'Change',
            'icon'   => 'plus',
            'title'  => 'Change'
        );

        // get user info
        // -------------
        $user = $this->leave_repo->getCheckUserActive($uid, $sitecode);

        // get contract info
        // -----------------
        $contract = $user['user']['user_latest_contract'];
        $date_from = $contract['date_from'];
        $date_to = $contract['date_to'];

        // get job info
        // ------------
        $job = $user['user']['user_latest_job'];
        $position = $job['position_name']['name'];

        // get site info
        // -------------
        $site = $user['user']['site_name_info'];

        // get rm info
        // -----------
        $chk_rm = $this->user_repo->getRegionBySitecode($sitecode);
        $rm = $chk_rm['region_manager_info']['region_manager_detail']['name'];

        // get contract lists
        // ------------------
        $contracts = $this->user_repo->getContractActiveDrop($uid, $sitecode);

        // get day lists
        // -------------
        $days = $this->mtn_repo->getDayAll()->toArray();

        // get work hour
        // -------------
        $work_hour = $this->mtn_repo->getWorkHourByStaff($uid, $sitecode);       
        return view('modules.leave.user.work-hour.view', compact('header', 'contracts', 'days', 'work_hour'));
    }

    // save new working hour
    // ---------------------
    public function postWorkHourUser(Requests\Mod\WorkHour\Create $request, $uid, $sitecode)
    {
        // save working hour
        // -----------------
        $save = $this->mtn_repo->getWorkHourCreate($request->all(), $uid, $sitecode);

        // redirect to list page
        // ---------------------
        return redirect()->route('mod.leave.user.work-hour.view', array($uid, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }
    
    // popup work hour
    // ---------------
    public function showWorkHourUserPopup($id, $uid, $sitecode)
    {
        // get user info
        // -------------
        $user = array('uid' => $uid, 'sitecode' => $sitecode);

        // get work hour info
        // ------------------
        $work = $this->mtn_repo->getWorkHourDetail($id, $uid, $sitecode);
        return View('modules.leave.user.work-hour.popup.detail', compact('user', 'work'));        
    }

    // show email notification
    // -----------------------
    public function showEmailNotification()
    {
        // header info
        // -----------
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Email Notification',
            'icon'   => 'envelope-open',
            'title'  => 'Email Notification'
        );    
        $emails = $this->mtn_repo->getEmailNotiList();
        return view('modules.email.notification.index', compact('header', 'emails'));
    }

    public function postEmailNotification()
    {
        dd(8);
    }

    // edit email notification
    // -----------------------
    public function showEmailNotiEdit($id)
    {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Email Notification',
            'child-a' => route('mod.email.notification'),
            'sub'     => 'Edit',
            'icon'    => 'envelope-open',
            'title'   => 'Edit'
        );
        $email = $this->mtn_repo->getEmailNotiByID($id);
        return View('modules.email.notification.edit', compact('header', 'email'));        
    }

    public function postEmailNotiEdit(Request $request)
    {

    }

    // create new email notification
    // -----------------------------
    public function showEmailNotiCreate()
    {
        // header info
        // -----------
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Email',
            'sub'    => 'Notification',
            'sub-a'  => route('mod.email.notification'),
            'cat1'   => 'Add',
            'icon'   => 'plus',
            'title'  => 'Add'
        );
        $types = $this->mtn_repo->getListEmailNotiTypes();
        return view('modules.email.notification.create', compact('header', 'types'));
    }

    // insert new email notification
    // -----------------------------
    public function postEmailNotiCreate(Requests\Mod\Email\Notification\Create $request)
    {
        $save = $this->mtn_repo->getEmailNotiInsert($request->all());
        return redirect()->route('mod.email.notification')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);        
    }

}
