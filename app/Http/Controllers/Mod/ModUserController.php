<?php
namespace IhrV2\Http\Controllers\Mod;

use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\Letter\LetterInterface as LetterRepository;
use IhrV2\Contracts\Upload\UploadInterface as UploadRepository;
use IhrV2\Contracts\User\DbUserInterface as DbUserRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon; 

class ModUserController extends Controller {
    protected $user_repo;
    protected $leave_repo;
    protected $db_user_repo;
    protected $upload_repo;
    protected $letter_repo;

    public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo, DbUserRepository $db_user_repo, UploadRepository $upload_repo, LetterRepository $letter_repo) {
        $this->middleware('auth');
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->db_user_repo = $db_user_repo;
        $this->upload_repo = $upload_repo;
        $this->letter_repo = $letter_repo;
    }

    // list user
    // ---------
    public function showUserIndex() 
    {
        $index = $this->user_repo->getUserIndex();
        return $index;
    }

    // search user
    // -----------
    public function postUserIndex(Request $request) 
    {
        $index = $this->user_repo->getUserIndex($request);
        return $index;
    }

    // select group before add user
    // ----------------------------
    public function showUserSelect()
    {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff',
            'child-a' => route('mod.user.index'),
            'icon'    => 'user-follow',
            'title'   => 'Add'
        );
        $groups = $this->user_repo->getGroupNotIn(array(3,6,7));
        return View('modules.user.select', compact('header', 'groups'));
    }

    // select group
    // ------------
    public function postUserSelect(Requests\Mod\User\Select $request)
    {
        return redirect()->route('mod.user.create')->withInput();
    }

    // add user form
    // -------------
    public function showUserCreate() 
    {
        // have value
        // ----------
        if (!empty(old('group_id'))) {

            // get header
            // ----------
            $header = array(
                'parent'  => 'Staff Administration',
                'child'   => 'Staff',
                'child-a' => route('mod.user.index'),
                'icon'    => 'user-follow',
                'title'   => 'Add'
            );

            // get group id
            // ------------
            $group_id = old('group_id');

            // get position lists
            // ------------------
            $positions = $this->user_repo->getPositionList(array($group_id));

            // get region lists
            // ----------------
            $regions = $this->user_repo->getRegionList();
            return view('modules.user.create', compact('header', 'group_id', 'positions', 'regions'));
        }

        // record not exist
        // ----------------
        else {
            return redirect()->route('mod.user.select');
        }
    }

    // save new user
    // -------------
    public function postUserCreate(Requests\Mod\User\Create $request) 
    {
        $save = $this->user_repo->getUserCreateOther($request->all());
        return redirect()->route('mod.user.index')->with([
            'message' => $save[1],
            'label'   => 'alert alert-' . $save[0] . ' alert-dismissible'
        ]);
    }

    // show change password form
    // -------------------------
    public function showUserPassword($id, $token) 
    {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff',
            'child-a' => route('mod.user.index'),
            'sub'     => 'Change Password',
            'icon'    => 'lock',
            'title'   => 'Change Password'
        );
        $detail = $this->user_repo->getUserByIDToken($id, $token);
        return View('modules.user.password', compact('header', 'detail'));
    }

    // update user password
    // --------------------
    public function updateUserPassword(Requests\Mod\User\Password\Update $request, $id, $token) 
    {
        $user = $this->user_repo->getUserByID($id);
        $user->fill(['password' => \Hash::make($request->new_password)])->save();
        $msg = array('Password successfully updated.', 'success');
        return redirect()->back()->with([
            'message' => $msg[0],
            'label'   => 'alert alert-' . $msg[1] . ' alert-dismissible'
        ]);
    }

    // view user detail
    // ----------------
    public function showUserView($id, $token) 
    {
        $detail = $this->user_repo->getUserByIDToken($id, $token);
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff',
            'child-a' => route('mod.user.index'),
            'sub'     => 'View',
            'icon'    => 'user',
            'title'   => $detail->name
        );
        $age = date('Y') - date('Y', strtotime($detail->dob));
        return View('modules.user.view', compact('detail', 'header', 'age'));
    }

    public function postUserView(Request $request, $id) {

    }

    // show tab personal
    // -----------------
    public function showUserViewPersonal($uid, $token) 
    {
        // get profile info
        // ----------------
        $personal = $this->user_repo->getUserByIDToken($uid, $token);

        // get age
        // -------
        $age = date('Y') - date('Y', strtotime($personal->dob));

        // get photo
        // ---------
        $photo = $this->user_repo->getUserPhotoByUserIDToken($uid, $token);

        // correspondence state
        // --------------------
        $corr_state = $this->user_repo->getStateByCode($personal->correspondence_state);

        // return personal tab
        // -------------------
        return View('modules.user.tab.personal', compact('personal', 'age', 'photo', 'corr_state'));
    }

    // show tab job
    // ------------
    public function showUserViewJob($uid, $token) 
    {
        // get active job
        // ---------------
        $job = $this->user_repo->getUserJobByUserIDToken($uid, $token);

        // get previous job
        // ----------------
        $prev = $this->user_repo->getUserJobPrevByUserIDToken($uid, $token);

        // get user info
        // -------------
        $user = $this->user_repo->getUserByID($uid);

        // check record transfer
        // ---------------------
        $transfer = $this->user_repo->getUserTransfer($uid, $user->icno);
        return View('modules.user.tab.job', compact('job', 'prev', 'transfer', 'token'));
    }

    // show tab contract
    // -----------------
    public function showUserViewContract($uid, $token) 
    {
        // initial variable
        // ----------------
        $prev = array();
        $letter = array();
        $entitled = 0;
        $taken = 0;

        // get active contract
        // -------------------
        $contract = $this->user_repo->getActiveContractUIDToken($uid, $token);
        if (!empty($contract)) {

            // get entitled leave
            // ------------------
            $al_entitled = $this->leave_repo->getTotalALByJoinDate($uid, $contract->sitecode, $contract->date_from, $contract->date_to);
            $entitled = $al_entitled['total'];

            // get leave taken
            // ---------------
            $taken = $this->leave_repo->getLeaveTaken($uid, $contract->sitecode, 1, $contract->date_from, $contract->date_to);

            // get duplicate contract
            // ----------------------
            $duplicate = $this->user_repo->getDuplicateContract($uid, $token);

            // get user letter
            // ---------------
            $letter = $this->letter_repo->getLetterAllActive($uid, $contract->sitecode);
        }
        return View('modules.user.tab.contract', compact('contract', 'entitled', 'taken', 'letter', 'duplicate', 'uid', 'token'));
    }

    // get lists of previous contract by paginate
    // ------------------------------------------
    public function showUserPrevContract($uid, $token) 
    {
        // get previous contract
        // ---------------------
        $prev = $this->user_repo->getPrevContractByPaginate($uid, $token);

        // initialize string
        // -----------------
        $str = "";

        // set table
        // ---------
        $str .= "<table class=\"table table-condensed table-responsive\">";

        // set row head
        // ------------
        $str .= "<thead>";
        $str .= "   <tr class=\"bg-danger\">";
        $str .= "       <td colspan=\"5\"><i class=\"icon-clock\"></i>&nbsp;<strong><span id=\"table-count\"></span></strong></td>";
        $str .= "   </tr>";
        $str .= "</thead>";

        // set row title
        // -------------
        $str .= "<tr class=\"active\">";
        $str .= "   <th class=\"col-md-1\">No</th>";
        $str .= "   <th class=\"col-md-1\">Year</th>";
        $str .= "   <th class=\"col-md-6 text-center\">Date</th>";
        $str .= "   <th class=\"col-md-2 text-center\">Days</th>";
        $str .= "   <th class=\"col-md-2 text-right\">Actions</th>";
        $str .= "</tr>";

        // have record
        // -----------
        if (count($prev) > 0) {

            // get iteration number
            // --------------------
            $no = $prev->firstItem() - 1;
            foreach ($prev as $i) {
                $no++;

                // get year
                // --------
                if ($i->date_from != '0000-00-00') {
                    $year = Carbon::parse($i->date_from)->format('Y');
                }
                else {
                    $year = '-';                                                            
                }

                // get dates
                // ---------
                if ($i->date_from != '0000-00-00') {
                    $from = Carbon::parse($i->date_from)->format('j M Y');
                }
                else {
                    $from = $i->date_from;
                }      
                if ($i->date_to != '0000-00-00') {
                    $to = Carbon::parse($i->date_to)->format('j M Y');
                }
                else {
                    $to = $i->date_to;
                }

                // get duration
                // ------------
                $duration = $i->duration;

                // get route detail
                // ----------------
                $route = route('mod.user.view.popup.contract', array($i->id, $uid, $token));

                // set link detail 
                // ---------------                
                $icon = "<a href=\"#\" id=\"modal-popup\" data-url=\"$route\" class=\"btn btn-success btn-sm\" title=\"View Details\"><i class=\"icon-magnifier-add\"></i></a>";

                // set row value
                // -------------
                $str .= "<tr>";
                $str .= "   <td>".$no."</td>";
                $str .= "   <td>".$year."</td>";
                $str .= "   <td class=\"text-center\">".$from." - ".$to."</td>";
                $str .= "   <td class=\"text-center\">".$duration."</td>";
                $str .= "   <td class=\"text-right\">".$icon."</td>";
                $str .= "</tr>";                
            }

            // show pagination button if record more than 5
            // --------------------------------------------
            if ($prev->total() > $prev->perPage()) {

                // disable previous button if 1st page
                // -----------------------------------
                if ($prev->currentPage() == 1) {
                    $first_disabled = 'disabled';
                }
                else {
                    $first_disabled = '';
                }

                // disable next button if current page is equal last page
                // ------------------------------------------------------
                if ($prev->currentPage() == $prev->lastPage()) {
                    $last_disabled = 'disabled';
                }
                else {
                    $last_disabled = '';
                }

                // get route
                // ---------
                $route = route('mod.user.contract.prev.api', array($uid, $token));

                // get page no
                // -----------
                $previous = $prev->currentPage() - 1;
                $next = $prev->currentPage() + 1;

                // set row button
                // --------------
                $str .= "<tr>";
                $str .= "   <td colspan=\"2\" class=\"text-left\"><button class=\"btn btn-sm btn-info\" id=\"previous\" title=\"Previous\" type=\"button\" onclick=\"getModDataList('$route', $previous)\" $first_disabled><i class=\"icon-arrow-left\"></i></button></td>";
                $str .= "   <td colspan=\"1\"></td>";
                $str .= "   <td colspan=\"2\" class=\"text-right\"><button class=\"btn btn-sm btn-info\" id=\"previous\" title=\"Next\" type=\"button\" onclick=\"getModDataList('$route', $next)\" $last_disabled><i class=\"icon-arrow-right\"></i></button></td>";            
                $str .= "</tr>";
            }
            $str .= "</table>";

            // put into array
            // --------------
            $json['list'] = $str;
            $json['paging'] = $prev;
            $json['total'] = $prev->total().' Previous Contract';
        }

        // no record
        // ---------
        else {

            // set row column
            // --------------
            $str .= "<tr><td colspan=\"5\">No record</td></tr>";
            $str .= "</table>";

            // put into array
            // --------------
            $json['list'] = $str;
            $json['total'] = '0 Previous Contract';
        }
        return response()->json($json);
    }

    // view popup contract
    // -------------------
    public function showUserPopupContract($id, $uid, $token) 
    {
        // get user info
        // -------------
        $user = array('id' => $id, 'uid' => $uid, 'token' => $token);

        // get contract info
        // -----------------
        $contract = $this->user_repo->getUserContractByID($id);

        // get duplicate contract
        // ----------------------
        $duplicate = $this->user_repo->getUserContractDuplicate($id, $uid, $contract->date_from, $contract->date_to);
        return View('modules.user.tab.popup.contract', compact('user', 'contract', 'duplicate'));
    }

    // cancel previous contract at popup
    // ---------------------------------
    public function postUserPopupContract(Request $request, $id, $uid, $token) 
    {
        // merge contract
        // --------------
        $this->user_repo->getUserContractMerge($id, $uid, $request->contract_id);

        // get all previous contract
        // -------------------------
        $prev = $this->showUserPrevContract($uid, $token);
        return $prev;
    }

    // view popup job
    // --------------
    public function showUserPopupJob($id, $uid, $token) {
        $job = $this->user_repo->getUserJobID($id);
        return View('modules.user.tab.popup.job', compact('job'));
    }

    // cancel previous job at popup
    // ----------------------------
    public function postUserPopupJob($id, $uid, $token, Request $request) {

        // cancel job
        // ----------
        $cancel = $this->user_repo->getUserJobCancel($id, $uid);
        return redirect()->route('mod.user.view', array($uid, $token))->with([
            'message' => $cancel[0],
            'label'   => 'alert alert-' . $cancel[1] . ' alert-dismissible'
        ]);
    }

    // view popup letter
    // -----------------
    public function showUserPopupLetter($id) {
        $letter = $this->letter_repo->getLetterInfo($id);
        return View('modules.user.tab.popup.letter', compact('letter'));
    }

    // show tab family
    // ---------------
    public function showUserViewFamily($uid, $token) {
        $user = $this->user_repo->getUserByIDToken($uid, $token);
        return view('modules.user.tab.family', compact('uid', 'token', 'user'));
    }

    public function showUserFamilyJson($uid, $token) {
        $data = $this->user_repo->getUserFamilyByUserIDToken($uid, $token);
        return response()->json($data);
    }

    // show tab education
    // ------------------
    public function showUserViewEducation($uid, $token) {
        $detail = $this->user_repo->getUserEducationByUserIDToken($uid, $token);
        return View('modules.user.tab.education', compact('detail'));
    }

    // show tab emergency
    // ------------------
    public function showUserViewEmergency($uid, $token) {
        $detail = $this->user_repo->getUserEmergencyByUserIDToken($uid, $token);
        return View('modules.user.tab.emergency', compact('detail'));
    }

    // show tab photo
    // --------------
    public function showUserViewPhoto($uid, $token) {
        $photo = $this->user_repo->getUserPhotoByUserIDToken($uid, $token);
        return View('modules.user.tab.photo', compact('photo'));
    }

    // upload photo at tab photo
    // -------------------------
    public function postUserViewPhoto(Requests\Mod\User\Photo\Create $request, $uid, $token)
    {
        // get user info
        // -------------
        $user = $this->user_repo->getUserByID($uid);

        // process upload photo
        // --------------------
        $save = $this->user_repo->processUserPhoto($request->all(), $user->id, $user->sitecode, 2);

        // return message
        // --------------
        return redirect()->back()->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // get lists user photo
    // -------------------------------
    public function showUserPhoto() {
        $index = $this->user_repo->getUserPhoto();
        return $index;
    }

    // search user photo
    // -----------------
    public function postUserPhoto(Request $request) {
        $index = $this->user_repo->getUserPhoto($request);
        return $index;
    }

    // view user photo
    // ---------------
    public function showUserPhotoDetail($id, $uid, $sitecode) 
    {
        // get user info
        // -------------
        $user = $this->user_repo->getUserByID($uid);
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff Photo',
            'child-a' => route('mod.user.photo'),
            'sub'     => 'View Photo',
            'sub-a'   => route('mod.user.photo.view', array($id, $uid, $sitecode)),
            'icon'    => 'picture',
            'title'   => $user->name
        );
        // get photo info
        // --------------
        $photo = $this->user_repo->getUserPhotoByArr($id, $uid, $sitecode);

        // get history photo
        // -----------------
        $history = $this->user_repo->getUserPhotoAllByID($id, $uid);
        return view('modules.user.photo.view', compact('header', 'photo', 'history'));
    }

    // cancel photo
    // ------------
    public function postUserPhotoDetail(Requests\Mod\User\Photo\Update $request, $id, $uid, $sitecode) 
    {
        $save = $this->user_repo->getProcessPhoto($request->all(), $id, $uid, $sitecode);
        return redirect()->route('mod.user.photo.view', array($id, $uid, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    public function showUserPhotoRemark() {
        return view('modules.user.photo.remark');
    }

    // show other staff
    // ----------------
    public function showUserOther() {
        $index = $this->user_repo->getUserOther();
        return $index;
    }

    // search other user
    // -----------------
    public function postUserOther(Request $request) {
        $index = $this->user_repo->getUserOther($request);
        return $index;
    }

    // reset imei device
    // -----------------
    public function showUserResetImei($id, $token) {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff',
            'child-a' => route('mod.user.index'),
            'sub'     => 'Reset Device',
            'icon'    => 'reload',
            'title'   => 'Reset Device'
        );
        $user = $this->user_repo->getUserByIDToken($id, $token);
        return View('modules.user.reset', compact('header', 'user'));
    }

    // update imei device
    // ------------------
    public function postUserResetImei(Request $request, $id, $token) {
        $save = $this->user_repo->getUserUpdateImei($id, $token);
        return redirect()->route('mod.user.reset', array($id, $token))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);        
    }


}

