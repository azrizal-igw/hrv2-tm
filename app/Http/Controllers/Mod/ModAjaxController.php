<?php namespace IhrV2\Http\Controllers\Mod;

use Carbon\Carbon;
use IhrV2\Contracts\Attendance\AttendanceInterface as AttendanceRepository;
use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;

class ModAjaxController extends Controller {
    protected $user_repo;
    protected $leave_repo;
    protected $att_repo;

    public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo, AttendanceRepository $att_repo) {
        $this->middleware('auth');
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->att_repo = $att_repo;
    }

    // get announcement info
    // ---------------------
    public function getAjaxModAnnouncement() 
    {
        $ann = $this->leave_repo->getLatestAnnouncement(2)->toArray();
        $block = '';
        if (count($ann) > 0) {
            foreach ($ann as $i) {
                $block .= "<i class='icon-feed'></i>&nbsp;" . $i['message'] . "<br>";
            }
            $block .= "<a href='#'>All Announcement</a>";
        } 
        else {
            $block .= "No announcement.";
        }

        // return json
        // -----------
        $data = array('block' => $block);
        return response()->json($data);
    }

    // get staff info
    // --------------
    public function getAjaxModStaffInfo() 
    {
        // get site codes
        // --------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];

        // check total active and inactive
        // -------------------------------
        $staff = $this->user_repo->getUserStatusAll($codes);

        // get total positions of site supervisor
        // --------------------------------------
        $position = $this->user_repo->getUserTotalPosition($codes);

        // merge 2 queries
        // ---------------
        $result = array_merge($staff->toArray(), $position->toArray()); 
        return response()->json(array('block' => $result));
    }

    // get leave info
    // --------------
    public function getAjaxModLeaveInfo() 
    {
        // get codes
        // ---------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];

        // get current year
        // ----------------
        $year = date('Y');

        // get total apply leave by status
        // -------------------------------
        $status = $this->leave_repo->getLeaveTotalByStatus($codes, $year)->toArray();

        /// get total apply leave by type
        // ------------------------------
        $types = $this->leave_repo->getLeaveTotalByType($codes, $year)->toArray();
        return response()->json(array('status' => $status, 'types' => $types));
    }

    // get rl request info
    // -------------------
    public function getAjaxModRLRequest() 
    {
        // get codes
        // ---------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];

        // get current year
        // ----------------
        $year = date('Y');

        // get total RL by status
        // ----------------------
        $status = $this->leave_repo->getTotalRLByStatus($codes, $year)->toArray();
        return response()->json(array('block' => $status));
    }

    // get attendance info
    // -------------------
    public function getAjaxModAttendance() {

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $this->user_repo->getTotalSiteByStatus($codes)->toArray();

        // get current date
        // ----------------
        $date = date('Y-m-d');
        // $date = '2019-05-02';
        $block = [];
        $punch = $this->att_repo->getPunchByState($date);
        if (count($punch)) {
            foreach ($punch as $p) {
                $block[] = array('state' => $p->state, 'in' => $p->att_in, 'out' => $p->att_out);
            }
        }
        return response()->json($data = array('block' => $block));
    }    

    // get public holiday dates
    // ------------------------
    public function getAjaxModCalendar() 
    {
        $states = array();
        $year_month = date('Y-m');

        // get lists state of region
        // -------------------------
        if (session()->get('job')['region_id'] != 0) {
            $states = $this->leave_repo->getListStateOfRegion(session()->get('job')['region_id']);
        }

        // get public holiday
        // ------------------
        $ph = $this->leave_repo->getLeavePublicByRegion($states);
        $total_ph = count($ph);

        // display public holiday
        // ----------------------
        $block = "<i class='icon-calendar'></i>&nbsp;Public Holiday: <strong>" . $total_ph . "</strong><br>";

        // have record
        // -----------
        if (count($ph) > 0) {
            foreach ($ph as $p => $i) {

                // set public holiday info
                // -----------------------
                $ph_date = Carbon::parse($i->PublicNameByDate->date)->format('d M Y');
                $title = $ph_date . ' - ' . $i->PublicNameByDate->desc;
                $block .= '<i class="glyphicon glyphicon-stop black" title="' . $title . '"></i>&nbsp;';
            }
        } else {
            $block .= "No public holiday.";
        }

        // get off days
        // ------------
        $od = $this->leave_repo->getOffDayByRegion($states, date('Y'), date('m'))->toArray();
        $total_od = count($od);

        // display off days
        // ----------------
        $block .= "<br><i class='icon-ban'></i>&nbsp;Weekly Off Day: <strong>" . $total_od . "</strong><br>";
        if (count($od) > 0) {
            foreach ($od as $o) {
                $title = $od_date = $o['off_date'] . ' - ' . $o['off_type_name']['name'];
                $block .= '<i class="glyphicon glyphicon-stop grey" title="' . $title . '"></i>&nbsp;';
            }
        } else {
            $block .= "No off day.";
        }

        // return json
        // -----------
        return response()->json($data = array('block' => $block));
    }

}
