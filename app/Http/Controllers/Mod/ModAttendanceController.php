<?php namespace IhrV2\Http\Controllers\Mod;

use IhrV2\Contracts\Attendance\AttendanceInterface;
use IhrV2\Contracts\Attendance\DbAttendanceInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;
use Session;
use PDF;

class ModAttendanceController extends Controller {
    protected $user_repo;
    protected $leave_repo;
    protected $att_repo;
    protected $db_att_repo;

    public function __construct(UserInterface $user_repo, LeaveInterface $leave_repo, AttendanceInterface $att_repo, DbAttendanceInterface $db_att_repo) {
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->att_repo = $att_repo;
        $this->db_att_repo = $db_att_repo;
    }

    // show all sites
    // --------------
    public function showAttSite() {
        $index = $this->att_repo->getAttSite();
        return $index;
    }

    // search all sites
    // ----------------
    public function postAttSite(Request $request) {
        $index = $this->att_repo->getAttSite($request);
        return $index;
    }

    // show monthly attendance
    // -----------------------
    public function showAttUser($icno, $sitecode) 
    {
        $index = $this->att_repo->showAttUser($request = null, $icno, $sitecode);
        return $index;
    }

    // search monthly attendance
    // -------------------------
    public function postAttUser(Request $request, $icno, $sitecode) 
    {
        $index = $this->att_repo->showAttUser($request, $icno, $sitecode);
        return $index;
    }

    // view attendance user
    // --------------------
    public function showAttUserView($uid, $icno, $sitecode, $date)
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Site',
            'child-a' => route('mod.attendance.site'),
            'sub'     => 'Staff',
            'sub-a'   => route('mod.attendance.user', array($icno, $sitecode)),
            'cat1'    => 'View',
            'icon'    => 'clock',
            'title'   => 'View'
        );

        // get position id
        // ---------------
        $jobs = $this->user_repo->getUserJobByID($uid);
        $position_id = $jobs->position_id;

        // check attendance view
        // ---------------------
        $data = $this->att_repo->getAttViewDetail($uid, $icno, $sitecode, $position_id, $date);

        // get attendance status (training/tbu/incomplete)
        // -----------------------------------------------
        $ids = array(3, 4, 7, 8);
        $status = $this->att_repo->getAttendanceStatusArray($ids);
        return View('modules.attendance.view', compact('header', 'data', 'status'));
    }

    // post view attendance user
    // -------------------------
    public function postAttUserView(Requests\Mod\Attendance\Remark\Create $request, $uid, $icno, $sitecode, $date) 
    {
        // set array
        // ---------
        $arr = array('uid' => $uid, 'icno' => $icno, 'sitecode' => $sitecode, 'date' => $date);

        // set new remark
        // --------------
        if ($request->type == 0) {
            $save = $this->att_repo->getSetNewRemark($request->all(), $arr, $date);
        }

        // approve remark
        // --------------
        else if ($request->type == 2) {
            $save = $this->att_repo->getAttRemarkApprove($request->all(), $arr);
        }

        // edit notes
        // ----------
        else if ($request->type == 6) {
            $save = $this->att_repo->getAttRemarkEditNote($request->all(), $arr);
        }

        // cancel/reject remark
        // --------------------
        else if (in_array($request->type, array(4,5))) {
            $save = $this->att_repo->getAttRemarkCancel($request->all(), $arr);
            return redirect()->route('mod.attendance.user', array($icno, $sitecode))->with([
                'message' => $save[0],
                'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
            ]);
        }

        // invalid status
        // --------------
        else {
            $save = array('Invalid Status', 'danger');
        }

        // return message
        // --------------
        return redirect()->route('mod.attendance.user.view', array($uid, $icno, $sitecode, $date))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // view attendance punch in/out 
    // ----------------------------
    public function showAttUserViewPunch($type_id, $uid, $icno, $sitecode, $date)
    {
        // get attendance record
        // ---------------------
        $att = $this->att_repo->getAttendanceByPunch($type_id, $icno, $sitecode, $date);

        // check attendance mobile
        // -----------------------
        // $mobile = $this->att_repo->dfd();

        // check attendance manual
        // -----------------------
        // $manual = '';
        return View('attendance.includes.view.popup.punch', compact('date', 'type_id', 'att'));
    }

    // edit remark attendance
    // ----------------------
    public function showAttUserEdit($uid, $icno, $sitecode, $date)
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Site',
            'child-a' => route('mod.attendance.site'),
            'sub'     => 'Staff',
            'sub-a'   => route('mod.attendance.user', array($icno, $sitecode)),
            'cat1'    => 'View',
            'cat1-a'  => route('mod.attendance.user.view', array($uid, $icno, $sitecode, $date)),
            'cat2'    => 'Edit',
            'icon'    => 'note',
            'title'   => 'Edit'
        );

        // get remark
        // ----------
        $remark = $this->att_repo->getAttRemarkPending($uid, $icno, $sitecode, $date);

        // get list of remarks status
        // --------------------------
        $remark_status = $this->att_repo->getAttendanceStatusArray(array(3,4,7,8));
        return View('modules.attendance.edit', compact('header', 'remark', 'remark_status'));
    }

    // update remark attendance
    // ------------------------
    public function postAttUserEdit(Requests\Mod\Attendance\Remark\Update $request, $uid, $icno, $sitecode, $date) 
    {
        // set array
        // ---------
        $arr = array('uid' => $uid, 'icno' => $icno, 'sitecode' => $sitecode, 'date' => $request->date);

        // edit the remark
        // ---------------
        $save = $this->att_repo->getAttRemarkEdit($request->all(), $arr);

        // return message
        // --------------
        return redirect()->route('mod.attendance.user.view', array($uid, $icno, $sitecode, $date))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // view attendance remark
    // ----------------------
    public function showAttRemarkView($id, $uid, $icno, $sitecode) 
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Remark',
            'child-a' => route('mod.attendance.remark'),
            'sub'     => 'View',
            'icon'    => 'speech',
            'title'   => 'View'
        );

        // get remark id
        // -------------
        $remark_id = $id;

        // get position id
        // ---------------
        $jobs = $this->user_repo->getUserJobByID($uid);
        $position_id = $jobs->position_id;

        // check attendance view
        // ---------------------
        $data = $this->att_repo->getAttViewDetail2($id, $uid, $icno, $sitecode, $position_id);

        // get attendance status (training/tbu/incomplete)
        // -----------------------------------------------
        $ids = array(3, 4, 7, 8);
        $status = $this->att_repo->getAttendanceStatusArray($ids);
        return View('modules.attendance.remark.view', compact('header', 'data', 'status', 'remark_id'));
    }

    // approve/cancel/edit/reject attendance remark
    // --------------------------------------------
    public function postAttRemarkView(Requests\Mod\Attendance\Remark\Create $request, $id, $uid, $icno, $sitecode) 
    {
        // set array
        // ---------
        $arr = array('uid' => $uid, 'icno' => $icno, 'sitecode' => $sitecode, 'date' => $request->date);

        // approve remark
        // --------------
        if ($request->type == 2) {
            $save = $this->att_repo->getAttRemarkApprove($request->all(), $arr);
        }

        // edit notes
        // ----------
        else if ($request->type == 6) {
            $save = $this->att_repo->getAttRemarkEditNote($request->all(), $arr);
        }

        // cancel/reject remark
        // --------------------
        else if (in_array($request->type, array(4,5))) {
            $save = $this->att_repo->getAttRemarkCancel($request->all(), $arr);
            return redirect()->route('mod.attendance.remark')->with([
                'message' => $save[0],
                'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
            ]);
        }

        // invalid status
        // --------------
        else {
            $save = array('Invalid Status', 'danger');
        }

        // return message
        // --------------
        return redirect()->route('mod.attendance.remark.view', array($id, $uid, $icno, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // edit attendance remark
    // ----------------------
    public function showAttRemarkEdit($id, $uid, $icno, $sitecode) {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Remark',
            'child-a' => route('mod.attendance.remark'),
            'sub'     => 'View',
            'sub-a'   => route('mod.attendance.remark.view', array($id, $uid, $icno, $sitecode)),
            'cat1'    => 'Edit',
            'icon'    => 'note',
            'title'   => 'Edit'
        );

        // get remark
        // ----------
        $arr = array('id' => $id, 'uid' => $uid, 'icno' => $icno, 'sitecode' => $sitecode);
        $remark = $this->att_repo->getAttRemarkByCond($arr);

        // get list of remarks status
        // --------------------------
        $remark_status = $this->att_repo->getAttendanceStatusArray(array(3,4,7,8));
        return View('modules.attendance.remark.edit', compact('header', 'remark', 'remark_status'));
    }

    // update attendance remark
    // ------------------------
    public function postAttRemarkEdit(Requests\Mod\Attendance\Remark\Update $request, $id, $uid, $icno, $sitecode) 
    {
        // set array
        // ---------
        $arr = array('uid' => $uid, 'icno' => $icno, 'sitecode' => $sitecode, 'date' => $request->date);

        // edit the remark
        // ---------------
        $save = $this->att_repo->getAttRemarkEdit($request->all(), $arr);

        // return message
        // --------------
        return redirect()->route('mod.attendance.remark.view', array($id, $uid, $icno, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }
    
    // set multiple remark
    // -------------------
    public function showAttRemarkSet(Request $request, $icno, $sitecode) 
    {
        // check selected dates session
        // ----------------------------
        if (old('dates')) {

            // set new session
            // ---------------
            $dates = old('dates');

            // get user info
            // -------------
            $user = $this->user_repo->getUserByICSitecode($icno, $sitecode);
            $arr = array(
                'uid'      => $user->id,
                'icno'     => $user->icno,
                'sitecode' => $user->sitecode
            );

            // check if selected dates already have remark
            // -------------------------------------------
            $check = $this->att_repo->getCheckAttRemarkAll($arr, $dates);

            // none have remark yet
            // --------------------
            if ($check == 0) {

                // set header
                // ----------
                $header = array(
                    'parent'  => 'Attendance',
                    'child'   => 'Site',
                    'child-a' => route('mod.attendance.site'),
                    'sub'     => 'Staff',
                    'sub-a'   => route('mod.attendance.user', array($icno, $sitecode)),
                    'cat1'    => 'Remark',
                    'icon'    => 'note',
                    'title'   => 'Remark'
                );

                // get total dates
                // ---------------
                $total = count($dates);

                // get input values
                // ----------------
                $input = array('att_status' => old('att_status'), 'att_notes' => old('att_notes'));

                // get attendance status (training/tbu/incomplete)
                // -----------------------------------------------
                $ids = array(3, 4, 7, 8);
                $status = $this->att_repo->getAttendanceStatusArray($ids);
                return view('modules.attendance.remark.set', compact('header', 'dates', 'total', 'status', 'input', 'icno', 'sitecode'));
            }

            // one or more selected date alredy have remark
            // --------------------------------------------
            else {
                return redirect()->route('mod.attendance.user', array($icno, $sitecode))->with([
                    'message' => 'Selected date already have remark. Please select again.',
                    'label'   => 'alert alert-danger alert-dismissible'
                ]);
            }
        }

        // not has session dates
        // ---------------------
        else {
            return redirect()->route('mod.attendance.user', array($icno, $sitecode))->withInput();
        }
    }

    // save multiple remark
    // --------------------
    public function postAttRemarkSet(Requests\Mod\Attendance\Remark\Multiple $request, $icno, $sitecode) 
    {
        // get user info
        // -------------
        $user = $this->user_repo->getUserByICSitecode($icno, $sitecode);
        $arr = array(
            'uid'      => $user->id,
            'icno'     => $user->icno,
            'sitecode' => $user->sitecode
        );

        // get selected date
        // -----------------
        $dates = $request->session()->get('dates');

        // process attendance remark
        // -------------------------
        $save = $this->att_repo->getApplyMultipleRemark($request->all(), $arr, $dates);

        // return success
        // --------------
        return redirect()->route('mod.attendance.user', array($icno, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // create attendance manual
    // ------------------------
    public function showAttManualCreate() 
    {
        $header = array(
            'parent' => 'Attendance',
            'child'  => 'Manual',
            'child-a' => route('mod.attendance.manual'),
            'sub'    => 'Add',
            'icon'   => 'plus',
            'title'  => 'Add'
        );
        return view('modules.attendance.manual.create', compact('header'));
    }

    // save attendance manual
    // ----------------------
    public function postAttManualCreate(Requests\Mod\Attendance\Manual\Create $request) 
    {
        // check user
        // ----------
        $user = $this->user_repo->getUserSite($request->icno, $request->sitecode);

        // icno and sitecode exist
        // -----------------------
        if (!empty($user)) {
            $save = $this->att_repo->getAttendanceManualAdd($request->all(), auth()->user()->id, $user);
        } 

        // record not found
        // ----------------
        else {
            $save = array('Staff is not exist.', 'danger');
        }

        // redirect to same page
        // ---------------------
        return redirect()->route('mod.attendance.manual.create')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // view all attendance manual
    // --------------------------
    public function showAttManual() {
        $index = $this->att_repo->getAttendanceManual();
        return $index;
    }

    // search attendance manual
    // ------------------------
    public function postAttManual(Request $request) 
    {
        $index = $this->att_repo->getAttendanceManual($request);
        return $index;
    }

    // view attendance manual
    // ----------------------
    public function showAttManualView($id, $icno, $sitecode) 
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Manual',
            'child-a' => route('mod.attendance.manual'),
            'sub'     => 'View',
            'sub-a'   => route('mod.attendance.manual.view', array($id, $icno, $sitecode)),
            'icon'    => 'info',
            'title'   => 'View'
        );
        $manual = $this->att_repo->getAttendanceManualOne($id, $icno, $sitecode);
        return view('modules.attendance.manual.view', compact('header', 'manual'));
    }

    // cancel attendance manual
    // ------------------------
    public function postAttManualView(Request $request) {
        $cancel = $this->att_repo->getAttManualCancel($request->all());
        return redirect()->route('mod.attendance.manual')->with([
            'message' => $cancel[1],
            'label'   => 'alert alert-'.$cancel[0].' alert-dismissible'
        ]);
    }

    // display attendance remark
    // -------------------------
    public function showAttRemark() {
        $index = $this->att_repo->getAttRemark();
        return $index;
    }

    // search attendance remark
    // ------------------------
    public function postAttRemark(Request $request) {
        $index = $this->att_repo->getAttRemark($request);
        return $index;
    }

    // get lists of time slip
    // ----------------------
    public function getTimeSlipIndex() {
        $index = $this->att_repo->getTimeSlipIndex();
        return $index;
    }

    // search time slip
    // ----------------
    public function postTimeSlipIndex(Request $request) {
        $index = $this->att_repo->getTimeSlipIndex($request);
        return $index;
    }

    // view time slip
    // --------------
    public function getTimeSlipView($id, $uid, $sitecode) {
        $timeslip = $this->att_repo->getAttTimeSlipInfo($id, $uid, $sitecode);
        return View('modules.attendance.timeslip.view', compact('timeslip'));
    }

    // cancel time slip
    // ----------------
    public function postTimeSlipView(Request $request, $id, $uid, $sitecode) 
    {
        $save = $this->att_repo->getTimeSlipCancel($id, $uid, $sitecode);
        return redirect()->route('mod.attendance.timeslip')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);        
    }

    // lists attendance mobile
    // -----------------------
    public function getAttMobileIndex() {
        $index = $this->att_repo->getAttMobileIndex();
        return $index;
    }

    // search attendance mobile
    // ------------------------
    public function postAttMobileIndex(Request $request) {
        $index = $this->att_repo->getAttMobileIndex($request);
        return $index;
    }   

    // view attendance mobile
    // ----------------------
    public function getAttMobileView($id, $uid, $sitecode) 
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Mobile',
            'child-a' => route('mod.attendance.mobile'),
            'sub'     => 'View',
            'icon'    => 'info',
            'title'   => 'View'
        );
        $mobile = $this->att_repo->getAttMobileByUser($id, $uid, $sitecode);
        return view('modules.attendance.mobile.view', compact('header', 'mobile'));        
    } 

    // cancel attendance mobile
    // ------------------------
    public function postAttMobileView(Request $request)
    {
        $cancel = $this->att_repo->getAttMobileCancel($request->all());
        return redirect()->route('mod.attendance.mobile')->with([
            'message' => $cancel[0],
            'label'   => 'alert alert-' . $cancel[1] . ' alert-dismissible'
        ]); 
    }

    // get app version
    // ---------------
    public function getAppVersion()
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Mobile',
            'child-a' => route('mod.attendance.mobile'),
            'sub'     => 'Version',
            'icon'    => 'info',
            'title'   => 'Version'
        );
        $version = $this->att_repo->getListAppVersion();
        return view('modules.attendance.mobile.version.index', compact('header', 'version'));
    }

    // save app version
    // ----------------
    public function postAppVersion(Requests\Mod\Attendance\Mobile\Version\Create $request)
    {
        $save = $this->att_repo->getSaveAppVersion($request->all());
        return redirect()->route('mod.attendance.mobile.version')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);         
    }

    // edit app version
    // ----------------
    public function editAppVersion($id)
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Mobile',
            'child-a' => route('mod.attendance.mobile'),
            'sub'     => 'Version',
            'sub-a'   => route('mod.attendance.mobile.version'),
            'cat1'    => 'Edit',
            'icon'    => 'note',
            'title'   => 'Edit'
        );
        $data = $this->att_repo->getAppVersionByID($id);
        return view('modules.attendance.mobile.version.edit', compact('header', 'data'));
    }

    // update app version
    // ------------------
    public function updateAppVersion(Requests\Mod\Attendance\Mobile\Version\Edit $request, $id)
    {
        $save = $this->att_repo->getUpdateAppVersion($request->all(), $id);
        return redirect()->route('mod.attendance.mobile.version')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);  
    }


}


