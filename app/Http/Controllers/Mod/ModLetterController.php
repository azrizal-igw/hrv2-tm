<?php namespace IhrV2\Http\Controllers\Mod;

use IhrV2\Contracts\Letter\LetterInterface as LetterRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;

class ModLetterController extends Controller {
    protected $letter_repo;
    protected $user_repo;

    public function __construct(LetterRepository $letter_repo, UserRepository $user_repo) {
        $this->letter_repo = $letter_repo;
        $this->user_repo = $user_repo;
    }

    // lists of all letter
    // -------------------
    public function showLetterIndex() {
        $index = $this->letter_repo->getLetterIndex();
        return $index;
    }

    // search letter
    // -------------
    public function postLetterIndex(Request $request) {
        $index = $this->letter_repo->getLetterIndex($request);
        return $index;
    }

    // set user letter
    // ---------------
    public function showLetterUser($uid, $sitecode) {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff',
            'child-a' => route('mod.user.index'),
            'sub'     => 'Letter',
            'icon'    => 'envelope',
            'title'   => 'Letter'
        );
        $info = $this->letter_repo->getUserInfo($uid, $sitecode);

        // get letters
        // -----------
        $letters = $this->letter_repo->getLetterAll($uid, $sitecode);

        // get letter types in drop down lists
        // -----------------------------------
        $letter_types = $this->letter_repo->getLetterTypeDrop();

        // letter types
        // ------------
        $types = $this->letter_repo->getLetterTypeList();

        // get reminder lists
        // ------------------
        $no = $this->letter_repo->getListReminderNo();
        return view('modules.letter.user.index', compact('header', 'info', 'letters', 'letter_types', 'types', 'no'));
    }

    // upload user letter
    // ------------------
    public function postLetterUser(Requests\Mod\Letter\Create $request, $uid, $sitecode) 
    {
        $save = $this->letter_repo->getProcessNewLetter($request->all(), $uid, $sitecode);
        return redirect()->route('mod.letter.user.index', array($uid, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // view user letter
    // ----------------
    public function showLetterUserView($id, $uid, $sitecode) 
    {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff',
            'child-a' => route('mod.user.index'),
            'sub'     => 'Letter',
            'sub-a'   => route('mod.letter.user.index', array($uid, $sitecode)),
            'cat1'    => 'View',
            'icon'    => 'envelope-open',
            'title'   => 'View'
        );

        // get letter info
        // ---------------
        $letter = $this->letter_repo->getLetterCheck($id, $uid, $sitecode);
        return view('modules.letter.user.view', compact('header', 'letter'));
    }

    // cancel user letter
    // ------------------
    public function postLetterUserView(Request $request, $id, $uid, $sitecode) 
    {
        // update letter
        // -------------
        $save = $this->letter_repo->getLetterCancel($id, $uid, $sitecode);
        
        // return message
        // --------------
        return redirect()->route('mod.letter.user.index', array($uid, $sitecode))->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

}
