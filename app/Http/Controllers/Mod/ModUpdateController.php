<?php

namespace IhrV2\Http\Controllers\Mod;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;

class ModUpdateController extends Controller
{
    // update attendance status of attendance manual
    // ---------------------------------------------
    public function getUpdateAttStatus() {
    	$total = 0;
    	$update = 0;    	
    	$q = \IhrV2\Models\Attendance::where(['att_sys_status' => 'IHRV2'])->get();
    	if (!empty($q)) {
    		$total = count($q);    		
    		foreach ($q as $i) {    			
    			$status = $i->ip_add.', '.$i->att_mykad_ic.', IHRV2, Manual';
        		\IhrV2\Models\Attendance::where(['att_log_id' => $i->att_log_id])->update(array('att_sys_status' => $status));
        		$update++;
    		}
    	}
    	return response()->json(['total' => $total, 'update' => $update, 'message' => 'success']);
    }

    // update remark of attendance manual
    // ----------------------------------
    public function getUpdateAttManualRemark() {
    	$total = 0;
    	$update = 0;    	
    	$q = \IhrV2\Models\AttendanceManual::where('remark', '=', null)->get();
    	if (!empty($q)) {
    		$total = count($q);    		
    		foreach ($q as $i) {    
    			if (!empty($i->AttendanceDetail)) {
	    			$remark = $i->AttendanceDetail->att_userstamp;			
	        		\IhrV2\Models\AttendanceManual::find($i->id)->update(array('remark' => $remark));
	        		$update++;    				
    			}
    		}
    	}
    	return response()->json(['total' => $total, 'update' => $update, 'message' => 'success']);    	  	
    }

    // update att_sys_status of mobile attendance
    // ------------------------------------------
    public function getUpdateAttStatusMobile() {
        // dd(0);
        $total = 0;
        $update_status = 0;
        $update_remark = 0;        
        $q = \IhrV2\Models\Attendance::where('att_sys_status', 'like', '%Mobile%')->with(array('UserAttendanceGps'))->get();
        if (!empty($q)) {
            $total = count($q);         
            foreach ($q as $i) {

                // update 1st delimiter
                // |QRCode: 11/07/2018 08:32:11#Server#B01C001|, Actual, Mobile, Direct
                $exp = explode('|, ', $i->att_sys_status);

                if (strpos($exp[0], 'Location') !== false) {
                    $name = 'Location';
                }
                else {
                    $name = 'QRCode';
                }



                $status = $name.', '.$exp[1];
                \IhrV2\Models\Attendance::where(['att_log_id' => $i->att_log_id])->update(array('att_sys_status' => $status));
                $update_status++;

                // have remark
                if (!empty($i->att_userstamp)) {
                    // dd($i->att_userstamp);


                    // update remark at attendance_gps
                    if (!empty($i->UserAttendanceGps)) {
                        // dd('ada');

                        // check if remark is null
                        if (empty($i->UserAttendanceGps->remark)) {
                            // dd('ada lagi');

                            // update remark at attendance_gps
                            \IhrV2\Models\AttendanceGps::where(['att_log_id' => $i->att_log_id])->update(array('remark' => $i->att_userstamp));
                            $update_remark++;
                        }
                    }  
                }
            }
        }
        return response()->json(['total' => $total, 'update_status' => $update_status, 'update_remark' => $update_remark, 'message' => 'success']);        
    }

    // update user_id at attendance_manual
    // -----------------------------------
    public function getUpdateAttManualUserID() {
        $total = 0;
        $update = 0;          
        $q = \IhrV2\Models\AttendanceManual::where('user_id', '=', null)->get();
        if (!empty($q)) {
            $total = count($q);
            foreach ($q as $i) {
                $user = \IhrV2\User::where(['icno' => $i->icno, 'sitecode' => $i->sitecode])->first();
                if (!empty($user)) {
                    \IhrV2\Models\AttendanceManual::where(['id' => $i->id])->update(['user_id' => $user->id]);
                    $update++;
                }
            }
        }
        return response()->json(['total' => $total, 'update' => $update, 'message' => 'success']);                        
    }

    // remove record attendance_log_records when cancel attendance manual
    // ------------------------------------------------------------------
    public function getDeleteAttManualCancel() {
        $total = 0;
        $delete = 0;
        $q = \IhrV2\Models\AttendanceManual::where(['active' => 2])->get();
        if (!empty($q)) {
            foreach ($q as $i) {
                $att = \IhrV2\Models\Attendance::where(['att_log_id' => $i->att_id])->first();
                if (!empty($att)) {
                    \IhrV2\Models\Attendance::where(['att_log_id' => $i->att_id])->delete();
                    $delete++;
                }
            }
        }
        return response()->json(['total' => $total, 'delete' => $delete, 'message' => 'success']);                                
    }

    // update sitecode of attendance gps
    // ---------------------------------
    public function getUpdateAttGpsSitecode() {
        $total = 0;
        $update = 0;
        $q = \IhrV2\Models\AttendanceGps::where('sitecode', '=', null)->with(array('AttGpsUser'))->get();
        if (!empty($q)) {
            foreach ($q as $i) {
                if (!empty($i->AttGpsUser) && !empty($i->AttGpsUser->sitecode)) {
                    \IhrV2\Models\AttendanceGps::find($i->id)->update(array('sitecode' => $i->AttGpsUser->sitecode));
                    $update++;
                }
            }
        }
        return response()->json(['total' => $total, 'update' => $update, 'message' => 'success']);  
    }

    // update transfer of attendance gps
    // ---------------------------------
    public function getUpdateAttGpsTransfer() {
        $total = 0;
        $update = 0;
        $q = \IhrV2\Models\Attendance::where('att_sys_status', 'Location, Manual, Mobile, Indirect')
        ->with(array('UserAttendanceGps'))
        ->get();
        if (!empty($q)) {
            foreach ($q as $i) {
                if (!empty($i->UserAttendanceGps)) {
                    if ($i->UserAttendanceGps->transfer != 2) {
                        $id = $i->UserAttendanceGps->id;
                        \IhrV2\Models\AttendanceGps::find($id)->update(array('transfer' => 2));
                        $update++;                        
                    }
                }
            }
        }
        return response()->json(['total' => $total, 'update' => $update, 'message' => 'success']);  
    }



}




