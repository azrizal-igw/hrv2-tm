<?php

namespace IhrV2\Http\Controllers\Mod;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;
use Carbon\Carbon;
use IhrV2\Contracts\Email\EmailInterface as EmailRepository;
use IhrV2\Contracts\Erp\ErpInterface as ErpRepository;

class ModSyncController extends Controller
{    
    protected $email_repo;
    protected $erp_repo;

    public function __construct(EmailRepository $email_repo, ErpRepository $erp_repo) {
        $this->email_repo = $email_repo;
        $this->erp_repo = $erp_repo;
    }
    
    // sync staff/contract
    // -------------------
    public function getSyncStaff() 
    {
        $header = array(
            'parent'  => 'Sync',
            'child'   => 'Staff',
            'child-a' => route('mod.sync.staff'),
            'icon'    => 'people',
            'title'   => 'Staff'
        );
        $lists = array(
            '' => '[Process]',
            1  => 'Staff',
            2  => 'Contract',
        );
        return view('modules.sync.staff.index', compact('header', 'lists'));
    }

    // process sync staff/contract
    // ---------------------------
    public function postSyncStaff(Requests\Mod\Erp\Sync\Staff\Process $request) 
    {
        // get input value
        // ---------------
        $record = ($request->record) ? $request->record : 0;
        $skip = ($request->skip) ? $request->skip : 0;

        // sync staff
        // ----------
        if ($request->type_id == 1) {
            $decode = $this->erp_repo->getSyncStaffActive(3, $record, $skip);
            $text = "Process: <strong>Sync Staff</strong><br>";
            $text .= "Start: " . $decode['start'] . "<br>";
            $text .= "End: " . Carbon::now() . "<br>";
            $text .= "Fail: " . $decode['fail'] . "<br>";
            $text .= "New Staff: " . $decode['insert_user'] . "<br>";
            $text .= "Update Staff: " . $decode['update_user'] . "<br>";
            $text .= "New Job: " . $decode['insert_job'] . "<br>";
            $text .= "Update Job: " . $decode['update_job'] . "<br>";
            $text .= "New Contract: " . $decode['insert_contract'] . "<br>";
            $text .= "Update Contract: " . $decode['update_contract'];
            $msg = array('Sync is completed!.', 'success');
        }

        // sync staff contract
        // -------------------
        else if ($request->type_id == 2) {
            $decode = $this->erp_repo->getSyncStaffContract($record, $skip);
            $text = "Process: <strong>Sync Contract</strong><br>";
            $text .= "Start: " . $decode['start'] . "<br>";
            $text .= "End: " . Carbon::now() . "<br>";
            $text .= "Total: " . $decode['total'] . "<br>";
            $text .= "Insert: " . $decode['insert'] . "<br>";
            $text .= "Fail: " . $decode['fail'];
            $msg = array('Sync is completed!.', 'success');            
        }

        // invalid process
        // ---------------
        else {
            $text = 'Invalid Process.';
            $msg = array($text, 'danger');
        }

        // return result
        // -------------
        return redirect()->route('mod.sync.staff')->with([
            'text'    => $text,
            'message' => $msg[0],
            'label'   => 'alert alert-'.$msg[1].' alert-dismissible'
        ]);
    }

    // sync site
    // ---------
    public function getSyncSite() 
    {
        $header = array(
            'parent'  => 'Sync',
            'child'   => 'Site',
            'child-a' => route('mod.site.index'),
            'icon'    => 'share',
            'title'   => 'Site'
        );
        return view('modules.sync.site.index', compact('header'));
    }

    // process sync site
    // -----------------
    public function postSyncSite(Request $request) 
    {
        // call sync site api
        // ------------------
        $decode = $this->erp_repo->getSyncSite();

        // set text
        // --------
        $text = "Start Process: " . $decode['start'] . "<br>";
        $text .= "End Process: " . Carbon::now() . "<br>";
        $text .= "Total: " . $decode['total'] . "<br>";
        $text .= "Insert: " . $decode['insert'] . "<br>";
        $text .= "Update: " . $decode['update'] . "<br>";
        $text .= "Fail: " . $decode['fail'];

        // return message
        // --------------
        return redirect()->route('mod.sync.site')->with([
            'text'    => $text,
            'message' => 'Sync is completed!',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }

    // sync public holiday
    // -------------------
    public function getSyncPublicHoliday() 
    {
        $header = array(
            'parent'  => 'Sync',
            'child'   => 'Public Holiday',
            'child-a' => route('mod.public-holiday.index'),
            'icon'    => 'event',
            'title'   => 'Public Holiday'
        );
        return view('modules.sync.public-holiday.index', compact('header'));
    }

    // process sync public holiday
    // ---------------------------
    public function postSyncPublicHoliday(Requests\Mod\Erp\Sync\PublicHoliday\Process $request) 
    {
        // call sync public holiday api
        // ----------------------------
        $decode = $this->erp_repo->getSyncPublicHoliday($request->year);

        // set text
        // --------
        $text = "Start Process: " . $decode['start'] . "<br>";
        $text .= "End Process: " . Carbon::now() . "<br>";
        $text .= "Year: " . $decode['year'] . "<br>";
        $text .= "Total: " . $decode['total'] . "<br>";
        $text .= "Insert: " . $decode['insert'] . "<br>";
        $text .= "Fail: " . $decode['fail'];

        // return message
        // --------------
        return redirect()->route('mod.sync.public-holiday')->with([
            'text'    => $text,
            'message' => 'Sync is completed!',
            'label'   => 'alert alert-success alert-dismissible'
        ]);
    }


    
}
