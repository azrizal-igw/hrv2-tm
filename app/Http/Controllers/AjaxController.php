<?php namespace IhrV2\Http\Controllers;

use Carbon\Carbon;
use IhrV2\Contracts\Attendance\AttendanceInterface as AttendanceRepository;
use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;

class AjaxController extends Controller
{
    protected $user_repo;
    protected $leave_repo;
    protected $att_repo;

    /**
     * [__construct description]
     * @param UserRepository       $user_repo  [description]
     * @param LeaveRepository      $leave_repo [description]
     * @param AttendanceRepository $att_repo   [description]
     */
    public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo, AttendanceRepository $att_repo)
    {
        $this->middleware('auth');
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->att_repo = $att_repo;
    }

    /**
     * get announcement info
     * @return [type] [description]
     */
    public function getAjaxAnnouncement()
    {
        $block = '';

        // manager/assistant manager/it supervisor
        // ---------------------------------------
        if (auth()->user()->group_id == 3 && in_array(session()->get('job')['position_id'], array(4, 5, 6))) {

            // contract not expired
            // --------------------
            if (session()->get('expired') != 1) {

                // get announcement
                // ----------------
                $ann = $this->leave_repo->getLatestAnnouncement(1);
                $total = 0;
                if (count($ann) > 0) {
                    $total = count($ann);
                    foreach ($ann as $p => $i) {
                        $block .= "<i class='icon-feed'></i>&nbsp;" . $i->message . "<br>";
                    }
                } else {
                    $block .= "No announcement.";
                }
            }

            // contract is not exist or expired
            // --------------------------------
            else {
                $block .= "No Contract/Expired.";
            }
        } else {
            $block .= "Not Applicable.";
        }

        // return json
        // -----------
        $data = array('block' => $block);
        return response()->json($data);
    }

    /**
     * [getAjaxStaffApplication description]
     * @return [type] [description]
     */
    public function getAjaxStaffApplication()
    {
        $block = '';

        // manager/assistant manager/it supervisor
        // ---------------------------------------
        if (auth()->user()->group_id == 3 && in_array(session()->get('job')['position_id'], array(4, 5, 6))) {

            // contract not expired
            // --------------------
            if (session()->get('expired') != 1) {

                // get contract info
                // -----------------
                $contract = session()->get('contract');
                $date_from = $contract->date_from;
                $date_to = $contract->date_to;

                // get contract date
                // -----------------
                $from = Carbon::parse($date_from)->format('d M Y');
                $to = Carbon::parse($date_to)->format('d M Y');

                $block = "<i class='icon-briefcase'></i>&nbsp;Contract: <strong>" . $from . "</strong> to <strong>" . $to . '</strong><br>';
                $block .= "<i class='icon-credit-card'></i>&nbsp;IC No: " . auth()->user()->icno . "<br>";
                $block .= "<i class='icon-phone'></i>&nbsp;Hp No: " . auth()->user()->hpno . "<br>";
                $block .= "<i class='icon-envelope'></i>&nbsp;Email: " . auth()->user()->email . "<br>";
                $block .= "<i class='icon-event'></i>&nbsp;Join Date: <strong>" . Carbon::parse(session()->get('job')['join_date'])->format('d M Y') . "</strong>";
            }

            // contract is not exist or expired
            // --------------------------------
            else {
                $block .= "No Contract/Expired.";
            }
        } else {
            $block .= "Not Applicable.";
        }

        // return json
        // -----------
        $data = array('block' => $block);
        return response()->json($data);
    }

    /**
     * [getAjaxLeaveApplication description]
     * @return [type] [description]
     */
    public function getAjaxLeaveApplication()
    {
        $block = '';

        // manager/assistant manager/it supervisor
        // ---------------------------------------
        if (auth()->user()->group_id == 3 && in_array(session()->get('job')['position_id'], array(4, 5, 6))) {

            // get user_id & sitecode
            // ----------------------
            $uid = auth()->user()->id;
            $sitecode = auth()->user()->sitecode;

            // contract not expired
            // --------------------
            if (session()->get('expired') != 1) {
                $date_from = session()->get('contract')['date_from'];
                $date_to = session()->get('contract')['date_to'];

                // get total entitled
                // ------------------
                $al_entitled = $this->leave_repo->getTotalALByJoinDate($uid, $sitecode, $date_from, $date_to);
                $entitled = $al_entitled['total'];

                // check reimburse/deduct leave
                // ----------------------------
                $rd = $this->leave_repo->getCheckReimburseDeduct($uid, 1, $date_from, $date_to);
                $reimburse = $rd['reimburse'];
                $deduct = $rd['deduct'];

                // calculate total leave
                // ---------------------
                $final_entitled = $this->leave_repo->getCalculateTotalExtra($entitled, $reimburse, $deduct);

                // check total taken for AL dan UL
                // -------------------------------
                $annual = 0;
                $unplan = 0;
                $check_taken = $this->leave_repo->getLeaveTakenByType($uid, $sitecode, array(1, 13), $date_from, $date_to);

                // have taken
                // ----------
                if (!empty($check_taken)) {
                    foreach ($check_taken as $taken) {

                        // get total annual
                        // ----------------
                        if ($taken['leave_type_id'] == 1) {
                            $annual = $taken['total'];
                        }

                        // get total unplan
                        // ----------------
                        if ($taken['leave_type_id'] == 13) {
                            $unplan = $taken['total'];
                        }
                    }
                }

                // calculate annual and unplan
                // ---------------------------
                $final_taken = $annual + $unplan;

                // get total balance
                // -----------------
                $final_balance = $this->leave_repo->getCalculateBalance($final_entitled, $final_taken);

                // display block
                // -------------
                $block .= '<i class="icon-grid"></i>&nbsp;Annual Leave<br>';
                $block .= 'Entitled:&nbsp;<strong>' . round($final_entitled, 1) . '</strong>&nbsp;Taken:&nbsp;<strong>' . round($final_taken, 1) . '</strong>&nbsp;Balance:&nbsp;<strong>' . round($final_balance, 1) . '</strong><br>';
                $block .= '<i class="icon-info"></i>&nbsp;All Leave Status (Per Leave)<br>';

                // get all leaves
                // --------------
                $cond = array($uid, $sitecode, $date_from, $date_to);
                $leaves = $this->leave_repo->getLeaveStatusTotal($cond);
                // dd($leaves);
                if (count($leaves) > 0) {
                    foreach ($leaves as $i) {
                        if ($i->status == 1) {
                            // pending
                            $color = 'red-light';
                        } elseif ($i->status == 2) {
                            // approved
                            $color = 'blue';
                        } elseif ($i->status == 3) {
                            // rejected
                            $color = 'maroon-light';
                        } elseif ($i->status == 4) {
                            // apply cancel
                            $color = 'brown-light';
                        } elseif ($i->status == 5) {
                            // cancel
                            $color = 'grey-light';
                        } elseif ($i->status == 6) {
                            // approved cancel
                            $color = 'green-light';
                        } elseif ($i->status == 7) {
                            // rejected cancel
                            $color = 'green-dark';
                        } else {
                            $color = 'black';
                        }
                        $block .= '<i class="glyphicon glyphicon-stop ' . $color . '" title="' . $i->LeaveStatusName->name . '"></i>&nbsp;<strong>' . $i->total . '</strong> ';
                    }
                } else {
                    $block .= 'No new request.';
                }
                $block .= '<br><a href="' . route('sv.leave.summary') . '">Summary</a>&nbsp;|&nbsp;<a href="' . route('sv.leave.index') . '">All Leave</a>&nbsp;|&nbsp;<a href="' . route('sv.leave.create') . '">Add Leave</a>';
            }

            // contract is not exist or expired
            // --------------------------------
            else {
                $block .= "No Contract/Expired.";
            }
        } else {
            $block .= "Not Applicable.";
        }

        // return json
        // -----------
        $data = array('block' => $block);
        return response()->json($data);
    }

    /**
     * [getAjaxRLRequest description]
     * @return [type] [description]
     */
    public function getAjaxRLRequest()
    {
        $block = '';

        // manager/assistant manager/it supervisor
        // ---------------------------------------
        if (auth()->user()->group_id == 3 && in_array(session()->get('job')['position_id'], array(4, 5, 6))) {

            // get user_id
            // -----------
            $uid = auth()->user()->id;
            $sitecode = auth()->user()->sitecode;

            // contract not expired
            // --------------------
            if (session()->get('expired') != 1) {

                // get contract info
                // -----------------
                $contract = session()->get('contract');
                $contract_id = $contract->id;
                $date_from = $contract->date_from;
                $date_to = $contract->date_to;

                // get total entitled
                // ------------------
                $ent = $this->leave_repo->getLeaveRepEntitled($uid, $sitecode, $contract_id, $date_from, $date_to);
                $entitled = ($ent) ? $ent->total : 0;

                // check reimburse/deduct leave
                // ----------------------------
                $rd = $this->leave_repo->getCheckReimburseDeduct($uid, 6, $date_from, $date_to);
                $reimburse = $rd['reimburse'];
                $deduct = $rd['deduct'];

                // calculate entitled and reimburse/deduct
                // ---------------------------------------
                $final_entitled = $this->leave_repo->getCalculateTotalExtra($entitled, $reimburse, $deduct);

                // get total taken
                // ---------------
                $taken = $this->leave_repo->getLeaveTaken($uid, $sitecode, 6, $date_from, $date_to);
                $leave_taken = ($taken) ? $taken->total : 0;

                // calculate balance
                // -----------------
                $leave_balance = $this->leave_repo->getCalculateBalance($final_entitled, $leave_taken);

                // display color if more than 0
                // ----------------------------
                if ($leave_balance > 0) {
                    $span = 'label label-pill label-info';
                } else {
                    $span = '';
                }

                // display block
                // -------------
                $block .= '<i class="icon-plus"></i>&nbsp;Balance Entitled:&nbsp;<strong><span class="' . $span . '">' . $leave_balance . '</span></strong><br>';
                $block .= '<i class="icon-info"></i>&nbsp;Request Status&nbsp;(Per Request)<br>';

                // get request status
                // ------------------
                $q = $this->leave_repo->getLeaveRepStatusTotal($uid, $sitecode, $date_from, $date_to);
                if (count($q) > 0) {
                    foreach ($q as $i) {
                        if ($i->status == 1) {
                            // pending
                            $color = 'red-light';
                        } elseif ($i->status == 2) {
                            // approved
                            $color = 'blue';
                        } elseif ($i->status == 3) {
                            // rejected
                            $color = 'maroon-light';
                        } elseif ($i->status == 4) {
                            // apply cancel
                            $color = 'brown-light';
                        } elseif ($i->status == 5) {
                            // cancel
                            $color = 'grey-light';
                        } elseif ($i->status == 6) {
                            // approved cancel
                            $color = 'green-light';
                        } elseif ($i->status == 7) {
                            // rejected cancel
                            $color = 'green-dark';
                        } else {
                            $color = 'black';
                        }
                        $block .= '<i class="glyphicon glyphicon-stop ' . $color . '" title="' . $i->LeaveRepStatusName->name . '"></i>&nbsp;<strong>' . $i->total . '</strong> ';
                        // $block .= $i->LeaveRepStatusName->name.': <strong>'.$i->total.'</strong>&nbsp;';
                    }
                } else {
                    $block .= 'No new request.';
                }
                $block .= '<br><a href="' . route('sv.leave.replacement.create') . '">Add Request</a>&nbsp;|&nbsp;<a href="' . route('sv.leave.replacement.index') . '">All Request</a>';
            }

            // contract is not exist or expired
            // --------------------------------
            else {
                $block .= "No Contract/Expired.";
            }
        } else {
            $block .= "Not Applicable.";
        }

        // return json
        // -----------
        $data = array('block' => $block);
        return response()->json($data);
    }

    /**
     * [getAjaxAttendance description]
     * @return [type] [description]
     */
    public function getAjaxAttendance()
    {
        $block = '';

        // manager/assistant manager/it supervisor
        // ---------------------------------------
        if (auth()->user()->group_id == 3 && in_array(session()->get('job')['position_id'], array(4, 5, 6))) {

            // contract not expired
            // --------------------
            if (session()->get('expired') != 1) {

                // get icno and sitecode
                // ---------------------
                $icno = auth()->user()->icno;
                $sitecode = auth()->user()->sitecode;

                // get today and yesterday date
                // ----------------------------
                $today = date('Y-m-d');
                $yesterday = Carbon::yesterday()->format('Y-m-d');
                $year = date('Y');

                // get site info
                // -------------
                $status_site = 0;
                $chk_site = $this->leave_repo->getUserStateID($sitecode)->toArray();

                // check site
                // ----------
                if (!empty($chk_site)) {
                    $status_site = 1;
                }

                // site exist
                // ----------
                if ($status_site == 1) {

                    // get region id
                    // -------------
                    $region_id = $chk_site['region_id'];

                    // get fasting date
                    // ----------------
                    $fasting = $this->att_repo->getFastingDate($chk_site['state_id'], $year);

                    // get time limit
                    // --------------
                    $cond['fasting'] = $fasting;
                    $limit_today = $this->att_repo->getAttendanceLimitOne($today, $region_id, $cond);
                    $limit_yest = $this->att_repo->getAttendanceLimitOne($yesterday, $region_id, $cond);

                    // get today time
                    // --------------
                    $chk_today = $this->att_repo->getAttendanceByDate($icno, $sitecode, $today);
                    $to = $this->att_repo->getAttendanceTimeInOut($chk_today, $limit_today);

                    // get yesterday time
                    // ------------------
                    $chk_yest = $this->att_repo->getAttendanceByDate($icno, $sitecode, $yesterday);
                    $yest = $this->att_repo->getAttendanceTimeInOut($chk_yest, $limit_yest);

                    // get current month and year
                    // --------------------------
                    $month_year = array('month' => date('m'), 'year' => date('Y'));

                    // get position id
                    // ---------------
                    $position_id = session()->get('job')['position_id'];

                    // get total time (all & mmcmc)
                    // ----------------------------
                    $arr = array(auth()->user()->id, $icno, $sitecode, $month_year, $position_id);
                    $total = $this->att_repo->getAttTotalTime($arr);

                    // display block
                    // -------------
                    $block = "<i class='icon-clock'></i>&nbsp;Today: ";
                    $block .= "In <strong>" . $to['in'] . "</strong> Out <strong>" . $to['out'] . "</strong>";
                    $block .= "<br><i class='icon-clock'></i>&nbsp;Yesterday: ";
                    $block .= "In <strong>" . $yest['in'] . "</strong> Out: <strong>" . $yest['out'] . "</strong>";

                    $block .= "<br>Total MCMC: <strong>" . $total['mcmc']['total_hours'] . ":" . $total['mcmc']['total_minutes'] . ":" . $total['mcmc']['total_seconds'] . "</strong> Total All: <strong>" . $total['all']['total_hours'] . ":" . $total['all']['total_minutes'] . ":" . $total['all']['total_seconds'] . "</strong>";

                    $block .= "<br><a href=" . route('sv.attendance.daily') . ">Daily Attendance</a>";
                }

                // site not exist
                // --------------
                else {
                    $block .= "Site not Exist.";
                }
            }

            // contract is not exist or expired
            // --------------------------------
            else {
                $block .= "No Contract/Expired.";
            }
        } else {
            $block .= "Not Applicable.";
        }

        // return json
        // -----------
        $data = array('block' => $block);
        return response()->json($data);
    }

    /**
     * [getAjaxCalendar description]
     * @return [type] [description]
     */
    public function getAjaxCalendar()
    {
        $block = '';

        // manager/assistant manager/it supervisor
        // ---------------------------------------
        if (auth()->user()->group_id == 3 && in_array(session()->get('job')['position_id'], array(4, 5, 6))) {

            // contract not expired
            // --------------------
            if (session()->get('expired') != 1) {

                // get state id (code)
                // -------------------
                $sitecode = auth()->user()->sitecode;
                $site = $this->user_repo->getSiteByCode($sitecode);
                $state_id = $site->state_id;

                // get current public holiday
                // --------------------------
                $ph = $this->leave_repo->getLeavePublicBySite($site->state_id, date('m'));
                $total_ph = count($ph);

                // get off day type id
                // -------------------
                $position_id = session()->get('job')['position_id'];
                $types = $this->leave_repo->getOffDayTypeIDByPosition($position_id);

                // get off days
                // ------------
                $off_dates = $this->leave_repo->getOffDateListMonth($state_id, $types, date('Y'), date('m'));
                $total_od = count($off_dates);

                // display public holiday
                // ----------------------
                $block = "<i class='icon-calendar'></i>&nbsp;Public Holiday: <strong>" . $total_ph . "</strong><br>";
                if (count($ph) > 0) {
                    foreach ($ph as $p => $i) {

                        // set public holiday info
                        // -----------------------
                        $ph_date = Carbon::parse($i->PublicName->date)->format('d M Y');
                        $title = $ph_date . ' - ' . $i->PublicName->desc;
                        $block .= '<i class="glyphicon glyphicon-stop black" title="' . $title . '"></i>&nbsp;';
                    }
                } else {
                    $block .= "No public holiday.";
                }

                // display off day
                // ---------------
                $block .= "<br><i class='icon-ban'></i>&nbsp;Off Day: <strong>" . $total_od . "</strong><br>";
                if (count($off_dates) > 0) {
                    foreach ($off_dates as $od) {
                        $title = $od_date = $od['off_date'] . ' - ' . $od['off_type_name']['name'];
                        $block .= '<i class="glyphicon glyphicon-stop grey" title="' . $title . '"></i>&nbsp;';
                    }
                } else {
                    $block .= "No off day.";
                }
            }

            // contract is not exist or expired
            // --------------------------------
            else {
                $block .= "No Contract/Expired.";
            }
        } else {
            $block .= "Not Applicable.";
        }

        // return json
        // -----------
        $data = array('block' => $block);
        return response()->json($data);
    }
}
