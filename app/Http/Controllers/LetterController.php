<?php

namespace IhrV2\Http\Controllers;
use IhrV2\Contracts\Letter\LetterInterface as LetterRepository;

class LetterController extends Controller {
    protected $letter_repo;

    public function __construct(LetterRepository $letter_repo) {
        $this->letter_repo = $letter_repo;
    }

    public function showLetterPopup($id) 
    {
        // view letter info
        // ----------------
        $letter = $this->letter_repo->getLetterByUserID($id, auth()->user()->id);
        return View('letter.popup', compact('letter'));
    }


}


