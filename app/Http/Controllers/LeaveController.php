<?php namespace IhrV2\Http\Controllers;

use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\Maintenance\MaintenanceInterface as MaintenanceRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;

class LeaveController extends Controller {
    protected $user_repo;
    protected $leave_repo;
    protected $mtn_repo;

    public function __construct(LeaveRepository $leave_repo, UserRepository $user_repo, MaintenanceRepository $mtn_repo) {
        $this->middleware('auth');
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->mtn_repo = $mtn_repo;
    }

    // show leave lists on dialog modal
    // --------------------------------
    public function showLeaveList($leave_type_id, $type) 
    {
        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get user session
        // ----------------
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;

        // get current contract from session
        // ---------------------------------
        $contract = $this->user_repo->getUserContractSession($uid);

        // click total taken
        // -----------------
        if ($type == 1) {
            $name = 'Leave Taken';
            $arr = array($uid, $sitecode, $leave_type_id, $contract->date_from, $contract->date_to);
            $leaves = $this->leave_repo->getLeaveTakenList($arr)->toArray();
        }

        // click entitled for replacement request
        // --------------------------------------
        else if ($type == 2) {
            $name = 'Entitled';
            $arr = array($uid, $sitecode, $contract->id, $contract->date_from, $contract->date_to);
            $leaves = $this->leave_repo->getLeaveRepApproveList($arr)->toArray();
        }

        // click deduct leave
        // ------------------
        else if ($type == 3) {
            $name = 'Deduct Leave';
            $arr = array($uid, $sitecode, $leave_type_id, 2, $contract->date_from, $contract->date_to);
            $leaves = $this->leave_repo->getLeaveReimburseDeductList($arr)->toArray();
        }

        // click reimburse leave
        // ---------------------
        else if ($type == 4) {
            $name = 'Reimburse Leave';
            $arr = array($uid, $sitecode, $leave_type_id, 1, $contract->date_from, $contract->date_to);
            $leaves = $this->leave_repo->getLeaveReimburseDeductList($arr)->toArray();
        }

        // get leave type name
        // -------------------
        $leave_type = \IhrV2\Models\LeaveType::find($leave_type_id);
        return View('leave.list', compact('contract', 'leaves', 'leave_type', 'name', 'type'));
    }

    // show request leave
    // ------------------
    public function showLeaveIndex() 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Leave',
            'icon'   => 'grid',
            'title'  => 'Leave'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get user info
        // -------------
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;

        // get contract from session
        // -------------------------
        $contract = session()->get('contract');

        // set array
        // ---------
        $cond = array($uid, $sitecode, $contract->date_from, $contract->date_to);

        // select distinct leave type
        // --------------------------
        $types = $this->leave_repo->getLeaveTypeDistinct($cond);

        // query leave application
        // -----------------------
        $leaves = $this->leave_repo->getLeaveListAll($cond);

        // return view
        // -----------
        return View('leave.index', compact('header', 'leaves', 'types'));
    }

    // search request leave
    // --------------------
    public function postLeaveIndex(Request $request) {

    }

    // view leave details
    // ------------------
    public function showLeaveView($id) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Leave',
            'child-a'  => route('sv.leave.index'),
            'sub'   => 'View',
            'icon'   => 'info',
            'title'  => 'View'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get leave details
        // -----------------
        $detail = $this->leave_repo->getLeaveByUser($id, auth()->user()->id, auth()->user()->sitecode);
        if ($detail === null) {
            abort(404);
        }
        return View('leave.view', compact('header', 'detail'));
    }

    // cancel leave / cancel approved leave (5/4)
    // ------------------------------------------
    public function postLeaveView(Requests\Leave\Cancel $request, $id) 
    {
        // get user session
        // ----------------
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // cancel leave/apply cancel leave
        // -------------------------------
        $save = $this->leave_repo->getProcessLeave($request->all(), $id, $uid, $sitecode, $uid);
        return redirect()->route('sv.leave.view', array($id))->with([
            'message' => $save[0],
            'label'   => 'alert alert-'.$save[1].' alert-dismissible'
        ]);
    }

    // edit RL attachment
    // ------------------
    public function showLeaveAttachment($id) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Leave',
            'child-a'  => route('sv.leave.index'),
            'sub'   => 'View',
            'sub-a' => route('sv.leave.view', array($id)),
            'cat1'   => 'Attachment',
            'icon'   => 'paper-clip',
            'title'  => 'Attachment'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // check leave info
        // ----------------
        $leave = $this->leave_repo->getCheckLeaveInfo($id, auth()->user()->id, auth()->user()->sitecode);

        // get attachment info
        // -------------------
        $detail = $leave->LeaveLatestAttachment;

        // get attachment history
        // ----------------------
        $inactive = $this->leave_repo->getLeaveAttachmentInactive($id, auth()->user()->id);
        return View('leave.attachment', compact('header', 'detail', 'inactive'));
    }

    // update RL attachment
    // --------------------
    public function postLeaveAttachment(Requests\Leave\Attachment\Update $request, $id) 
    {
        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // upload leave attachment
        // -----------------------
        $save = $this->leave_repo->getUpdateLeaveAttachment($request->all(), $id);
        return redirect()->route('sv.leave.attachment.edit', array($id))->with([
            'message' => $save[1],
            'label'   => 'alert alert-' . $save[0] . ' alert-dismissible'
        ]);
    }

    // select leave type
    // -----------------
    public function showLeaveSelect() 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Add Leave',
            'icon'   => 'plus',
            'title'  => 'Add Leave'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get lists of leave types
        // ------------------------
        $leave_types = $this->leave_repo->getLeaveTypeWithPrefix();
        return View('leave.select', compact('header', 'leave_types'));
    }

    // redirect if user access the route using the url
    // -----------------------------------------------
    public function postLeaveSelect(Requests\Leave\Select $request) {
        return redirect()->route('sv.leave.create')->withInput();
    }

    // show add leave form
    // -------------------
    public function showLeaveCreate(Request $request) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Add Leave',
            'icon'   => 'plus',
            'title'  => 'Add Leave'
        );

        // have selected leave type
        // ------------------------
        if (old('leave_type_id')) {

            // save leave type session
            // -----------------------
            session()->put('leave_type_id', old('leave_type_id'));
            $leave_type_id = session()->get('leave_type_id');

            // get user session
            // ----------------
            $uid = auth()->user()->id;
            $sitecode = auth()->user()->sitecode;

            // check conditions
            // ----------------
            $check = $this->leave_repo->getCheckApplyLeave($leave_type_id, $uid, $sitecode);

            // pass all conditions
            // -------------------
            if ($check['status'] == 1) {

                // return to create leave
                // ----------------------
                return View('leave.create', compact('header', 'check'));
            } 

            // not pass
            // --------
            else {
                return redirect()->route('sv.leave.select')->with([
                    'message' => $check['msg'],
                    'label' => 'alert alert-danger alert-dismissible'
                ]);
            }
        } 

        // not selected leave type/refresh
        // -------------------------------
        else {
            return redirect()->route('sv.leave.select');
        }
    }

    // save new leave
    // --------------
    public function storeLeaveCreate(Requests\Leave\Create $request) 
    {
        $save = $this->leave_repo->getApplyLeave($request->all());
        return redirect()->route($save[2])->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // leave summary
    // -------------
    public function showLeaveSummary() 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Summary',
            'icon'   => 'calculator',
            'title'  => 'Summary'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get user info
        // -------------
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;

        // get contract from session
        // -------------------------
        $contract = session()->get('contract');

        // get contract duration
        // ---------------------
        $duration = $this->leave_repo->DateRange($contract->date_from, $contract->date_to);

        // get summary lists
        // -----------------
        $lists = $this->leave_repo->getLeaveSummaryEach($uid, $sitecode, $contract->id, $contract->date_from, $contract->date_to);

        // get position name
        // -----------------
        $position = $this->user_repo->getUserPositionSession();

        // get site name
        // -------------
        $sitename = $this->user_repo->getuserSiteNameSession();
        return View('leave.summary', compact('header', 'contract', 'duration', 'lists', 'position', 'sitename'));
    }

    // rl request lists
    // ----------------
    public function showLeaveRepIndex() 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'RL Request',
            'icon'   => 'list',
            'title'  => 'RL Request'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get user session
        // ----------------
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;
        $contract = session()->get('contract');

        // get RL request detail
        // ---------------------
        $leaves = $this->leave_repo->getLeaveRepAllList($uid, $sitecode, $contract->date_from, $contract->date_to);
        return View('rl-request.index', compact('header', 'leaves'));
    }

    // view replacement leave
    // ----------------------
    public function showLeaveRepView($id) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'RL Request',
            'child-a' => route('sv.leave.replacement.index'),
            'sub'    => 'View',
            'icon'   => 'info',
            'title'  => 'View'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get RL request details
        // ----------------------
        $detail = $this->leave_repo->getLeaveRepView($id, auth()->user()->id, auth()->user()->sitecode);
        if ($detail === null) {
            abort(404);
        }        
        return View('rl-request.view', compact('header', 'detail'));
    }

    // cancel request by site supervisor
    // ---------------------------------
    public function postLeaveRepView(Requests\Leave\RL\Cancel $request, $id) 
    {
        // get user session
        // ----------------
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // 5 - cancel RL request
        // 4 - apply cancel RL request 
        // ---------------------------
        $save = $this->leave_repo->getProcessLeaveRep($request->all(), $id, $uid, $sitecode, $uid);
        return redirect()->route('sv.leave.replacement.view', array($id))->with([
            'message' => $save[0],
            'label'   => 'alert alert-'.$save[1].' alert-dismissible'
        ]);
    }

    // apply replacement leave
    // -----------------------
    public function showLeaveRepCreate() 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'RL Request',
            'sub'    => 'Add Request',
            'icon'   => 'plus',
            'title'  => 'Add Request'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // set variables
        // -------------
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;
        $expired = 1;
        $more = 0;
        $rm = 0;
        $years = array();
        $months = array();
        $days = array();

        // check existing request for today
        // --------------------------------
        $today = $this->leave_repo->getLeaveRepToday($uid, $sitecode);

        // check if request already exist
        // ------------------------------
        if (!empty($today)) {
            if (in_array($today->LeaveRepLatestHistory->status, array(1,2,4,7))) {
                return redirect()->route('message')->with(['type' => 2]);
            }
        }

        // populate drop down value
        // ------------------------
        $years = $this->leave_repo->getLeaveYear();
        $months = $this->leave_repo->getLeaveMonth();
        $days = array('' => '[Day]') + array(1 => 1, 2 => 2);

        // set value array
        // ---------------
        $arr = array($years, $months, $days);
        return View('rl-request.create', compact('header', 'arr'));
    }

    // save replacement leave
    // ----------------------
    public function storeLeaveRepCreate(Requests\Leave\RL\Create $request) 
    {
        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // save rl request
        // ---------------
        $save = $this->leave_repo->getApplyLeaveRep($request->all());
        return redirect()->route($save[2])->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

    // edit attachment RL request
    // --------------------------
    public function showRLAttachment($id) 
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'RL Request',
            'child-a' => route('sv.leave.replacement.index'),
            'sub'    => 'View',
            'sub-a'  => route('sv.leave.replacement.view', array($id)),
            'cat1'   => 'Attachment',
            'icon'   => 'paper-clip',
            'title'  => 'Attachment'
        );

        // check leave info
        // ----------------
        $request = $this->leave_repo->getCheckLeaveRepInfo($id, auth()->user()->id, auth()->user()->sitecode);

        // get attachment info
        // -------------------
        $detail = $request->LeaveRepLatestAttachment;

        // get inactive attachment
        // -----------------------
        $inactive = $this->leave_repo->getInactiveRLRepAttachment($id, auth()->user()->id);
        return View('rl-request.attachment', compact('header', 'detail', 'inactive'));
    }

    // save attachment replacement leave
    // ---------------------------------
    public function postRLAttachment(Requests\Leave\RL\Attachment\Update $request, $id) 
    {
        // upload new attachment
        // ---------------------
        $save = $this->leave_repo->getUpdateLeaveRepAttachment($request->all(), $id, auth()->user()->id);
        
        // return result
        // -------------
        return redirect()->route('sv.leave.replacement.attachment.edit', array($id))->with([
            'message' => $save[1],
            'label'   => 'alert alert-' . $save[0] . ' alert-dismissible'
        ]);
    }

    // display calendar
    // ----------------
    public function showCalendar() 
    {
        $index = $this->mtn_repo->showCalendar(null, auth()->user()->sitecode);
        return $index;
    }

    // search calendar
    // ---------------
    public function postCalendar(Request $request) 
    {
        $index = $this->mtn_repo->showCalendar($request, auth()->user()->sitecode);
        return $index;
    }

    // display calendar date
    // ---------------------
    public function showCalendarView($date) 
    {
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Calendar',
            'child-a' => route('sv.leave.calendar'),
            'sub'     => 'View Date',
            'icon'    => 'event',
            'title'   => 'View Date'
        );

        // date is valid
        // -------------
        if (strtotime($date)) {

            // get state id
            // ------------
            $state_id = null;
            $site = $this->leave_repo->getUserStateID(auth()->user()->sitecode);
            if (!empty($site)) {
                $state_id = $site->state_id;
            }

            // get off day
            // -----------
            $off = $this->mtn_repo->getOffDaySite($date, $state_id);

            // get public holiday
            // ------------------
            $public = $this->mtn_repo->getPublicHolidaySite($date, $state_id);
            return view('calendar.view', compact('header', 'off', 'public'));
        }

        // date is invalid
        // ---------------
        else {

            // return to calendar
            // ------------------
            return redirect()->route('sv.leave.calendar')->with([
                'message' => 'Date is invalid.',
                'label'   => 'alert alert-danger alert-dismissible'
            ]);
        }
    }

}
