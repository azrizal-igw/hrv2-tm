<?php namespace IhrV2\Http\Controllers;

use Auth;
use Hash;
use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\Letter\LetterInterface as LetterRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $user_repo;
    protected $leave_repo;
    protected $letter_repo;

    public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo, LetterRepository $letter_repo) {
        $this->middleware('auth');
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->letter_repo = $letter_repo;
    }

    // display message
    // ---------------
    public function showMessage() 
    {
        $header = array(
            'parent' => 'Information',
            'child'  => false,
            'icon'   => 'info',
            'title'  => 'Information'
        );

        // display message
        // ---------------
        if (session()->get('type') == 1) {
            // no contact/expired
            $arr = 'No Contract/Expired. Please contact HR.';
        } else if (session()->get('type') == 2) {
            // if already apply rl today
            $arr = 'Today Request already exist. Please apply next day.';
        } else if (session()->get('type') == 3) {
            // go to photo page but not site supervisor
            $arr = 'The page is not allowed.';
        } else {
            $arr = '-';
        }
        return view('message', compact('header', 'arr'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showHome() 
    {
        $group_id = auth()->user()->group_id;



        return view('home', compact('group_id'));
    }

    // display user profile
    // --------------------
    public function showProfile() {
        $header = array(
            'parent' => 'Profile',
            'child'  => false,
            'icon'   => (auth()->user()->gender_id == 2) ? 'user-female' : 'user',
            'title'  => 'View Profile'
        );
        $arr = array();
        $empty = 0;
        $pending = 0;
        $approved = 0;
        $rejected = 0;

        // get user info
        // -------------
        $marital = $this->user_repo->getMaritalStatusByID(auth()->user()->marital_id);
        $nationality = $this->user_repo->getNationalityByID(auth()->user()->nationality_id);
        $race = $this->user_repo->getRaceByID(auth()->user()->race_id);
        $religion = $this->user_repo->getReligionByID(auth()->user()->religion_id);
        $corr_state = $this->user_repo->getStateByCode(auth()->user()->correspondence_state);

        // check user photo
        // ----------------
        $data = $this->user_repo->getCheckUserPhoto(auth()->user()->id, auth()->user()->sitecode);
        return view('auth.profile', compact('header', 'marital', 'nationality', 'race', 'religion', 'corr_state', 'data'));
    }

    // edit user profile
    // -----------------
    public function editProfile() {
        $header = array(
            'parent' => 'Profile',
            'child'  => false,
            'icon'   => 'note',
            'title'  => 'Edit Info'
        );
        $marital_status = $this->user_repo->getMaritalStatusList();
        $states = $this->user_repo->getStateListAll();
        return view('auth.edit', compact('header', 'photo', 'marital_status', 'states'));
    }

    // update user profile
    // -------------------
    public function postProfile(Requests\Auth\Profile\Update $request) 
    {
        // save updated record
        // -------------------
        $save = $this->user_repo->getUserProfileUpdate($request->all(), auth()->user()->id);

        // return message
        // --------------
        return redirect()->route('auth.profile')->with([
            'message' => $save[1],
            'label'   => 'alert alert-' . $save[0] . ' alert-dismissible'
        ]);        
    }

    // edit user password
    // ------------------
    public function editPassword() {
        $data = array();
        $data['header'] = array(
            'parent' => 'Profile',
            'child'  => false,
            'icon'   => 'lock',
            'title'  => 'Change Password'
        );
        return view('auth.password', $data);
    }

    // update user password
    // --------------------
    public function updatePassword(Requests\Auth\Password\Update $request) 
    {
        $user = Auth::user();
        if (Hash::check($request->old_password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->new_password)
            ])->save();
            $msg = array('Password successfully updated.', 'success');
        } else {
            $msg = array('Current password is incorrect.', 'danger');
        }
        return redirect()->back()->with([
            'message' => $msg[0],
            'label'   => 'alert alert-' . $msg[1] . ' alert-dismissible'
        ]);
    }

    // show photo upload
    // -----------------
    public function showPhoto() 
    {
        $header = array(
            'parent' => 'Photo',
            'child'  => false,
            'icon'   => 'picture',
            'title'  => 'Upload Photo'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // group id is not site supervisor
        // -------------------------------
        if (auth()->user()->group_id != 3) {
            return redirect()->route('message')->with(['type' => 3]);
        }

        // get all photo
        // -------------
        $all = $this->user_repo->getUserPhotoAll(auth()->user()->id, auth()->user()->sitecode);

        // get latest photo
        // ----------------
        $data = $this->user_repo->getCheckUserPhoto(auth()->user()->id, auth()->user()->sitecode);
        return view('auth.photo', compact('header', 'all', 'data'));
    }

    // upload photo
    // ------------
    public function postPhoto(Requests\Auth\Photo\Upload $request) 
    {
        // process upload photo
        // --------------------
        $save = $this->user_repo->processUserPhoto($request->all(), auth()->user()->id, auth()->user()->sitecode, 1);

        // succcessfully uploaded
        // ----------------------
        if ($save[2] == 1) {

            // remove previous photo session
            // -----------------------------
            session()->forget('photo');

            // check photo record
            // ------------------
            $photo = $this->user_repo->getUserPhotoOne(auth()->user()->id, auth()->user()->sitecode);
            if (!empty($photo)) {
                session()->put('photo', $photo->toArray());
            }
        }

        // return message
        // --------------
        return redirect()->back()->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

}
