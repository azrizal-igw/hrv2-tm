<?php namespace IhrV2\Http\Controllers;

use Carbon\Carbon;
use IhrV2\Contracts\Attendance\AttendanceInterface as AttendanceRepository;
use IhrV2\Http\Requests;
use Illuminate\Http\Request;
use IhrV2\Helpers\AttendanceHelper;

class AttendanceController extends Controller {
    protected $att_repo;
    public function __construct(AttendanceRepository $att_repo) {
        $this->att_repo = $att_repo;
    }

    // display monthly attendance
    // --------------------------
    public function showAttDaily() {
        $index = $this->att_repo->getAttDaily();
        return $index;
    }

    // search daily attendance
    // -----------------------
    public function postAttDaily(Request $request) {
        $index = $this->att_repo->getAttDaily($request);
        return $index;
    }

    // display attendance map
    // ----------------------
    public function showAttDailyMap($id)
    {
        $data = array('block' => 'asdasds');
        return response()->json($data);
    }

    // view attendance details
    // -----------------------
    public function showDailyView($date) 
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Daily',
            'child-a' => route('sv.attendance.daily'),
            'sub'     => 'View',
            'icon'    => 'clock',
            'title'   => 'View'
        );

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // set variables
        // -------------
        $uid = auth()->user()->id;
        $icno = auth()->user()->icno;
        $sitecode = auth()->user()->sitecode;

        // get position id
        // ---------------
        $position_id = session()->get('job')['position_id'];

        // check attendance view
        // ---------------------
        $data = $this->att_repo->getAttViewDetail($uid, $icno, $sitecode, $position_id, $date);

        // get attendance status
        // ---------------------
        $status = $this->att_repo->getAttendanceStatusArray(array(3,4,7));
        return View('attendance.view', compact('header', 'date', 'data', 'years', 'status'));
    }

    // submit new remark/time slip
    // ---------------------------
    public function postDailyView(Requests\Attendance\Remark\Create $request, $date) 
    {
        // initial variables
        // -----------------
        $today = Carbon::now()->format('Y-m-d');
        $invalid = 0;
        $previous = 0;
        $range = 0;

        // check if contract expired
        // -------------------------
        if (session()->get('expired') == 1) {
            return redirect()->route('message')->with(['type' => 1]);
        }

        // get user info
        // -------------
        $user = array(
            'uid'      => auth()->user()->id,
            'icno'     => auth()->user()->icno,
            'sitecode' => auth()->user()->sitecode
        );

        // insert remarks
        // --------------
        if ($request->type == 1) {
            $msg = $this->att_repo->getApplyNewRemark($request->all(), $user, $date);
        }

        // insert other info
        // -----------------
        else if ($request->type == 2) {
            $msg = $this->att_repo->getApplyOtherInfo($request->all(), $user, $date);
        }

        // invalid type
        // ------------
        else {
            $msg = array('Operation is not allowed.', 'danger');
        }

        // insert attendance remarks
        // -------------------------
        return redirect()->route('sv.attendance.daily.view', array($date))->with([
            'message' => $msg[0],
            'label'   => 'alert alert-' . $msg[1] . ' alert-dismissible'
        ]);
    }

    // page to set remark by range
    // ---------------------------
    public function showDailyChecked(Request $request) 
    {
        // check selected dates session
        // ----------------------------
        if (old('dates')) {

            // set new session
            // ---------------
            $dates = old('dates');

            // get user info
            // -------------
            $user = array(
                'uid'      => auth()->user()->id,
                'icno'     => auth()->user()->icno,
                'sitecode' => auth()->user()->sitecode
            );

            // check if selected dates already have remark
            // -------------------------------------------
            $check = $this->att_repo->getCheckAttRemarkAll($user, $dates);

            // none have remark yet
            // --------------------
            if ($check == 0) {

                // set header
                // ----------
                $header = array(
                    'parent'  => 'Attendance',
                    'child'   => 'Daily',
                    'child-a' => route('sv.attendance.daily'),
                    'sub'     => 'Set Remark',
                    'icon'    => 'note',
                    'title'   => 'Set Remark'
                );

                // get total dates
                // ---------------
                $total = count($dates);

                // get input values
                // ----------------
                $input = array('att_status' => old('att_status'), 'att_notes' => old('att_notes'));

                // get attendance status
                // ---------------------
                $status = $this->att_repo->getAttendanceStatusArray(array(3, 4));
                return view('attendance.set', compact('header', 'dates', 'total', 'status', 'input'));
            }

            // one or more selected date alredy have remark
            // --------------------------------------------
            else {
                return redirect()->route('sv.attendance.daily')->with([
                    'message' => 'Selected date already have remark. Please select again.',
                    'label'   => 'alert alert-danger alert-dismissible'
                ]);
            }
        }

        // not has session dates
        // ---------------------
        else {
            return redirect()->route('sv.attendance.daily')->withInput();
        }
    }

    // process set remark
    // ------------------
    public function postDailyChecked(Requests\Attendance\Remark\Multiple $request) 
    {    
        // get user info
        // -------------
        $user = array(
            'uid'      => auth()->user()->id,
            'icno'     => auth()->user()->icno,
            'sitecode' => auth()->user()->sitecode
        );

        // get selected date
        // -----------------
        $dates = $request->session()->get('dates');

        // process attendance remark
        // -------------------------
        $save = $this->att_repo->getApplyRangeRemark($request->all(), $user, $dates);

        // return success
        // --------------
        return redirect()->route('sv.attendance.daily')->with([
            'message' => $save[0],
            'label'   => 'alert alert-' . $save[1] . ' alert-dismissible'
        ]);
    }

}
