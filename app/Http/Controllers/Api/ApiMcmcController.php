<?php namespace IhrV2\Http\Controllers\Api;

use IhrV2\Contracts\Mcmc\McmcInterface as McmcRepository;
use IhrV2\Http\Controllers\Controller;

class ApiMcmcController extends Controller {
    protected $mcmc_repo;
    public function __construct(McmcRepository $mcmc_repo) {
        $this->mcmc_repo = $mcmc_repo;
    }

    // get api token
    // -------------
    public function getTokenID() {
        $get = $this->mcmc_repo->getTokenAndPath();
        return $get['token'];
    }

    // select data staff detail all
    // ----------------------------
    public function getCallStaffAll() {
        return $this->mcmc_repo->getCallBack('usp_im.v_pi1m_cms_staff_details');
    }

    // select data staff attendance all
    // --------------------------------
    public function getCallAttAll() {
        return $this->mcmc_repo->getCallBack('usp_im.v_pi1m_cms_staff_att');
    }

    /*
    |--------------------------------------------------------------------------
    | Staff Attendance
    |--------------------------------------------------------------------------
     */

    // process staff attendance by current date
    // ----------------------------------------
    public function getProAttDaily() {
        $exe = $this->mcmc_repo->getProAttDaily();
        return response()->json($exe);
    }

    // process staff attendance by date
    // --------------------------------
    public function getProAttDate($date) {
        $exe = $this->mcmc_repo->getProAttDate($date);
        return response()->json($exe);
    }

    // process staff attendance by sitecode and date range
    // ---------------------------------------------------
    public function getProAttSiteDate($sitecode, $start_date, $end_date) {
        $exe = $this->mcmc_repo->getProAttSiteDate($sitecode, $start_date, $end_date);
        return response()->json($exe);
    }

    // process staff attendance by start and end date
    // ----------------------------------------------
    public function getProAttStartEnd($start_date, $end_date) {
        $exe = $this->mcmc_repo->getProAttStartEnd($start_date, $end_date);
        return response()->json($exe);
    }

    // export process table to agg db
    // ------------------------------
    public function getExpAttDaily() {
        $exe = $this->mcmc_repo->getExpAttDaily();
        return response()->json($exe);
    }

    // export process table to agg db
    // ------------------------------
    public function getExpAttDate($date) {
        $exe = $this->mcmc_repo->getExpAttDate($date);
        return response()->json($exe);
    }

    // export staff attendance by sitecode and date range
    // --------------------------------------------------
    public function getExpAttSiteDate($sitecode, $start_date, $end_date) {
        $exe = $this->mcmc_repo->getExpAttSiteDate($sitecode, $start_date, $end_date);
        return response()->json($exe);
    }

    // export staff attendance by date range
    // -------------------------------------
    public function getExpAttStartEnd($start_date, $end_date) {
        $exe = $this->mcmc_repo->getExpAttStartEnd($start_date, $end_date);
        return response()->json($exe);
    }

    // check daily attendance fail
    // ---------------------------
    public function getChkAttFail($start, $end) {
        $exe = $this->mcmc_repo->getChkAttFail($start, $end);
        return response()->json($exe);
    }

    // reset staff attendance by date
    // ------------------------------
    public function getResetAttDate($date) {
        $exe = $this->mcmc_repo->getResetAttDate($date);
        return response()->json($exe);
    }

    // reset staff attendance by date range
    // ------------------------------------
    public function getResetAttStartEnd($start_date, $end_date) {
        $exe = $this->mcmc_repo->getResetAttStartEnd($start_date, $end_date);
        return response()->json($exe);
    }

    /*
    |--------------------------------------------------------------------------
    | Staff Detail
    |--------------------------------------------------------------------------
     */

    // export staff detail
    // -------------------
    public function getExpStaff($status, $start, $end) {
        $exe = $this->mcmc_repo->getExpStaff($status, $start, $end);
        return response()->json($exe);
    }

    /*
    |--------------------------------------------------------------------------
    | Site Detail
    |--------------------------------------------------------------------------
     */

    // export site detail
    // ------------------
    public function getExpSite() {
        $exe = $this->mcmc_repo->getExpSite();
        return response()->json($exe);
    }

}
