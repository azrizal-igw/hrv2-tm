<?php

namespace IhrV2\Http\Controllers\Api\Auth;

use IhrV2\Contracts\Api\Auth\ApiAuthInterface as ApiAuthRepository;
use IhrV2\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiAuthController extends Controller {

    protected $auth;
    public function __construct(ApiAuthRepository $auth) {
        $this->auth = $auth;
    }

    // get user login
    // --------------
    public function getApiLogin(Request $request) {
        $get = $this->auth->getApiLogin($request);
        return $get;
    }

    // get user token
    // --------------
    public function getApiToken() {
        $get = $this->auth->getApiToken();
        return $get;
    }

    // get user info
    // -------------
    public function getApiUser(Request $request) {
        $get = $this->auth->getApiUser($request);
        return $get;
    }

    // destroy current token
    // ---------------------
    public function getApiLogout() {
        $get = $this->auth->getApiLogout();
        return $get;
    }



}
