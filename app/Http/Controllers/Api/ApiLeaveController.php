<?php namespace IhrV2\Http\Controllers\Api;

use Carbon\Carbon;
use Crypt;
use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;

class ApiLeaveController extends Controller {
    protected $user_repo;
    protected $leave_repo;

    public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo) {
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
    }

    // approve leave from url
    // ----------------------
    public function getLeaveApprove($url) 
    {
        // check the url
        // -------------
        try {

            // decrypt the url
            // ---------------
            $decrypted = Crypt::decrypt($url);

            // separate value from the url
            // ---------------------------
            $exp = explode('/', $decrypted);

            // count total value
            // -----------------
            if (count($exp) == 4) {

                // get value from url
                // ------------------
                $id = $exp[0];
                $uid = $exp[1];
                $sitecode = $exp[2];
                $action_by = $exp[3];

                // approve leave
                //--------------
                $data = array('remark' => 'Approved By Email', 'type' => 2);
                $msg = $this->leave_repo->getProcessLeave($data, $id, $uid, $sitecode, $action_by);
            }

            // invalid parameter
            // -----------------
            else {
                $msg = array('Parameter is Invalid.');
            }

            // return message
            // --------------
            return response()->json($msg[0]);
        }

        // url is invalid
        // --------------
         catch (DecryptException $e) {
            return response()->json('URL is Invalid.');
        }
    }

    // reject leave
    // ------------
    public function getLeaveReject($url) 
    {
        // check the url
        // -------------
        try {

            // decrypt the url
            // ---------------
            $decrypted = Crypt::decrypt($url);

            // split the value from url
            // ------------------------
            $exp = explode('/', $decrypted);
            if (count($exp) == 4) {

                // get value from url
                // ------------------
                $id = $exp[0];
                $uid = $exp[1];
                $sitecode = $exp[2];
                $action_by = $exp[3];

                // reject leave
                //-------------
                $data = array('remark' => 'Rejected By Email', 'type' => 3);
                $msg = $this->leave_repo->getProcessLeave($data, $id, $uid, $sitecode, $action_by);
            }

            // invalid parameter
            // -----------------
            else {
                $msg = array('Parameter is Invalid.');
            }            
            return response()->json($msg[0]);
        }

        // url is invalid
        // --------------
         catch (DecryptException $e) {
            return response()->json('URL is Invalid.');
        }
    }

    // approve RL request
    // ------------------
    public function getLeaveRepApprove($url) 
    {
        // check the url
        // -------------
        try {

            // decrypt the url
            // ---------------
            $decrypted = Crypt::decrypt($url);

            // check parameter
            // ---------------
            $exp = explode('/', $decrypted);
            if (count($exp) == 4) {

                // get value from url
                // ------------------
                $id = $exp[0];
                $uid = $exp[1];
                $sitecode = $exp[2];
                $action_by = $exp[3];

                // approve RL request
                //-------------------
                $data = array('remark' => 'Approved By Email', 'type' => 2);
                $msg = $this->leave_repo->getProcessLeaveRep($data, $id, $uid, $sitecode, $action_by);
            }

            // invalid parameter
            // -----------------
            else {
                $msg = array('Parameter is Invalid.');
            }            
            return response()->json($msg[0]);
        }

        // url is invalid
        // --------------
         catch (DecryptException $e) {
            return response()->json('URL is Invalid.');
        }
    }

    // reject RL request
    // -----------------
    public function getLeaveRepReject($url) 
    {
        // check the url
        // -------------
        try {

            // decrypt the url
            // ---------------
            $decrypted = Crypt::decrypt($url);

            // check parameter
            // ---------------
            $exp = explode('/', $decrypted);
            if (count($exp) == 4) {

                // get value from url
                // ------------------
                $id = $exp[0];
                $uid = $exp[1];
                $sitecode = $exp[2];
                $action_by = $exp[3];

                // process RL request
                //-------------------
                $data = array('remark' => 'Rejected By Email', 'type' => 3);
                $msg = $this->leave_repo->getProcessLeaveRep($data, $id, $uid, $sitecode, $action_by);
            }
            return response()->json($msg[0]);
        }

        // url is invalid
        // --------------
         catch (DecryptException $e) {
            return response()->json('URL is Invalid.');
        }
    }

}
