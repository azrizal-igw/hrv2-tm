<?php

namespace IhrV2\Http\Controllers\APi;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Contracts\Erp\ErpInterface;

class ApiSyncController extends Controller
{

    protected $erp;
    /**
     * @param ErpInterface $erp
     */
    public function __construct(ErpInterface $erp) {
        $this->erp = $erp;
    }

    // sync staff both active & inactive
    // ---------------------------------
    public function getSyncStaffActive($record, $skip) {
        $sync = $this->erp->getSyncStaffActive($record, $skip);
        return response()->json($sync);
    }

    // sync staff contract
    // -------------------
    public function getSyncStaffContract($record, $skip) {
        $sync = $this->erp->getSyncStaffContract($record, $skip);
        return response()->json($sync);
    }

    // sync staff other
    // ----------------
    public function getSyncStaffOther($group) {
        $sync = $this->erp->getSyncStaffOther($group);
        return response()->json($sync);
    }

    // sync site
    // ---------
    public function getSyncSite() {
        $sync = $this->erp->getSyncSite();
        return response()->json($sync);
    }

    // sync public holiday
    // -------------------
    public function getSyncPublicHoliday($year = null) {
        $sync = $this->erp->getSyncPublicHoliday($year);
        return response()->json($sync);
    }

    // sync region
    // -----------
    public function getSyncRegion() {
        $sync = $this->erp->getSyncRegion();
        return response()->json($sync);
    }

    // sync state
    // ----------
    public function getSyncState() {
        $sync = $this->erp->getSyncState();
        return response()->json($sync);
    }

    // sync position
    // -------------
    public function getSyncPosition() {
        $sync = $this->erp->getSyncPosition();
        return response()->json($sync);
    }

    // sync phase
    // ----------
    public function getSyncPhase() {
        $sync = $this->erp->getSyncPhase();
        return response()->json($sync);
    }


}
