<?php namespace IhrV2\Http\Controllers\Api;

use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;

class ApiMigrationController extends Controller {
	protected $user_repo;
	protected $leave_repo;

	public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo) {
		$this->user_repo = $user_repo;
		$this->leave_repo = $leave_repo;
	}

	// insert data attendance from old server (.15)
	// url: api/migration/attendance/insert/{date}
	// -------------------------------------------
	public function getMigrateRemainAttendance($date) {
		$total = 0;
		$insert = 0;

		// check old attendance
		// --------------------
		$att = \IhrV2\Models\MAttendanceLogRecord::whereDate('att_log_time', '=', $date)->get();

		// found record
		// ------------
		if (!empty($att)) {
			foreach ($att as $i) {

				// check exsiting attendance
				// -------------------------
				$check = \IhrV2\Models\Attendance::where('att_log_time', '=', $i->att_log_time)
					->where('att_mykad_ic', $i->att_mykad_ic)
					->where('location_id', $i->location_id)
					->where('att_record_type', $i->att_record_type)
					->first();

				// no record
				// ---------
				if (empty($check)) {

					// insert new record at attendance
					// -------------------------------
					$arr = array(
						'att_mykad_ic' => $i->att_mykad_ic,
						'att_password' => $i->att_password,
						'location_id' => $i->location_id,
						'att_record_type' => $i->att_record_type,
						'att_input_type' => $i->att_input_type,
						'att_log_time' => $i->att_log_time,
						'att_userstamp' => $i->att_userstamp,
						'ip_add' => $i->ip_add,
						'att_timestamp' => $i->att_timestamp,
						'att_sys_status' => $i->att_sys_status,
					);
					$sav = new \IhrV2\Models\Attendance($arr);
					$sav->save();
					$insert++;
				}
			}
		}

		// return json
		// -----------
		$x = array(
			'total' => $total,
			'insert' => $insert,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateUpdateRL() {
		$total = 0;
		$update = 0;

		$q = \IhrV2\Models\MSleaveApprove::where('flag', 6)->get();
		if (!empty($q)) {
			foreach ($q as $i) {
				$total++;

				// chck leave_applications
				// -----------------------
				$leave = \IhrV2\Models\LeaveApplication::where('leave_id', $i->leave_id)->first();
				if (!empty($leave)) {

					// update leave_type_id
					// --------------------
					$update = \IhrV2\Models\LeaveApplication::where('leave_id', $i->leave_id)
						->update(array('leave_type_id' => 6));
					$update++;
				}
			}
		}
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateLeaveDetail($year) {
		// set variables
		// -------------
		$total = 0;
		$update = 0;
		$uid = 0;
		$report_to = 0;
		$contract_id = 0;
		$sitecode = 0;

		// 2016
		// min 29219
		// max 42653

		// follow batch
		// 29219 - 33000
		// 33001 - 34500
		// 34501 - 37000
		// 37001 - 40000
		// 40001 - 42653

		// 2017
		// min 39007
		// max 44755 (recheck again) select max(leave_details_id) from sleave_details where year(date_from) = '2017'

		// follow batch
		// 39007 - 43000

		// start again 43925 >

		// last id 44833!!!!!!!!
		// 30000 done
		// 32000
		// 34000
		// 36000
		// 38000
		// 40000
		// 42000
		// 44000

		// query sleave_details
		// --------------------
		$q = \IhrV2\Models\MSleaveDetail::with(array('LeaveInfo' => function ($h) {
			$h->with('UserInfo');
		}))
		// ->whereYear('date_from', '=', $year)
			->whereBetween('leave_details_id', [44001, 44833]) // run all
			->get();

		// have record
		// -----------
		if (count($q)) {
			$total = count($q);
			foreach ($q as $i) {

				// check existing records
				// ----------------------
				$exist = \IhrV2\Models\LeaveApplication::where('leave_details_id', $i->leave_details_id)->first();

				// process only new record
				// -----------------------
				if (empty($exist)) {

					// check if user_id is not empty
					// -----------------------------
					if (!empty($i->LeaveInfo->UserInfo)) {

						// set user id
						// -----------
						$uid = $i->LeaveInfo->UserInfo->id;

						// get reporting officer
						// ---------------------
						if (!empty($i->LeaveInfo->UserInfo->sitecode)) {
							$rm = $this->user_repo->getRegionManager($i->LeaveInfo->UserInfo->sitecode);
							if (!empty($rm->RegionName->RegionManager)) {
								$report_to = $rm->RegionName->RegionManager->RegionManagerDetail->id;
							}
						}

						// check contract id
						// -----------------
						if (!empty($i->LeaveInfo->UserInfo->UserLatestContract)) {
							$contract_id = $i->LeaveInfo->UserInfo->UserLatestContract->id;
						}

						// check sitecode
						// --------------
						if (!empty($i->LeaveInfo->UserInfo->sitecode)) {
							$sitecode = $i->LeaveInfo->UserInfo->sitecode;
						}
					}

					// check half day
					// --------------
					if ($i->half_day == 1) {
						$is_half_day = 1;
					} else {
						$is_half_day = 0;
					}

					// insert leave_applications
					// -------------------------
					$arr = array(
						'user_id' => $uid,
						'contract_id' => $contract_id,
						'leave_type_id' => $i->LeaveInfo->leave_type_id,
						'date_apply' => $i->date_from,
						'report_to' => $report_to,
						'date_from' => $i->date_from,
						'date_to' => $i->date_to,
						'is_half_day' => $is_half_day,
						'desc' => $i->LeaveInfo->notes,
						'sitecode' => $sitecode,
						'active' => 1,
						'leave_id' => $i->leave_id,
						'leave_details_id' => $i->leave_details_id,
					);
					$sav = new \IhrV2\Models\LeaveApplication($arr);
					$sav->save();
					$leave_id = $sav->id;

					// get date range
					// --------------
					$duration = $this->leave_repo->DateRange($i->date_from, $i->date_to);

					// loop date range
					// ---------------
					foreach ($duration as $date) {

						// insert leave_dates
						// ------------------
						$d = array(
							'user_id' => $uid,
							'leave_id' => $leave_id,
							'leave_date' => $date,
							'leave_type' => 1, // available
							'status' => 1,
						);
						$ld = new \IhrV2\Models\LeaveDate($d);
						$ld->save();
					}

					// insert leave_histories
					// ----------------------
					$dh = array(
						'user_id' => $uid,
						'leave_id' => $leave_id,
						'action_date' => $i->date_from,
						'action_by' => $uid,
						'action_remark' => '',
						'status' => 1, // pending
						'flag' => 1, // active 1st
					);
					$lh = new \IhrV2\Models\LeaveHistory($dh);
					$lh->save();

					// insert leave_balances
					// ---------------------
					// $db = array(
					// 	'user_id' => $uid,
					// 	'leave_type_id' => $i->LeaveInfo->leave_type_id,
					// 	'balance' => $entitled,
					// 	'contract_id' => $contract->id,
					// 	'year' => date('Y')
					// );
					// $lb = new \IhrV2\Models\LeaveBalance($db);
					// $lb->save();

					// insert leave_attachments
					// ------------------------
					if (!empty($i->attachment)) {
						$exp = explode('.', $i->attachment);
						$arr = array(
							'user_id' => $uid,
							'leave_id' => $leave_id,
							'filename' => $exp[0],
							'ext' => $exp[1],
							'size' => 0,
							'thumb_name' => null,
							'status' => 1,
						);
						$la = new \IhrV2\Models\LeaveAttachment($arr);
						$la->save();
					}
					$update++;

				}
			}
		}

		// return result
		// -------------
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateUpdateStatus($year) {
		$total = 0;
		$rejected = 0; // 3
		$apply_cancel = 0; // 4
		$cancel = 0; // 5
		$approved_cancel = 0; // 6
		$st = array(3, 4, 5, 6);

		// query leave history of status
		// -----------------------------
		$q = \IhrV2\Models\MSleaveHistory::with(array('LeaveInfo' => function ($h) {
			$h->with('LeaveUserDetail');
		}))
			->whereYear('date_of_action', '=', $year)
			->whereIn('status_id', $st)
			->get();

		// check record
		// ------------
		if (!empty($q)) {
			foreach ($q as $i) {

				// get status id
				// -------------
				$status = $i->status_id;

				// get action date
				// ---------------
				$date = $i->date_of_action;

				// check leave history at hrdb
				// ---------------------------
				if (!empty($i->LeaveInfo)) {
					$total++;

					$check = \IhrV2\Models\LeaveHistory::where('leave_id', $i->LeaveInfo->id)
						->where('status', $i->status_id)
						->where('flag', 1)
						->first();

					// not found record at history
					// -----------------------
					if (empty($check)) {

						// step
						// ----
						// 1) update current flag to 0
						// 2) insert new history

						// approved cancel (6)
						// -------------------
						if ($status == 6) {

							// update leave_approves
							// ---------------------
							$update = \IhrV2\Models\LeaveApprove::where('user_id', $i->LeaveInfo->user_id)
								->where('leave_id', $i->LeaveInfo->id)
								->where('flag', 1)
								->update(array('flag' => 0));

							// inactive current leave_histories
							// --------------------------------
							$update = \IhrV2\Models\LeaveHistory::where('leave_id', $i->LeaveInfo->id)
								->where('user_id', $i->LeaveInfo->user_id)
							// ->where('status', 1)
								->where('flag', 1)
								->update(array('flag' => 0));

							// insert leave_histories
							// ----------------------
							$dh = array(
								'user_id' => $i->LeaveInfo->user_id,
								'leave_id' => $i->LeaveInfo->id,
								'action_date' => $date,
								'action_by' => $i->LeaveInfo->report_to,
								'action_remark' => '',
								'status' => 6,
								'flag' => 1,
							);
							$lh = new \IhrV2\Models\LeaveHistory($dh);
							$lh->save();
							$approved_cancel++;
						}

						// cancel (5) - no need
						// --------------------

						// apply cancel (4)
						// ----------------
						if ($status == 4) {

							// inactive current leave_histories
							// --------------------------------
							$update = \IhrV2\Models\LeaveHistory::where('leave_id', $i->LeaveInfo->id)
								->where('user_id', $i->LeaveInfo->user_id)
							// ->where('status', 1)
								->where('flag', 1)
								->update(array('flag' => 0));

							// insert leave_histories
							// ----------------------
							$dh = array(
								'user_id' => $i->LeaveInfo->user_id,
								'leave_id' => $i->LeaveInfo->id,
								'action_date' => $date,
								'action_by' => $i->LeaveInfo->user_id,
								'action_remark' => '',
								'status' => 4,
								'flag' => 1,
							);
							$lh = new \IhrV2\Models\LeaveHistory($dh);
							$lh->save();
							$apply_cancel++;
						}

						// rejected (3)
						// ------------
						if ($status == 3) {

							// update leave_approves
							// ---------------------
							$update = \IhrV2\Models\LeaveApprove::where('user_id', $i->LeaveInfo->user_id)
								->where('leave_id', $i->LeaveInfo->id)
								->where('flag', 1)
								->update(array('flag' => 0));

							// inactive current leave_histories
							// --------------------------------
							$update = \IhrV2\Models\LeaveHistory::where('leave_id', $i->LeaveInfo->id)
								->where('user_id', $i->LeaveInfo->user_id)
							// ->where('status', 1)
								->where('flag', 1)
								->update(array('flag' => 0));

							// insert leave_histories
							// ----------------------
							$dh = array(
								'user_id' => $i->LeaveInfo->user_id,
								'leave_id' => $i->LeaveInfo->id,
								'action_date' => $date,
								'action_by' => $i->LeaveInfo->report_to,
								'action_remark' => '',
								'status' => 3,
								'flag' => 1,
							);
							$lh = new \IhrV2\Models\LeaveHistory($dh);
							$lh->save();
							$rejected++;
						}
					}

					// no record found
					// ---------------
					else {

						// cancel (5)
						// ----------
						if ($status == 5) {

							// insert leave_histories
							// ----------------------
							$dh = array(
								'user_id' => $i->LeaveInfo->user_id,
								'leave_id' => $i->LeaveInfo->id,
								'action_date' => $date,
								'action_by' => $i->LeaveInfo->user_id,
								'action_remark' => '',
								'status' => 5,
								'flag' => 1,
							);
							$lh = new \IhrV2\Models\LeaveHistory($dh);
							$lh->save();
							$cancel++;
						}
					}

				}

			}
		}

		// return result
		// -------------
		$x = array(
			'total' => $total,
			'rejected' => $rejected,
			'apply_cancel' => $apply_cancel,
			'cancel' => $cancel,
			'approved_cancel' => $approved_cancel,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateLeaveApproved($year) {
		// set variables
		// -------------
		$total = 0;
		$update = 0;
		$report_to = 0;

		// select max(leave_app_id) from sleave_approved where year(date_from) = '2017'

		// 2016
		// min 26977
		// max 37196

		// 2017
		// min 34521
		// max 38023

		// next process 37288

		// last id 38081!!!!!!!

		// query sleave_approved
		// --------------------
		$q = \IhrV2\Models\MSleaveApprove::with(array('LeaveInfo' => function ($h) {
			$h->with('UserInfo');
		}))

		// ->whereYear('date_from', '=', $year)
			->whereBetween('leave_app_id', [37001, 38081])
			->get();

		// loop all records
		// ----------------
		if (count($q)) {
			$total = count($q);
			foreach ($q as $i) {

				$exist = \IhrV2\Models\LeaveApprove::where('leave_app_id', $i->leave_app_id)->first();
				if (empty($exist)) {

					// check leave id old
					// ------------------
					$check = \IhrV2\Models\LeaveApplication::where('leave_id', $i->leave_id)->first();
					if (!empty($check)) {

						// get rm
						// ------
						$rm = $this->user_repo->getRegionManager($check->sitecode);
						if (!empty($rm->RegionName->RegionManager)) {
							$report_to = $rm->RegionName->RegionManager->RegionManagerDetail->id;
						}

						// update leave_histories flag to 0
						// --------------------------------
						$update_his = \IhrV2\Models\LeaveHistory::where('leave_id', $check->id)
							->where('user_id', $check->user_id)
							->where('flag', 1)
							->update(array('flag' => 0));

						// insert new leave_histories
						// --------------------------
						$arr_his = array(
							'user_id' => $check->user_id,
							'leave_id' => $check->id,
							'action_date' => date('Y-m-d H:i:s'),
							'action_by' => $report_to, // rm id
							'action_remark' => '',
							'status' => 2, // approve
							'flag' => 1, // active
						);
						$sav_his = new \IhrV2\Models\LeaveHistory($arr_his);
						$sav_his->save();

						// insert new leave_approves
						// -------------------------
						$arr_app = array(
							'user_id' => $check->user_id,
							'leave_id' => $check->id,
							'date_action' => date('Y-m-d H:i:s'),
							'action_by' => $report_to,
							'date_value' => $i->date_value,
							'flag' => 1,
							'leave_app_id' => $i->leave_app_id,
						);
						$sav_app = new \IhrV2\Models\LeaveApprove($arr_app);
						$sav_app->save();
						$update++;
					}
				}
			}
		}

		// return result
		// -------------
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateLeaveAdd($year) {
		// set variables
		// -------------
		$total = 0;
		$update = 0;

		// query sleave_add
		// ----------------
		if (!empty($year)) {
			$q = \IhrV2\Models\MSleaveAdd::with(array('UserInfo'))
				->whereYear('date_action', '=', $year)
				->get();

			// $q = \IhrV2\Models\MSleaveAdd::with(array('UserInfo' => function($h) {
			//        $h->with('UserLatestContract');
			//    }))
			// ->whereYear('date_action', '=', $year)
			// ->get();

			if (count($q)) {
				$total = count($q);
				foreach ($q as $i) {

					// have link with users
					// --------------------
					if (!empty($i->UserInfo)) {

						// get user contract
						// -----------------
						$check_contract = \IhrV2\Models\UserContract::where('user_id', $i->UserInfo->id)->first();
						if (!empty($check_contract)) {
							$contract_id = $check_contract->id;
						} else {
							$contract_id = 0;
						}

						// insert leave_add
						// ----------------
						$arr = array(
							'user_id' => $i->UserInfo->id,
							'contract_id' => $contract_id,
							'leave_type_id' => $i['leave_type_id'],
							'type_id' => ($i['flg'] == 1) ? 1 : 2,
							'action_date' => $i['date_action'],
							'action_by' => $i->UserInfo->id,
							'total' => $i['total_add'],
							'reason' => $i['reason'],
							'status' => 1, // set as active
						);
						$sav = new \IhrV2\Models\LeaveAdd($arr);
						$sav->save();
						$update++;
					}
				}
			}
		}

		// return result
		// -------------
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateStaffContractInfo() {
		// query old contracts
		// -------------------
		$old_contract = \IhrV2\Models\MStaffContractInfo::get();
		if (count($old_contract) > 0) {
			foreach ($old_contract as $i) {

				// date from and date to is not empty
				// ----------------------------------
				if (!empty($i->date_from) && !empty($i->date_to)) {

					// check current contract
					// ----------------------
					// $curr_contract = \IhrV2\Models\UserContract::where('user_id', )

					// check if date end contract is greater than current
					// --------------------------------------------------
					// if (Carbon::parse($contract_end)->gt(Carbon::parse($check_contract->date_to))) {

					// }
				}
			}
		}
	}

	public function getMigrateLeaveRepApplication() {
		// set variables
		// -------------
		$total = 0;
		$update = 0;

		// min 6838
		// max 7775

		// query old
		// ---------
		$q = \IhrV2\Models\MLeaveRepApplication::with(array('UserInfo'))
			->whereIn('year', array('2016', '2017'))
		// ->whereBetween('leave_rep_id', [6800, 7750])
			->get();

		if (count($q) > 0) {
			$total = count($q);
			foreach ($q as $i) {

				if (!empty($i->UserInfo)) {
					$uid = $i->UserInfo->id;
					$sitecode = $i->UserInfo->sitecode;

					// check month
					// -----------
					$month = \IhrV2\Models\Month::where('name', $i['month'])->first();

					// month is valid
					// --------------
					if (!empty($month)) {

						// check reporting officer
						// -----------------------
						if (!empty($i->UserInfo->sitecode)) {

							// get rm
							// ------
							$rm = $this->user_repo->getRegionManager($i->UserInfo->sitecode);
							if (!empty($rm->RegionName->RegionManager)) {
								$report_to = $rm->RegionName->RegionManager->RegionManagerDetail->id;
							} else {
								$report_to = 0;
							}

							// get user contract
							// -----------------
							$check_contract = \IhrV2\Models\UserContract::where('user_id', $uid)
								->first();

							if (!empty($check_contract)) {
								$contract_id = $check_contract->id;
							} else {
								$contract_id = 0;
							}

							// insert leave_rep_applications
							// -----------------------------
							$arr = array(
								'user_id' => $uid,
								'contract_id' => $contract_id,
								'date_apply' => date('Y-m-d H:i:s'),
								'no_day' => $i['no_of_days'],
								'year_month' => $i['year'] . '-' . $month->no . '-01',
								'report_to' => $report_to,
								'instructed_by' => $i['instruct_by'],
								'location' => $i['work_place'],
								'reason' => $i['reason'],
								'notes' => $i['notes'],
								'sitecode' => $i->UserInfo->sitecode,
								'active' => 1,
								'leave_rep_id' => $i['leave_rep_id'],
							);
							$sav = new \IhrV2\Models\LeaveRepApplication($arr);
							$sav->save();
							$rep_id = $sav->id;

							// insert leave_rep_history
							// ------------------------
							$arr_his = array(
								'user_id' => $uid,
								'leave_rep_id' => $rep_id,
								'action_date' => date('Y-m-d H:i:s'),
								'action_by' => $uid,
								'action_remark' => '',
								'status' => 1, // pending
								'flag' => 1, // active
							);
							$sav_his = new \IhrV2\Models\LeaveRepHistory($arr_his);
							$sav_his->save();

							// insert leave_rep_attachments
							// ----------------------------
							if (!empty($i['attach_form'])) {
								$exp = explode('.', $i['attach_form']);
								$arr_att = array(
									'user_id' => $uid,
									'leave_rep_id' => $rep_id,
									'filename' => $exp[0],
									'ext' => $exp[1],
									'size' => 0,
									'thumb_name' => null,
									'status' => 1,
								);
								$sav_att = new \IhrV2\Models\LeaveRepAttachment($arr_att);
								$sav_att->save();
							}
							$update++;
						}
					}
				}
			}
		}

		// return result
		// -------------
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateLeaveRepApproved() {
		$total = 0;
		$update = 0;
		$report_to = 0;

		$q = \IhrV2\Models\MLeaveRepApprove::get();
		if (count($q) > 0) {
			$total = count($q);
			foreach ($q as $i) {

				// check leave rep id old
				// ----------------------
				$check = \IhrV2\Models\LeaveRepApplication::where('leave_rep_id', $i->leave_rep_id)->first();
				if (!empty($check)) {

					// get rm
					// ------
					$rm = $this->user_repo->getRegionManager($check->sitecode);
					if (!empty($rm->RegionName->RegionManager)) {
						$report_to = $rm->RegionName->RegionManager->RegionManagerDetail->id;
					} else {
						$report_to = 0;
					}

					// update leave_rep_histories flag to 0
					// ------------------------------------
					$update_history = \IhrV2\Models\LeaveRepHistory::where('leave_rep_id', $check->id)
						->where('user_id', $check->user_id)
						->where('flag', 1)
						->update(array('flag' => 0));

					// insert new leave_rep_history
					// ----------------------------
					$arr_his = array(
						'user_id' => $check->user_id,
						'leave_rep_id' => $check->id,
						'action_date' => date('Y-m-d H:i:s'),
						'action_by' => $report_to, // rm id
						'action_remark' => '',
						'status' => 2, // approve
						'flag' => 1, // active
					);
					$sav_his = new \IhrV2\Models\LeaveRepHistory($arr_his);
					$sav_his->save();

					// insert leave_rep_approves
					// -------------------------
					$arr = array(
						'user_id' => $check->user_id,
						'leave_rep_id' => $check->id,
						'date_action' => date('Y-m-d H:i:s'),
						'action_by' => $report_to,
						'total_day' => $i->date_value,
						'flag' => 1,
					);
					$sav = new \IhrV2\Models\LeaveRepApprove($arr);
					$sav->save();
					$update++;
				}
			}
		}
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	// update location_id at table sites
	// ---------------------------------
	public function getMigrateLocationID() {
		// set variables
		// -------------
		$total = 0;
		$update = 0;

		// query all sites (only location_id is 0)
		// ---------------------------------------
		$sites = \IhrV2\Models\Site::where('location_id', '=', null)->get();
		if (count($sites) > 0) {
			$total = count($sites);
			foreach ($sites as $i) {

				// query staff_site_location
				// -------------------------
				$old = \IhrV2\Models\MStaffSiteLocation::where('location_code', $i->code)
					->first();

				// match record in old
				// -------------------
				if (!empty($old)) {

					// update location_id
					// ------------------
					$update_site = \IhrV2\Models\Site::where('code', $old->location_code)
						->update(array('location_id' => $old->location_id));
					$update++;
				}
			}
		}
		// return result
		// -------------
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	// update sid at table users
	// -------------------------
	public function getMigrateUserID() {
		// set variables
		// -------------
		$total = 0;
		$update = 0;

		// query all staff (which sid still null)
		// --------------------------------------
		$user = \IhrV2\User::where('sid', '=', null)->where('group_id', 3)->get();
		if (count($user) > 0) {
			$total = count($user);
			foreach ($user as $i) {

				// query staff_personal_info
				// -------------------------
				$arr = array('staff_id', $i->staff_id, 'sitecode' => $i->sitecode);
				$check_old = \IhrV2\Models\MStaffPersonalInfo::with(array('StaffInfo' => function ($h) use ($arr) {
					$sitecode = $arr[1];
					$h->where('staff_id', $arr[0]);
					$h->with(array('LocationName' => function ($l) use ($sitecode) {
						$l->where('location_code', $sitecode);
					}));
				}))
					->where('ic', $i->icno)
					->first();

				// have record on old db
				// ---------------------
				if (!empty($check_old)) {

					// update sid on new db
					// --------------------
					$update_sid = \IhrV2\User::where('icno', $check_old->ic)
						->update(array('sid' => $check_old->sid));
					$update++;
				}
			}
		}

		// return result
		// -------------
		$x = array(
			'total' => $total,
			'update' => $update,
		);
		$resp = response()->json($x);
		return $resp;
	}

	public function getMigrateAttendanceLogRecord() {
		return 1;
	}

}
