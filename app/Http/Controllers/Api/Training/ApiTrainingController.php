<?php

namespace IhrV2\Http\Controllers\Api\Training;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Contracts\Api\Training\ApiTrainingInterface as ApiTrainingRepository;

class ApiTrainingController extends Controller
{

    protected $training;
    public function __construct(ApiTrainingRepository $training) {
        $this->training = $training;
    }

	// create training attendance
	// --------------------------
    public function getApiTtgAttCreate(Request $request) {
        $get = $this->training->getApiTtgAttCreate($request->all());
        return $get;
    }    

    // get active training
    // -------------------
    public function getApiTtgAttActive(Request $request) {
        $get = $this->training->getApiTtgAttActive($request->all());
        return $get;
    }    



}
