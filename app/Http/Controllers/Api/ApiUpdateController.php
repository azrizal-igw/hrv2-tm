<?php

namespace IhrV2\Http\Controllers\Api;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Contracts\Leave\LeaveInterface as LeaveRepository;
use IhrV2\Contracts\User\UserInterface as UserRepository;

class ApiUpdateController extends Controller
{

	protected $user_repo;
	protected $leave_repo;

	public function __construct(UserRepository $user_repo, LeaveRepository $leave_repo) {
		$this->user_repo = $user_repo;
		$this->leave_repo = $leave_repo;
	}


	// update user job
	// ---------------
	public function getApiUpdateUserJob() {

		// set variable
		// ------------
		$active_total = 0;
		$active_update = 0;
		$active_null = array();
		$inactive_total = 0;
		$inactive_update = 0;
		$inactive_null = array();

		// get all staff
		// -------------
		$users = \IhrV2\User::where(['group_id' => 3])->with(array('UserLatestJob2', 'UserInactiveJob'))->get();
		if (!empty($users)) {
			foreach ($users as $i) {

				// status active
				// -------------
				if ($i->status == 1) {
					$active_total++;

					// check user job
					// --------------
					if (!empty($i->UserLatestJob2)) {
						$job = $i->UserLatestJob2;

						// check flag
						// ----------
						if ($job->flag == 0) {

							// update flag user job
							// --------------------
					        $save = \IhrV2\Models\UserJob::where([
					        	'id' => $job->id,
					        ])
					        ->update(array('flag' => 1));
					        $active_update++;	
						}
					}

					// get user that not have job
					// --------------------------
					else {
						$active_null[] = $i->toArray();
					}					
				}

				// status inactive
				// ---------------
				else {
					$inactive_total++;

					// check user job
					// --------------
					if (!empty($i->UserInactiveJob)) {
						$inactive = $i->UserInactiveJob;

						// check flag
						// ----------
						if ($inactive->flag == 0) {

							// update flag user job
							// --------------------
					        $save = \IhrV2\Models\UserJob::where([
					        	'id' => $inactive->id,
					        ])
					        ->update(array('flag' => 1));
					        $inactive_update++;	
						}
					}

					// get user that not have job
					// --------------------------
					else {
						$inactive_null[] = $i->toArray();
					}						
				}
			}
		}

		// return result
		// -------------
		$arr = array(
			'active_total' => $active_total,
			'active_update' => $active_update,
			'active_null' => $active_null,
			'inactive_total' => $inactive_total,
			'inactive_update' => $inactive_update,
			'inactive_null' => $inactive_null,			
		);
		return response()->json($arr);
	}

	// update user contract
	// --------------------
	public function getApiUpdateUserContract() {

		// set variable
		// ------------
		$active_total = 0;
		$active_update = 0;
		$active_null = array();
		$inactive_total = 0;
		$inactive_update = 0;
		$inactive_null = array();

		// get all staff
		// -------------
		$users = \IhrV2\User::where(['group_id' => 3])->with(array('UserLatestContract', 'UserLastContract'))->get();
		if (!empty($users)) {
			foreach ($users as $i) {

				// status active
				// -------------
				if ($i->status == 1) {
					$active_total++;

					// check user contract
					// -------------------
					if (!empty($i->UserLatestContract)) {
						$contract = $i->UserLatestContract;

						// check flag
						// ----------
						if ($contract->flag == 0) {

							// update flag user job
							// --------------------
					        $save = \IhrV2\Models\UserContract::where([
					        	'id' => $contract->id,
					        ])
					        ->update(array('flag' => 1));
					        $active_update++;	
						}
					}

					// get user that not have contract
					// -------------------------------
					else {
						$active_null[] = $i->toArray();
					}
				}

				// status inactive
				// ---------------
				else {
					$inactive_total++;

					// check user contract
					// --------------
					if (!empty($i->UserLastContract)) {
						$inactive = $i->UserLastContract;

						// check flag
						// ----------
						if ($inactive->flag == 0) {

							// update flag user job
							// --------------------
					        $save = \IhrV2\Models\UserContract::where([
					        	'id' => $inactive->id,
					        ])
					        ->update(array('status' => 1, 'flag' => 1));
					        $inactive_update++;	
						}
					}

					// get user that not have job
					// --------------------------
					else {
						$inactive_null[] = $i->toArray();
					}	
				}								

			}

		}

		// return result
		// -------------
		$arr = array(
			'active_total' => $active_total,
			'active_update' => $active_update,
			'active_null' => $active_null,
			'inactive_total' => $inactive_total,
			'inactive_update' => $inactive_update,
			'inactive_null' => $inactive_null,			
		);
		return response()->json($arr);
	}


}


