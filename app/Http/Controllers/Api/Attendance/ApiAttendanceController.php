<?php

namespace IhrV2\Http\Controllers\Api\Attendance;

use Illuminate\Http\Request;
use IhrV2\Http\Requests;
use IhrV2\Http\Controllers\Controller;
use IhrV2\Contracts\Api\Attendance\ApiAttendanceInterface as ApiAttendanceRepository;

class ApiAttendanceController extends Controller
{    
    protected $att;
    public function __construct(ApiAttendanceRepository $att) {
        $this->att = $att;
    }

	// create attendance
	// -----------------
    public function getApiAttCreate(Request $request) {
        $get = $this->att->getApiAttCreate($request->all());
        return $get;
    }

    // check attendance
    // ----------------
    public function getApiAttCheck(Request $request) {
        $get = $this->att->getApiAttCheck($request->all());
        return $get;
    }

    // sync attendance
    // ---------------
    public function getApiAttSync(Request $request) {
        $get = $this->att->getApiAttSync($request->all());
        return $get;        
    }

    // file upload
    // -----------
    public function getApiAttLogUpload(Request $request) {
        $get = $this->att->getApiAttLogUpload($request->all());
        return $get;        
    }

    // check attendance sitecode
    // -------------------------
    public function getApiChkSitecode(Request $request) {
        $get = $this->att->getApiChkSitecode($request->all());
        return $get;        
    }

}
