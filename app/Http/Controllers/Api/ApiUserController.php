<?php namespace IhrV2\Http\Controllers\Api;

use IhrV2\Contracts\User\UserInterface as UserRepository;
use IhrV2\Http\Controllers\Controller;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class ApiUserController extends Controller {

    protected $user_repo;
    public function __construct(UserRepository $user_repo) {
        $this->user_repo = $user_repo;
    }

    // approve user photo
    // ------------------
    public function getUserPhotoApprove($url) 
    {
        // check the url
        // -------------
        try {

            // decrypt the url
            // ---------------
            $decrypted = Crypt::decrypt($url);

            // separate value from the url
            // ---------------------------
            $exp = explode('/', $decrypted);

            // count total value
            // -----------------
            if (count($exp) == 4) {

                // get value from url
                // ------------------
                $id = $exp[0];
                $uid = $exp[1];
                $sitecode = $exp[2];
                $report_to = $exp[3];

                // approve photo
                //--------------
                $data = array(
                    'remark' => 'Approved By Email', 
                    'type' => 2, 
                    'action_by' => $report_to 
                );
                $msg = $this->user_repo->getProcessPhoto($data, $id, $uid, $sitecode);
            }

            // invalid parameter
            // -----------------
            else {
                $msg = array('Parameter is Invalid.');
            }

            // return message
            // --------------
            return response()->json($msg[0]);
        }

        // url is invalid
        // --------------
         catch (DecryptException $e) {
            return response()->json('URL is Invalid.');
        }
    }

    // reject user photo
    // -----------------
    public function getUserPhotoReject($url) 
    {
        // check the url
        // -------------
        try {

            // decrypt the url
            // ---------------
            $decrypted = Crypt::decrypt($url);

            // separate value from the url
            // ---------------------------
            $exp = explode('/', $decrypted);

            // count total value
            // -----------------
            if (count($exp) == 4) {

                // get value from url
                // ------------------
                $id = $exp[0];
                $uid = $exp[1];
                $sitecode = $exp[2];
                $report_to = $exp[3];

                // reject photo
                //-------------
                $data = array(
                    'remark' => 'Rejected By Email', 
                    'type' => 3, 
                    'action_by' => $report_to
                );
                $msg = $this->user_repo->getProcessPhoto($data, $id, $uid, $sitecode);
            }

            // invalid parameter
            // -----------------
            else {
                $msg = array('Parameter is Invalid.');
            }

            // return message
            // --------------
            return response()->json($msg[0]);
        }

        // url is invalid
        // --------------
         catch (DecryptException $e) {
            return response()->json('URL is Invalid.');
        }
    }

}
