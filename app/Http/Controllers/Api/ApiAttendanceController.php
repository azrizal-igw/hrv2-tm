<?php namespace IhrV2\Http\Controllers\Api;

use IhrV2\Http\Controllers\Controller;
use IhrV2\Contracts\Attendance\AttendanceInterface as AttendanceRepository;

class ApiAttendanceController extends Controller {

    protected $att_repo;
    public function __construct(AttendanceRepository $att_repo) {
        $this->att_repo = $att_repo;
    }

    // check daily attendance summary
    // ------------------------------
    public function getChkAttSummary($date) {
        $exe = $this->att_repo->getChkAttSummary($date);
        return response()->json($exe);
    }

}
