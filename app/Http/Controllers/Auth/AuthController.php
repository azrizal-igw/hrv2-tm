<?php namespace IhrV2\Http\Controllers\Auth;

use IhrV2\User;
use Validator;
use IhrV2\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Letter\LetterInterface;
use IhrV2\Contracts\Email\EmailInterface;
use Event;
use IhrV2\Events\LoginEvent;

class AuthController extends Controller
{



    protected $username = 'staff_id';



    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';
    // protected $redirectTo = 'home';


    // protected function authenticated()
    // {
    //     // if (Auth::user()->role==0) {
    //     //     return redirect('/volunteer') ;
    //     // } else {
    //     //     return redirect('/donor');
    //     // }
    //     return redirect('home');
    // }




    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

    protected $user_repo;
    protected $leave_repo;
    protected $email_repo;
    protected $letter_repo;

    public function __construct(UserInterface $user_repo, LeaveInterface $leave_repo, EmailInterface $email_repo, LetterInterface $letter_repo)
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
        $this->email_repo = $email_repo;
        $this->letter_repo = $letter_repo;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'staff_id' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ],
        [
            'staff_id.required' => 'Please insert Staff ID.'
        ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'staff_id' => $data['staff_id'],
            'email' => $data['email'],
            'api_token' => str_random(60),
            'group_id' => 3,
            'password' => bcrypt($data['password']),
        ]);
    }





    // disable register page
    public function showRegistrationForm()
    {
        return redirect('/');
    }

    public function register()
    {
        return redirect('/');
    }



/**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'staff_id' => 'required|exists:users,staff_id,status,1', 
            'password' => 'required',
        ],
        [
            'staff_id.required' => 'The Staff ID field is required.',
            'staff_id.exists' => 'The selected Staff ID is invalid or the account has been disabled.'
        ]      
        );
    }





    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (auth()->guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {

            // check if session job is not exist
            // ---------------------------------
            if (!session()->has('job')) {

                // get user jobs
                // -------------
                $job = $this->user_repo->getUserJobByID(auth()->user()->id);

                // store session user jobs
                // -----------------------
                session()->put('job', $job);
            }

            // check if session position name is not exist
            // -------------------------------------------
            if (!session()->has('position_name')) {

                // get position name
                // -----------------
                $position = $this->user_repo->getPositionByID(session()->get('job')['position_id']);

                // store session position name
                // ---------------------------
                session()->put('position_name', $position->name);                  
            }

            // site supervisor
            // ---------------
            if (auth()->user()->group_id == 3) {

                // check if session contract is not exist
                // --------------------------------------
                if (!session()->has('contract')) {

                    // query contract info
                    // --------------------
                    $contract = $this->user_repo->getContractByIDSitecode(auth()->user()->id, auth()->user()->sitecode);

                    // contract is empty
                    // -----------------
                    if (empty($contract)) {

                        dd('Contract is not exist. Please contact HR.');

                        // send email notification
                        // -----------------------
                        // $this->email_repo->getEmailNoContract();
                    }

                    // have contract
                    // -------------
                    else {

                        // save contract session
                        // ---------------------
                        session()->put('contract', $contract);

                        // check contract if expired
                        // -------------------------
                        $expired = 0;
                        if (!empty($contract)) {
                            $check = $this->leave_repo->CheckIfExpired($contract->date_to);
                            if ($check == 1) {
                                $expired = 1;

                                dd('Contract is expired. Please contact HR.');

                            }
                        }
                    }

                    // save expired session
                    // --------------------
                    session()->put('expired', $expired);
                }   

                // check if session site name is not exist
                // ---------------------------------------
                if (!session()->has('site_name')) {

                    // query site info
                    // ---------------
                    $site = $this->user_repo->getSiteByCode(auth()->user()->sitecode);

                    // store site name session
                    // -----------------------
                    session()->put('site_name', $site->name.' '.$site->code);
                }

                // check if session rm info is not exist
                // -------------------------------------
                if (!session()->has('rm_info')) {

                    // query rm info
                    // -------------
                    $rm = $this->user_repo->getRegionBySitecode(auth()->user()->sitecode);

                    // rm exist
                    // --------
                    if (!empty($rm['region_manager_info'])) { 

                        // get rm detail
                        // -------------
                        $rm_detail = $rm['region_manager_info']['region_manager_detail'];
                        $rm_info = array(
                            'id' => $rm_detail['id'], 
                            'name' => $rm_detail['name'], 
                            'email' => $rm_detail['email'], 
                            'region_id' => $rm['region_manager_info']['region_id']
                        );
                        session()->put('rm_info', $rm_info);
                    }
                }

                // check session letter
                // --------------------
                if (!session()->has('letters')) {
                    $letters = $this->letter_repo->getLetterAllActive(auth()->user()->id, auth()->user()->sitecode);
                    if (!empty($letters)) {
                        session()->put('letters', $letters->toArray());
                    }
                }

                // check staff photo
                // -----------------
                if (!session()->has('photo')) {
                    $photo = $this->user_repo->getUserPhotoOne(auth()->user()->id, auth()->user()->sitecode);
                    if (!empty($photo)) {
                        session()->put('photo', $photo->toArray());
                    }
                }                
            }

            // region manager
            // --------------
            else if (in_array(auth()->user()->group_id, array(4,6,7))) {

                // check if session region name is not exist
                // ------------------------------------------
                if (!session()->has('region_name')) {

                    // get region name
                    // ---------------
                    $region = $this->user_repo->getRegionInfo(session()->get('job')['region_id']);

                    // store session region name
                    // -------------------------
                    session()->put('region_name', $region->name); 
                }   
            }

            // update user last login
            // ----------------------
            $user = $this->user_repo->getUserByID(auth()->user()->id);
            Event::fire(new LoginEvent($user));

            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }
        return $this->sendFailedLoginResponse($request);
    }
    



    public function logout()
    {
        auth()->guard($this->getGuard())->logout();

        // destroy all sessions
        session()->flush();
        
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }




    // status must active (1) to login
    protected function getCredentials(Request $request)
    {
        // return $request->only($this->loginUsername(), 'password');
        return [
            'staff_id' => $request->{$this->loginUsername()}, 
            'password' => $request->password, 
            'status' => 1
        ];
    }




}


