<?php

namespace IhrV2\Http\Controllers\Auth;

use IhrV2\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Contracts\Auth\PasswordBroker;

class PasswordController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */

    use ResetsPasswords;



    protected $redirectTo = '/login';


    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }



    protected function validateSendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'personal_email' => 'required|email',
        ], 
        [
            'personal_email.required' => 'Please insert Personal Email Address.',
        ]
        );
    }

    protected function getSendResetLinkEmailCredentials(Request $request)
    {
        return $request->only('personal_email');
    }

    protected function getSendResetLinkEmailFailureResponse($response)
    {
        return redirect()->back()->withErrors(['personal_email' => trans($response)]);
    }

    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }

        $personal_email = $request->input('email');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'personal_email'));
        }

        if (view()->exists('auth.passwords.reset')) {
            return view('auth.passwords.reset')->with(compact('token', 'personal_email'));
        }

        return view('auth.reset')->with(compact('token', 'personal_email'));
    }

    protected function getResetValidationRules()
    {
        return [
            'token' => 'required',
            'personal_email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }


    protected function getResetCredentials(Request $request)
    {
        return $request->only(
            'personal_email', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Get the response for after a successful password reset.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse($response)
    {
        Auth::logout();
        return redirect($this->redirectPath())->with('status', trans($response));
    }

    /**
     * Get the response for after a failing password reset.
     *
     * @param  Request  $request
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetFailureResponse(Request $request, $response)
    {
        return redirect()->back()
            ->withInput($request->only('personal_email'))
            ->withErrors(['personal_email' => trans($response)]);
    }



}
