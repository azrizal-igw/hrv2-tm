<?php namespace IhrV2\Http\Controllers;

use Image;

class LibraryController extends Controller 
{
    // show general image
    // ------------------
    public function showImage($slug) {
        $storagePath = storage_path('app/images/' . $slug);
        return Image::make($storagePath)->response();
    }

    // get path of attachment
    // ----------------------
    public function getCheckPath($name, $slug, $image = false, $thumb = false) 
    {
        // get old path
        // ------------
        $path = storage_path('app/files/' . $name . '/' . $slug);

        // check extension name
        // --------------------
        $exp = explode('.', $slug);
        if (in_array($exp[1], array('pdf', 'xls', 'xlsx'))) {
            $image = false;
        }

        // file at old path
        // ----------------
        if (file_exists($path)) {

            // is image
            // --------
            if ($image == true) {

                // resize if thumbnail
                // -------------------
                if ($thumb == true) {

                    // resize the image to a height of 200 and constrain aspect ratio (auto width)
                    // ----------------------------------------------------------------------------
                    return Image::make($path)->resize(null, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->response();
                }

                // display original size
                // ---------------------
                else {
                    return Image::make($path)->response();
                }
            }

            // is file
            // -------
            else {
                return response()->file($path);
            }
        }

        // file at new path
        // ----------------
        else {
            $exp = explode('_', $slug);
            $year_month = substr($exp[0], 0, 6);
            $new_path = storage_path('app/files/' . $name . '/' . $year_month . '/' . $slug);

            // image exist on server
            // ---------------------
            if (file_exists($new_path)) {

                // is image
                // --------
                if ($image == true) {

                    // resize if thumbnail
                    // -------------------
                    if ($thumb == true) {

                        // resize the image to a height of 200 and constrain aspect ratio (auto width)
                        // ----------------------------------------------------------------------------
                        return Image::make($new_path)->resize(null, 200, function ($constraint) {
                            $constraint->aspectRatio();
                        })->response();
                    }

                    // display original size
                    // ---------------------
                    else {
                        return Image::make($new_path)->response();
                    }
                }

                // is file
                // -------
                else {
                    return response()->file($new_path);
                }
            }

            // image not found
            // ---------------
            else {
                return response()->file(storage_path('app/images/notavailable.png'));
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Staff Photo Attachment
    |--------------------------------------------------------------------------
     */

    // show user photo
    // ---------------
    public function showUserPhoto($slug) {
        return $this->getCheckPath('user', $slug, true);
    }

    // show user photo thumbnail
    // -------------------------
    public function showUserPhotoThumb($slug) {
        return $this->getCheckPath('user/thumb', $slug, true, true);
    }

    /*
    |--------------------------------------------------------------------------
    | Leave Attachment
    |--------------------------------------------------------------------------
     */

    // show leave attachment
    // ---------------------
    public function showLeaveAttachment($slug) {
        return $this->getCheckPath('leave', $slug, true);
    }

    // show leave attachment thumbnail
    // -------------------------------
    public function showLeaveAttachmentThumb($slug) {
        return $this->getCheckPath('leave/thumb', $slug, true, true);
    }

    /*
    |--------------------------------------------------------------------------
    | Attendance Remark Attachment
    |--------------------------------------------------------------------------
     */

    // show attendance remark attachment
    // ---------------------------------
    public function showAttRemAttach($slug) {
        return $this->getCheckPath('attendance/remark', $slug, true);
    }

    // show attendance remark attachment thumbnail
    // -------------------------------------------
    public function showAttRemAttachThumb($slug) {
        return $this->getCheckPath('attendance/remark/thumb', $slug, true, true);
    }

    /*
    |--------------------------------------------------------------------------
    | Attendance Manual Attachment
    |--------------------------------------------------------------------------
     */

    // show attendance manual attachment
    // ---------------------------------
    public function showAttManualAttach($slug) {
        return $this->getCheckPath('attendance/manual', $slug, true);
    }

    // show attendance manual attachment thumbnail
    // -------------------------------------------
    public function showAttManualAttachThumb($slug) {
        return $this->getCheckPath('attendance/manual/thumb', $slug, true, true);
    }

    /*
    |--------------------------------------------------------------------------
    | Letter Attachment
    |--------------------------------------------------------------------------
     */

    // show letter attachment
    // ----------------------
    public function showLetterAttachment($slug) {
        return $this->getCheckPath('letter', $slug);
    }

    /*
    |--------------------------------------------------------------------------
    | Misc Attachment (Work Hour/)
    |--------------------------------------------------------------------------
     */

    // show misc attachment
    // --------------------
    public function showMiscAttachment($slug) {
        return $this->getCheckPath('misc', $slug, true);
    }

    // show misc attachment thumbnail
    // ------------------------------
    public function showMiscAttachmentThumb($slug) {
        return $this->getCheckPath('misc/thumb', $slug, true, true);
    }

    /*
    |--------------------------------------------------------------------------
    | Training Attachment
    |--------------------------------------------------------------------------
     */

    // show training attachment
    // ------------------------
    public function showTrainingAttachment($slug) {
        return $this->getCheckPath('training', $slug, true);
    }

    // show training attachment thumbnail
    // ----------------------------------
    public function showTrainingAttachmentThumb($slug) {
        return $this->getCheckPath('training/thumb', $slug, true, true);
    }

    /*
    |--------------------------------------------------------------------------
    | Attendance Mobile Log
    |--------------------------------------------------------------------------
     */

    // show log file
    // -------------
    public function showAttGpsLog($slug) {
        return $this->getCheckPath('attendance/log', $slug);
    }


}
