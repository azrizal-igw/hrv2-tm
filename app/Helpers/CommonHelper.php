<?php

namespace IhrV2\Helpers;

class CommonHelper
{

    // to shorten the text
    // -------------------
    public static function shorten($string, $limit = 100, $suffix = '...')
    {
        if (strlen($string) < $limit) {
            return $string;
        }
        return substr($string, 0, $limit) . $suffix;
    }

    // readable file size
    // ------------------
    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }
        return round($bytes, 2) . ' ' . $units[$i];
    }

    // display number with ordinal suffix
    // ----------------------------------
    public static function ordinal($number)
    {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        if ((($number % 100) >= 11) && (($number % 100) <= 13)) {
            return $number . 'th';
        } else {
            return $number . $ends[$number % 10];
        }
    }

    // check session
    // -------------
    public static function checkSession($x, $default = null)
    {

        // have session
        // ------------
        if (session()->has($x)) {
            $y = session()->get($x);
        }

        // no session
        // ----------
        else {
            if (!empty($default)) {
                $y = $default;
            } else {
                $y = null;
            }
        }
        return $y;
    }

    // check search session
    // --------------------
    public static function checkSessionPost($x, $y, $z)
    {
        if (!empty($x) && $y == 0) {
            session()->put($z, $x);
            $i = session()->get($z);
        } else {
            session()->forget($z);
            $i = null;
        }
        return $i;
    }

    // return message
    // --------------
    public static function getMessage($type)
    {
        $msg = null;
        switch ($type) {
            case 'leave_invalid_date':
                $msg = 'Selected Date is Invalid.';
                break;
            case 'leave_empty_entitled':
                $msg = 'Entitled Leave is Empty.';
                break;
            case 'leave_less_entitled':
                $msg = 'Total selected days is more than Entitled Leave.';
                break;
            case 'leave_empty_balance':
                $msg = 'Balance Leave is Empty.';
                break;
            case 'leave_less_balance':
                $msg = 'Total selected days is more than Balance Leave.';
                break;
            case 'leave_end_expired':
                $msg = 'End Date is over Contract.';
                break;
            case 'leave_out_contract':
                $msg = 'Apply Date is not within Contract.';
                break;
            case 'leave_no_rm':
                $msg = 'Regional Manager is not Set.';
                break;
            case 'leave_on_partner':
                $msg = 'Requested Date falls on Partner Leave. Please try again.';
                break;
            case 'leave_empty_date':
                $msg = 'Leave is added but no available date.';
                break;
            case 'leave_fail_email':
                $msg = 'Leave successfully added and fail send email to RM.';
                break;
            case 'leave_success_apply':
                $msg = 'Leave successfully added and send to Regional Manager.';
                break;
            case 'leave_max_unplan':
                $msg = 'Unplan Leave reach limit. Please contact RM for further information.';
                break;
            case 'leave_minus_balance':
                $msg = 'Data incorrect.';
                break;
            case 'leave_max_attach':
                $msg = 'Upload attachment reach limit.';
                break;
            case 'leave_success_attach':
                $msg = 'Attachment successfully uploaded.';
                break;
            case 'leave_exist_process':
                $msg = 'Leave already Processed.';
                break;
            case 'leave_not_exist':
                $msg = 'Leave not Exist.';
                break;
            case 'rl_success_apply':
                $msg = 'RL Request successfully Added and Send to RM.';
                break;
            case 'rl_out_contract':
                $msg = 'Work/Start Date is not within Contract. Please try again.';
                break;
            case 'rl_exist_today':
                $msg = 'Today Request already Exist. Please apply Next Day.';
                break;



            default:
                $msg = 'Unknown';
                break;
        }
        return $msg;
    }
}
