<?php

namespace IhrV2\Helpers;

use Carbon\Carbon;

class AttendanceHelper
{


    // recalculate total time
    // ----------------------
    public static function getTotalHours($h, $m, $s)
    {
        // convert minutes to seconds
        // --------------------------
        $minutes = $m * 60;
        $total = $minutes + $s;

        // calculate seconds
        // -----------------
        $hours = floor($total / 3600);
        $minutes = floor(($total / 60) % 60);
        $seconds = $total % 60;

        // combine hours from second
        // -------------------------
        $total_hrs = $h + $hours;

        // return time
        // -----------
        return array('total_hours' => $total_hrs, 'total_minutes' => $minutes, 'total_seconds' => $seconds);
    }

    // compare 2 date and get the different
    // ------------------------------------
    public static function getCompareDateTime($date1, $date2, $limit)
    {
        $check_in = Carbon::createFromFormat('Y-m-d H:i:s', $date1)->format('H:i:s');
        $limit_in = Carbon::createFromFormat('H:i:s', $limit['time_in'])->format('H:i:s');

        // get start date
        // --------------
        $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $date1);

        // get date end
        // ------------
        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $date2);

        // get days
        // --------
        $days = $startDate->diffInDays($endDate);

        // get hours
        // ---------
        $hours = $startDate->copy()->addDays($days)->diffInHours($endDate);

        // get minute
        // ----------
        $minutes = $startDate->copy()->addDays($days)->addHours($hours)->diffInMinutes($endDate);

        // get seconds
        // -----------
        $seconds = $startDate->copy()->addDays($days)->addHours($hours)->addMinutes($minutes)->diffInSeconds($endDate);

        // get long time
        // -------------
        $long_time = sprintf("%02d", $hours) . ':' . sprintf("%02d", $minutes) . ':' . sprintf("%02d", $seconds);

        // check if total is 9 hours
        // -------------------------
        if ($hours < 9) {
            $time = '<span class="text-danger">' . $long_time . '</span>';
        } else {
            $time = $long_time;
        }
        return array('time' => $time, 'hour' => $hours, 'minute' => $minutes, 'second' => $seconds);
    }

    // compare time mcmc
    // -----------------
    public static function getCompareMcmc($date1, $date2, $limit)
    {
        // get time in and out
        // -------------------
        $check_in = Carbon::createFromFormat('Y-m-d H:i:s', $date1)->format('H:i:s');
        $check_out = Carbon::createFromFormat('Y-m-d H:i:s', $date2)->format('H:i:s');

        // get limit in and out
        // --------------------
        $limit_in = Carbon::createFromFormat('H:i:s', $limit['time_in'])->format('H:i:s');
        $limit_out = Carbon::createFromFormat('H:i:s', $limit['time_out'])->format('H:i:s');

        // check if out is greater than limit in
        // ex: in 08:30 am | out 8:45 am | limit in 9:00 am
        // ------------------------------------------------
        if (Carbon::parse($check_out)->gt(Carbon::parse($limit_in))) {

            // if end date from the limit time (6:00PM >)
            // ------------------------------------------
            if (Carbon::parse($check_out)->gt(Carbon::parse($limit_out))) {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $date2)->format('Y-m-d');
                $datetime = $date . ' ' . $limit_out;
                $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $datetime);
            } else {
                $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $date2);
            }

            // if start date want from the limit time (< 9:00AM)
            // -------------------------------------------------
            if (Carbon::parse($check_in)->lt(Carbon::parse($limit_in))) {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $date1)->format('Y-m-d');
                $datetime = $date . ' ' . $limit_in;
                $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $datetime);
            } else {
                $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $date1);
            }
        }

        // time out is less than limit in
        // ------------------------------
        else {
            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $date1);
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $date2);
        }

        // get days
        // --------
        $days = $startDate->diffInDays($endDate);

        // get hours
        // ---------
        $hours = $startDate->copy()->addDays($days)->diffInHours($endDate);

        // get minute
        // ----------
        $minutes = $startDate->copy()->addDays($days)->addHours($hours)->diffInMinutes($endDate);

        // get seconds
        // -----------
        $seconds = $startDate->copy()->addDays($days)->addHours($hours)->addMinutes($minutes)->diffInSeconds($endDate);

        // get long time
        // -------------
        $long_time = sprintf("%02d", $hours) . ':' . sprintf("%02d", $minutes) . ':' . sprintf("%02d", $seconds);

        // check if total is 9 hours
        // -------------------------
        if ($hours < 9) {
            $time = '<span class="text-danger">' . $long_time . '</span>';
        } else {
            $time = $long_time;
        }
        return array('time' => $time, 'hour' => $hours, 'minute' => $minutes, 'second' => $seconds);
    }

    // check attendance status
    // -----------------------
    public static function checkAttStatus($arr, $type_id, $limit, $att_gps)
    {
        $att_time = ($type_id == 0) ? $arr->att_in : $arr->att_out;
        $att_status = ($type_id == 0) ? $arr->att_status_in : $arr->att_status_out;
        $att_timestamp = ($type_id == 0) ? $arr->att_timestamp_in : $arr->att_timestamp_out;
        $att_log_id = ($type_id == 0) ? $arr->att_id_in : $arr->att_id_out;
        $ip_add = ($type_id == 0) ? $arr->ip_add_in : $arr->ip_add_out;

        // get timestamp
        // -------------
        $timestamp = Carbon::parse($att_timestamp)->format('Y-m-d g:i:s A');

        // check if status from mobile
        // ---------------------------
        if (strpos($att_status, 'Mobile') !== false) {
            if (!empty($att_gps)) {
                foreach ($att_gps as $a) {
                    if ($a['att_log_id'] == $att_log_id) {

                        // check status color
                        // ------------------
                        if (in_array($a['flag'], array(1))) {
                            $color = 'success';
                        } 
                        else {
                            $color = 'danger';
                        }

                        // check using qrcode
                        // ------------------
                        if (!empty($a['qr_code'])) {
                            $method = 'QR Code';
                            $exp = explode('#', $a['qr_code']);

                            // check qrcode status
                            // -------------------
                            if (!empty($a['att_gps_status'])) {
                                if ($a['flag'] != 1) {
                                    $remark = '<span class="text-danger">'.$a['att_gps_status']['name'].'</span>';
                                } 
                                else {
                                    $remark = $a['att_gps_status']['name'];
                                }
                            } 
                            else {
                                $remark = '<span class="text-danger">Unknown</span>';
                            }

                            // get location
                            // ------------
                            $location = $a['location'];

                            // set tooltip for qrcode
                            // ----------------------
                            $status_icon = 'fa fa-qrcode';
                            $status_color = $color;
                            $label1 = 'Sitecode';
                            $label2 = 'Status';
                        // $status_content = 'QR Code: '.$exp[2].'<br>Date Time: '.$status_new;
                        }

                        // check using gps
                        // ---------------
                        else {
                            $method = 'GPS';

                            // location is empty
                            // -----------------
                            if (empty($a['location'])) {

                                // check flag
                                // ----------
                                if ($a['flag'] == 3) {
                                    $location = 'GPS Fake';
                                } else {
                                    $location = '<span class="text-danger">Empty</span>';
                                }
                                $color = 'danger';
                            }

                            // have location
                            // -------------
                            else {

                                // check flag
                                // ----------
                                if ($a['flag'] == 3) {
                                    $color = 'danger';
                                    $location = 'GPS Fake';
                                } else {
                                    $color = 'warning';
                                    $location = $a['location'];
                                }
                            }

                            // set tooltip for gps
                            // -------------------
                            $status_icon = 'fa fa-map-marker';
                            $status_color = $color;
                            // $status_content = 'Location: '.$location.'<br>Remark: '.$a['remark'].$status_notes;

                            // get remark
                            // ----------
                            if (!empty($a['remark'])) {
                                $remark = $a['remark'];
                            } else {
                                $remark = '-';
                            }

                            $label1 = 'Location';
                            $label2 = 'Remark';
                        }

                        // get accuracy
                        // ------------
                        if (!empty($a['accuracy'])) {
                            $accuracy = $a['accuracy'];
                        } 
                        else {
                            $accuracy = '-';
                        }

                        // get model
                        // ---------
                        if (!empty($a['model'])) {
                            $model = $a['model'];
                        } 
                        else {
                            $model = '<span class="text-danger">Empty</span>';
                        }

                        // get transfer
                        // ------------
                        if ($a['transfer'] == 1) {
                            $transfer = 'Direct';
                        } else {
                            $transfer = '<span class="text-danger">Indirect</span>';
                        }

                        // get route for map
                        // -----------------
                        $route = route('sv.attendance.daily.map', array($a['id']));
                        $id = $a['id'];

                        // set content
                        // -----------
                        $status_content =  "<div class='row'>";
                        $status_content .= "    <dl class='dl'>";
                        $status_content .= "        <dt class='col-lg-3'>Method</dt>";
                        $status_content .= "        <dd class='col-lg-9'>$method</dd>";
                        $status_content .= "        <dt class='col-lg-3'>$label1</dt>";
                        $status_content .= "        <dd class='col-lg-9'>$location</dd>";
                        $status_content .= "        <dt class='col-lg-3'>$label2</dt>";
                        $status_content .= "        <dd class='col-lg-9'>$remark</dd>";
                        $status_content .= "        <dt class='col-lg-3'>Transfer</dt>";
                        $status_content .= "        <dd class='col-lg-9'>".$transfer."</dd>";
                        $status_content .= "        <dt class='col-lg-3'>Phone Model</dt>";
                        $status_content .= "        <dd class='col-lg-9'>".$model."</dd>";

                        if (auth()->user()->group_id != 3) {
                            $status_content .= "        <dt class='col-lg-3'>IP Address</dt>";
                            $status_content .= "        <dd class='col-lg-9'>".$ip_add."</dd>";                                
                        }
                                          
                        $status_content .= "    </dl>";
                        $status_content .= "</div>";
                    }
                }
            }
        }

        // attendance system
        // -----------------
        else {
            $status = explode(', ', $att_status);
            if (count($status) == 4) {

                // get method punch
                // ----------------
                $method = 'Attendance System';

                // server time
                // -----------
                if (strpos($status[2], 'server') !== false) {
                    $status_icon = 'fa fa-check-circle';
                    $status_color = 'success';
                    $transfer = 'Direct';
                }

                // local time
                // ----------
                else if (strpos($status[2], 'local') !== false) {
                    if (strpos($status[3], 'indirect') !== false) {
                        $status_icon = 'fa fa-exclamation-triangle';
                        $status_color = 'danger';
                        $transfer = '<span class="text-danger">Indirect</span>';
                    } 
                    else {
                        if ($type_id == 1) {
                            if (Carbon::parse($att_timestamp)->format('H:i:s') < $limit) {                                
                                $status_icon = 'fa fa-exclamation-triangle';
                                $status_color = 'danger';
                                $transfer = '<span class="text-danger">Indirect</span>';
                            } 
                            else {
                                if (Carbon::parse($att_timestamp)->diffInMinutes(Carbon::parse($att_time)) > 10) {
                                    $status_icon = 'fa fa-exclamation-triangle';
                                    $status_color = 'danger';
                                    $transfer = '<span class="text-danger">Indirect</span>';
                                }
                                else {
                                    $status_icon = 'fa fa-check-circle';
                                    $status_color = 'success';
                                    $transfer = 'Direct';                                   
                                }
                            }
                        } 
                        else {
                            if (Carbon::parse($att_timestamp)->format('H:i:s') > $limit) {
                                $status_icon = 'fa fa-exclamation-triangle';
                                $status_color = 'danger';
                                $transfer = '<span class="text-danger">Indirect</span>';
                            } 
                            else {
                                if (Carbon::parse($att_timestamp)->diffInMinutes(Carbon::parse($att_time)) > 10) {
                                    $status_icon = 'fa fa-exclamation-triangle';
                                    $status_color = 'danger';
                                    $transfer = '<span class="text-danger">Indirect</span>';
                                }
                                else {
                                    $status_icon = 'fa fa-check-circle';
                                    $status_color = 'success';
                                    $transfer = 'Direct';                                    
                                }
                            }
                        }
                    }
                }

                // attendance manual
                // -----------------
                else if (strpos($status[2], 'IHRV2') !== false) {
                    $method = 'Attendance Manual';
                    $status_icon = 'fa fa-pencil';
                    $status_color = 'default';
                    $transfer = 'Manual';
                } 
                else {
                    $status_icon = 'fa fa-question-circle';
                    $status_color = 'danger';
                    $transfer = 'Unknown';
                }

                // set content
                // -----------
                $status_content =  "<div class='row'>";
                $status_content .= "    <dl class='dl'>";
                $status_content .= "        <dt class='col-lg-3'>Method</dt>";
                $status_content .= "        <dd class='col-lg-9'>".$method."</dd>";
                $status_content .= "        <dt class='col-lg-3'>Transfer</dt>";
                $status_content .= "        <dd class='col-lg-9'>".$transfer."</dd>";

                if ($transfer != 'Direct') {
                    $status_content .= "        <dt class='col-lg-3'>Timestamp</dt>";
                    $status_content .= "        <dd class='col-lg-9'>".$timestamp."</dd>";
                }

                if (auth()->user()->group_id != 3) {
                    $status_content .= "        <dt class='col-lg-3'>IP Address</dt>";
                    $status_content .= "        <dd class='col-lg-9'>".$ip_add."</dd>";                    
                }
              
                $status_content .= "    </dl>";
                $status_content .= "</div>";
            } 
            else {
                $status_icon = 'fa fa-times-circle';
                $status_color = 'danger';
                $status_content =  "<div class='row'>";
                $status_content .= "    <dl class='dl'>";
                $status_content .= "        <dt class='col-lg-3'>Method</dt>";
                $status_content .= "        <dd class='col-lg-9'>Attendance System</dd>";
                $status_content .= "        <dt class='col-lg-3'>Transfer</dt>";
                $status_content .= "        <dd class='col-lg-9'><span class=\"text-danger\">Invalid Status</span></dd>";
                $status_content .= "    </dl>";
                $status_content .= "</div>";
            }
        }
        
        // result result
        // -------------
        return array('icon' => $status_icon, 'color' => $status_color, 'content' => $status_content);
    }

    // get attendance working hour
    // ---------------------------
    public function getAttWorkHour()
    {

    }


}
