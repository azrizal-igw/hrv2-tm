<?php namespace IhrV2\Helpers;

use Carbon\Carbon;

class ErpHelper
{
    public static function checkDate($x)
    {
        if (strlen($x) != 10) {
            $d = '0000-00-00';
        } else {
            $d = Carbon::createFromFormat('m/d/Y', $x)->format('Y-m-d');
        }
        return $d;
    }

    public static function checkDateTime($x)
    {
        if (!empty($x)) {
            if (strlen($x) > 10) {
                $d = Carbon::createFromFormat('m/d/Y H:i:s', $x)->format('Y-m-d H:i:s');
            } else {
                $d = '0000-00-00 00:00:00';
            }
        } else {
            $d = '0000-00-00 00:00:00';
        }
        return $d;
    }

    public static function StatusEmployment($x)
    {
        if ($x == 'Active') {
            $y = 1;
        } elseif ($x == 'Not Active') {
            $y = 2;
        } elseif ($x == 'Transfer') {
            $y = 3;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function TitleName($x)
    {
        if ($x == 'Encik') {
            $y = 1;
        } elseif ($x == 'Cik') {
            $y = 2;
        } elseif ($x == 'Puan') {
            $y = 3;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function GenderName($x)
    {
        if ($x == 'Male') {
            $y = 1;
        } elseif ($x == 'Female') {
            $y = 2;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function MaritialStatus($x)
    {
        if ($x == 'Single') {
            $y = 1;
        } elseif ($x == 'Married') {
            $y = 2;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function RaceName($x)
    {
        if ($x == 'Melayu') {
            $y = 1;
        } elseif ($x == 'Cina') {
            $y = 2;
        } elseif ($x == 'India') {
            $y = 3;
        } elseif ($x == 'Lain-Lain') {
            $y = 4;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function ReligionName($x)
    {
        if ($x == 'Islam') {
            $y = 1;
        } elseif ($x == 'Kristian') {
            $y = 2;
        } elseif ($x == 'Buddha') {
            $y = 3;
        } elseif ($x == 'Hindu') {
            $y = 4;
        } elseif ($x == 'Lain-Lain') {
            $y = 5;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function Nationality($x)
    {
        if ($x == 'Malaysia' || $x == 'Warganegara') {
            $y = 1;
        } elseif ($x == 'Bukan Warganegara') {
            $y = 2;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function JobStatus($x)
    {
        if ($x == 'Contract') {
            $y = 1;
        } elseif ($x == 'Permanent') {
            $y = 2;
        } else {
            $y = 0;
        }
        return $y;
    }

    // get screenshots from erp tables (shuhada)
    public static function StateName($x)
    {
        if ($x == 0) {
            // all
            $y = 0;
        } elseif ($x == 1) {
            // perlis
            $y = 508;
        } elseif ($x == 2) {
            // perak
            $y = 507;
        } elseif ($x == 3) {
            // kedah
            $y = 502;
        } elseif ($x == 4) {
            // selangor
            $y = 512;
        } elseif ($x == 5) {
            // negeri sembilan
            $y = 505;
        } elseif ($x == 6) {
            // melaka
            $y = 504;
        } elseif ($x == 7) {
            // kelantan
            $y = 503;
        } elseif ($x == 8) {
            // pahang
            $y = 506;
        } elseif ($x == 9) {
            // terengganu
            $y = 513;
        } elseif ($x == 10) {
            // sarawak
            $y = 511;
        } elseif ($x == 11) {
            // sabah
            $y = 510;
        } elseif ($x == 12) {
            // johor
            $y = 501;
        } else {
            $y = 0;
        }
        return $y;
    }

    public static function JobTitle($x)
    {
        if ($x == 'Assistant Manager') {
            $y = 4;
        } elseif ($x == 'Manager') {
            $y = 3;
        } elseif ($x == 'IT Supervisor') {
            $y = 5;
        } elseif ($x == 'Supervisor') {
            $y = 9;
        } elseif ($x == 'Part Time' || $x == 'Part Timer') {
            $y = 6;
        } elseif ($x == 'Penyelia IT') {
            $y = 8;
        } elseif ($x == 'HR Officer' || $x == 'HR Assistant' || $x == 'Junior HR Officer' || $x == 'Senior HR  Executive') {
            $y = 2;
        } elseif ($x == 'Regional Manager (Southern)' || $x == 'Regional Manager (Sabah & Sarawak)' || $x == 'Regional Manager (Kelantan & Terengganu)' || $x == 'Regional Manager (Northern)' || $x == 'Regional Manager (Pahang)' || 'Regional Manager (Central)') {
            $y = 7;
        } else {
            $y = 9;
        }
        return $y;
    }
}
