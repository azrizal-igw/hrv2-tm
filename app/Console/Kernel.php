<?php namespace IhrV2\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel 
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        // class clear data
        // ----------------
        Commands\Clear::class,

        // class sync erp
        // --------------
        Commands\Erp\Sync\Staff\Active::class,
        Commands\Erp\Sync\Staff\Contract::class,
        Commands\Erp\Sync\Staff\Other::class,
        Commands\Erp\Sync\Site\Active::class,
        Commands\Erp\Sync\PublicHoliday\Active::class,

        // class sync site
        // ---------------
        Commands\Site\Sync\Agg\Active::class,

        // class attendance
        // ----------------
        Commands\Attendance\Punch::class,
        Commands\Attendance\Summary::class,

        // class aggregator
        // ----------------
        Commands\Agg\Staff\Export\StatusStartEnd::class,
        Commands\Agg\Attendance\Check\Fail::class,
        Commands\Agg\Attendance\Check\Incomplete::class,
        Commands\Agg\Attendance\Process\Backdated::class,
        Commands\Agg\Attendance\Process\Date::class,
        Commands\Agg\Attendance\Export\Date::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) 
    {
        // current datetime details
        // ------------------------
        $now = Carbon::now();

        // current year
        // ------------
        $year = $now->year;

        // current month
        // -------------
        $month = $now->month;

        // current year & month
        // --------------------
        $yearmonth = $now->format('Y-m');

        // current date
        // ------------
        $today = $now->toDateString();

        // current time
        // ------------
        $time = $now->toTimeString();

        // yesterday date
        // --------------
        $yesterday = Carbon::yesterday()->toDateString();

        // date previous 7 days
        // --------------------        
        $previous7 = $now->subDays(7)->toDateString();

        // date previous 2 days
        // --------------------        
        $previous2 = $now->subDays(2)->toDateString();

        // get url transfer staff detail
        // -----------------------------
        $url_staff = env('AGG_URL_STAFF');

        // get url transfer staff attendance
        // ---------------------------------
        $url_att = env('AGG_URL_ATTENDANCE');

        /*
        |--------------------------------------------------------------------------
        | Backup DB & Files (/storage/app)
        |--------------------------------------------------------------------------
         */

        // backup files
        // ------------
        $schedule->command('backup:run --only-files')->dailyAt('01:00');

        // backup database
        // ---------------
        $schedule->command('backup:run --only-db')->dailyAt('02:00');

        /*
        |--------------------------------------------------------------------------
        | Clear Data
        |--------------------------------------------------------------------------
         */

        $schedule->command('clear:data')->dailyAt('01:30');

        /*
        |--------------------------------------------------------------------------
        | Sync ERP into IHR
        |--------------------------------------------------------------------------
         */

        // daily sync site
        // ---------------
        $schedule->command('erp-sync-site:active')->dailyAt('19:00');

        // daily sync public holiday
        // -------------------------
        $schedule->command('erp-sync-public:active ' . $year)->dailyAt('19:05');

        // daily sync staff (active & inactive) 
        // ------------------------------------
        $schedule->command('erp-sync-staff:active 0 0')->dailyAt('19:10');
        $schedule->command('erp-sync-staff:active 0 0')->dailyAt('20:10');
        $schedule->command('erp-sync-staff:active 0 0')->dailyAt('21:10');
        $schedule->command('erp-sync-staff:active 0 0')->dailyAt('22:10');

        // daily sync staff contract
        // -------------------------
        $schedule->command('erp-sync-staff:contract 0 0')->dailyAt('19:20');

        // daily sync staff drm
        // --------------------
        $schedule->command('erp-sync-staff:other 6')->dailyAt('19:21');

        // daily sync staff clerk
        // ----------------------
        $schedule->command('erp-sync-staff:other 7')->dailyAt('19:22');

        // daily sync staff rm
        // -------------------
        $schedule->command('erp-sync-staff:other 4')->dailyAt('19:23');

        // daily sync staff hr
        // -------------------
        $schedule->command('erp-sync-staff:other 2')->dailyAt('19:24');

        // daily sync staff project
        // ------------------------
        $schedule->command('erp-sync-staff:other 10')->dailyAt('19:25');

        /*
        |--------------------------------------------------------------------------
        | Sync IHR with AGG
        |--------------------------------------------------------------------------
         */
        $schedule->command('site-sync-agg:active')->dailyAt('19:30');

        /*
        |--------------------------------------------------------------------------
        | Attendance Daily Notification
        |--------------------------------------------------------------------------
         */

        // daily check attendance punch in & out
        // -------------------------------------
        $schedule->command('att:punch ' . $today . ' 0 10')->everyTenMinutes();
        $schedule->command('att:punch ' . $today . ' 1 15')->cron('*/15 * * * *');

        // daily check attendance summary
        // ------------------------------
        $schedule->command('att:summary ' . $today)->dailyAt('13:05');
        $schedule->command('att:summary ' . $today)->dailyAt('18:35');
        $schedule->command('att:summary ' . $today)->dailyAt('21:05');

        /*
        |--------------------------------------------------------------------------
        | Process Aggregator
        |--------------------------------------------------------------------------
         */

        // daily export staff inactive
        // ---------------------------
        $schedule->command('agg-staff-export:status-start-end 2 ' . $today . ' ' . $today)->dailyAt('19:30');
        $schedule->exec('curl '.$url_staff)->dailyAt('19:32');

        // daily export staff active
        // -------------------------
        $schedule->command('agg-staff-export:status-start-end 1 ' . $today . ' ' . $today)->dailyAt('20:40');
        $schedule->exec('curl '.$url_staff)->dailyAt('20:42');

        // daily process & export attendance
        // ---------------------------------
        $schedule->command('agg-att-process:date ' . $today)->dailyAt('22:00');
        $schedule->command('agg-att-export:date ' . $today)->dailyAt('22:35');

        // daily check fail transfer attendance
        // ------------------------------------
        $schedule->command('agg-att-check:fail ' . $today . ' ' . $today)->dailyAt('23:59');

        // daily process & export backdated attendance (last 7 days)
        // ---------------------------------------------------------
        $schedule->command('agg-att-process:backdated 7')->daily();

        // daily check & transfer staff attendance & staff detail
        // ------------------------------------------------------
        $schedule->exec('curl '.$url_att)->hourly();
        $schedule->exec('curl '.$url_staff)->hourly();

        /*
        |--------------------------------------------------------------------------
        | Re-Send/Check Next Day
        |--------------------------------------------------------------------------
         */

        // daily sync staff & contract (again if previous sync is fail)
        // ------------------------------------------------------------
        $schedule->command('erp-sync-staff:active 0 0')->dailyAt('05:10');
        $schedule->command('erp-sync-staff:contract 0 0')->dailyAt('05:20');

        // daily export staff inactive (if previous sync staff inactive is fail)
        // ---------------------------------------------------------------------
        $schedule->command('agg-staff-export:status-start-end 2 ' . $previous2 . ' ' . $today)->dailyAt('05:30');
        $schedule->exec('curl '.$url_staff)->dailyAt('05:32');

        // daily export staff active (if previous sync staff active is fail)
        // -----------------------------------------------------------------
        $schedule->command('agg-staff-export:status-start-end 1 ' . $previous2 . ' ' . $today)->dailyAt('06:40');
        $schedule->exec('curl '.$url_staff)->dailyAt('06:42');

        // daily process & export yesterday attendance
        // -------------------------------------------
        $schedule->command('agg-att-process:date ' . $yesterday)->dailyAt('06:00');
        $schedule->command('agg-att-export:date ' . $yesterday)->dailyAt('06:15');

        // daily check fail transfer attendance
        // ------------------------------------
        $schedule->command('agg-att-check:fail ' . $yesterday . ' ' . $yesterday)->dailyAt('08:30');

        // process attendance 


        // daily reset exported attendance
        // update send date and error message to null if has error message
        // ---------------------------------------------------------------
        // $schedule->command('agg-att-reset:daily')->daily();

        // every weeek
        // every 15 days
        // every monthly

        // process and export again last month attendance
        // ----------------------------------------------

        // daily check staff at mcmc (updated status_mcmc)
        // -----------------------------------------------

    }

}
