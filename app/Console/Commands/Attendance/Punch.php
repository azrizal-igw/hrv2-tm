<?php

namespace IhrV2\Console\Commands\Attendance;

use IhrV2\Contracts\Attendance\AttendanceInterface as AttendanceRepository;
use Illuminate\Console\Command;

class Punch extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'att:punch {date?} {type?} {minute?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $att_repo;
    public function __construct(AttendanceRepository $att_repo) {
        parent::__construct();
        $this->att_repo = $att_repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $date = $this->argument('date');
        $type = $this->argument('type');
        $minute = $this->argument('minute');
        $this->att_repo->getChkAttRegion($date, $type, $minute);
    }
}
