<?php

namespace IhrV2\Console\Commands\Agg\Staff\Export;

use Illuminate\Console\Command;
use IhrV2\Contracts\Mcmc\McmcInterface as McmcRepository;

class StatusStartEnd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agg-staff-export:status-start-end {status?} {start?} {end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $mcmc_repo;
    public function __construct(McmcRepository $mcmc_repo)
    {
        parent::__construct();
        $this->mcmc_repo = $mcmc_repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $status = $this->argument('status');                
        $start = $this->argument('start');                
        $end = $this->argument('end');                
        $this->mcmc_repo->getExpStaff($status, $start, $end);
    }
}
