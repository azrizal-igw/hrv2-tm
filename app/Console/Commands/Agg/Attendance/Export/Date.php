<?php

namespace IhrV2\Console\Commands\Agg\Attendance\Export;

use IhrV2\Contracts\Mcmc\McmcInterface as McmcRepository;
use Illuminate\Console\Command;

class Date extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agg-att-export:date {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Attendance Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $mcmc_repo;
    public function __construct(McmcRepository $mcmc_repo) {
        parent::__construct();
        $this->mcmc_repo = $mcmc_repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->mcmc_repo->getExpAttDate($this->argument('date'));
    }
}
