<?php

namespace IhrV2\Console\Commands\Agg\Attendance\Process;

use Illuminate\Console\Command;
use Carbon\Carbon;
use IhrV2\Contracts\Mcmc\McmcInterface as McmcRepository;

class Backdated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agg-att-process:backdated {days?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process and Export Backdated Attendance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $mcmc_repo;
    public function __construct(McmcRepository $mcmc_repo)
    {
        parent::__construct();
        $this->mcmc_repo = $mcmc_repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days');        
        $this->mcmc_repo->getProAttBackdated($days);
    }
}
