<?php

namespace IhrV2\Console\Commands\Agg\Attendance\Process;

use Illuminate\Console\Command;
use IhrV2\Contracts\Mcmc\McmcInterface as McmcRepository;

class SiteStartEnd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agg-att-process:sitecode-start-end {sitecode?} {start?} {end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Attendance By Sitecode and Date Range';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $mcmc_repo;
    public function __construct(McmcRepository $mcmc_repo)
    {
        parent::__construct();
        $this->mcmc_repo = $mcmc_repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitecode = $this->argument('sitecode');
        $start = $this->argument('start');
        $end = $this->argument('end');
        $this->mcmc_repo->getProAttSiteDate($sitecode, $start, $end);
    }
}
