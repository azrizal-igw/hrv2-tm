<?php

namespace IhrV2\Console\Commands\Site\Sync\Agg;

use Illuminate\Console\Command;
use IhrV2\Contracts\Agg\AggInterface as AggRepository;

class Active extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site-sync-agg:active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $agg_repo;
    public function __construct(AggRepository $agg_repo)
    {
        parent::__construct();
        $this->agg_repo = $agg_repo;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->agg_repo->getSiteSync(1);
    }
}
