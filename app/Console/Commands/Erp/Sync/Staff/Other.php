<?php

namespace IhrV2\Console\Commands\Erp\Sync\Staff;

use Illuminate\Console\Command;
use IhrV2\Contracts\Erp\ErpInterface as ErpRepository;

class Other extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'erp-sync-staff:other {group?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $erp_repo;
    public function __construct(ErpRepository $erp_repo)
    {
        parent::__construct();
        $this->erp_repo = $erp_repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $group = $this->argument('group'); 
        $this->erp_repo->getSyncStaffOther($group);
    }
}
