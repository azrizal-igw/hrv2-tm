<?php namespace IhrV2\Repositories\Download;

use Carbon\Carbon;
use IhrV2\Contracts\Download\DownloadInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use IhrV2\Helpers\AttendanceHelper;

class DownloadRepository implements DownloadInterface
{
	protected $upload_repo;

    public function __construct(UploadInterface $upload_repo)
    {
        $this->upload_repo = $upload_repo;
    }

    // download attendance by word
    // ---------------------------
    public function getAttByDoc($datas)
    {
        // generate word content
        // ---------------------
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $section->addText($datas['name'], ['size' => 12, 'bold' => true]);
        $section->addText($datas['site'], ['size' => 12, 'bold' => true]);
        $table = $section->addTable();
        
        if (count($datas['datas']) > 0) {
            foreach ($datas['datas'] as $data) {
                if (count($data['all_dates']) > 0) {
                    $start = null;
                    $end = null;
                    $hours  = null;
                    $minutes = null;
                    $seconds = null;
                    $mcmc_hours = null;
                    $mcmc_minutes = null;
                    $mcmc_seconds = null;

                    foreach ($data['all_dates'] as $x => $y) {
                        $table->addRow();
                        $date = Carbon::parse($y)->format('Y-m-d');
                        $table->addCell(2000)->addText($date);

                        $on = 0;
                        $apply_remark = 0;
                        $approve_remark = 0;
                        $checkbox = 0;
                        $limit = array();

                        if (array_key_exists($y, $data['limit'])) {
                            $limit['time_in'] = $data['limit'][$y]['time_in'];
                            $limit['time_out'] = $data['limit'][$y]['time_out'];
                        }

                        $in = 0;
                        $out = 0;
                        $have_in = 0;
                        $have_out = 0;
                        $less = 0;

                        if (in_array(Carbon::parse($y)->format('Y-m-d'), $data['dates'])) {
                            foreach ($data['att'] as $t) {
                                if (Carbon::parse($t->att_date)->format('Y-m-d') == Carbon::parse($y)->format('Y-m-d')) {
                                    if ($t->att_record_type == 0 && $have_in == 0) {
                                        $in = 1;
                                        $late_in = '';
                                        $start = $t->att_in;
                                        $have_in = 1;
                                        $status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_in'], $data['mobile']);
                                        $fin = Carbon::parse($t->att_in)->format('g:i:s A');
                                    }
                                    if ($t->att_record_type == 1 && $have_out == 0) {
                                        $out = 1;
                                        $early_out = '';
                                        $end = $t->att_out;
                                        $have_out = 1;
                                        $status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_out'], $data['mobile']);
                                        if ($in == 0) {
                                            $fout = '-';
                                        }
                                        $fout = Carbon::parse($t->att_out)->format('g:i:s A');
                                    }
                                }
                            }

                            if ($in == 1 && $out == 0) {
                                $fout = '-';
                            }
                        } else {
                            $fin = '-';
                            $fout = '-';
                        }
                        $table->addCell(2000)->addText($fin);
                        $table->addCell(2000)->addText($fout);

                        if ($in == 1 && $out == 1) {
                            if ($start < $end) {
                                $get = AttendanceHelper::getCompareDateTime($start, $end, $limit);
                                $hours += $get['hour'];
                                $minutes += $get['minute'];
                                $seconds += $get['second'];
                                $total_time = $get['time'];
                            } else {
                                $less = 1;
                                $total_time = '-';
                            }
                            $tall = $total_time;
                        } else {
                            $tall = '-';
                        }
                        $table->addCell(2000)->addText($tall);

                        if ($in == 1 && $out == 1) {
                            if ($start < $end) {
                                $mcmc = AttendanceHelper::getCompareMcmc($start, $end, $limit);
                                $mcmc_hours += $mcmc['hour'];
                                $mcmc_minutes += $mcmc['minute'];
                                $mcmc_seconds += $mcmc['second'];
                                $total_mcmc = $mcmc['time'];
                            } else {
                                $less = 1;
                                $total_mcmc = '-';
                            }
                            $tmcmc = $total_mcmc;
                        } else {
                            $tmcmc = '-';
                        }
                        $table->addCell(2000)->addText($tmcmc);

                        $remark = '-';
                        if ($date <= $data['today']) {
                            if ($in == 1 && $out == 1 && $less != 1) {
                                $remark = 'Working';
                            } else {
                                if (in_array($date, $data['leave_dates'])) {
                                    foreach ($data['leave_details'] as $ld) {
                                        if ($date == $ld['date']) {
                                            $remark = $ld['name'];
                                        }
                                    }
                                } elseif (in_array($date, $data['leave_half'])) {
                                    $counts = array_count_values($data['leave_half']);
                                    $half = $counts[$date];
                                    if ($half >= 2) {
                                        $remark = 'Leave 2X';
                                    }
                                } elseif (in_array($date, $data['public_dates'])) {
                                    $remark = 'Public Holiday';
                                } elseif (in_array($date, $data['off_dates'])) {
                                    $remark = 'Off Day';
                                }
                            }
                        }

                        if (in_array($date, $data['approve_dates']) && $remark == '-') {
                            foreach ($data['remark_details'] as $rd) {
                                if ($date == $rd['date']) {
                                    $remark = $rd['name'];
                                }
                            }
                        }

                        if (in_array($date, $data['rm_approve_dates']) && $remark == '-') {
                            foreach ($data['remark_details'] as $rd) {
                                if ($date == $rd['date']) {
                                    $remark = $rd['name'];
                                }
                            }
                        }
                        $table->addCell(2000)->addText($remark);
                    }
                }
            }
        }

        // get path
        // --------
        $dirname = 'attendance/doc';
        $dirpath = $this->upload_repo->getCheckFolder($dirname);
        $dirfull = $dirpath.'/'.$datas['filename'].'.docx';

        // upload file into server
        // -----------------------
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objWriter->save($dirfull);
        } catch (Exception $e) {
        }
        return $dirfull;
    }

    // download attendance by excel
    // ----------------------------
    public function getAttByExcel($datas, $sitecode, $icno)
    {
        // generate file excel
        // -------------------
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // set header value
        // ----------------
        $sheet->setCellValue('A1', $datas['name']);
        $sheet->setCellValue('A2', $datas['site']);

        // chek record
        // -----------
        $no = 2;
        if (count($datas['datas']) > 0) {
            foreach ($datas['datas'] as $data) {

                // have record
                // -----------
                if (count($data['all_dates']) > 0) {
                    $start = null;
                    $end = null;
                    $hours  = null;
                    $minutes = null;
                    $seconds = null;
                    $mcmc_hours = null;
                    $mcmc_minutes = null;
                    $mcmc_seconds = null;

                    // loop record
                    // -----------
                    foreach ($data['all_dates'] as $x => $y) {
                        $no++;

                        // set date attendance
                        // -------------------
                        $date = Carbon::parse($y)->format('Y-m-d');
                        $sheet->setCellValue('A'.$no, $date);

                        // set variables
                        // -------------
                        $on = 0;
                        $apply_remark = 0;
                        $approve_remark = 0;
                        $checkbox = 0;
                        $limit = array();

                        if (array_key_exists($y, $data['limit'])) {
                            $limit['time_in'] = $data['limit'][$y]['time_in'];
                            $limit['time_out'] = $data['limit'][$y]['time_out'];
                        }

                        $in = 0;
                        $out = 0;
                        $have_in = 0;
                        $have_out = 0;
                        $less = 0;

                        if (in_array(Carbon::parse($y)->format('Y-m-d'), $data['dates'])) {
                            foreach ($data['att'] as $t) {
                                if (Carbon::parse($t->att_date)->format('Y-m-d') == Carbon::parse($y)->format('Y-m-d')) {
                                    if ($t->att_record_type == 0 && $have_in == 0) {
                                        $in = 1;
                                        $late_in = '';
                                        $start = $t->att_in;
                                        $have_in = 1;
                                        $status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_in'], $data['mobile']);
                                        $fin = Carbon::parse($t->att_in)->format('g:i:s A');
                                    }
                                    if ($t->att_record_type == 1 && $have_out == 0) {
                                        $out = 1;
                                        $early_out = '';
                                        $end = $t->att_out;
                                        $have_out = 1;
                                        $status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_out'], $data['mobile']);
                                        if ($in == 0) {
                                            $fout = '-';
                                        }
                                        $fout = Carbon::parse($t->att_out)->format('g:i:s A');
                                    }
                                }
                            }

                            if ($in == 1 && $out == 0) {
                                $fout = '-';
                            }
                        } else {
                            $fin = '-';
                            $fout = '-';
                        }

                        $sheet->setCellValue('B'.$no, $fin);
                        $sheet->setCellValue('C'.$no, $fout);

                        if ($in == 1 && $out == 1) {
                            if ($start < $end) {
                                $get = AttendanceHelper::getCompareDateTime($start, $end, $limit);
                                $hours += $get['hour'];
                                $minutes += $get['minute'];
                                $seconds += $get['second'];
                                $total_time = $get['time'];
                            } else {
                                $less = 1;
                                $total_time = '-';
                            }
                            $tall = $total_time;
                        } else {
                            $tall = '-';
                        }
                        $sheet->setCellValue('D'.$no, $tall);

                        if ($in == 1 && $out == 1) {
                            if ($start < $end) {
                                $mcmc = AttendanceHelper::getCompareMcmc($start, $end, $limit);
                                $mcmc_hours += $mcmc['hour'];
                                $mcmc_minutes += $mcmc['minute'];
                                $mcmc_seconds += $mcmc['second'];
                                $total_mcmc = $mcmc['time'];
                            } else {
                                $less = 1;
                                $total_mcmc = '-';
                            }
                            $tmcmc = $total_mcmc;
                        } else {
                            $tmcmc = '-';
                        }

                        $sheet->setCellValue('E'.$no, $tmcmc);


                        $remark = '-';
                        if ($date <= $data['today']) {
                            if ($in == 1 && $out == 1 && $less != 1) {
                                $remark = 'Working';
                            } else {
                                if (in_array($date, $data['leave_dates'])) {
                                    foreach ($data['leave_details'] as $ld) {
                                        if ($date == $ld['date']) {
                                            $remark = $ld['name'];
                                        }
                                    }
                                } elseif (in_array($date, $data['leave_half'])) {
                                    $counts = array_count_values($data['leave_half']);
                                    $half = $counts[$date];
                                    if ($half >= 2) {
                                        $remark = 'Leave 2X';
                                    }
                                } elseif (in_array($date, $data['public_dates'])) {
                                    $remark = 'Public Holiday';
                                } elseif (in_array($date, $data['off_dates'])) {
                                    $remark = 'Off Day';
                                }
                            }
                        }

                        if (in_array($date, $data['approve_dates']) && $remark == '-') {
                            foreach ($data['remark_details'] as $rd) {
                                if ($date == $rd['date']) {
                                    $remark = $rd['name'];
                                }
                            }
                        }

                        if (in_array($date, $data['rm_approve_dates']) && $remark == '-') {
                            foreach ($data['remark_details'] as $rd) {
                                if ($date == $rd['date']) {
                                    $remark = $rd['name'];
                                }
                            }
                        }
                        $sheet->setCellValue('F'.$no, $remark);
                    }
                }
            }
        }

        // set filename
        // ------------
        $filename = $sitecode . '_' . $icno . '_' .date('YmdHis') . '_' . str_random(20);

        // download excel
        // --------------
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'.xlsx"');
        $writer->save("php://output");
    }

    // download masterlist staff
    // -------------------------
    public function getUserByExcel($datas)
    {        
        // generate file excel
        // -------------------
        $spreadsheet = new Spreadsheet();

        // get phases
        // ----------
        $phases = $this->getPhaseAll();
        if (!empty($phases)) {
            $no = 0;

            // loop all phases
            // ---------------
            foreach ($phases as $p) {
                $no++;

                // set sheet name (excel allow 30 character for sheet name)
                // --------------------------------------------------------
                if (strlen($p->name_both) >= 30) {
                    $sname = substr($p->name_both, 0, 30);
                }
                else {
                    $sname = $p->name_both;
                }

                // set first sheet
                // ---------------
                if ($no == 1) {    
                    $sheet = $spreadsheet->getActiveSheet()->setTitle($sname);
                    $this->getExcelContent($p->id, $sheet, $datas);
                }

                // set rest of sheets
                // ------------------
                else {
                    $sheet = $spreadsheet->createSheet($no);
                    $this->getExcelContent($p->id, $sheet, $datas);                    
                    $sheet->setTitle($sname);                     
                }
            }
        }
  
        // set filename
        // ------------
        $filename = 'Masterlist Staff '.date('YmdHis');
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'.xlsx"');
        $writer->save("php://output");
    }

    // generate ezcel content
    // ----------------------
    public function getExcelContent($phase_id, $sheet, $datas)
    {
        // set initial row
        // ---------------
        $total = 7;
        $no = 0; 
        
        // set phase info
        // --------------
        $sheet->setCellValue('A1', 'MAKLUMAT PETUGAS PUSAT INTERNET')->getStyle('A1:A1')->getFont()->setBold(true)->setSize(18);
        
        // display contract info
        // ---------------------
        $sheet->setCellValue('A3', 'TEMPOH KONTRAK')->getStyle('A3:A3')->getFont()->setBold(true);
        
        // display table
        // -------------
        $sheet->setCellValue('A7', 'BIL')->getStyle('A7:A7')->getFont()->setBold(true);
        $sheet->setCellValue('B7', 'SITE CODE')->getStyle('B7:B7')->getFont()->setBold(true);
        $sheet->setCellValue('C7', 'SITE NAME')->getStyle('C7:C7')->getFont()->setBold(true);
        $sheet->setCellValue('D7', 'PARLIAMENT')->getStyle('D7:D7')->getFont()->setBold(true);
        $sheet->setCellValue('E7', 'DUN')->getStyle('E7:E7')->getFont()->setBold(true);
        $sheet->setCellValue('F7', 'STATE')->getStyle('F7:F7')->getFont()->setBold(true);
        $sheet->setCellValue('G7', 'SITE ADDRESS')->getStyle('G7:G7')->getFont()->setBold(true);

        $sheet->setCellValue('H7', 'COORDINATE')->getColumnDimension('H')->setAutoSize(TRUE);
        $sheet->setCellValue('H7', 'COORDINATE')->getStyle('H7:H7')->getFont()->setBold(true);

        $sheet->setCellValue('I7', 'POSITION')->getStyle('I7:I7')->getFont()->setBold(true);
        $sheet->setCellValue('J7', 'STAFF STATUS')->getStyle('J7:J7')->getFont()->setBold(true);
        $sheet->setCellValue('K7', 'STAFF NAME')->getStyle('K7:K7')->getFont()->setBold(true);
        $sheet->setCellValue('L7', 'STAFF ID')->getStyle('L7:L7')->getFont()->setBold(true);
        $sheet->setCellValue('M7', 'STAFF IC')->getStyle('M7:M7')->getFont()->setBold(true);
        $sheet->setCellValue('N7', 'STAFF ADDRESS')->getStyle('N7:N7')->getFont()->setBold(true);
        $sheet->setCellValue('O7', 'TEL NO 1')->getStyle('O7:O7')->getFont()->setBold(true);
        $sheet->setCellValue('P7', 'TEL NO 2')->getStyle('P7:P7')->getFont()->setBold(true);
        $sheet->setCellValue('Q7', 'TEL NO 3')->getStyle('Q7:Q7')->getFont()->setBold(true);
        $sheet->setCellValue('R7', 'PERSONAL EMAIL')->getStyle('R7:R7')->getFont()->setBold(true);
        $sheet->setCellValue('S7', 'PI1M EMAIL')->getStyle('S7:S7')->getFont()->setBold(true);
        $sheet->setCellValue('T7', 'URL PORTAL')->getStyle('T7:T7')->getFont()->setBold(true);
        $sheet->setCellValue('U7', 'JOIN DATE')->getStyle('U7:U7')->getFont()->setBold(true);
        $sheet->setCellValue('V7', 'QUALIFICATION')->getStyle('V7:V7')->getFont()->setBold(true);
        $sheet->setCellValue('W7', 'CONTRACT START')->getStyle('W7:W7')->getFont()->setBold(true);
        $sheet->setCellValue('X7', 'CONTRACT END')->getStyle('X7:X7')->getFont()->setBold(true);        
        $sheet->setCellValue('Y7', 'RESIGN DATE')->getStyle('Y7:Y7')->getFont()->setBold(true);
        $sheet->setCellValue('Z7', 'NOTES')->getStyle('Z7:Z7')->getFont()->setBold(true);


        // loop user record
        // ----------------
        if (!empty($datas)) {            
            foreach ($datas as $i) {

                // check site info
                // ---------------
                if (!empty($i->SiteNameInfo)) {
                    $site_code = $i->SiteNameInfo->code;
                    $site_name = $i->SiteNameInfo->name;
                    $site_city = $i->SiteNameInfo->city;
                    $site_district = $i->SiteNameInfo->district;
                    $site_state = $i->SiteNameInfo->StateName->name;
                    $site_address = $i->SiteNameInfo->street2;
                    $site_longitude = $i->SiteNameInfo->longitude;
                }
                else {
                    $site_code = '-';
                    $site_name = '-';
                    $site_city = '-';
                    $site_district = '-';
                    $site_state = '-';
                    $site_address = '-';
                    $site_longitude = '-';
                }

                // get staff status
                // ----------------
                if ($i->status == 1) {
                    $status = 'ACTIVE';                    
                }
                else {
                    $status = 'INACTIVE';
                }

                // get staff job info
                // ------------------
                if (!empty($i->UserLatestJob)) {
                    $join_date = $i->UserLatestJob->join_date;
                    $position = $i->UserLatestJob->PositionName->name;
                    $notes = $i->UserLatestJob->notes;

                    if ($i->UserLatestJob->resign_date != '0000-00-00') {
                        $resign_date = $i->UserLatestJob->resign_date;
                    }
                    else {
                        $resign_date = '-';
                    }
                }
                else {
                    $join_date = '-';
                    $position = '-';
                    $notes = '-';
                    $resign_date = '-';
                }

                // get contract info
                // -----------------
                if (!empty($i->UserLatestContract)) {
                    $contract_start = $i->UserLatestContract->date_from;
                    $contract_end = $i->UserLatestContract->date_to;
                }
                else {
                    $contract_start = '-';
                    $contract_end = '-';
                }

                // get staff education
                // -------------------
                if (!empty($i->UserEducationOne)) {
                    $education = $i->UserEducationOne->name_education;
                }
                else {
                    $education = '-';
                }

                // get previous staff info
                // -----------------------
                if ($i->UserLatestJob->phase_id == $phase_id) {
                    $total++;
                    $no++;

                    // display table info
                    // ------------------
                    $sheet->setCellValue('A'.$total, $no)
                    ->getStyle('A'.$total.':'.'A'.$total)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue('B'.$total, $site_code)->getColumnDimension('B')->setAutoSize(TRUE);
                    $sheet->setCellValue('C'.$total, $site_name)->getColumnDimension('C')->setAutoSize(TRUE);
                    $sheet->setCellValue('D'.$total, $site_city)->getColumnDimension('D')->setAutoSize(TRUE);
                    $sheet->setCellValue('E'.$total, $site_district)->getColumnDimension('E')->setAutoSize(TRUE);
                    $sheet->setCellValue('F'.$total, $site_state)->getColumnDimension('F')->setAutoSize(TRUE);
                    $sheet->setCellValue('G'.$total, $site_address)->getColumnDimension('G')->setAutoSize(TRUE);

                    $sheet->setCellValue('H'.$total, $site_longitude)
                    ->getStyle('H'.$total.':'.'H'.$total)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue('H'.$total, $site_longitude)->getColumnDimension('I')->setAutoSize(TRUE);
                    
                    $sheet->setCellValue('I'.$total, $position)->getColumnDimension('I')->setAutoSize(TRUE);
                    $sheet->setCellValue('J'.$total, $status)->getColumnDimension('J')->setAutoSize(TRUE);

                    $sheet->setCellValue('K'.$total, $i->name)->getColumnDimension('K')->setAutoSize(TRUE);
                    $sheet->setCellValue('L'.$total, $i->staff_id)->getColumnDimension('L')->setAutoSize(TRUE);

                    $sheet->setCellValueExplicit('M'.$total, $i->icno, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                    ->getColumnDimension('M')->setAutoSize(TRUE);

                    $sheet->setCellValue('N'.$total, $i->permanent_street_1.' '.$i->permanent_street_2.' '.$i->permanent_postcode.' '.$i->permanent_city.' '.$i->permanent_state)
                    ->getColumnDimension('N')->setAutoSize(TRUE);

                    $sheet->setCellValue('O'.$total, $i->hpno)->getColumnDimension('O')->setAutoSize(TRUE);
                    $sheet->setCellValue('P'.$total, $i->hpno2)->getColumnDimension('P')->setAutoSize(TRUE);
                    $sheet->setCellValue('Q'.$total, $i->telno1)->getColumnDimension('Q')->setAutoSize(TRUE);
                    $sheet->setCellValue('R'.$total, $i->personal_email)->getColumnDimension('R')->setAutoSize(TRUE);
                    $sheet->setCellValue('S'.$total, $i->work_email)->getColumnDimension('S')->setAutoSize(TRUE);
                    $sheet->setCellValue('T'.$total, $i->website)->getColumnDimension('T')->setAutoSize(TRUE);
                    $sheet->setCellValue('U'.$total, $join_date)->getColumnDimension('U')->setAutoSize(TRUE);
                    $sheet->setCellValue('V'.$total, $education)->getColumnDimension('V')->setAutoSize(TRUE);
                    $sheet->setCellValue('W'.$total, $contract_start)->getColumnDimension('W')->setAutoSize(TRUE);
                    $sheet->setCellValue('X'.$total, $contract_end)->getColumnDimension('X')->setAutoSize(TRUE);                    
                    $sheet->setCellValue('Y'.$total, $resign_date)->getColumnDimension('Y')->setAutoSize(TRUE);
                    $sheet->setCellValue('Z'.$total, $notes)->getColumnDimension('Z')->setAutoSize(TRUE);

                }
            }
        }                
    }

    // generate filename
    // -----------------
    public function getFileName($sitecode, $icno)
    {
        return $sitecode . '_' . $icno . '_' .date('YmdHis') . '_' . str_random(20);
    }

    // get lists f phases
    // ------------------
    public function getPhaseAll()
    {
        $excludes = [
            ['name', 'NOT LIKE', '%saifon%'],
            ['name', 'NOT LIKE', '%mini pjk%'],
            ['name', 'NOT LIKE', '%pjl%']
        ];        
        $q = \IhrV2\Models\Phase::where($excludes)->get();
        return $q;
    }
}


