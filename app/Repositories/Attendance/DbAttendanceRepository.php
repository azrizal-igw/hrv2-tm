<?php namespace IhrV2\Repositories\Attendance;

use Carbon\Carbon;
use IhrV2\Contracts\Attendance\DbAttendanceInterface;

class DbAttendanceRepository implements DbAttendanceInterface {

    // insert attendance
    // -----------------
    public function dbInsertAttendance($data) {
        $arr = array(
            'att_mykad_ic'    => strtoupper($data['icno']),
            'location_id'     => strtoupper($data['sitecode']),
            'att_record_type' => $data['type_id'], // 0 = in, 1 = out
            'att_input_type'  => 0,                // not using card reader
            'att_log_time'    => Carbon::parse($data['datetime'])->format('Y-m-d H:i:s'),
            'att_userstamp'   => $data['remark'],
            'ip_add'          => \Request::ip(), // ip address network
            'att_sys_status'  => $data['status']
        );
        $sav = new \IhrV2\Models\Attendance($arr);
        $sav->save();
        return $sav->id;
    }

    // insert attendance manual
    // ------------------------
    public function dbInsertAttendanceManual($id, $data, $uid) {
        $arr = array(
            'att_id'       => $id,
            'icno'         => strtoupper($data['icno']),
            'user_id'      => $data['user_id'],
            'type_id'      => $data['type_id'],
            'att_log_time' => Carbon::parse($data['datetime'])->format('Y-m-d H:i:s'),
            'sitecode'     => strtoupper($data['sitecode']),
            'remark'       => $data['remark'],
            'action_by'    => $uid,
            'action_date'  => Carbon::now(),
            'active'       => 1
        );
        $sav = new \IhrV2\Models\AttendanceManual($arr);
        $sav->save();
        return $sav->id;
    }

    public function dbInsertAttRemark($data, $user, $date, $other = null) {

        // check if set remark by rm (status_id and other_id set same)
        // -----------------------------------------------------------
        $other_id = ($other) ? $data['att_status'] : null;

        // insert remark
        // -------------
        $arr = array(
            'user_id'     => $user['uid'],
            'icno'        => $user['icno'],
            'sitecode'    => $user['sitecode'],
            'date'        => $date,
            'status_id'   => $data['att_status'],
            'other_id'    => $other_id,
            'notes'       => $data['att_notes'],
            'action_by'   => auth()->user()->id,
            'action_date' => Carbon::now(),
            'active'      => 1
        );
        $sav = new \IhrV2\Models\AttendanceRemark($arr);
        $sav->save();
        return $sav->id;
    }

    public function dbInsertAttRemarkAttachment($id, $uid, $arr) {
        $arr = array(
            'att_remark_id' => $id,
            'user_id'       => $uid,
            'filename'      => $arr['filename'],
            'ext'           => strtolower($arr['fileext']),
            'size'          => $arr['filesize'],
            'thumb_name'    => $arr['filethumb'],
            'status'        => 1
        );
        $sav = new \IhrV2\Models\AttendanceRemarkAttachment($arr);
        $sav->save();
        return true;
    }

    public function dbInsertAttOtherAttachment($id, $uid, $arr) {
        $arr = array(
            'user_id'      => $uid,
            'filename'     => $arr['filename'],
            'ext'          => strtolower($arr['fileext']),
            'size'         => $arr['filesize'],
            'thumb_name'   => $arr['filethumb'],
            'att_other_id' => $id,
            'status'       => 1
        );
        $sav = new \IhrV2\Models\AttendanceRemarkAttachment($arr);
        $sav->save();
        return true;
    }

    // set inactive current remark status
    // ----------------------------------
    public function dbUpdateAttRemarkHistory($id, $uid) {
        $q = \IhrV2\Models\AttendanceRemarkHistory::where(
            [
                'att_remark_id' => $id,
                'user_id'       => $uid,
                'flag'          => 1
            ])
        ->update(array(
            'flag' => 0
        ));
        return true;
    }

    // set attendance remark cancel
    // ----------------------------
    public function dbUpdateAttRemarkCancel($id, $updated_by) {
        $arr = array(
            'updated_by' => $updated_by,
            'active'     => 2 // inactive
        );
        $save = \IhrV2\Models\AttendanceRemark::where(
            [
                'id' => $id
            ])
        ->update($arr);
        return true;
    }

    // insert new remark status
    // ------------------------
    public function dbInsertAttRemarkHistory($arr) {
        $data = array(
            'att_remark_id' => $arr[0],
            'user_id'       => $arr[1],
            'action_date'   => Carbon::now(),
            'action_by'     => auth()->user()->id,
            'action_remark' => $arr[3],
            'status'        => $arr[2], // 1 = pending | 2 = approve | 3 = updated | 4 = canceled | 5 = rejected
            'flag'          => 1
        );
        $sav = new \IhrV2\Models\AttendanceRemarkHistory($data);
        $sav->save();
        return true;
    }

    // update attendance remark
    // ------------------------
    public function dbUpdateAttRemark($arr, $status) {
        $data = array(
            'other_id' => $status
        );
        $save = \IhrV2\Models\AttendanceRemark::where(
            [
                'user_id'  => $arr['uid'],
                'icno'     => $arr['icno'],
                'sitecode' => $arr['sitecode'],
                'date'     => $arr['date'],
                'active'   => 1
            ])
        ->update($data);
        return true;
    }

    // update attendance remark note
    // -----------------------------
    public function dbUpdateAttRemarkNote($i, $user) {
        $q = \IhrV2\Models\AttendanceRemark::where(
            [
                'id'       => $i['id'],
                'user_id'  => $user['uid'],
                'icno'     => $user['icno'],
                'sitecode' => $user['sitecode'],
                'date'     => $user['date']
            ])
        ->update(array(
            'notes'      => $i['remark'],
            'updated_by' => auth()->user()->id
        ));
        return true;
    }

    // insert attendance_remark_others
    // -------------------------------
    public function dbInsertAttendanceOther($i, $user, $date) {
        $data = array(
            'user_id'     => $user['uid'],
            'icno'        => $user['icno'],
            'sitecode'    => $user['sitecode'],
            'date'        => $date,
            'notes'       => $i['att_notes'],
            'action_date' => Carbon::now(),
            'action_by'   => auth()->user()->id,
            'active'      => 1
        );
        $sav = new \IhrV2\Models\AttendanceRemarkOther($data);
        $sav->save();
        return $sav->id;
    }

    // cancel attendance manual
    // ------------------------
    public function dbUpdateAttendanceManual($id) {
        $q = \IhrV2\Models\AttendanceManual::where(
            [
                'id' => $id
            ])
        ->update(array(
            'updated_by' => auth()->user()->id,
            'active'     => 2
        ));
        return true;
    }

    // delete attendance manual at attendance_log_records
    // case: if cancel the attendance manual
    // --------------------------------------------------
    public function dbDeleteAttendanceLogRecord($i) {
        $q = \IhrV2\Models\Attendance::where(
            [
                'att_log_id'      => $i->att_id,
                // 'att_mykad_ic'    => $i->icno,
                // 'location_id'     => $i->sitecode,
                // 'att_record_type' => $i->type_id,
                // 'att_log_time'    => $i->att_log_time,
            ])->delete();
        return true;
    }

    // update type_id of attendance manual
    // -----------------------------------
    public function dbUpdateAttManualTypeID($id, $type_id) {
        $save = \IhrV2\Models\AttendanceManual::where(
            [
                'id' => $id
            ])
        ->update(array(
            'type_id' => $type_id
        ));
        return true;
    }

    // cancel attendance time slip
    // ---------------------------
    public function dbUpdateAttendanceOther($id, $uid, $sitecode) {
        $save = \IhrV2\Models\AttendanceRemarkOther::where(
            [
                'id'       => $id,
                'user_id'  => $uid,
                'sitecode' => $sitecode
            ])
        ->update(array(
            'active'     => 2,
            'updated_by' => auth()->user()->id
        ));
        return true;
    }

    // insert attendance manual attachment
    // -----------------------------------
    public function dbInsertAttManualAttachment($id, $uid, $arr) {
        $arr = array(
            'att_manual_id' => $id,
            'user_id'       => $uid,
            'filename'      => $arr['filename'],
            'ext'           => strtolower($arr['fileext']),
            'size'          => $arr['filesize'],
            'thumb_name'    => $arr['filethumb'],
            'status'        => 1
        );
        $sav = new \IhrV2\Models\AttendanceManualAttachment($arr);
        $sav->save();
        return true;
    }

    // update attendance gps
    // ---------------------
    public function dbUpdateAttGps($id)
    {
        $q = \IhrV2\Models\AttendanceGps::where(['id' => $id, 'status' => 1])
        ->update(array(
            'status'     => 2,
            'updated_by' => auth()->user()->id
        ));
        return true;
    }

    // delete attendance log records
    // -----------------------------
    public function dbDeleteAttLog($id)
    {
        return \IhrV2\Models\Attendance::where('att_log_id', $id)->delete();
    }

    // inactive app version
    // --------------------
    public function dbInactiveAppVersion()
    {
        $q = \IhrV2\Models\AttendanceApp::where(['status' => 1])
        ->update(array(
            'status'     => 2,
        ));
        return true;        
    }

    // insert app version
    // ------------------
    public function dbInsertAppVersion($i)
    {
        $arr = array(
            'version' => $i['version'],
            'build_no' => $i['build_no'],
            'server1' => $i['server1'],
            'server2' => $i['server2'],
            'server3' => $i['server3'],
            'server4' => $i['server4'],
            'server5' => $i['server5'],
            'status' => 1
        );
        $sav = new \IhrV2\Models\AttendanceApp($arr);
        $sav->save();
        return true;
    }

    // update app version
    // ------------------
    public function dbUpdateAppVersion($i, $id)
    {
        $q = \IhrV2\Models\AttendanceApp::find($id)
        ->update(array(
            'version' => $i['version'],
            'build_no' => $i['build_no'],
            'server1' => $i['server1'],
            'server2' => $i['server2'],
            'server3' => $i['server3'],
            'server4' => $i['server4'],
            'server5' => $i['server5']
        ));
        return true;     
    }


}
