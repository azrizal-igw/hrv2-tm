<?php namespace IhrV2\Repositories\Attendance;

use Carbon\Carbon;
use IhrV2\Contracts\Attendance\AttendanceInterface;
use IhrV2\Contracts\Attendance\DbAttendanceInterface;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Contracts\Maintenance\MaintenanceInterface;
use IhrV2\Contracts\Download\DownloadInterface;
use IhrV2\Helpers\CommonHelper;
use IhrV2\User;
use IhrV2\Helpers\AttendanceHelper;
use DB;
use Session;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AttendanceRepository implements AttendanceInterface
{
    protected $db_att_repo;
    protected $leave_repo;
    protected $user_repo;
    protected $email_repo;
    protected $upload_repo;
    protected $mtn_repo;
    protected $down_repo;

    public function __construct(DbAttendanceInterface $db_att_repo, LeaveInterface $leave_repo, UserInterface $user_repo, EmailInterface $email_repo, UploadInterface $upload_repo, MaintenanceInterface $mtn_repo, DownloadInterface $down_repo)
    {
        $this->db_att_repo = $db_att_repo;
        $this->leave_repo = $leave_repo;
        $this->user_repo = $user_repo;
        $this->email_repo = $email_repo;
        $this->upload_repo = $upload_repo;
        $this->mtn_repo = $mtn_repo;
        $this->down_repo = $down_repo;
    }
    
    // view attendance details
    // -----------------------
    public function getAttDetail($icno, $sitecode, $date, $type)
    {
        // get attendance latest
        // ---------------------
        $latest = $this->getAttendanceLatest($icno, $sitecode, $date, $type);

        // get attendance details
        // ----------------------
        $att = $this->getAttInfo($latest->att_log_id);

        // no record found
        // ---------------
        if (empty($latest) || empty($att)) {

            // return to view attendance
            // -------------------------
            return redirect()->route('sv.attendance.daily.view', array($date))->with([
                'message' => 'Record not found.',
                'label'   => 'alert alert-danger alert-dismissible'
            ]);
        }

        // initlal variables
        // -----------------
        $method = null;
        $mobile = array();
        $manual = array();

        // check attendance status
        // -----------------------
        if (strpos($att->att_sys_status, 'Mobile') !== false) {

            // punch using mobile apps
            // -----------------------
            if (!empty($att->UserAttGpsActive)) {
                if ($att->UserAttGpsActive->att_log_id == $att->att_log_id) {
                    $mobile = $att->UserAttGpsActive;
                    $method = 'Mobile Apps';
                }
            }
        }

        // check other method
        // ------------------
        else {
            $status = explode(', ', $att->att_sys_status);
            if (count($status) == 4) {

                // attendance system
                // -----------------
                if (strpos($status[2], 'server') !== false) {
                    $method = 'Attendance System';
                } elseif (strpos($status[2], 'local') !== false) {
                    $method = 'Attendance System';
                }

                // attendance manual
                // -----------------
                elseif (strpos($status[2], 'IHRV2') !== false) {
                    $method = 'Attendance Manual';

                    // check manual attendance
                    // -----------------------
                    if (!empty($att->UserAttManualActive)) {
                        $manual = $att->UserAttManualActive;
                    }
                }

                // unknown
                // -------
                else {
                    $method = 'Unknown';
                }
            }

            // invalid status
            // --------------
            else {
                $method = 'Invalid Status';
            }
        }

        // return result
        // -------------
        $data = array(
            'att' => $att,
            'method' => $method,
            'mobile' => $mobile,
            'manual' => $manual
        );
        return $data;
    }

    // get attendance by month range
    // -----------------------------
    public function showAttRange($icno, $sitecode, $year, $from, $to)
    {

        // get user info
        // -------------
        $user = $this->user_repo->getUserSiteCurrent($icno, $sitecode);

        // get user id
        // -----------
        $uid = $user->id;
        $name = $user->name;

        // get site info
        // -------------
        $site = $user->sitecode.' '.$user->SiteNameInfo->name;

        // get position id
        // ---------------
        $position_id = $user->UserCurrentJob2->position_id;

        // get month range
        // ---------------
        $months = range($from, $to);

        // set array
        // ---------
        $datas = [];

        // loop months
        // -----------
        if (!empty($months)) {
            foreach ($months as $m) {

                // get attendance lists
                // --------------------
                $arr = array($uid, $icno, $sitecode, $year, $m, $position_id);
                $datas[] = $this->getAttByMonthList($arr);
            }
        }

        // set filename
        // ------------
        $filename = $sitecode . '_' . $icno . '_' .date('YmdHis') . '_' . str_random(20);

        // return result
        // -------------
        return array(
            'datas' => $datas,
            'filename' => $filename,
            'name' => $name,
            'site' => $site
        );
    }

    // view monthly attendance
    // -----------------------
    public function showAttUser($request, $icno, $sitecode)
    {
        // no request
        // ----------
        if (empty($request)) {

            // session year
            // ------------
            if (session()->has('year')) {
                $year = session()->get('year');
            } else {
                $year = date('Y');
            }

            // session month
            // -------------
            if (session()->has('month')) {
                $month = session()->get('month');
            } else {
                $month = date('m');
            }
        }

        // have request
        // ------------
        else {

            // year session
            // ------------
            if (!empty($request->year)) {
                session()->put('year', $request->year);
                $year = session()->get('year');
            } else {
                Session::forget('year');
                $year = null;
            }

            // month session
            // -------------
            if (!empty($request->month)) {
                session()->put('month', $request->month);
                $month = session()->get('month');
            } else {
                Session::forget('month');
                $month = null;
            }

            // download attendance by word
            // ---------------------------
            if (!empty($request->btn_word) &&
                !empty($request->month_from) &&
                !empty($request->month_to) &&
                $request->month_from <= $request->month_to) {

                // get attendance record
                // ---------------------
                $datas = $this->showAttRange($icno, $sitecode, $year, $request->month_from, $request->month_to);

                // download word
                // -------------
                $word = $this->down_repo->getAttByDoc($datas);

                // download file
                // -------------
                return response()->download($word);
            }

            // download attendance by pdf
            // --------------------------
            if (!empty($request->btn_pdf) &&
                !empty($request->month_from) &&
                !empty($request->month_to) &&
                $request->month_from <= $request->month_to) {

                // get the record
                // --------------
                $datas = $this->showAttRange($icno, $sitecode, $year, $request->month_from, $request->month_to);

                // download pdf
                // ------------
                $pdf = PDF::loadView('modules.attendance.download.pdf', compact('datas'));
                return $pdf->download($datas['filename'].'.pdf');
            }

            // download attendance by excel
            // ----------------------------
            if (!empty($request->btn_excel) &&
                !empty($request->month_from) &&
                !empty($request->month_to) &&
                $request->month_from <= $request->month_to) {

                // get attendance record
                // ---------------------
                $datas = $this->showAttRange($icno, $sitecode, $year, $request->month_from, $request->month_to);

                // download excel
                // --------------
                $this->down_repo->getAttByExcel($datas, $sitecode, $icno);
            }

            // click set remark of selected date
            // ---------------------------------
            if (!empty($request->btn_remark) && $request->btn_remark == 'remark') {
                // have check the date
                // -------------------
                if (!empty($request->chk_date)) {
                    session()->put('dates', $request->chk_date);
                    return redirect()->route('mod.attendance.remark.set', array($icno, $sitecode))->withInput([
                        'dates' => $request->chk_date
                    ]);
                } else {
                    return redirect()->route('mod.attendance.user', array($icno, $sitecode))->with([
                        'message' => 'Not selected any date.',
                        'label'   => 'alert alert-danger alert-dismissible'
                    ]);
                }
            }
        }

        // get user info
        // -------------
        $user = $this->user_repo->getUserSiteCurrent($icno, $sitecode);

        // check user job and contract
        // ---------------------------
        if (!empty($user->UserCurrentJob2) && !empty($user->UserCurrentContract2)) {

            // set header
            // ----------
            $header = array(
                'parent'  => 'Attendance',
                'child'   => 'Site',
                'child-a' => route('mod.attendance.site'),
                'sub'     => 'Staff',
                'sub-a'   => route('mod.attendance.user', array($icno, $sitecode)),
                'icon'    => 'clock',
                'title'   => $user->name
            );

            // get user id
            // -----------
            $uid = $user->id;

            // get position id
            // ---------------
            $position_id = $user->UserCurrentJob2->position_id;

            // get attendance lists
            // --------------------
            $arr = array($uid, $icno, $sitecode, $year, $month, $position_id);
            $data = $this->getAttByMonthList($arr);

            // get search sessions
            // -------------------
            $sessions = array('month' => $month, 'year' => $year);

            // get month list
            // --------------
            $months = $this->user_repo->getMonthList();

            // get year list
            // -------------
            $years = $this->leave_repo->getListYear();
            return View('modules.attendance.user', compact('header', 'arr', 'data', 'sessions', 'position_id', 'months', 'years'));
        }

        // redirect to previous page
        // -------------------------
        else {
            return redirect()->route('mod.attendance.site')->with([
                'message' => 'Staff Job/Contract is not found. Please contact HR.',
                'label'   => 'alert alert-danger alert-dismissible'
            ]);
        }
    }

    // get site attendance
    // -------------------
    public function getAttSite($request = null)
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Site',
            'child-a' => route('mod.attendance.site'),
            'icon'    => 'share',
            'title'   => 'Site'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $keyword = CommonHelper::checkSession('keyword');
            $status_id = CommonHelper::checkSession('status_id');
        } else {
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('site_id', 'region_id', 'phase_id', 'keyword', 'status_id'));
                return redirect()->route('mod.attendance.site');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get lists of regions
        // --------------------
        $regions = $this->user_repo->getRegionList();

        // get lists of phases
        // -------------------
        $phases = $this->user_repo->getPhaseList();

        // get lists staff status
        // ----------------------
        $staff_status = $this->user_repo->getStaffStatusList();

        // set sessions
        // ------------
        $sessions = array(
            'site_id'   => $site_id,
            'region_id' => $region_id,
            'phase_id'  => $phase_id,
            'keyword'   => $keyword,
            'status_id' => $status_id
        );

        // query sites
        // -----------
        $lists = $this->getSiteListAll($codes, $sessions);
        return View('modules.attendance.site.index', compact('header', 'lists', 'sites', 'regions', 'phases', 'staff_status', 'sessions'));
    }

    // get attendance manual
    // ---------------------
    public function getAttendanceManual($request = null)
    {
        $header = array(
            'parent' => 'Attendance',
            'child'  => 'Manual',
            'icon'   => 'list',
            'title'  => 'Manual'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('site_id', 'region_id', 'phase_id', 'keyword'));
                return redirect()->route('mod.attendance.manual');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get drop down lists
        // -------------------
        $phases = $this->user_repo->getPhaseList();
        $regions = $this->user_repo->getRegionList();

        // set sessions
        // ------------
        $sessions = array(
            'site_id'   => $site_id,
            'region_id' => $region_id,
            'phase_id'  => $phase_id,
            'keyword'   => $keyword
        );

        // get attendance manual
        // ---------------------
        $att_manual = $this->getAttManualAll($sessions, $codes);
        return View('modules.attendance.manual.index', compact('header', 'sites', 'phases', 'regions', 'att_manual', 'sessions'));
    }

    // get attendance remark
    // ---------------------
    public function getAttRemark($request = null)
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Remark',
            'icon'    => 'speech',
            'title'   => 'Remark'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $remark_id = CommonHelper::checkSession('remark_id');
            $status_id = CommonHelper::checkSession('status_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('site_id', 'region_id', 'phase_id', 'remark_id', 'status_id', 'keyword'));
                return redirect()->route('mod.attendance.remark');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $remark_id = CommonHelper::checkSessionPost($request->remark_id, $reset, 'remark_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get drop down lists
        // -------------------
        $remarks = $this->getAttendanceStatusArray(array(3, 4, 7, 8));
        $status = $this->getStatusList();
        $phases = $this->user_repo->getPhaseList();
        $regions = $this->user_repo->getRegionList();

        // get remarks
        // -----------
        $arr = array($site_id, $region_id, $phase_id, $remark_id, $status_id, $keyword);
        $att_remarks = $this->getAttendanceRemarkAll($arr, $codes);

        // set sessions
        // ------------
        $sessions = array(
            'site_id'   => $site_id,
            'region_id' => $region_id,
            'phase_id'  => $phase_id,
            'remark_id' => $remark_id,
            'status_id' => $status_id,
            'keyword'   => $keyword
        );
        return view('modules.attendance.remark.index', compact('header', 'sites', 'att_remarks', 'status', 'phases', 'regions', 'remarks', 'sessions'));
    }

    // site supervisor get daily attendance
    // ------------------------------------
    public function getAttDaily($request = null)
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Daily',
            'child-a' => route('sv.attendance.daily'),
            'icon'    => 'clock',
            'title'   => 'Daily'
        );

        // empty request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $year = date('Y');
            $month = date('m');
        }

        // have request
        // ------------
        else {
            $year = CommonHelper::checkSessionPost($request->year, 0, 'year');
            $month = CommonHelper::checkSessionPost($request->month, 0, 'month');

            // click set remark of selected date
            // ---------------------------------
            if (!empty($request->btn_remark) && $request->btn_remark == 'remark') {

                // have check the date
                // -------------------
                if (!empty($request->chk_date)) {
                    session()->put('dates', $request->chk_date);
                    return redirect()->route('sv.attendance.daily.set')->withInput([
                        'dates' => $request->chk_date
                    ]);
                } 
                else {
                    return redirect()->route('sv.attendance.daily')->with([
                        'message' => 'Not selected any date.',
                        'label'   => 'alert alert-danger alert-dismissible'
                    ]);
                }
            }
        }

        // check if year still empty
        // -------------------------
        $year = (empty($year)) ? date('Y') : $year;

        // get user info
        // --------------
        $uid = auth()->user()->id;
        $icno = auth()->user()->icno;
        $sitecode = auth()->user()->sitecode;

        // get position id
        // ---------------
        $position_id = session()->get('job')['position_id'];

        // get attendance lists
        // --------------------
        $arr = array($uid, $icno, $sitecode, $year, $month, $position_id);
        $data = $this->getAttByMonthList($arr);

        // get lists of year
        // -----------------
        $years = $this->leave_repo->getListYear();

        // get search sessions
        // -------------------
        $sessions = array('month' => $month, 'year' => $year);
        return View('attendance.daily', compact('header', 'years', 'data', 'sessions'));
    }

    // get fasting dates
    // -----------------
    public function getFastingDate($state_id, $year)
    {
        $q = \IhrV2\Models\FastingDate::whereHas(
            'ListStates',
            function ($a) use ($state_id) {
                $a->where('state_id', $state_id);
            }
        )
        ->where(
            [
                'year'   => $year,
                'active' => 1
            ]
        )
        ->first();
        return $q;
    }

    // get attendance limit by date range
    // ----------------------------------
    public function getLimitByRange($group, $date, $arr)
    {
        // initial status
        // --------------
        $status = 0;

        // check if have new work hour
        // 2nd priority
        // ---------------------------
        if ($arr['work_hour']['status'] == 1) {
            $time_in = $arr['work_hour']['time']['in'];
            $time_out = $arr['work_hour']['time']['out'];
            $status = 1;
        }

        // check if have fasting time
        // 1st priority
        // --------------------------
        if ($arr['fasting']['status'] == 1) {
            $time_in = $arr['fasting']['time']['in'];
            $time_out = $arr['fasting']['time']['out'];
            $status = 1;
        }

        // no new work hour or fasting time
        // --------------------------------
        if ($status == 0) {

            // peninsular time
            // ---------------
            if ($group == 1) {
                $time_in = '09:00:00';
                $time_out = '18:00:00';
            }

            // sabah & sarawak time
            // --------------------
            else {
                $time_in = '08:00:00';
                $time_out = '17:00:00';
            }
        }

        // set into array
        // --------------
        $data[$date] = array('time_in' => $time_in, 'time_out' => $time_out);

        // return limit
        // ------------
        return $data;
    }

    // check attendance in (0 = in, 1 = out)
    // -------------------------------------
    public function getAttendanceTime($icno, $type, $date, $order)
    {
        $q = \IhrV2\Models\Attendance::select('att_log_time')
        ->where('att_mykad_ic', $icno)
        ->whereDate('att_log_time', '=', $date)
        ->where('att_record_type', $type)
        ->orderBy('att_log_time', $order)
        ->first();
        return $q;
    }

    public function getAttendanceByDate($icno, $sitecode, $date)
    {
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', 'att_log_time', 'att_record_type')
        ->selectRaw("MIN(att_log_time) AS att_in, MAX(att_log_time) AS att_out")
        ->where('att_mykad_ic', $icno)
        ->where('location_id', $sitecode)
        ->whereDate('att_log_time', '=', $date)
        ->groupBy('att_mykad_ic', 'location_id', \DB::raw('DATE(att_log_time)'), 'att_record_type')
        ->orderBy('att_record_type', 'asc')
        ->orderBy('att_log_time', 'asc')
        ->get()
        ->toArray();
        return $q;
    }

    // get punch details of selected in/out
    // ------------------------------------
    public function getAttendanceByPunch($type_id, $icno, $sitecode, $date)
    {
        if ($type_id == 0) {
            $raw = "MIN(att_log_time) AS att_in";
            $order = 'att_in ASC';
        }
        else {
            $raw = "MAX(att_log_time) AS att_out";
            $order = 'att_out DESC';
        }
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', 'att_log_time', 'att_record_type', 'att_timestamp', 'ip_add', 'att_sys_status')
        ->selectRaw($raw)       
        ->where(['att_mykad_ic' => $icno, 'location_id' => $sitecode, 'att_record_type' => $type_id])
        ->whereDate('att_log_time', '=', $date)
        ->groupBy('att_mykad_ic', 'location_id', 'att_record_type', 'att_timestamp', 'ip_add', 'att_sys_status')
        ->orderByRaw(\DB::raw($order))
        ->first();
        return $q;
    }

    // get total attendance in a month
    // -------------------------------
    public function getAttendanceCurrMonth($icno, $sitecode, $type, $month)
    {
        $q = \IhrV2\Models\Attendance::where('att_mykad_ic', $icno)
        ->where('location_id', $sitecode)
        ->whereMonth('att_log_time', '=', $month)
        ->where('att_record_type', $type)
        ->groupBy('att_mykad_ic', 'location_id', \DB::raw('DATE(att_log_time)'), 'att_record_type')
        ->count();
        return $q;
    }

    // get attendance remark that pending
    // ----------------------------------
    public function getAttRemarkPending($uid, $icno, $sitecode, $date)
    {
        $q = \IhrV2\Models\AttendanceRemark::has('AttPendingStatus')
        ->with(array('AttStatusName'))
        ->where([
            'user_id' => $uid,
            'icno' => $icno,
            'sitecode' => $sitecode,
            'date' => $date,
            'active' => 1
        ])
        ->first();
        return $q;
    }

    // get all attendance remark
    // -------------------------
    public function getAttendanceRemarkAll($arr, $codes)
    {
        // get value from input
        // --------------------
        $site_id = $arr[0];
        $region_id = $arr[1];
        $phase_id = $arr[2];
        $remark_id = $arr[3];
        $status_id = $arr[4];
        $keyword = $arr[5];
        $jobs = array('region_id' => $region_id, 'phase_id' => $phase_id);

        // query attendance remark (flag = 1)
        // ----------------------------------
        $q = \IhrV2\Models\AttendanceRemark::with(array('AttSiteInfo', 'AttStatusName', 'AttUserInfo'))
        ->whereHas('AttActiveStatus', function ($arh) use ($status_id) {
            if (!empty($status_id)) {
                $arh->where('status', $status_id);
            }
        })
        ->with(array('AttActiveStatus' => function ($h) {
            $h->with(array('AttRemarkStatusName'));
        }))
        ->with(array('AttUserJob' => function ($i) {
            $i->with(array('PositionName', 'PhaseName'));
        }))
        ->where(function ($s) use ($site_id) {
            if (!empty($site_id)) {
                $s->where('sitecode', $site_id);
            }
        })
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('sitecode', 'like', $code . '%');
                }
            }
        })
        ->where(function ($sid) use ($remark_id) {
            if (!empty($remark_id)) {
                $sid->where('status_id', $remark_id);
            }
        })
        ->whereHas('AttUserJob', function ($j) use ($jobs) {
            foreach ($jobs as $column => $key) {
                if (!is_null($key)) {
                    $j->where($column, $key);
                }
            }
        })
        ->whereHas('AttUserInfo', function ($i) use ($keyword) {
            $i->where('status', 1);
            if (!empty($keyword)) {
                $i->where('name', 'like', '%' . $keyword . '%');
                $i->orWhere('icno', 'like', '%' . $keyword . '%');
                $i->orWhere('username', 'like', '%' . $keyword . '%');
                $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
            }
        })
        // ->where(['active' => 1])
        ->orderBy('id', 'desc')
        ->paginate(20);
        return $q;
    }

    public function getAttendanceRemarkList()
    {
        return \IhrV2\Models\AttendanceStatus::orderBy('id', 'ASC')->pluck('name', 'id');
        // return \IhrV2\Models\Remark::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Remove]', 0);
    }

    public function getAttendanceRemarkListSelect()
    {
        return \IhrV2\Models\AttendanceStatus::orderBy('name', 'ASC')
        ->pluck('name', 'id')
        ->prepend('[Status]', '');
    }

    public function getAttendanceStatusArray($arr)
    {
        return \IhrV2\Models\AttendanceStatus::whereIn('id', $arr)
        ->orderBy('id', 'ASC')
        ->pluck('name', 'id')
        ->prepend('[Remark]', '');
    }

    public function getAttendanceStatusList()
    {
        return \IhrV2\Models\AttendanceStatus::whereNotIn('id', array(1, 2, 4, 5, 6, 8, 9))
        ->orderBy('name', 'ASC')
        ->pluck('name', 'id')
        ->prepend('[Status]', '');
    }

    public function getAttendanceStatusAll()
    {
        return \IhrV2\Models\AttendanceStatus::orderBy('id', 'ASC')
        ->pluck('name', 'id')
        ->prepend('[Status]', '');
    }

    // get attendance status by id
    // ---------------------------
    public function getAttStatusByID($id)
    {
        return \IhrV2\Models\AttendanceStatus::find($id);
    }

    // get total attendance punch
    // --------------------------
    public function getTotalAttPunch($codes, $date)
    {
        $q = \IhrV2\Models\Attendance::select(\DB::raw('COUNT(DISTINCT att_mykad_ic) AS total'), \DB::raw('(CASE WHEN att_record_type = 0 THEN "In" ELSE "Out" END) AS att_type'), 'att_record_type')
        ->whereDate('att_log_time', '=', $date)
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('location_id', 'like', $code . '%');
                }
            }
        })
        ->groupBy('att_record_type')
        ->get();
        return $q;
    }

    public function getPunchByState($date)
    {
        $q = \IhrV2\Models\Attendance::select(\DB::raw('(CASE WHEN location_id LIKE "S%" THEN "Sabah" WHEN location_id LIKE "Q%" THEN "Sarawak" WHEN location_id LIKE "B%" THEN "Tengah" WHEN location_id LIKE "N%" THEN "Tengah" WHEN location_id LIKE "C%" THEN "Timur 2" WHEN location_id LIKE "K%" THEN "Utara" WHEN location_id LIKE "A%" THEN "Utara" WHEN location_id LIKE "D%" THEN "Timur 1" WHEN location_id LIKE "T%" THEN "Timur 1" WHEN location_id LIKE "M%" THEN "Selatan" WHEN location_id LIKE "J%" THEN "Selatan" END) as state'), \DB::raw('SUM(CASE WHEN att_record_type = 0 THEN 1 ELSE 0 END) AS att_in'), \DB::raw('SUM(CASE WHEN att_record_type = 1 THEN 1 ELSE 0 END) AS att_out'))
        ->whereDate('att_log_time', '=', $date)
        ->groupBy(\DB::raw('(CASE WHEN location_id LIKE "S%" THEN "Sabah" WHEN location_id LIKE "Q%" THEN "Sarawak" WHEN location_id LIKE "B%" THEN "Tengah" WHEN location_id LIKE "N%" THEN "Tengah" WHEN location_id LIKE "C%" THEN "Timur 2" WHEN location_id LIKE "K%" THEN "Utara" WHEN location_id LIKE "A%" THEN "Utara" WHEN location_id LIKE "D%" THEN "Timur 1" WHEN location_id LIKE "T%" THEN "Timur 1" WHEN location_id LIKE "M%" THEN "Selatan" WHEN location_id LIKE "J%" THEN "Selatan" END)'))
        ->get();
        return $q;
    }

    // get total attendance remark
    // ---------------------------
    public function getTotalAttRemarkToday($codes, $date)
    {
        $q = \IhrV2\Models\AttendanceRemark::whereDate('action_date', '=', $date)
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('sitecode', 'like', $code . '%');
                }
            }
        })
        ->where('active', 1)
        ->count();
        return $q;
    }

    // get total attendance manual
    // ---------------------------
    public function getTotalAttRemarkManual($codes, $date)
    {
        $q = \IhrV2\Models\AttendanceManual::whereDate('action_date', '=', $date)
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('sitecode', 'like', $code . '%');
                }
            }
        })
        ->where('active', 1)
        ->count();
        return $q;
    }

    // get attendance limit by date range
    // ----------------------------------
    public function getAttLimitRange($uid, $sitecode, $dates, $year, $contract_id)
    {
        // initial variables
        // -----------------
        $region_id = null;
        $state_id = null;
        $limit_dates = array();

        // get region and state id
        // -----------------------
        $site = $this->leave_repo->getUserStateID($sitecode);
        if (!empty($site)) {
            $region_id = $site->region_id;
            $state_id = $site->state_id;
        }

        // have record
        // -----------
        if (!empty($region_id) && !empty($state_id)) {

            // check fasting date by range
            // ---------------------------
            $fast_time = array();
            $fast_days = array();
            $chk_fast = $this->getFastingDate($state_id, $year);
            if (!empty($chk_fast)) {

                // put fasting time in array
                // -------------------------
                $fast_time = array('in' => $chk_fast->start_time, 'out' => $chk_fast->end_time);

                // get duration of fasting
                // -----------------------
                $fast_days = $this->leave_repo->DateRange($chk_fast->start_date, $chk_fast->end_date);
            }

            // check new work hour
            // -------------------
            $work_time = array();
            $work_days = array();
            $chk_work = $this->mtn_repo->getWorkHourOne($uid, $sitecode, $contract_id);
            if (!empty($chk_work)) {

                // get new work time
                // -----------------
                $work_time = array('in' => $chk_work->time_in, 'out' => $chk_work->time_out);

                // get duration of new work time
                // -----------------------------
                $work_days = $this->leave_repo->DateRange($chk_work->date_from, $chk_work->date_to);
            }

            // check every date
            // ----------------
            if (count($dates) > 0) {
                foreach ($dates as $date) {

                    // have fast duration
                    // ------------------
                    $fast_status = 0;
                    if (!empty($fast_days)) {

                        // check date in fasting duration
                        // ------------------------------
                        if (in_array($date, $fast_days)) {
                            $fast_status = 1;
                        }
                    }

                    // have new work hour
                    // ------------------
                    $work_status = 0;
                    if (!empty($work_days)) {

                        // check date in new work hour
                        // ---------------------------
                        if (in_array($date, $work_days)) {
                            $work_status = 1;
                        }
                    }

                    // peninsular time
                    // ---------------
                    if (!in_array($region_id, array(6, 7, 8))) {
                        $group = 1;
                    }

                    // sabah or sarawak time
                    // ---------------------
                    else {
                        $group = 2;
                    }

                    // set other work time in array
                    // ----------------------------
                    $arr = array(
                        'fasting' => array(
                            'status' => $fast_status,
                            'time' => $fast_time
                        ),
                        'work_hour' => array(
                            'status' => $work_status,
                            'time' => $work_time
                        )
                    );

                    // get limit
                    // ---------
                    $limit_dates[] = $this->getLimitByRange($group, $date, $arr);
                }
            }
        }

        // return limit of dates
        // ---------------------
        return $limit_dates;
    }

    // get attendance limit
    // --------------------
    public function getAttendanceLimitOne($date, $region_id, $cond)
    {
        // initial variables
        // -----------------
        $status = 0;
        $data = array();

        // fasting time exist
        // ------------------
        if (!empty($cond['fasting'])) {
            $fasting = $cond['fasting'];

            // get date range
            // --------------
            $duration = $this->leave_repo->DateRange($fasting->start_date, $fasting->end_date);

            // check if today is fasting date
            // ------------------------------
            if (in_array($date, $duration)) {
                $status = 1;
                $data[$date] = array('time_in' => $fasting->start_time, 'time_out' => $fasting->end_time);
            }
        }

        // no fasting time
        // ---------------
        if ($status == 0) {

            // peninsular limit time
            // ---------------------
            if (!in_array($region_id, array(6, 7, 8))) {
                $data[$date] = array('time_in' => env('ATT_TIME1_IN'), 'time_out' => env('ATT_TIME1_OUT'));
            }

            // sabah n sarawak limit time
            // --------------------------
            else {
                $data[$date] = array('time_in' => env('ATT_TIME2_IN'), 'time_out' => env('ATT_TIME2_OUT'));
            }
        }
        return $data;
    }

    // get attendance time in and out
    // ------------------------------
    public function getAttendanceTimeInOut($att, $limit)
    {
        // set label
        // ---------
        $in = 'Empty';
        $out = 'Empty';

        // set color time
        // --------------
        $col_in = '';
        $col_out = '';

        // set time
        // --------
        $att_in = null;
        $att_out = null;

        // get limit in and out
        // --------------------
        $time_in = null;
        $time_out = null;
        foreach ($limit as $k => $v) {
            $time_in = $v['time_in'];
            $time_out = $v['time_out'];
        }
        
        // get limit time
        // --------------
        $limit_in = Carbon::createFromFormat('H:i:s', $time_in);
        $limit_out = Carbon::createFromFormat('H:i:s', $time_out);

        if (!empty($att)) {
            foreach ($att as $t) {
                if ($t['att_record_type'] == 0) {
                    $att_in = Carbon::createFromFormat('Y-m-d H:i:s', $t['att_in']);
                    if (Carbon::parse(Carbon::createFromFormat('Y-m-d H:i:s', $t['att_in'])->format('H:i:s'))->gt(Carbon::parse($limit_in))) {
                        $col_in = 'text-danger';
                    }
                    $in = '<span class="' . $col_in . '">' . Carbon::parse($t['att_in'])->format('g:i:s A') . '</span>';
                }
                if ($t['att_record_type'] == 1) {
                    $att_out = Carbon::createFromFormat('Y-m-d H:i:s', $t['att_out']);
                    if (Carbon::parse(Carbon::createFromFormat('Y-m-d H:i:s', $t['att_out'])->format('H:i:s'))->lt(Carbon::parse($limit_out))) {
                        $col_out = 'text-danger';
                    }
                    $out = '<span class="' . $col_out . '">' . Carbon::parse($t['att_out'])->format('g:i:s A') . '</span>';
                }
            }
        }

        // return time
        // -----------
        return array('in' => $in, 'out' => $out, 'att_in' => $att_in, 'att_out' => $att_out);
    }

    // get lists of sites
    // ------------------
    public function getSiteListAll($codes, $arr)
    {
        // get value from array
        // --------------------
        $site_id = $arr['site_id'];
        $region_id = $arr['region_id'];
        $phase_id = $arr['phase_id'];
        $keyword = $arr['keyword'];
        $status_id = $arr['status_id'];

        // set exclude values
        // ------------------
        $excludes = [
            ['name', 'NOT LIKE', '%mini pjk%'],
            ['name', 'NOT LIKE', '%saifon%'],
            ['name', 'NOT LIKE', '%pjl%'],
            ['code', '!=', 'X01C001']
        ];

        // query sites
        // -----------
        $q = \IhrV2\Models\Site::orderBy('code', 'ASC')
        ->with(array('UserJobSite' => function ($d) {
            $d->with(array('UserDetail', 'PositionName'));
        }))

        // filter sites by excludes specific values
        // ----------------------------------------
        ->whereHas('PhaseName', function ($ph) use ($excludes) {
            $ph->where($excludes); // remove exclude values
        })

        // initial sites by region
        // -----------------------
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('code', 'like', $code . '%');
                }
            }
        })

        // search by site
        // --------------
        ->where(function ($l) use ($site_id) {
            if (!empty($site_id)) {
                $l->where('code', $site_id);
            }
        })

        // search by region
        // ----------------
        ->where(function ($r) use ($region_id) {
            if (!empty($region_id)) {
                $r->where('region_id', $region_id);
            }
        })

        // search by phase
        // ---------------
        ->where(function ($p) use ($phase_id) {
            if (!empty($phase_id)) {
                $p->where('phase_id', $phase_id);
            }
        })

        // search by keyword
        // -----------------
        ->where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('code', 'like', "%" . $keyword . "%");
                $q->orWhere('name', 'like', "%" . $keyword . "%");
            }
        })
        ->with(array('RegionName', 'PhaseName'))
        ->where('status', 1) // site active
        ->where($excludes) // remove exclude values
        ->orderBy('code', 'ASC')
        ->paginate(20);
        return $q;
    }

    // get attendance by month
    // -----------------------
    public function getAttUserPerMonth($cond)
    {
        $icno = $cond[0];
        $sitecode = $cond[1];
        $year = $cond[2];
        $month = $cond[3];
        $sql = "select x.att_mykad_ic, x.location_id, x.att_record_type, x.att_date, x.att_out, x.att_in, ";
        $sql .="f.att_sys_status as att_status_out, g.att_sys_status as att_status_in, "; // get att_sys_status
        $sql .="f.att_timestamp as att_timestamp_out, g.att_timestamp as att_timestamp_in, "; // get att_timestamp
        $sql .="f.att_log_id as att_id_out, g.att_log_id as att_id_in, "; // get att_log_id
        $sql .="f.ip_add as ip_add_out, g.ip_add as ip_add_in "; // get ip_add
        $sql .="from (select `att_mykad_ic`, `location_id`, `att_record_type`, ";
        $sql .="DATE(att_log_time) AS att_date, MAX(att_log_time) AS att_out, MIN(att_log_time) AS att_in ";
        $sql .="from attendance_log_records ";
        $sql .="where att_mykad_ic = '$icno' ";
        $sql .="and location_id = '$sitecode' ";
        $sql .="and year(att_log_time) = '$year' ";
        $sql .="and month(att_log_time) = '$month' ";
        $sql .="group by att_mykad_ic, location_id, DATE(att_log_time), att_record_type) as x ";
        $sql .="inner join attendance_log_records as f ";
        $sql .="on f.att_mykad_ic = x.att_mykad_ic and f.location_id = x.location_id and f.att_log_time = x.att_out ";
        $sql .="inner join attendance_log_records as g ";
        $sql .="on g.att_mykad_ic = x.att_mykad_ic and g.location_id = x.location_id and g.att_log_time = x.att_in ";
        $att = DB::select($sql);
        // dd($att);
        return $att;
    }

    public function getAttendanceRemark($icno, $sitecode, $date_start, $date_end)
    {
        // convert to proper date
        // ----------------------
        $start = Carbon::createFromFormat('Y-m-d', $date_start);
        $end = Carbon::createFromFormat('Y-m-d', $date_end);

        // query remark
        // ------------
        $q = \IhrV2\Models\AttendanceRemark::with(array('AttActiveStatus' => function ($h) {
            $h->with('AttRemarkStatusName');
        }))
        ->with(array('AttPendingHistory', 'AttStatusName'))
        ->where('icno', $icno)
        ->where('sitecode', $sitecode)
        ->whereBetween('date', array($start->format('Y-m-d'), $end->format('Y-m-d')))
        ->where('active', 1)
        ->get()
        ->toArray();
        return $q;
    }

    public function getAttendanceRemarkOnlyDate($icno, $sitecode, $date_start, $date_end)
    {
        // convert to proper date
        // ----------------------
        $start = Carbon::createFromFormat('Y-m-d', $date_start);
        $end = Carbon::createFromFormat('Y-m-d', $date_end);

        // query remark
        // ------------
        $q = \IhrV2\Models\AttendanceRemark::where('icno', $icno)
        ->where('sitecode', $sitecode)
        ->whereBetween('date', array($start->format('Y-m-d'), $end->format('Y-m-d')))
        ->where('active', 1)
        ->get()
        ->toArray();
        return $q;
    }

    // get attendance remark by date
    // -----------------------------
    public function getAttendanceRemarkByDate($date, $arr)
    {
        $q = \IhrV2\Models\AttendanceRemark::where('user_id', $arr[0])
        ->whereDate('date', '=', $date)
        ->where([
            'icno'     => $arr[1],
            'sitecode' => $arr[2],
            'active'   => 1
        ])
        ->with(array('AttAllStatus' => function ($u) {
            $u->with(array('AttRemarkActionBy', 'AttRemarkStatusName'));
        }))
        ->with(array('AttActiveStatus' => function ($a) {
            $a->with(array('AttRemarkStatusName'));
        }))
        ->with(array(
            'AttStatusName',
            'AttActiveAttachment',
            'AttActionBy'
        ))
        ->first();
        return $q;
    }

    // check remark by date without join any table
    // -------------------------------------------
    public function getAttendanceRemarkByDateOne($date, $arr)
    {
        $q = \IhrV2\Models\AttendanceRemark::where('user_id', $arr['uid'])
        ->whereDate('date', '=', $date)
        ->where([
            'icno'     => $arr['icno'],
            'sitecode' => $arr['sitecode'],
            'active'   => 1
        ])
        ->with(array('AttStatusName'))
        ->first();
        return $q;
    }

    // check if selected month have any leaves
    // ---------------------------------------
    public function getAttendanceLeave($uid, $sitecode, $year, $month)
    {
        $q = \IhrV2\Models\LeaveDate::with(array('LeaveApproveOne'))
        ->with(array('LeaveInfo' => function ($d) {
            $d->with(array('LeaveTypeName')); // leave name
        }))
        ->where([
            'user_id'    => $uid,
                'leave_type' => 1, // available
                'status'     => 1
            ])
        ->whereMonth('leave_date', '=', $month)
        ->whereYear('leave_date', '=', $year)
        ->with(array('LeaveInfo' => function ($h) {
            $h->with('LeaveTypeName');
        }))
        ->get()
        ->toArray();
        return $q;
    }

    // get leave of one date
    // ---------------------
    public function getAttendanceLeaveOne($uid, $sitecode, $date)
    {
        // $q = \IhrV2\Models\LeaveDate::whereHas('LeaveHistoryOne', function($i) {
        //     $i->where('status', 2);
        //     $i->where('flag', 1);
        // })
        $q = \IhrV2\Models\LeaveDate::has('LeaveApproveOne')
        ->whereDate('leave_date', '=', $date)
        ->where([
                'user_id'    => $uid,
                'leave_type' => 1, // available
                'status'     => 1
            ])
        ->with(array('LeaveInfo' => function ($h) {
            $h->with('LeaveTypeName');
        }))
        ->with(array('LeaveApproveOne' => function ($a) {
            $a->with('LeaveTypeInfo');
        }))
        ->first();
        return $q;
    }

    // check if selected date have off day
    // -----------------------------------
    public function getAttendanceOffDayDate($state_id, $types, $date)
    {
        $q = \IhrV2\Models\LeaveOffDate::whereHas(
            'OffDayState',
            function ($i) use ($state_id) {
                $i->where('state_id', $state_id);
            }
        )
        ->with(array('OffTypeName'))
        ->whereIn('type_id', $types)
        ->whereDate('off_date', '=', $date)
        ->where('status', 1)
        ->first();
        return $q;
    }

    // check if selected date have public holiday
    // ------------------------------------------
    public function getAttendancePublicDate($state_id, $date)
    {
        $q = \IhrV2\Models\LeavePublicState::where('state_id', $state_id)
        ->whereDate('date', '=', $date)
        ->where('status', 1)
        ->with(array('PublicName'))
        ->first();
        return $q;
    }

    // admin set multiple remark and approve
    // -------------------------------------
    public function getApplyMultipleRemark($data, $user, $dates)
    {
        // initialize variables
        // --------------------
        $ids = array();
        $attach = array();
        $exist = 0;

        // check dates
        // -----------
        if (!empty($dates)) {
            foreach ($dates as $date) {

                // check if date already have remark
                // ---------------------------------
                $check = $this->getAttendanceRemarkByDateOne($date, $user);

                // no record yet
                // -------------
                if (!empty($check)) {
                    $exist = 1;
                }

                // no record yet and have region manager
                // -------------------------------------
                if ($exist == 0) {

                    // insert attendance remarks (get id remark)
                    // -----------------------------------------
                    $id = $this->db_att_repo->dbInsertAttRemark($data, $user, $date, 1);
                    $ids[] = $id;

                    // check attendance remark history
                    // -------------------------------
                    $chk_history = $this->getCheckAttRemarkHistory($id, $user['uid']);
                    if (empty($chk_history)) {

                        // insert attendance remarks histories (set approve and remark by rm)
                        // 3 = is inserted by RM
                        // ------------------------------------------------------------------
                        $arr = array($id, $user['uid'], 3, null);
                        $this->db_att_repo->dbInsertAttRemarkHistory($arr);
                    }
                }
            }

            // check the attachment
            // --------------------
            if (!empty($data['att_file'])) {

                // upload file attachment
                // ----------------------
                $attach = $this->upload_repo->uploadAttRemarkRange($data['att_file'], $ids, $user['uid']);
            }

            // get site supervisor info
            // ------------------------
            $user = $this->user_repo->getUserByID($user['uid']);
            if (!empty($user)) {

                // get remark status
                // -----------------
                $status = $this->getAttStatusByID($data['att_status']);

                // email body info
                // ---------------
                $arr_body = array(
                    'status' => $status->name,
                    'dates'  => $dates,
                    'notes'  => $data['att_notes']
                );

                // set array info
                // --------------
                $arr = array(
                    'mgr_name'  => $user->name,
                    'mgr_email' => $user->email,
                    'attach'    => $attach
                );

                // send email notification to site supervisor
                // ------------------------------------------
                $this->email_repo->getAttRemarkRangeAdmin($arr_body, $arr);

                // success message
                // ---------------
                $msg = array('Multiple Remark successfully Added!', 'success');
            }

            // no user record
            // --------------
            else {
                $msg = array('Staff is not Exist.', 'danger');
            }
        }

        // not selected any date
        // ---------------------
        else {

            // fail message
            // ------------
            $msg = array('Not selected any date', 'danger');
        }
        return $msg;
    }

    // site supervisor set multiple remark
    // -----------------------------------
    public function getApplyRangeRemark($data, $user, $dates)
    {

        // initialize variables
        // --------------------
        $ids = array();
        $url = array();
        $attach = array();

        // check dates
        // -----------
        if (!empty($dates)) {
            foreach ($dates as $date) {

                // check if date already have remark
                // ---------------------------------
                $check = $this->getAttendanceRemarkByDateOne($date, $user);

                // remark not exist yet
                // --------------------
                if (empty($check)) {

                    // insert attendance remarks
                    // -------------------------
                    $id = $this->db_att_repo->dbInsertAttRemark($data, $user, $date);
                    $ids[] = $id;
                    $url[] = route('mod.attendance.user.view', array($user['uid'], $user['icno'], $user['sitecode'], $date));

                    // check attendance remark history
                    // -------------------------------
                    $chk_history = $this->getCheckAttRemarkHistory($id, $user['uid']);
                    if (empty($chk_history)) {

                        // insert attendance remarks histories (set status pending)
                        // --------------------------------------------------------
                        $arr = array($id, $user['uid'], 1, null);
                        $this->db_att_repo->dbInsertAttRemarkHistory($arr);
                    }
                }
            }

            // check the attachment
            // --------------------
            if (!empty($data['att_file'])) {

                // upload file attachment
                // ----------------------
                $attach = $this->upload_repo->uploadAttRemarkRange($data['att_file'], $ids, $user['uid']);
            }

            // get remark notes
            // ----------------
            $remark = array(
                'status' => $this->getAttStatusByID($data['att_status']),
                'notes'  => $data['att_notes']
            );

            // get rm info
            // -----------
            $rm = session()->get('rm_info');

            // email body info
            // ---------------
            $arr_body = array(
                'position_name' => session()->get('position_name'),
                'site_name'     => session()->get('site_name'),
                'remark'        => $remark,
                'dates'         => $dates,
                'url'           => $url
            );

            // get drm region
            // --------------
            $drm = array();
            $cond = array('region_id' => $rm['region_id'], 'position_id' => 2, 'status' => 1);
            $chk_drm = $this->getDrmRegionList($cond);
            if (!empty($chk_drm)) {
                foreach ($chk_drm->toArray() as $d) {
                    $drm[$d['email']] = $d['name'];
                }
            }

            // receiver info
            // -------------
            $arr = array(
                'rm_email' => $rm['email'],
                'rm_name'  => $rm['name'],
                'attach'   => $attach,
                'drm'      => $drm
            );

            // send email notification to rm
            // -----------------------------
            $this->email_repo->getAttRemarkByRange($arr_body, $arr);

            // success message
            // ---------------
            $msg = array('Attendance Remark successfully added.', 'success');
        }

        // not selected any date
        // ---------------------
        else {

            // fail message
            // ------------
            $msg = array('Not selected any date', 'danger');
        }
        return $msg;
    }

    // check multiple attendance remark
    // --------------------------------
    public function getCheckAttRemarkAll($user, $dates)
    {
        $q = \IhrV2\Models\AttendanceRemark::whereIn('date', $dates)
        ->where([
            'user_id'  => $user['uid'],
            'icno'     => $user['icno'],
            'sitecode' => $user['sitecode'],
            'active'   => 1
        ])
        ->get();

        // check if record exist
        // ---------------------
        if (count($q) > 0) {
            $total = 1;
        } else {
            $total = 0;
        }
        return $total;
    }

    // site supervisor apply new request for remark
    // --------------------------------------------
    public function getApplyNewRemark($data, $user, $date)
    {
        // initial variables
        // -----------------
        $exist = 0;
        $report_to = 0;

        // check if date already have remark
        // ---------------------------------
        $check = $this->getAttendanceRemarkByDateOne($date, $user);

        // no record yet
        // -------------
        if (!empty($check)) {
            $exist = 1;
        }

        // check rm
        // --------
        if (!empty(session()->get('rm_info'))) {
            $report_to = 1;
        }

        // no record yet and have region manager
        // -------------------------------------
        if ($exist == 0 && $report_to == 1) {

            // initial array
            // -------------
            $attach = array();

            // insert attendance remarks
            // -------------------------
            $id = $this->db_att_repo->dbInsertAttRemark($data, $user, $date);

            // check attendance remark history
            // -------------------------------
            $chk_history = $this->getCheckAttRemarkHistory($id, $user['uid']);
            if (empty($chk_history)) {

                // insert attendance remarks histories (set status pending)
                // --------------------------------------------------------
                $arr = array($id, $user['uid'], 1, null);
                $this->db_att_repo->dbInsertAttRemarkHistory($arr);
            }

            // check the attachment
            // --------------------
            if (!empty($data['att_file'])) {

                // upload file attachment
                // ----------------------
                $attach = $this->upload_repo->uploadAttRemarkFile($data['att_file'], $id, $user['uid']);
            }

            // get rm info
            // -----------
            $rm = session()->get('rm_info');

            // get attendance info
            // -------------------
            $att = $this->getAttInOut($user['icno'], $user['sitecode'], $date);

            // get saved remark
            // ----------------
            $remark = $this->getRemarkInfo($id);

            // email body info
            // ---------------
            $arr_body = array(
                'position_name' => session()->get('position_name'),
                'site_name'     => session()->get('site_name'),
                'remark'        => $remark,
                'remark_status' => $remark->AttStatusName->name,
                'in'            => $att['in'],
                'out'           => $att['out'],
                'url'           => route('mod.attendance.user.view', array($user['uid'], $user['icno'], $user['sitecode'], $date))
            );

            // get drm region
            // --------------
            $drm = array();
            $cond = array('region_id' => $rm['region_id'], 'position_id' => 2, 'status' => 1);
            $chk_drm = $this->getDrmRegionList($cond);
            if (!empty($chk_drm)) {
                foreach ($chk_drm->toArray() as $d) {
                    $drm[$d['email']] = $d['name'];
                }
            }

            // receiver info
            // -------------
            $arr = array(
                'rm_email' => $rm['email'],
                'rm_name'  => $rm['name'],
                'attach'   => $attach,
                'drm'      => $drm
            );

            // send email notification to rm
            // -----------------------------
            $this->email_repo->getEmailAttApplyRemark($arr_body, $arr);

            // success message
            // ---------------
            $msg = array('Remark successfully added and send to RM for verification.', 'success');
        }

        // fail message
        // ------------
        else {
            if ($exist == 1) {
                $msg = array('Request already exist.', 'danger');
            }
            if ($report_to == 0) {
                $msg = array('Reporting Officer is not set.', 'danger');
            }
        }
        return $msg;
    }

    // get attendance remark by id
    // ---------------------------
    public function getAttRemarkByID($id)
    {
        $q = \IhrV2\Models\AttendanceRemark::find($id);
        return $q;
    }

    // get lists of drm region
    // -----------------------
    public function getDrmRegionList($arr)
    {
        


        
        // only notification 1 receive email
        // ---------------------------------
        $arr['notification'] = 1;

        // query staff drm
        // ---------------
        $q = \IhrV2\User::whereHas(
            'UserLatestJob',
            function ($j) use ($arr) {
                foreach ($arr as $column => $key) {
                    if (!is_null($key)) {
                        $j->where($column, $key);
                    }
                }
            }
        )
        ->where(
            [
                'group_id' => 6,
                'status'   => 1
            ]
        )
        ->get();
        return $q;
    }

    // site supervisor put other info on working status attendance
    // -----------------------------------------------------------
    public function getApplyOtherInfo($data, $user, $date)
    {

        // check if already insert
        // -----------------------
        $check = $this->getAttendanceOtherInfo($user, $date);

        // not exist yet
        // -------------
        if (empty($check)) {

            // insert attendance remark other
            // ------------------------------
            $id = $this->db_att_repo->dbInsertAttendanceOther($data, $user, $date);
        } else {
            $id = $check['id'];
        }

        // check the attachment
        // --------------------
        $attach = array();
        if (!empty($data['att_file'])) {

            // upload file attachment
            // ----------------------
            $attach = $this->upload_repo->uploadAttRemarkOther($data['att_file'], $id, $user['uid']);
        }

        // get rm info
        // -----------
        $rm = session()->get('rm_info');

        // get attendance info
        // -------------------
        $att = $this->getAttInOut($user['icno'], $user['sitecode'], $date);

        // get other info
        // --------------
        $other = $this->getAttendanceOtherByID($id);

        // email body info
        // ---------------
        $arr_body = array(
            'position_name' => session()->get('position_name'),
            'site_name'     => session()->get('site_name'),
            'other'         => $other,
            'date'          => $date,
            'in'            => $att['in'],
            'out'           => $att['out'],
            'url'           => route('mod.attendance.user.view', array($user['uid'], $user['icno'], $user['sitecode'], $date))
        );

        // get drm region
        // --------------
        $drm = array();
        $cond = array('region_id' => $rm['region_id'], 'position_id' => 2, 'status' => 1);
        $chk_drm = $this->getDrmRegionList($cond);
        if (!empty($chk_drm)) {
            foreach ($chk_drm->toArray() as $d) {
                $drm[$d['email']] = $d['name'];
            }
        }

        // receiver info
        // -------------
        $arr = array(
            'rm_email' => $rm['email'],
            'rm_name'  => $rm['name'],
            'attach'   => $attach,
            'drm'      => $drm
        );

        // send email notification to rm
        // -----------------------------
        $this->email_repo->getEmailAttOtherInfo($arr_body, $arr);

        // return message
        // --------------
        return array('Time Slip successfully insert and send to RM.', 'success');
    }

    // get attendance other info by id
    // -------------------------------
    public function getAttendanceOtherByID($id)
    {
        return \IhrV2\Models\AttendanceRemarkOther::find($id);
    }

    // check attendance remark
    // -----------------------
    public function getCheckAttRemark($data, $user, $date)
    {
        $q = \IhrV2\Models\AttendanceRemark::where(
            [
                'user_id'  => $user['id'],
                'icno'     => $user['icno'],
                'sitecode' => $user['sitecode'],
                'date'     => $date,
                'active'   => 1
            ]
        )
        ->first();
        return $q;
    }

    // check attendance remark history
    // -------------------------------
    public function getCheckAttRemarkHistory($id, $uid)
    {
        $q = \IhrV2\Models\AttendanceRemarkHistory::where(
            [
                'att_remark_id' => $id,
                'user_id'       => $uid,
                'status'        => 1,
                'flag'          => 1
            ]
        )
        ->first();
        return $q;
    }

    // check attendance remark history
    // -------------------------------
    public function getCheckRemarkHistOther($id, $uid)
    {
        $q = \IhrV2\Models\AttendanceRemarkHistory::where(
            [
                'att_remark_id' => $id,
                'user_id'       => $uid,
                'status'        => 3,
                'flag'          => 1
            ]
        )
        ->first();
        return $q;
    }

    // check remark approve
    // --------------------
    public function getCheckAttRemarkApprove($id, $uid)
    {
        $q = \IhrV2\Models\AttendanceRemarkHistory::where(
            [
                'att_remark_id' => $id,
                'user_id'       => $uid,
                'status'        => 2,
                'flag'          => 1
            ]
        )
        ->first();
        return $q;
    }

    // get attendance remark
    // ---------------------
    public function getAttRemarkOne($id, $uid)
    {
        $q = \IhrV2\Models\AttendanceRemark::where('id', $id)
        ->where('user_id', $uid)
        ->first();
        return $q;
    }

    // get lists of remark status
    // --------------------------
    public function getRemarkStatusList()
    {
        return \IhrV2\Models\AttendanceStatus::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Remark]', '');
    }

    // get lists of status
    // -------------------
    public function getStatusList()
    {
        return \IhrV2\Models\AttendanceRemarkStatus::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Status]', '');
    }

    // get attendance mobile
    // ---------------------
    public function getAttMobile($uid, $sitecode, $year, $month)
    {
        $q = \IhrV2\Models\AttendanceGps::where(['user_id' => $uid, 'sitecode' => $sitecode, 'status' => 1])
        ->whereYear('datetime', '=', $year)
        ->whereMonth('datetime', '=', $month)
        ->with(array('AttGpsStatus'))
        ->get();
        return $q;
    }

    // display attendance by month
    // ---------------------------
    public function getAttByMonthList($arr)
    {
        // initial variables
        // -----------------
        $uid = $arr[0];
        $icno = $arr[1];
        $sitecode = $arr[2];
        $year = $arr[3];
        $month = $arr[4];
        $position_id = $arr[5];

        // query attendance
        // ----------------
        $cond = array($icno, $sitecode, $year, $month);
        $att = $this->getAttUserPerMonth($cond);

        // get attendance mobile
        // ---------------------
        $mobile = $this->getAttMobile($uid, $sitecode, $year, $month)->toArray();

        // get only all dates
        // ------------------
        $dates = [];
        foreach ($att as $a) {
            $dates[] = Carbon::parse($a->att_date)->format('Y-m-d');
        }

        // get all dates in current month
        // ------------------------------
        $all_dates = $this->leave_repo->getAllDatesInMonth(date($year . '-' . $month));

        // get contract
        // ------------
        $contract = $this->user_repo->getContractByIDSitecode($uid, $sitecode);

        // get limit of current month
        // --------------------------
        $get_limit = $this->getAttLimitRange($uid, $sitecode, $all_dates, $year, $contract->id);

        // set array into a single array (remove key but remain value)
        // -----------------------------------------------------------
        $arr_limit = (array_collapse($get_limit));

        // get today
        // ---------
        $today = date('Y-m-d');

        // start date of set remark
        // ------------------------
        $start = $this->getDateStartRemark(3);

        // get state id
        // ------------
        $site = $this->leave_repo->getUserStateID($sitecode);
        $state_id = $site->state_id;

        // get type of off day
        // -------------------
        $types = $this->leave_repo->getOffDayTypeIDByPosition($position_id);

        // check off day
        // -------------
        $off_dates = array();

        // check if have new work hour
        // ---------------------------
        $work_hour = $this->mtn_repo->getWorkHourOne($uid, $sitecode, $contract->id);
        if (!empty($work_hour)) {

            // get new work hour dates
            // -----------------------
            if (!empty($work_hour->OffDates)) {
                foreach ($work_hour->OffDates as $who) {
                    $off_dates[] = $who['off_date'];
                }
            }
        }

        // check default off day
        // ---------------------
        else {
            $offs = $this->leave_repo->getOffDateMonthYear($state_id, $types, $year, $month);
            if (count($offs) > 0) {
                foreach ($offs as $o) {
                    $off_dates[] = $o['off_date'];
                }
            }
        }

        // check public holiday
        // --------------------
        $public_dates = array();
        $publics = $this->leave_repo->getPublicDateMonthYear($state_id, $year, $month);
        if (count($publics) > 0) {
            foreach ($publics as $p) {
                $public_dates[] = $p['date'];
            }
        }

        // check leaves
        // ------------
        $leave_dates = array();
        $leave_half = array();
        $leave_details = array();
        $leaves = $this->getAttendanceLeave($uid, $sitecode, $year, $month);
        if (count($leaves) > 0) {
            foreach ($leaves as $l) {
                if (!empty($l['leave_approve_one']) && !empty($l['leave_info'])) {

                    // leave must not half day
                    // -----------------------
                    if ($l['leave_info']['is_half_day'] != 1) {
                        $leave_dates[] = $l['leave_date'];
                        $leave_details[] = array('date' => $l['leave_date'], 'name' => $l['leave_info']['leave_type_name']['name']);
                    } else {
                        $leave_half[] = $l['leave_date'];
                        $leave_details[] = array('date' => $l['leave_date'], 'name' => $l['leave_info']['leave_type_name']['name']);
                    }
                }
            }
        }

        // get 1st and last date
        // ---------------------
        $date_start = reset($all_dates);
        $date_end = end($all_dates);

        // get attendance remark
        // ---------------------
        $remarks = $this->getAttendanceRemark($icno, $sitecode, $date_start, $date_end);

        // set remarks info
        // ----------------
        $pending_dates = array();
        $approve_dates = array();
        $rm_approve_dates = array();
        $remark_details = array();
        if (count($remarks) > 0) {
            foreach ($remarks as $r) {

                // get approve remark date
                // -----------------------
                if (!empty($r['att_active_status'])) {

                    // status approved
                    // ---------------
                    if ($r['att_active_status']['status'] == 2) {
                        $approve_dates[] = $r['date'];
                        $remark_details[] = array('date' => $r['date'], 'name' => $r['att_status_name']['name']);
                    }

                    // get remark by rm (status updated)
                    // ---------------------------------
                    if ($r['att_active_status']['status'] == 3) {
                        $rm_approve_dates[] = $r['date'];
                        $remark_details[] = array('date' => $r['date'], 'name' => $r['att_status_name']['name']);
                    }
                }

                // get pending remark date
                // -----------------------
                if (!empty($r['att_pending_history'])) {
                    if ($r['att_pending_history']['status'] == 1) {
                        $pending_dates[] = $r['date'];
                    }
                }
            }
        }

        // get attendance remark other
        // ---------------------------
        $other_dates = array();
        $other = $this->getAttendanceOtherAll($icno, $sitecode, $date_start, $date_end);
        if (!empty($other)) {
            foreach ($other as $o) {
                $other_dates[] = $o['date'];
            }
        }

        // return variables
        // ----------------
        $data = array(
            'dates'            => $dates,
            'att'              => $att,
            'all_dates'        => $all_dates,
            'today'            => $today,
            'limit'            => $arr_limit,
            'start'            => $start,
            'off_dates'        => $off_dates,
            'public_dates'     => $public_dates,
            'leave_dates'      => $leave_dates,
            'leave_half'       => $leave_half,
            'leave_details'    => $leave_details,
            'pending_dates'    => $pending_dates,
            'approve_dates'    => $approve_dates,
            'rm_approve_dates' => $rm_approve_dates,
            'remark_details'   => $remark_details,
            'other_dates'      => $other_dates,
            'mobile'           => $mobile,
            'year'             => $year,
            'month'            => $month
        );
        // dd($data);
        return $data;
    }

    // get start date of set remark
    // ----------------------------
    public function getDateStartRemark($days)
    {
        return Carbon::now()->subDays($days)->format('Y-m-d');
    }

    // get all attendance other info
    // -----------------------------
    public function getAttendanceOtherAll($icno, $sitecode, $date_start, $date_end)
    {

        // convert to proper date
        // ----------------------
        $start = Carbon::createFromFormat('Y-m-d', $date_start);
        $end = Carbon::createFromFormat('Y-m-d', $date_end);

        // query remark
        // ------------
        $q = \IhrV2\Models\AttendanceRemarkOther::where([
            'icno'     => $icno,
            'sitecode' => $sitecode
        ])
        ->whereBetween('date', array($start->format('Y-m-d'), $end->format('Y-m-d')))
        ->where('active', 1)
        ->get()
        ->toArray();
        return $q;
    }

    // display attendance date
    // -----------------------
    public function getAttViewDetail2($id, $uid, $icno, $sitecode, $position_id)
    {
        $off = 0;
        $ph = 0;
        $lv = 0;

        // get remarks
        // -----------
        $remarks = $this->getAttRemarkByID($id);

        // get remark date
        // ---------------
        $date = $remarks->date;

        // get site info
        // -------------
        $site = $this->leave_repo->getUserStateID($sitecode)->toArray();

        // get region id
        // -------------
        $region_id = $site['region_id'];

        // get year from date
        // ------------------
        $year = Carbon::parse($date)->format('Y');

        // get fasting date
        // ----------------
        $fasting = $this->getFastingDate($site['state_id'], $year);
        $cond['fasting'] = $fasting;

        // get time limit
        // --------------
        $limit = $this->getAttendanceLimitOne($date, $region_id, $cond);

        // get attendance
        // --------------
        $get = $this->getAttendanceByDate($icno, $sitecode, $date);

        // get time in and out
        // -------------------
        $att = $this->getAttendanceTimeInOut($get, $limit);

        // get state id
        // ------------
        $site = $this->leave_repo->getUserStateID($sitecode);
        $state_id = $site->state_id;

        // get type of off day
        // -------------------
        $types = $this->leave_repo->getOffDayTypeIDByPosition($position_id);

        // get contract
        // ------------
        $contract = $this->user_repo->getContractByIDSitecode($uid, $sitecode);

        // get off day
        // -----------
        $off_day = null;

        // check if have new work hour
        // ---------------------------
        $work_hour = $this->mtn_repo->getWorkHourOne($uid, $sitecode, $contract->id);
        if (!empty($work_hour)) {

            // get new work hour dates
            // -----------------------
            if (!empty($work_hour->OffDates)) {
                foreach ($work_hour->OffDates as $who) {
                    if ($who->off_date == $date) {
                        $off_day = array('work_hour' => array('date' => $who->off_date, 'label' => 'Cuti Mingguan'));
                    }
                }
            }
        }

        // check default off day
        // ---------------------
        else {
            $chk_off = $this->getAttendanceOffDayDate($state_id, $types, $date);
            if (!empty($chk_off)) {
                $off = 1;
                $off_day = $chk_off->toArray();
            }
        }

        // get public holiday
        // ------------------
        $public = null;
        $chk_public = $this->getAttendancePublicDate($state_id, $date);
        if (!empty($chk_public)) {
            $ph = 1;
            $public = $chk_public->toArray();
        }

        // get leave date
        // --------------
        $leave = $this->getAttendanceLeaveOne($uid, $sitecode, $date);
        if (!empty($leave)) {
            if (!empty($leave->LeaveApproveOne) && !empty($leave->LeaveInfo)) {
                $lv = 1;
            }
        }

        // get time slip
        // -------------
        $arr = array(
            'uid'      => $uid,
            'icno'     => $icno,
            'sitecode' => $sitecode
        );
        $other = $this->getAttendanceOtherInfo($arr, $date);

        // return value
        // ------------
        $data = array(
            'user'    => array($uid, $icno, $sitecode),
            'date'    => $date,
            'limit'   => $limit[$date],
            'att'     => $att,
            'remarks' => $remarks,
            'off_day' => $off_day,
            'public'  => $public,
            'leave'   => $leave,
            'other'   => $other,
        );
        return $data;
    }

    // display attendance date
    // -----------------------
    public function getAttViewDetail($uid, $icno, $sitecode, $position_id, $date)
    {
        $exist = 0; // 1 = remark already exist
        $off = 0;
        $ph = 0;
        $lv = 0;
        $on = 0; // 1 = enable the form

        // get site info
        // -------------
        $site = $this->leave_repo->getUserStateID($sitecode)->toArray();

        // get region id
        // -------------
        $region_id = $site['region_id'];

        // get year from date
        // ------------------
        $year = Carbon::parse($date)->format('Y');

        // get fasting date
        // ----------------
        $fasting = $this->getFastingDate($site['state_id'], $year);
        $cond['fasting'] = $fasting;

        // get time limit
        // --------------
        $limit = $this->getAttendanceLimitOne($date, $region_id, $cond);

        // get attendance
        // --------------
        $get = $this->getAttendanceByDate($icno, $sitecode, $date);

        // get time in and out
        // -------------------
        $att = $this->getAttendanceTimeInOut($get, $limit);

        // get remarks
        // -----------
        $arr = array($uid, $icno, $sitecode);
        $remarks = $this->getAttendanceRemarkByDate($date, $arr);

        // remark exist
        // ------------
        if (!empty($remarks)) {
            $exist = 1;
        }

        // get today date
        // --------------
        $today = Carbon::now()->format('Y-m-d');

        // start date of set remark
        // ------------------------
        $start = $this->getDateStartRemark(3);

        // get state id
        // ------------
        $site = $this->leave_repo->getUserStateID($sitecode);
        $state_id = $site->state_id;

        // get type of off day
        // -------------------
        $types = $this->leave_repo->getOffDayTypeIDByPosition($position_id);

        // get contract
        // ------------
        $contract = $this->user_repo->getContractByIDSitecode($uid, $sitecode);

        // get off day
        // -----------
        $off_day = null;

        // check if have new work hour
        // ---------------------------
        $work_hour = $this->mtn_repo->getWorkHourOne($uid, $sitecode, $contract->id);
        if (!empty($work_hour)) {

            // get new work hour dates
            // -----------------------
            if (!empty($work_hour->OffDates)) {
                foreach ($work_hour->OffDates as $who) {
                    if ($who->off_date == $date) {
                        $off_day = array('work_hour' => array('date' => $who->off_date, 'label' => 'Cuti Mingguan'));
                    }
                }
            }
        }

        // check default off day
        // ---------------------
        else {
            $chk_off = $this->getAttendanceOffDayDate($state_id, $types, $date);
            if (!empty($chk_off)) {
                $off = 1;
                $off_day = $chk_off->toArray();
            }
        }

        // get public holiday
        // ------------------
        $public = null;
        $chk_public = $this->getAttendancePublicDate($state_id, $date);
        if (!empty($chk_public)) {
            $ph = 1;
            $public = $chk_public->toArray();
        }

        // get leave date
        // --------------
        $leave = $this->getAttendanceLeaveOne($uid, $sitecode, $date);
        if (!empty($leave)) {
            if (!empty($leave->LeaveApproveOne) && !empty($leave->LeaveInfo)) {
                $lv = 1;
            }
        }

        // get other info
        // --------------
        $arr = array(
            'uid'      => $uid,
            'icno'     => $icno,
            'sitecode' => $sitecode
        );
        $other = $this->getAttendanceOtherInfo($arr, $date);

        // check if punch via mobile apps
        // ------------------------------
        $mobile = $this->getAttMobileByDate($uid, $sitecode, $date);

        // check if punch in or punch out is empty
        // ---------------------------------------
        if ($att['in'] == 'Empty' || $att['out'] == 'Empty') {

            // check other condition
            // ---------------------
            if ($exist == 0 && $off == 0 && $ph == 0 && $lv == 0) {
                $on = 1;
            }
        }

        // return value
        // ------------
        $data = array(
            'user'    => array($uid, $icno, $sitecode),
            'date'    => $date,
            'limit'   => $limit[$date],
            'att'     => $att,
            'remarks' => $remarks,
            'today'   => $today,
            'off_day' => $off_day,
            'public'  => $public,
            'leave'   => $leave,
            'on'      => $on,
            'other'   => $other,
            'start'   => $start,
            'mobile'  => $mobile
        );
        // dd($data);
        return $data;
    }

    // get attendance other info
    // -------------------------
    public function getAttendanceOtherInfo($user, $date)
    {
        $q = \IhrV2\Models\AttendanceRemarkOther::where([
            'user_id'  => $user['uid'],
            'icno'     => $user['icno'],
            'sitecode' => $user['sitecode'],
            'active'   => 1
        ])
        ->whereDate('date', '=', $date)
        ->with(array('AttAttachment'))
        ->first();
        return $q;
    }

    // get attendance remark info
    // --------------------------
    public function getRemarkInfo($id)
    {
        $q = \IhrV2\Models\AttendanceRemark::find($id);
        return $q;
    }

    public function getAttInfo($id)
    {
        return \IhrV2\Models\Attendance::where('att_log_id', $id)->first();
    }

    // get attendance remark by condition
    // ----------------------------------
    public function getAttRemarkByCond($arr)
    {
        $q = \IhrV2\Models\AttendanceRemark::where([
            'id'       => $arr['id'],
            'user_id'  => $arr['uid'],
            'icno'     => $arr['icno'],
            'sitecode' => $arr['sitecode'],
            'active'   => 1
        ])
        ->firstOrFail();
        return $q;
    }

    // region manager set a new remark
    // -------------------------------
    public function getSetNewRemark($data, $user, $date)
    {
        // initial variables
        // -----------------
        $exist = 0;
        $attach = array();

        // check if date already have remark
        // ---------------------------------
        $check = $this->getAttendanceRemarkByDateOne($date, $user);

        // no record yet
        // -------------
        if (!empty($check)) {
            $exist = 1;
        }

        // no record yet and have region manager
        // -------------------------------------
        if ($exist == 0) {

            // insert attendance remarks (get id remark)
            // -----------------------------------------
            $id = $this->db_att_repo->dbInsertAttRemark($data, $user, $date, 1);

            // check attendance remark history
            // -------------------------------
            $chk_history = $this->getCheckAttRemarkHistory($id, $user['uid']);
            if (empty($chk_history)) {

                // insert attendance remarks histories (set approve and remark by rm)
                // ------------------------------------------------------------------
                $arr = array($id, $user['uid'], 3, null);
                $this->db_att_repo->dbInsertAttRemarkHistory($arr);
            }

            // check the attachment
            // --------------------
            if (!empty($data['att_file'])) {

                // upload file attachment
                // ----------------------
                $attach = $this->upload_repo->uploadAttRemarkFile($data['att_file'], $id, $user['uid']);
            }

            // get site supervisor info
            // ------------------------
            $user = $this->user_repo->getUserByID($user['uid']);
            if (!empty($user)) {

                // get remark info
                // ---------------
                $att_remark = $this->getRemarkInfo($id);

                // get attendance info
                // -------------------
                $att = $this->getAttInOut($user['icno'], $user['sitecode'], $user['date']);

                // email body info
                // ---------------
                $arr_body = array(
                    'att_remark'        => $att_remark,
                    'att_remark_status' => $att_remark->AttStatusName->name,
                    'in'                => $att['in'],
                    'out'               => $att['out']
                );

                // set array info
                // --------------
                $arr = array(
                    'mgr_name'  => $user->name,
                    'mgr_email' => $user->email,
                    'attach'    => $attach
                );

                // send email notification to site supervisor
                // ------------------------------------------
                $this->email_repo->getEmailAttSetRemark($arr_body, $arr);

                // success message
                // ---------------
                $msg = array('Remark successfully Added!', 'success');
            }

            // no user record
            // --------------
            else {
                $msg = array('Staff is not Exist.', 'danger');
            }
        }

        // fail message
        // ------------
        else {
            if ($exist == 1) {
                $msg = array('Remark already Exist', 'danger');
            }
        }
        return $msg;
    }

    // rm approve attendance remark
    // ----------------------------
    public function getAttRemarkApprove($data, $user)
    {
        // set variables
        // -------------
        $id = $data['id'];
        $remark = $data['remark'];

        // check remark history
        // --------------------
        $chk_history = $this->getCheckAttRemarkApprove($id, $user['uid']);
        if (empty($chk_history)) {

            // set current history inactive
            // ----------------------------
            $this->db_att_repo->dbUpdateAttRemarkHistory($id, $user['uid']);

            // insert history approve
            // ----------------------
            $arr = array($id, $user['uid'], 2, $remark);
            $this->db_att_repo->dbInsertAttRemarkHistory($arr);

            // get site supervisor info
            // ------------------------
            $user = $this->user_repo->getUserByID($user['uid']);
            if (!empty($user)) {

                // get remark info
                // ---------------
                $att_remark = $this->getRemarkInfo($id);

                // get attendance info
                // -------------------
                $att = $this->getAttInOut($user['icno'], $user['sitecode'], $user['date']);

                // email body info
                // ---------------
                $arr_body = array(
                    'att_remark'        => $att_remark,
                    'att_remark_status' => $att_remark->AttStatusName->name,
                    'remark'            => $remark,
                    'in'                => $att['in'],
                    'out'               => $att['out']
                );

                // set array info
                // --------------
                $arr = array(
                    'mgr_name'  => $user->name,
                    'mgr_email' => $user->email,
                    'status'    => 'Approved'
                );

                // send email to site supervisor
                // -----------------------------
                $this->email_repo->getEmailAttRemarkResult($arr_body, $arr);

                // set message success
                // -------------------
                $msg = array('Remark successfully Approved.', 'success');
            }

            // user is empty
            // -------------
            else {
                $msg = array('Staff not Exist.', 'danger');
            }
        }

        // record exist
        // ------------
        else {
            $msg = array('Remark already Exist.', 'danger');
        }
        // return message
        // --------------
        return $msg;
    }

    // check attendance remark by status
    // ---------------------------------
    public function getCheckHistoryByStatus($id, $uid, $status)
    {
        $q = \IhrV2\Models\AttendanceRemarkHistory::where(
            [
                'att_remark_id' => $id,
                'user_id'       => $uid,
                'status'        => $status,
                'flag'          => 1
            ]
        )
        ->first();
        return $q;
    }

    // rm cancel/reject attendance remark
    // ----------------------------------
    public function getAttRemarkCancel($data, $user)
    {
        // set variables
        // -------------
        $id = $data['id'];
        $uid = $user['uid'];
        $remark = $data['remark'];
        $type = $data['type'];

        // update remark (set inactive)
        // ----------------------------
        $update = $this->db_att_repo->dbUpdateAttRemarkCancel($id, auth()->user()->id);

        // check remark history
        // --------------------
        $chk_history = $this->getCheckHistoryByStatus($id, $uid, $type);
        if (empty($chk_history)) {

            // inactive current history
            // ------------------------
            $inactive = $this->db_att_repo->dbUpdateAttRemarkHistory($id, $uid);

            // insert new history (cancel/reject)
            // ----------------------------------
            $arr = array($id, $uid, $type, $remark);
            $insert = $this->db_att_repo->dbInsertAttRemarkHistory($arr);

            // get site supervisor info
            // ------------------------
            $chk_user = $this->user_repo->getUserByID($uid);
            if (!empty($chk_user)) {

                // get remark info
                // ---------------
                $att_remark = $this->getRemarkInfo($id);

                // get attendance info
                // -------------------
                $att = $this->getAttInOut($user['icno'], $user['sitecode'], $user['date']);

                // email body info
                // ---------------
                $arr_body = array(
                    'att_remark'        => $att_remark,
                    'att_remark_status' => $att_remark->AttStatusName->name,
                    'remark'            => $remark,
                    'in'                => $att['in'],
                    'out'               => $att['out']
                );

                // get status name
                // ---------------
                if ($type == 4) {
                    $status = 'Canceled';
                } else {
                    $status = 'Rejected';
                }

                // set array info
                // --------------
                $arr = array(
                    'mgr_name'  => $chk_user->name,
                    'mgr_email' => $chk_user->email,
                    'status'    => $status
                );

                // send email notification to site supervisor
                // ------------------------------------------
                $this->email_repo->getEmailAttRemarkResult($arr_body, $arr);

                // return success
                // --------------
                $msg = array('Remark successfully ' . $status . '.', 'success');
            }

            // user is empty
            // -------------
            else {
                $msg = array('Staff not Exist.', 'danger');
            }
        }

        // already have history
        // --------------------
        else {
            $msg = array('Remark already Cancel.', 'danger');
        }
        return $msg;
    }

    // edit notes of attendance remark
    // -------------------------------
    public function getAttRemarkEditNote($data, $user)
    {
        // update note
        // -----------
        $save = $this->db_att_repo->dbUpdateAttRemarkNote($data, $user);

        // get site supervisor info
        // ------------------------
        $chk_user = $this->user_repo->getUserByID($user['uid']);
        if (!empty($chk_user)) {

            // get remark info
            // ---------------
            $remark = $this->getRemarkInfo($data['id']);

            // get attendance info
            // -------------------
            $att = $this->getAttInOut($user['icno'], $user['sitecode'], $user['date']);

            // email body info
            // ---------------
            $arr_body = array(
                'remark' => $remark,
                'status' => $remark->AttStatusName->name,
                'action_remark' => $remark->AttActiveStatus->action_remark,
                'in' => $att['in'],
                'out' => $att['out'],
                'updated_by' => $remark->UserUpdatedBy->name
            );

            // set array info
            // --------------
            $arr = array(
                'mgr_name'  => $chk_user->name,
                'mgr_email' => $chk_user->email,
                'status'    => 'Edited'
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailAttRemarkNoteEdit($arr_body, $arr);

            // return success message
            // ----------------------
            $msg = array('Notes successfully Updated.', 'success');
        }

        // user not exist
        // --------------
        else {

            // return fail message
            // -------------------
            $msg = array('Operation is Fail.', 'danger');
        }

        // return output
        // -------------
        return $msg;
    }

    // get attendance in and out
    // -------------------------
    public function getAttInOut($icno, $sitecode, $date)
    {
        // get site info
        // -------------
        $site = $this->leave_repo->getUserStateID($sitecode)->toArray();

        // get region id
        // -------------
        $region_id = $site['region_id'];

        // get year from date
        // ------------------
        $year = Carbon::parse($date)->format('Y');

        // get fasting date
        // ----------------
        $fasting = $this->getFastingDate($site['state_id'], $year);
        $cond['fasting'] = $fasting;

        // get time limit
        // --------------
        $limit = $this->getAttendanceLimitOne($date, $region_id, $cond);

        // get attendance
        // --------------
        $get = $this->getAttendanceByDate($icno, $sitecode, $date);

        // get time in and out
        // -------------------
        $att = $this->getAttendanceTimeInOut($get, $limit);
        
        // return attendance in and out
        // ----------------------------
        return $att;
    }

    // edit attendance remark
    // ----------------------
    public function getAttRemarkEdit($data, $arr)
    {
        // set variables
        // -------------
        $id = $data['id'];
        $remark = $data['att_notes'];
        $other_remark = $this->getAttStatusByID($data['att_status']);

        // check attendance remark history
        // -------------------------------
        $chk_history = $this->getCheckRemarkHistOther($id, $arr['uid']);
        if (empty($chk_history)) {

            // update remark (other id)
            // ------------------------
            $this->db_att_repo->dbUpdateAttRemark($arr, $data['att_status']);

            // inactive current history
            // ------------------------
            $this->db_att_repo->dbUpdateAttRemarkHistory($id, $arr['uid']);

            // insert new history
            // ------------------
            $arr_hist = array($id, $arr['uid'], 3, $data['att_notes']);
            $this->db_att_repo->dbInsertAttRemarkHistory($arr_hist);

            // get site supervisor info
            // ------------------------
            $chk_user = $this->user_repo->getUserByID($arr['uid']);
            if (!empty($chk_user)) {

                // get attendance info
                // -------------------
                $att = $this->getAttInOut($arr['icno'], $arr['sitecode'], $arr['date']);

                // get remark info
                // ---------------
                $att_remark = $this->getRemarkInfo($id);

                // email body info
                // ---------------
                $arr_body = array(
                    'att_remark'        => $att_remark,
                    'att_remark_status' => $att_remark->AttStatusName->name,
                    'remark'            => $remark,
                    'other_remark'      => $other_remark->name,
                    'in'                => $att['in'],
                    'out'               => $att['out']
                );

                // set array info
                // --------------
                $arr = array(
                    'mgr_name'  => $chk_user->name,
                    'mgr_email' => $chk_user->email,
                    'status'    => 'Updated'
                );

                // send email notification
                // -----------------------
                $this->email_repo->getEmailAttRemarkResult($arr_body, $arr);

                // return success
                // --------------
                $msg = array('Remark successfully Updated.', 'success');
            }

            // user is empty
            // -------------
            else {
                $msg = array('Staff not Exist.', 'danger');
            }
        }

        // remark exist
        // ------------
        else {
            $msg = array('Remark already Exist.', 'danger');
        }
        return $msg;
    }

    // get all attendance manual
    // -------------------------
    public function getAttManualAll($arr, $codes)
    {
        $site_id = $arr['site_id'];
        $region_id = $arr['region_id'];
        $phase_id = $arr['phase_id'];
        $keyword = $arr['keyword'];
        $jobs = array('region_id' => $region_id, 'phase_id' => $phase_id);

        // query attendance manual
        // -----------------------
        $q = \IhrV2\Models\AttendanceManual::with(array('UserInfo' =>
            function ($h) use ($jobs) {
                $h->with(array('UserCurrentJob2' => function ($p) {
                    $p->with(array('PhaseName', 'PositionName'));
                }));
            }))
        ->with(array(
            'UserSite',
            'UserActionBy'
        ))

            // search by job
            // -------------
        ->whereHas('UserJob', function ($l) use ($jobs) {
            foreach ($jobs as $column => $key) {
                if (!is_null($key)) {
                    $l->where($column, $key);
                }
            }
        })

            // filter by region
            // ----------------
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('sitecode', 'like', $code . '%');
                }
            }
        })

            // search by site
            // --------------
        ->where(function ($s) use ($site_id) {
            if (!empty($site_id)) {
                $s->where('sitecode', $site_id);
            }
        })
        ->whereHas('UserInfo', function ($i) use ($keyword) {
            if (!empty($keyword)) {
                $i->where('name', 'like', '%' . $keyword . '%');
                $i->orWhere('icno', 'like', '%' . $keyword . '%');
                $i->orWhere('username', 'like', '%' . $keyword . '%');
                $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
            }
        })
        ->where('active', 1)
        ->orderBy('id', 'desc')
        ->paginate(20);
        return $q;
    }

    public function getAttendanceType($type)
    {
        return ($type == 0) ? 'IN' : 'OUT';
    }

    // get max/min attendance
    // ----------------------
    public function getAttendanceLatest($icno, $sitecode, $date, $type)
    {
        // query attendance
        // ----------------
        $order = ($type == 0) ? 'asc' : 'desc';
        $q = \IhrV2\Models\Attendance::whereDate('att_log_time', '=', $date)
        ->selectRaw("att_log_time, att_log_id")
        ->orderBy('att_log_time', $order)
        ->where(['att_mykad_ic' => $icno, 'location_id' => $sitecode, 'att_record_type' => $type])
        ->first();
        return $q;
    }

    // get attendance daily
    // --------------------
    public function getAttendanceDaily($date, $codes)
    {
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', 'att_log_time', 'att_record_type')
        ->selectRaw("MIN(att_log_time) AS att_in, MAX(att_log_time) AS att_out")
        ->whereDate('att_log_time', '=', $date)
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('location_id', 'like', $code . '%');
                }
            }
        })
        ->groupBy('att_mykad_ic', 'location_id', \DB::raw('DATE(att_log_time)'), 'att_record_type')
        ->orderBy('att_log_id', 'desc')
            // ->paginate(20);
        ->get()
        ->toArray();
        return $q;
    }

    // get one attendance manual
    // -------------------------
    public function getAttendanceManualOne($id, $icno, $sitecode)
    {
        $q = \IhrV2\Models\AttendanceManual::where(
            [
                'id'       => $id,
                'icno'     => $icno,
                'sitecode' => $sitecode,
                'active'   => 1
            ]
        )
        ->with(array('UserDetail', 'UserActionBy', 'AttendanceDetail', 'AttManualAttachment'))
        ->first();
        return $q;
    }

    // get attendance manual by id
    // ---------------------------
    public function getAttendanceManualbyID($id)
    {
        return \IhrV2\Models\AttendanceManual::find($id);
    }

    // add attendance manual
    // ---------------------
    public function getAttendanceManualAdd($data, $uid, $user)
    {

        // save attendance
        // ---------------
        $data['status'] = \Request::ip() . ', ' . $data['icno'] . ', IHRV2, Manual';
        $att_id = $this->db_att_repo->dbInsertAttendance($data);

        // save attendance manual (to keep track who keyin and date)
        // ---------------------------------------------------------
        if ($att_id) {
            $data['user_id'] = $user->id;
            $id = $this->db_att_repo->dbInsertAttendanceManual($att_id, $data, $uid);
        }

        // query rm info
        // -------------
        $rm = $this->user_repo->getRegionManager($user->sitecode);
        if (!empty($rm)) {

            // check the attachment
            // --------------------
            $attach = array();
            if (!empty($data['att_file'])) {

                // upload file attachment
                // ----------------------
                $attach = $this->upload_repo->uploadAttManual($data['att_file'], $id, $uid);
            }

            // set rm info into array
            // ----------------------
            $arr = array(
                'mgr_name' => $user->name,
                'mgr_email' => $user->email,
                'sitecode' => $user->sitecode,
                'rm_email' => $rm->RegionName->RegionManager->RegionManagerDetail->email,
                'rm_name'  => $rm->RegionName->RegionManager->RegionManagerDetail->name,
                'attach'   => $attach
            );

            // email body info
            // ---------------
            $arr_body = array(
                'user'     => $user,
                'position' => $user->UserLatestJob->PositionName->name,
                'site'     => $user->SiteNameInfo,
                'manual'   => $this->getAttendanceManualByID($id),
                'created'  => $this->user_repo->getUserByID($uid),
                'notes'    => $data['remark']
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailAttManualCreate($arr_body, $arr);

            // set success message
            // -------------------
            $msg = array('Attendance Manual successfully Added and Send to Site Supervisor and RM.', 'success');
        }

        // no rm record
        // ------------
        else {
            $msg = array('Attendance Manual successfully Added but fail to Send RM.', 'success');
        }

        // return success
        // --------------
        return $msg;
    }

    // get all states
    // exclude: perlis|penang|kl|labuan|putrajaya
    // ------------------------------------------
    public function getAllState()
    {
        $q = \IhrV2\Models\State::whereNotIn('code', array('508', '509', '514', '515', '516'))->get();
        return $q;
    }

    // get daily attendance of all sites
    // ---------------------------------
    public function getAttendanceDailyAll()
    {

        // get today date
        // --------------
        // $date = Carbon::now()->format('Y-m-d');
        $date = '2017-10-02';

        // display all states
        // ------------------
        $states = $this->getAllState();

        return array(
            'states' => $states
        );
    }

    // get total time of current month & year
    // --------------------------------------
    public function getAttTotalTime($arr)
    {
        // initial variables
        // -----------------
        $uid = $arr[0];
        $icno = $arr[1];
        $sitecode = $arr[2];
        $month = $arr[3]['month'];
        $year = $arr[3]['year'];
        $position_id = $arr[4];

        // set variables
        // -------------
        $empty = 0;

        // query attendance
        // ----------------
        $cond = array($icno, $sitecode, $year, $month);
        $att = $this->getAttUserPerMonth($cond);
        if (empty($att)) {
            $empty = 1;
        }

        // get only all dates
        // ------------------
        $dates = [];
        if ($empty != 1) {
            foreach ($att as $a) {
                $dates[] = Carbon::parse($a->att_date)->format('Y-m-d');
            }
        }

        // get year month
        // --------------
        $year_month = date($year . '-' . $month);

        // get all dates in current month
        // ------------------------------
        $all_dates = $this->leave_repo->getAllDatesInMonth($year_month);

        // get contract
        // ------------
        $contract = $this->user_repo->getContractByIDSitecode($uid, $sitecode);

        // get limit of current month
        // --------------------------
        $get_limit = $this->getAttLimitRange($uid, $sitecode, $all_dates, $year, $contract->id);

        // set array into a single array (remove key but remain value)
        // -----------------------------------------------------------
        $arr_limit = (array_collapse($get_limit));

        // set variables for hour minute and second
        // ----------------------------------------
        $hours = null;
        $minutes = null;
        $seconds = null;
        $mcmc_hours = null;
        $mcmc_minutes = null;
        $mcmc_seconds = null;

        // loop all dates in month
        // -----------------------
        foreach ($all_dates as $x => $y) {

            // initial in and out
            // ------------------
            $limit = array();

            // check if date and date limit found
            // ----------------------------------
            if (array_key_exists($y, $arr_limit)) {

                // set array values
                // ----------------
                $limit['time_in'] = $arr_limit[$y]['time_in'];
                $limit['time_out'] = $arr_limit[$y]['time_out'];
            }

            // date of month
            // -------------
            $d = Carbon::parse($y)->format('Y-m-d');
            $start = null;
            $end = null;
            $in = 0;
            $out = 0;

            // have attendance record
            // ----------------------
            if ($empty != 1) {

                // attendance have in a date
                // -------------------------
                if (in_array($d, $dates)) {

                    // check in and out
                    // ----------------
                    foreach ($att as $a) {
                        if (Carbon::parse($a->att_date)->format('Y-m-d') == $d) {

                            // get time in
                            // -----------
                            if ($a->att_record_type == 0) {
                                $in = 1;
                                $start = $a->att_in;
                            }

                            // get time out
                            // ------------
                            if ($a->att_record_type == 1) {
                                $out = 1;
                                $end = $a->att_out;
                            }
                        }
                    }
                }
            }

            if ($in == 1 && $out == 1) {
                if ($start < $end) {

                    // get total time
                    // --------------
                    $get = AttendanceHelper::getCompareDateTime($start, $end, $limit);
                    $hours += $get['hour'];
                    $minutes += $get['minute'];
                    $seconds += $get['second'];

                    // get total mcmc
                    // --------------
                    $mcmc = AttendanceHelper::getCompareMcmc($start, $end, $limit);
                    $mcmc_hours += $mcmc['hour'];
                    $mcmc_minutes += $mcmc['minute'];
                    $mcmc_seconds += $mcmc['second'];
                }
            }
        }

        // return total time
        // -----------------
        $total_all = AttendanceHelper::getTotalHours($hours, $minutes, $seconds);
        $total_mcmc = AttendanceHelper::getTotalHours($mcmc_hours, $mcmc_minutes, $mcmc_seconds);
        return array('all' => $total_all, 'mcmc' => $total_mcmc);
    }

    // get attendance summary
    // ----------------------
    public function getChkAttSummary($date)
    {
        // get year from date
        // ------------------
        $year = Carbon::parse($date)->format('Y');

        // get active staff
        // ----------------
        $users = array();
        $chk_users = $this->getStaffActiveAll();
        if (!empty($chk_users)) {
            foreach ($chk_users as $i) {

                // get staff site name
                // -------------------
                if (!empty($i->SiteNameInfo)) {
                    $site_name = $i->SiteNameInfo->name;
                } else {
                    $site_name = 'Empty';
                }
                $users[] = array(
                    'uid'         => $i->id,
                    'name'        => $i->name,
                    'icno'        => $i->icno,
                    'position'    => $i->UserLatestJob->PositionName->name,
                    'position_id' => $i->UserLatestJob->position_id,
                    'sitecode'    => $i->sitecode,
                    'sitename'    => $site_name
                );
            }
        }

        // get regions
        // -----------
        $regions = $this->getAllRegionUser()->toArray();

        // regions exist
        // -------------
        if (!empty($regions)) {
            foreach ($regions as $re) {
                $total = 0;
                $staff = array();
                $total_off_day = 0;
                $total_public = 0;
                $total_leave = 0;

                // get drm region
                // --------------
                $drm = array();
                $cond = array('region_id' => $re['region_id'], 'position_id' => 2, 'status' => 1);
                $chk_drm = $this->getDrmRegionList($cond);
                if (!empty($chk_drm)) {
                    foreach ($chk_drm->toArray() as $d) {
                        $drm[$d['email']] = $d['name'];
                    }
                }

                // get clerk for specific region
                // -----------------------------
                $clerk = array();
                if ($re['region_id'] == 2) {
                    $clerk['anis@msd.net.my'] = 'Nurul Anis Azimi';
                }
                if ($re['region_id'] == 3) {
                    $clerk['nurul.izzati@msd.net.my'] = 'Nurul \'Izzati Abd Shukor';
                }
                if ($re['region_id'] == 8) {
                    $clerk['niswah@msd.net.my'] = 'Niswah Hasinah Ibrahim';
                }

                // get sitecodes
                // -------------
                $codes = explode(':', $re['codes']);
                foreach ($users as $user) {
                    $remark = null;

                    // get 1st character at sitecode
                    // -----------------------------
                    $code = substr($user['sitecode'], 0, 1);
                    if (in_array($code, $codes)) {
                        $total++;

                        // get site info
                        // -------------
                        $site = $this->leave_repo->getUserStateID($user['sitecode'])->toArray();

                        // get fasting date
                        // ----------------
                        $fasting = $this->getFastingDate($site['state_id'], $year);
                        $cond['fasting'] = $fasting;

                        // get type of off day
                        // -------------------
                        $types = $this->leave_repo->getOffDayTypeIDByPosition($user['position_id']);

                        // get off day
                        // -----------
                        $chk_off = $this->getAttendanceOffDayDate($site['state_id'], $types, $date);
                        if (!empty($chk_off)) {
                            $total_off_day++;
                            $remark = 'Off Day';
                        }

                        // get public holiday
                        // ------------------
                        $chk_public = $this->getAttendancePublicDate($site['state_id'], $date);
                        if (!empty($chk_public)) {
                            $total_public++;
                            $remark = 'Public Holiday';
                        }

                        // get leave date
                        // --------------
                        $chk_leave = $this->getAttendanceLeaveOne($user['uid'], $user['sitecode'], $date);
                        if (!empty($chk_leave)) {
                            if (!empty($chk_leave->LeaveApproveOne)) {
                                $total_leave++;
                                $remark = $chk_leave->LeaveApproveOne->LeaveTypeInfo->name;
                            }
                        }

                        // get time limit
                        // --------------
                        $limit = $this->getAttendanceLimitOne($date, $re['region_id'], $cond);

                        // get attendance
                        // --------------
                        $get = $this->getAttendanceByDate($user['icno'], $user['sitecode'], $date);

                        // get time in and out
                        // -------------------
                        $att = $this->getAttendanceTimeInOut($get, $limit);

                        // get attendance
                        // --------------
                        // $att = $this->getAttendanceByDate($user['icno'], $user['sitecode'], $date);

                        // set staff info
                        // --------------
                        $staff[] = array(
                            'name'     => $user['name'],
                            'icno'     => $user['icno'],
                            'position' => $user['position'],
                            'sitecode' => $user['sitecode'],
                            'sitename' => $user['sitename'],
                            'att'      => $att,
                            'remark'   => $remark
                        );
                    }
                }

                // have records
                // ------------
                if ($total > 0) {

                    // get region name
                    // ---------------
                    $region = $re['region_name']['name_eng'];

                    // get rm info
                    // -----------
                    $rm_name = $re['region_manager_detail']['name'];
                    $rm_email = $re['region_manager_detail']['email'];

                    // set email content
                    // -----------------
                    $arr = array(
                        'region'   => $region,
                        'date'     => $date,
                        'rm_name'  => $rm_name,
                        'rm_email' => $rm_email,
                        'drm'      => $drm,
                        'clerk'    => $clerk
                    );

                    // set email body
                    // --------------
                    $arr_body = array(
                        'region'        => $region,
                        'date'          => Carbon::parse($date)->format('j M Y'),
                        'total'         => $total,
                        'staff'         => $staff,
                        'total_off_day' => $total_off_day,
                        'total_public'  => $total_public,
                        'total_leave'   => $total_leave
                    );

                    // send email notification
                    // -----------------------
                    $this->email_repo->getEmailAttSummary($arr_body, $arr);
                }
                // dd('end');
            }
        }

        // return result
        // -------------
        $data = array(
            'date' => $date,
            'msg'  => 'Attendance Summary successfully send to RM.'
        );
        return $data;
    }

    // get region user
    // ---------------
    public function getAllRegionUser()
    {
        $q = \IhrV2\Models\RegionUser::with(array('RegionManagerDetail', 'RegionName'))
        ->get();
        return $q;
    }

    public function getRegionUser()
    {
        $q = \IhrV2\Models\RegionUser::get();
        return $q;
    }

    // get staff active all
    // -------------------
    public function getStaffActiveAll()
    {
        $q = \IhrV2\User::whereHas(
            'UserLatestJob',
            function ($j) {
                $j->whereIn('position_id', array(4, 5, 6, 84));
            }
        )->where('sitecode', 'NOT LIKE', '%L0%') // exclude site PJL
        ->where([
         'group_id' => 3,
         'status'   => 1
     ])
        ->with(array('SiteNameInfo'))
        ->with(array('UserLatestJob' => function ($p) {
            $p->with('PositionName');
        }))
        ->orderBy('id', 'asc')
        ->get();
        return $q;
    }

    // get all sitecodes of each region
    // --------------------------------
    public function getChkAttRegion($date, $type, $minute)
    {

        // get time start
        // --------------
        $chk_start = Carbon::createFromFormat('Y-m-d', $date)->subMinutes($minute)->toDateTimeString();
        $start = Carbon::parse($chk_start)->format('Y-m-d H:i') . ':00';

        // get time end
        // ------------
        $end = date('Y-m-d H:i') . ':00';
        $time = array($start, $end);

        // get all region
        // --------------
        $regions = $this->getAllRegionUser()->toArray();

        // regions exist
        // -------------
        if (!empty($regions)) {
            foreach ($regions as $re) {

                // get drm region
                // --------------
                $drm = array();
                $cond = array('region_id' => $re['region_id'], 'position_id' => 2, 'status' => 1);
                $chk_drm = $this->getDrmRegionList($cond);
                if (!empty($chk_drm)) {
                    foreach ($chk_drm->toArray() as $d) {
                        $drm[$d['email']] = $d['name'];
                    }
                }

                // get clerk for specific region
                // -----------------------------
                $clerk = array();
                if ($re['region_id'] == 2) {
                    $clerk['anis@msd.net.my'] = 'Nurul Anis Azimi';
                }
                if ($re['region_id'] == 3) {
                    $clerk['nurul.izzati@msd.net.my'] = 'Nurul \'Izzati Abd Shukor';
                }
                if ($re['region_id'] == 8) {
                    $clerk['niswah@msd.net.my'] = 'Niswah Hasinah Ibrahim';
                }

                // get sitecodes
                // -------------
                $codes = explode(':', $re['codes']);

                // get attendance
                // --------------
                $data = $this->getAttDateByMinute($date, $type, $time, $codes);

                // have record
                // -----------
                if ($data['total'] > 0) {

                    // get rm info
                    // -----------
                    $rm_name = $re['region_manager_detail']['name'];
                    $rm_email = $re['region_manager_detail']['email'];

                    // get region name
                    // ---------------
                    $data['region'] = $re['region_name']['name_eng'];

                    // set email sender
                    // ----------------
                    $arr = array(
                        'rm_name'  => $rm_name,
                        'rm_email' => $rm_email,
                        'drm'      => $drm,
                        'clerk'    => $clerk
                    );

                    // send email notification
                    // -----------------------
                    $this->email_repo->getEmailAttPunch($data, $arr);
                }
            }
        }

        // return record
        // -------------
        return array(
            'date'  => $date,
            'type'  => ($type == 0) ? 'In' : 'Out',
            'start' => $start,
            'end'   => $end
        );
    }

    // get attendance between minutes
    // ------------------------------
    public function getAttDateByMinute($date, $type, $time, $codes = null)
    {

        // get start and end time
        // ----------------------
        $start = $time[0];
        $end = $time[1];

        // set array
        // ---------
        $arr = array($date, $type, $start, $end, $codes);

        // get attendance
        // --------------
        $att = \IhrV2\Models\Attendance::whereDate('att_log_time', '=', $date)
        ->where(function ($s) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $s->orWhere('location_id', 'like', $code . '%');
                }
            }
        })
        ->where([
                'att_record_type' => $type // 0 = in | 1 = out
            ])
        ->whereBetween('att_log_time', [$start, $end])
        ->orWhere(function ($t) use ($arr) {
            $cod = $arr[4];
            $t->whereDate('att_log_time', '=', $arr[0]);
            $t->where('att_record_type', $arr[1]);
            $t->whereBetween('att_timestamp', [$arr[2], $arr[3]]);
            $t->where(function ($s2) use ($cod) {
                if (count($cod) > 0) {
                    foreach ($cod as $code) {
                        $s2->orWhere('location_id', 'like', $code . '%');
                    }
                }
            });
        })
        ->with(array('UserAttendanceDetail', 'UserAttendanceSite'))
        ->orderBy('att_log_time', 'asc')
        ->get()
        ->toArray();

        // return record
        // -------------
        return array(
            'date'  => $date,
            'type'  => ($type == 0) ? 'In' : 'Out',
            'start' => Carbon::parse($start)->format('g:i:s a'),
            'end'   => Carbon::parse($end)->format('g:i:s a'),
            'total' => count($att),
            'att'   => $att
        );
    }

    // get lists of time slip
    // ----------------------
    public function getTimeSlipIndex($request = null)
    {
        $header = array(
            'parent'  => 'Attendance',
            'child'   => 'Time Slip',
            'icon'    => 'doc',
            'title'   => 'Time Slip'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $att_date = CommonHelper::checkSession('phase_id');
            $status_id = CommonHelper::checkSession('phase_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('site_id', 'region_id', 'phase_id', 'att_date', 'status_id', 'keyword'));
                return redirect()->route('mod.attendance.timeslip');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $att_date = CommonHelper::checkSessionPost($request->att_date, $reset, 'att_date');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get lists of regions
        // --------------------
        $regions = $this->user_repo->getRegionList();

        // get lists of phases
        // -------------------
        $phases = $this->user_repo->getPhaseList();

        // get lists of status
        // -------------------
        $status = array('' => '[Status]', 1 => 'ACTIVE', 2 => 'INACTIVE');

        // set sessions
        // ------------
        $sessions = array(
            'site_id'   => $site_id,
            'region_id' => $region_id,
            'phase_id'  => $phase_id,
            'att_date'  => $att_date,
            'status_id' => $status_id,
            'keyword'   => $keyword
        );

        // query timeslip
        // --------------
        $timeslip = $this->getAttTimeSlip($sessions, $codes);

        // query sites
        // -----------
        return View('modules.attendance.timeslip.index', compact('header', 'sites', 'regions', 'phases', 'status', 'sessions', 'timeslip'));
    }

    // get attendance time slip
    // ------------------------
    public function getAttTimeSlip($arr, $codes)
    {
        $site_id = $arr['site_id'];
        $region_id = $arr['region_id'];
        $phase_id = $arr['phase_id'];
        $date = $arr['att_date'];
        $status_id = $arr['status_id'];
        $keyword = $arr['keyword'];
        $jobs = array('region_id' => $region_id, 'phase_id' => $phase_id);

        // query time slip
        // ---------------
        $q = \IhrV2\Models\AttendanceRemarkOther::with(array('AttSiteInfo', 'AttUserInfo'))
        ->whereHas('AttUserJob', function ($j) use ($jobs) {
            foreach ($jobs as $column => $key) {
                if (!is_null($key)) {
                    $j->where($column, $key);
                }
            }
        })
        ->with(array('AttUserJob' => function ($i) {
            $i->with(array('PositionName', 'PhaseName', 'RegionName'));
        }))

            // search by site
            // --------------
        ->where(function ($l) use ($site_id) {
            if (!empty($site_id)) {
                $l->where('code', $site_id);
            }
        })

            // search by attendance date
            // -------------------------
        ->where(function ($d) use ($date) {
            if (!empty($date)) {
                $d->whereDate('date', '=', $date);
            }
        })

            // search by status
            // ----------------
        ->where(function ($s) use ($status_id) {
            if (!empty($status_id)) {
                $d->whereDate('active', $status_id);
            }
        })

            // search by keyword
            // -----------------
        ->whereHas('AttUserInfo', function ($i) use ($keyword) {
            if (!empty($keyword)) {
                $i->where('name', 'like', '%' . $keyword . '%');
                $i->orWhere('icno', 'like', '%' . $keyword . '%');
                $i->orWhere('username', 'like', '%' . $keyword . '%');
                $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
            }
        })
        ->orderBy('id', 'desc')
        ->paginate(20);
        return $q;
    }

    // get time slip info
    // ------------------
    public function getAttTimeSlipInfo($id, $uid, $sitecode)
    {
        return \IhrV2\Models\AttendanceRemarkOther::where(
            [
                'id'       => $id,
                'user_id'  => $uid,
                'sitecode' => $sitecode
            ]
        )
        ->with(array('AttUserInfo', 'AttUserJob', 'AttSiteInfo', 'AttAttachment'))
        ->first();
    }

    // get time slip by id
    // -------------------
    public function getAttTimeSlipByID($id)
    {
        return \IhrV2\Models\AttendanceRemarkOther::find($id);
    }

    // cancel time slip
    // ----------------
    public function getTimeSlipCancel($id, $uid, $sitecode)
    {
        // cancel time slip
        // ----------------
        $cancel = $this->db_att_repo->dbUpdateAttendanceOther($id, $uid, $sitecode);

        // get time slip info
        // ------------------
        $timeslip = $this->getAttTimeSlipByID($id);

        // email header
        // ------------
        $arr = array(
            'mgr_name' => $timeslip->AttUserInfo->name,
            'mgr_email' => $timeslip->AttUserInfo->email,
        );

        // get email body
        // --------------
        $arr_body = array(
            'timeslip' => $timeslip
        );

        // email notification
        // ------------------
        $this->email_repo->getEmailTimeSlipCancel($arr_body, $arr);

        // return message
        // --------------
        $msg = array('Time Slip successfully Cancel.', 'success');
        return $msg;
    }

    // cancel attendance manual
    // ------------------------
    public function getAttManualCancel($data)
    {
        // check if cancel already exist
        // -----------------------------
        $check = $this->getAttManualCheck($data['id'], 2);

        // no record yet
        // -------------
        if (empty($check)) {

            // cancel the attendance manual
            // ----------------------------
            $this->db_att_repo->dbUpdateAttendanceManual($data['id']);

            // get attendance manual
            // ---------------------
            $manual = $this->getAttendanceManualbyID($data['id']);

            // remove record from attendance_log_records
            // -----------------------------------------
            $this->db_att_repo->dbDeleteAttendanceLogRecord($manual);

            // email header
            // ------------
            $arr = array(
                'mgr_name' => $manual->UserInfo->name,
                'mgr_email' => $manual->UserInfo->email,
                'sitecode' => $manual->sitecode
            );

            // email content
            // -------------
            $arr_body = array(
                'manual' => $manual
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailAttManualCancel($arr_body, $arr);

            // return message
            // --------------
            $msg = array('success', 'Attendance Manual successfully Cancel.');
        }

        // cancel already exist
        // --------------------
        else {
            $msg = array('danger', 'Attendance Manual already Cancel.');
        }
        return $msg;
    }

    // check attendance manual status
    // ------------------------------
    public function getAttManualCheck($id, $status)
    {
        $q = \IhrV2\Models\AttendanceManual::where(['id' => $id, 'active' => $status])->first();
        return $q;
    }

    // lists attendance mobile
    // -----------------------
    public function getAttMobileIndex($request = null)
    {
        $header = array(
            'parent' => 'Attendance',
            'child'  => 'Mobile',
            'icon'   => 'screen-smartphone',
            'title'  => 'Mobile'
        );

        // check request
        // -------------
        if (empty($request)) {
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $att_date = CommonHelper::checkSession('att_date');
            $punch_id = CommonHelper::checkSession('punch_id');
            $type_id = CommonHelper::checkSession('type_id');
            $keyword = CommonHelper::checkSession('keyword');
        } else {
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('site_id', 'region_id', 'phase_id', 'att_date', 'punch_id', 'type_id', 'keyword'));
                return redirect()->route('mod.attendance.mobile');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $att_date = CommonHelper::checkSessionPost($request->att_date, $reset, 'att_date');
            $punch_id = CommonHelper::checkSessionPost($request->punch_id, $reset, 'punch_id');
            $type_id = CommonHelper::checkSessionPost($request->type_id, $reset, 'type_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get lists of regions
        // --------------------
        $regions = $this->user_repo->getRegionList();

        // get lists of phases
        // -------------------
        $phases = $this->user_repo->getPhaseList();

        // get types of punch
        // ------------------
        $punch = array('' => '[Punch]', 1 => 'IN', 2 => 'OUT');

        // get types of mobile attendance
        // ------------------------------
        $types = array('' => '[Type]', 1 => 'QRCODE', 2 => 'GPS');

        // set sessions
        // ------------
        $sessions = array(
            'site_id'   => $site_id,
            'region_id' => $region_id,
            'phase_id'  => $phase_id,
            'att_date'  => $att_date,
            'punch_id'  => $punch_id,
            'type_id'  => $type_id,
            'keyword'   => $keyword
        );

        // query attendance mobile
        // -----------------------
        $att_gps = $this->getAttMobileList($sessions, $codes);
        return View('modules.attendance.mobile.index', compact('header', 'sites', 'regions', 'phases', 'punch', 'types', 'sessions', 'att_gps'));
    }

    // get lists of attendance mobile
    // ------------------------------
    public function getAttMobileList($arr, $codes)
    {
        // get value from input
        // --------------------
        $site_id = $arr['site_id'];
        $att_date = $arr['att_date'];
        $punch_id = $arr['punch_id'];
        $type_id = $arr['type_id'];
        $keyword = $arr['keyword'];
        $jobs = array('region_id' => $arr['region_id'], 'phase_id' => $arr['phase_id']);

        // query attendance remark
        // -----------------------
        $q = \IhrV2\Models\AttendanceGps::orderBy('id', 'desc')
        ->with(array('AttGpsSite'))
        ->with(array('AttGpsUserJob' => function ($i) {
            $i->with(array('PositionName', 'PhaseName', 'RegionName'));
        }))
        ->whereHas('AttGpsUserJob', function ($j) use ($jobs) {
            foreach ($jobs as $column => $key) {
                if (!is_null($key)) {
                    $j->where($column, $key);
                }
            }
        })
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('sitecode', 'like', $code . '%');
                }
            }
        })
        ->where(function ($s) use ($site_id) {
            if (!empty($site_id)) {
                $s->where('sitecode', $site_id);
            }
        })
        ->whereHas('AttGpsUser', function ($i) use ($keyword) {
            if (!empty($keyword)) {
                $i->where('name', 'like', '%' . $keyword . '%');
                $i->orWhere('icno', 'like', '%' . $keyword . '%');
                $i->orWhere('username', 'like', '%' . $keyword . '%');
                $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
            }
        })
        ->where(function ($p) use ($punch_id) {
            if (!empty($punch_id)) {
                if ($punch_id == 1) {
                    $val = 0;
                } else {
                    $val = 1;
                }
                $p->where('in_out', $val);
            }
        })
        ->where(function ($d) use ($att_date) {
            if (!empty($att_date)) {
                $d->whereDate('datetime', '=', $att_date);
            }
        })
        ->where(function ($t) use ($type_id) {
            if (!empty($type_id)) {
                if ($type_id == 1) {
                    $t->where('qr_code', '!=', null);
                } else {
                    $t->where('qr_code', '=', null);
                }
            }
        })
        ->whereNotIn('sitecode', array('X01C001'))
        ->where('status', 1)
        ->paginate(20);
        return $q;
    }

    // get attendance mobile by user info
    // ----------------------------------
    public function getAttMobileByUser($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\AttendanceGps::where(['id' => $id, 'user_id' => $uid, 'sitecode' => $sitecode])->first();
        return $q;
    }

    // get attendance mobile by date (in & out)
    // ----------------------------------------
    public function getAttMobileByDate($uid, $sitecode, $date)
    {
        $q = \IhrV2\Models\AttendanceGps::where(['user_id' => $uid, 'sitecode' => $sitecode, 'status' => 1])
        ->whereDate('datetime', '=', $date)
        ->get();
        return $q;
    }

    // get one mobile attendance
    // -------------------------
    public function getAttMobileOne($id)
    {
        $q = \IhrV2\Models\AttendanceGps::find($id);
        return $q;
    }

    // get attendance mobile that active
    // ---------------------------------
    public function getAttMobileActive($id)
    {
        $q = \IhrV2\Models\AttendanceGps::where(['id' => $id, 'status' => 1])->first();
        return $q;
    }

    // cancel attendance mobile
    // ------------------------
    public function getAttMobileCancel($i)
    {
        // check attendance mobile
        // -----------------------
        $mobile = $this->getAttMobileActive($i['id']);

        // record exist
        // ------------
        if (!empty($mobile)) {

            // set inactive attendance gps
            // ---------------------------
            $this->db_att_repo->dbUpdateAttGps($mobile->id);

            // remove attendance log records
            // -----------------------------
            $this->db_att_repo->dbDeleteAttLog($mobile->att_log_id);

            // email body info
            // ---------------
            $arr_body = array(
                'mobile' => $mobile,
                'updated_by' => $this->user_repo->getUserByID(auth()->user()->id),
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailAttMobileCancel($arr_body);

            // set message
            // -----------
            $data = array('Record successfully Cancel.', 'success');
        }

        // record not exist
        // ----------------
        else {
            $data = array('Record not found.', 'danger');
        }

        // return result
        // -------------
        return $data;
    }

    // check attendance record by mobile
    // ---------------------------------
    public function getAttGpsPunch($i)
    {
        $q = \IhrV2\Models\Attendance::where([
            'att_mykad_ic' => $i['icno'],
            'location_id' => $i['sitecode'],
            'att_record_type' => $i['type_id']
        ])
        ->whereDate('att_log_time', '=', $i['date'])
        ->whereNotIn('location_id', array('X01C001'))
        ->count();
        return $q;
    }

    // get status flag
    // ---------------
    public function getAttGpsFlag($id)
    {
        return \IhrV2\Models\AttendanceGpsStatus::find($id);
    }

    // get attendance mobile by user_id and date
    // -----------------------------------------
    public function getAttGpsCheck($user_id, $date)
    {
        $q = \IhrV2\Models\AttendanceGps::whereDate('datetime', '=', $date)
        ->where(['user_id' => $user_id, 'status' => 1])
        ->get();
        return $q;
    }

    // get lists of version attendance mobile
    // --------------------------------------
    public function getListAppVersion()
    {
        return \IhrV2\Models\AttendanceApp::orderBy('id', 'desc')->get();
    }


    // save new version of attendance mobile
    // -------------------------------------
    public function getSaveAppVersion($data)
    {
        // inactive current version
        // ------------------------
        $this->db_att_repo->dbInactiveAppVersion();

        // save new version
        // ----------------
        $this->db_att_repo->dbInsertAppVersion($data);
        return array('App Version successfully Added.', 'success');
    }

    // get app version by id
    // ---------------------
    public function getAppVersionByID($id)
    {
        return \IhrV2\Models\AttendanceApp::find($id);
    }

    // update app version
    // ------------------
    public function getUpdateAppVersion($data, $id)
    {
        $this->db_att_repo->dbUpdateAppVersion($data, $id);
        return array('App Version successfully Updated.', 'success');
    }
}
