<?php namespace IhrV2\Repositories\Api\Attendance;

use Carbon\Carbon;
use IhrV2\Contracts\Api\Attendance\DbApiAttendanceInterface;

class DbApiAttendanceRepository implements DbApiAttendanceInterface {

    // insert attendance gps
    // ---------------------
    public function dbInsertAtt($data) {
        $arr = array(
            'att_mykad_ic'    => strtoupper($data['icno']),
            'location_id'     => strtoupper($data['sitecode']),
            'att_record_type' => $data['type_id'],
            'att_log_time'    => Carbon::parse($data['datetime'])->format('Y-m-d H:i:s'),
            'att_userstamp'   => $data['remark'],
            'ip_add'          => \Request::ip(),
            'att_sys_status'  => $data['status']
        );
        $sav = new \IhrV2\Models\Attendance($arr);
        $sav->save();
        return $sav->id;
    }

    // insert attendance_gps by api
    // ----------------------------
    public function dbInsertAttGps($data, $id) {
        $arr = array(
            'att_log_id' => $id,
            'user_id'    => $data['user_id'],
            'sitecode'   => $data['sitecode'],
            'latitude'   => $data['latitude'],
            'longitude'  => $data['longitude'],
            'qr_code'    => $data['qr_code'],
            'datetime'   => Carbon::parse($data['datetime'])->format('Y-m-d H:i:s'),
            'in_out'     => $data['type_id'],
            'location'   => $data['location'],
            'remark'     => $data['remark'],
            'flag'       => $data['flag'],
            'imei'       => ($data['imei']) ? $data['imei'] : null,
            'version'    => (!empty($data['version'])) ? $data['version'] : null,
            'build_no'   => (!empty($data['build_no'])) ? $data['build_no'] : null,
            'model'      => (!empty($data['model'])) ? $data['model'] : null,
            'transfer'   => $data['transfer'],
            'accuracy'   => (!empty($data['accuracy'])) ? $data['accuracy'] : null,
            'ip_address' => (!empty($data['ip_address'])) ? $data['ip_address'] : null,
            'status'     => 1 // active
        );
        $sav = new \IhrV2\Models\AttendanceGps($arr);
        $sav->save();
        return true;
    }
    
    // insert attendance_gps_logs
    // --------------------------
    public function dbInsertAttGpsLog($data, $uid, $sitecode)
    {
        $arr = array(
            'user_id'      => $uid,
            'sitecode'     => $sitecode,
            'filename'     => $data['filename'],
            'ext'          => strtolower($data['fileext']),
            'size'         => $data['filesize'],
            'status'       => 1
        );
        $sav = new \IhrV2\Models\AttendanceGpsLog($arr);
        $sav->save();
        return true;
    }


}




