<?php
namespace IhrV2\Repositories\Api\Attendance;

use IhrV2\Contracts\Api\Attendance\ApiAttendanceInterface;
use IhrV2\Contracts\Api\Attendance\DbApiAttendanceInterface;
use IhrV2\Contracts\Attendance\AttendanceInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\Email\EmailInterface;
use Carbon\Carbon;

class ApiAttendanceRepository implements ApiAttendanceInterface {

    protected $api_att_db;
    protected $att;
    protected $user;
    protected $upload;
    protected $email;

    public function __construct(DbApiAttendanceInterface $api_att_db, AttendanceInterface $att, UserInterface $user, UploadInterface $upload, EmailInterface $email) {
        $this->api_att_db = $api_att_db;
        $this->att = $att;
        $this->user = $user;
        $this->upload = $upload;
        $this->email = $email;
    }

    // check attendance
    // ----------------
    public function getApiAttCheck($i)
    {
        // try check attendance
        // --------------------
        try {

            // set initial status
            // ------------------
            $empty = 0;
            $status = null;

            // check request value (must have)
            // -------------------------------
            if (empty($i['user_id'])): $empty = 1; endif;
            if (empty($i['date'])): $empty = 1; endif;

            // ensure no empty value from mobile
            // ---------------------------------
            if ($empty != 1) {

                // initial time
                // ------------
                $in = null; 
                $out = null;

                // get attendance gps
                // ------------------                
                $check = $this->att->getAttGpsCheck($i['user_id'], $i['date']);
                if (!empty($check) && count($check) > 0) {

                    // get value in and out
                    // --------------------
                    foreach ($check as $i) {
                        if ($i->in_out == 0) {
                            $in = Carbon::parse($i->datetime)->format('H:i:s');
                        }
                        else if ($i->in_out == 1) {
                            $out = Carbon::parse($i->datetime)->format('H:i:s');
                        }
                    }

                    // check value in and out
                    // ----------------------
                    if ($in == null || $out == null) {
                        $message = 'att_check_incomplete';
                    } 
                    else {
                        $message = 'att_check_complete';
                    }                    
                    $data = array('status' => 1, 'in' => $in, 'out' => $out, 'message' => $message);
                }
                else {
                    $data = array('status' => 0, 'message' => 'att_check_empty');
                }
            }

            // request value is empty
            // ----------------------
            else {
                $data = array('status' => 0, 'message' => 'att_check_empty');
            }
            return response()->json($data);
        }

        // something wrong with api
        // ------------------------
        catch (JWTException $e) {
            return response()->json(['error' => 'att_check_problem'], 500);
        }         
    }

    // store attendance mobile
    // -----------------------
    public function getApiAttCreate($i) 
    {
        // try store attendance
        // --------------------
        try {

            // set initial status
            // ------------------
            $empty = 0;
            $unknown = 0;
            $status = null;

            // check request value (must have)
            // -------------------------------
            if (empty($i['icno'])): $empty = 1; endif;
            if (empty($i['sitecode'])): $empty = 1; endif;
            if (empty($i['datetime'])): $empty = 1; endif;
            if (empty($i['imei'])): $empty = 1; endif;

            // ensure no empty value from mobile
            // ---------------------------------
            if ($empty != 1) {

                // get method name
                // ---------------
                $method = $this->getApiAttMethod($i['qr_code']);
                if ($method['name'] == 'QRCode') {
                    $i['sitecode'] = $method['sitecode'];
                }

                // qrcode and gps is empty
                // -----------------------
                if (empty($i['qr_code']) && empty($i['location'])) {
                    $unknown = 1;
                }                

                // check whether using qrcode or gps
                // ---------------------------------
                if ($unknown != 1) {

                    // check if check in/out already insert
                    // ------------------------------------
                    $i['date'] = Carbon::parse($i['datetime'])->format('Y-m-d');
                    $check = $this->att->getAttGpsPunch($i);

                    // attendance is empty
                    // -------------------
                    if ($check == 0) {

                        // get status name
                        // ---------------
                        $status = $this->getApiAttStatus($method['name'], $i['flag'], $i['transfer']);
                        $i['status'] = $status;

                        // process attendance
                        // ------------------
                        $i['transfer'] = 1;
                        $data = $this->getApiAttProcess($i);
                    }

                    // attendance already exist
                    // ------------------------
                    else {
                        $data = array('status' => 0, 'message' => 'att_check_exist');
                    }
                }

                // qrcode or gps is empty
                // ----------------------
                else {
                    $data = array('status' => 0, 'message' => 'att_check_fail');
                }
            }

            // have empty value
            // ----------------
            else {
                $data = array('status' => 2, 'message' => 'att_check_empty');
            }

            // return object
            // -------------
            return response()->json($data);
        }

        // something wrong with api
        // ------------------------
        catch (JWTException $e) {
            return response()->json(['error' => 'att_check_problem'], 500);
        }        
    }

    // get method name
    // ---------------
    public function getApiAttMethod($qr_code)
    {
        // initial variable
        // ----------------
        $sitecode = null;

        // check if using qrcode
        // ---------------------
        if (!empty($qr_code)) {

            // get sitecode from qrcode
            // if location it get sitecode from mobile
            // ---------------------------------------
            $method = 'QRCode';
            $exp = explode('#', $qr_code);
            $sitecode = end($exp);
        }

        // check if using gps
        // ------------------
        else {
            $method = 'Location';                
        }

        // return result
        // -------------
        return array('name' => $method, 'sitecode' => $sitecode);
    }

    // get status name
    // ---------------
    public function getApiAttStatus($name, $flag, $transfer)
    {
        // get status flag
        // ---------------
        $status_flag = $this->att->getAttGpsFlag($flag);
        if (!empty($status_flag)) {
            $status_2nd = $status_flag->name;
        }
        else {
            $status_2nd = 'Unknown';
        }

        // get transfer name
        // -----------------
        $status_4th = ($transfer == 1) ? 'Direct': 'Indirect';

        // combine status
        // --------------
        $data = $name.', '.$status_2nd.', Mobile, '.$status_4th;        

        // return result
        // -------------
        return $data;
    }

    // process attendance
    // ------------------
    public function getApiAttProcess($i)
    {
        // insert attendance log
        // ---------------------
        $id = $this->api_att_db->dbInsertAtt($i);

        // insert success
        // --------------
        if ($id) {

            // insert attendance gps
            // ---------------------
            $this->api_att_db->dbInsertAttGps($i, $id);
            $data = array('status' => 1, 'message' => 'att_check_success');
        }

        // insert fail
        // -----------
        else {
            $data = array('status' => 0, 'message' => 'att_check_fail');
        }

        // return result
        // -------------
        return $data;        
    }

    // sync attendance
    // ---------------
    public function getApiAttSync($i)
    {
        // try check attendance
        // --------------------
        try {

            // initial variables
            // -----------------
            $total = 0;
            $insert = 0;
            $fail = 0;
            $fail_data = array();
            $message = 'att_check_empty';
            $transfer = 2;
            $date = date('Y-m-d H:i:s');
            $name = '';
            $sitecode = '';

            // have record
            // -----------
            if (count($i['data']) > 0) {

                // loop record
                // -----------
                foreach ($i['data'] as $a) {
                    $total++;

                    // set initial status empty
                    // ------------------------
                    $empty = 0;

                    // check request value (must have)
                    // -------------------------------
                    if (empty($a['icno'])): $empty = 1; endif;
                    if (empty($a['sitecode'])): $empty = 1; endif;
                    if (empty($a['datetime'])): $empty = 1; endif;
                    if (empty($a['imei'])): $empty = 1; endif;

                    // ensure no empty value from mobile
                    // ---------------------------------
                    if ($empty != 1) {

                        // get user info
                        // -------------
                        $user = $this->user->getUserByActiveICSite($a['icno'], $a['sitecode']);
                        $name = $user->name;
                        $sitecode = $user->sitecode;

                        // get status name
                        // ---------------
                        $status = $this->getApiAttStatus('Location', $a['flag'], $transfer);
                        $a['status'] = $status;

                        // process attendance
                        // ------------------
                        $a['transfer'] = $transfer;
                        $save = $this->getApiAttProcess($a);
                        if ($save['status'] == 1) {
                            $insert++;
                        }
                        else {
                            $fail++;
                        }
                    }

                    // have empty value
                    // ----------------
                    else {

                        // store data fail
                        // ---------------
                        $fail_data[] = array('datetime' => $a['datetime'], 'type_id' => $a['type_id']);
                        $fail++;
                    }
                }

                // have at leave 1 record success
                // ------------------------------
                if ($insert > 0) {
                    $message = 'att_check_partial';
                }
            }

            // check if all fail
            // -----------------
            if ($total == $fail) {
                $message = 'att_sync_fail';
            }

            // check if all success
            // --------------------
            if ($total == $insert) {
                $message = 'att_sync_success';
            }

            // return result
            // -------------
            $data = array(
                'total' => $total, 
                'insert' => $insert, 
                'fail' => array('total' => $fail, 'data' => $fail_data),                 
                'message' => $message,
                'date' => $date
            );

            // set email header
            // ----------------
            $header = array(
                'total' => $total,
                'insert' => $insert,
                'date' => $date,
                'name' => $name,
                'sitecode' => $sitecode                
            );            

            // send email notification
            // -----------------------
            $this->email->getEmailApiAttSync($data, $header);

            // return object
            // -------------
            return response()->json($data);
        }

        // something wrong with api
        // ------------------------
        catch (JWTException $e) {
            return response()->json(['error' => 'att_check_problem'], 500);
        }          
    }

    // get device info
    // ---------------
    public function getApiDevice() {

    }

    // upload log file
    // ---------------
    public function getApiAttLogUpload($i)
    {
        // try execute api
        // ---------------
        try {

            // initial variables
            // -----------------
            $insert = 0;
            $fail = 0;
            $empty = 0;

            // check request value (must have)
            // -------------------------------
            if (empty($i['filename'])): $empty = 1; endif;
            if (empty($i['user_id'])): $empty = 1; endif;
            if (empty($i['sitecode'])): $empty = 1; endif;

            // both have value
            // ---------------
            if ($empty != 1) {

                // get request value
                // -----------------
                $filename = $i['filename'];
                $uid = $i['user_id'];
                $sitecode = $i['sitecode'];
                
                // upload text file & insert db
                // ----------------------------
                $upload = $this->upload->uploadLogAttGps($filename, $uid, $sitecode);

                // get user info
                // -------------
                $user = $this->user->getUserActive($uid, $sitecode);

                // set email header
                // ----------------
                $header = array(
                    'user' => $user
                );

                // set email content
                // -----------------
                $content = array(
                    'user' => $user,
                    'upload' => $upload,
                );

                // send email notification
                // -----------------------
                $this->email->getEmailApiAttUpload($content, $header);

                // return success
                // --------------
                $data = array('status' => 1, 'message' => 'att_upload_success');  
            }

            // have empty value
            // ----------------
            else {

                // return fail
                // -----------
                $data = array('status' => 0, 'message' => 'att_upload_fail');  
            }

            // return result
            // -------------
            return response()->json($data);
        }

        // something wrong with api
        // ------------------------
        catch (JWTException $e) {
            return response()->json(['error' => 'att_check_problem'], 500);
        } 
    }

    // check attendance sitecode
    // -------------------------
    public function getApiChkSitecode()
    {
        // try execute api
        // ---------------
        try {
            $total = 0;

            // query lists of sitecodes
            // ------------------------
            $q = \IhrV2\Models\AttendanceGpsSitecode::where(['status' => 1])
            ->orderBy('sitecode', 'asc')
            ->get();
            $data = array();
            if (!empty($q)) {
                foreach ($q as $i) {
                    $total++;
                    $data[] = $i['sitecode'];
                }
            }
            return response()->json(array('status' => 1, 'total' => $total, 'message' => 'att_chk_sitecode', 'sitecode' => $data));
        }

        // something wrong with api
        // ------------------------
        catch (JWTException $e) {
            return response()->json(['error' => 'att_check_problem'], 500);
        } 
    }

}


