<?php
namespace IhrV2\Repositories\Api\Training;

use IhrV2\Contracts\Api\Training\ApiTrainingInterface;
use IhrV2\Contracts\Api\Training\DbApiTrainingInterface;
use IhrV2\Contracts\Training\TrainingInterface;
use Carbon\Carbon;

class ApiTrainingRepository implements ApiTrainingInterface 
{

    protected $api_tt_db;
    protected $training;

    public function __construct(DbApiTrainingInterface $api_tt_db, TrainingInterface $training) {
        $this->api_tt_db = $api_tt_db;
        $this->training = $training;
    }


	// store attendance training
	// -------------------------
	public function getApiTtgAttCreate($i)
	{
        // try check attendance
        // --------------------
        try {

            // set initial status
            // ------------------
            $empty = 0;

            // check request value (must have)
            // -------------------------------
            if (empty($i['training_id'])): $empty = 1; endif;
            if (empty($i['user_id'])): $empty = 1; endif;
            if (empty($i['sitecode'])): $empty = 1; endif;

            // ensure no empty value from mobile
            // ---------------------------------
            if ($empty != 1) {

            	// check training
            	// --------------
            	$chk_tt = $this->training->getTrainingByID($i['training_id']);

            	// training exist
            	// --------------
            	if (!empty($chk_tt)) {

            		// get user_id and sitecode
            		// ------------------------
            		$uid = $i['user_id'];
            		$sitecode = $i['sitecode'];

            		// check training attendance
            		// -------------------------
            		$chk_tt_att = $this->training->getTrainingAttByUser($chk_tt->id, $uid, $sitecode);

            		// no attendance yet
            		// -----------------
            		if (empty($chk_tt_att)) {

						// insert attendance
						// -----------------
						$save = $this->api_tt_db->dbInsertTrainingAtt($i);

						// save success
						// ------------
						if ($save) {
							$data = array('status' => 1, 'message' => 'att_tt_success');
						}

						// save fail
						// ---------
						else {
							$data = array('status' => 0, 'message' => 'att_tt_fail');
						}	
            		}

            		// attendance already exist
            		// ------------------------
            		else {
						$data = array('status' => 0, 'message' => 'att_tt_exist');
            		}			            		
            	}

            	// no record training 
            	// ------------------
            	else {
					$data = array('status' => 0, 'message' => 'att_tt_empty');
            	}			
			}

			// request record is empty
			// -----------------------
			else {
                $data = array('status' => 0, 'message' => 'att_tt_empty');
			}

			// return result
			// -------------
			return response()->json($data);				
		}

        // something wrong with api
        // ------------------------
        catch (JWTException $e) {
            return response()->json(['error' => 'att_tt_problem'], 500);
        }   

	}

}


