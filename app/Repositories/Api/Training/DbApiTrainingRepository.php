<?php
namespace IhrV2\Repositories\Api\Training;

use IhrV2\Contracts\Api\Training\DbApiTrainingInterface;
use Carbon\Carbon;

class DbApiTrainingRepository implements DbApiTrainingInterface 
{

	// insert training attendance
	// --------------------------
	public function dbInsertTrainingAtt($data)
	{
        $arr = array(
            'training_id' => $data['training_id'],
            'user_id'    => $data['user_id'],
            'sitecode'   => $data['sitecode'],
            'latitude'   => $data['latitude'],
            'longitude'  => $data['longitude'],
            'qr_code'    => $data['qr_code'],
            'datetime'   => Carbon::parse($data['datetime'])->format('Y-m-d H:i:s'),
            'location'   => $data['location'],
            'remark'     => $data['remark'],
            'flag'       => $data['flag'],
            'imei'       => ($data['imei']) ? $data['imei'] : null,
            'version'    => (!empty($data['version'])) ? $data['version'] : null,
            'build_no'   => (!empty($data['build_no'])) ? $data['build_no'] : null,
            'model'      => (!empty($data['model'])) ? $data['model'] : null,
        );
        $sav = new \IhrV2\Models\TrainingAttendanceLog($arr);
        $sav->save();
        return true;
	}

}


