<?php
namespace IhrV2\Repositories\Api\Auth;

use IhrV2\Contracts\Api\Auth\ApiAuthInterface;
use IhrV2\Contracts\Api\Auth\DbApiAuthInterface;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiAuthRepository implements ApiAuthInterface {

    protected $db_auth;
    public function __construct(DbApiAuthInterface $db_auth) {
        $this->db_auth = $db_auth;
    }

	// api login
	// ---------
	public function getApiLogin($request) {

        // grab credentials from the request
        // ---------------------------------
        $credentials = $request->only('staff_id', 'password');
        try {

            // group id must site supervisor/mobile tester (mobile = 1)
            // --------------------------------------------------------
            $credentials['mobile'] = 1;

            // status must active
            // ------------------
            $credentials['status'] = 1;

            // attempt to verify the credentials and create a token for the user
            // -----------------------------------------------------------------
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {

            // something went wrong whilst attempting to encode the token
            // ----------------------------------------------------------
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        // ----------------------------
        return response()->json(compact('token'));
	}

	// api token
    // if trigger this api it will create new token
	// --------------------------------------------
	public function getApiToken() {
        $token = JWTAuth::getToken();
        if (!$token) {
            return response()->json(['error' => 'token_invalid'], 401);
        }
        try {
            // if token expired renew token
            // ----------------------------
            $refresh = JWTAuth::refresh($token);
        } catch (JWTException $e) {
            return response()->json(['error' => 'someting_when_wrong'], 500);
        }
        return response()->json(compact('refresh'));
	}

	// api user
	// --------
	public function getApiUser($request) {

        // get user info
        // -------------
        $user = JWTAuth::parseToken()->toUser();

        // imei server is null
        // -------------------
        if (empty($user->imei)) {

            // have request imei
            // -----------------
            if (!empty($request->imei)) {

                // update imei & device
                // --------------------
                \IhrV2\User::find($user->id)->update([
                    'imei' => ($request->imei) ? $request->imei : null,
                    'device' => ($request->device) ? $request->device : null,
                    'last_login' => date('Y-m-d H:i:s')
                ]);

                // get updated user
                // ----------------
                $user = \IhrV2\User::find($user->id);
                $arr = array(1, 'imei_updated');                   
            }

            // request imei empty
            // ------------------
            else {
                $arr = array(0, 'imei_empty');
            }         
        }

        // imei server exist
        // -----------------
        else {

            // parameter imei exist
            // --------------------
            if (!empty($request->imei)) {

                // update device if exist
                // ----------------------
                if (!empty($request->device)) {
                    \IhrV2\User::find($user->id)->update([
                        'device' => ($request->device) ? $request->device : null
                    ]);
                    $user = \IhrV2\User::find($user->id);                    
                }

                // compare imei
                // ------------
                if ($request->imei != $user->imei) {
                    $arr = array(0, 'imei_invalid');
                } 

                // both imei same
                // --------------
                else {
                    $arr = array(1, 'imei_valid');
                }
            }

            // parameter imei not exist
            // ------------------------
            else {
                $arr = array(0, 'imei_empty');
            }
        }

        // return object
        // -------------
        return response()->json(['status' => $arr[0], 'message' => $arr[1], 'user' => $user]);
	}

	// api logout
	// ----------
	public function getApiLogout() {
        $user = JWTAuth::parseToken()->authenticate();
        $user->delete();
        return response()->json(['message' => 'successfully_logout']);
	}


}


