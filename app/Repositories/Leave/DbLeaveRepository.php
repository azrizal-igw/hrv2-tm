<?php
namespace IhrV2\Repositories\Leave;

use Carbon\Carbon;
use IhrV2\Contracts\Leave\DbLeaveInterface;

class DbLeaveRepository implements DbLeaveInterface {

    public function dbUpdateMissingLeaveAdd($arr) {
        $update = \IhrV2\Models\LeaveAdd::where('user_id', $arr[0])
            ->update(array(
                'contract_id' => $arr[2],
                'sitecode'    => $arr[1]
            ));
        return true;
    }

    // update replacement leave history
    // --------------------------------
    public function dbUpdateLeaveRepHistory($id, $uid) {
        $update = \IhrV2\Models\LeaveRepHistory::where('leave_rep_id', $id)
            ->where('user_id', $uid)
            ->where('flag', 1)
            ->update(array('flag' => 0));
        return true;
    }

    // insert replacement leave attachment
    // -----------------------------------
    public function dbInsertLeaveRepAttachment($rep_id, $user_id, $i) {
        $arr = array(
            'user_id'      => $user_id,
            'leave_rep_id' => $rep_id,
            'filename'     => $i['filename'],
            'ext'          => strtolower($i['fileext']),
            'size'         => $i['filesize'],
            'thumb_name'   => $i['filethumb'],
            'status'       => 1
        );
        $sav = new \IhrV2\Models\LeaveRepAttachment($arr);
        $sav->save();
        return true;
    }

    // insert reimburse/deduct leave attachment
    // ----------------------------------------
    public function dbInsertLeaveAddAttachment($id, $user_id, $i) {
        $arr = array(
            'user_id'      => $user_id,
            'leave_add_id' => $id,
            'filename'     => $i['filename'],
            'ext'          => strtolower($i['fileext']),
            'size'         => $i['filesize'],
            'thumb_name'   => $i['filethumb'],
            'status'       => 1
        );
        $sav = new \IhrV2\Models\LeaveAddAttachment($arr);
        $sav->save();
        return true;
    }

    // insert replacement leave
    // ------------------------
    public function dbInsertLeaveRepApplication($i, $report_to) {
        $date_work = Carbon::createFromFormat('d/m/Y', $i['date_work'])->format('Y-m-d');
        $arr = array(
            'user_id'       => auth()->user()->id,
            'contract_id'   => $i['contract_id'],
            'date_apply'    => date('Y-m-d H:i:s'),
            'no_day'        => $i['no_day'],
            'year_month'    => $i['year'] . '-' . $i['month'] . '-01',
            'date_work'     => $date_work,
            'report_to'     => $report_to,
            'instructed_by' => $i['instructed_by'],
            'location'      => $i['location'],
            'reason'        => $i['reason'],
            'notes'         => $i['notes'],
            'sitecode'      => auth()->user()->sitecode,
            'active'        => 1
        );
        $sav = new \IhrV2\Models\LeaveRepApplication($arr);
        $sav->save();
        return $sav->id;
    }

    // insert replacement leave history
    // --------------------------------
    public function dbInsertLeaveRepHistory($rep_id) {
        $arr = array(
            'user_id'       => auth()->user()->id,
            'leave_rep_id'  => $rep_id,
            'action_date'   => date('Y-m-d H:i:s'),
            'action_by'     => auth()->user()->id,
            'action_remark' => '',
            'status'        => 1, // pending
            'flag'          => 1 // active
        );
        $sav = new \IhrV2\Models\LeaveRepHistory($arr);
        $sav->save();
        return true;
    }

    // process replacement leave history
    // ---------------------------------
    public function dbProcessLeaveRepHistory($rep_id, $uid, $data) 
    {
        $arr = array(
            'user_id'       => $uid,
            'leave_rep_id'  => $rep_id,
            'action_date'   => date('Y-m-d H:i:s'),
            'action_by'     => $data['action_by'],
            'action_remark' => $data['remark'],
            'status'        => $data['type'],
            'flag'          => 1 // active
        );
        $sav = new \IhrV2\Models\LeaveRepHistory($arr);
        $sav->save();
        return true;
    }

    // approve replacement leave
    // -------------------------
    public function dbInsertLeaveRepApprove($rep_id, $uid, $no_day, $rid) {
        $arr = array(
            'user_id'      => $uid,
            'leave_rep_id' => $rep_id,
            'date_action'  => date('Y-m-d H:i:s'),
            'action_by'    => $rid,
            'total_day'    => $no_day,
            'flag'         => 1
        );
        $sav = new \IhrV2\Models\LeaveRepApprove($arr);
        $sav->save();
        return true;
    }

    // update replacement leave approve
    // --------------------------------
    public function dbUpdateLeaveRepApprove($rep_id, $uid) {
        $update_approve = \IhrV2\Models\LeaveRepApprove::where('leave_rep_id', $rep_id)
            ->where('user_id', $uid)
            ->where('flag', 1)
            ->update(array('flag' => 0));
        return true;
    }

    public function dbUpdateLeaveApprove($id, $uid) {
        $update = \IhrV2\Models\LeaveApprove::where('user_id', $uid)
            ->where('leave_id', $id)
            ->where('flag', 1)
            ->update(array('flag' => 0));
        return true;
    }

    // insert record contract_id and leave_type_id
    // -------------------------------------------
    public function dbUpdateLeaveApproveAdd($id, $leave_type_id, $contract_id) {
        $update = \IhrV2\Models\LeaveApprove::where('id', $id)
            ->update(array(
                'contract_id'   => $contract_id,
                'leave_type_id' => $leave_type_id
            ));
        return true;
    }

    public function dbUpdateLeaveAttachment($id, $uid) {
        $update = \IhrV2\Models\LeaveAttachment::where('leave_id', $id)
        // ->where('user_id', $uid)
        ->where('status', 1)
        ->update(array('status' => 2));
        return true;
    }

    public function dbUpdateLeaveRepAttachment($id, $uid) {
        $update = \IhrV2\Models\LeaveRepAttachment::where('leave_rep_id', $id)
            ->where('user_id', $uid)
            ->where('status', 1)
            ->update(array('status' => 2));
        return true;
    }

    public function dbInsertLeaveApplication($i) {
        $arr = array(
            'user_id'       => $i['user_id'],
            'contract_id'   => $i['contract_id'],
            'leave_type_id' => $i['leave_type_id'],
            'date_apply'    => date('Y-m-d H:i:s'),
            'report_to'     => $i['report_to'],
            'date_from'     => $i['date_from'],
            'date_to'       => $i['date_to'],
            'is_half_day'   => $i['is_half_day'],
            'desc'          => $i['desc'],
            'sitecode'      => $i['sitecode'],
            'active'        => 1
        );
        $sav = new \IhrV2\Models\LeaveApplication($arr);
        $sav->save();
        return $sav->id;
    }

    // insert leave date
    // -----------------
    public function dbInsertLeaveDate($user_id, $leave_id, $date, $type_id) {
        $d = array(
            'user_id'    => $user_id,
            'leave_id'   => $leave_id,
            'leave_date' => $date,
            'leave_type' => $type_id, // 1 = available | 2/3/4/5/6 (leave_off_types) 
            'status'     => 1
        );
        $ld = new \IhrV2\Models\LeaveDate($d);
        $ld->save();
        return true;
    }

    // insert leave histories
    // ----------------------
    public function dbInsertLeaveHistory($user_id, $leave_id) {
        $dh = array(
            'user_id'       => $user_id,
            'leave_id'      => $leave_id,
            'action_date'   => date('Y-m-d H:i:s'),
            'action_by'     => $user_id,
            'action_remark' => '',
            'status'        => 1, // pending
            'flag'          => 1 // active 1st
        );
        $lh = new \IhrV2\Models\LeaveHistory($dh);
        $lh->save();
        return true;
    }

    // insert leave histories inactive
    // -------------------------------
    public function dbInsertLeaveHistInactive($user_id, $leave_id) {
        $dh = array(
            'user_id'       => $user_id,
            'leave_id'      => $leave_id,
            'action_date'   => date('Y-m-d H:i:s'),
            'action_by'     => $user_id,
            'action_remark' => '',
            'status'        => 1, // pending
            'flag'          => 0
        );
        $lh = new \IhrV2\Models\LeaveHistory($dh);
        $lh->save();
        return true;
    }

    public function dbInsertLeaveHistoryByStatus($user_id, $leave_id, $rm, $status) {
        $dh = array(
            'user_id'       => $user_id,
            'leave_id'      => $leave_id,
            'action_date'   => date('Y-m-d H:i:s'),
            'action_by'     => $rm,
            'action_remark' => '',
            'status'        => $status,
            'flag'          => 1
        );
        $lh = new \IhrV2\Models\LeaveHistory($dh);
        $lh->save();
        return true;
    }

    public function dbInsertLeaveHistoryArray($i) {
        $arr = array(
            'user_id'       => $i['user_id'],
            'leave_id'      => $i['leave_id'],
            'action_date'   => $i['action_date'],
            'action_by'     => $i['action_by'],
            'action_remark' => $i['remark'],
            'status'        => $i['status'],
            'flag'          => 1 // active
        );
        $sav = new \IhrV2\Models\LeaveHistory($arr);
        $sav->save();
        return true;
    }

    public function dbInsertLeaveApprove($i) {
        $arr = array(
            'user_id'       => $i['user_id'],
            'leave_id'      => $i['leave_id'],
            'leave_type_id' => $i['leave_type_id'],
            'contract_id'   => $i['contract_id'],
            'date_action'   => $i['date_action'],
            'action_by'     => $i['action_by'],
            'date_value'    => $i['date_value'],
            'flag'          => 1 // active
        );
        $sav = new \IhrV2\Models\LeaveApprove($arr);
        $sav->save();
        return true;
    }

    // update status leave pending to inactive
    // ---------------------------------------
    public function dbUpdateLeaveHistory($user_id, $leave_id) {
        $update = \IhrV2\Models\LeaveHistory::where('leave_id', $leave_id)
            ->where('user_id', $user_id)
            // ->where('status', 1)
            ->where('flag', 1)
            ->update(array('flag' => 0));
        return true;
    }

    public function dbInsertLeaveAttachment($leave_id, $user_id, $i) {
        $arr = array(
            // 'user_id' => $user_id,
            'user_id'    => auth()->user()->id,
            'leave_id'   => $leave_id,
            'filename'   => $i['filename'],
            'ext'        => strtolower($i['fileext']),
            'size'       => $i['filesize'],
            'thumb_name' => $i['filethumb'],
            'status'     => 1
        );
        $la = new \IhrV2\Models\LeaveAttachment($arr);
        $la->save();
        return true;
    }

    public function dbUpdateLeaveBalance($user_id, $leave_type_id, $contract_id, $bal) {
        $update = \IhrV2\Models\LeaveBalance::where('user_id', $user_id)
            ->where('leave_type_id', $leave_type_id)
            ->where('contract_id', $contract_id)
            ->update(array('balance' => $bal));
        return true;
    }

    public function dbInsertLeaveBalance($user_id, $leave_type_id, $entitled, $contract_id) {
        $db = array(
            'user_id'       => $user_id,
            'leave_type_id' => $leave_type_id,
            'balance'       => $entitled,
            'contract_id'   => $contract_id,
            'year'          => date('Y')
        );
        $lb = new \IhrV2\Models\LeaveBalance($db);
        $lb->save();
        return true;
    }

    // insert leave add
    // ----------------
    public function dbInsertLeaveAdd($i, $uid, $sitecode) {

        // insert leave add
        // ----------------
        $arr = array(
            'contract_id'   => $i['contract_id'],
            'user_id'       => $uid,
            'leave_type_id' => $i['leave_type_id'],
            'type_id'       => $i['type_id'],
            'action_date'   => date('Y-m-d'),
            'action_by'     => auth()->user()->id,
            'total'         => $i['total'],
            'reason'        => $i['reason'],
            'sitecode'      => $sitecode,
            'status'        => 1
        );
        $sav = new \IhrV2\Models\LeaveAdd($arr);
        $sav->save();
        return $sav->id;
    }

    public function dbUpdateLeaveAdd($i, $uid, $id) {

        // update leave add
        // ----------------
        $arr = array(
            'leave_type_id' => $i['leave_type_id'],
            'type_id'       => $i['type_id'],
            'total'         => $i['total'],
            'reason'        => $i['reason']
        );
        $save = \IhrV2\Models\LeaveAdd::where('id', $id)->where('user_id', $uid)->update($arr);

        // update leave balance
        // --------------------
        return true;
    }

    // insert new reimburse for unplan leave limit
    // -------------------------------------------
    public function dbInsertUnplanLimit($i, $uid, $sitecode) {

        // insert leave_unplan_limits
        // --------------------------
        $arr = array(
            'user_id'     => $uid,
            'contract_id' => $i['contract_id'],
            'action_date' => date('Y-m-d H:i:s'),
            'action_by'   => auth()->user()->id,
            'total'       => $i['total'],
            'reason'      => $i['reason'],
            'sitecode'    => $sitecode,
            'status'      => 1
        );
        $sav = new \IhrV2\Models\LeaveUnplanLimit($arr);
        $sav->save();
        return $sav->id;
    }

}
