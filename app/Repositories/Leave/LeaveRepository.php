<?php namespace IhrV2\Repositories\Leave;

use Carbon\Carbon;
use Crypt;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Leave\DbLeaveInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Contracts\Maintenance\MaintenanceInterface;
use IhrV2\Helpers\CommonHelper;
use IhrV2\Models\LeaveApplication;
use IhrV2\Models\LeaveType;
use IhrV2\User;

class LeaveRepository implements LeaveInterface
{
    protected $db_leave_repo;
    protected $user_repo;
    protected $email_repo;
    protected $upload_repo;

    public function __construct(UserInterface $user_repo, DbLeaveInterface $db_leave_repo, EmailInterface $email_repo, UploadInterface $upload_repo)
    {
        $this->db_leave_repo = $db_leave_repo;
        $this->user_repo = $user_repo;
        $this->email_repo = $email_repo;
        $this->upload_repo = $upload_repo;
    }

    // get leave type that available
    // -----------------------------
    public function getLeaveTypeAvail()
    {
        return LeaveType::where('attachment', 1)->pluck('id')->toArray();
    }

    // by pass end contract if following leave type (no end date)
    // ----------------------------------------------------------
    public function getByPassEndCont()
    {
        return LeaveType::where('bypass', 1)->pluck('id')->toArray();        
    }

    // get m/am off day id
    // -------------------
    public function getOffDayID($type)
    {
        return \IhrV2\Models\LeaveOffType::where('positions', 'like', '%' . $type . '%')->pluck('id')->push('7')->toArray();
    }

    // get off type list
    // -----------------
    public function getLeaveOffTypeAll()
    {
        return \IhrV2\Models\LeaveOffType::get();
    }

    // get off day type id by position
    // -------------------------------
    public function getOffDayTypeIDByPosition($position_id)
    {
        $ids = array();
        $lists = $this->getLeaveOffTypeAll()->toArray();
        foreach ($lists as $i) {
            $exp = explode(':', $i['positions']);
            if (in_array($position_id, $exp)) {
                $ids[] = $i['id'];
            }
        }
        return $ids;
    }

    // get off day type id all
    // -----------------------
    public function getOffDayTypeIDAll()
    {
        $ids = array();
        $lists = $this->getLeaveOffTypeAll()->toArray();
        foreach ($lists as $i) {
            $ids[] = $i['id'];
        }
        return $ids;
    }

    // lists all leaves
    // ----------------
    public function getLeaveIndex($request = null)
    {
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Leave',
            'child-a' => route('mod.leave.index'),
            'icon'    => 'grid',
            'title'   => 'Leave'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $leave_type_id = CommonHelper::checkSession('leave_type_id');
            $leave_status_id = CommonHelper::checkSession('leave_status_id');
            $half_day_id = CommonHelper::checkSession('half_day_id');
            $position_id = CommonHelper::checkSession('position_id');
            $start_date = CommonHelper::checkSession('start_date');
            $end_date = CommonHelper::checkSession('end_date');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                $inputs = array(
                    'site_id',
                    'region_id',
                    'phase_id',
                    'leave_type_id',
                    'leave_status_id',
                    'half_day_id',
                    'position_id',
                    'start_date',
                    'end_date',
                    'keyword'
                );
                session()->forget($inputs);
                return redirect()->route('mod.leave.index');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $leave_type_id = CommonHelper::checkSessionPost($request->leave_type_id, $reset, 'leave_type_id');
            $leave_status_id = CommonHelper::checkSessionPost($request->leave_status_id, $reset, 'leave_status_id');
            $half_day_id = CommonHelper::checkSessionPost($request->half_day_id, $reset, 'half_day_id');
            $position_id = CommonHelper::checkSessionPost($request->position_id, $reset, 'position_id');
            $start_date = CommonHelper::checkSessionPost($request->start_date, $reset, 'start_date');
            $end_date = CommonHelper::checkSessionPost($request->end_date, $reset, 'end_date');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];
        $groups = $chk['groups'];

        // query leaves
        // ------------
        $sessions = array(
            'site_id'         => $site_id,
            'leave_type_id'   => $leave_type_id,
            'start_date'      => $start_date,
            'end_date'        => $end_date,
            'keyword'         => $keyword,
            'leave_status_id' => $leave_status_id,
            'half_day_id'     => $half_day_id,
            'position_id'     => $position_id,
            'region_id'       => $region_id,
            'phase_id'        => $phase_id
        );
        $leaves = $this->getLeaveIndexListAll($sessions, $codes);

        // lists drop down
        // ---------------
        $phases = $this->user_repo->getPhaseList();
        $regions = $this->user_repo->getRegionList();
        $leave_types = $this->getLeaveType();
        $leave_status = $this->getLeaveStatusList();
        $half_day = $this->getListHalfDay();
        $positions = $this->user_repo->getPositionListDistinct($groups);
        return View('modules.leave.index', compact('header', 'leaves', 'sites', 'phases', 'regions', 'leave_types', 'leave_status', 'half_day', 'positions', 'sessions'));
    }

    // lists of manage leave
    // ---------------------
    public function getLeaveUserIndex($request = null)
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Manage leave',
            'icon'   => 'settings',
            'title'  => 'Manage Leave'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $position_id = CommonHelper::checkSession('position_id');
            $status_id = CommonHelper::checkSession('status_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('site_id', 'region_id', 'phase_id', 'position_id', 'status_id', 'keyword'));
                return redirect()->route('mod.leave.user.index');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $position_id = CommonHelper::checkSessionPost($request->position_id, $reset, 'position_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];
        $groups = $chk['groups'];

        // query replacement request
        // -------------------------
        $arr = array($site_id, $region_id, $phase_id, $position_id, $status_id, $keyword);
        $users = $this->getLeaveManageListAll($arr, $codes);

        // drop down lists
        // ---------------
        $regions = $this->user_repo->getRegionList();
        $phases = $this->user_repo->getPhaseList();
        $positions = $this->user_repo->getPositionListDistinct($groups);
        $status = $this->user_repo->getUserStatusList();

        // session values
        // --------------
        $sessions = array(
            'site_id'     => $site_id,
            'region_id'   => $region_id,
            'phase_id'    => $phase_id,
            'position_id' => $position_id,
            'status_id'   => $status_id,
            'keyword'     => $keyword
        );
        return View('modules.leave.user.index', compact('header', 'users', 'sites', 'regions', 'phases', 'positions', 'status', 'sessions'));
    }

    // get lists of RL request
    // -----------------------
    public function getLeaveRepIndex($request = null)
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'RL Request',
            'icon'   => 'note',
            'title'  => 'RL Request'
        );

        // check request / next pagination
        // -------------------------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $leave_status_id = CommonHelper::checkSession('leave_status_id');
            $apply_date = CommonHelper::checkSession('apply_date');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                $inputs = array('site_id', 'region_id', 'phase_id', 'leave_status_id', 'apply_date', 'keyword');
                session()->forget($inputs);
                return redirect()->route('mod.leave.replacement.index');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $leave_status_id = CommonHelper::checkSessionPost($request->leave_status_id, $reset, 'leave_status_id');
            $apply_date = CommonHelper::checkSessionPost($request->apply_date, $reset, 'apply_date');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // query replacement request
        // -------------------------
        $arr = array(
            'site_id'         => $site_id,
            'phase_id'        => $phase_id,
            'leave_status_id' => $leave_status_id,
            'keyword'         => $keyword,
            'region_id'       => $region_id,
            'apply_date'      => $apply_date
        );
        $leaves = $this->getLeaveRepIndexListAll($arr, $codes);

        // drop down data
        // --------------
        $leave_status = $this->getRLReqStatusList();
        $months = $this->user_repo->getMonthList();
        $phases = $this->user_repo->getPhaseList();
        $regions = $this->user_repo->getRegionList();

        // sessions data
        // -------------
        $sessions = array(
            'site_id'         => $site_id,
            'region_id'       => $region_id,
            'phase_id'        => $phase_id,
            'leave_status_id' => $leave_status_id,
            'apply_date'      => $apply_date,
            'keyword'         => $keyword
        );
        return View('modules.rl-request.index', compact('header', 'sites', 'leaves', 'leave_status', 'months', 'phases', 'regions', 'sessions'));
    }

    // view all RL request by user
    // ---------------------------
    public function getLeaveUserRepIndex($request, $uid, $sitecode)
    {
        // get contract id from current contract
        // -------------------------------------
        if (empty($request)) {
            $user = $this->user_repo->getUserJoinContract($uid, $sitecode);
            $contract = $user->UserLatestContract;
        }

        // get contract id form drop down lists
        // ------------------------------------
        else {
            $user = $this->user_repo->getUserActive($uid, $sitecode);
            $contract = $this->user_repo->getUserContractByID($request->contract_id);
        }

        // set page header
        // ---------------
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'RL Request',
            'child-a' => route('mod.leave.replacement.index'),
            'sub'     => 'Staff',
            'icon'    => 'list',
            'title'   => $user->name
        );

        // get contracts lists
        // -------------------
        $contracts = $this->user_repo->getUserContractDrop($uid, $sitecode);

        // get contract
        // ------------
        $contract_id = $contract->id;

        // query replacement leave
        // -----------------------
        $leaves = $this->getLeaveRepByContractID($uid, $sitecode, $contract_id);
        return View('modules.leave.user.rl-request.index', compact('header', 'uid', 'sitecode', 'leaves', 'contracts', 'contract_id'));
    }

    // get all dates in a month
    // ------------------------
    public function getAllDatesInMonth($year_month)
    {
        // $year_month = '2015-01';
        $start = Carbon::parse($year_month)->startOfMonth();
        $end = Carbon::parse($year_month)->endOfMonth();

        $dates = [];
        while ($start->lte($end)) {
            // $dates[] = $start->copy();
            $dates[] = $start->copy()->format('Y-m-d');
            $start->addDay();
        }
        return $dates;
    }

    public function getLeavePublicBySite($state_id, $month)
    {
        $ph = \IhrV2\Models\LeavePublicState::where('year', date('Y'))
            ->where('state_id', $state_id)
            ->where('status', 1)
            ->whereHas('PublicName', function ($d) use ($month) {
                $d->whereMonth('date', '=', $month);
                $d->where('year', date('Y'));
            })
            ->orderBy('date', 'asc')
            ->get();
        return $ph;
    }

    public function getLeavePublicByRegion($states)
    {
        $month = date('m');
        // $month = '09';
        $q = \IhrV2\Models\LeavePublicState::select(\DB::raw('count(leave_public_id) as ph_id'), 'date')
            ->where('year', date('Y'))
            ->whereYear('date', '=', date('Y'))
            ->whereMonth('date', '=', $month)
            ->where(function ($k) use ($states) {
                if (count($states) > 0) {
                    $k->whereIn('state_id', $states);
                }
            })
            ->where('status', 1)
            ->whereHas('PublicName', function ($d) use ($month) {
                $d->whereMonth('date', '=', $month);
                $d->where('year', date('Y'));
            })
            ->with(array('PublicNameByDate'))
            ->groupBy('date')
            ->orderBy('date', 'asc')
            ->get();
        return $q;
    }

    public function getOffDayByRegion($states, $year, $month)
    {
        $q = \IhrV2\Models\LeaveOffDate::whereYear('off_date', '=', $year)
            ->whereMonth('off_date', '=', $month)
            ->where([
                'year'   => $year,
                'status' => 1
            ])
            ->whereHas('ListStates', function ($d) use ($states) {
                if (count($states) > 0) {
                    $d->whereIn('state_id', $states);
                }
            })
            ->groupBy('off_date')
            ->with(array('OffTypeName'))
            ->orderBy('off_date', 'asc')
            ->get();
        return $q;
    }

    public function getListStateOfRegion($region_id)
    {
        $q = \IhrV2\Models\State::where('region_id', $region_id)
            ->lists('code')
            ->toArray();
        return $q;
    }

    // get latest announcement
    // -----------------------
    public function getLatestAnnouncement($type_id)
    {
        $q = \IhrV2\Models\Announcement::where('type_id', $type_id)
            ->where('status', 1)
            ->orderBy('order', 'asc')
            ->get()->take(5);
        return $q;
    }

    // get leave info
    // --------------
    public function getLeaveInfo($id, $uid)
    {
        $q = LeaveApplication::with(array('LeavePrevHistory' =>
            function ($h) {
                $h->with('LeaveStatusName');
            }))
            ->with(array('LeaveDate' => function ($d) {
                $d->where('leave_type', 1); // available
            }))
            ->where('id', $id)
            ->where('user_id', $uid)
            ->firstOrFail();
        return $q;
    }

    // get active attachment of RL request
    // -----------------------------------
    public function getLeaveRepAttachment($id, $uid)
    {
        $detail = \IhrV2\Models\LeaveRepAttachment::where('leave_rep_id', $id)
        // ->where('user_id', $uid)
        ->where('status', 1)
        ->first();
        return $detail;
    }

    // get inactive attachment of RL request
    // -------------------------------------
    public function getInactiveRLRepAttachment($id, $uid)
    {
        $q = \IhrV2\Models\LeaveRepAttachment::where([
            'leave_rep_id' => $id,
            // 'user_id' => $uid,
            'status'       => 2
        ])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // drop list reimburse or deduct
    // -----------------------------
    public function getReimburseDeductList()
    {
        $x = array('' => '[Type]', 1 => 'Reimburse (+)', 2 => 'Deduct (-)');
        return $x;
    }

    // site supervisor apply replacement leave
    // ---------------------------------------
    public function getApplyLeaveRep($data)
    {
        $uid = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;
        $report_to = null;
        $attach = array();
        $not_within = 0;

        // check today request
        // -------------------
        $today = $this->getLeaveRepToday($uid, $sitecode);

        // no record yet for today
        // -----------------------
        if (empty($today) || in_array($today->LeaveRepLatestHistory->status, array(3, 5, 6))) {

            // get contract info
            // -----------------
            $contract = $this->user_repo->getUserContractByUserIDStatus($uid);

            // get date work
            // -------------
            $date_work = Carbon::createFromFormat('d/m/Y', $data['date_work'])->format('Y-m-d');

            // check if date work is within contract or not
            // --------------------------------------------
            if (Carbon::parse($date_work)->lt(Carbon::parse($contract->date_from)) || Carbon::parse($date_work)->gt(Carbon::parse($contract->date_to))) {
                $not_within = 1;
            }

            // get reporting officer
            // ---------------------
            if (!empty(session()->get('rm_info'))) {
                $report_to = session()->get('rm_info');
            }

            // have rm and date work is within contract
            // ----------------------------------------
            if (!empty($report_to) && $not_within != 1) {

                // insert leave_rep_applications
                // -----------------------------
                $data['contract_id'] = $contract->id;
                $rep_id = $this->db_leave_repo->dbInsertLeaveRepApplication($data, $report_to['id']);

                // insert leave_rep_histories
                // --------------------------
                $this->db_leave_repo->dbInsertLeaveRepHistory($rep_id);

                // check if have attachment
                // ------------------------
                if (!empty($data['rep_file'])) {

                    // upload file attachment
                    // ----------------------
                    $attach = $this->upload_repo->uploadFileLeaveRep($data['rep_file'], $rep_id, $uid);
                }

                // generate url
                // ------------
                $com = $rep_id . '/';
                $com .= $uid . '/';
                $com .= $sitecode . '/';
                $com .= $report_to['id'];
                $url = Crypt::encrypt($com) . '?api_token=' . auth()->user()->api_token;

                // email body info
                // ---------------
                $arr_body = array(
                    'position_name' => session()->get('position_name'),
                    'site_name'     => session()->get('site_name'),
                    'rl'            => $this->getLeaveRepByID($rep_id),
                    'approve'       => route('api.rl-request.approve', array($url)),
                    'reject'        => route('api.rl-request.reject', array($url))
                );

                // receiver info
                // -------------
                $arr = array(
                    'rm_email'  => $report_to['email'],
                    'rm_name'   => $report_to['name'],
                    'region_id' => $report_to['region_id'],
                    'attach'    => $attach
                );

                // send email notification
                // -----------------------
                $this->email_repo->getEmailRLCreate($arr_body, $arr);

                // return message
                // --------------
                $msg = array(CommonHelper::getMessage('rl_success_apply'), 'success', 'sv.leave.replacement.index');
            }

            // have rm and contract
            // --------------------
            else {

                // date work is less than start contract
                // -------------------------------------
                if ($not_within == 1) {
                    $msg = array(CommonHelper::getMessage('rl_out_contract'), 'danger', 'sv.leave.replacement.index');
                }

                // no record region manager
                // ------------------------
                else {
                    $msg = array(CommonHelper::getMessage('leave_no_rm'), 'danger', 'sv.leave.replacement.index');
                }
            }
        } else {
            $msg = array(CommonHelper::getMessage('rl_exist_today'), 'danger', 'sv.leave.replacement.index');
        }
        return $msg;
    }

    // update replacement leave attachment
    // -----------------------------------
    public function getUpdateLeaveRepAttachment($data, $id, $uid)
    {
        // check if upload already 5 times
        // -------------------------------
        $count = $this->getTotalLeaveRepAttachment($id, $uid);
        if ($count >= 5) {
            $msg = array('danger', CommonHelper::getMessage('leave_max_attach'));
        }

        // times upload not reach 5 yet
        // ----------------------------
        else {

            // check previous attachment (if any)
            // ----------------------------------
            $prev = $this->getLeaveRepAttachment($id, $uid);

            // have prev record
            // ----------------
            if (!empty($prev)) {

                // update status to 0
                // ------------------
                $this->db_leave_repo->dbUpdateLeaveRepAttachment($id, $uid);
            }

            // upload attachment
            // -----------------
            $this->upload_repo->uploadFileLeaveRep($data['leave_file'], $id, $uid);

            // return success
            // --------------
            $msg = array('success', CommonHelper::getMessage('leave_success_attach'));
        }
        return $msg;
    }

    // get total attachment RL request
    // -------------------------------
    public function getTotalLeaveRepAttachment($id, $uid)
    {
        $q = \IhrV2\Models\LeaveRepAttachment::where(
            [
                'leave_rep_id' => $id,
                'user_id'      => $uid
            ]
        )
            ->count();
        return $q;
    }

    // get leave attachment
    // --------------------
    public function getLeaveAttachment($id, $uid)
    {
        $prev = \IhrV2\Models\LeaveAttachment::where('leave_id', $id)
            ->where('user_id', $uid)
            ->first();
        return $prev;
    }

    // get active leave attachment
    // ---------------------------
    public function getLeaveAttachmentActive($id, $uid)
    {
        $arr = array($id, $uid);
        $q = \IhrV2\Models\LeaveAttachment::where([
            'leave_id' => $id,
            // 'user_id' => $uid,
            'status'   => 1
        ])
            ->first();
        return $q;
    }

    // get leave attachment inactive
    // -----------------------------
    public function getLeaveAttachmentInactive($leave_id, $uid)
    {
        $q = \IhrV2\Models\LeaveAttachment::where(
            [
                'leave_id' => $leave_id,
                // 'user_id' => $uid,
                'status'   => 2
            ]
        )
            ->with(array('UserDetail'))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // update leave attachment
    // -----------------------
    public function getUpdateLeaveAttachment($data, $id)
    {
        $uid = auth()->user()->id;

        // check if upload already 5 times
        // -------------------------------
        $count = $this->getTotalLeaveAttachment($id, $uid);
        if ($count >= 5) {
            $msg = array('danger', CommonHelper::getMessage('leave_max_attach'));
        }

        // still less than 5
        // -----------------
        else {

            // check previous attachment (if any)
            // ----------------------------------
            $prev = $this->getLeaveAttachment($id, $uid);

            // have prev record
            // ----------------
            if (!empty($prev)) {

                // update status to 0
                // ------------------
                $this->db_leave_repo->dbUpdateLeaveAttachment($id, $uid);
            }

            // upload file attachment
            // ----------------------
            $this->upload_repo->uploadFileLeave($data['leave_file'], $id, $uid);

            // return success message
            // ----------------------
            $msg = array('success', CommonHelper::getMessage('leave_success_attach'));
        }
        return $msg;
    }

    // get total leave attachment
    // --------------------------
    public function getTotalLeaveAttachment($id, $uid)
    {
        $q = \IhrV2\Models\LeaveAttachment::where(
            [
                'leave_id' => $id,
                'user_id'  => $uid
            ]
        )
            ->count();
        return $q;
    }

    // upload leave attachment
    // -----------------------
    public function getUploadLeaveAttachment($data, $id, $uid)
    {

        // update current attachment inactive
        // ----------------------------------
        $this->db_leave_repo->dbUpdateLeaveAttachment($id, $uid);

        // check if have attachment
        // ------------------------
        if (!empty($data['leave_file'])) {

            // upload file attachment
            // ----------------------
            $this->upload_repo->uploadFileLeave($data['leave_file'], $id, $uid);
        }
        return true;
    }

    // check if have reimburse or deduct
    // type_id 1 = reimburse | 2 = deduct
    // ---------------------------------
    public function getCheckReimburseDeduct($uid, $leave_type_id, $date_from, $date_to)
    {

        // check leave add
        // ---------------
        $check = \IhrV2\Models\LeaveAdd::selectRaw('type_id, sum(total) as total')
            ->where([
                'user_id'       => $uid,
                'leave_type_id' => $leave_type_id,
                'status'        => 1
            ])
            ->whereDate('action_date', '>=', $date_from)
            ->whereDate('action_date', '<=', $date_to)
            ->groupBy('type_id')
            ->get();

        // initialize total
        // ----------------
        $reimburse = 0;
        $deduct = 0;

        // if have reimburse/deduct
        // ------------------------
        if (!empty($check)) {
            foreach ($check as $ex) {

                // total reimburse
                // ---------------
                if ($ex->type_id == 1) {
                    $reimburse = $ex->total;
                }

                // total deduct
                // ------------
                if ($ex->type_id == 2) {
                    $deduct = $ex->total;
                }
            }
        }

        // return value
        // ------------
        return array('reimburse' => $reimburse, 'deduct' => $deduct);
    }

    // get all reimburse and deduct (date is within the current contract)
    // ------------------------------------------------------------------
    public function getLeaveAddAll($arr)
    {
        $q = \IhrV2\Models\LeaveAdd::where(
            [
                'user_id'  => $arr[0],
                'sitecode' => $arr[1],
                'status'   => 1
            ]
        )
            ->whereDate('action_date', '>=', $arr[2])
            ->whereDate('action_date', '<=', $arr[3])
            ->get();
        return $q;
    }

    // get lists of reimburse or deduct (site supervisor)
    // --------------------------------------------------
    public function getLeaveReimburseDeductList($arr)
    {
        $q = \IhrV2\Models\LeaveAdd::where(
            [
                'user_id'       => $arr[0],
                'sitecode'      => $arr[1],
                'leave_type_id' => $arr[2],
                'type_id'       => $arr[3],
                'status'        => 1
            ]
        )
            ->whereDate('action_date', '>=', $arr[4])
            ->whereDate('action_date', '<=', $arr[5])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // calculate total leave with extra
    // --------------------------------
    public function getCalculateTotalExtra($total, $reimburse, $deduct)
    {
        return ($total + $reimburse) - $deduct;
    }

    // get total unplan leave
    // ----------------------
    public function getChecktotalUnplan($uid, $sitecode, $leave_type_id, $date_from, $date_to)
    {
        $cond = array($uid, $sitecode, $leave_type_id, $date_from, $date_to);
        $query = \IhrV2\Models\LeaveApprove::whereHas(
            'LeaveInfo',
            function ($x) use ($cond) {
                $x->where([
                    'user_id'       => $cond[0],
                    'sitecode'      => $cond[1],
                    'leave_type_id' => $cond[2],
                    'active'        => 1
                ]);
                $x->whereDate('date_from', '>=', $cond[3]);
            }
        )
            ->where([
                'user_id'       => $uid,
                'leave_type_id' => $leave_type_id,
                'flag'          => 1
            ])
            ->count();
        return $query;
    }

    // check before apply leave (site supervisor)
    // check_type 1: when user want tp apply leave
    // check_type 2: view leave info by rm or clerk
    // --------------------------------------------
    public function getCheckApplyLeave($leave_type_id, $user_id, $sitecode)
    {
        // get leave type info
        // -------------------
        $leave_type = $this->getLeaveTypeByID($leave_type_id);

        // prepare variables
        // -----------------
        $status = 0;
        $entitled_empty = 0;
        $balance_empty = 0;
        $contract_empty = 0;
        $minus = 0;
        $limit = 0;
        $over = 0;

        // only have start date if choose this leave
        // -----------------------------------------
        // haji - 40 days
        // umrah - 10 days
        // maternity - 60 days
        // paternity - 3 days
        // prolong illness - 90 days

        // check if no end date
        // --------------------
        $to = 0;
        if (in_array($leave_type_id, $this->getByPassEndCont())) {
            $to = 1;
        }

        // get user info
        // -------------
        $user = $this->user_repo->getUserByIDSitecode($user_id, $sitecode);

        // check user job
        // --------------
        if (!empty($user->UserLatestJob)) {
            $status_job = 1;
        }

        // check user contract
        // -------------------
        if (!empty($user->UserLatestContract)) {
            $status_contract = 1;
        }

        // user job and contract exist
        // ---------------------------
        if ($status_job == 1 && $status_contract == 1) {

            // get user contract
            // -----------------
            $contract = $user->UserLatestContract;

            // set leave type Unplan Leave as Annual Leave
            // -------------------------------------------
            if ($leave_type_id == 13) {

                // get total taken unplan
                // ----------------------
                $limit = $this->getTakenUnplan($user_id, $sitecode, $leave_type_id, $contract);

                // revert type id unplan
                // ---------------------
                $leave_type_id = 1;
            }

            // taken unplan less than 3
            // ------------------------
            if ($limit != 1) {

                // get total entitled leave
                // ------------------------
                $entitled = $this->getEntitledLeave($user_id, $sitecode, $leave_type_id, $contract);

                // total leave is 0
                // ----------------
                if ($entitled < 1) {
                    $entitled_empty = 1;
                }

                // get total taken leave
                // ---------------------
                $taken = $this->getTakenLeave($user_id, $sitecode, $leave_type_id, $contract);

                // check if contract 1 year 
                // 1-6 month half AL
                // 7-12 month full AL
                // ------------------
                // if ($entitled >= 12) {
                //     $entitled = $this->getCheckAvailAL($contract->date_to, $entitled, $entitled / 2);
                // }

                // leave entitled and leave taken is same (X.X). no balance leave
                // --------------------------------------------------------------
                if ($this->getCheckIfTally($entitled, $taken) == 1) {
                    $balance_empty = 1;
                }

                // calculate balance
                // -----------------
                $leave_balance = $this->getCalculateBalance($entitled, $taken);

                // check if balance is minus
                // -------------------------
                if ($leave_balance < 0) {
                    $minus = 1;
                }

                // check leave balance (all leave types)
                // -------------------------------------
                if ($entitled_empty != 1 && $balance_empty != 1 && $minus != 1) {

                    // check leave balance
                    // -------------------
                    $status = 1;
                    $check_balance = $this->getLeaveBalance($user_id, $leave_type_id, $contract->id);
                    if ($check_balance) {

                        // if not tally update balance in leave_balances
                        // ---------------------------------------------
                        if ($this->getCheckIfTally($check_balance->balance, $leave_balance) != 1) {
                            $this->db_leave_repo->dbUpdateLeaveBalance($user_id, $leave_type_id, $contract->id, $leave_balance);
                        }
                    }
                }

                // set error message
                // -----------------
                else {

                    // entitled is empty
                    // -----------------
                    if ($entitled_empty == 1) {
                        $msg = CommonHelper::getMessage('leave_empty_entitled');
                    }

                    // balance is empty
                    // ----------------
                    if ($balance_empty == 1) {
                        $msg = CommonHelper::getMessage('leave_empty_balance');
                    }

                    // balance less than 0
                    // -------------------
                    if ($minus == 1) {
                        $msg = CommonHelper::getMessage('leave_minus_balance');
                    }
                }
            }

            // unplan reach limit
            // ------------------
            else {

                // unplan taken already 3 times
                // ----------------------------
                if ($limit == 1) {
                    $msg = CommonHelper::getMessage('leave_max_unplan');
                }
            }
        }

        // user job/contract not exist
        // ---------------------------
        else {
            $msg = 'Staff Job/Contract is Inactive.';
        }

        // pass all conditions
        // -------------------
        if ($status == 1) {

            // set values into array
            // ---------------------
            $data = array(
                'status'        => $status,
                'leave_type'    => $leave_type,
                'leave_total'   => $entitled,
                'leave_balance' => $leave_balance,
                'leave_taken'   => $taken,
                'to'            => $to
            );
            return $data;
        }

        // return result
        // -------------
        return array('status' => $status, 'msg' => $msg);
    }

    // get total taken unplan leave
    // ----------------------------
    public function getTakenUnplan($uid, $sitecode, $leave_type_id, $contract)
    {
        // initial value
        // -------------
        $limit = 0;

        // check taken unplan leave
        // ------------------------
        $total_unplan = $this->getChecktotalUnplan($uid, $sitecode, $leave_type_id, $contract->date_from, $contract->date_to);

        // get total unplan limit
        // ----------------------
        $total_limit = 3;

        // check unplan limit
        // ------------------
        if ($total_unplan >= $total_limit) {
            $limit = 1;
        }

        // return result
        // -------------
        return $limit;
    }

    // check calendar status
    // ---------------------
    public function getCalendarStatus($year)
    {
        $q = \IhrV2\Models\Calendar::where(['year' => $year, 'status' => 1])->first();
        return $q;
    }

    // apply leave (site supervisor)
    // -----------------------------
    public function getApplyLeave($data)
    {
        $msg = array();
        $invalid = 0;   // 1 = date range is incorrect
        $balance = 0;   // 1 = no balance or equal entitled
        $more = 0;      // 1 = total apply leave more than entitled
        $success = 0;   // 1 = success insert new leave
        $empty = 0;     // 1 = entitled is 0
        $over = 0;      // 1 = selected date is more balance leave
        $xrm = 0;       // 1 = no rm
        $avail = 0;     // 1 = check total available dates
        $reimburse = 0; // 1 = extra leave
        $deduct = 0;    // 1 = deduct leave
        $expired = 0;   // 1 = check if end date over end contract
        $unplan = 0;    // 1 = select unplan leave
        $partner = 0;   // 1 = falls on partner leave
        $less = 0;      // 1 = start date less than start contract
        $xcalendar = 0; // 1 = calendar not exist
        $xsame = 0;

        // get current year
        // ----------------
        $year = date('Y');

        // get user session
        // ----------------
        $user_id = auth()->user()->id;
        $sitecode = auth()->user()->sitecode;
        $leave_type_id = $data['leave_type_id'];
        $attach = array();

        // get contract info
        // -----------------
        $contract = session()->get('contract');

        // set leave type Unplan Leave as Annual Leave
        // -------------------------------------------
        if ($leave_type_id == 13) {
            $unplan = 1;
            $leave_type_id = 1;
        }

        // convert date
        // ------------
        $from = Carbon::createFromFormat('d/m/Y', $data['date_from'])->format('Y-m-d');

        // check date to
        // -------------
        if (empty($data['date_to'])) {

            // get leave type
            // --------------
            $leave_type = $this->getLeaveTypeName($leave_type_id);

            // automatic check date_to (haji/umrah/maternity/paternity/marriage) it must consistent
            // ------------------------------------------------------------------------------------
            $to = Carbon::createFromFormat('d/m/Y', $data['date_from'])->addDays($leave_type->total)->subDay(1)->toDateTimeString();
        }

        // have date to
        // ------------
        else {
            $to = Carbon::createFromFormat('d/m/Y', $data['date_to'])->format('Y-m-d');
        }

        // check year
        // ----------
        if (in_array($leave_type_id, array(1,6)) && $unplan != 1) {

            // check calendar status
            // ---------------------
            $calendar = $this->getCalendarStatus($year);
            if (empty($calendar)) {
                $xcalendar = 1;
            }

            // both date year is same
            // ----------------------
            if (Carbon::parse($from)->year == Carbon::parse($to)->year) {
                $year = Carbon::parse($from)->year;
            }

            // date year is not same
            // ---------------------
            else {
                $xsame = 1;
            }
        }

        // check if date end over end contract
        // exception for leave type umrah/haji/maternity/paternity
        // -------------------------------------------------------
        if (Carbon::parse($to)->gt(Carbon::parse($contract->date_to)) && !in_array($leave_type_id, $this->getByPassEndCont())) {
            $expired = 1;
        }

        // check if start date less than start contract
        // --------------------------------------------
        if (Carbon::parse($from)->lt(Carbon::parse($contract->date_from))) {
            $less = 1;
        }

        // check if date from is less than or equal date to
        // ------------------------------------------------
        if (Carbon::parse($from)->lte(Carbon::parse($to)) && $xcalendar != 1 && $xsame != 1 && $expired != 1 && $less != 1) {

            // get date range
            // --------------
            $duration = $this->DateRange($from, $to);

            // get total entitled
            // ------------------
            $entitled = $this->getEntitledLeave($user_id, $sitecode, $leave_type_id, $contract);

            // annual leave is 0 when contract date is short / total at leave type is 0
            // ------------------------------------------------------------------------
            if ($entitled == 0 || $entitled == 0.0) {
                $empty = 1;
            }

            // total days is more than entitled
            // --------------------------------
            if (count($duration) > $entitled) {
                $more = 1;
            }

            // apply dates is not exceed entitled and have entitled value
            // ----------------------------------------------------------
            if ($more != 1 && $empty != 1) {

                // get total taken
                // ---------------
                $taken = $this->getTakenLeave($user_id, $sitecode, $leave_type_id, $contract);

                // check entitled and taken
                // ------------------------
                if ($this->getCheckIfTally($entitled, $taken) == 1) {

                    // no balance left (equal)
                    // -----------------------
                    $balance = 1;
                }

                // calculate balance
                // -----------------
                $bal = $this->getCalculateBalance($entitled, $taken);

                // check if half day
                // -----------------
                if (!empty($data['is_half_day']) && count($duration) == 1) {
                    $total = 0.5;
                } else {
                    $total = count($duration);
                }

                // check if apply days more than balance
                // -------------------------------------
                if ($total > $bal) {
                    $over = 1;
                }
            }

            // check if partner already take the leave (AL/RL)
            // -----------------------------------------------
            if ($balance != 1 && $over != 1 && in_array($leave_type_id, array(1,6)) && $unplan != 1) {

                // get position id
                // ---------------
                $position_id = session()->get('job')['position_id'];

                // check partner leave
                // -------------------
                $partner = $this->getPartnerLeave($user_id, $sitecode, $position_id, $duration);
            }

            // have balance and apply days is not more
            // ---------------------------------------
            if ($more != 1 && $balance != 1 && $over != 1 && $partner != 1) {

                // check leave_balances
                // --------------------
                $check_bal = $this->getLeaveBalance($user_id, $leave_type_id, $contract->id);
                if ($check_bal) {

                    // update the balance if leave_balances is not tally
                    // -------------------------------------------------
                    if ($this->getCheckIfTally($check_bal->balance, $bal) != 1) {
                        $this->db_leave_repo->dbUpdateLeaveBalance($user_id, $leave_type_id, $contract->id, $bal);
                    }
                }

                // no leave_balances
                // -----------------
                else {

                    // insert leave_balances
                    // ---------------------
                    $this->db_leave_repo->dbInsertLeaveBalance($user_id, $leave_type_id, $entitled, $contract->id);
                }

                // check if half day
                // -----------------
                if (!empty($data['is_half_day']) && count($duration) == 1) {
                    $is_half_day = $data['is_half_day'];
                } 
                else {
                    $is_half_day = 0;
                }

                // check rm info
                // -------------
                if (!empty(session()->get('rm_info'))) {

                    // get rm info
                    // -----------
                    $report_to = session()->get('rm_info');

                    // insert leave_applications
                    // -------------------------
                    $arr_leave = array(
                        'user_id'       => $user_id,
                        'contract_id'   => $contract->id,
                        'leave_type_id' => ($unplan == 1) ? 13 : $leave_type_id,
                        'report_to'     => $report_to['id'],
                        'date_from'     => $from,
                        'date_to'       => $to,
                        'is_half_day'   => $is_half_day,
                        'desc'          => $data['desc'],
                        'sitecode'      => $sitecode
                    );
                    $leave_id = $this->db_leave_repo->dbInsertLeaveApplication($arr_leave);

                    // get state id
                    // ------------
                    $state = $this->getUserStateID($sitecode);
                    $state_id = $state->state_id;

                    $s_p = array();
                    $o_d = array();
                    $off_dates = array();

                    // get public date of current state
                    // --------------------------------
                    $p = $this->getPublicDateList($state_id, $year);
                    if ($p) {
                        foreach ($p as $date) {
                            $s_p[] = $date->date;
                        }
                    }

                    // check change work hour
                    // ----------------------
                    $change = $this->getChangeWorkHour($user_id, $sitecode, $contract->id);
                    if (!empty($change)) {

                        // get change work hour off days
                        // -----------------------------
                        if (!empty($change->OffDates)) {
                            $off_dates = $change->OffDates;
                        }

                        // check if have partner
                        // ---------------------
                        if ($change->partner == 1) {

                            // get off days id partner
                            // -----------------------
                            if ($position_id == 4) {
                                $types = $this->getOffDayID(5);
                            }                            
                            else {
                                $types = $this->getOffDayID(4);                                
                            } 

                            // get off days partner
                            // --------------------
                            $off_partner[] = $this->getOffDateList($state_id, $types, $year);

                            // merge off days
                            // --------------
                            $off_dates = array_merge($off_dates->toArray(), $off_partner[0]->toArray());                            
                        }                    
                    }

                    // not change work hour (get both off days)
                    // ----------------------------------------
                    else  {
                        $types = $this->getOffDayTypeIDAll();
                        $off_dates = $this->getOffDateList($state_id, $types, $year)->toArray();
                    }

                    // put off days in array
                    // ---------------------
                    if (!empty($off_dates)) {
                        foreach ($off_dates as $off) {
                            $o_d[] = $off['off_date'];
                        }
                    }

                    // loop apply dates
                    // ----------------
                    foreach ($duration as $date) {

                        // set these leave type dates available
                        // mc/dl/cl/hosp/hj/um/mt/pt/mar/unplan
                        // ------------------------------------
                        if (in_array($leave_type_id, $this->getLeaveTypeAvail()) || $unplan == 1) {
                            $type_id = 1; // available
                            $avail++;
                        }

                        // others leave type
                        // -----------------
                        else {

                            // whether date is public holiday or off date set status 0
                            // -------------------------------------------------------
                            if (in_array($date, $s_p)) {
                                $type_id = 7; // public holiday
                            }

                            // get type id of off days
                            // -----------------------
                            else if (in_array($date, $o_d)) {

                                // get off day group id
                                // --------------------
                                $group = $this->getOffDayGroup($state_id);

                                // get off day type
                                // ----------------
                                $check_type = $this->getOffDayTypeID($date, $year, $group->group_id);

                                // have off day type id
                                // --------------------
                                if (!empty($check_type)) {
                                    $type_id = $check_type->type_id; // off day id (2-X)
                                } 
                                else {
                                    $type_id = 0;
                                }
                            } 
                            else {
                                $type_id = 1; // available
                                $avail++;
                            }
                        }

                        // check leave dates
                        // -----------------
                        $chk_ldate = $this->getLeaveDateOne($user_id, $leave_id, $date, $type_id);

                        // no record yet
                        // -------------
                        if (empty($chk_ldate)) {

                            // insert leave dates
                            // ------------------
                            $this->db_leave_repo->dbInsertLeaveDate($user_id, $leave_id, $date, $type_id);
                        }
                    }

                    // check leave histories
                    // ---------------------
                    $chk_history = $this->getCheckLeaveHistory($user_id, $leave_id, 1);
                    if (empty($chk_history)) {

                        // insert leave_histories
                        // ----------------------
                        $this->db_leave_repo->dbInsertLeaveHistory($user_id, $leave_id);
                    }

                    // insert leave_histories for no available dates (automatically rejected)
                    // ----------------------------------------------------------------------
                    if ($avail == 0) {

                        // inactive current history
                        // ------------------------
                        $this->db_leave_repo->dbUpdateLeaveHistory($user_id, $leave_id);

                        // insert rejected history
                        // -----------------------
                        $this->db_leave_repo->dbInsertLeaveHistoryByStatus($user_id, $leave_id, $report_to['id'], 3);
                    }

                    // check if have attachment
                    // ------------------------
                    if (!empty($data['leave_file'])) {

                        // upload file attachment
                        // ----------------------
                        $attach = $this->upload_repo->uploadFileLeave($data['leave_file'], $leave_id, $user_id);
                    }

                    // return success
                    // --------------
                    $success = 1;
                }

                // rm is not exist
                // ---------------
                else {
                    $xrm = 1;
                }
            }
        }

        // date range is not valid
        // -----------------------
        else {
            $invalid = 1;
        }

        // return message error
        // --------------------
        if ($invalid == 1) {
            $msg = array(CommonHelper::getMessage('leave_invalid_date'), 'danger', 'sv.leave.select');
        }
        if ($empty == 1) {
            $msg = array(CommonHelper::getMessage('leave_empty_entitled'), 'danger', 'sv.leave.select');
        }
        if ($more == 1) {
            $msg = array(CommonHelper::getMessage('leave_less_entitled'), 'danger', 'sv.leave.select');
        }
        if ($balance == 1) {
            $msg = array(CommonHelper::getMessage('leave_empty_balance'), 'danger', 'sv.leave.select');
        }
        if ($over == 1) {
            $msg = array(CommonHelper::getMessage('leave_less_balance'), 'danger', 'sv.leave.select');
        }
        if ($expired == 1) {
            $msg = array(CommonHelper::getMessage('leave_end_expired'), 'danger', 'sv.leave.select');
        }
        if ($less == 1) {
            $msg = array(CommonHelper::getMessage('leave_out_contract'), 'danger', 'sv.leave.select');
        }
        if ($xrm == 1) {
            $msg = array(CommonHelper::getMessage('leave_no_rm'), 'danger', 'sv.leave.select');
        }
        if ($partner == 1) {
            $msg = array(CommonHelper::getMessage('leave_on_partner'), 'danger', 'sv.leave.select');
        }
        if ($xcalendar == 1) {
            $msg = array('Calendar of selected date is not Exist. Please contact HR.', 'danger', 'sv.leave.select');
        }
        if ($xsame == 1) {
            $msg = array('Both date range is within different year. Please separate the date.', 'danger', 'sv.leave.select');
        }

        // return message success
        // ----------------------
        if ($success == 1) {
            $send = 0;

            // if there's available dates
            // --------------------------
            if ($avail > 0) {

                // have rm email
                // -------------
                if (!empty($report_to['email'])) {
                    $send = 1;

                    // generate url (leave id + user id + sitecode + rm id + user token)
                    // -----------------------------------------------------------------
                    $com = $leave_id . '/';
                    $com .= $user_id . '/';
                    $com .= $sitecode . '/';
                    $com .= $report_to['id'];
                    $url = Crypt::encrypt($com) . '?api_token=' . auth()->user()->api_token;

                    // get leave info
                    // --------------
                    $leave = $this->getLeaveDetail($leave_id);

                    // email body info
                    // ---------------
                    $arr_body = array(
                        'position_name' => session()->get('position_name'),
                        'site_name'     => session()->get('site_name'),
                        'leave'         => $leave,
                        'available'     => $avail,
                        'approve'       => route('api.leave.approve', array($url)),
                        'reject'        => route('api.leave.reject', array($url))
                    );

                    // receiver info
                    // -------------
                    $arr = array(
                        'leave_name' => $leave->LeaveTypeName->name,
                        'rm_email'   => $report_to['email'],
                        'rm_name'    => $report_to['name'],
                        'attach'     => $attach
                    );

                    // send email to rm
                    // ----------------
                    $this->email_repo->getEmailLeaveApply($arr_body, $arr);
                }

                // email rm is empty
                // -----------------
                else {
                    $msg = array(CommonHelper::getMessage('leave_fail_email'), 'success', 'sv.leave.index');
                }
            }

            // no available date
            // -----------------
            else {
                $msg = array(CommonHelper::getMessage('leave_empty_date'), 'danger', 'sv.leave.index');
            }

            // have rm email and available date
            // --------------------------------
            if ($send == 1 && $avail > 0) {
                $msg = array(CommonHelper::getMessage('leave_success_apply'), 'success', 'sv.leave.index');
            }
        }

        // return result
        // -------------
        return $msg;
    }

    // get change work hour
    // --------------------
    public function getChangeWorkHour($uid, $sitecode, $contract_id)
    {
        $q = \IhrV2\Models\WorkHour::where([
            'user_id' => $uid, 
            'sitecode' => $sitecode, 
            'contract_id' => $contract_id,
            'status' => 1
        ])
        ->first();
        return $q;
    }

    // get change work hour by sitecode
    // --------------------------------
    public function getWorkHourBySitecode($sitecode)
    {
        $q = \IhrV2\Models\WorkHour::where(['sitecode' => $sitecode, 'partner' => 1, 'status' => 1])->first();
        return $q;
    }

    // check leave date
    // ----------------
    public function getLeaveDateOne($user_id, $leave_id, $date, $type_id)
    {
        $q = \IhrV2\Models\LeaveDate::where(
            [
                'user_id'    => $user_id,
                'leave_id'   => $leave_id,
                'leave_date' => $date,
                'leave_type' => $type_id,
                'status'     => 1
            ]
        )
            ->first();
        return $q;
    }

    // get leave info
    // --------------
    public function getLeaveDetail($id)
    {
        // return \IhrV2\Models\LeaveApplication::find($id);
        return \IhrV2\Models\LeaveApplication::where('id', $id)
            ->with(array('LeaveTypeName'))
            ->first();
    }

    // get leave info
    // --------------
    public function getCheckLeaveInfo($id, $uid, $sitecode)
    {
        return \IhrV2\Models\LeaveApplication::where('id', $id)
            ->where('user_id', $uid)
            ->where('sitecode', $sitecode)
            ->where('active', 1)
            ->with(array('LeaveLatestAttachment'))
            ->firstOrFail();
    }

    // get rl info
    // -----------
    public function getCheckLeaveRepInfo($id, $uid, $sitecode)
    {
        return \IhrV2\Models\LeaveRepApplication::where('id', $id)
            ->where('user_id', $uid)
            ->where('sitecode', $sitecode)
            ->where('active', 1)
            ->with(array('LeaveRepLatestAttachment'))
            ->firstOrFail();
    }

    // get one clerk region
    // --------------------
    public function getClerkRegionList($cond)
    {
        $q = \IhrV2\User::whereHas(
            'UserLatestJob',
            function ($clk) use ($cond) {
                foreach ($cond as $column => $key) {
                    if (!is_null($key)) {
                        $clk->where($column, $key);
                    }
                }
            }
        )
            ->where('group_id', 7)
            ->where('status', 1)
            ->first();
        return $q;
    }

    // explode multiple characters in a string
    // ---------------------------------------
    public function multiExplode($delimiters, $string)
    {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return $launch;
    }

    // get off day type id
    // -------------------
    public function getOffDayTypeID($date, $year, $group_id)
    {
        $check_type = \IhrV2\Models\LeaveOffDate::whereDate('off_date', '=', $date)
            ->where('year', $year)
            ->where('group_id', $group_id)
            ->where('status', 1)
            ->first();
        return $check_type;
    }

    public function getOffDateListAll($state_id, $year)
    {
        $q = \IhrV2\Models\LeaveOffDate::whereHas(
            'ListStates',
            function ($i) use ($state_id) {
                $i->where('state_id', $state_id);
            }
        )
            ->where('year', $year)
            ->where('status', 1)
            ->get();
        return $q;
    }

    // get group id off day
    // --------------------
    public function getOffDayGroup($state_id)
    {
        return \IhrV2\Models\LeaveOffState::where('state_id', $state_id)->first();
    }

    public function getOffDateList($state_id, $types, $year)
    {
        $off_dates = \IhrV2\Models\LeaveOffDate::whereHas(
            'ListStates',
            function ($q) use ($state_id) {
                $q->where('state_id', $state_id);
            }
        )
            ->where('year', $year)
            ->whereIn('type_id', $types)
            ->where('status', 1)
            ->get();
        return $off_dates;
    }

    public function getOffDateListMonth($state_id, $types, $year, $month)
    {
        $off_dates = \IhrV2\Models\LeaveOffDate::whereHas(
            'ListStates',
            function ($q) use ($state_id) {
                $q->where('state_id', $state_id);
            }
        )
            ->with(array('OffTypeName'))
            ->where('year', date('Y'))
            ->whereMonth('off_date', '=', $month)
            ->whereIn('type_id', $types)
            ->where('status', 1)
            ->orderBy('off_date', 'asc')
            ->get()
            ->toArray();
        return $off_dates;
    }

    public function getPublicDateListAll($state_id, $year)
    {
        $q = \IhrV2\Models\LeavePublicState::groupBy('date')
            ->whereYear('date', '=', $year)
            ->where('year', $year)
            ->where('state_id', $state_id)
            ->where('status', 1)
            ->get();
        return $q;
    }

    // get public holiday dates
    // ------------------------
    public function getPublicDateList($state_id, $year)
    {
        $p = \IhrV2\Models\LeavePublicState::where('state_id', $state_id)
            ->where('year', $year)
            ->where('status', 1)
            ->get();
        return $p;
    }

    public function getPublicDateMonthYear($state_id, $year, $month)
    {
        $p = \IhrV2\Models\LeavePublicState::where('state_id', $state_id)
            ->whereYear('date', '=', $year)
            ->whereMonth('date', '=', $month)
            ->where('year', $year)
            ->where('status', 1)
            ->get()->toArray();
        return $p;
    }

    // get list of public holiday for site supervisor
    // ----------------------------------------------
    public function getPublicDateListName($state_id, $year)
    {
        $q = \IhrV2\Models\LeavePublicState::with(array('PublicName'))
            ->where([
                'year'     => $year,
                'state_id' => $state_id,
                'status'   => 1
            ])
            ->orderBy('date', 'asc')
            ->get();
        return $q;
    }

    // get leave info by user
    // ----------------------
    public function getLeaveByUser($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\LeaveApplication::with(array('LeavePrevHistory' =>
        function ($h) {
            $h->with(array('LeaveStatusName', 'LeaveActionByName'));
        }))
        ->with(array('LeaveLatestHistory' => function ($l) {
            $l->with(array('LeaveStatusName', 'LeaveActionByName'));
        }))
        ->with(array('LeaveUserDetail' => function ($d) {
            $d->with(array('UserLatestJob'));
        }))
        ->with(array(
            // 'LeaveReportToName',
            'LeaveLatestAttachment',
            // 'LeaveDate',
            'LeaveDateAvail',
            'LeaveDateAll',
            'LeaveSiteName',
            'LeaveTypeName'
        ))
        ->where([
            'id'       => $id,
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'active'   => 1
        ])
        ->first();
        return $q;
    }

    // get total entitled leave
    // ------------------------
    public function getEntitledLeave($uid, $sitecode, $leave_type_id, $contract)
    {
        // check entitled AL
        // -----------------
        if ($leave_type_id == 1) {
            $al_entitled = $this->getTotalALByJoinDate($uid, $sitecode, $contract->date_from, $contract->date_to);
            $entitled = $al_entitled['total'];
        }

        // check entitled RL
        // -----------------
        elseif ($leave_type_id == 6) {
            $rl_entitled = $this->getLeaveRepEntitled($uid, $sitecode, $contract->id, $contract->date_from, $contract->date_to);
            $entitled = ($rl_entitled) ? $rl_entitled->total : 0;
        }

        // check entitled others
        // ---------------------
        else {
            $leave_type = $this->getLeaveTypeByID($leave_type_id);
            $entitled = $leave_type->total;
        }

        // check reimburse/deduct leave
        // ----------------------------
        $rd = $this->getCheckReimburseDeduct($uid, $leave_type_id, $contract->date_from, $contract->date_to);

        // calculate total leave
        // ---------------------
        $fentitled = $this->getCalculateTotalExtra($entitled, $rd['reimburse'], $rd['deduct']);

        // return total entitled
        // ---------------------
        return $fentitled;
    }

    // get total taken leave
    // ---------------------
    public function getTakenLeave($uid, $sitecode, $leave_type_id, $contract)
    {
        // have leave taken
        // ----------------
        $check_taken = $this->getLeaveTaken($uid, $sitecode, $leave_type_id, $contract->date_from, $contract->date_to);
        $taken = ($check_taken) ? $check_taken->total : 0;

        // check if have unplan leave
        // --------------------------
        if ($leave_type_id == 1) {

            // get total unplan
            // ----------------
            $check_unplan = $this->getLeaveTaken($uid, $sitecode, 13, $contract->date_from, $contract->date_to);

            // have unplan leave
            // -----------------
            $ftaken = $taken;
            if (!empty($check_unplan)) {
                $ftaken = $taken + $check_unplan->total;
            }
        }

        // other leave taken
        // -----------------
        else {
            $ftaken = $taken;
        }

        // return total taken
        // ------------------
        return $ftaken;
    }

    // get partner leave
    // -----------------
    public function getPartnerLeave($uid, $sitecode, $position_id, $dates)
    {
        // initial value
        // -------------
        $partner = 0;

        // get partner info
        // ----------------
        $get_partner = $this->user_repo->getUserPartner($uid, $sitecode, $position_id);

        // have partner
        // ------------
        if (!empty($get_partner)) {

            // check if this partner apply leave on the date
            // ---------------------------------------------
            $leave_partner = $this->getCheckLeavePartner($get_partner->id, $sitecode, $dates);

            // leave exist (approved AL/RL leave)
            // ----------------------------------
            if (!empty($leave_partner)) {
                $partner = 1;
            }
        }

        // return output
        // -------------
        return $partner;
    }

    // process approve leave
    // ---------------------
    public function getApproveLeave($id, $uid, $sitecode, $data)
    {
        // initial value
        // -------------
        $empty = 0;           // 1 = entitled leave is empty
        $xbalance = 0;        // 1 = no balance
        $over = 0;            // 1 = apply day is more
        $success = 0;         // 1 = leave successfully approved
        $unplan = 0;          // 1 = apply unplan leave
        $partner = 0;         // 1 = partner on leave

        // get record from array
        // ---------------------
        $leave = $data['leave'];
        $job = $data['user']->UserLatestJob;
        $contract = $data['user']->UserLatestContract;

        // get leave type id
        // -----------------
        $leave_type_id = $leave->leave_type_id;

        // set leave type Unplan Leave as Annual Leave
        // -------------------------------------------
        if ($leave_type_id == 13) {
            $leave_type_id = 1;
            $unplan = 1;
        }

        // get entitled leave
        // ------------------
        $entitled = $this->getEntitledLeave($uid, $sitecode, $leave_type_id, $contract);

        // entitled leave is empty
        // -----------------------
        if ($entitled == 0 || $entitled == 0.0) {
            $empty = 1;
        }

        // check leave taken
        // -----------------
        if ($empty != 1) {

            // get total taken leave
            // ---------------------
            $taken = $this->getTakenLeave($uid, $sitecode, $leave_type_id, $contract);

            // check if entitled and taken same
            // --------------------------------
            if ($this->getCheckIfTally($entitled, $taken) == 1) {

                // no balance (empty)
                // ------------------
                $xbalance = 1;
            }

            // calculate balance
            // -----------------
            $bal = $this->getCalculateBalance($entitled, $taken);

            // get total leave date
            // --------------------
            $check_total = $this->getTotalLeaveDate($id, $uid);

            // check if half day
            // -----------------
            $total = ($check_total == 1 && $leave->is_half_day == 1) ? 0.5 : $check_total;

            // check if apply days more than balance
            // -------------------------------------
            if ($total > $bal) {
                $over = 1;
            }

            // check if partner already apply the leave (AL/RL)
            // ------------------------------------------------
            if ($xbalance != 1 && $over != 1 && in_array($leave_type_id, array(1,6)) && $unplan != 1) {

                // get position id
                // ---------------
                $position_id = $job->position_id;

                // get available dates
                // -------------------
                $leave_dates = array();
                if (!empty($leave->LeaveDate)) {
                    $leave_dates = $leave->LeaveDate->toArray();
                }

                // get list of dates
                // -----------------
                $dates = array();
                foreach ($leave_dates as $d) {
                    $dates[] = $d['leave_date'];
                }

                // check partner leave
                // -------------------
                $partner = $this->getPartnerLeave($uid, $sitecode, $position_id, $dates);
            }
        }

        // entitled leave is empty
        // -----------------------
        else {
            $msg = array('Entitled Leave is Empty.', 'danger');
        }

        // have balance/apply not more/not fall on partner leave
        // -----------------------------------------------------
        if ($xbalance != 1 && $over != 1 && $partner != 1) {

            // if unplan use leave type id annual
            // ----------------------------------
            if ($unplan == 1) {
                $leave_type_id = 1;
            }

            // check leave balance
            // -------------------
            $check_bal = $this->getLeaveBalance($uid, $leave_type_id, $contract->id);
            if ($check_bal) {

                // calculate new balance
                // ---------------------
                $new_bal = $this->getCalculateBalance($bal, $total);

                // update leave_balances
                // ---------------------
                $this->db_leave_repo->dbUpdateLeaveBalance($uid, $leave_type_id, $contract->id, $new_bal);
            }

            // insert leave_balances
            // ---------------------
            else {
                $this->db_leave_repo->dbInsertLeaveBalance($uid, $leave_type_id, $bal, $contract->id);
            }

            // process leave history
            // ---------------------
            $this->getProcessLeaveHistory($id, $uid, $data);

            // check leave approves
            // --------------------
            $chk_approve = $this->getCheckLeaveApprove($uid, $id);

            // leave approves empty
            // --------------------
            if (empty($chk_approve)) {

                // get leave_type_id
                // -----------------
                if ($unplan == 1) {
                    $ltid = 13;
                } else {
                    $ltid = $leave_type_id;
                }

                // insert leave_approves
                // ---------------------
                $arr = array(
                    'user_id'       => $uid,
                    'leave_id'      => $id,
                    'leave_type_id' => $ltid,
                    'contract_id'   => $contract->id,
                    'date_action'   => date('Y-m-d H:i:s'),
                    'action_by'     => $data['action_by'],
                    'date_value'    => $total,
                    'flag'          => 1
                );
                $this->db_leave_repo->dbInsertLeaveApprove($arr);
            }

            // successfully approve
            // --------------------
            $success = 1;
        }

        // set error message
        // -----------------
        else {

            // entitled leave is empty
            // -----------------------
            if ($xbalance == 1) {
                $msg = array('Entitled Leave currently Empty.', 'danger');
            }

            // apply more day than balance
            // ---------------------------
            if ($over == 1) {
                $msg = array('Total selected days is more than Balance Leave.', 'danger');
            }

            // partner already apply leave
            // ---------------------------
            if ($partner == 1) {
                $msg = array('Partner already apply on that Date.', 'danger');
            }
        }

        // return success
        // --------------
        if ($success == 1) {

            // email body info
            // ---------------
            $arr_body = array(
                'leave'     => $leave,
                'leave_name' => $leave->LeaveTypeName->name,
                'available' => $this->getTotalLeaveDate($id, $uid),
                'status'    => $this->getLeaveStatus($data['type']),
                'remark'    => $data['remark']
            );

            // receiver info
            // -------------
            $arr = array(
                'mgr_name'  => $data['user']->name,
                'mgr_email' => $data['user']->email
            );

            // send email to site supervisor
            // -----------------------------
            $this->email_repo->getEmailLeaveResult($arr_body, $arr);

            // success message
            // ---------------
            $msg = array('Leave successfully Approved!', 'success');
        }

        // return message
        // --------------
        return $msg;
    }

    // process leave history
    // ---------------------
    public function getProcessLeaveHistory($id, $uid, $data)
    {
        // inactive status pending (update current flag to 0)
        // --------------------------------------------------
        $this->db_leave_repo->dbUpdateLeaveHistory($uid, $id);

        // check leave histories
        // ---------------------
        $chk_history = $this->getCheckLeaveHistory($uid, $id, $data['type']);
        if (empty($chk_history)) {

            // insert new leave_histories
            // --------------------------
            $arr = array(
                'user_id'     => $uid,
                'leave_id'    => $id,
                'action_date' => date('Y-m-d H:i:s'),
                'action_by'   => $data['action_by'],
                'remark'      => $data['remark'],
                'status'      => $data['type'],
                'flag'        => 1
            );
            $this->db_leave_repo->dbInsertLeaveHistoryArray($arr);
        }
        return true;
    }

    // process apply cancel leave
    // --------------------------
    public function getApproveCancelLeave($id, $uid, $sitecode, $data)
    {
        // get record from array
        // ---------------------
        $leave = $data['leave'];
        $contract = $data['user']->UserLatestContract;

        // get leave type id
        // -----------------
        $leave_type_id = $leave->leave_type_id;

        // process leave history
        // ---------------------
        $this->getProcessLeaveHistory($id, $uid, $data);

        // get entitled leave
        // ------------------
        $entitled = $this->getEntitledLeave($uid, $sitecode, $leave_type_id, $contract);

        // update leave_approve (set flag = 0)
        // -----------------------------------
        $this->db_leave_repo->dbUpdateLeaveApprove($id, $uid);

        // get total taken leave
        // ---------------------
        $taken = $this->getTakenLeave($uid, $sitecode, $leave_type_id, $contract);

        // calculate balance
        // -----------------
        $bal = $this->getCalculateBalance($entitled, $taken);

        // update leave_balances (update balance)
        // --------------------------------------
        $this->db_leave_repo->dbUpdateLeaveBalance($uid, $leave_type_id, $contract->id, $bal);

        // return result
        // -------------
        return true;
    }

    // site supervisor = cancel leave 5
    // site supervisor = apply cancel 4
    // region manager = approve leave 2
    // region manager = reject leave 3
    // region manager = approve apply cancel 6
    // region manager = reject apply cancel 7
    // --------------------------------------
    public function getProcessLeave($data, $id, $uid, $sitecode, $action_by)
    {
        // set variables
        // -------------
        $type = $data['type'];
        $xleave = 0;
        $pass = 0;
        $status_job = 0;
        $status_contract = 0;
        $expired = 0;
        $email = 0;

        // get leave details
        // -----------------
        $leave = $this->getLeaveByUser($id, $uid, $sitecode);

        // record exist
        // ------------
        if (!empty($leave) && !empty($leave->LeaveLatestHistory)) {

            // approve leave
            // -------------
            if ($type == 2 && $leave->LeaveLatestHistory->status == 1) {
                $pass = 1;
            }

            // reject leave
            // ------------
            if ($type == 3 && $leave->LeaveLatestHistory->status == 1) {
                $pass = 1;
            }

            // apply cancel leave
            // ------------------
            if ($type == 4 && $leave->LeaveLatestHistory->status == 2) {
                $pass = 1;
            }

            // cancel leave
            // ------------
            if ($type == 5 && $leave->LeaveLatestHistory->status == 1) {
                $pass = 1;
            }

            // approve cancel
            // --------------
            if ($type == 6 && $leave->LeaveLatestHistory->status == 4) {
                $pass = 1;
            }

            // reject cancel
            // -------------
            if ($type == 7 && $leave->LeaveLatestHistory->status == 4) {
                $pass = 1;
            }

            // delete leave
            // ------------
            if ($type == 8 && $leave->LeaveLatestHistory->status == 2) {
                $pass = 1;
            }
        }

        // record not exist
        // ----------------
        else {
            $xleave = 1;
        }

        // record exist and valid
        // ----------------------
        if ($xleave != 1 && $pass == 1) {

            // get leave type id
            // -----------------
            $leave_type_id = $leave->leave_type_id;

            // get user info
            // -------------
            $user = $this->user_repo->getUserByIDSitecode($uid, $sitecode);

            // check user job
            // --------------
            if (!empty($user->UserLatestJob)) {
                $status_job = 1;
            }

            // check user contract
            // -------------------
            if (!empty($user->UserLatestContract)) {
                $status_contract = 1;
            }

            // user job and contract exist
            // ---------------------------
            if ($status_job == 1 && $status_contract == 1) {

                // get user contract
                // -----------------
                $contract = $user->UserLatestContract;

                // check contract is not expired
                // -----------------------------
                $expired = $this->CheckIfExpiredLeave($contract->date_to);

                // contract is active
                // ------------------
                if ($expired == 0) {

                    // set action by into array
                    // ------------------------
                    $data['action_by'] = $action_by;

                    // approve leave
                    // -------------
                    if ($type == 2) {

                        // process approve leave
                        // ---------------------
                        $data['leave'] = $leave;
                        $data['user'] = $user;
                        $msg = $this->getApproveLeave($id, $uid, $sitecode, $data);
                    }

                    // approve apply cancel
                    // --------------------
                    elseif ($type == 6) {

                        // process approve apply cancel leave
                        // ----------------------------------
                        $data['leave'] = $leave;
                        $data['user'] = $user;
                        $msg = $this->getApproveCancelLeave($id, $uid, $sitecode, $data);

                        // set status approve
                        // ------------------
                        $msg = array("Cancel successfully Approved!", 'success');
                        $status = 'Approved';
                        $email = 1;
                    }

                    // cancel leave
                    // ------------
                    elseif ($type == 5) {

                        // process leave history
                        // ---------------------
                        $this->getProcessLeaveHistory($id, $uid, $data);

                        // set message
                        // -----------
                        $msg = array('Leave successfully Cancel!', 'success');
                    }

                    // apply cancel
                    // ------------
                    elseif ($type == 4) {

                        // process leave history
                        // ---------------------
                        $this->getProcessLeaveHistory($id, $uid, $data);

                        // email body info
                        // ---------------
                        $arr_body = array(
                            'position_name' => session()->get('position_name'),
                            'site_name'     => session()->get('site_name'),
                            'leave'         => $leave,
                            'leave_name'    => $leave->LeaveTypeName->name,
                            'available'     => $this->getTotalLeaveDate($id, $uid),
                            'remark'        => $data['remark'],
                            'url'           => route('mod.leave.view', array($id, $uid, $sitecode))
                        );

                        // get rm info
                        // -----------
                        $rm = session()->get('rm_info');

                        // receiver info
                        // -------------
                        $arr = array(
                            'rm_email' => $rm['email'],
                            'rm_name'  => $rm['name']
                        );

                        // email notification to rm
                        // ------------------------
                        $this->email_repo->getEmailLeaveCancel($arr_body, $arr);

                        // cancel approved leave message
                        // -----------------------------
                        $msg = array('Cancel is in Progress and send to RM for verification.', 'success');
                    }

                    // reject apply cancel
                    // -------------------
                    elseif ($type == 7) {

                        // process leave history
                        // ---------------------
                        $this->getProcessLeaveHistory($id, $uid, $data);

                        // set message
                        // -----------
                        $status = 'Rejected';
                        $msg = array("Cancel is $status!", 'success');
                        $email = 1;
                    }

                    // reject leave
                    // ------------
                    elseif ($type == 3) {

                        // process leave history
                        // ---------------------
                        $this->getProcessLeaveHistory($id, $uid, $data);

                        // set message
                        // -----------
                        $msg = array('Leave successfully Rejected!', 'success');
                        $email = 1;
                    }

                    // rm cancel leave
                    // ---------------
                    elseif ($type == 8) {

                        // process cancel leave
                        // --------------------
                        $data['leave'] = $leave;
                        $data['user'] = $user;
                        $msg = $this->getApproveCancelLeave($id, $uid, $sitecode, $data);

                        // set message
                        // -----------
                        $msg = array('Leave successfully Canceled!', 'success');
                        $email = 1;
                    }

                    // type is invalid
                    // ---------------
                    else {
                        $msg = array('Operation is Failed.', 'danger');
                    }

                    // send email notification to site supervisor
                    // ------------------------------------------
                    if ($email == 1) {

                        // set email header
                        // ----------------
                        $arr = array(
                            'mgr_name'  => $user->name,
                            'mgr_email' => $user->email
                        );

                        // set email body
                        // --------------
                        $arr_body = array(
                            'leave'     => $leave,
                            'leave_name' => $leave->LeaveTypeName->name,
                            'available' => $this->getTotalLeaveDate($id, $uid),
                            'status'    => $this->getLeaveStatus($type),
                            'remark'    => $data['remark']
                        );

                        // rm reject/cancel the leave
                        // --------------------------
                        if (in_array($type, array(3,8))) {
                            $this->email_repo->getEmailLeaveResult($arr_body, $arr);
                        }

                        // rm approve/reject apply cancel
                        // ------------------------------
                        if ($type == 6 || $type == 7) {
                            $arr['status'] = $status;

                            // get apply cancel history
                            // ------------------------
                            $arr_body['history'] = $this->getLeaveApplyCancelHist($id, $uid, 4);
                            $this->email_repo->getEmailApplyCancelResult($arr_body, $arr);
                        }
                    }
                }

                // contract expired
                // ----------------
                else {
                    $msg = array('Contract is Expired. Please contact HR for further information.', 'danger');
                }
            }

            // user job/contract not exist
            // ---------------------------
            else {
                $msg = array('Staff Job/Contract is Inactive.', 'danger');
            }
        }

        // set error message
        // -----------------
        else {

            // leave not exist
            // ---------------
            if ($xleave == 1) {
                $msg = array('Leave is not Exist.', 'danger');
            }
            if ($pass == 0) {
                $msg = array('Leave already Processed.', 'danger');
            }
        }

        // return result
        // -------------
        return $msg;
    }

    // get apply cancel info
    // ---------------------
    public function getLeaveApplyCancelHist($id, $uid, $status)
    {
        $q = \IhrV2\Models\LeaveHistory::where([
            'leave_id' => $id,
            'user_id'  => $uid,
            'status'   => $status
        ])
            ->first();
        return $q;
    }

    public function getTotalLeavePending()
    {
        $filters = array('status' => 1, 'flag' => 1);
        $leave = \IhrV2\Models\LeaveApplication::whereHas(
            'LeaveLatestHistory',
            function ($q) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $q->where($column, $key);
                    }
                }
            }
        )
            ->where(['active' => 1])
            ->count();
        return $leave;
    }

    public function getTotalLeavePendingHistory()
    {
        $leave = \IhrV2\Models\LeaveHistory::where('status', 1)
            ->where('flag', 1)
            ->where('user_id', '!=', 0)
            ->count();
        return $leave;
    }

    public function getTotalLeavePendingOneHistory($user_id)
    {
        $leave = \IhrV2\Models\LeaveHistory::where('user_id', $user_id)
            ->where('status', 1)
            ->where('flag', 1)
            ->where('user_id', '!=', 0)
            ->count();
        return $leave;
    }

    public function getTotalLeaveRepPending()
    {
        $filters = array('status' => 1, 'flag' => 1);
        $total = \IhrV2\Models\LeaveRepApplication::whereHas(
            'LeaveRepLatestHistory',
            function ($q) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $q->where($column, $key);
                    }
                }
            }
        )
            ->where(['active' => 1])
            ->count();
        return $total;
    }

    public function getTotalLeaveRepPendingOneHistory($user_id)
    {
        $total = \IhrV2\Models\LeaveRepHistory::where('user_id', $user_id)
            ->where('status', 1)
            ->where('flag', 1)
            ->count();
        return $total;
    }

    public function getTotalLeaveRepPendingHistory()
    {
        $total = \IhrV2\Models\LeaveRepHistory::where('status', 1)
            ->where('flag', 1)
            ->count();
        return $total;
    }

    // get all status of leaves
    // ------------------------
    public function getLeaveStatusTotal($cond)
    {
        $q = \IhrV2\Models\LeaveHistory::select(\DB::raw('COUNT(*) as total'), 'status')
            ->whereHas('LeaveApplicationName', function ($r) use ($cond) {
                $r->where([
                    'user_id'  => $cond[0],
                    'sitecode' => $cond[1],
                    'active'   => 1
                ]);
                $r->whereDate('date_from', '>=', $cond[2]);
                // $r->whereHas('LeaveUserContractActive', function($c) use ($cond) {
                //     $c->where([
                //         'user_id' => $cond[0],
                //         'sitecode' => $cond[1],
                //     ]);
                //     $c->whereDate('date_from', '>=', $cond[2]);
                //     $c->whereDate('date_to', '<=', $cond[3]);
                // });
            })
            ->with(array('LeaveStatusName'))
            ->where([
                'user_id' => $cond[0],
                'flag'    => 1
            ])
            ->groupBy('status')
            ->get();
        return $q;
    }

    // get total of each status replacement request
    // --------------------------------------------
    public function getLeaveRepStatusTotal($uid, $sitecode, $date_from, $date_to)
    {
        $from = Carbon::parse($date_from)->format('Y-m') . '-01';
        $cond = array($uid, $sitecode, $from, $date_to);
        $q = \IhrV2\Models\LeaveRepHistory::select(\DB::raw('COUNT(*) as total'), 'status')
            ->whereHas('LeaveRepInfo', function ($r) use ($cond) {
                $r->where([
                    'user_id'  => $cond[0],
                    'sitecode' => $cond[1],
                    'active'   => 1
                ]);
                $r->whereDate('year_month', '>=', $cond[2]);

                // $r->whereHas('LeaveRepContractActive', function($c) use ($cond) {
                //     $c->where([
                //         'user_id' => $cond[0],
                //         'sitecode' => $cond[1],
                //         'status' => 1
                //     ]);
                //     $c->whereDate('date_from', '>=', $cond[2]);
                //     $c->whereDate('date_to', '<=', $cond[3]);
                // });
            })
            ->with(array('LeaveRepStatusName'))
            ->where(['user_id' => $uid, 'flag' => 1])
            ->groupBy('status')
            ->get();
        return $q;
    }

    // get total entitled of replacement leave
    // ---------------------------------------
    public function getLeaveRepEntitled($uid, $sitecode, $contract_id, $date_from, $date_to, $prev = false)
    {
        $from = Carbon::parse($date_from)->format('Y-m') . '-01';
        $cond = array($uid, $sitecode, $contract_id, $from, $date_to, $prev);
        $q = \IhrV2\Models\LeaveRepApprove::selectRaw('SUM(total_day) AS total')
            ->whereHas('LeaveRepInfo', function ($r) use ($cond) {
                $r->where([
                    'user_id'  => $cond[0],
                    'sitecode' => $cond[1],
                    'active'   => 1
                ]);
                if ($cond[5] == true) {
                    $r->whereDate('date_apply', '<=', $cond[4]);
                }
                // year_month mesti bermula dari contract active
                // ---------------------------------------------
                $r->whereDate('year_month', '>=', $cond[3]);
                // renew contract 18 feb 2018
                // apply rl 24 feb 2018
                // 2018-02-01 >= 2018-02-18
                // $r->whereDate('date_apply', '>=', $cond[2]);
            })
            ->where(['user_id' => $uid, 'flag' => 1])
            ->groupBy('user_id')
            ->first();
        return $q;
    }

    // get lists of replacement that approve (site supervisor)
    // -------------------------------------------------------
    public function getLeaveRepApproveList($cond)
    {
        $year_month = Carbon::parse($cond[3])->format('Y-m') . '-01';
        $q = \IhrV2\Models\LeaveRepApplication::has('LeaveRepApprove')
            ->whereDate('year_month', '>=', $year_month)
            ->where([
                'user_id'  => $cond[0],
                'sitecode' => $cond[1],
                'active'   => 1
            ])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get lists of replacement that approve (backend)
    // -----------------------------------------------
    public function getLeaveRepApproveList2($cond)
    {
        $year_month = Carbon::parse($cond[2])->format('Y-m') . '-01';
        $q = \IhrV2\Models\LeaveRepApplication::has('LeaveRepApprove')
            ->whereDate('year_month', '>=', $year_month)
            ->where([
                'user_id'  => $cond[0],
                'sitecode' => $cond[1],
                'active'   => 1
            ])
            // ->whereDate('date_apply', '>=', $cond[2])
            ->whereDate('date_apply', '<=', $cond[3])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get lists of RL request
    // -----------------------
    public function getLeaveRepAllList($uid, $sitecode, $date_from, $date_to)
    {
        $year_month = Carbon::parse($date_from)->format('Y-m') . '-01';
        $q = \IhrV2\Models\LeaveRepApplication::with(array('LeaveRepLatestHistory' =>
            function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->with(array('LeaveRepLatestAttachment'))
            ->whereDate('year_month', '>=', $year_month)
            ->where([
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    // get lists of RL request by contract id
    // --------------------------------------
    public function getLeaveRepByContractID($uid, $sitecode, $contract_id)
    {
        $q = \IhrV2\Models\LeaveRepApplication::with(array('LeaveRepLatestHistory' =>
            function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->with(array('LeaveRepLatestAttachment'))
            ->where([
                'contract_id' => $contract_id,
                'user_id' => $uid,
                'sitecode' => $sitecode,
                'active' => 1
            ])
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    // search  of replacement leave
    // ------------------------------
    public function getLeaveRepAllSearch($cond)
    {
        $q = \IhrV2\Models\LeaveRepApplication::with(array('LeaveRepLatestHistory' =>
            function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->with(array('LeaveRepLatestAttachment'))
            ->where([
                'user_id'     => $cond[0],
                'sitecode'    => $cond[1],
                'contract_id' => $cond[2],
                'active'      => 1
            ])
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    // get lists of replacement leave (inactive staff)
    // -----------------------------------------------
    public function getLeaveRepInactive($cond)
    {
        $q = \IhrV2\Models\LeaveRepApplication::with(array('LeaveRepLatestHistory' =>
            function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->whereHas('LeaveRepContract', function ($c) use ($cond) {
                $c->where([
                    'user_id'  => $cond[0],
                    'sitecode' => $cond[1]
                    // 'status' => 1
                ]);
                // $c->whereDate('date_from', '>=', $cond[2]);
                // $c->whereDate('date_to', '<=', $cond[3]);
            })
            ->where([
                'user_id'  => $cond[0],
                'sitecode' => $cond[1],
                'active'   => 1
            ])
            ->orderBy('id', 'desc')
            ->paginate(10);
        return $q;
    }

    public function getTotalLeaveDate($id, $uid)
    {
        return \IhrV2\Models\LeaveDate::where([
            'leave_id'   => $id,
            'user_id'    => $uid,
            'leave_type' => 1, // available
            'status'     => 1
        ])
            ->count();
    }

    public function getCalculateBalance($total1, $total2)
    {
        $x = number_format($total1, 1);
        $y = number_format($total2, 1);
        $z = $x - $y;
        return round($z, 1);
    }

    public function getCheckIfTally($total1, $total2)
    {
        $x = 0;
        if ($total1 == $total2) {
            $x = 1;
        }
        return $x;
    }

    public function getLeaveYear()
    {
        $year = array('' => '[Year]') + array_combine(range(date('Y')+1, 2016), range(date('Y')+1, 2016));
        return $year;
    }

    // get lists of years
    // ------------------
    public function getListYear()
    {
        $year = array('' => '[Year]') + array_combine(range(date('Y')+1, 2017), range(date('Y')+1, 2017));
        return $year;
    }

    // get lists of long years
    // -----------------------
    public function getListLongYear()
    {
        $year = array('' => '[Year]') + array_combine(range(date('Y')+1, 2000), range(date('Y')+1, 2000));
        return $year;
    }    

    public function getLeaveMonth()
    {
        $month = array('' => '[Month]') + array_combine(range(1, 12), range(1, 12));
        return $month;
    }

    public function getLeaveTypeIn($arr)
    {
        return \IhrV2\Models\LeaveType::whereIn('id', $arr)
            ->orderBy('id', 'asc')
            ->pluck('name', 'id')
            ->prepend('[Leave Type]', '');
    }

    public function getLeaveType()
    {
        return \IhrV2\Models\LeaveType::where('id', '!=', 6)->orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Leave Type]', '');
    }

    public function getLeaveTypeNotIn($arr)
    {
        return \IhrV2\Models\LeaveType::whereNotIn('id', $arr)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id')
            ->prepend('[Leave Type]', '');
    }

    public function getLeaveTypeAll()
    {
        return \IhrV2\Models\LeaveType::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Leave Type]', '');
    }

    public function getLeaveTypeWithPrefix()
    {
        return \IhrV2\Models\LeaveType::select(\DB::raw('concat (code, " - ", name) as name, id'))->orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Leave Type]', '');
        // return \IhrV2\Models\LeaveType::select(\DB::raw('concat (code, " - ", name) as name, id'))->where('id', '!=', 6)->orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Leave Type]', '');
    }

    public function getLeaveTypeName($id)
    {
        return \IhrV2\Models\LeaveType::find($id);
    }

    // get this when to create / update user_contracts row
    // count total annual by pro rate
    // ---------------------------------------------------
    public function getTotalAL($from_date, $to_date, $total)
    {
        $leap = 0;
        $from = Carbon::parse($from_date); // 2016-01-01
        $to = Carbon::parse($to_date);     // 2016-12-01

        // get different date
        // ------------------
        $diff = $from->diff($to)->days;

        // check leap year
        // ---------------
        if ($to->isLeapYear()) {
            $leap = 1;
            $total = round(($diff / 366) * $total);
        } else {
            $total = round(($diff / 365) * $total);
        }
        return $total;
    }

    // get total annual by join date
    // -----------------------------
    public function getTotalALByJoinDate($user_id, $sitecode, $date_from, $date_to)
    {
        // set variables
        // -------------
        $total = 0;
        $from = Carbon::parse($date_from);
        $to = Carbon::parse($date_to);

        // get total (12/14)
        // find join date from the beginning
        // ---------------------------------
        $job = \IhrV2\Models\UserJob::select('join_date')
            ->where([
                'user_id'  => $user_id,
                'sitecode' => $sitecode
            ])
            ->orderBy('id', 'asc')
            ->first();

        // have user job
        // -------------
        if (!empty($job)) {
            $join = Carbon::parse($job->join_date);

            // get only year & month
            // ---------------------
            // $diff_year = $join->diffInYears($to);
            // $diff_month = $join->diffInMonths($to);

            // get total year
            // --------------
            $diff_year = $join->diff($to)->format('%y');
            
            // get total month
            // ---------------
            $diff_month = $join->diff($to)->format('%m');

            // calculation total annual leave
            // ------------------------------
            // 0-2 = 12 days
            // >2-5 = 14 days
            // >5 = 16 days

            // check total years
            // -----------------
            if ($diff_year >= 5) {
                $total = 16;
            } elseif ($diff_year >= 2 && $diff_year < 5) {
                $total = 14;
            } else {
                $total = 12;
            }
        }

        // no user job
        // -----------
        else {
            $total = 12;
        }

        // get different date of current contract
        // need to +1 if not count is 364
        // ------------------------------
        $diff = $from->diff($to)->days + 1;

        // check total days in current year
        // --------------------------------
        $now = Carbon::now();
        if ($now->isLeapYear()) {
            $day_year = 366;
        } else {
            $day_year = 365;
        }

        // check if contract days less than total days in current year
        // -----------------------------------------------------------
        if ($diff < $day_year) {

            // count total in pro rate
            // -----------------------
            $total = $this->getTotalAL($date_from, $date_to, $total);
            $half = null;
        }

        // contract is 1 year
        // ------------------
        else {
            $half = $total / 2;
        }

        // return entitled
        // ---------------
        return array('total' => $total);
    }

    // get total eligibility of AL
    // ---------------------------
    public function getCheckAvailAL($date_to, $total, $half)
    {
        // get remaining month from current date
        // -------------------------------------
        $now = Carbon::now();
        $diff = $now->diffInMonths($date_to) + 1;

        // 1-6 month get use half of AL
        // ----------------------------
        if ($diff > 6) {
            $al = $half;
        }

        // 7-12 month can use all AL
        // -------------------------
        else {
            $al = $total;  
        }
        return $al;        
    }

    // check if end date contract is more than today
    // ---------------------------------------------
    public function CheckIfExpired($date)
    {
        $today_date = Carbon::now();
        $end_date = Carbon::createFromFormat('Y-m-d', $date);
        if ($today_date->gt($end_date)) {
            $x = 1; // expired
        } else {
            $x = 0; // not expired
        }
        return $x;
    }

    // check if end date contract is more than today
    // if current date is last date still allow approve/reject
    // -------------------------------------------------------
    public function CheckIfExpiredLeave($date)
    {
        $today_date = Carbon::now();
        $end_date = Carbon::createFromFormat('Y-m-d', $date)->addDay();
        if ($today_date->gt($end_date)) {
            $x = 1; // expired
        } else {
            $x = 0; // not expired
        }
        return $x;
    }

    public function DateRange($first, $last, $step = '+1 day', $output_format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
        while ($current <= $last) {
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }

    // compare 2 dates
    // ---------------
    public function LeaveCompareDate($from, $to, $half)
    {
        $x = Carbon::createFromFormat('Y-m-d', $from);
        $y = Carbon::createFromFormat('Y-m-d', $to);
        // if ($x == $y) {
        if ($x->diff($y)->days < 1) {
            if ($half == 1) {
                $diff = 'Half Day';
            } else {
                $diff = '1 Day';
            }
        } else {
            $diff = ($x->diffInDays($y) + 1) . ' Days';
        }
        return $diff;
    }

    public function LeaveHalfDay($from, $to, $half)
    {
        if ($from == $to) {
            if ($half == 1) {
                $x = 'Yes';
            } else {
                $x = 'No';
            }
        } else {
            $x = '-';
        }
        return $x;
    }

    public function getUserStateID($sitecode)
    {
        $x = \IhrV2\Models\Site::where('code', $sitecode)->first();
        return $x;
    }

    // check whether partner already taken the leave on apply date
    // -----------------------------------------------------------
    public function getCheckLeavePartner($uid, $sitecode, $dates)
    {
        $q = \IhrV2\Models\LeaveDate::has('LeaveApproveALRL')
            ->whereIn('leave_date', $dates)
            ->where([
                'user_id'    => $uid,
                'leave_type' => 1, // available
                'status'     => 1
            ])
            ->first();
        return $q;
    }

    public function getLeaveTypeDistinct($cond)
    {
        $q = \IhrV2\Models\LeaveApplication::select('leave_type_id')
        // ->whereHas('LeaveUserContractActive', function($c) use ($cond) {
        //     $c->where([
        //         'user_id' => $cond[0],
        //         'sitecode' => $cond[1],
        //     ]);
        //     $c->whereDate('date_from', '>=', $cond[2]);
        //     $c->whereDate('date_to', '<=', $cond[3]);
        // })
        ->with(array('LeaveTypeName'))
        ->where([
            'user_id'  => $cond[0],
            'sitecode' => $cond[1],
            'active'   => 1
        ])
        ->whereDate('date_from', '>=', $cond[2])
        ->groupBy('leave_type_id')
        ->orderBy('leave_type_id', 'asc')
        ->get();
        return $q;
    }

    public function getLeaveTypeInactive($cond)
    {
        $q = \IhrV2\Models\LeaveApplication::select('leave_type_id')
            ->with(array('LeaveTypeName'))
            ->where([
                'user_id'     => $cond[0],
                'sitecode'    => $cond[1],
                'contract_id' => $cond[2],
                'active'      => 1
            ])
            ->groupBy('leave_type_id')
            ->orderBy('leave_type_id', 'asc')
            ->get();
        return $q;
    }

    // get all leaves
    // --------------
    public function getLeaveListAll($cond)
    {
        $q = \IhrV2\Models\LeaveApplication::with(array('LeaveLatestHistory' => function ($h) {
            $h->with('LeaveStatusName');
        }))

        // check contract & includes leave previous contract too
        // -----------------------------------------------------
        // ->whereHas('LeaveUserContractActive', function($c) use ($cond) {
        //     $c->where([
        //         'user_id' => $cond[0],
        //         'sitecode' => $cond[1],
        //     ]);
        //     $c->whereDate('date_from', '>=', $cond[2]);
        //     $c->whereDate('date_to', '<=', $cond[3]);
        // })

            ->whereDate('date_from', '>=', $cond[2])
            ->with(array('LeaveDate' => function ($d) {
                $d->where('leave_type', 1); // available
            }))
            ->with(array('LeaveTypeName', 'LeaveDateAll', 'LeaveLatestAttachment'))
            ->where([
                'user_id'  => $cond[0],
                'sitecode' => $cond[1],
                'active'   => 1
            ])

            // disable checking on date_to because probably the leave long and by pass end contract
            // ------------------------------------------------------------------------------------
            ->whereDate('date_from', '>=', $cond[2])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    public function getLeaveAllInactive($cond)
    {
        $q = \IhrV2\Models\LeaveApplication::with(array('LeaveLatestHistory' =>
            function ($h) {
                $h->with('LeaveStatusName');
            }))
            ->with(array('LeaveDate' => function ($d) {
                $d->where('leave_type', 1); // available
            }))
            ->with(array('LeaveTypeName', 'LeaveDateAll', 'LeaveLatestAttachment'))
            ->where([
                'user_id'     => $cond[0],
                'sitecode'    => $cond[1],
                'contract_id' => $cond[2],
                'active'      => 1
            ])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get all leaves (inactive)
    // -------------------------
    public function getLeaveListInactive($cond)
    {
        $q = \IhrV2\Models\LeaveApplication::with(array('LeaveLatestHistory' =>
            function ($h) {
                $h->with('LeaveStatusName');
            }))
            ->whereHas('LeaveUserContract', function ($c) use ($cond) {
                $c->where([
                    'user_id'  => $cond[0],
                    'sitecode' => $cond[1]
                ]);
            })
            ->with(array('LeaveDate' => function ($d) {
                $d->where('leave_type', 1); // available
            }))
            ->with(array('LeaveTypeName', 'LeaveDateAll'))
            ->where([
                'user_id'  => $cond[0],
                'sitecode' => $cond[1],
                'active'   => 1
            ])
            ->whereDate('date_from', '>=', $cond[2])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get taken leave (date is within the current contract)
    // -----------------------------------------------------
    public function getLeaveTaken($uid, $sitecode, $leave_type_id, $date_from, $date_to)
    {
        $cond = array($uid, $sitecode, $leave_type_id, $date_from, $date_to);
        $query = \IhrV2\Models\LeaveApprove::selectRaw('SUM(date_value) AS total')
            ->whereHas('LeaveInfo', function ($x) use ($cond) {

                // $x->whereHas('LeaveUserContractActive', function($c) use ($cond) {
                //     $c->where([
                //         'user_id' => $cond[0],
                //         'sitecode' => $cond[1],
                //     ]);
                //     $c->whereDate('date_from', '>=', $cond[3]);
                //     $c->whereDate('date_to', '<=', $cond[4]);
                // });

                $x->where([
                    'user_id'       => $cond[0],
                    'sitecode'      => $cond[1],
                    'leave_type_id' => $cond[2],
                    'active'        => 1
                ]);
                $x->whereDate('date_from', '>=', $cond[3]);
            })
            ->where([
                'user_id'       => $uid,
                'leave_type_id' => $leave_type_id,
                'flag'          => 1
            ])
            ->groupBy('user_id')
            ->first();
        return $query;
    }

    // get leave taken by group leave type id
    // --------------------------------------
    public function getLeaveTakenByType($uid, $sitecode, $types, $date_from, $date_to)
    {
        $cond = array($uid, $sitecode, $types, $date_from, $date_to);
        $query = \IhrV2\Models\LeaveApprove::select('leave_type_id')
            ->selectRaw('SUM(date_value) AS total')
            ->whereHas('LeaveInfo', function ($x) use ($cond) {
                $x->where([
                    'user_id'  => $cond[0],
                    'sitecode' => $cond[1],
                    'active'   => 1
                ]);
                $x->whereIn('leave_type_id', $cond[2]);
                $x->whereDate('date_from', '>=', $cond[3]);
            })
            ->where([
                'user_id' => $uid,
                'flag'    => 1
            ])
            ->whereIn('leave_type_id', $types)
            ->groupBy('user_id', 'leave_type_id')
            ->get()
            ->toArray();
        return $query;
    }

    // get lists of taken leave on popup dialog (site supervisor)
    // ----------------------------------------------------------
    public function getLeaveTakenList($cond)
    {
        $q = \IhrV2\Models\LeaveApplication::with(array('LeaveLatestHistory'))
            ->with(array('LeaveDateAll'))
            ->with(array('LeaveDate' => function ($d) {
                $d->where('leave_type', 1); // available
            }))
            ->where([
                'user_id'       => $cond[0],
                'sitecode'      => $cond[1],
                'leave_type_id' => $cond[2],
                'active'        => 1
            ])
            ->whereDate('date_from', '>=', $cond[3])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get lists of taken leave on popup dialog (backend)
    // --------------------------------------------------
    public function getLeaveTakenList2($cond)
    {
        $q = \IhrV2\Models\LeaveApplication::with(array('LeaveLatestHistory'))
            ->with(array('LeaveDateAll'))
            ->with(array('LeaveDate' => function ($d) {
                $d->where('leave_type', 1); // available
            }))
            ->where([
                'user_id'       => $cond[0],
                'sitecode'      => $cond[1],
                'leave_type_id' => $cond[2],
                'active'        => 1
            ])
            ->whereDate('date_from', '>=', $cond[3])
            ->whereDate('date_to', '<=', $cond[4])
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // check leave balance by contract id
    // ----------------------------------
    public function getLeaveBalance($user_id, $leave_type_id, $contract_id)
    {
        $query = \IhrV2\Models\LeaveBalance::where([
            'user_id'       => $user_id,
            'leave_type_id' => $leave_type_id,
            'contract_id'   => $contract_id,
            'year'          => date('Y')
        ])
            ->first();
        return $query;
    }

    // leave status
    // ------------
    public function getLeaveStatusList()
    {
        return \IhrV2\Models\LeaveStatus::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Leave Status]', '');
    }

    // RL Request status
    // -----------------
    public function getRLReqStatusList()
    {
        return \IhrV2\Models\LeaveStatus::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Request Status]', '');
    }

    public function getLeaveStatus($id)
    {
        return \IhrV2\Models\LeaveStatus::find($id);
    }

    public function getLeaveTypeByID($leave_type_id)
    {
        return \IhrV2\Models\LeaveType::find($leave_type_id);
    }

    // get RL request by ID
    // --------------------
    public function getLeaveRepByID($id)
    {
        return \IhrV2\Models\LeaveRepApplication::find($id);
    }

    // get replacement leave info
    // --------------------------
    public function getLeaveRepDetail($id)
    {
        return \IhrV2\Models\LeaveRepApplication::find($id);
    }

    // get replacement request by user info
    // ------------------------------------
    public function getLeaveRepByUser($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\LeaveRepApplication::with(array('LeaveRepLatestHistory' =>
            function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->with(array(
                'LeaveRepUserDetail',
                'LeaveRepSiteName',
                'LeaveRepReportToName',
                'LeaveRepLatestAttachment'
            ))
            ->where([
                'id'       => $id,
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->first();
        return $q;
    }

    // get replacement request by history
    // ----------------------------------
    public function getLeaveRepByHistory($id, $uid, $sitecode, $contract_id)
    {
        $q = \IhrV2\Models\LeaveRepApplication::with(array('LeaveRepLatestHistory' =>
            function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->where([
                'user_id'  => $uid,
                'contract_id' => $contract_id,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->whereNotIn('id', array($id))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get record of apply cancel
    // --------------------------
    public function getRLApplyCancelHist($id, $uid, $status)
    {
        $q = \IhrV2\Models\LeaveRepHistory::where(
            [
                'leave_rep_id' => $id,
                'user_id'      => $uid,
                'status'       => $status
            ]
        )
            ->first();
        return $q;
    }

    // 5 - site supervisor cancel RL request
    // 4 - site supervisor apply cancel RL request
    // 2 - rm approve RL request
    // 3 - rm reject RL request
    // 6 - rm approve apply cancel RL request
    // 7 - rm reject apply cancel RL request
    // -------------------------------------
    public function getProcessLeaveRep($data, $id, $uid, $sitecode, $action_by)
    {
        // set variables
        // -------------
        $type = $data['type'];
        $xexist = 0;
        $pass = 0;
        $status_job = 0;
        $status_contract = 0;
        $expired = 0;
        $success = 0;
        $email_rl = 0;
        $email_cancel = 0;

        // get RL request info
        // -------------------
        $request = $this->getLeaveRepByUser($id, $uid, $sitecode);

        // have record
        // -----------
        if (!empty($request) && !empty($request->LeaveRepLatestHistory)) {

            // approve request (current = pending)
            // -----------------------------------
            if ($type == 2 && $request->LeaveRepLatestHistory->status == 1) {
                $pass = 1;
            }

            // reject request (current = pending)
            // ----------------------------------
            if ($type == 3 && $request->LeaveRepLatestHistory->status == 1) {
                $pass = 1;
            }

            // cancel request (current = pending)
            // ----------------------------------
            if ($type == 5 && $request->LeaveRepLatestHistory->status == 1) {
                $pass = 1;
            }

            // apply cancel request (current = approve)
            // ----------------------------------------
            if ($type == 4 && $request->LeaveRepLatestHistory->status == 2) {
                $pass = 1;
            }

            // approve apply cancel request (current = apply cancel)
            // -----------------------------------------------------
            if ($type == 6 && $request->LeaveRepLatestHistory->status == 4) {
                $pass = 1;
            }

            // reject apply cancel request (current = apply cancel)
            // ----------------------------------------------------
            if ($type == 7 && $request->LeaveRepLatestHistory->status == 4) {
                $pass = 1;
            }
        }

        // no record
        // ---------
        else {
            $xexist = 1;
        }

        // have record and pass condition
        // ------------------------------
        if ($xexist != 1 && $pass == 1) {

            // get user info
            // -------------
            $user = $this->user_repo->getUserByIDSitecode($uid, $sitecode);

            // check user job
            // --------------
            if (!empty($user->UserLatestJob)) {
                $status_job = 1;
            }

            // check user contract
            // -------------------
            if (!empty($user->UserLatestContract)) {
                $status_contract = 1;
            }

            // user job and contract exist
            // ---------------------------
            if ($status_job == 1 && $status_contract == 1) {

                // get user contract
                // -----------------
                $contract = $user->UserLatestContract;

                // check contract is not expired
                // -----------------------------
                $check_expired = $this->CheckIfExpired($contract->date_to);
                if ($check_expired) {
                    $expired = 1;
                }

                // contract is not expired
                // -----------------------
                if ($expired != 1) {

                    // set current flag to 0
                    // ---------------------
                    $this->db_leave_repo->dbUpdateLeaveRepHistory($id, $uid);

                    // check leave rep histories
                    // -------------------------
                    $chk_history = $this->getCheckLeaveRepHistory($uid, $id, $type);
                    if (empty($chk_history)) {

                        // insert new leave_rep_histories
                        // ------------------------------
                        $data['action_by'] = $action_by;
                        $this->db_leave_repo->dbProcessLeaveRepHistory($id, $uid, $data);
                    }

                    // approve RL request
                    // ------------------
                    if ($type == 2) {

                        // check leave rep approve
                        // -----------------------
                        $chk_approve = $this->getCheckLeaveRepApprove($uid, $id);
                        if (empty($chk_approve)) {

                            // insert leave_rep_approves
                            // -------------------------
                            $this->db_leave_repo->dbInsertLeaveRepApprove($id, $uid, $request->no_day, $action_by);
                        }

                        // return message
                        // --------------
                        $status_name = 'Approved';
                        $msg = array('RL request successfully '.$status_name.'.', 'success');
                        $email_rl = 1;
                    }

                    // apply cancel RL request
                    // -----------------------
                    elseif ($type == 4) {

                        // get rm info
                        // -----------
                        $rm = session()->get('rm_info');

                        // email body info
                        // ---------------
                        $arr_body = array(
                            'position_name' => session()->get('position_name'),
                            'site_name'     => session()->get('site_name'),
                            'rl'            => $this->getLeaveRepDetail($id),
                            'history'       => array('remark' => $data['remark'], 'action_date' => Carbon::now()),
                            'url'           => route('mod.leave.replacement.view', array($id, $uid, $sitecode))
                        );

                        // receiver info
                        // -------------
                        $arr = array(
                            'rm_email' => $rm['email'],
                            'rm_name'  => $rm['name']
                        );

                        // email notification to rm
                        // ------------------------
                        $this->email_repo->getEmailRLCancel($arr_body, $arr);

                        // return message
                        // --------------
                        $msg = array('Apply Cancel is in progress. Wait for RM Approval.', 'success');
                    }

                    // cancel RL request
                    // -----------------
                    elseif ($type == 5) {
                        $msg = array('RL Request successfully Cancel.', 'success');
                    }

                    // reject RL request
                    // -----------------
                    elseif ($type == 3) {

                        // return message
                        // --------------
                        $status_name = 'Rejected';
                        $msg = array('RL Request is '.$status_name.'.', 'success');
                        $email_rl = 1;
                    }

                    // approve apply cancel RL request
                    // -------------------------------
                    elseif ($type == 6) {

                        // set current flag to 0 leave_rep_approves
                        // ----------------------------------------
                        $this->db_leave_repo->dbUpdateLeaveRepApprove($id, $uid);

                        // return message
                        // --------------
                        $status_name = 'Approved';
                        $msg = array('Apply Cancel successfully '.$status_name.'.', 'success');
                        $email_cancel = 1;
                    }

                    // reject apply cancel RL request
                    // ------------------------------
                    elseif ($type == 7) {

                        // return message
                        // --------------
                        $status_name = 'Rejected';
                        $msg = array('Apply Cancel is '.$status_name.'.', 'success');
                        $email_cancel = 1;
                    }

                    // unknown type
                    // ------------
                    else {
                        $msg = array('Operation is not Permitted.', 'danger');
                    }

                    // email result RL request
                    // -----------------------
                    if ($email_rl == 1) {

                        // get user name and email
                        // -----------------------
                        $arr = array(
                            'mgr_name'  => $user->name,
                            'mgr_email' => $user->email,
                            'status'    => $status_name
                        );

                        // get rl info
                        // -----------
                        $arr_body = array(
                            'rl'  => $this->getLeaveRepByID($id),
                            'status'    => $status_name,
                            'remark' => $data['remark']
                        );

                        // send email notification
                        // -----------------------
                        $this->email_repo->getEmailRLResult($arr_body, $arr);
                    }

                    // email result apply cancel
                    // -------------------------
                    if ($email_cancel == 1) {

                        // get user name and email
                        // -----------------------
                        $arr = array(
                            'mgr_name'  => $user->name,
                            'mgr_email' => $user->email,
                            'status'    => $status_name
                        );

                        // get apply cancel history
                        // ------------------------
                        $history = $this->getRLApplyCancelHist($id, $uid, 4);

                        // get rl info
                        // -----------
                        $arr_body = array(
                            'rl'      => $this->getLeaveRepDetail($id),
                            'status'  => $status_name,
                            'remark'  => $data['remark'],
                            'history' => $history
                        );

                        // send email notification
                        // -----------------------
                        $this->email_repo->getEmailRLCancelResult($arr_body, $arr);
                    }
                }

                // contract is expired
                // -------------------
                else {
                    $msg = array('Contract is Expired. Please contact HR for further information.', 'danger');
                }
            }

            // staff job/contract is inactive
            // ------------------------------
            else {
                $msg = array('Staff Job/Contract is Inactive.', 'danger');
            }
        }

        // set error message
        // -----------------
        else {

            // RL request not exist
            // --------------------
            if ($xexist == 1) {
                $msg = array('RL request is not Exist.', 'danger');
            }

            // record already processed
            // ------------------------
            if ($pass == 0) {
                $msg = array('RL request already Processed.', 'danger');
            }
        }

        // return output
        // -------------
        return $msg;
    }

    // get RL request details
    // ----------------------
    public function getLeaveRepView($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\LeaveRepApplication::with(array('LeaveRepPrevHistory' =>
            function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->where([
                'id'       => $id,
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->firstOrFail();
        return $q;
    }

    public function getLeavePendingAll($codes)
    {
        $filters = array('status' => 1, 'flag' => 1);
        $total = \IhrV2\Models\LeaveApplication::select(\DB::raw('COUNT(*) as total'), 'leave_type_id')
            ->whereHas('LeaveLatestHistory', function ($q) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $q->where($column, $key);
                    }
                }
            })
            ->with(array('LeaveTypeName'))
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    $k->whereIn('sitecode', $codes);
                }
            })
            ->where('active', 1)
            ->groupBy('leave_type_id')
            ->get();
        return $total;
    }

    // get total apply leave by type
    // -----------------------------
    public function getLeaveTotalByType($codes, $year)
    {
        $total = \IhrV2\Models\LeaveApplication::select(\DB::raw('COUNT(*) as total'), 'leave_type_id')
        ->with(array('LeaveTypeName'))
        ->where(function ($k) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $k->orWhere('sitecode', 'like', $code . '%');
                }
            }
        })
        ->where('active', 1)
        ->whereYear('date_apply', '=', $year)
        ->groupBy('leave_type_id')
        ->get();
        return $total;
    }

    // get total apply leave by status
    // -------------------------------
    public function getLeaveTotalByStatus($codes, $year)
    {
        $q = \IhrV2\Models\LeaveHistory::select(\DB::raw('COUNT(*) as total'), 'status')
        ->whereHas('LeaveApplicationName', function ($r) use ($codes) {
            $r->where([
                'active' => 1
            ]);
            $r->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            });
        })
        ->with(array('LeaveStatusName'))
        ->where([
            'flag' => 1
        ])
        ->whereYear('action_date', '=', $year)
        ->groupBy('status')
        ->get();
        return $q;
    }

    public function getLeaveTotalByTypeSite($uid, $sitecode, $start_date, $end_date)
    {
        $total = \IhrV2\Models\LeaveApplication::select(\DB::raw('COUNT(*) as total'), 'leave_type_id')
        ->with(array('LeaveTypeName'))
        ->where(['user_id' => $uid, 'sitecode' => $sitecode, 'active' => 1])
        ->whereDate('date_from', '>=', $start_date)
        ->whereDate('date_to', '<=', $end_date)
        ->groupBy('leave_type_id')
        ->get();
        return $total;
    }

    public function getLeaveTotalByStatusSite($uid, $sitecode, $start_date, $end_date)
    {
        $arr = array($uid, $sitecode, $start_date, $end_date);
        $q = \IhrV2\Models\LeaveHistory::select(\DB::raw('COUNT(*) as total'), 'status')
        ->whereHas('LeaveApplicationName', function ($r) use ($arr) {
            $r->where([
                'user_id' => $arr[0],
                'sitecode' => $arr[1],
                'active' => 1
            ]);
            $r->whereDate('date_from', '>=', $arr[2]);
            $r->whereDate('date_to', '<=', $arr[3]);
        })
        ->with(array('LeaveStatusName'))
        ->where([
            'user_id' => $uid,
            'flag' => 1
        ])
        ->groupBy('status')
        ->get();
        return $q;
    }

    // get total apply leave of current date
    // -------------------------------------
    public function getLeaveTotalToday($codes, $date)
    {
        $total = \IhrV2\Models\LeaveApplication::where('active', 1)
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->whereDate('date_apply', '=', $date) // current date
            ->count();
        return $total;
    }

    // display all leave status
    // ------------------------
    public function getLeavePendingAllHistory($codes)
    {
        $total = \IhrV2\Models\LeaveApplication::select(\DB::raw('COUNT(*) as total'), 'leave_type_id')
            ->has('LeavePending')
            ->with(array('LeaveTypeName'))

            // ->whereHas('LeaveUserContractActive', function($c) {
            //     $c->where([
            //         'status' => 1, // contract is active
            //     ]);
            //     $c->whereYear('date_from', '>=', '2016');
            // })

            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->where('active', 1)
            // ->whereYear('date_from', '>=', '2016')
            ->where('date_from', '>=', '2016-05-01')
            ->where('date_apply', 'NOT LIKE', '%00:00:00%') // remove old leave
            ->groupBy('leave_type_id')
            ->get();
        return $total;
    }

    // get all rl request that pending
    // -------------------------------
    public function getLeaveRepPendingAll($codes)
    {
        $q = \IhrV2\Models\LeaveRepApplication::has('LeaveRepPending')
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->whereHas('LeaveRepContractActive', function ($c) {
                $c->where('status', 1);
                $c->whereYear('date_from', '>=', '2016');
                $c->whereDate('date_to', '>=', date('Y-m-d'));
            })
            ->where(['active' => 1])
            ->whereYear('year_month', '>=', '2017')
            // ->whereDate('year_month', '>=', '2016-05-01')
            // ->whereDate('year_month', '>=', '2017-01-01')
            ->count();
        return $q;
    }

    // get total RL by date
    // --------------------
    public function getTotalRLByDate($codes, $date)
    {
        $q = \IhrV2\Models\LeaveRepApplication::where(['active' => 1])
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->whereDate('date_apply', '=', $date)
            ->count();
        return $q;
    }

    // get total RL by status
    // ----------------------
    public function getTotalRLByStatus($codes, $year)
    {
        $q = \IhrV2\Models\LeaveRepHistory::select(\DB::raw('COUNT(*) as total'), 'status')
        ->whereHas('LeaveRepInfo', function ($r) use ($codes) {
            $r->where([
                'active' => 1
            ]);
            $r->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            });
        })
        ->with(array('LeaveRepStatusName'))
        ->where([
            'flag' => 1
        ])
        ->whereYear('action_date', '=', $year)
        ->groupBy('status')
        ->get();
        return $q;
    }

    // lists all leaves
    // ----------------
    public function getLeaveIndexListAll($arr, $codes)
    {
        // get value from input
        // --------------------
        $site_id = $arr['site_id'];
        $leave_type_id = $arr['leave_type_id'];
        $start_date = $arr['start_date'];
        $end_date = $arr['end_date'];
        $keyword = $arr['keyword'];
        $leave_status_id = $arr['leave_status_id'];
        $half_day_id = $arr['half_day_id'];
        $position_id = $arr['position_id'];
        $region_id = $arr['region_id'];
        $phase_id = $arr['phase_id'];

        // set array
        // ---------
        $dates = array($start_date, $end_date);
        $job = array('region_id' => $region_id, 'phase_id' => $phase_id, 'position_id' => $position_id);

        // query leaves
        // ------------
        $leaves = \IhrV2\Models\LeaveApplication::with(
            array(
                'LeaveTypeName',
                'LeaveUserDetail',
                'LeaveSiteName',
                'LeaveDateAvail'
            )
        )
        ->with(array('LeaveUserJob' => function ($j) {
            $j->with(array('PositionName'));
        }))
        ->with(array('LeaveLatestHistory' => function ($h) {
            $h->with('LeaveStatusName');
        }))

        // initial sites by region
        // -----------------------
        ->where(function ($c) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $c->orWhere('sitecode', 'like', $code . '%');
                }
            }
        })

        // search by site
        // --------------
        ->where(function ($s) use ($site_id) {
            if (!empty($site_id)) {
                $s->where('sitecode', $site_id);
            }
        })

        // search by half day
        // ------------------
        ->where(function ($h) use ($half_day_id) {
            if (!empty($half_day_id)) {
                $h->where('is_half_day', $half_day_id);
            }
        })

        // search by region and phase
        // --------------------------
        ->where(function ($j) use ($job) {
            $j->whereHas('LeaveUserJob', function ($i) use ($job) {
                foreach ($job as $column => $key) {
                    if (!is_null($key)) {
                        $i->where($column, $key);
                    }
                }
            });
        })

        // search by leave type
        // --------------------
        ->where(function ($t) use ($leave_type_id) {
            if (!empty($leave_type_id)) {
                $t->where('leave_type_id', $leave_type_id);
            }
        })

        // search by leave status
        // ----------------------
        ->where(function ($l) use ($leave_status_id) {
            if (!empty($leave_status_id)) {
                $l->whereHas('LeaveLatestHistory', function ($i) use ($leave_status_id) {
                    $i->where('status', $leave_status_id);
                });
            }
        })

        // search by leave dates
        // ---------------------
        ->where(function ($a) use ($dates) {
            if (!empty($dates[0]) && !empty($dates[1])) {
                $a->whereDate('date_from', '>=', $dates[0]);
                $a->whereDate('date_to', '<=', $dates[1]);
            }
            if (!empty($dates[0]) && empty($dates[1])) {
                $a->whereDate('date_from', '=', $dates[0]);
            }
            if (!empty($dates[1]) && empty($dates[0])) {
                $a->whereDate('date_to', '=', $dates[1]);
            }
        })

        // search by staff detail
        // ----------------------
        ->where(function ($k) use ($keyword) {
            if (!empty($keyword)) {
                $k->whereHas('LeaveUserDetail', function ($i) use ($keyword) {
                    $i->where('name', 'like', '%' . $keyword . '%');
                    $i->orWhere('icno', 'like', '%' . $keyword . '%');
                    $i->orWhere('username', 'like', '%' . $keyword . '%');
                    $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
                });
            }
        })
        ->where('active', 1)
        // ->whereDate('date_from', '>=', '2016-05-01')    // do not show may below
        ->whereYear('date_from', '>=', '2018')
        ->where('date_apply', 'NOT LIKE', '%00:00:00%') // remove old leave
        ->orderBy('id', 'desc')
        ->paginate(20);
        return $leaves;
    }

    // search all leaves
    // -----------------
    public function getLeaveRepIndexListAll($arr, $codes)
    {
        // get value from input
        // --------------------
        $site_id = $arr['site_id'];
        $phase_id = $arr['phase_id'];
        $leave_status_id = $arr['leave_status_id'];
        $keyword = $arr['keyword'];
        $region_id = $arr['region_id'];
        $apply_date = $arr['apply_date'];

        // region and phase value
        // ----------------------
        $jobs = array('phase_id' => $phase_id, 'region_id' => $region_id);

        // query replacement request
        // -------------------------
        $leaves = \IhrV2\Models\LeaveRepApplication::whereHas(
            'LeaveRepLatestHistory',
            function ($q) use ($leave_status_id) {
                if (!empty($leave_status_id)) {
                    $q->where('status', $leave_status_id);
                }
            }
        )
            ->with(array('LeaveRepLatestHistory' => function ($h) {
                $h->with('LeaveRepStatusName');
            }))
            ->with(array('LeaveRepUserJob' => function ($j) {
                $j->with(array('PositionName', 'PhaseName', 'RegionName'));
            }))
            ->whereHas('LeaveRepUserJob', function ($l) use ($jobs) {
                foreach ($jobs as $column => $key) {
                    if (!is_null($key)) {
                        $l->where($column, $key);
                    }
                }
            })
            ->where(function ($s) use ($site_id) {
                if (!empty($site_id)) {
                    $s->where('sitecode', $site_id);
                }
            })
            ->where(function ($c) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $c->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->where(function ($d) use ($apply_date) {
                if (!empty($apply_date)) {
                    $d->whereDate('date_apply', '=', $apply_date);
                }
            })
            ->where(function ($k) use ($keyword) {
                if (!empty($keyword)) {
                    $k->whereHas('LeaveRepUserDetail', function ($i) use ($keyword) {
                        $i->where('name', 'like', '%' . $keyword . '%');
                        $i->orWhere('icno', 'like', '%' . $keyword . '%');
                        $i->orWhere('username', 'like', '%' . $keyword . '%');
                        $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
                    });
                }
            })
            ->with(array(
                'LeaveRepSiteName',
                'LeaveRepUserDetail'
            ))
            ->whereYear('year_month', '>=', '2017')
            ->where(['active' => 1])
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $leaves;
    }

    // lists of manage user leave
    // --------------------------
    public function getLeaveManageListAll($arr, $codes)
    {
        // get value from input
        // --------------------
        $site_id = $arr[0];
        $region_id = $arr[1];
        $phase_id = $arr[2];
        $position_id = $arr[3];
        $status_id = $arr[4];
        $keyword = $arr[5];
        $jobs = array('region_id' => $region_id, 'phase_id' => $phase_id, 'position_id' => $position_id);

        // query users
        // -----------
        $users = \IhrV2\User::whereHas(
            'UserCurrentJob2',
            function ($j) use ($jobs) {
                foreach ($jobs as $column => $key) {
                    if (!is_null($key)) {
                        $j->where($column, $key);
                    }
                }
            }
        )
            ->with(array('UserCurrentJob2' => function ($h) {
                $h->with('PositionName');
                $h->with('RegionName');
            }))
            ->where(function ($s) use ($site_id) {
                if (!empty($site_id)) {
                    $s->where('sitecode', $site_id);
                }
            })
            ->where(function ($t) use ($status_id) {
                if (!empty($status_id)) {
                    $t->where('status', $status_id);
                }
            })
            ->where(function ($c) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $c->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->where(function ($k) use ($keyword) {
                if (!empty($keyword)) {
                    $k->where('name', 'like', '%' . $keyword . '%');
                    $k->orWhere('icno', 'like', '%' . $keyword . '%');
                    $k->orWhere('username', 'like', '%' . $keyword . '%');
                    $k->orWhere('sitecode', 'like', '%' . $keyword . '%');
                }
            })
            ->with(array('SiteNameInfo', 'StatusName'))
            ->where(['group_id' => 3])
            ->whereNotIn('sitecode', array('X01C001'))
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $users;
    }

    // get leave pending one
    // ---------------------
    public function getLeavePendingOne($uid, $sitecode)
    {
        $arr = array('user_id' => $uid, 'status' => 1, 'flag' => 1);
        $total = \IhrV2\Models\LeaveApplication::select(\DB::raw('COUNT(*) as total'))
            ->whereHas('LeaveLatestHistory', function ($q) use ($arr) {
                foreach ($arr as $column => $key) {
                    if (!is_null($key)) {
                        $q->where($column, $key);
                    }
                }
            })
            ->where([
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->whereDate('date_from', '>=', '2016-05-01') // do not show may below
            ->first();
        return $total;
    }

    // get rl pending one
    // ------------------
    public function getLeaveRepPendingOne($uid, $sitecode)
    {
        $arr = array('user_id' => $uid, 'status' => 1, 'flag' => 1);
        $total = \IhrV2\Models\LeaveRepApplication::select(\DB::raw('COUNT(*) as total'))
            ->whereHas('LeaveRepLatestHistory', function ($q) use ($arr) {
                foreach ($arr as $column => $key) {
                    if (!is_null($key)) {
                        $q->where($column, $key);
                    }
                }
            })
            ->where([
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->whereDate('year_month', '>=', '2016-05-01')
            ->first();
        return $total;
    }

    public function getLeaveTypeListAll()
    {
        $q = \IhrV2\Models\LeaveType::all()->toArray();
        return $q;
    }

    // get total reimburse/deduct by leave_type_id
    // -------------------------------------------
    public function getLeaveTotalReimburse($uid, $sitecode, $contract_id, $date_from, $date_to, $prev)
    {
        $cond = array($contract_id, $date_from, $date_to, $prev);
        $q = \IhrV2\Models\LeaveAdd::selectRaw('leave_type_id, count(*) as total')
            ->where([
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'status'   => 1
            ])
            ->where(function ($f) use ($cond) {
                if ($cond[3] == true) {
                    $f->where('contract_id', $cond[0]);
                } else {
                    $f->whereDate('action_date', '>=', $cond[1]);
                    $f->whereDate('action_date', '<=', $cond[2]);
                }
            })
            ->groupBy('leave_type_id')
            ->get()
            ->toArray();
        return $q;
    }

    // get all total reimburse or deduct
    // ---------------------------------
    public function getLeaveReimburseDeductAll($cond, $leave_type_id)
    {
        $q = \IhrV2\Models\LeaveAdd::selectRaw('leave_type_id, type_id, sum(total) as total_all')
            ->where([
                'user_id'       => $cond[0],
                'leave_type_id' => $leave_type_id,
                'sitecode'      => $cond[1],
                'status'        => 1
            ])
            ->where(function ($f) use ($cond) {
                if ($cond[5] == true) {
                    $f->where('contract_id', $cond[2]);
                } else {
                    $f->whereDate('action_date', '>=', $cond[3]);
                    $f->whereDate('action_date', '<=', $cond[4]);
                }
            })
            ->groupBy('leave_type_id', 'type_id')
            ->get();
        return $q;
    }

    // get total leave taken by each leave type
    // ----------------------------------------
    public function getLeaveTakenAll($uid, $sitecode, $contract_id, $date_from, $date_to, $prev = false)
    {
        $cond = array($uid, $sitecode, $contract_id, $date_from, $date_to, $prev);
        $q = \IhrV2\Models\LeaveApprove::selectRaw('SUM(date_value) AS total, leave_type_id')
            ->whereHas('LeaveInfo', function ($x) use ($cond) {
                $x->where([
                    'user_id'  => $cond[0],
                    'sitecode' => $cond[1],
                    'active'   => 1
                ]);
                if ($cond[5] == true) {
                    $x->whereDate('date_from', '>=', $cond[3]);
                    $x->whereDate('date_to', '<=', $cond[4]);
                } else {
                    $x->whereDate('date_from', '>=', $cond[3]);
                }
            })
            ->where([
                'user_id' => $uid,
                'flag'    => 1
            ])
            ->groupBy('user_id', 'leave_type_id')
            ->get()
            ->toArray();
        return $q;
    }

    // display leave summary
    // ---------------------
    public function getLeaveSummaryEach($uid, $sitecode, $contract_id, $date_from, $date_to, $prev = false)
    {
        // set array
        // ---------
        $arr = array($uid, $sitecode, $contract_id, $date_from, $date_to, $prev);
        $rows = array();

        // check if have reimburse/deduct
        // ------------------------------
        $rd = array();
        $chk_rd = $this->getLeaveTotalReimburse($uid, $sitecode, $contract_id, $date_from, $date_to, $prev);
        if (count($chk_rd) > 0) {
            foreach ($chk_rd as $c) {
                $rd[] = $c['leave_type_id'];
            }
        }

        // get leave taken all
        // -------------------
        $chk_taken = $this->getLeaveTakenAll($uid, $sitecode, $contract_id, $date_from, $date_to, $prev);

        // get leave types
        // ---------------
        $types = $this->getLeaveTypeListAll();

        // leave types exist
        // -----------------
        if (count($types) > 0) {
            foreach ($types as $i) {

                // get leave id & name
                // -------------------
                $id = $i['id'];
                $name = $i['name'];

                // entitled AL
                // -----------
                if ($i['id'] == 1) {
                    $chk_entitled = $this->getTotalALByJoinDate($uid, $sitecode, $date_from, $date_to);
                    $entitled = $chk_entitled['total'];
                }

                // entitled approved RL request
                // ----------------------------
                elseif ($i['id'] == 6) {
                    $chk_entitled = $this->getLeaveRepEntitled($uid, $sitecode, $contract_id, $date_from, $date_to, true);
                    if (!empty($chk_entitled)) {
                        $entitled = $chk_entitled->total;
                    } else {
                        $entitled = 0;
                    }
                }

                // entitled from db
                // ----------------
                else {
                    $entitled = $i['total'];
                }

                // initialize value reimburse & deduct
                // -----------------------------------
                $reimburse = 0;
                $deduct = 0;

                // if leave type have reimburse/deduct
                // -----------------------------------
                if (in_array($i['id'], $rd)) {

                    // get total reimburse & deduct
                    // ----------------------------
                    $get_rd = $this->getLeaveReimburseDeductAll($arr, $i['id'])->toArray();
                    if (!empty($get_rd)) {
                        foreach ($get_rd as $grd) {

                            // have reimburse
                            // --------------
                            if ($grd['type_id'] == 1) {
                                $reimburse = $grd['total_all'];
                            }

                            // have deduct
                            // -----------
                            if ($grd['type_id'] == 2) {
                                $deduct = $grd['total_all'];
                            }
                        }
                    }
                }

                // get leave taken
                // ---------------
                $taken = 0;
                $fnl_taken = 0;

                // have record
                // -----------
                if (!empty($chk_taken)) {
                    foreach ($chk_taken as $t) {
                        if ($t['leave_type_id'] == $i['id']) {

                            // if have unplan taken
                            // --------------------
                            if ($t['leave_type_id'] == 13 && $t['total'] > 0) {

                                // set final taken for annual
                                // --------------------------
                                $rows[0]['fnl_taken'] = $rows[0]['taken'] + $t['total'];
                            }
                            $taken = $t['total'];
                            $fnl_taken = $t['total'];
                        }
                    }
                }

                // no leave taken
                // --------------
                else {
                    $taken = 0;
                    $fnl_taken = 0;
                }

                // get balance (if unplan leave do not check balance)
                // --------------------------------------------------
                if ($i['id'] != 13) {

                    // calculate balance
                    // ----------------
                    $bal = $this->getCalculateBalance($entitled, $fnl_taken);

                    // check on reimburse and deduct
                    // -----------------------------
                    $balance = ($bal + $reimburse) - $deduct;
                }

                // set unplan leave balance to 0
                // -----------------------------
                else {

                    // have taken unplan leave
                    // -----------------------
                    if ($fnl_taken > 0) {

                        // calculate balance again for annual leave
                        // ----------------------------------------
                        $chk_bal = $this->getCalculateBalance($rows[0]['entitled'], $rows[0]['fnl_taken']);

                        // check on reimburse and deduct
                        // -----------------------------
                        $rows[0]['balance'] = ($chk_bal + $rows[0]['reimburse']) - $rows[0]['deduct'];
                    }
                    $balance = 0;
                }

                // put data into array
                // -------------------
                $rows[] = array(
                    'id' => $id,
                    'name' => $name,
                    'entitled' => $entitled,
                    'reimburse' => $reimburse,
                    'deduct' => $deduct,
                    'taken' => $taken,
                    'balance' => $balance,
                    'fnl_taken' => $fnl_taken
                );
            }
        }
        // dd($rows);
        return $rows;
    }

    // check today RL request
    // ----------------------
    public function getLeaveRepToday($uid, $sitecode)
    {
        $q = \IhrV2\Models\LeaveRepApplication::has('LeaveRepLatestHistory')
            ->whereDate('date_apply', '=', Carbon::today()->format('Y-m-d'))
            ->where([
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->with(array('LeaveRepLatestHistory'))
            ->orderBy('id', 'desc')
            ->first();
        return $q;
    }

    public function getOffDateMonthYear($state_id, $types, $year, $month)
    {
        $q = \IhrV2\Models\LeaveOffDate::whereHas('OffDayState', function ($i) use ($state_id) {
            $i->where('state_id', $state_id);
        })
            ->whereIn('type_id', $types)
            ->where('year', $year)
            ->whereYear('off_date', '=', $year)
            ->whereMonth('off_date', '=', $month)
            ->where('status', 1)
            ->get()->toArray();
        return $q;
    }

    // check table leave history (prevent multiple submit)
    // ---------------------------------------------------
    public function getCheckLeaveHistory($uid, $id, $status)
    {
        $q = \IhrV2\Models\LeaveHistory::where([
            'user_id'  => $uid,
            'leave_id' => $id,
            'status'   => $status,
            'flag'     => 1
        ])
            ->first();
        return $q;
    }

    // check table approve for approve status (prevent multiple submit)
    // ----------------------------------------------------------------
    public function getCheckLeaveApprove($uid, $id)
    {
        $q = \IhrV2\Models\LeaveApprove::where([
            'user_id'  => $uid,
            'leave_id' => $id,
            'flag'     => 1
        ])
            ->first();
        return $q;
    }

    // check leave rep history
    // -----------------------
    public function getCheckLeaveRepHistory($uid, $id, $status)
    {
        $q = \IhrV2\Models\LeaveRepHistory::where([
            'user_id'      => $uid,
            'leave_rep_id' => $id,
            'status'       => $status,
            'flag'         => 1
        ])
            ->first();
        return $q;
    }

    // check leave rep approve
    // -----------------------
    public function getCheckLeaveRepApprove($uid, $id)
    {
        $q = \IhrV2\Models\LeaveRepApprove::where([
            'user_id'      => $uid,
            'leave_rep_id' => $id,
            'flag'         => 1
        ])
            ->first();
        return $q;
    }

    // create new reimburse/deduct leave
    // ---------------------------------
    public function getProcessReimburseDeduct($data, $uid, $sitecode)
    {
        // get id and name
        // ---------------
        $leave_type_id = $data['leave_type_id'];
        $type_name = ($data['type_id'] == 1) ? 'Reimburse' : 'Deduct';

        // get user info
        // -------------
        $user = $this->getCheckUserActive($uid, $sitecode);

        // get contract id
        // ---------------
        $contract = $user['user']['user_latest_contract'];

        // insert leave add
        // ----------------
        $data['contract_id'] = $contract['id'];
        $id = $this->db_leave_repo->dbInsertLeaveAdd($data, $uid, $sitecode);

        // check leave balance
        // -------------------
        $balance = $this->getLeaveBalance($uid, $leave_type_id, $contract['id']);

        // have balance record
        // -------------------
        if (!empty($balance)) {

            // get latest balance
            // ------------------
            $new = $balance->balance + $data['total'];

            // update leave balance
            // --------------------
            $this->db_leave_repo->dbUpdateLeaveBalance($uid, $leave_type_id, $contract['id'], $new);
        }

        // get leave add info
        // ------------------
        $leave = $this->getLeaveAddInfo($id);
        $leave_name = $leave->LeaveTypeName->name;
        $action_by = $leave->ActionByDetail->name;

        // check if have attachment
        // ------------------------
        if (!empty($data['leave_file'])) {

            // upload file attachment
            // ----------------------
            $attach = $this->upload_repo->uploadFileLeaveAdd($data['leave_file'], $id, $uid);
        }

        // receiver info
        // -------------
        $arr = array(
            'mgr_name'   => $user['user']['name'],
            'mgr_email'  => $user['user']['email'],
            'leave_name' => $leave_name,
            'type_name'  => $type_name,
            'attach'     => $attach            
        );

        // get email content
        // -----------------
        $arr_body = array(
            'type_name'  => $type_name,
            'leave_name' => $leave_name,
            'leave'      => $leave,
            'action_by'  => $action_by
        );

        // send email notification
        // -----------------------
        $this->email_repo->getLeaveAddCreate($arr_body, $arr);
        return true;
    }

    // get leave add info
    // ------------------
    public function getLeaveAddInfo($id)
    {
        $q = \IhrV2\Models\LeaveAdd::where('id', $id)
            ->with(array('LeaveTypeName', 'ActionByDetail'))
            ->first();
        return $q;
    }

    // check user by id and sitecode and return status
    // -----------------------------------------------
    public function getCheckUserActive($uid, $sitecode)
    {
        // initialize variables
        // --------------------
        $user = array();
        $job = 0;
        $contract = 0;
        $expired = 0;
        $rm = array();

        // check user by id and sitecode
        // -----------------------------
        $query = $this->user_repo->getUserByIDSitecode($uid, $sitecode);
        if (!empty($query)) {
            $user = $query->toArray();

            // check user job
            // --------------
            if (!empty($user['user_latest_job'])) {
                $job = 1;
            }

            // check user contract
            // -------------------
            if (!empty($user['user_latest_contract'])) {
                $contract = 1;
            }

            // check whether contract is expired or not
            // ----------------------------------------
            if ($contract == 1) {

                // get date contract
                // -----------------
                $date_to = $user['user_latest_contract']['date_to'];

                // check expired
                // -------------
                $chk_expired = $this->CheckIfExpired($date_to);
                if ($chk_expired == 1) {
                    $expired = 1;
                }
            }
        }

        // user not found
        // --------------
        if (empty($user)) {
            abort(404);
        }

        // return status user
        // ------------------
        return array(
            'user' => $user
        );
    }

    // get detail reimburse/deduct leave
    // ---------------------------------
    public function getReimburseDeductDetail($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\LeaveAdd::where([
            'id'       => $id,
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'status'   => 1
        ])
            ->with(array('LeaveTypeName', 'ActionByDetail'))
            ->firstOrFail();
        return $q;
    }

    // save backdated leave
    // --------------------
    public function getApplyBackdatedLeave($data, $uid, $sitecode)
    {
        // get contract info
        // -----------------
        $contract_id = $data['chk_contract'];
        $contract = $this->user_repo->getUserContractByID($contract_id);

        // get date from
        // -------------
        $from = Carbon::createFromFormat('d/m/Y', $data['date_from'])->format('Y-m-d');

        // get date to
        // -----------
        $to = Carbon::createFromFormat('d/m/Y', $data['date_from'])->addDays($data['no_day'])->subDay(1)->toDateTimeString();

        // get date range
        // --------------
        $duration = $this->DateRange($from, $to);

        // get region manager
        // ------------------
        $rm = $this->user_repo->getRegionBySitecode($sitecode);
        $rm_info = $rm['region_manager_info']['region_manager_detail'];
        $report_to = $rm['region_manager_info']['user_id'];

        // insert leave_applications
        // -------------------------
        $arr_leave = array(
            'user_id'       => $uid,
            'contract_id'   => $contract_id,
            'leave_type_id' => $data['leave_type_id'],
            'report_to'     => $report_to,
            'date_from'     => $from,
            'date_to'       => $to,
            'is_half_day'   => 0,
            'desc'          => $data['desc'],
            'sitecode'      => $sitecode
        );
        $leave_id = $this->db_leave_repo->dbInsertLeaveApplication($arr_leave);

        // loop apply dates
        // ----------------
        foreach ($duration as $date) {

            // check leave dates
            // -----------------
            $chk_ldate = $this->getLeaveDateOne($uid, $leave_id, $date, 1);

            // no record yet
            // -------------
            if (empty($chk_ldate)) {

                // insert leave dates
                // ------------------
                $this->db_leave_repo->dbInsertLeaveDate($uid, $leave_id, $date, 1);
            }
        }

        // check history inactive
        // ----------------------
        $chk_histx = $this->getLeaveHistInactive($uid, $leave_id);
        if (empty($chk_histx)) {

            // insert leave histories pending with inactive status
            // ---------------------------------------------------
            $this->db_leave_repo->dbInsertLeaveHistInactive($uid, $leave_id);
        }

        // check leave histories
        // ---------------------
        $chk_history = $this->getCheckLeaveHistory($uid, $leave_id, 2);
        if (empty($chk_history)) {

            // insert leave histories approved (automatically approve)
            // this leave not have approve/reject
            // ----------------------------------
            $arr = array(
                'user_id'     => $uid,
                'leave_id'    => $leave_id,
                'action_date' => date('Y-m-d H:i:s'),
                'action_by'   => $report_to,
                'remark'      => 'Approved Backdated Leave',
                'status'      => 2, // Approve
                'flag'        => 1
            );
            $this->db_leave_repo->dbInsertLeaveHistoryArray($arr);
        }

        // check leave approves
        // --------------------
        $chk_approve = $this->getCheckLeaveApprove($uid, $leave_id);
        if (empty($chk_approve)) {

            // insert leave_approves
            // ---------------------
            $arr = array(
                'user_id'       => $uid,
                'leave_id'      => $leave_id,
                'leave_type_id' => $data['leave_type_id'],
                'contract_id'   => $contract_id,
                'date_action'   => date('Y-m-d H:i:s'),
                'action_by'     => $report_to,
                'date_value'    => count($duration),
                'flag'          => 1
            );
            $this->db_leave_repo->dbInsertLeaveApprove($arr);
        }

        // check if have attachment
        // ------------------------
        $attach = array();
        if (!empty($data['leave_file'])) {

            // upload file attachment
            // ----------------------
            $attach = $this->upload_repo->uploadFileLeave($data['leave_file'], $leave_id, $uid);
        }

        // get user info
        // -------------
        $user = $this->user_repo->getUserCheckIDSitecode($uid, $sitecode);

        // get leave type name
        // -------------------
        $leave_name = $this->getLeaveTypeName($data['leave_type_id']);

        // receiver info
        // -------------
        $body = array(
            'mgr_name'   => $user->name,
            'mgr_email'  => $user->email,
            'sitecode'   => $sitecode,
            'rm_email'   => $rm_info['email'],
            'rm_name'    => $rm_info['name'],
            'leave_name' => $leave_name->name,
            'attach'     => $attach
        );

        // get job info
        // ------------
        $job = $this->user_repo->getUserJobIDSitecode($uid, $sitecode);

        // set email content
        // -----------------
        $arr_body = array(
            'name'          => $user->name,
            'position_name' => $job->PositionName->name,
            'site_name'     => $rm['code'] . '&nbsp;' . $rm['name'],
            'leave'         => $this->getLeaveDetail($leave_id),
            'leave_name'    => $leave_name->name,
            'duration'      => count($duration),
            'created_by'    => auth()->user()->name,
            'contract'      => $contract->date_from . ' - ' . $contract->date_to
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailLeaveBackdated($arr_body, $body);

        // return result
        // -------------
        return array('Backdated Leave successfully added.', 'success');
    }

    // check if history of pending inactive
    // ------------------------------------
    public function getLeaveHistInactive($uid, $leave_id)
    {
        $q = \IhrV2\Models\LeaveHistory::where([
            'user_id'  => $uid,
            'leave_id' => $leave_id,
            'status'   => 1,
            'flag'     => 0
        ])
            ->first();
        return $q;
    }

    // display all leave of lite version
    // ---------------------------------
    public function getLeaveAllLite($codes)
    {
        $q = \IhrV2\Models\LeaveApplication::where(['active' => 1])
            ->with(array('LeaveUserDetail', 'LeaveSiteName', 'LeaveTypeName'))
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    // get total reimburse of unplan limit
    // -----------------------------------
    public function getReimburseUnplanLimit($arr)
    {
        $q = \IhrV2\Models\LeaveUnplanLimit::where([
            'user_id'  => $arr[0],
            'sitecode' => $arr[1],
            'status'   => 1
        ])
            ->whereDate('action_date', '>=', $arr[2])
            ->whereDate('action_date', '<=', $arr[3])
            ->with(array('UserActionBy'))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get unplan limit by id
    // ----------------------
    public function getUnplanLimitByID($id)
    {
        $q = \IhrV2\Models\LeaveUnplanLimit::where('id', $id)
            ->with(array('UserActionBy'))
            ->first();
        return $q;
    }

    // process new reimburse unplan limit
    // ----------------------------------
    public function getProReimburseUnplan($i, $uid, $sitecode)
    {

        // save into db
        // ------------
        $id = $this->db_leave_repo->dbInsertUnplanLimit($i, $uid, $sitecode);

        // callback record
        // ---------------
        $limit = $this->getUnplanLimitByID($id)->toArray();

        // get action by name
        // ------------------
        $action_by = $limit['user_action_by']['name'];

        // get user info
        // -------------
        $user = $this->user_repo->getUserJobNContract($uid)->toArray();

        // email body info
        // ---------------
        $arr_body = array(
            'limit'     => $limit,
            'action_by' => $action_by
        );

        // set email header
        // ----------------
        $arr = array(
            'mgr_name'  => $user['name'],
            'mgr_email' => $user['email'],
            'total'     => $limit['total']
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailReimburseUnplan($arr_body, $arr);

        // return message
        // --------------
        return array('success', 'Limit Unplan successfully Added.');
    }

    // get total taken unpaid current year
    // -----------------------------------
    public function getSumUnpaidYear($uid, $sitecode, $leave_type_id, $year)
    {
        $cond = array($uid, $sitecode, $leave_type_id, $year);
        $query = \IhrV2\Models\LeaveApprove::selectRaw('SUM(date_value) AS value')
            ->whereHas('LeaveInfo', function ($x) use ($cond) {
                $x->where([
                    'user_id'       => $cond[0],
                    'sitecode'      => $cond[1],
                    'leave_type_id' => $cond[2],
                    'active'        => 1
                ]);
                $x->whereYear('date_from', '=', $cond[3]);
            })
            ->where([
                'user_id'       => $uid,
                'leave_type_id' => $leave_type_id,
                'flag'          => 1
            ])
            ->groupBy('user_id', 'leave_type_id')
            ->first();
        return $query;
    }

    // get dates of selected day types
    // -------------------------------
    public function getDateBetweenDates($startDate, $endDate, $day_number)
    {
        $endDate = strtotime($endDate);
        $days = array(
            '1' => 'Monday',
            '2' => 'Tuesday',
            '3' => 'Wednesday',
            '4' => 'Thursday',
            '5' => 'Friday',
            '6' => 'Saturday',
            '7' => 'Sunday'
        );
        for ($i = strtotime($days[$day_number], strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i)) {
            $date_array[] = date('Y-m-d', $i);
        }
        return $date_array;
    }

    // get lists half day
    // ------------------
    public function getListHalfDay()
    {
        return array('' => '[Day]') + array(1 => 'Half Day', 0 => 'Full Day');
    }

    // get leave summary user
    // ----------------------
    public function getLeaveSummaryUser($request = null, $uid, $sitecode)
    {
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Leave',
            'child-a' => route('mod.leave.index'),
            'sub'     => 'Summary',
            'icon'    => 'calculator',
            'title'   => 'Summary'
        );

        // get user info
        // -------------
        $user = $this->getCheckUserActive($uid, $sitecode);
        $name = $user['user']['name'];

        // get job info
        // ------------
        $job = $user['user']['user_latest_job'];
        $position = $job['position_name']['name'];
        $join_date = $job['join_date'];

        // get site info
        // -------------
        $site = $user['user']['site_name_info'];
        $sname = $site['name'];

        // get contract info
        // -----------------
        if (empty($request)) {
            $contract = $user['user']['user_latest_contract'];
            $prev = false;
        }

        // select specific contract
        // ------------------------
        else {
            $contract = $this->user_repo->getUserContractByID($request->contract_id)->toArray();
            $prev = true;
        }

        // get contract details
        // --------------------
        $contract_id = $contract['id'];
        $date_from = $contract['date_from'];
        $date_to = $contract['date_to'];
        $duration = $this->DateRange($date_from, $date_to);

        // get summary lists
        // -----------------
        $lists = $this->getLeaveSummaryEach($uid, $sitecode, $contract_id, $date_from, $date_to, $prev);

        // get display info
        // ----------------
        $info = array(
            'uid'       => $uid,
            'sitecode'  => $sitecode,
            'name'      => $name,
            'position'  => $position,
            'sitename'  => $sname,
            'date_from' => $date_from,
            'date_to'   => $date_to,
            'join_date' => $join_date
        );

        // get lists of contracts
        // ----------------------
        $contracts = $this->user_repo->getUserContractDrop($uid, $sitecode);
        return View('modules.leave.user.summary', compact('header', 'duration', 'lists', 'info', 'contracts', 'contract_id'));
    }
}
