<?php
namespace IhrV2\Repositories\User;

use Carbon\Carbon;
use Crypt;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\User\DbUserInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Contracts\Download\DownloadInterface;
use IhrV2\Helpers\CommonHelper;
use IhrV2\User;
use Illuminate\Contracts\Encryption\DecryptException;

class UserRepository implements UserInterface
{
    protected $db_user_repo;
    protected $email_repo;
    protected $upload_repo;
    protected $down_repo;

    public function __construct(DbUserInterface $db_user_repo, EmailInterface $email_repo, UploadInterface $upload_repo, DownloadInterface $down_repo)
    {
        $this->db_user_repo = $db_user_repo;
        $this->email_repo = $email_repo;
        $this->upload_repo = $upload_repo;
        $this->down_repo = $down_repo;
    }

    // get lists of users
    // ------------------
    public function getUserIndex($request = null)
    {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Staff',
            'child-a' => route('mod.user.index'),
            'icon'    => 'people',
            'title'   => 'Staff'
        );

        // check request / pagination
        // --------------------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $group_id = CommonHelper::checkSession('group_id');
            $position_id = CommonHelper::checkSession('position_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $status_id = CommonHelper::checkSession('status_id');
            $contract_id = CommonHelper::checkSession('contract_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // check if reset searching
            // ------------------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                $inputs = array('site_id', 'group_id', 'position_id', 'region_id', 'phase_id', 'status_id', 'contract_id', 'keyword');
                session()->forget($inputs);
                return redirect()->route('mod.user.index');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $group_id = CommonHelper::checkSessionPost($request->group_id, $reset, 'group_id');
            $position_id = CommonHelper::checkSessionPost($request->position_id, $reset, 'position_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $contract_id = CommonHelper::checkSessionPost($request->contract_id, $reset, 'contract_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];
        $groups = $chk['groups'];

        // query all users
        // ---------------
        $arr = array($site_id, $phase_id, $group_id, $position_id, $status_id, $contract_id, $keyword, $region_id);
        $users = $this->getUserIndexListAll($arr, $codes);

        // data drop down
        // --------------
        $thegroups = $this->getGroupList($groups);
        $positions = $this->getPositionListDistinct($groups);
        $status = $this->getUserStatusList();
        $phases = $this->getPhaseList();
        $contracts = $this->getContractSession();
        $regions = $this->getRegionList();

        // search sessions
        // ---------------
        $sessions = array(
            'site_id'     => $site_id,
            'phase_id'    => $phase_id,
            'region_id'   => $region_id,
            'group_id'    => $group_id,
            'position_id' => $position_id,
            'status_id'   => $status_id,
            'contract_id' => $contract_id,
            'keyword'     => $keyword
        );
        return View('modules.user.index', compact(
            'header',
            'users',
            'sites',
            'thegroups',
            'positions',
            'status',
            'phases',
            'contracts',
            'regions',
            'sessions'
        ));
    }

    // get user record by phases
    // -------------------------
    public function getUserGroupByPhase($month, $year, $status)
    {
        // get user record
        // ---------------
        $q = \IhrV2\User::where(['group_id' => 3])
        // ->where(function ($k) use ($codes) {
        //     if (count($codes) > 0) {
        //         foreach ($codes as $code) {
        //             $k->orWhere('sitecode', 'like', $code . '%');
        //         }
        //     }
        // }) 
        ->where(function ($s) use ($status) {
            if (!empty($status)) {
                $s->where('status', $status);
            }
        })       
        ->whereHas('UserLatestJob', function ($y) use ($year) {
            if (!empty($year)) {
                $y->whereYear('join_date', '=', $year);
            }
        })
        ->whereHas('UserLatestJob', function ($m) use ($month) {
            if (!empty($month)) {
                $m->whereMonth('join_date', '=', $month);
            }
        })
        ->with(array('UserEducationOne', 'UserLatestContract'))
        ->with(array('SiteNameInfo' => function ($s) {
            $s->with('StateName');
        }))            
        ->with(array('UserLatestJob' => function ($h) {
            $h->with('PositionName');
        }))
        ->whereNotIn('sitecode', array('X01C001'))
        ->orderBy('sitecode', 'asc')
        ->get();
        return $q;
    }

    // lists all user photo
    // --------------------
    public function getUserPhoto($request = null)
    {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Photo',
            'child-a' => route('mod.user.photo'),
            'icon'    => 'picture',
            'title'   => 'Photo'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $status_id = CommonHelper::checkSession('status_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget('site_id');
                session()->forget('region_id');
                session()->forget('status_id');
                session()->forget('keyword');
                return redirect()->route('mod.user.photo');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get all photos
        // --------------
        $arr = array($site_id, $region_id, $status_id, $keyword);
        $photos = $this->getUserPhotoIndex($arr, $codes);

        // get drop down lists
        // -------------------
        $status = $this->getUserPhotoStatus();
        $regions = $this->getRegionList();

        // set sessions
        // ------------
        $sessions = array('site_id' => $site_id, 'region_id' => $region_id, 'status_id' => $status_id, 'keyword' => $keyword);
        return view('modules.user.photo.index', compact('header', 'sites', 'photos', 'status', 'regions', 'sessions'));
    }

    // get all user other
    // ------------------
    public function getUserOther($request = null)
    {
        $header = array(
            'parent'  => 'Staff Administration',
            'child'   => 'Management',
            'child-a' => route('mod.user.other'),
            'icon'    => 'people',
            'title'   => 'Management'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $group_id = CommonHelper::checkSession('group_id');
            $position_id = CommonHelper::checkSession('position_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget('group_id');
                session()->forget('position_id');
                session()->forget('keyword');
                return redirect()->route('mod.user.other');
            }

            // check session
            // -------------
            $group_id = CommonHelper::checkSessionPost($request->group_id, $reset, 'group_id');
            $position_id = CommonHelper::checkSessionPost($request->position_id, $reset, 'position_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get other users
        // ---------------
        $arr = array($group_id, $position_id, $keyword);
        $others = $this->getUserOtheAll($arr);
        // dd($others->toArray());

        // data drop down
        // --------------
        $thegroups = $this->getGroupList();
        $positions = $this->getPositionListDistinct();

        // search sessions
        // ---------------
        $sessions = array(
            'group_id'    => $group_id,
            'position_id' => $position_id,
            'keyword'     => $keyword
        );
        return view('modules.user.other', compact('header', 'others', 'thegroups', 'positions', 'sessions'));
    }

    public function getUserCheckIDSitecode($uid, $sitecode)
    {
        $q = User::where(['id' => $uid, 'sitecode' => $sitecode])
            ->with(array('UserLatestJob', 'SiteNameInfo'))
            ->firstOrFail();
        return $q;
    }

    public function getUserByICSitecode($icno, $sitecode)
    {
        return \IhrV2\User::with(array('UserLatestJob', 'SiteNameInfo'))
            ->where([
                'icno'     => $icno,
                'sitecode' => $sitecode
            ])
            ->first();
    }

    public function getUserSiteActive($icno, $sitecode)
    {
        $q = \IhrV2\User::with(array('UserLatestJob', 'SiteNameInfo'))
            ->where([
                'icno'     => $icno,
                'sitecode' => $sitecode,
                'group_id' => 3,
                'status'   => 1
            ])
            ->first();
        return $q;
    }

    // get staff info to update manual staff detailk aggregator
    // --------------------------------------------------------
    public function getUserAggUpdateManual($data)
    {
        dd($data);        
        // $q = \IhrV2\User::where([''])
    }

    // get current user job (get active staff priority = status 1)
    // -----------------------------------------------------------
    public function getUserSiteCurrent($icno, $sitecode)
    {
        $q = \IhrV2\User::with(array('UserCurrentJob2', 'UserCurrentContract2', 'SiteNameInfo'))
            ->where([
                'icno'     => $icno,
                'sitecode' => $sitecode,
                'group_id' => 3,
            ])
            ->whereHas('UserCurrentJob2', function ($j) {
                $j->whereIn('position_id', array(4,5));
            })
            ->orderBy('status', 'asc')
            ->first();
        return $q;
    }

    // get user site by icno and sitecode
    // ----------------------------------
    public function getUserSite($icno, $sitecode)
    {
        $q = \IhrV2\User::with(array('UserLatestJob', 'SiteNameInfo'))
            ->where([
                'icno'     => $icno,
                'sitecode' => $sitecode,
                'group_id' => 3
            ])
            ->first();
        return $q;
    }

    // check user contract from session
    // --------------------------------
    public function getUserContractSession($uid)
    {
        if (empty(session()->get('contract'))) {
            $contract = $this->getUserContractByUserIDStatus($uid);
        } else {
            $contract = session()->get('contract');
        }
        return $contract;
    }

    public function getUserPositionSession()
    {
        if (empty(session()->get('position_name'))) {
            $q = \IhrV2\Models\Position::where('id', session()->get('job')['position_id'])->first();
            $position = $q->name;
        } else {
            $position = session()->get('position_name');
        }
        return $position;
    }

    public function getuserSiteNameSession()
    {
        if (empty(session()->get('site_name'))) {
            $q = \IhrV2\Models\Site::where('code', auth()->user()->sitecode)->first();
            $sitename = $q->name;
        } else {
            $sitename = session()->get('site_name');
        }
        return $sitename;
    }

    public function gtUserActiveInactive($codes)
    {
        $q = \IhrV2\User::where('group_id', 3)
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    $k->whereIn('sitecode', $codes);
                }
            })
            ->paginate(20);
        return $q;
    }

    // delete user contract
    // --------------------
    public function dbDeleteUserContract()
    {
        $this->delete();
        $msg = array('Contract successfully deleted.', 'success', 'mod.user.index');
        return $msg;
    }

    public function getContractSession()
    {
        // $arr = array('' => '[Contract]', 1 => 'Active', 2 => 'Ended', 3 => 'No Contract');
        $arr = array('' => '[Contract]', 1 => 'Active', 2 => 'Ended');
        return $arr;
    }

    public function getPositionData($id)
    {
        $positions = \IhrV2\Models\Position::where('group_id', $id)->orderBy('name', 'asc')->get();
        return response()->json($positions);
    }

    // check user id and user token
    // ----------------------------
    public function getUserByIDToken($id, $token)
    {
        return User::where('id', $id)->where('api_token', $token)->firstOrFail();
    }

    // get current user job (1 = active | 2 = inactive)
    // ------------------------------------------------
    public function getUserJobByUserIDToken($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserJob::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->where(['flag' => 1])
        ->with(array('SiteName', 'PositionName', 'PhaseName', 'RegionName'))
        ->first();
        return $query;
    }

    // get previous user job (inactive and not current job)
    // ----------------------------------------------------
    public function getUserJobPrevByUserIDToken($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserJob::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->where(['status' => 2, 'flag' => 0])
        ->with(array('SiteName', 'PositionName', 'PhaseName', 'RegionName'))
        ->orderBy('id', 'desc')
        ->get();
        return $query;
    }

    // get active user contract
    // ------------------------
    public function getActiveContractUIDToken($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserContract::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->where(['status' => 1, 'flag' => 1])
        ->orderBy('date_to', 'desc') // get the latest
        ->first();
        return $query;
    }

    // get active duplicate contract
    // -----------------------------
    public function getDuplicateContract($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserContract::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->where(['status' => 1, 'flag' => 1])
        ->orderBy('date_to', 'desc')
        ->get();
        return $query;
    }

    // get previous user contract
    // --------------------------
    public function getPrevContractUIDToken($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserContract::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->where(['status' => 2, 'flag' => 0])
        ->with(array('LeaveApplication'))
        ->orderBy('id', 'desc')
        ->get();
        return $query;
    }

    // get previous user contract by pagination
    // ----------------------------------------
    public function getPrevContractByPaginate($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserContract::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->where(['status' => 2, 'flag' => 0])
        ->orderBy('date_to', 'desc')
        ->paginate(5);
        return $query;
    }

    public function getUserEducationByUserIDToken($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserEducation::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->orderBy('id', 'desc')
        ->get();
        return $query;
    }

    public function getUserEmergencyByUserIDToken($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserEmergency::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->orderBy('id', 'desc')
        ->get();
        return $query;
    }

    public function getUserPhotoByUserIDToken($uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserPhoto::whereHas(
            'UserDetail',
            function ($x) use ($filters) {
                foreach ($filters as $column => $key) {
                    if (!is_null($key)) {
                        $x->where($column, $key);
                    }
                }
            }
        )
        ->with(array('PhotoApproved'))
        ->where([
            'user_id' => $uid,
            'active'  => 1
        ])
        ->first();
        return $query;
    }

    // check user id
    // -------------
    public function getUserByID($id)
    {
        $q = \IhrV2\User::find($id);
        return $q;
    }

    // region manager name
    // -------------------
    public function getRegionManager($sitecode)
    {
        $x = \IhrV2\Models\Site::select('code', 'region_id')
            ->where('code', $sitecode)
            ->with(array('RegionName' => function ($h) {
                $h->with(array('RegionManager' => function ($u) {
                    $u->with(array('RegionManagerDetail'));
                }));
            }))
            ->first();
        return $x;
    }

    // get region manager by sitecode
    // ------------------------------
    public function getRegionBySitecode($sitecode)
    {
        $q = \IhrV2\Models\Site::where('code', $sitecode)
            ->with(array('RegionManagerInfo' => function ($h) {
                $h->with(array('RegionManagerDetail'));
            }))
            ->first()
            ->toArray();
        return $q;
    }

    // get user contract id
    // --------------------
    public function getUserContractByID($id)
    {
        return \IhrV2\Models\UserContract::find($id);
    }

    // get user contract by id, and user info
    // --------------------------------------
    public function getUserContractByUser($uid, $sitecode, $contract_id)
    {
        $q = \IhrV2\Models\UserContract::where(['id' => $contract_id, 'user_id' => $uid, 'sitecode' => $sitecode])->first();
        return $q;
    }

    public function getUserContractByUserIDStatus($user_id)
    {
        return \IhrV2\Models\UserContract::where('user_id', $user_id)->where('status', 1)->first();
    }

    // get user contract by user id
    // ----------------------------
    public function getUserContractByUid($uid)
    {
        return \IhrV2\Models\UserContract::where('user_id', $uid)->get();
    }

    // get user contract by user id and date contract
    // ----------------------------------------------
    public function getContractUserIDDate($uid, $from, $to)
    {
        $q = \IhrV2\Models\UserContract::where([
            'user_id'   => $uid,
            'date_from' => $from,
            'date_to'   => $to,
            'status'    => 1
        ])->first();
        return $q;
    }

    // get user contract by user id and sitecode
    // -----------------------------------------
    public function getContractByIDSitecode($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserContract::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'status'   => 1
        ])
            ->first();
        return $q;
    }

    // get user info with job and contract info
    // ----------------------------------------
    public function getUserJobNContract($uid)
    {
        $q = \IhrV2\User::with(array('UserLatestJob', 'UserLatestContract'))
            ->where([
                'id'     => $uid,
                'status' => 1
            ])
            ->first();
        return $q;
    }

    // get user info by staff id and sitecode
    // --------------------------------------
    public function getUserByStaffIDSitecode($staff_id, $sitecode)
    {
        $q = \IhrV2\User::with(array('UserLatestJob', 'UserLatestContract'))
            ->where([
                'staff_id' => $staff_id,
                'sitecode' => $sitecode,
                'status'   => 1
            ])
            ->first();
        return $q;
    }

    // get user info with job and contract info
    // ----------------------------------------
    public function getUserByIDSitecode($uid, $sitecode)
    {
        $q = \IhrV2\User::with(array('UserLatestContract', 'SiteNameInfo'))
            ->where([
                'id'       => $uid,
                'sitecode' => $sitecode,
                'group_id' => 3,
                'status'   => 1
            ])
            ->with(array('UserLatestJob' => function ($h) {
                $h->with('PositionName');
            }))
            ->first();
        return $q;
    }

    public function getUserContractWithUser($id, $uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserContract::whereHas('UserDetail', function ($x) use ($filters) {
            foreach ($filters as $column => $key) {
                if (!is_null($key)) {
                    $x->where($column, $key);
                }
            }
        })
            ->where('id', $id)
            ->firstOrFail();
        return $query;
    }

    // user job
    public function getUserJobWithUser($id, $uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserJob::whereHas('UserDetail', function ($x) use ($filters) {
            foreach ($filters as $column => $key) {
                if (!is_null($key)) {
                    $x->where($column, $key);
                }
            }
        })
            ->with('UserDetail')
            ->where('id', $id)
            ->firstOrFail();
        return $query;
    }

    public function getUserJobID($id)
    {
        return \IhrV2\Models\UserJob::find($id);
    }

    public function getUserJobByID($id)
    {
        return \IhrV2\Models\UserJob::where('user_id', $id)->where('status', 1)->firstOrFail();
    }

    public function getUserJobIDSitecode($uid, $sitecode)
    {
        return \IhrV2\Models\UserJob::with(array('PositionName'))
            ->where('user_id', $uid)
            ->where('sitecode', $sitecode)
            ->firstOrFail();
    }

    // user education
    public function getUserEducationWithUser($id, $uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserEducation::whereHas('UserDetail', function ($x) use ($filters) {
            foreach ($filters as $column => $key) {
                if (!is_null($key)) {
                    $x->where($column, $key);
                }
            }
        })
            ->where('id', $id)
            ->firstOrFail();
        return $query;
    }

    // user emergency contact
    public function getUserEmergencyWithUser($id, $uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserEmergency::whereHas('UserDetail', function ($x) use ($filters) {
            foreach ($filters as $column => $key) {
                if (!is_null($key)) {
                    $x->where($column, $key);
                }
            }
        })
            ->where('id', $id)
            ->firstOrFail();
        return $query;
    }

    // user photo
    public function getUserPhotoWithUser($id, $uid, $token)
    {
        $filters = array('id' => $uid, 'api_token' => $token);
        $query = \IhrV2\Models\UserPhoto::whereHas('UserDetail', function ($x) use ($filters) {
            foreach ($filters as $column => $key) {
                if (!is_null($key)) {
                    $x->where($column, $key);
                }
            }
        })
            ->where('id', $id)
            ->firstOrFail();
        return $query;
    }

    // user status
    public function getUserStatusByID($id)
    {
        return \IhrV2\Models\UserStatus::find($id);
    }

    public function getUserStatusList()
    {
        return \IhrV2\Models\UserStatus::whereNotIn('id', array(3))->orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[User Status]', '');
    }

    public function getUserStatusActive()
    {
        return \IhrV2\Models\UserStatus::whereNotIn('id', array(3))->orderBy('name', 'ASC')->pluck('name', 'id');
    }

    // user contract status
    public function getUserContractStatusList()
    {
        return \IhrV2\Models\UserContractStatus::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Contract Status]', '');
    }

    public function getUserContractStatusByID($id)
    {
    }

    // district
    public function getDistrictList()
    {
        return \IhrV2\Models\District::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[District]', '');
    }

    // district name
    public function getDistrictNameList()
    {
        return \IhrV2\Models\District::orderBy('name', 'ASC')->pluck('name', 'name')->prepend('[District]', '');
    }

    public function getDistrictByID($id)
    {
        return \IhrV2\Models\District::find($id);
    }

    // gender
    public function getGenderList()
    {
        return \IhrV2\Models\Gender::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Gender]', '');
    }

    public function getGenderByID($id)
    {
        return \IhrV2\Models\Gender::find($id);
    }

    // marital status
    public function getMaritalStatusList()
    {
        return \IhrV2\Models\MaritalStatus::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Marital Status]', '');
    }

    public function getMaritalStatusByID($id)
    {
        return \IhrV2\Models\MaritalStatus::find($id);
    }

    // month
    public function getMonthList()
    {
        return \IhrV2\Models\Month::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Month]', '');
    }

    public function getMonthList2()
    {
        return \IhrV2\Models\Month::orderBy('no', 'ASC')->pluck('name', 'no')->prepend('[Month]', '');
    }

    public function getMonthByID($id)
    {
        return \IhrV2\Models\Month::find($id);
    }

    // mukim
    public function getMukimList()
    {
        return \IhrV2\Models\Mukim::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Mukim]', '');
    }

    public function getMukimByID($id)
    {
        return \IhrV2\Models\Mukim::find($id);
    }

    // nationality
    public function getNationalityList()
    {
        return \IhrV2\Models\Nationality::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Nationality]', '');
    }

    public function getNationalityByID($id)
    {
        return \IhrV2\Models\Nationality::find($id);
    }

    // occupation
    public function getOccupationList()
    {
        return \IhrV2\Models\Occupation::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Occupation]', '');
    }

    public function getOccupationByID($id)
    {
        return \IhrV2\Models\Occupation::find($id);
    }

    // group
    public function getGroupList($groups = null)
    {
        if (!empty($groups)) {
            $i = \IhrV2\Models\Group::whereIn('id', $groups)->pluck('name', 'id');
        } else {
            $i = \IhrV2\Models\Group::pluck('name', 'id');
        }
        $i->prepend('[Group]', '');
        return $i;
    }

    public function getGroupNotIn($groups)
    {
        return \IhrV2\Models\Group::whereNotIn('id', $groups)->pluck('name', 'id')->prepend('[Group]', '');
    }

    public function getGroupByID($id)
    {
        return \IhrV2\Models\Group::find($id);
    }

    // lists of phases
    // ---------------
    public function getPhaseList()
    {

        // set exclude values
        // ------------------
        $excludes = [
            ['name', 'NOT LIKE', '%saifon%'],
            ['name', 'NOT LIKE', '%mini pjk%'],
            ['name', 'NOT LIKE', '%pjl%']
        ];

        // query phases
        // ------------
        $q = \IhrV2\Models\Phase::select(\DB::raw('concat (name, " : ", name_tm) as name, id'))
            ->where($excludes) // remove exclude values
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id')
            ->prepend('[Phase]', '');
        return $q;
    }

    public function getPhaseByID($id)
    {
        return \IhrV2\Models\Phase::find($id);
    }

    // lists of region
    // ---------------
    public function getRegionList()
    {
        // check if rm/drm/clerk
        // ---------------------
        $region_id = null;
        if (in_array(auth()->user()->group_id, array(4, 6, 7))) {
            $region_id = session()->get('job')['region_id'];
        }
        $q = \IhrV2\Models\Region::whereIn('id', [1,2,3,4,5,7,8]) // except borneo
        ->where(function ($p) use ($region_id) {
            if (!empty($region_id)) {
                $p->where('id', $region_id);
            }
        })
        ->orderBy('name', 'ASC')
        ->pluck('name', 'id')
        ->prepend('[Region]', '');
        return $q;
    }

    public function getRegionInfo($id)
    {
        return \IhrV2\Models\Region::find($id);
    }

    public function getRegionByID($id)
    {
        $q = \IhrV2\Models\Region::where('id', $id)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->prepend('[Region]', '');
        return $q;
    }

    // get lists of region manager by region id
    // ----------------------------------------
    public function getRmByRegionID($region_id)
    {
        $rm = \IhrV2\User::whereHas('UserLatestJob', function ($j) use ($region_id) {
            $j->where('region_id', $region_id);
            $j->where('position_id', 1); // position 'Region Manager'
        })
            ->where('group_id', 4)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->prepend('[Region Manager]', '');
        return $rm;
    }

    // site
    public function getSiteListWithID($region_id = null)
    {
        $arr = array();
        if (!empty($region_id)) {
            $arr = array_add($arr, 'region_id', $region_id);
        }
        $cond = array('mini', 'pjk');
        $q = \IhrV2\Models\Site::select(\DB::raw('concat (code, " - ", name) as name, code'))
            ->where($arr)
            ->where('status', 1)
            ->where('name', 'NOT LIKE', '%mini pjk%') // remove mini pjk from the lists
            ->orderBy('code', 'ASC')
            ->pluck('name', 'code')
            ->prepend('[Site]', '');
        return $q;
    }

    // get lists sites by codes
    // ------------------------
    public function getListSiteByCode($codes = null)
    {
        $q = \IhrV2\Models\Site::select(\DB::raw('concat (code, " - ", name) as name, code'))
            ->where(function ($c) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $c->orWhere('code', 'like', $code . '%');
                    }
                }
            })
            ->where([
                'status' => 1
            ])
            ->where('name', 'NOT LIKE', '%mini pjk%') // remove mini pjk from the lists
            ->orderBy('code', 'ASC')
            ->pluck('name', 'code')
            ->prepend('[Site]', '');
        return $q;
    }

    // get lists sites and codes
    // -------------------------
    public function getListSiteNCode()
    {
        $codes = array();
        $sites = array();
        $groups = array();

        // check if rm/drm/clerk
        // ---------------------
        if (in_array(auth()->user()->group_id, array(4, 6, 7))) {

            // get region id
            // -------------
            $region_id = session()->get('job')['region_id'];

            // get codes
            // ---------
            $check = $this->getRegionCode($region_id)->toArray();
            $codes = explode(':', $check['codes']);

            // get sites
            // ---------
            $sites = $this->getListSiteByCode($codes);

            // get groups
            // ----------
            $groups = array(3);
        }

        // show all sites
        // --------------
        else {
            $sites = $this->getListSiteByCode();
            // $groups = array(1, 2, 3, 4);
            $groups = array(3);
        }
        return array('codes' => $codes, 'sites' => $sites, 'groups' => $groups);
    }

    // get codes of region id
    // ----------------------
    public function getRegionCode($region_id)
    {
        $q = \IhrV2\Models\RegionUser::where('region_id', $region_id)
            ->first();
        return $q;
    }

    public function getSitecodeByRegion($region_id)
    {
        $i = \IhrV2\Models\Site::where('region_id', $region_id)->lists('name', 'code')->all();
        $codes = array();
        foreach ($i as $k => $v) {
            $codes[] = $k;
        }
        return $codes;
    }

    public function getSiteList()
    {
        return \IhrV2\Models\Site::orderBy('name', 'ASC')->pluck('name', 'code')->prepend('[Site]', '');
    }

    public function getSiteByID($id)
    {
        return \IhrV2\Models\Site::find($id);
    }

    // position
    public function getPositionByID($id)
    {
        return \IhrV2\Models\Position::find($id);
    }

    // get lists of position by value name
    // -----------------------------------
    public function getPositionListName($groups)
    {
        $q = \IhrV2\Models\Position::orderBy('name', 'asc')
            ->where(function ($g) use ($groups) {
                if (count($groups) > 0) {
                    $g->whereIn('group_id', $groups);
                }
            })
            ->pluck('name', 'name')
            ->prepend('[Position]', '');
        return $q;
    }

    public function getPositionListDistinct($groups = null)
    {
        // dd($groups);

        // get unique position id
        // ----------------------
        $p = \IhrV2\Models\UserJob::distinct()->lists('position_id');

        // dd($p);

        // query lists of position
        // -----------------------
        if (!empty($groups)) {
            $i = \IhrV2\Models\Position::whereIn('group_id', $groups)
                ->whereIn('id', $p)
                ->where('id', '!=', 106)
                ->orderBy('name', 'ASC')
                ->pluck('name', 'id')
                ->prepend('[Position]', '');
        } else {
            $i = \IhrV2\Models\Position::whereIn('id', $p)
                ->where('id', '!=', 106) // exclude senior programmer
                ->orderBy('name', 'ASC')
                ->pluck('name', 'id')
                ->prepend('[Position]', '');
        }
        return $i;
    }

    public function getPositionList($groups = null)
    {
        $i = \IhrV2\Models\Position::orderBy('name', 'ASC')
            ->where(function ($g) use ($groups) {
                if (count($groups) > 0) {
                    $g->whereIn('group_id', $groups);
                }
            })
            ->pluck('name', 'id')
            ->prepend('[Position]', '');
        return $i;
    }

    public function getPositionListByGroup($group_id)
    {
        return \IhrV2\Models\Position::where('group_id', $group_id)->orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Position]', '');
    }

    // project
    public function getProjectList()
    {
        return \IhrV2\Models\Project::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Project]', '');
    }

    public function getProjectByID($id)
    {
        return \IhrV2\Models\Project::find($id);
    }

    // race
    public function getRaceList()
    {
        return \IhrV2\Models\Race::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Race]', '');
    }

    public function getRaceByID($id)
    {
        return \IhrV2\Models\Race::find($id);
    }

    // religion
    public function getReligionList()
    {
        return \IhrV2\Models\Religion::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Religion]', '');
    }

    public function getReligionByID($id)
    {
        return \IhrV2\Models\Religion::find($id);
    }

    // skill level
    public function getSkillLevelList()
    {
        return \IhrV2\Models\SkillLevel::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Skill Level]', '');
    }

    public function getSkillLevelByID($id)
    {
        return \IhrV2\Models\SkillLevel::find($id);
    }

    // state
    public function getStateList()
    {
        return \IhrV2\Models\State::whereNotIn('code', [514, 515, 516])
            ->orderBy('name', 'ASC')->pluck('name', 'code')->prepend('[State]', '');
    }

    // state all
    public function getStateListAll()
    {
        return \IhrV2\Models\State::orderBy('name', 'ASC')->pluck('name', 'code')->prepend('[State]', '');
    }

    public function getStateByRegion($region_id)
    {
        $q = \IhrV2\Models\State::where('region_id', $region_id)
            ->orderBy('name', 'ASC')
            ->pluck('name', 'code')
            ->prepend('[State]', '');
        return $q;
    }

    public function getStateByID($id)
    {
        return \IhrV2\Models\State::find($id);
    }

    public function getStateByCode($code)
    {
        return \IhrV2\Models\State::where('code', $code)->first();
    }

    // off day type
    public function getOffDayTypeList()
    {
        return \IhrV2\Models\OffDayType::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Type]', '');
    }

    // status
    public function getStatusList()
    {
        return \IhrV2\Models\Status::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Status]', '');
    }

    // staff status
    public function getStaffStatusList()
    {
        return \IhrV2\Models\Status::orderBy('name', 'ASC')->pluck('name', 'id')->prepend('[Staff Status]', '');
    }

    public function getStatusByID($id)
    {
        return \IhrV2\Models\Status::find($id);
    }

    // get total user active and inactive
    // ----------------------------------
    public function getUserStatusAll($codes)
    {
        $q = \IhrV2\User::select(\DB::raw('COUNT(*) as total'), \DB::raw('(CASE WHEN status = 1 THEN "Active" WHEN status = 2 THEN "Inactive" ELSE "Transfer" END) AS status_l'))
            ->whereIn('group_id', array(3))
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->groupBy('status')
            ->get();
        return $q;
    }

    // get total sites by status
    // -------------------------
    public function getTotalSiteByStatus($codes)
    {
        $q = \IhrV2\Models\Site::select(\DB::raw('COUNT(*) as total'), \DB::raw('(CASE WHEN status = 1 THEN "Active" WHEN status = 2 THEN "Inactive" ELSE "Transfer" END) AS status_l'))
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('code', 'like', $code . '%');
                    }
                }
            })
            ->whereNotIn('code', array('X01C001'))
            ->groupBy('status')
            ->get();
        return $q;
    }

    // get total new user in current month and year
    // --------------------------------------------
    public function getUserTotalNewStaffInMonth($codes)
    {
        $q = \IhrV2\User::whereHas('UserLatestJob', function ($j) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $j->orWhere('sitecode', 'like', $code . '%');
                }
            }
            $j->whereYear('join_date', '=', date('Y'));
            $j->whereMonth('join_date', '=', date('m'));
            $j->where('region_id', 0);
            $j->where('status', 1);
        })
            ->where('group_id', 3)
            ->where('status', 1)
            ->count();
        return $q;
    }

    // get total end contract in current month and year
    // ------------------------------------------------
    public function getUserTotalEndConInMonth($codes)
    {
        $q = \IhrV2\User::whereHas('UserLatestContract', function ($j) use ($codes) {
            if (count($codes) > 0) {
                foreach ($codes as $code) {
                    $j->orWhere('sitecode', 'like', $code . '%');
                }
            }
            $j->whereYear('date_to', '=', date('Y'));
            $j->whereMonth('date_to', '=', date('m'));
            $j->where('status_contract_id', 1);
            $j->where('status', 1);
        })
            ->where('group_id', 3)
            ->where('status', 1)
            ->count();
        return $q;
    }

    public function getUserTotalPosition($codes)
    {
        $q = \IhrV2\Models\UserJob::select(\DB::raw('count(*) as total'), 'position_id')
            ->whereHas('UserDetail', function ($j) {
                $j->where('group_id', 3);
                $j->where('status', 1);
            })
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->where('status', 1)
            ->with(array('PositionName'))
            ->groupBy('position_id')
            ->get();
        return $q;
    }

    // get total site
    // --------------
    public function getUserTotalSite($codes)
    {
        $q = \IhrV2\Models\Site::where('status', 1)
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    $k->whereIn('code', $codes);
                }
            })
            ->count();
        return $q;
    }

    public function getUserIndexListAll($arr, $codes)
    {
        // get value from array
        // --------------------
        $site_id = $arr[0];
        $phase_id = $arr[1];
        $group_id = $arr[2];
        $position_id = $arr[3];
        $status_id = $arr[4];
        $contract_id = $arr[5];
        $keyword = $arr[6];
        $region_id = $arr[7];

        // set arrays
        // ----------
        $job = array('position_id' => $position_id, 'phase_id' => $phase_id, 'region_id' => $region_id);
        $cond = array('sitecode' => $site_id, 'group_id' => $group_id, 'status' => $status_id);

        // query users
        // -----------
        $user = User::with(
            array(
                'SiteNameInfo',
                'GroupName',
                'StatusName',
                'UserLatestContract'
            )
        )
            ->with(array('UserCurrentJob2' => function ($h) {
                $h->with('PositionName');
                $h->with('PhaseName');
                $h->with('RegionName');
            }))
            ->with(array('UserPhotoActive' => function ($s) {
                $s->with('PhotoLatestStatus');
            }))

            // initial sites by region
            // -----------------------
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })

            // search by (sitecode/group id/status)
            // ------------------------------------
            ->where(function ($s) use ($cond) {
                foreach ($cond as $column => $key) {
                    if (!is_null($key)) {
                        $s->where($column, $key);
                    }
                }
            })

            // search by job
            // -------------
            ->where(function ($j) use ($job) {
                $j->whereHas('UserCurrentJob2', function ($i) use ($job) {
                    foreach ($job as $column => $key) {
                        if (!is_null($key)) {
                            $i->where($column, $key);
                        }
                    }
                });
            })

            // search by contract
            // ------------------
            ->whereHas('UserLatestContract', function ($c) use ($contract_id) {
                if (!empty($contract_id)) {
                    if ($contract_id == 1) {
                        $c->whereDate('date_to', '>=', Carbon::now()->format('Y-m-d'));
                    } else {
                        $c->whereDate('date_to', '<', Carbon::now()->format('Y-m-d'));
                    }
                }
            })

            // search by keyword
            // -----------------
            ->where(function ($t) use ($keyword) {
                if (!empty($keyword)) {
                    $t->where('name', 'like', '%' . $keyword . '%');
                    $t->orWhere('icno', 'like', '%' . $keyword . '%');
                    $t->orWhere('username', 'like', '%' . $keyword . '%');
                    $t->orWhere('sitecode', 'like', '%' . $keyword . '%');
                }
            })
            ->where('is_admin', '!=', 1)
            ->whereNotIn('sitecode', array('X01C001'))
            ->where('group_id', 3)
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $user;
    }

    public function getUserJoinContract($uid, $sitecode)
    {
        $q = \IhrV2\User::whereHas(
            'UserLatestContract2',
            function ($c) use ($sitecode) {
                $c->where('sitecode', $sitecode);
            }
        )
            ->where([
                'id'       => $uid,
                'sitecode' => $sitecode,
                'group_id' => 3,
                'status'   => 1
            ])
            ->first();
        return $q;
    }

    public function getUserContractInactive($uid, $sitecode)
    {
        $q = \IhrV2\User::whereHas('UserLastContract', function ($c) use ($sitecode) {
            $c->where('sitecode', $sitecode);
        })
        ->where([
            'id'       => $uid,
            'sitecode' => $sitecode,
            'group_id' => 3
        ])
        ->with(array('UserLastContract'))
        ->first();
        return $q;
    }

    // check site supervisor partner
    // -----------------------------
    public function getUserPartner($uid, $sitecode, $position_id)
    {
        $q = \IhrV2\User::where([
            'sitecode' => $sitecode,
            'group_id' => 3,
            'status'   => 1
        ])
        ->whereNotIn('id', array($uid))
        ->whereHas('UserLatestJob', function ($j) use ($position_id) {
            $j->where('position_id', '!=', $position_id);
        })
        ->first();
        return $q;
    }

    public function getIfContractExpired()
    {
        // if (session()->get('expired') == 1) {
        //     return redirect()->route('message');
        // }
    }

    public function getUserPhotoByID($id)
    {
        return \IhrV2\Models\UserPhoto::find($id);
    }

    public function getUserPhotoActive($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserPhoto::with(array('PhotoPending', 'PhotoApproved'))
        ->where(['user_id' => $uid, 'sitecode' => $sitecode])
        ->where('active', 1)
        ->get()->toArray();
        return $q;
    }

    // site supervisor upload the photo
    // --------------------------------
    public function processUserPhoto($data, $uid, $sitecode, $upl_type)
    {
        // check existing photo
        // --------------------
        $check = $this->getUserPhotoActive($uid, $sitecode);

        // not exist yet
        // -------------
        if (empty($check) || empty($check['photo_pending']) || empty($check['photo_approved'])) {

            // upload the file into the server
            // -------------------------------
            $attach = $this->upload_repo->uploadUserPhoto($data['photo_file'], $uid, $sitecode);

            // insert photo histories
            // ----------------------
            $data = array(
                'action_by' => auth()->user()->id, 
                'type' => 1, // pending
                'remark' => null
            );
            $this->db_user_repo->dbInsertUserPhotoHistory($attach['id'], $uid, $data);

            // upload by site supervisor
            // -------------------------
            if ($upl_type == 1) {

                // get reporting officer
                // ---------------------
                $report_to = session()->get('rm_info');

                // generate url
                // ------------
                $com = $attach['id'] . '/';
                $com .= $uid . '/';
                $com .= auth()->user()->sitecode . '/';
                $com .= $report_to['id'];
                $url = Crypt::encrypt($com) . '?api_token=' . auth()->user()->api_token;

                // email body info
                // ---------------
                $arr_body = array(
                    'position_name' => session()->get('position_name'),
                    'site_name'     => session()->get('site_name'),
                    'icno'          => auth()->user()->icno,
                    'approve'       => route('api.user.photo.approve', array($url)),
                    'reject'        => route('api.user.photo.reject', array($url))
                );

                // receiver info
                // -------------
                $arr = array(
                    'rm_email' => $report_to['email'],
                    'rm_name'  => $report_to['name'],
                    'attach'   => $attach
                );

                // send email notification
                // -----------------------
                $this->email_repo->getEmailUserUploadPhoto($arr_body, $arr);

                // return message
                // --------------
                $msg = array('Photo successfully uploaded and send to RM for verification!', 'success', 1);
            }

            // upload by hr/region
            // -------------------
            else {

                // update current history flag = 0
                // -------------------------------
                $this->db_user_repo->dbUpdateUserPhotoHistory($attach['id'], $uid);

                // approve photo history
                // ---------------------
                $data = array(
                    'action_by' => auth()->user()->id, 
                    'type' => 2, // approved
                    'remark' => 'Upload Manual'
                );                 
                $this->db_user_repo->dbInsertUserPhotoHistory($attach['id'], $uid, $data);

                // get user info
                // -------------
                $user = $this->getUserByID($uid);

                // email body info
                // ---------------
                $arr_body = array(
                    'name' => $user->name,
                    'icno' => $user->icno,
                    'position_name' => $user->UserLatestJob->PositionName->name,                 
                    'site_name'     => $user->sitecode.' '.$user->SiteNameInfo->name
                );

                // receiver info
                // -------------
                $arr = array(
                    'name' => $user->name,
                    'sitecode' => $user->sitecode,                    
                    'attach'   => $attach
                );

                // send email notification
                // -----------------------
                $this->email_repo->getEmailUserUploadManual($arr_body, $arr);

                // return message
                // --------------
                $msg = array('Photo successfully Uploaded!', 'success', 1);
            }
        }

        // record already exist
        // --------------------
        else {
            $msg = array('Photo already Exist!', 'danger', 0);
        }
        return $msg;
    }

    // get lists of user photo
    // -----------------------
    public function getUserPhotoIndex($arr, $codes)
    {
        // get value from array
        // --------------------
        $site_id = $arr[0];
        $region_id = $arr[1];
        $status_id = $arr[2];
        $keyword = $arr[3];

        // query user photo
        // ----------------
        $q = \IhrV2\Models\UserPhoto::with(array('SiteDetail'))
            ->where(function ($s) use ($site_id) {
                if (!empty($site_id)) {
                    $s->where('sitecode', $site_id);
                }
            })
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->where(function ($j) use ($region_id) {
                if (!empty($region_id)) {
                    $j->whereHas('UserJob', function ($i) use ($region_id) {
                        $i->where('region_id', $region_id);
                    });
                }
            })
            ->whereHas('PhotoLatestStatus', function ($m) use ($status_id) {
                if (!empty($status_id)) {
                    $m->where('status', $status_id);
                }
            })
            ->with(array('PhotoLatestStatus' => function ($n) {
                $n->with(array('PhotoStatusName'));
            }))
            ->with(array('UserJob' => function ($h) {
                $h->with(array('PositionName', 'RegionName'));
            }))
            ->whereHas('UserDetail', function ($i) use ($keyword) {
                $i->where('status', 1);
                if (!empty($keyword)) {
                    $i->where('name', 'like', '%' . $keyword . '%');
                    $i->orWhere('icno', 'like', '%' . $keyword . '%');
                    $i->orWhere('username', 'like', '%' . $keyword . '%');
                }
            })
            ->with(array('UserDetail'))
            ->whereIn('active', array(1, 2))
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    // get history photo
    // -----------------
    public function getUserPhotoAll($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserPhoto::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode
        ])
            ->with(array('UserDetail'))
            ->with(array('PhotoLatestStatus' => function ($h) {
                $h->with(array('PhotoStatusName', 'PhotoActionBy'));
            }))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get history photo by id
    // -----------------------
    public function getUserPhotoAllByID($id, $uid)
    {
        $q = \IhrV2\Models\UserPhotoHistory::where([
            'photo_id' => $id,
            'user_id'  => $uid
        ])
            ->with(array('PhotoStatusName', 'PhotoActionBy'))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // get latest photo
    // ----------------
    public function getUserPhotoLatest($uid)
    {
        $q = \IhrV2\Models\UserPhoto::with(array('PhotoLatestStatus'))
            ->where([
                'user_id' => $uid,
                'active'  => 1
            ])
            ->orderBy('id', 'desc')
            ->first();
        return $q;
    }

    // check active user photo
    // -----------------------
    public function getCheckUserPhoto($uid, $sitecode)
    {
        // initialize variables
        // --------------------
        $arr = array();
        $empty = 0;
        $pending = 0;
        $approved = 0;
        $rejected = 0;

        // get photo
        // ---------
        $photo = $this->getUserPhotoOne($uid, $sitecode);
        if (!empty($photo)) {

            // check photo status
            // ------------------
            if ($photo->PhotoLatestStatus->status == 1) {
                // pending
                $pending = 1;
            }
            if ($photo->PhotoLatestStatus->status == 2) {
                // approved
                $approved = 1;
            }
            if ($photo->PhotoLatestStatus->status == 3) {
                // rejected
                $rejected = 1;
            }
        }

        // photo is empty
        // --------------
        else {
            $empty = 1;
        }
        $arr = array(
            'photo'    => $photo,
            'empty'    => $empty,
            'pending'  => $pending,
            'approved' => $approved,
            'rejected' => $rejected
        );
        return $arr;
    }

    public function getUserPhotoOne($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserPhoto::with(array('PhotoLatestStatus', 'PhotoApproved'))
            ->where([
                'user_id'  => $uid,
                'sitecode' => $sitecode,
                'active'   => 1
            ])
            ->first();
        return $q;
    }

    public function getUserPhotoByArr($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\UserPhoto::with(array('PhotoLatestStatus'))
            ->where([
                'id'       => $id,
                'user_id'  => $uid,
                'sitecode' => $sitecode
            ])
            ->first();
        return $q;
    }

    // get user info with latest contract
    // ----------------------------------
    public function getUserInfoContract($uid, $sitecode)
    {
        $q = \IhrV2\User::with(array('UserLatestContract'))
            ->where('id', $uid)
            ->where('sitecode', $sitecode)
            ->first();
        return $q;
    }

    public function getUserPhotoStatus()
    {
        return \IhrV2\Models\UserPhotoStatus::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Status]', '');
    }

    // get photo history
    // -----------------
    public function getUserPhotoHistory($id, $uid, $status)
    {
        $q = \IhrV2\Models\UserPhotoHistory::where([
            'photo_id' => $id,
            'user_id'  => $uid,
            'status'   => $status,
            'flag'     => 1
        ])
        ->first();
        return $q;
    }

    public function getCheckPhotoProcess($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\UserPhoto::where([
            'id'       => $id,
            'user_id'  => $uid,
            'sitecode' => $sitecode
        ])
        ->with(array('PhotoPending'))
        ->first();
        return $q;
    }

    public function getCheckPhoto($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\UserPhoto::where([
            'id'       => $id,
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'active'   => 1
        ])
        ->with(array('PhotoLatestStatus'))
        ->first();
        return $q;
    }

    // get other users
    // ---------------
    public function getUserOtheAll($arr)
    {        
        // separate values from array
        // --------------------------
        $group_id = $arr[0];
        $position_id = $arr[1];
        $keyword = $arr[2];

        // query other users
        // -----------------
        $q = \IhrV2\User::whereHas('UserCurrentJob2', function ($q) use ($position_id) {
            if (!empty($position_id)) {
                $q->where('position_id', $position_id);
            }
        })
        ->where('group_id', '!=', 3)
        ->with(array('UserCurrentJob2' => function ($j) {
            $j->with(array('PositionName'));
        }))
        ->with(array('GroupName', 'StatusName'))
        ->where('id', '!=', 1)
        ->where(function ($g) use ($group_id) {
            if (!empty($group_id)) {
                $g->where('group_id', $group_id);
            }
        })
        ->where(function ($i) use ($keyword) {
            if (!empty($keyword)) {
                $i->where('name', 'like', '%' . $keyword . '%');
                $i->orWhere('icno', 'like', '%' . $keyword . '%');
                $i->orWhere('username', 'like', '%' . $keyword . '%');
            }
        })
        ->orderBy('id', 'desc')
        ->paginate(20);
        return $q;
    }

    // get user info by icno
    // ---------------------
    public function getUserByIcno($icno)
    {
        $q = \IhrV2\User::where('icno', $icno)->first();
        return $q;
    }

    // get user info by icno all
    // -------------------------
    public function getUserByIcnoAll($icno)
    {
        $q = \IhrV2\User::where('icno', $icno)
        ->with(array('SiteNameInfo'))
        ->with(array('UserCurrentJob2' => function ($h) {
            $h->with('PositionName');
        }))        
        ->orderBy('id', 'desc')
        ->get();
        return $q;
    }

    // get site detail by sitecode
    // ---------------------------
    public function getSiteByCode($code)
    {
        return \IhrV2\Models\Site::where('code', $code)->first();
    }

    // get lists of contracts
    // ----------------------
    public function getUserContractLists($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserContract::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'status'   => 2
        ])
            ->with(array('LeaveApplication'))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    public function getUserActive($uid, $sitecode)
    {
        $q = \IhrV2\User::where([
            'id'       => $uid,
            'sitecode' => $sitecode,
            'status'   => 1
        ])
            ->first();
        return $q;
    }

    // get user by active icno and sitecode
    // ------------------------------------
    public function getUserByActiveICSite($icno, $sitecode)
    {
        $q = \IhrV2\User::where(['icno' => $icno, 'sitecode' => $sitecode, 'status' => 1])->first();
        return $q;
    }

    // get lists of contracts (active & inactive)
    // ------------------------------------------
    public function getUserContractDrop($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserContract::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode
        ])
        ->whereIn('status', array(1, 2))
        // ->select(\DB::raw('concat (date_from, " - ", date_to) as date, id'))
        ->select(\DB::raw('concat ("",DATE_FORMAT(date_from, "%d %b %Y"), " - ",DATE_FORMAT(date_to, "%d %b %Y"),"") as date, id'))
        // ->select(\DB::raw('DATE_FORMAT(date_from, "%d-%b-%Y") as date, id'))
        ->orderBy('id', 'desc')
        ->pluck('date', 'id')
        ->prepend('[Contract]', '');
        return $q;
    }

    // get lists of contracts (active)
    // -------------------------------
    public function getContractActiveDrop($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserContract::where([
            'user_id' => $uid,
            'sitecode' => $sitecode,
            'status' => 1,
            'flag' => 1
        ])
        ->select(\DB::raw('concat ("[",date_from, "] - [", date_to,"]") as date, id'))
        ->orderBy('date_to', 'desc')
        ->pluck('date', 'id')
        ->prepend('[Contract]', '');
        return $q;
    }

    // merge user contract
    // -------------------
    public function getUserContractMerge($id, $uid, $contract_ids)
    {

        // check duplicate contract
        // ------------------------
        if (!empty($contract_ids)) {
            foreach ($contract_ids as $i) {

                // update contract_id leave_applications
                // -------------------------------------
                $this->db_user_repo->dbUpdateConLeaveApp($id, $uid, $i);

                // update contract_id leave_approves
                // ---------------------------------
                $this->db_user_repo->dbUpdateConLeaveApprove($id, $uid, $i);

                // update contract_id leave_balances
                // ---------------------------------
                $this->db_user_repo->dbUpdateConLeaveBal($id, $uid, $i);

                // update contract_id leave_rep_applications
                // -----------------------------------------
                $this->db_user_repo->dbUpdateConLeaveRLApp($id, $uid, $i);

                // update contract inactive to cancel
                // ----------------------------------
                $this->db_user_repo->dbUpdateConCancel($i, $uid);
            }

            // get contract info
            // -----------------
            $contract = $this->getUserContractByID($id);

            // set email content
            // -----------------
            $arr_body = array(
                'contract' => $contract
            );

            // send email notification
            // -----------------------
            // $this->email_repo->getEmailMergeContract($arr_body);
            $msg = array('Contract Successfully Merge.', 'success');
        }

        // no record contract
        // ------------------
        else {
            $msg = array('Contract not Exist.', 'danger');
        }

        // return message
        // --------------
        return $msg;
    }

    // cancel user contract
    // --------------------
    public function getUserContractCancel($id, $uid)
    {

        // cancel contract
        // ---------------
        $cancel = $this->db_user_repo->dbCancelContract($uid, $id);

        // get contract info
        // -----------------
        $contract = $this->getUserContractByID($id);

        // set email content
        // -----------------
        $arr_body = array(
            'contract' => $contract
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailCancelContract($arr_body);

        // return message
        // --------------
        return array('Contract Successfully Cancel.', 'success');
    }

    // cancel user job
    // ---------------
    public function getUserJobCancel($id, $uid)
    {

        // cancel job
        // ----------
        $cancel = $this->db_user_repo->dbCancelJob($uid, $id);

        // get job info
        // ------------
        $job = $this->getUserJobID($id);

        // set email content
        // -----------------
        $arr_body = array(
            'job' => $job
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailCancelJob($arr_body);

        // return message
        // --------------
        return array('Job Successfully Cancel.', 'success');
    }

    // process user photo
    // 2 = approved | 3 = rejected | 4 = canceled
    // ------------------------------------------
    public function getProcessPhoto($data, $id, $uid, $sitecode)
    {
        $empty = 0;   // 1 = user not exist
        $xphoto = 0;  // 1 = photo not exist
        $success = 0; // 1 = success
        $pass = 0;    // 1 = valid process
        $approved = 0; // 1 = is approved
        $rejected = 0; // 1 = is rejected
        $canceled = 0; // 1 = is canceled
        $arr = array($id, $uid, $sitecode);

        // get user info
        // -------------
        $user = $this->getUserByID($uid);
        if (empty($user)) {
            $empty = 1;
        }

        // have record
        // -----------
        if ($empty != 1) {

            // check photo record
            // ------------------
            $check = $this->getCheckPhoto($id, $uid, $sitecode);
            if (!empty($check) && !empty($check->PhotoLatestStatus)) {

                // request approve
                // ---------------
                if ($data['type'] == 2 && $check->PhotoLatestStatus->status == 1) {
                    $pass = 1;
                }

                // request reject
                // --------------
                if ($data['type'] == 3 && $check->PhotoLatestStatus->status == 1) {
                    $pass = 1;
                }

                // request canceled
                // ----------------
                if ($data['type'] == 4 && $check->PhotoLatestStatus->status == 2) {
                    $pass = 1;
                }                
            }

            // no photo record
            // ---------------
            else {
                $xphoto = 1;
            }
        }

        // have record and valid status
        // ----------------------------
        if ($empty != 1 && $xphoto != 1 && $pass == 1) {

            // check history
            // -------------
            $history = $this->getUserPhotoHistory($id, $uid, $data['type']);

            // not record yet
            // --------------
            if (empty($history)) {

                // approve photo
                // -------------
                if ($data['type'] == 2) {

                    // update current history flag = 0
                    // -------------------------------
                    $this->db_user_repo->dbUpdateUserPhotoHistory($id, $uid);

                    // insert history
                    // --------------
                    $this->db_user_repo->dbInsertUserPhotoHistory($id, $uid, $data);

                    // set label status
                    // ----------------
                    $data['label'] = 'Approved';
                    $approved = 1;
                }

                // reject photo
                // ------------
                else if ($data['type'] == 3) {

                    // inactive current photo
                    // ----------------------
                    $this->db_user_repo->dbUpdateUserPhotoStatus($arr);

                    // update current history flag = 0
                    // -------------------------------
                    $this->db_user_repo->dbUpdateUserPhotoHistory($id, $uid);

                    // insert history
                    // --------------
                    $this->db_user_repo->dbInsertUserPhotoHistory($id, $uid, $data);

                    // set label status
                    // ----------------
                    $data['label'] = 'Rejected';                    
                    $rejected = 1;
                }

                // cancel photo
                // ------------
                else if ($data['type'] == 4) {

                    // inactive current photo
                    // ----------------------
                    $this->db_user_repo->dbUpdateUserPhotoStatus($arr);

                    // update current history flag = 0
                    // -------------------------------
                    $this->db_user_repo->dbUpdateUserPhotoHistory($id, $uid);

                    // insert new history
                    // ------------------
                    $this->db_user_repo->dbInsertUserPhotoHistory($id, $uid, $data);

                    // set label status
                    // ----------------
                    $data['label'] = 'Canceled';                    
                    $canceled = 1;
                }
            }
        }

        // return message
        // --------------
        if ($empty == 1) {
            $msg = array('Staff is not Exist.', 'danger');
        }
        if ($xphoto == 1) {
            $msg = array('Photo not Exist.', 'danger');
        }
        if ($pass == 0) {
            $msg = array('Photo already Processed.', 'danger');
        }

        // return success
        // --------------
        if ($approved == 1 || $rejected == 1 || $canceled == 1) {

            // set email header
            // ----------------
            $header = array(
                'mgr_name'  => $user->name,
                'mgr_email' => $user->email,
                'label'    => $data['label']
            );

            // set email body
            // --------------
            $body = array(
                'remark' => $data['remark'],
                'label' => $data['label']
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailUserPhotoResult($body, $header);

            // set label message
            // -----------------
            if ($approved == 1) {
                $msg = array('Photo successfully Approved!', 'success');
            }
            if ($rejected == 1) {
                $msg = array('Photo is Rejected!', 'danger');
            }
            if ($canceled == 1) {
                $msg = array('Photo is Canceled!', 'danger');
            }                          
        }

        // return result
        // -------------
        return $msg;
    }

    // update icno record
    // ------------------
    public function getUserUpdateICNo($icno, $uid, $token)
    {
        // check user record
        // -----------------
        $user = $this->getUserByIDToken($uid, $token);

        // update icno
        // -----------
        $save = $this->db_user_repo->dbUpdateUserICNo($user->id, $icno);

        // send email notification
        // -----------------------
        $arr_body = array(
            'user'     => $user,
            'icno'     => $icno,
            'site'     => $user->SiteNameInfo,
            'position' => $user->UserLatestJob->PositionName
        );
        $this->email_repo->getEmailUpdateICNo($arr_body);

        // return result
        // -------------
        return array('IC No. Successfully Updated.', 'success', 'mod.user.view');
    }

    // get staff job by id/staff id/sitecode
    // -------------------------------------
    public function getUserJobByActive($uid, $staff_id, $sitecode)
    {
        $q = \IhrV2\Models\UserJob::where([
            'user_id'  => $uid,
            'staff_id' => $staff_id,
            'sitecode' => $sitecode,
            'status'   => 1,
            'flag'     => 1
        ])
            ->first();
        return $q;
    }

    //get user by icno and staff id
    // ----------------------------
    public function getUserByICStaffID($icno, $staff_id)
    {
    }

    // get user transfer
    // -----------------
    public function getUserTransfer($uid, $icno)
    {
        $q = \IhrV2\User::where('icno', $icno)
            ->whereNotIn('id', array($uid))
            ->with(array('StatusName'))
            ->get();
        return $q;
    }

    // get duplicate user contract
    // ---------------------------
    public function getUserContractDuplicate($id, $uid, $start_date, $end_date)
    {
        $q = \IhrV2\Models\UserContract::where([
            'user_id'   => $uid,
            'date_from' => $start_date,
            'date_to'   => $end_date,
            'status'    => 2
        ])
            ->whereNotIn('id', array($id))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // save user other
    // ---------------
    public function getUserCreateOther($i)
    {
        // check if mobile tester set mobile = 1
        // -------------------------------------
        $i['mobile'] = ($i['group_id'] == 9) ? 1 : null;
        $i['sitecode'] = 'X01C001';

        // save user
        // ---------
        $id = $this->db_user_repo->dbInsertUserOther($i);

        // save user job
        // -------------
        $this->db_user_repo->dbInsertUserJobOther($id, $i);

        // return output
        // -------------
        $data = array('success', 'Staff Successfully Added.');
        return $data;
    }

    // update imei device
    // ------------------
    public function getUserUpdateImei($id, $token)
    {
        // update imei
        // -----------
        $save = $this->db_user_repo->dbUpdateUserImei($id, $token);

        // get user info
        // -------------
        $user = $this->getUserByID($id);

        // get updated user
        // ----------------
        $updated_by = $this->getUserByID(auth()->user()->id);

        // email body info
        // ---------------
        $arr_body = array(
            'user' => $user,
            'updated_by' => $updated_by
        );

        // receiver info
        // -------------
        $arr = array(
            'name'  => $user->name,
            'sitecode' => $user->sitecode,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailUpdateImei($arr_body, $arr);

        // return result
        // -------------
        return array('Staff Device successfully Reset.', 'success');
    }

    // update user profile
    // -------------------
    public function getUserProfileUpdate($i, $uid)
    {
        // get user
        // --------
        $user = \IhrV2\User::find($uid);

        // insert user history
        // -------------------
        $history = $this->db_user_repo->dbInsertUserHistory($user);

        // update current record
        // ---------------------
        $save = $this->db_user_repo->dbUpdateUserProfile($i, $uid);

        // set email header
        // ----------------
        $arr = array('name' => $user->name, 'sitecode' => $user->sitecode);

        // set email body
        // --------------
        $arr_body = array(
            'name' => $user->name,
            'icno' => $user->icno,
            'position' => $user->UserLatestJob->PositionName->name,
            'site_name' => $user->SiteNameInfo->name,
            'site_code' => $user->SiteNameInfo->code,
            'marital' => $this->getMaritalStatusByID($i['marital_id']),
            'state' => $this->getStateByCode($i['correspondence_state']),
            'new' => $i
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailUpdateProfile($arr_body, $arr);

        // return result
        // -------------
        return $data = array('success', 'Profile Successfully Updated and Submitted to HR.');
    }




}
