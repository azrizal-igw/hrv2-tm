<?php
namespace IhrV2\Repositories\User;

use Carbon\Carbon;
use Hash;
use IhrV2\Contracts\User\DbUserInterface;

class DbUserRepository implements DbUserInterface {

    // update only hp no
    // -----------------
    public function dbUpdateAuthContact($uid, $data) {
        $q = \IhrV2\User::where('id', $uid)
            ->update(array('hpno' => $data['hpno']));
        return true;
    }

    // update status user contract
    // ---------------------------
    public function dbUpdateStatusContract($user_id) {
        $q = \IhrV2\Models\UserContract::where('user_id', '=', $user_id)
        ->where('status', '=', 1)
        ->update(array('status' => 2));
        return true;
    }

    // insert user contract
    // --------------------
    public function dbInsertUserContract($i) {
        $arr = array(
            'user_id'            => $i['user_id'],
            'date_from'          => $i['from'],
            'date_to'            => $i['to'],
            'salary'             => $i['salary'],
            'status_contract_id' => $i['status_contract_id'],
            'sitecode'           => $i['sitecode'],
            'total_al'           => $i['total_al'],
            'status'             => 1
        );
        $sav = new \IhrV2\Models\UserContract($arr);
        $sav->save();
        return $sav->id;
    }

    // save updated user contract
    // --------------------------
    public function dbUpdateUserContract($i) {
        $q = \IhrV2\Models\UserContract::where('id', $i['id'])
            ->where('user_id', $i['user_id'])
            ->update(array(
                'date_from'          => $i['from'],
                'date_to'            => $i['to'],
                'salary'             => $i['salary'],
                'status_contract_id' => $i['status_contract_id'],
                'total_al'           => $i['total_al']
            ));
        return true;
    }

    // update leave balance (annual leave)
    // -----------------------------------
    public function dbUpdateLeaveBalance($uid, $id, $total_al) {
        $q = \IhrV2\Models\LeaveBalance::where('user_id', $uid)
            ->where('leave_type_id', 1)
            ->where('contract_id', $id)
            ->update(array('balance' => $total_al));
        return true;
    }

    // insert leave balance (annual leave)
    // -----------------------------------
    public function dbInsertLeaveBalance($i) {
        $arr = array(
            'user_id'       => $i['user_id'],
            'leave_type_id' => 1,
            'balance'       => $i['balance'],
            'contract_id'   => $i['id'],
            'year'          => date('Y')
        );
        $sav = new \IhrV2\Models\LeaveBalance($arr);
        $sav->save();
        return true;
    }

    // insert user other
    // -----------------
    public function dbInsertUserOther($i) {
        $arr = array(
            'username'  => $i['staff_id'],
            'name'      => $i['name'],
            'staff_id'  => $i['staff_id'],
            'email'     => $i['email'],
            'password'  => Hash::make('password'),
            'api_token' => str_random(60),
            'group_id'  => $i['group_id'],
            'icno'      => $i['icno'],
            'hpno'      => $i['hpno'],
            'sitecode'  => $i['sitecode'],
            'status'    => 1,
            'mobile'    => $i['mobile']
        );
        $sav = new \IhrV2\User($arr);
        $sav->save();
        return $sav->id;
    }

    // insert user job other
    // ---------------------
    public function dbInsertUserJobOther($id, $i)
    {
        $arr = array(
            'user_id'  => $id,
            'staff_id'  => $i['staff_id'],
            'join_date'     => date('Y-m-d'),
            'position_id'  => $i['position_id'],
            'region_id' => (!empty($i['region_id'])) ? $i['region_id'] : null,
            'status'    => 1,
            'flag' => 1,
        );
        $sav = new \IhrV2\Models\UserJob($arr);
        $sav->save();
        return true;
    }

    // insert user
    // -----------
    public function dbInsertUser($i) {

        // remove white space
        // ------------------
        $data = array_map('trim', $i);

        // check if admin
        // --------------
        $is_admin = 0;
        if ($data['group_id'] == 1) {
            $is_admin = 1;
        }

        // remove icno that has dash
        // -------------------------
        $icno = str_replace("-", "", $data['icno']);

        // insert users
        // ------------
        $sitecode = isset($data['site_id']) ? $data['site_id'] : '';
        $arr = array(
            'staff_id'   => $data['staff_id'],
            'username'   => $data['staff_id'],
            'name'       => $data['name'],
            'email'      => $data['email'],
            'hpno'       => $data['hpno'],
            'password'   => \Hash::make($data['password']),
            'api_token'  => str_random(60),
            'group_id'   => $data['group_id'],
            'is_admin'   => $is_admin,
            'icno'       => $icno,
            'sitecode'   => $sitecode,
            'status'     => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        );
        $save_user = new \IhrV2\User($arr);
        $save_user->save();
        $user_id = $save_user->id;

        // insert user_jobs
        // ----------------
        $region_id = isset($data['region_id']) ? $data['region_id'] : '';
        $phase_id = isset($data['phase_id']) ? $data['phase_id'] : '';
        $arr_job = array(
            'user_id'     => $user_id,
            'staff_id'    => $data['staff_id'],
            'join_date'   => Carbon::now()->format('Y-m-d'),
            'position_id' => $data['position_id'],
            'phase_id'    => $phase_id,
            'region_id'   => $region_id,
            'sitecode'    => $sitecode,
            'status'      => 1
        );
        $sav_job = new \IhrV2\Models\UserJob($arr_job);
        $sav_job->save();
        $msg = array('Staff successfully created.', 'success', 'mod.user.index');
        return $msg;
    }

    // insert user employment
    // ----------------------
    public function dbInsertUserEmployment($data, $uid) {
        $arr = array(
            'user_id'   => $uid,
            'date_from' => $data['from_year'] . '-' . $data['from_month'] . '-' . '01',
            'date_to'   => $data['to_year'] . '-' . $data['to_month'] . '-' . '01',
            'company'   => $data['company'],
            'position'  => $data['position'],
            'salary'    => $data['salary']
        );
        $sav = new \IhrV2\Models\UserEmployment($arr);
        $sav->save();
        $msg = array('Employment successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // update user employment
    // ----------------------
    public function dbUpdateUserEmployment($data, $id) {
        $arr = array(
            'date_from' => $data['from_year'] . '-' . $data['from_month'] . '-' . '01',
            'date_to'   => $data['to_year'] . '-' . $data['to_month'] . '-' . '01',
            'company'   => $data['company'],
            'position'  => $data['position'],
            'salary'    => $data['salary']
        );
        $save = \IhrV2\Models\UserEmployment::where('id', $id)->update($arr);
        $msg = array('Employment successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user employment
    // ----------------------
    public function dbDeleteUserEmployment() {
        $this->delete();
        $msg = array('User Employment successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user skill
    // -----------------
    public function dbInsertUserSkill($data, $uid) {
        $arr = array(
            'user_id'  => $uid,
            'name'     => $data['name'],
            'level_id' => $data['level']
        );
        $sav = new \IhrV2\Models\UserSkill($arr);
        $sav->save();
        $msg = array('Skill successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // update user skill
    // -----------------
    public function dbUpdateUserSkill($data, $id) {
        $arr = array(
            'name'     => $data['name'],
            'level_id' => $data['level']
        );
        $save = \IhrV2\Models\UserSkill::where('id', $id)->update($arr);
        $msg = array('Skill successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user skill
    // -----------------
    public function dbDeleteUserSkill() {
        $this->delete();
        $msg = array('User Skill successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user reference
    // ---------------------
    public function dbInsertUserReference($data, $uid) {
        $arr = array(
            'user_id'      => $uid,
            'name'         => $data['name'],
            'relation'     => $data['relation'],
            'address'      => $data['address'],
            'telno'        => $data['telno'],
            'occupation'   => $data['occupation'],
            'period_known' => $data['period_known']
        );
        $sav = new \IhrV2\Models\UserReference($arr);
        $sav->save();
        $msg = array('Reference successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // update user reference
    // ---------------------
    public function dbUpdateUserReference($data, $id) {
        $arr = array(
            'name'         => $data['name'],
            'relation'     => $data['relation'],
            'address'      => $data['address'],
            'telno'        => $data['telno'],
            'occupation'   => $data['occupation'],
            'period_known' => $data['period_known']
        );
        $save = \IhrV2\Models\UserReference::where('id', $id)->update($arr);
        $msg = array('Reference successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user reference
    // ---------------------
    public function dbDeleteUserReference() {
        $this->delete();
        $msg = array('User Reference successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // upload user photo
    // -----------------
    public function dbUploadUserPhoto($data) {

        // inactive current photo
        // ----------------------
        $update = \IhrV2\Models\UserPhoto::where('user_id', $data['id'])
            ->where('status', 1)
            ->update(array('status' => 0));

        // insert new record
        // -----------------
        $arr = array(
            'user_id'     => $data['id'],
            'photo'       => $data['photo'],
            'photo_thumb' => $data['photo_thumb'],
            'ext'         => $data['ext'],
            'size'        => $data['size'],
            'status'      => 1
        );
        $sav = new \IhrV2\Models\UserPhoto($arr);
        $sav->save();
        $msg = array('Photo successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // remove user photo
    // -----------------
    public function dbRemoveUserPhoto() {
        $this->status = 0;
        $this->save();
        $msg = array('Photo successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user language
    // --------------------
    public function dbInsertUserLanguage($data, $uid) {
        $arr = array(
            'user_id' => $uid,
            'dialect' => $data['dialect'],
            'desc'    => $data['desc'],
            'written' => $data['written'],
            'reading' => $data['reading'],
            'spoken'  => $data['spoken']
        );
        $sav = new \IhrV2\Models\UserLanguage($arr);
        $sav->save();
        $msg = array('Language successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // update user language
    // --------------------
    public function dbUpdateUserLanguage($data, $id) {
        $arr = array(
            'dialect' => $data['dialect'],
            'desc'    => $data['desc'],
            'written' => $data['written'],
            'reading' => $data['reading'],
            'spoken'  => $data['spoken']
        );
        $save = \IhrV2\Models\UserLanguage::where('id', $id)->update($arr);
        $msg = array('Language successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user language
    // --------------------
    public function dbDeleteUserLanguage() {
        $this->delete();
        $msg = array('User Language successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user job
    // ---------------
    public function dbInsertUserJob($data, $uid) {
        $group_id = $data['group_id'];

        // get current staff id
        // --------------------
        $curr = \IhrV2\User::find($uid);

        // update staff id users
        // ---------------------
        $upd_staffid = \IhrV2\User::where('id', $uid)->update(array(
            'prev_staff_id' => $curr->staff_id,
            'staff_id'      => trim($data['staff_id'])
        ));

        // update current status user_jobs to 2 (inactive)
        // -----------------------------------------------
        $upd_job = \IhrV2\Models\UserJob::where('user_id', $uid)->where('status', 1)->update(array(
            'status' => 2
        ));

        // set variables
        // -------------
        $phase_id = null;
        $sitecode = null;
        $region_id = null;

        // site supervisor
        // ---------------
        if ($group_id == 3) {
            $phase_id = $data['phase_id'];
            $sitecode = $data['sitecode'];
        }

        // region manager
        // --------------
        if ($group_id == 4) {
            $region_id = $data['region_id'];
        }

        // insert new user_jobs
        // --------------------
        $arr = array(
            'user_id'     => $uid,
            'staff_id'    => trim($data['staff_id']),
            'join_date'   => date('Y-m-d', strtotime(str_replace('/', '-', $data['join_date']))),
            'position_id' => $data['position_id'],
            'phase_id'    => $phase_id,
            'region_id'   => $region_id,
            'sitecode'    => $sitecode,
            'status'      => 1
        );
        $sav = new \IhrV2\Models\UserJob($arr);
        $sav->save();
        $msg = array('Job successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // update user job
    // ---------------
    public function dbUpdateUserJob($data, $id) {
        $arr = array(
            'date_from' => $data['from_year'] . '-' . $data['from_month'] . '-' . '01',
            'date_to'   => $data['to_year'] . '-' . $data['to_month'] . '-' . '01',
            'company'   => $data['company'],
            'position'  => $data['position'],
            'salary'    => $data['salary']
        );
        $save = \IhrV2\Models\UserJob::where('id', $id)->update($arr);
        $msg = array('Job successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user job
    // ---------------
    public function dbDeleteUserJob() {
        $this->delete();
        $msg = array('User Job successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user family
    // ------------------
    public function dbInsertUserFamily($data) {
        $arr = array(
            'user_id'       => $data['uid'],
            'name'          => $data['name'],
            'age'           => $data['age'],
            'occupation'    => $data['occupation'],
            'school_office' => $data['school_office'],
            'relation'      => $data['relation']
        );
        $sav = new \IhrV2\Models\UserFamily($arr);
        $sav->save();
        return true;
    }

    // update user family
    // ------------------
    public function dbUpdateUserFamily($data, $id) {
        $arr = array(
            'name'          => $data['name'],
            'age'           => $data['age'],
            'occupation'    => $data['occupation'],
            'school_office' => $data['school_office'],
            'relation'      => $data['relation']
        );
        $save = \IhrV2\Models\UserFamily::where('id', $id)->update($arr);
        $msg = array('Family successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user family
    // ------------------
    public function dbDeleteUserFamily() {
        $this->delete();
        $msg = array('User Family successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user emergency
    // ---------------------
    public function dbInsertUserEmergency($data, $uid) {
        $arr = array(
            'user_id'  => $uid,
            'name'     => $data['name'],
            'telno'    => $data['telno'],
            'address'  => $data['address'],
            'relation' => $data['relation']
        );
        $sav = new \IhrV2\Models\UserEmergency($arr);
        $sav->save();
        $msg = array('Emergency Contact successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // update user emergency
    // ---------------------
    public function dbUpdateUserEmergency($data, $id) {
        $arr = array(
            'name'     => $data['name'],
            'telno'    => $data['telno'],
            'address'  => $data['address'],
            'relation' => $data['relation']
        );
        $save = \IhrV2\Models\UserEmergency::where('id', $id)->update($arr);
        $msg = array('Emergency Contact successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user emergency
    // ---------------------
    public function dbDeleteUserEmergency() {
        $this->delete();
        $msg = array('User Emergency successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user education
    // ---------------------
    public function dbInsertUserEducation($data, $uid) {
        $arr = array(
            'user_id'        => $uid,
            'year_from'      => $data['year_from'],
            'year_to'        => $data['year_to'],
            'name_education' => $data['name_education'],
            'result'         => $data['result']
        );
        $sav = new \IhrV2\Models\UserEducation($arr);
        $sav->save();
        $msg = array('Education successfully added.', 'success', 'mod.user.view');
        return $msg;
    }

    // update user education
    // ---------------------
    public function dbUpdateUserEducation($data, $id) {
        $arr = array(
            'year_from'      => $data['year_from'],
            'year_to'        => $data['year_to'],
            'name_education' => $data['name_education'],
            'result'         => $data['result']
        );
        $save = \IhrV2\Models\UserEducation::where('id', $id)->update($arr);
        $msg = array('Education successfully updated.', 'success', 'mod.user.view');
        return $msg;
    }

    // delete user education
    // ---------------------
    public function dbDeleteUserEducation() {
        $this->delete();
        $msg = array('User Education successfully deleted.', 'success', 'mod.user.view');
        return $msg;
    }

    // insert user photo
    // -----------------
    public function dbInsertUserPhoto($uid, $sitecode, $i) {
        $arr = array(
            'user_id'     => $uid,
            'sitecode'    => $sitecode,
            'action_date' => Carbon::now(),
            'action_by'   => auth()->user()->id,
            'photo'       => $i['filename'],
            'photo_thumb' => $i['filethumb'],
            'ext'         => $i['fileext'],
            'size'        => $i['filesize'],
            'active'      => 1
        );
        $sav = new \IhrV2\Models\UserPhoto($arr);
        $sav->save();
        return $sav->id;
    }

    // update status user photo to 0
    // -----------------------------
    public function dbUpdateUserPhoto($uid) {
        $q = \IhrV2\Models\UserPhoto::where('user_id', '=', $uid)
            ->where('status', 1)
            ->update(array('status' => 2));
        return true;
    }

    // update user photo status
    // ------------------------
    public function dbUpdateUserPhotoStatus($arr) {
        $q = \IhrV2\Models\UserPhoto::where(
            [
                'id'       => $arr[0],
                'user_id'  => $arr[1],
                'sitecode' => $arr[2],
                'active'   => 1
            ])
            ->update(array('active' => 2));
        return true;
    }

    // insert user photo history
    // -------------------------
    public function dbInsertUserPhotoHistory($id, $uid, $data) 
    {
        $action_by = (!empty($data['action_by'])) ? $data['action_by'] : auth()->user()->id;
        $arr = array(
            'photo_id'    => $id,
            'user_id'     => $uid,
            'action_date' => Carbon::now()->toDateString(),
            'action_by'   => $action_by,
            'remark'      => $data['remark'],
            'status'      => $data['type'],
            'flag'        => 1
        );
        $sav = new \IhrV2\Models\UserPhotoHistory($arr);
        $sav->save();
        return true;
    }

    // 
    public function dbInsertUserPhotoApprove()
    {

    }

    // update user photo history
    // -------------------------
    public function dbUpdateUserPhotoHistory($id, $uid) {
        $q = \IhrV2\Models\UserPhotoHistory::where(
            [
                'photo_id' => $id,
                'user_id'  => $uid,
                'flag'     => 1
            ])
            ->update(array('flag' => 0));
        return true;
    }

    // insert user by manual to export to agg
    // --------------------------------------
    public function dbInsertUserManual($user, $i) {
        $arr = array(
            'Action_By'        => auth()->user()->id,
            'Action_Date'      => Carbon::now(),
            'PI1M_REFID'       => strtoupper($i['sitecode']),
            'SERVICE_PROVIDER' => 'TM',
            'Staff_IC'         => strtoupper($user->icno),
            'Staff_Name'       => strtoupper($user->name),
            'Contact_Number'   => strtoupper($user->hpno),
            'Contact_Email'    => strtoupper($user->email),
            'Position'         => strtoupper($i['position_id']),
            'Status'           => strtoupper($i['status_id'])
        );
        $sav = new \IhrV2\Models\UserManual($arr);
        $sav->save();
        return $sav->id;
    }

    public function dbInsertUserManualNotExist($i) {
        $arr = array(
            'Action_By'        => auth()->user()->id,
            'Action_Date'      => Carbon::now(),
            'PI1M_REFID'       => strtoupper($i['sitecode']),
            'SERVICE_PROVIDER' => 'TM',
            'Staff_IC'         => strtoupper($i['icno']),
            'Staff_Name'       => strtoupper($i['name']),
            'Contact_Number'   => strtoupper($i['hpno']),
            'Contact_Email'    => strtoupper($i['email']),
            'Position'         => strtoupper($i['position_id']),
            'Status'           => strtoupper($i['status_id'])
        );
        $sav = new \IhrV2\Models\UserManual($arr);
        $sav->save();
        return $sav->id;
    }

    // insert agg staff detail of user manual
    // --------------------------------------
    public function dbInsertAggStaffDetail($i) {
        $arr = array(
            'PI1M_REFID'       => trim(strtoupper($i->PI1M_REFID)),
            'SERVICE_PROVIDER' => 'TM',
            'Staff_IC'         => trim($i->Staff_IC),
            'Staff_Name'       => trim(strtoupper($i->Staff_Name)),
            'Contact_Number'   => trim($i->Contact_Number),
            'Contact_Email'    => trim(strtoupper($i->Contact_Email)),
            'Position'         => trim($i->Position),
            'Status'           => $i->Status
        );
        $sav = new \IhrV2\Models\AggStaffDetail($arr);
        $sav->save();
        return true;
    }

    public function dbInsertAggStaffDirect($i)
    {
        $arr = array(
            'PI1M_REFID'       => trim(strtoupper($i['PI1M_REFID'])),
            'SERVICE_PROVIDER' => $i['SERVICE_PROVIDER'],
            'Staff_IC'         => trim($i['Staff_IC']),
            'Staff_Name'       => trim(strtoupper($i['Staff_Name'])),
            'Contact_Number'   => trim($i['Contact_Number']),
            'Contact_Email'    => trim(strtoupper($i['Contact_Email'])),
            'Position'         => trim($i['Position']),
            'Status'           => $i['Status']
        );
        $sav = new \IhrV2\Models\AggStaffDetail($arr);
        $sav->save();
        return true;
    }

    // cancel user contract
    // --------------------
    public function dbCancelContract($uid, $contract_id) {
        $q = \IhrV2\Models\UserContract::where(
            [
                'id'      => $contract_id,
                'user_id' => $uid
            ])
            ->update(array(
                'updated_by' => auth()->user()->id,
                'status'     => 3, // cancel
                'flag'       => 0
            ));
        return true;
    }

    // cancel user job
    // ---------------
    public function dbCancelJob($uid, $job_id) {
        $q = \IhrV2\Models\UserJob::where(
            [
                'id'      => $job_id,
                'user_id' => $uid
            ])
            ->update(array(
                'updated_by' => auth()->user()->id,
                'status'     => 3, // cancel
                'flag'       => 0
            ));
        return true;
    }

    // update user icno
    // ----------------
    public function dbUpdateUserICNo($id, $icno) {
        $q = \IhrV2\User::where(
            [
                'id' => $id
            ])
            ->update(array(
                'icno' => $icno
            ));
        return true;
    }

    // update contract_id leave_applications
    // -------------------------------------
    public function dbUpdateConLeaveApp($id, $uid, $id2) {
        $q = \IhrV2\Models\LeaveApplication::where(['contract_id' => $id2, 'user_id' => $uid])
            ->update(array('contract_id' => $id));
        return true;
    }

    // update contract_id leave_approves
    // ---------------------------------
    public function dbUpdateConLeaveApprove($id, $uid, $id2) {
        $q = \IhrV2\Models\LeaveApprove::where(['contract_id' => $id2, 'user_id' => $uid])
            ->update(array('contract_id' => $id));
        return true;
    }

    // update contract_id leave_balances
    // ---------------------------------
    public function dbUpdateConLeaveBal($id, $uid, $id2) {
        $q = \IhrV2\Models\LeaveBalance::where(['contract_id' => $id2, 'user_id' => $uid])
            ->update(array('contract_id' => $id));
        return true;
    }

    // update contract_id leave_rep_applications
    // -----------------------------------------
    public function dbUpdateConLeaveRLApp($id, $uid, $id2) {
        $q = \IhrV2\Models\LeaveRepApplication::where(['contract_id' => $id2, 'user_id' => $uid])
            ->update(array('contract_id' => $id));
        return true;
    }

    // update user contract inactive to cancel
    // ---------------------------------------
    public function dbUpdateConCancel($id, $uid) {
        $q = \IhrV2\Models\UserContract::where(['id' => $id, 'user_id' => $uid])
            ->update(array('status' => 3, 'flag' => 0));
        return true;
    }

    // update user device
    // ------------------
    public function dbUpdateUserImei($id, $token) {
        $q = \IhrV2\User::where(['id' => $id, 'api_token' => $token])
            ->update(array('imei' => null));
        return true;
    }

    // update user profile
    // -------------------
    public function dbUpdateUserProfile($i, $uid) 
    {
        $q = \IhrV2\User::find($uid)
        ->update(array(
            'marital_id' => $i['marital_id'],
            'hpno2' => $i['hpno2'],
            'partner_name' => $i['partner_name'],
            'partner_phone' => $i['partner_phone'],
            'child_no' => $i['child_no'],
            'correspondence_street_1' => $i['correspondence_street_1'],
            'correspondence_street_2' => $i['correspondence_street_2'],
            'correspondence_postcode' => $i['correspondence_postcode'],
            'correspondence_city' => $i['correspondence_city'],
            'correspondence_state' => $i['correspondence_state'],
            'edit' => auth()->user()->edit + 1
        ));
        return true;
    }

    // insert user history
    // -------------------
    public function dbInsertUserHistory($i) 
    {
        $arr = array(
            'username'                => $i->username,
            'name'                    => $i->name,
            'staff_id'                => $i->staff_id,
            'prev_staff_id'           => $i->prev_staff_id,
            'email'                   => $i->email,
            'password'                => $i->password,
            'api_token'               => $i->api_token,
            'group_id'                => $i->group_id,
            'icno'                    => $i->icno,
            'permanent_street_1'      => $i->permanent_street_1,
            'permanent_street_2'      => $i->permanent_street_2,
            'permanent_postcode'      => $i->permanent_postcode,
            'permanent_city'          => $i->permanent_city,
            'permanent_state'         => $i->permanent_state,
            'correspondence_street_1' => $i->correspondence_street_1,
            'correspondence_street_2' => $i->correspondence_street_2,
            'correspondence_postcode' => $i->correspondence_postcode,
            'correspondence_city'     => $i->correspondence_city,
            'correspondence_state'    => $i->correspondence_state,
            'telno1'                  => $i->telno1,
            'telno2'                  => $i->telno1,
            'hpno'                    => $i->hpno,
            'hpno2'                   => $i->hpno2,
            'faxno'                   => $i->faxno,
            'website'                 => $i->website,
            'personal_email'          => $i->personal_email,
            'work_email'              => $i->work_email,
            'gender_id'               => $i->gender_id,
            'marital_id'              => $i->marital_id,
            'dob'                     => $i->dob,
            'pob'                     => $i->pob,
            'nationality_id'          => $i->nationality_id,
            'race_id'                 => $i->race_id,
            'religion_id'             => $i->religion_id,
            'partner_name'            => $i->partner_name,
            'partner_phone'           => $i->partner_phone,
            'child_no'                => $i->child_no,
            'sitecode'                => $i->sitecode,
            'epfno'                   => $i->epfno,
            'bankname'                => $i->bankname,
            'bankno'                  => $i->bankno,
            'status'                  => $i->status,
            'action_date'             => $i->action_date,
            'last_login'              => $i->last_login,
            'date_created'            => $i->date_created,
            'date_modified'           => $i->date_modified,
            'imei'                    => $i->imei,
            'device'                  => $i->device,
            'edit'                    => $i->edit,
            'mobile'                  => $i->mobile            
        );
        $sav = new \IhrV2\Models\UserHistory($arr);
        $sav->save();
        return true;
    }
}
