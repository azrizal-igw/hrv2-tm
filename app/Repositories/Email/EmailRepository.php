<?php
namespace IhrV2\Repositories\Email;

use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Helpers\CommonHelper;
use Mail;
use Carbon\Carbon;

class EmailRepository implements EmailInterface
{
    // set from email address
    // ----------------------
    public function getEmailFrom()
    {
        $q = \IhrV2\Models\EmailNotification::where(['type_id' => 1, 'status' => 1])->first();
        $email = [$q->email => $q->name];
        return $email;
    }

    // get email no reply
    // ------------------
    public function getEmailNoReply()
    {
        $email = ['hrv2@msd.net.my' => 'Administrator'];
        return $email;        
    }

    // set email address for bcc
    // -------------------------
    public function getEmailBcc()
    {
        $q = \IhrV2\Models\EmailNotification::where(['type_id' => 3, 'status' => 1])->get();
        foreach ($q as $i) {
            $arr_email[] = array($i->email => $i->name);
        }

        // convert multi-dimensional array inro single array
        // ------------------------------------------------
        $emails = array_collapse($arr_email);
        return $emails;
    }

    // get email boss
    // --------------
    public function getEmailBoss()
    {
        $emails = [
            'mtawfik@msd.net.my' => 'Mohd Tawfik Abdul Rahman'
        ];
        return $emails;
    }

    // get email sync person in charge
    // -------------------------------
    public function getEmailSync()
    {
        $emails = [
            's.radziah@msd.net.my' => 'Sharifah Radziah Mat'
        ];
        return $emails;
    }

    // get email agg person in charge
    // ------------------------------
    public function getEmailAgg()
    {
        $emails = [
            'yuslinda@msd.net.my' => 'Siti Yuslinda Che Ya\'acob'
        ];
        return $emails;
    }

    // send email process
    // ------------------
    public function getSendEmail($arr_body, $arr)
    {
        // get from info
        // -------------
        $arr['from'] = $this->getEmailFrom();

        // send email
        // ----------
        $content = Mail::send($arr['path'], $arr_body, function ($message) use ($arr) {

            // set from info
            // -------------
            $message->from($arr['from']);

            // set to info
            // -----------
            if (empty($arr['to_system'])) {
                $message->to($arr['email'], $arr['name'])->subject($arr['subject']);
            } else {
                $message->to($arr['to_system'])->subject($arr['subject']);
            }

            // set if have cc
            // --------------
            if (!empty($arr['cc_name']) && !empty($arr['cc_email'])) {
                $message->cc($arr['cc_email'], $arr['cc_name'])->subject($arr['subject']);
            }

            // check drm
            // ---------
            if (!empty($arr['drm'])) {
                $chk_drm = $arr['bcc'] + $arr['drm'];
            } else {
                $chk_drm = $arr['bcc'];
            }

            // check clerk
            // -----------
            if (!empty($arr['clerk'])) {
                $chk_clk = $chk_drm + $arr['clerk'];
            } else {
                $chk_clk = $chk_drm;
            }

            // set bcc info
            // ------------
            $message->bcc($chk_clk)->subject($arr['subject']);

            // check if have attachment
            // ------------------------
            if (!empty($arr['attach'])) {
                if (count($arr['attach']) > 0) {

                    // set value attachment
                    // --------------------
                    $file = $arr['attach']['file'];
                    $as = $arr['attach']['as'];
                    $mime = $arr['attach']['mime'];

                    // attach attachment
                    // -----------------
                    if (!empty($file) && !empty($as) && !empty($mime)) {
                        $message->attach($file, ['as' => $as, 'mime' => $mime]);
                    }
                }
            }
        });
        return $content;
    }

    /*
    |--------------------------------------------------------------------------
    | Leave Notification
    |--------------------------------------------------------------------------
     */

    // site supervisor apply leave
    // ---------------------------
    public function getEmailLeaveApply($arr_body, $arr)
    {
        $arr['path'] = 'emails.leave.create';
        $arr['subject'] = '[New ' . $arr['leave_name'] . '] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // site supervisor get leave result
    // --------------------------------
    public function getEmailLeaveResult($arr_body, $arr)
    {
        $arr['path'] = 'emails.leave.result';
        $arr['subject'] = $arr_body['leave_name'] . ' - ' . $arr_body['status']->name.'!';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // site supervisor cancel the approved leave
    // -----------------------------------------
    public function getEmailLeaveCancel($arr_body, $arr)
    {
        $arr['path'] = 'emails.leave.apply-cancel.create';
        $arr['subject'] = '[Apply Cancel - ' . $arr_body['leave_name'] . '] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // rm approve/reject the apply cancel
    // ----------------------------------
    public function getEmailApplyCancelResult($arr_body, $arr)
    {
        $status = $arr['status'];
        $leave_name = $arr_body['leave_name'];
        $arr['path'] = 'emails.leave.apply-cancel.result';
        $arr['subject'] = "[Apply Cancel - $leave_name] - $status!";
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $arr_body['leave_name'] = $arr_body['leave_name'];
        $arr_body['cancel_status'] = $status;
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // apply backdated leave
    // ---------------------
    public function getEmailLeaveBackdated($arr_body, $arr)
    {
        $arr['path'] = 'emails.leave.backdated';
        $arr['subject'] = '[New Backdated Leave] - '.$arr['leave_name'];
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['cc_email'] = $arr['rm_email'];
        $arr['cc_name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // reimburse unplan limit
    // ----------------------
    public function getEmailReimburseUnplan($arr_body, $arr)
    {
        $arr['path'] = 'emails.leave.unplan.reimburse-limit';
        $total = $arr['total'];
        $arr['subject'] = "[Add $total for Unplan Limit]";
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // create RL request
    // -----------------
    public function getEmailRLCreate($arr_body, $arr)
    {
        $arr['path'] = 'emails.rl.create';
        $arr['subject'] = '[New RL Request] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // result RL request
    // -----------------
    public function getEmailRLResult($arr_body, $arr)
    {
        $arr['path'] = 'emails.rl.result';
        $arr['subject'] = "[RL Request - " . $arr['status'] . "!]";
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // apply cancel RL request
    // -----------------------
    public function getEmailRLCancel($arr_body, $arr)
    {
        $arr['path'] = 'emails.rl.apply-cancel.create';
        $arr['subject'] = '[Apply Cancel RL] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // result apply cancel RL request
    // ------------------------------
    public function getEmailRLCancelResult($arr_body, $arr)
    {
        $arr['path'] = 'emails.rl.apply-cancel.result';
        $arr['subject'] = '[Apply Cancel RL - '.$arr['status'].'!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // hr/rm reimburse total leave
    // ---------------------------
    public function getLeaveAddCreate($arr_body, $arr)
    {
        $arr['path'] = 'emails.reimburse-deduct.create';
        $leave_name = $arr['leave_name'];
        $type_name = $arr['type_name'];
        $arr['subject'] = "[New $type_name - $leave_name]";
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // register change off day
    // -----------------------
    public function getEmailLeaveOffChange($arr_body, $arr)
    {
        $arr['path'] = 'emails.off-day.change';
        $arr['subject'] = '[New Change Off Day] - ' . $arr['mgr_name'] . ' (' . $arr['sitecode'] . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Attendance Notification
    |--------------------------------------------------------------------------
     */

    // site supervisor set new attendance remark
    // -----------------------------------------
    public function getEmailAttApplyRemark($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.remark.create';
        $arr['subject'] = '[New Attendance Remark] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // edit note remark
    // -------------------
    public function getEmailAttRemarkNoteEdit($arr_body, $arr)
    {
        // $arr_body['status'] = $arr['status'];
        $arr['path'] = 'emails.attendance.remark.note';
        $arr['subject'] = '[Attendance Remark - ' . $arr['status'] . '!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // rm approve/updated/cancel/reject remark
    // ---------------------------------------
    public function getEmailAttRemarkResult($arr_body, $arr)
    {
        $arr_body['status'] = $arr['status'];
        $arr['path'] = 'emails.attendance.remark.result';
        $arr['subject'] = '[Attendance Remark - ' . $arr['status'] . '!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // set multiple remark by admin
    // -----------------------------
    public function getAttRemarkRangeAdmin($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.remark.range-admin';
        $arr['subject'] = '[New Attendance Remark From RM!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // region manager set remark attendance
    // ------------------------------------
    public function getEmailAttSetRemark($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.remark.set';
        $arr['subject'] = '[New Attendance Remark From RM!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // site supervisor set attendance remark by date range
    // ---------------------------------------------------
    public function getAttRemarkByRange($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.remark.range';
        $arr['subject'] = '[New Attendance Remark] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // site supervisor put timeslip
    // ----------------------------
    public function getEmailAttOtherInfo($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.remark.timeslip';
        $arr['subject'] = '[New Time Slip] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // apply attendance manual
    // -----------------------
    public function getEmailAttManualCreate($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.manual.create';
        $arr['subject'] = '[New Attendance Manual From RM!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['cc_email'] = $arr['rm_email'];
        $arr['cc_name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // cancel attendance manual
    // ------------------------
    public function getEmailAttManualCancel($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.manual.cancel';
        $arr['subject'] = '[Attendance Manual - Canceled!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // notify on punch the attendance
    // ------------------------------
    public function getEmailAttPunch($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.check.region';
        $total = $arr_body['total'];
        $type = $arr_body['type'];
        $region = $arr_body['region'];
        $start = $arr_body['start'];
        $end = $arr_body['end'];
        $arr['subject'] = "[$total Punch $type Attendance] - $region ($start to $end)";
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // attendance summary
    // ------------------
    public function getEmailAttSummary($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.check.summary';
        $arr['subject'] = '[Attendance Summary] - ' . $arr['region'] . ' (' . Carbon::parse($arr['date'])->format('j M Y') . ' at ' . date('g:i:s a'). ')';
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // cancel time slip
    // ----------------
    public function getEmailTimeSlipCancel($arr_body, $arr)
    {
        $arr['path'] = 'emails.attendance.timeslip.cancel';
        $arr['subject'] = '[Attendance Time Slip - Canceled!]';
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // cancel attendance mobile
    // ------------------------
    public function getEmailAttMobileCancel($arr_body)
    {
        $arr['path'] = 'emails.attendance.mobile.cancel';
        $arr['subject'] = '[Attendance Mobile - Canceled!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | APi Attendance Notification
    |--------------------------------------------------------------------------
     */

    // sync record attendance
    // ----------------------
    public function getEmailApiAttSync($arr_body, $arr)
    {
        $arr['path'] = 'emails.api.attendance.sync';
        $arr['subject'] = '[Mobile Attendance Sync] - '. $arr['name']. ' ('. $arr['sitecode']. ') '.$arr['date'];
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // upload log attendance
    // ---------------------
    public function getEmailApiAttUpload($arr_body, $arr)
    {
        $arr['path'] = 'emails.api.attendance.upload';
        $arr['subject'] = '[Mobile Attendance Upload] - ' . $arr->name . ' (' . $arr->sitecode . ')';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Staff Notification
    |--------------------------------------------------------------------------
     */

    // send rm to verify the user photo
    // --------------------------------
    public function getEmailUserUploadPhoto($arr_body, $arr)
    {
        $arr['path'] = 'emails.user.photo.upload';
        $arr['subject'] = '[New Staff Photo] - ' . auth()->user()->name . ' (' . auth()->user()->sitecode . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['rm_email'];
        $arr['name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // upload staff photo manually
    // ---------------------------
    public function getEmailUserUploadManual($arr_body, $arr)
    {
        $arr['path'] = 'emails.user.photo.manual';
        $arr['subject'] = '[New Staff Photo Manual] - ' . $arr['name'] . ' (' . $arr['sitecode'] . ') ' . date('Y-m-d H:i:s');
        $arr['to_system'] = $this->getEmailNoReply();
        $arr['bcc'] = $this->getEmailBcc();            
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // send user upload photo status
    // -----------------------------
    public function getEmailUserPhotoResult($arr_body, $arr)
    {
        $arr['path'] = 'emails.user.photo.result';
        $arr['subject'] = "[Staff Photo - " . $arr['label'] . "!]";
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // cancel user contract
    // --------------------
    public function getEmailCancelContract($arr_body)
    {
        $arr['path'] = 'emails.user.contract.cancel';
        $arr['subject'] = '[Staff Contract - Canceled!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // merge user contract
    // -------------------
    public function getEmailMergeContract($arr_body)
    {
        $arr['path'] = 'emails.user.contract.merge';
        $arr['subject'] = '[Staff Contract - Merged!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // cancel user job
    // ---------------
    public function getEmailCancelJob($arr_body)
    {
        $arr['path'] = 'emails.user.job.cancel';
        $arr['subject'] = '[Staff Job - Canceled!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // update icno user
    // ----------------
    public function getEmailUpdateICNo($arr_body)
    {
        $arr['path'] = 'emails.user.profile';
        $arr['subject'] = '[Staff IC No - Updated!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // update user profile
    // -------------------
    public function getEmailUpdateProfile($arr_body, $arr)
    {
        $arr['path'] = 'emails.user.profile';
        $arr['subject'] = '[Staff Profile - Updated!] - '.$arr['name'].' ('.$arr['sitecode'].')';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }


    // update user imei
    // ----------------
    public function getEmailUpdateImei($arr_body, $arr)
    {
        $arr['path'] = 'emails.user.imei';
        $arr['subject'] = '[Staff Imei - Updated!] - '.$arr['name'].' ('.$arr['sitecode'].')';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Agg Notification
    |--------------------------------------------------------------------------
     */

    // process staff attendance
    // ------------------------
    public function getMcmcProAttendance($arr_body)
    {
        $arr['path'] = 'emails.agg.process.attendance.index';
        $arr['subject'] = "[Process Attendance - Completed!]";
        $arr['to_system'] = $this->getEmailAgg();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // export staff attendance
    // -----------------------
    public function getMcmcExpAttendance($arr_body)
    {
        $arr['path'] = 'emails.agg.export.attendance.index';
        $arr['subject'] = "[Export Attendance - Completed!]";
        $arr['to_system'] = $this->getEmailAgg();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // export staff detail
    // -------------------
    public function getMcmcExpStaff($arr_body, $status)
    {
        $name = ($status == 1) ? 'Active' : 'Inactive';
        $arr['path'] = 'emails.agg.export.staff.index';
        $arr['subject'] = "[Export Staff " . $name . " - Completed!]";
        $arr_body['status'] = $status;
        $arr['to_system'] = $this->getEmailAgg();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // export site detail
    // ------------------
    public function getMcmcExpSite($arr_body)
    {
        $arr['path'] = 'emails.agg.export.site.index';
        $arr['subject'] = "[Export Site - Completed!]";
        $arr['to_system'] = $this->getEmailAgg();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // failed transfer staff attendance
    // --------------------------------
    public function getEmailAggAttFail($arr_body)
    {
        $arr['path'] = 'emails.agg.check.attendance.fail';
        $total = $arr_body['total'];
        $arr['subject'] = "[$total Fail Transfer Attendance!]";
        $arr['to_system'] = $this->getEmailAgg();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // reset staff attendance by date range
    // ------------------------------------
    public function getAggAttReset($arr_body)
    {
        $arr['path'] = 'emails.agg.reset.attendance.index';
        $total = $arr_body['total'];
        $arr['subject'] = "[$total Reset Staff Attendance!]";
        $arr['to_system'] = $this->getEmailAgg();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync site ihr and agg
    // ---------------------
    public function getAggSyncSite($arr_body)
    {
        $arr['path'] = 'emails.agg.sync.site.index';
        $status = $arr_body['status'];
        $arr['subject'] = "[Sync Site $status Aggregator!]";
        $arr['to_system'] = $this->getEmailAgg();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Sync Notification
    |--------------------------------------------------------------------------
     */

    // sync site
    // ---------
    public function getSyncSite($arr_body)
    {
        $arr['path'] = 'emails.erp.sync.site.active';
        $arr['subject'] = '[Site - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync public holiday
    // -------------------
    public function getSyncPublicHoliday($arr_body)
    {
        $year = $arr_body['year'];
        $arr['path'] = 'emails.erp.sync.public-holiday.active';
        $arr['subject'] = '[Public Holiday & State ' . $year . ' - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync staff active
    // -----------------
    public function getSyncStaffActive($arr_body)
    {
        $arr['path'] = 'emails.erp.sync.staff.active';
        $arr['subject'] = '[Staff Active - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync staff contract
    // -------------------
    public function getSyncStaffContract($arr_body)
    {
        $arr['path'] = 'emails.erp.sync.staff.contract';
        $arr['subject'] = '[Staff Contract - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync staff other
    // ----------------
    public function getSyncStaffOther($arr_body)
    {
        $name = $arr_body['name'];
        $arr['path'] = 'emails.erp.sync.staff.other';
        $arr['subject'] = "[Staff $name - Synced!]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync phase
    // ----------
    public function getSyncPhase($arr_body)
    {
        $arr['path'] = 'emails.erp.sync.phase';
        $arr['subject'] = '[Phase - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync position
    // -------------
    public function getSyncPosition($arr_body)
    {
        $arr['path'] = 'emails.erp.sync.position';
        $arr['subject'] = '[Position - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync state
    // ----------
    public function getSyncState($arr_body)
    {
        $arr['path'] = 'emails.erp.sync.state';
        $arr['subject'] = '[State - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // sync region
    // -----------
    public function getSyncRegion($arr_body)
    {
        $arr['path'] = 'emails.erp.sync.region';
        $arr['subject'] = '[Region - Synced!]';
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Off Day Notification
    |--------------------------------------------------------------------------
     */

    // create new off day
    // ------------------
    public function getEmailCreateOffDay($arr_body)
    {
        $arr['path'] = 'emails.off-day.create';
        $arr['subject'] = "[New Off Day - Added!]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // cancel off day
    // --------------
    public function getEmailCancelOffDay($arr_body)
    {
        $arr['path'] = 'emails.off-day.cancel';
        $arr['subject'] = "[Off Day - Canceled!]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // update off day
    // --------------
    public function getEmailUpdateOffDay($arr_body)
    {
        $arr['path'] = 'emails.off-day.update';
        $arr['subject'] = "[Off Day - Updated!]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Public Holiday Notification
    |--------------------------------------------------------------------------
     */

    // cancel public holiday
    // ---------------------
    public function getEmailCancelPublic($arr_body)
    {
        $arr['path'] = 'emails.public-holiday.cancel';
        $arr['subject'] = "[Public Holiday - Canceled!]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // update public holiday
    // ---------------------
    public function getEmailUpdatePublic($arr_body)
    {
        $arr['path'] = 'emails.public-holiday.update';
        $arr['subject'] = "[Public Holiday - Updated!]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // create public holiday
    // ---------------------
    public function getEmailCreatePublic($arr_body)
    {
        $arr['path'] = 'emails.public-holiday.create';
        $arr['subject'] = "[Public Holiday - Created!]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Fasting Notification
    |--------------------------------------------------------------------------
     */
    public function getEmailFastingCreate($arr_body)
    {
        $arr['path'] = 'emails.fasting.create';
        $arr['subject'] = "[New Fasting Date]";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Work Hour Notification
    |--------------------------------------------------------------------------
     */
    public function getEmailCreateWorkHour($arr_body, $arr)
    {
        $arr['path'] = 'emails.work-hour.create';
        $arr['subject'] = '[New Work Hour & Off Dates] - ' . $arr['mgr_name'] . ' (' . $arr['sitecode'] . ') ' . date('Y-m-d H:i:s');
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['cc_email'] = $arr['rm_email'];
        $arr['cc_name'] = $arr['rm_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    public function getEmailUpdateWorkHour($arr_body, $arr)
    {
        $arr['path'] = 'emails.work-hour.edit';
        $arr['subject'] = '[Updated Work Hour & Off Dates] - ' . $arr['mgr_name'] . ' (' . $arr['sitecode'] . ') ' . date('Y-m-d H:i:s');
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Email Notification
    |--------------------------------------------------------------------------
     */
    public function getEmailInsertEmailNoti($arr_body, $arr)
    {
        $arr['path'] = 'emails.email.notification.create';
        $arr['subject'] = "[New Email Notification] - ".$arr->name." (". $arr->email .")";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Announcement Notification
    |--------------------------------------------------------------------------
     */

    public function getEmailAnnoCreate()
    {
    }

    public function getEmailAnnoUpdate()
    {
    }

    public function getEmailAnnoCancel()
    {
    }

    /*
    |--------------------------------------------------------------------------
    | Letter Notification
    |--------------------------------------------------------------------------
     */

    // cancel letter
    // -------------
    public function getEmailLetterCancel($arr_body, $arr)
    {
        $letter_name = $arr['letter_name'];
        $status = $arr['status'];
        $arr['path'] = 'emails.letter.cancel';
        $arr['subject'] = "[$letter_name - $status!]";
        $arr['email'] = $arr['mgr_email'];
        $arr['name'] = $arr['mgr_name'];
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | Training Notification
    |--------------------------------------------------------------------------
     */

    // new training
    // ------------
    public function getEmailTrainingCreate($arr_body)
    {
        $name = $arr_body['training']->name;
        $from = $arr_body['training']->start_date;
        $to = $arr_body['training']->end_date;
        $arr['path'] = 'emails.training.create';
        $arr['subject'] = "[New Training - $name] - From $from To $to";
        $arr['to_system'] = $this->getEmailSync();
        $arr['bcc'] = $this->getEmailBcc();
        $send = $this->getSendEmail($arr_body, $arr);
        return true;
    }

    // site supervisor got new letter
    // ------------------------------
    public function getEmailLetterNew($arr_body, $arr)
    {
        // get email from
        // --------------
        $arr['from'] = $this->getEmailFrom();

        // get email boss
        // --------------
        $arr['boss'] = $this->getEmailBoss();

        // get email bcc
        // -------------
        $arr['bcc'] = $this->getEmailBcc();

        // send email
        // ----------
        $content = Mail::send('emails.letter.create', $arr_body, function ($message) use ($arr) {
            $letter_name = $arr['letter_name'];
            $ordinal = CommonHelper::ordinal($arr['reminder_no']);
            $subject = "[New " . $letter_name . " - " . $ordinal . " Reminder]";
            $message->from($arr['from']);

            // notification to all
            // -------------------
            if ($arr['all'] == 1) {

                // send to site supervisor
                // -----------------------
                $message->to($arr['mgr_email'], $arr['mgr_name'])->subject($subject);

                // include email boss into cc (rm & drm)
                // -------------------------------------
                $cc = $arr['cc'] + $arr['boss'];

                // cc to region manager & deputy region manager
                // --------------------------------------------
                $message->cc($cc)->subject($subject);

                // cc to human resource
                // --------------------
                $message->cc('hr.msd@msd.net.my', 'Human Resource')->subject($subject);
            }

            // notification to hr only
            // -----------------------
            else {
                $message->to('hr.msd@msd.net.my', 'Human Resource')->subject($subject);
            }

            // send to bcc
            // -----------
            $message->bcc($arr['bcc'])->subject($subject);

            // check if have attachment
            // ------------------------
            if (count($arr['attach']) > 0) {

                // set value attachment
                // --------------------
                $file = $arr['attach']['file'];
                $as = $arr['attach']['as'];
                $mime = $arr['attach']['mime'];

                // attach attachment
                // -----------------
                if (!empty($file) && !empty($as) && !empty($mime)) {
                    $message->attach($file, ['as' => $as, 'mime' => $mime]);
                }
            }
        });
        return $content;
    }
}
