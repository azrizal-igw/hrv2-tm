<?php
namespace IhrV2\Repositories\Letter;

use IhrV2\Contracts\Attendance\AttendanceInterface;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Letter\DbLetterInterface;
use IhrV2\Contracts\Letter\LetterInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Helpers\CommonHelper;
use IhrV2\Models\Letter;

class LetterRepository implements LetterInterface {
    protected $db_letter_repo;
    protected $leave_repo;
    protected $user_repo;
    protected $upload_repo;
    protected $email_repo;
    protected $att_repo;

    public function __construct(DbLetterInterface $db_letter_repo, LeaveInterface $leave_repo, UserInterface $user_repo, UploadInterface $upload_repo, EmailInterface $email_repo, AttendanceInterface $att_repo) {
        $this->db_letter_repo = $db_letter_repo;
        $this->leave_repo = $leave_repo;
        $this->user_repo = $user_repo;
        $this->upload_repo = $upload_repo;
        $this->email_repo = $email_repo;
        $this->att_repo = $att_repo;
    }

    // get lists of letters
    // --------------------
    public function getLetterIndex($request = null) 
    {
        $header = array(
            'parent' => 'Staff Administration',
            'child'  => 'Letter',
            'icon'   => 'envelope',
            'title'  => 'Letter'
        );

        // check if have request
        // ---------------------
        if (empty($request)) {

            // get session
            // -----------
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $position_id = CommonHelper::checkSession('position_id');
            $letter_type_id = CommonHelper::checkSession('letter_type_id');
            $reminder_no = CommonHelper::checkSession('reminder_no');
            $status_id = CommonHelper::checkSession('status_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('site_id', 'region_id', 'phase_id', 'position_id', 'letter_type_id', 'reminder_no', 'status_id', 'keyword'));
                return redirect()->route('mod.letter.index');
            }

            // get session
            // -----------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $position_id = CommonHelper::checkSessionPost($request->position_id, $reset, 'position_id');
            $letter_type_id = CommonHelper::checkSessionPost($request->letter_type_id, $reset, 'letter_type_id');
            $reminder_no = CommonHelper::checkSessionPost($request->reminder_no, $reset, 'reminder_no');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];
        $groups = $chk['groups'];

        // get drop down lists
        // -------------------
        $letter_types = $this->getLetterTypeDrop();        
        $reminder_lists = $this->getListReminderNo();
        $phases = $this->user_repo->getPhaseList();
        $regions = $this->user_repo->getRegionList();
        $positions = $this->user_repo->getPositionListDistinct($groups);
        $status = $this->getLetterStatus();

        // set sessions
        // ------------
        $sessions = array(
            'site_id'        => $site_id,
            'region_id'      => $region_id,
            'phase_id'       => $phase_id,
            'position_id'    => $position_id,
            'letter_type_id' => $letter_type_id,
            'reminder_no'    => $reminder_no,
            'status_id'      => $status_id,
            'keyword'        => $keyword
        );

        // get lists of letter
        // -------------------
        $letters = $this->getListLetterAll($codes, $sessions);
        return view('modules.letter.index', compact('header', 'sites', 'letter_types', 'reminder_lists', 'phases', 'regions', 'positions', 'status', 'sessions', 'letters'));
    }

    // lists of letter status
    // ----------------------
    public function getLetterStatus() {
        $q = array('' => '[Status]', 1 => 'Active', 2 => 'Cancel');
        return $q;
    }

    // lists of letter types
    // ---------------------
    public function getLetterTypeDrop() {
        $q = \IhrV2\Models\LetterType::orderBy('id', 'asc')->pluck('name', 'id')->prepend('[Letter Type]', '');
        return $q;
    }

    // get all letter types lists
    // --------------------------
    public function getLetterTypeList() {
        return \IhrV2\Models\LetterType::get();
    }

    // get letter info
    // ---------------
    public function getLetterInfo($id) {
        $q = \IhrV2\Models\Letter::where(['id' => $id])
            ->with(array('LetterTypeName', 'LetterActionBy'))
            ->first();
        return $q;
    }

    // get letter by id
    // ----------------
    public function getLetterByID($id)
    {
        return \IhrV2\Models\Letter::find($id);
    }

    // get letter info by id and user id
    // ---------------------------------
    public function getLetterByUserID($id, $uid) {
        $q = \IhrV2\Models\Letter::where([
            'id'      => $id,
            'user_id' => $uid
        ])
            ->with(array('LetterTypeName', 'LetterActionBy'))
            ->first();
        return $q;
    }

    public function getLetterCheck($id, $uid, $sitecode) {
        $q = \IhrV2\Models\Letter::where([
            'id'       => $id,
            'user_id'  => $uid,
            'sitecode' => $sitecode
            // 'status' => 1
        ])
            ->with(array(
                'LetterTypeName',
                'LetterActionBy',
                'LetterAttachment',
                'LetterSiteName',
                'LetterReportTo',
                'LetterUpdatedBy'
            ))
            ->first();
        return $q;
    }

    // process new letter
    // ------------------
    public function getProcessNewLetter($data, $uid, $sitecode) 
    {
        // save letter info
        // ----------------
        $letter_id = $this->db_letter_repo->dbInsertLetter($data, $uid, $sitecode);

        // check if have attachment
        // ------------------------
        $attach = array();
        if (!empty($data['letter_file'])) {

            // upload file attachment
            // ----------------------
            $attach = $this->upload_repo->uploadLetterNew($data['letter_file'], $letter_id, $uid, $sitecode);
        }

        // get user info
        // -------------
        $user = $this->user_repo->getUserCheckIDSitecode($uid, $sitecode);

        // get rm info
        // -----------
        $get_rm = $this->user_repo->getUserByID($data['report_to']);

        // get letter info
        // ---------------
        $letter = $this->getLetterInfo($letter_id);

        // check the notification to all
        // -----------------------------
        $drm = array();
        if (!empty($data['email'])) {
            $all = 1;

            // get drm region
            // --------------
            $cond = array('region_id' => $data['region_id'], 'position_id' => 2, 'status' => 1);
            $chk_drm = $this->att_repo->getDrmRegionList($cond);
            if (!empty($chk_drm)) {
                foreach ($chk_drm->toArray() as $d) {
                    $drm[$d['email']] = $d['name'];
                }
            }
        }

        // uncheck email notification
        // --------------------------
        else {
            $all = 0;
        }

        // set email and name of rm
        // ------------------------
        $rm[$get_rm->email] = $get_rm->name;

        // get email header
        // ----------------
        $arr = array(
            'mgr_name'    => $user->name,
            'mgr_email'   => $user->email,
            'cc'          => $rm + $drm,
            'letter_name' => $letter->LetterTypeName->name,
            'reminder_no' => $letter->reminder_no,
            'attach'      => $attach,
            'all'         => $all
        );

        // get site info
        // -------------
        if (!empty($user->SiteNameInfo)) {
            $sitename = $user->SiteNameInfo->code . ' ' . $user->SiteNameInfo->name;
        } else {
            $sitename = null;
        }

        // get position
        // ------------
        if (!empty($user->UserLatestJob)) {
            $position = $user->UserLatestJob->PositionName->name;
        } else {
            $position = null;
        }

        // get action by
        // -------------
        if (!empty($letter->LetterActionBy)) {
            $report_by = $letter->LetterActionBy->GroupName->name;
        } else {
            $report_by = null;
        }

        // get email body
        // --------------
        $arr_body = array(
            'letter'    => $letter,
            'report_by' => $report_by,
            'name'      => $user->name,
            'position'  => $position,
            'sitename'  => $sitename,
            'all'       => $all
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailLetterNew($arr_body, $arr);

        // return message
        // --------------
        return array('Letter successfully Added.', 'success');
    }

    public function getUserInfo($uid, $sitecode) 
    {
        // get user info
        // -------------
        $user = $this->leave_repo->getCheckUserActive($uid, $sitecode);

        // get contract info
        // -----------------
        $contract = $user['user']['user_latest_contract'];
        $date_from = $contract['date_from'];
        $date_to = $contract['date_to'];

        // get job info
        // ------------
        $job = $user['user']['user_latest_job'];
        $position = $job['position_name']['name'];

        // get site info
        // -------------
        $site = $user['user']['site_name_info'];

        // get rm info
        // -----------
        $chk_rm = $this->user_repo->getRegionBySitecode($sitecode);
        $rm_name = $chk_rm['region_manager_info']['region_manager_detail']['name'];
        $rm_id = $chk_rm['region_manager_info']['user_id'];
        $region_id = $chk_rm['region_manager_info']['region_id'];

        // get display info
        // ----------------
        $info = array(
            'uid'       => $uid,
            'sitecode'  => $sitecode,
            'name'      => $user['user']['name'],
            'icno'      => $user['user']['icno'],
            'position'  => $position,
            'sitename'  => $site['name'],
            'date_from' => $date_from,
            'date_to'   => $date_to,
            'rm_name'   => $rm_name,
            'rm_id'     => $rm_id,
            'region_id' => $region_id
        );
        return $info;
    }

    // get all letter of user
    // ----------------------
    public function getLetterAll($uid, $sitecode) {
        $q = \IhrV2\Models\Letter::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode
            // 'status' => 1
        ])
            ->with(array('LetterTypeName', 'LetterAttachment', 'LetterActionBy'))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

    // cancel letter
    // -------------
    public function getLetterCancel($id, $uid, $sitecode) {

        // update updated_by and status
        // ----------------------------
        $update = $this->db_letter_repo->dbUpdateLetter($id, $uid, $sitecode);

        // successfully update
        // -------------------
        if ($update) {

            // get letter info
            // ---------------
            $letter = $this->getLetterByID($id);

            // get email header
            // ----------------
            $arr = array(
                'mgr_name' => $letter->LetterUserDetail->name,
                'mgr_email' => $letter->LetterUserDetail->email,
                'letter_name' => $letter->LetterTypeName->name,                
                'status' => 'Canceled'
            );

            // get email body
            // --------------
            $arr_body = array(
                'letter' => $letter,
                'letter_name' => $letter->LetterTypeName->name,
                'updated_by' => $letter->LetterUpdatedBy->name
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailLetterCancel($arr_body, $arr);

            // return message
            // --------------
            return array('Letter successfully Cancel.', 'success');
        }

        // fail to update
        // --------------
        else {
            return array('Letter already cancel.', 'danger');
        }

    }

    // lists of reminder no
    // --------------------
    public function getListReminderNo() {
        return array('' => '[Reminder No]') + array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5);
    }

    // get lists of all letter
    // -----------------------
    public function getListLetterAll($codes, $arr) {

        // get the search values
        // ---------------------
        $site_id = $arr['site_id'];
        $region_id = $arr['region_id'];
        $phase_id = $arr['phase_id'];
        $position_id = $arr['position_id'];
        $letter_type_id = $arr['letter_type_id'];
        $reminder_no = $arr['reminder_no'];
        $status_id = $arr['status_id'];
        $keyword = $arr['keyword'];
        $jobs = array('region_id' => $region_id, 'phase_id' => $phase_id, 'position_id' => $position_id);

        // query letter
        // ------------
        $q = \IhrV2\Models\Letter::with(
            array(
                'LetterTypeName',
                'LetterUserDetail',
                'LetterSiteName',
            ))
            ->with(array('LetterUserJob' => function ($h) {
                $h->with(array('RegionName', 'PositionName'));
            }))
            ->where(function ($k) use ($codes) {
                if (count($codes) > 0) {
                    foreach ($codes as $code) {
                        $k->orWhere('sitecode', 'like', $code . '%');
                    }
                }
            })
            ->where(function ($s) use ($site_id) {
                if (!empty($site_id)) {
                    $s->where('sitecode', $site_id);
                }
            })
            ->where(function ($t) use ($letter_type_id) {
                if (!empty($letter_type_id)) {
                    $t->where('letter_type_id', $letter_type_id);
                }
            })
            ->where(function ($a) use ($reminder_no) {
                if (!empty($reminder_no)) {
                    $a->where('reminder_no', $reminder_no);
                }
            })
            ->where(function ($b) use ($status_id) {
                if (!empty($status_id)) {
                    $b->where('status', $status_id);
                }
            })            
            ->whereHas('LetterUserJob', function ($l) use ($jobs) {
                foreach ($jobs as $column => $key) {
                    if (!is_null($key)) {
                        $l->where($column, $key);
                    }
                }                
            })
            ->whereHas('LetterUserDetail', function ($i) use ($keyword) {
                if (!empty($keyword)) {
                    $i->where('name', 'like', '%' . $keyword . '%');
                    $i->orWhere('icno', 'like', '%' . $keyword . '%');
                    $i->orWhere('username', 'like', '%' . $keyword . '%');
                    $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
                }
            })
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    // get all letter of user
    // ----------------------
    public function getLetterAllActive($uid, $sitecode) {
        $q = \IhrV2\Models\Letter::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'status'   => 1
        ])
            ->with(array('LetterTypeName', 'LetterAttachment', 'LetterActionBy'))
            ->orderBy('id', 'desc')
            ->get();
        return $q;
    }

}
