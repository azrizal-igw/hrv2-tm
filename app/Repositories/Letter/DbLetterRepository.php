<?php namespace IhrV2\Repositories\Letter;

use Carbon\Carbon;
use IhrV2\Contracts\Letter\DbLetterInterface;

class DbLetterRepository implements DbLetterInterface {

    // insert letter
    // -------------
    public function dbInsertLetter($data, $uid, $sitecode) {
        $letter_date = Carbon::createFromFormat('d/m/Y', $data['letter_date'])->format('Y-m-d');
        $arr = array(
            'letter_type_id' => $data['letter_type_id'],
            'user_id' => $uid,
            'letter_date' => $letter_date,
            'reminder_no' => $data['reminder_no'],
            'action_date' => date('Y-m-d H:i:s'),
            'action_by' => auth()->user()->id,
            'notes' => $data['notes'],
            'report_to' => $data['report_to'],
            'sitecode' => $sitecode,
            'status' => 1,
        );
        $sav = new \IhrV2\Models\Letter($arr);
        $sav->save();
        return $sav->id;
    }

    // insert letter attachment
    // ------------------------
    public function dbInsertLetterAttach($letter_id, $uid, $i) {
        $arr = array(
            'user_id' => $uid,
            'letter_id' => $letter_id,
            'filename' => $i['filename'],
            'ext' => strtolower($i['fileext']),
            'size' => $i['filesize'],
            'thumb_name' => $i['filethumb'],
            'status' => 1,
        );
        $sav = new \IhrV2\Models\LetterAttachment($arr);
        $sav->save();
        return true;
    }

    // update letter info
    // ------------------
    public function dbUpdateLetter($id, $uid, $sitecode) {
        $q = \IhrV2\Models\Letter::where([
            'id' => $id,
            'user_id' => $uid,
            'sitecode' => $sitecode,
            'status' => 1,
        ])

            ->update(array('updated_by' => auth()->user()->id, 'status' => 2));

        return true;
    }

}
