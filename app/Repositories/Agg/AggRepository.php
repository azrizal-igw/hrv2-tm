<?php
namespace IhrV2\Repositories\Agg;

use Carbon\Carbon;
use IhrV2\Contracts\Agg\AggInterface;
use IhrV2\Contracts\Agg\DbAggInterface;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Mcmc\McmcInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Helpers\CommonHelper;

class AggRepository implements AggInterface {

    protected $mcmc_repo;
    protected $db_agg_repo;
    protected $email_repo;
    protected $user_repo;

    public function __construct(McmcInterface $mcmc_repo, DbAggInterface $db_agg_repo, EmailInterface $email_repo, UserInterface $user_repo) {
        $this->mcmc_repo = $mcmc_repo;
        $this->db_agg_repo = $db_agg_repo;
        $this->email_repo = $email_repo;
        $this->user_repo = $user_repo;
    }

    // get staff transfer
    // ------------------
    public function getStaffTransfer($request = null) {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Staff',
            'sub'    => 'Check Transfer',
            'icon'   => 'check',
            'title'  => 'Check Transfer'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $position_id = CommonHelper::checkSession('position_id');
            $status_id = CommonHelper::checkSession('status_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget('site_id');
                session()->forget('position_id');
                session()->forget('status_id');
                session()->forget('keyword');
                return redirect()->route('mod.agg.staff.list');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $position_id = CommonHelper::checkSessionPost($request->position_id, $reset, 'position_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');

        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get lists of status
        // -------------------
        $status = $this->getStaffStatusList();

        // get lists of positions
        // ----------------------
        $positions = $this->getStaffPositionList();

        // get staff detail agg
        // --------------------
        $arr = array($site_id, $position_id, $status_id, $keyword);
        $users = $this->getStaffDetailList($arr, $codes);
        return view('modules.agg.staff.agg.index', compact('header', 'sites', 'status', 'positions', 'users'));
    }

    // get staff manual
    // ----------------
    public function getStaffManual($request = null) {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Staff Detail',
            'sub'    => 'Lists Manual',
            'icon'   => 'list',
            'title'  => 'Lists Manual'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $status_id = CommonHelper::checkSession('status_id');
            $export_id = CommonHelper::checkSession('export_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget('site_id');
                session()->forget('status_id');
                session()->forget('export_id');
                session()->forget('keyword');
                return redirect()->route('mod.agg.staff.manual.list');
            }
            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $export_id = CommonHelper::checkSessionPost($request->export_id, $reset, 'export_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        // get codes and sites
        // -------------------
        $chk = $this->user_repo->getListSiteNCode();
        $codes = $chk['codes'];
        $sites = $chk['sites'];

        // get lists of status
        // -------------------
        $status = array('' => '[Status]', 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE');

        // get lists of status export
        // --------------------------
        $export = array('' => '[Export]', 1 => 'SEND', 2 => 'NOT YET');

        // get lists of updated manual
        // ---------------------------
        $arr = array($site_id, $status_id, $export_id, $keyword);
        $manual = $this->user_repo->getUserManualList($arr, $codes);

        // search sessions
        // ---------------
        $sessions = array(
            'site_id'   => $site_id,
            'status_id' => $status_id,
            'export_id' => $export_id,
            'keyword'   => $keyword
        );
        return view('modules.agg.staff.manual.index', compact('header', 'sites', 'status', 'export', 'manual', 'sessions'));
    }

    // get attendance transfer
    // -----------------------
    public function getAttendanceCheck($request = null) {
        $header = array(
            'parent' => 'Aggregator',
            'child'  => 'Attendance',
            'sub'    => 'Check Transfer',
            'icon'   => 'check',
            'title'  => 'Check Transfer'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $site_id = CommonHelper::checkSession('site_id');
            $status_id = CommonHelper::checkSession('status_id');
            $date = CommonHelper::checkSession('date');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget('site_id');
                session()->forget('status_id');
                session()->forget('date');
                return redirect()->route('mod.agg.attendance.submitted');
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $date = CommonHelper::checkSessionPost($request->date, $reset, 'date');
        }

        // get lists of sites
        // ------------------
        $codes = array();
        if (in_array(auth()->user()->group_id, array(4, 6, 7))) {
            $codes = $this->user_repo->getSitecodeByRegion(session()->get('job')['region_id']);
            $sites = $this->user_repo->getSiteListWithID(session()->get('job')['region_id']);
        } else {
            $sites = $this->user_repo->getSiteListWithID();
        }

        // get search session
        // ------------------
        $sessions = array(
            'site_id'   => $site_id,
            'status_id' => $status_id,
            'date'      => $date
        );

        // get lists of status
        // -------------------
        $status = $this->getStatusListAll();

        // get mycomm attendance
        // ---------------------
        $att = $this->getMyCommAtt($sessions);
        return view('modules.agg.attendance.check.index', compact('header', 'sites', 'status', 'att', 'sessions'));
    }

    // execute process/export/reset attendance
    // ---------------------------------------
    public function getAttProExpExe($i) {
        // get date
        // --------
        $start = Carbon::createFromFormat('d/m/Y', $i['start_date'])->format('Y-m-d');
        $end = Carbon::createFromFormat('d/m/Y', $i['end_date'])->format('Y-m-d');

        // get token
        // ---------
        $token = '?api_token=' . auth()->user()->api_token;

        // process attendance
        // ------------------
        if ($i['type_id'] == 1) {
            if ($i['by_id'] == 1) {
                $path = route('api.agg.att.process.start.end', array($start, $end));
            } else {
                $path = route('api.agg.att.process.sitecode.start.end', array($i['sitecode'], $start, $end));
            }
            // $url = json_decode(SimpleCurl::get($path . $token)->getResponse(), true);
            $arr = array('Process Attendance is Completed.', 'success');
        }

        // export attendance
        // -----------------
        else if ($i['type_id'] == 2) {
            if ($i['by_id'] == 1) {
                $path = route('api.agg.att.export.start.end', array($start, $end));
            } else {
                $path = route('api.agg.att.export.sitecode.start.end', array($i['sitecode'], $start, $end));
            }
            // $url = json_decode(SimpleCurl::get($path . $token)->getResponse(), true);
            $arr = array('Export Attendance is Completed.', 'success');
        }

        // reset attendance
        // ----------------
        else {
            $this->mcmc_repo->getResetAttStartEnd($start, $end);
            $arr = array('Reset is Completed.', 'success');
        }

        // return message
        // --------------
        return $arr;
    }

    public function getListProcessType() {
        $q = array(
            1 => 'By Date',
            2 => 'By Date and Sitecode'
        );
        return $q;
    }

    public function getListTypes() {
        $q = array(
            '' => '[Select Type]',
            1  => 'Process',
            2  => 'Export',
            3  => 'Reset'
        );
        return $q;
    }

    // get lists of mycomm attendance
    // ------------------------------
    public function getMyCommAtt($arr) {
        // set value from array
        // --------------------
        $site_id = $arr['site_id'];
        $status_id = $arr['status_id'];
        $date = $arr['date'];

        // query mycomm attendance
        // -----------------------
        $q = \IhrV2\Models\AggStaffAttendance::with(array('UserDetail', 'SiteDetail'))
            ->where(function ($s) use ($site_id) {
                if (!empty($site_id)) {
                    $s->where('PI1M_REFID', $site_id);
                }
            })
            ->where(function ($st) use ($status_id) {
                if (!empty($status_id)) {
                    $st->where('CheckIn_Status', $status_id);
                }
            })
            ->where(function ($d) use ($date) {
                if (!empty($date)) {
                    $d->whereDate('CheckIn_Date', '=', $date);
                }
            })
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    public function getProcessName($id) {
        $name = null;
        $lists = $this->getListProcessType();
        foreach ($lists as $i => $j) {
            if ($i == $id) {
                $name = $j;
            }
        }
        return $name;
    }

    // get lists of status
    // -------------------
    public function getStatusListAll() {
        $q = \IhrV2\Models\AttendanceStatus::orderBy('id', 'asc')
            ->pluck('name', 'name')
            ->prepend('[Status]', '');
        return $q;
    }

    public function getTypeName($id) {
        $name = null;
        $lists = $this->getListTypes();
        foreach ($lists as $i => $j) {
            if ($i == $id) {
                $name = $j;
            }
        }
        return $name;
    }

    // get detail staff attendance
    // ---------------------------
    public function getMyCommAttDetail($id) {
        $q = \IhrV2\Models\AggStaffAttendance::find($id);
        return $q;
    }

    // sync site detail
    // ----------------
    public function getSiteSync($status) {

        // set initial
        // -----------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;

        // get all sites
        // -------------
        $sites = \IhrV2\Models\Site::where('status', $status)
            ->with(array('PhaseName', 'StateName'))
            ->whereNotIn('code', array('X01C001'))
            ->orderBy('id', 'desc')
            ->get();
        $total = count($sites);

        // record site exist
        // -----------------
        if (!empty($sites)) {
            foreach ($sites as $i) {

                // check state
                // -----------
                $state = null;
                if (!empty($i->StateName)) {
                    $state = $i->StateName->name;
                }

                // check phase
                // -----------
                $phase = null;
                if (!empty($i->PhaseName)) {
                    $phase = $i->PhaseName->name;
                }

                // check at mycomm
                // ---------------
                $agg = \IhrV2\Models\AggSiteDetail::where('REF_ID', $i->code)->first();
                if (!empty($agg)) {

                    // compare date modified
                    // ---------------------
                    if (Carbon::parse($i->date_modified)->ne(Carbon::parse($agg->date_modified))) {

                        // insert site mycomm history
                        // --------------------------
                        $this->db_agg_repo->dbInsertSiteHistory($agg->toArray());

                        // update site mycomm
                        // ------------------
                        $this->db_agg_repo->dbUpdateSite($i->toArray(), $state, $phase);
                        $update++;
                    }
                }

                // no record at mycomm
                // -------------------
                else {

                    // insert new site
                    // ---------------
                    $this->db_agg_repo->dbInsertSite($i, $state, $phase);
                    $insert++;
                }
            }
        }
        $arr = array(
            'start'  => $start,
            'end'    => date('Y-m-d H:i:s'),
            'status' => ($status == 1) ? 'Active' : 'Inactive',
            'total'  => $total,
            'insert' => $insert,
            'update' => $update
        );

        // send email notification
        // -----------------------
        $this->email_repo->getAggSyncSite($arr);
        return $arr;
    }

    // get staff detail lists
    // ----------------------
    public function getStaffDetailList($arr, $codes) {
        // set value from array
        // --------------------
        $site_id = $arr[0];
        $position_id = $arr[1];
        $status_id = $arr[2];
        $keyword = $arr[3];

        // query staff detail
        // ------------------
        $q = \IhrV2\Models\AggStaffDetail::with(array('SiteDetail'))
            ->where(function ($s) use ($site_id) {
                if (!empty($site_id)) {
                    $s->where('PI1M_REFID', $site_id);
                }
            })
            ->where(function ($p) use ($position_id) {
                if (!empty($position_id)) {
                    $p->where('Position', $position_id);
                }
            })
            ->where(function ($t) use ($status_id) {
                if (!empty($status_id)) {
                    $t->where('Status', $status_id);
                }
            })
            ->where(function ($k) use ($keyword) {
                if (!empty($keyword)) {
                    $k->where('Staff_Name', 'like', '%' . $keyword . '%');
                    $k->orWhere('Staff_IC', 'like', '%' . $keyword . '%');
                }
            })
            ->orderBy('id', 'desc')
            ->paginate(20);
        return $q;
    }

    // get distinct status staff detail
    // --------------------------------
    public function getStaffStatusList() {
        $q = \IhrV2\Models\AggStaffDetail::distinct()->pluck('Status', 'Status')->prepend('[Status]', '');
        return $q;
    }

    // get distinct position staff detail
    // ----------------------------------
    public function getStaffPositionList() {
        $q = \IhrV2\Models\AggStaffDetail::distinct()->pluck('Position', 'Position')->prepend('[Position]', '');
        return $q;
    }

    // get staff detail aggregator
    // ---------------------------
    public function getAggStaffDetail($id) {
        $q = \IhrV2\Models\AggStaffDetail::where('id', $id)->first();
        return $q;
    }

}
