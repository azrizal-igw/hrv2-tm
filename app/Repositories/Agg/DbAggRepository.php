<?php

namespace IhrV2\Repositories\Agg;

use IhrV2\Contracts\Agg\DbAggInterface;

class DbAggRepository implements DbAggInterface {

    // insert site detail history
    // --------------------------
    public function dbInsertSiteHistory($i) {
        $arr = array(
            'REF_ID'           => $i['REF_ID'],
            'SITE_NAME'        => $i['SITE_NAME'],
            'STATE'            => $i['STATE'],
            'ADDRESS1'         => $i['ADDRESS1'],
            'ADDRESS2'         => $i['ADDRESS2'],
            'POSTCODE'         => $i['POSTCODE'],
            'CITY'             => $i['CITY'],
            'SERVICE_PROVIDER' => $i['SERVICE_PROVIDER'],
            'PHASE'            => $i['PHASE'],
            'date_modified'    => $i['date_modified'],
            'BANDWIDTH'        => $i['BANDWIDTH'],
            'TECHNOLOGY'       => $i['TECHNOLOGY']
        );
        $sav = new \IhrV2\Models\AggSiteDetailHistory($arr);
        $sav->save();
        return true;
    }

    // update site detail
    // ------------------
    public function dbUpdateSite($i, $state, $phase) {
        $arr = array(
            'SITE_NAME'        => $i['name'],
            'STATE'            => $state,
            'ADDRESS1'         => $i['street1'],
            'ADDRESS2'         => $i['street1'],
            'POSTCODE'         => $i['postal_code'],
            'CITY'             => $i['city'],
            'SERVICE_PROVIDER' => 'TM',
            'PHASE'            => $phase,
            'date_modified'    => $i['date_modified'],
            'BANDWIDTH'        => $i['bandwidth'],
            'TECHNOLOGY'       => $i['backhaul']
        );
        $sav = \IhrV2\Models\AggSiteDetail::where('REF_ID', $i['code'])->update($arr);
        return true;
    }

    // insert site detail
    // ------------------
    public function dbInsertSite($i, $state, $phase) {
        $arr = array(
            'REF_ID'           => $i['code'],
            'SITE_NAME'        => $i['name'],
            'STATE'            => $state,
            'ADDRESS1'         => $i['street1'],
            'ADDRESS2'         => $i['street2'],
            'POSTCODE'         => $i['postal_code'],
            'CITY'             => $i['city'],
            'SERVICE_PROVIDER' => 'TM',
            'PHASE'            => $phase,
            'BANDWIDTH'        => $i['bandwidth'],
            'TECHNOLOGY'       => $i['backhaul']
        );
        $sav = new \IhrV2\Models\AggSiteDetail($arr);
        $sav->save();
        return true;
    }

}
