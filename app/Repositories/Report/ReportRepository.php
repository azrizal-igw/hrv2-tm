<?php namespace IhrV2\Repositories\Report;

use Carbon\Carbon;
use IhrV2\Contracts\Report\ReportInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Download\DownloadInterface;

class ReportRepository implements ReportInterface
{
    protected $leave_repo;
    protected $user_repo;
    protected $down_repo;

    public function __construct(LeaveInterface $leave_repo, UserInterface $user_repo, DownloadInterface $down_repo)
    {
        $this->leave_repo = $leave_repo;
        $this->user_repo = $user_repo;
        $this->down_repo = $down_repo;
    }

    // report user
    // -----------
	public function getReportStaff($request = null)
	{
        $header = array(
            'parent'  => 'Report',
            'child'   => 'Staff',
            'icon'    => 'people',
            'title'   => 'Staff'
        );

        // have request
        // ------------
        if (!empty($request)) {

            // get staff record by phases
            // --------------------------
            $datas = $this->user_repo->getUserGroupByPhase($request->month, $request->year, $request->status);

            // download masterlist
            // -------------------
            $this->down_repo->getUserByExcel($datas);
        }

        // get lists down value
        // --------------------
        $months = $this->user_repo->getMonthList();
        $years = $this->leave_repo->getListLongYear();        
        $status = $this->user_repo->getUserStatusActive();        
        return View('modules.report.staff.index', compact('header', 'months', 'years', 'status'));
	}

}


