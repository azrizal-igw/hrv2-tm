<?php namespace IhrV2\Repositories\Maintenance;

use Carbon\Carbon;
use IhrV2\Contracts\Maintenance\DbMaintenanceInterface;

class DbMaintenanceRepository implements DbMaintenanceInterface {

    // insert region user
    // ------------------
    public function dbInsertRegionUser($i, $id) {
        $arr = array(
            'region_id' => $id,
            'user_id'   => $i['report_to'],
            'status'    => 1
        );
        $sav = new \IhrV2\Models\RegionUser($arr);
        $sav->save();
        return true;
    }

    // update region user (current status to 0)
    // ----------------------------------------
    public function dbUpdateRegionUser($id) {
        $arr = array(
            'status' => 0
        );
        $sav = \IhrV2\Models\RegionUser::where('region_id', $id)->where('status', 1)->update($arr);
        return true;
    }

    // update region
    // -------------
    public function dbUpdateRegion($i, $id) {
        $arr = array(
            'name'     => $i['name'],
            'name_eng' => $i['name_eng']
        );
        $sav = \IhrV2\Models\Region::where('id', $id)->update($arr);
        return true;
    }

    // insert site
    // -----------
    public function dbInsertSite($i) {
        $val = array(
            'code'          => $i['code'],
            'name'          => $i['name'],
            'full_name'     => $i['full_name'],
            'phase_id'      => $i['phase_id'],
            'region_id'     => $i['region_id'],
            'street1'       => $i['street1'],
            'street2'       => $i['street2'],
            'postal_code'   => $i['postal_code'],
            'city'          => $i['city'],
            'district'      => $i['district'],
            'state_id'      => $i['state_id'],
            'address_label' => $i['address_label'],
            'email'         => $i['email'],
            'website'       => $i['website'],
            'latitude'      => $i['latitude'],
            'longitude'     => $i['longitude'],
            'bandwidth'     => $i['bandwidth'],
            'backhaul'      => $i['backhaul'],
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        );
        $sav = new \IhrV2\Models\Site($val);
        $sav->save();
        return true;
    }

    // update site
    // -----------
    public function dbUpdateSite($i, $id) {
        $val = array(
            'name'          => $i['name'],
            'full_name'     => $i['full_name'],
            'phase_id'      => $i['phase_id'],
            'region_id'     => $i['region_id'],
            'street1'       => $i['street1'],
            'street2'       => $i['street2'],
            'postal_code'   => $i['postal_code'],
            'city'          => $i['city'],
            'district'      => $i['district'],
            'state_id'      => $i['state_id'],
            'address_label' => $i['address_label'],
            'email'         => $i['email'],
            'website'       => $i['website'],
            'latitude'      => $i['latitude'],
            'longitude'     => $i['longitude'],
            'bandwidth'     => $i['bandwidth'],
            'backhaul'      => $i['backhaul']
        );
        $upd = \IhrV2\Models\Site::where('id', $id)->update($val);
        return true;
    }

    // cancel site
    // -----------
    public function dbCancelSite($id) {
        $upd = \IhrV2\Models\Site::where('id', $id)
            ->update(array(
                'status' => 2
            ));
        return true;
    }

    // delete site
    // -----------
    public function dbDeleteSite() {
        $msg = array('Site successfully deleted.', 'success', 'mod.site.index');
        return $msg;
    }

    // insert public holiday
    // ---------------------
    public function dbInsertPublicHoliday($i) {
        $arr = array(
            'year' => $i['year'],
            'desc' => $i['desc'],
            'date' => $i['date'],
            'status' => 1
        );
        $sav = new \IhrV2\Models\LeavePublic($arr);
        $sav->save();
        return $sav->id;
    }

    // insert public holiday states
    // ----------------------------
    public function dbInsertPublicHolidayState($i) {
        $arr = array(
            'leave_public_id' => $i['public_id'],
            'year'            => $i['year'],
            'date'            => $i['date'],
            'state_id'        => $i['state_id'],
            'status'          => 1
        );
        $sav = new \IhrV2\Models\LeavePublicState($arr);
        $sav->save();
        return true;
    }

    // cancel public holiday
    // ---------------------
    public function dbCancelPublicHoliday($id) {
        $arr = array('id' => $id, 'status' => 1);
        \IhrV2\Models\LeavePublic::where($arr)->update(array('status' => 2, 'updated_by' => auth()->user()->id));
        \IhrV2\Models\LeavePublicState::where(['leave_public_id' => $id, 'status' => 1])->update(array('status' => 2));
        return true;
    }

    // update public holiday
    // ---------------------
    public function dbUpdatePublicHoliday($i, $id) {
        $arr = array(
            'year' => $i['year'],
            'desc' => $i['desc'],
            'date' => $i['date'],
            'updated_by' => auth()->user()->id
        );
        \IhrV2\Models\LeavePublic::where('id', $id)->update($arr);
        return true;
    }

    // reset public holiday states
    // ---------------------------
    public function dbResetPublicState($id) {
        return \IhrV2\Models\LeavePublicState::where('leave_public_id', $id)->where('status', 1)->update(array('status' => 2));
    }

    // insert public holiday states
    // ----------------------------
    public function dbPublicStateInsert($i, $id) {
        $arr = array(
            'leave_public_id' => $id,
            'year'            => $i['year'],
            'date'            => $i['date'],
            'state_id'        => $i['code'],
            'status'          => 1
        );
        $sav = new \IhrV2\Models\LeavePublicState($arr);
        $sav->save();
        return true;
    }

    public function dbInsertLeaveOffDate($i, $date) {
        $arr = array(
            'action_date' => Carbon::now(),
            'action_by'   => auth()->user()->id,
            'year'        => $i['off_year'],
            'group_id'    => $i['off_group'],
            'type_id'     => $i['off_type'],
            'off_date'    => $date,
            'status'      => 1
        );
        $sav = new \IhrV2\Models\LeaveOffDate($arr);
        $sav->save();
        $id = $sav->id;
        return $id;
    }

    public function dbInsertLeaveOffDateState($id, $state_id) {
        $arr = array(
            'date_id'  => $id,
            'state_id' => $state_id
        );
        $sav = new \IhrV2\Models\LeaveOffDateState($arr);
        $sav->save();
        return true;
    }

    public function dbInsertGroup($i) {
        $arr = array(
            'name'   => $i['name'],
            'prefix' => $i['prefix']
        );
        $sav = new \IhrV2\Models\Group($arr);
        $sav->save();
        return true;
    }

    public function dbUpdateGroup($i, $id) {
        $upd = \IhrV2\Models\Group::where('id', $id)
            ->update(array(
                'name'   => $i['name'],
                'prefix' => $i['prefix']
            ));
        return true;
    }

    public function dbInsertPosition($i) {
        $arr = array(
            'name'     => $i['name'],
            'salary'   => $i['salary'],
            'group_id' => $i['group_id']
        );
        $sav = new \IhrV2\Models\Position($arr);
        $sav->save();
        return true;
    }

    public function dbUpdatePosition($i, $id) {
        $upd = \IhrV2\Models\Position::where('id', $id)
            ->update(array(
                'group_id' => $i['group_id']
            ));
        return true;
    }

    public function dbCancelOffDay($id) {
        $upd = \IhrV2\Models\LeaveOffDate::where('id', $id)
            ->update(array(
                'updated_by' => auth()->user()->id,
                'status'     => 2 // inactive
            ));
        return true;
    }

    public function dbInsertNewAnno($i) {
        $arr = array(
            'user_id'    => auth()->user()->id,
            'apply_date' => Carbon::now(),
            'message'    => $i['message'],
            'type_id'    => $i['type_id'],
            'status'     => 1,
            'order'      => $i['order']
        );
        $sav = new \IhrV2\Models\Announcement($arr);
        $sav->save();
        return true;
    }

    // update fasting status
    // ---------------------
    public function dbUpdateFasting($id) {
        $upd = \IhrV2\Models\FastingDate::where('id', $id)
            ->update(array(
                'updated_by' => auth()->user()->id,
                'active'     => 2 // inactive
            ));
        return true;
    }

    // insert fasting
    // --------------
    public function dbInsertFasting($data) {
        $arr = array(
            'year'        => $data['year'],
            'start_date'  => $data['start_date'],
            'end_date'    => $data['end_date'],
            'start_time'  => $data['start_time'],
            'end_time'    => $data['end_time'],
            'action_date' => Carbon::now(),
            'action_by'   => auth()->user()->id,
            'active'      => 1
        );
        $sav = new \IhrV2\Models\FastingDate($arr);
        $sav->save();
        return $sav->id;
    }

    // insert fasting state
    // --------------------
    public function dbInsertFastingState($id, $state_id) {
        $arr = array(
            'fasting_date_id' => $id,
            'state_id'        => $state_id,
            'status'          => 1
        );
        $sav = new \IhrV2\Models\FastingDateState($arr);
        $sav->save();
        return true;
    }

    // insert leave off change
    // -----------------------
    public function dbInsertChangeOffDay($data, $uid, $sitecode) {
        $arr = array(
            'user_id'    => $uid,
            'sitecode'   => $sitecode,
            'start_date' => $data['start_date'],
            'end_date'   => $data['end_date'],
            'reason'     => $data['reason'],
            'created_by' => auth()->user()->id,
            'status'     => 1
        );
        $sav = new \IhrV2\Models\LeaveOffChange($arr);
        $sav->save();
        return $sav->id;
    }

    // insert leave off change attachment
    // ----------------------------------
    public function dbInsertOffChangeAttach($id, $uid, $i) {
        $arr = array(
            'user_id'       => $uid,
            'off_change_id' => $id,
            'filename'      => $i['filename'],
            'ext'           => strtolower($i['fileext']),
            'size'          => $i['filesize'],
            'thumb_name'    => $i['filethumb'],
            'status'        => 1
        );
        $sav = new \IhrV2\Models\LeaveOffChangeAttachment($arr);
        $sav->save();
        return true;
    }

    // insert off day date
    // -------------------
    public function dbInsertChangeOffDayDate($i, $date) {
        $arr = array(
            'off_change_id' => $i['id'],
            'type_id'       => $i['type_id'],
            'time_id'       => $i['time_id'],
            'off_date'      => $date,
            'status'        => 1
        );
        $sav = new \IhrV2\Models\LeaveOffChangeDate($arr);
        $sav->save();
        return true;
    }

    // update status off day
    // ---------------------
    public function dbUpdateOffDayStatus($id, $status) {
        return \IhrV2\Models\LeaveOffDate::where('id', $id)
        ->update(array(
            'updated_by' => auth()->user()->id,
            'status'     => $status
        ));
    }

    // delete previous off day states
    // ------------------------------
    public function dbResetOffDayState($id) {
        return \IhrV2\Models\LeaveOffDateState::where('date_id', $id)->delete();
    }

    // insert new off day states
    // -------------------------
    public function dbInsertOffState($date_id, $state_id) {
        $sav = new \IhrV2\Models\LeaveOffDateState(array('date_id' => $date_id, 'state_id' => $state_id));
        $sav->save();
        return true;
    }

    // insert work hour
    // ----------------
    public function dbInsertWorkHour($i, $uid, $sitecode)
    {
        $arr = array(
            'user_id' => $uid,
            'sitecode' => $sitecode,
            'contract_id' => $i['contract_id'],
            'date_from' => $i['date_from'],
            'date_to' => $i['date_to'],
            'time_in' => $i['time_in'],
            'time_out' => $i['time_out'],
            'days' => $i['days'],
            'reason' => $i['reason'],
            'partner' => $i['status_partner'], // 0 = no partner | 1 = have partner
            'created_by' => auth()->user()->id,
            'status' => 1
        );
        $sav = new \IhrV2\Models\WorkHour($arr);
        $sav->save();
        return $sav->id;
    }

    // insert work hour attachment
    // ---------------------------
    public function dbInsertWorkHourAttach($id, $uid, $i)
    {
        $arr = array(
            'work_hour_id'  => $id,            
            'user_id'       => $uid,
            'filename'      => $i['filename'],
            'ext'           => strtolower($i['fileext']),
            'size'          => $i['filesize'],
            'thumb_name'    => $i['filethumb'],
            'status'        => 1
        );
        $sav = new \IhrV2\Models\WorkHourAttachment($arr);
        $sav->save();
        return true;
    }

    // edit work hour
    // --------------
    public function dbUpdateWorkHour($i, $id)
    {
        $val = array(
            'date_from' => $i['start_date'],
            'date_to' => $i['end_date'],
            'time_in' => $i['time_in'],
            'time_out' => $i['time_out'],
            'days' => $i['days'],
            'reason' => $i['reason'],
            'partner' => (!empty($i['partner']) ? 1 : 0), // 0 = no partner | 1 = have partner
            'updated_by' => auth()->user()->id,
        );
        \IhrV2\Models\WorkHour::where('id', $id)->update($val);
        return true;
    }

    // inactive work hour off dates
    // ----------------------------
    public function dbInactiveWorkHourOffDates($id)
    {
        return \IhrV2\Models\WorkHourOffDate::where('work_hour_id', $id)
        ->update(array(
            'status'     => 2
        ));
    }

    // insert work hour off date
    // -------------------------
    public function dbInsertWorkOffDate($id, $date)
    {
        $arr = array(
            'work_hour_id' => $id,
            'off_date' => $date,
            'status' => 1
        );
        $sav = new \IhrV2\Models\WorkHourOffDate($arr);
        $sav->save();
        return true;
    }

    // insert email notification
    // -------------------------
    public function dbInsertEmailNoti($data)
    {
        $arr = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'type_id' => $data['type_id'],
            'status' => 1,
            'created_by' => auth()->user()->id
        );
        $sav = new \IhrV2\Models\EmailNotification($arr);
        $sav->save();
        return $sav->id;
    }

    // activate the calendar
    // ---------------------
    public function dbActivateCalendar($year)
    {
        $arr = array(
            'user_id' => auth()->user()->id,
            'year' => $year,
            'status' => 1
        );
        $sav = new \IhrV2\Models\Calendar($arr);
        $sav->save();
        return true;
    }

}




