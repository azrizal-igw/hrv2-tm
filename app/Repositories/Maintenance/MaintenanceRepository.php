<?php namespace IhrV2\Repositories\Maintenance;

use Carbon\Carbon;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Maintenance\DbMaintenanceInterface;
use IhrV2\Contracts\Maintenance\MaintenanceInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Helpers\CommonHelper;

class MaintenanceRepository implements MaintenanceInterface {
    protected $db_mtn_repo;
    protected $leave_repo;
    protected $user_repo;
    protected $email_repo;
    protected $upload_repo;

    public function __construct(DbMaintenanceInterface $db_mtn_repo, LeaveInterface $leave_repo, UserInterface $user_repo, EmailInterface $email_repo, UploadInterface $upload_repo) {
        $this->db_mtn_repo = $db_mtn_repo;
        $this->leave_repo = $leave_repo;
        $this->user_repo = $user_repo;
        $this->email_repo = $email_repo;
        $this->upload_repo = $upload_repo;
    }

    // excludes states (perlis, penang, kl, labuan, putrajaya)
    // -------------------------------------------------------
    public function getStateExclude() {
        return array(508, 509, 514, 515, 516);
    }

    // get group of states
    // -------------------
    public function getGroupState($group_id) {
        return \IhrV2\Models\State::where('group_calendar_id', $group_id)->pluck('code')->toArray();
    }
    
    // display calendar for site supervisor
    // ------------------------------------
    public function showCalendar($request = null, $sitecode)
    {
        $header = array(
            'parent' => 'Leave Application',
            'child'  => 'Calendar',
            'icon'   => 'calendar',
            'title'  => 'Calendar'
        );

        // no request
        // ----------
        if (empty($request)) {
            if (session()->has('year')) {
                $year = session()->get('year');
            } 
            else {
                $year = date('Y');
            }
        }

        // have request
        // ------------
        else {
            if (!empty($request->year)) {
                session()->put('year', $request->year);
                $year = session()->get('year');
            } 
            else {
                session()->forget('year');
                $year = date('Y');
            }
        }

        // get state id
        // ------------
        $state_id = null;
        $site = $this->leave_repo->getUserStateID($sitecode);
        if (!empty($site)) {
            $state_id = $site->state_id;
        }

        // get state name
        // --------------
        $state = $this->getStateName($state_id);

        // get all off day
        // ---------------
        $data = $this->getListOffDayState($year, $state_id);

        // get list years
        // --------------
        $years = $this->leave_repo->getListYear();

        // get sessions
        // ------------
        $sessions = array('year' => $year);
        return View('calendar.index', compact('header', 'state', 'data', 'years', 'sessions'));        
    }

    // get off/rest day
    // ----------------
    public function getOffDay($request = null) 
    {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Off Day',
            'icon'   => 'close',
            'title'  => 'Off Day'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $year = CommonHelper::checkSession('year');            
            $type_id = CommonHelper::checkSession('type_id');
            $state_id = CommonHelper::checkSession('state_id');
            $date = CommonHelper::checkSession('date');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                $inputs = array('year', 'type_id', 'state_id', 'date');
                session()->forget($inputs);
                return redirect()->route('mod.off-day.index');
            }

            // check session
            // -------------
            $year = CommonHelper::checkSessionPost($request->year, $reset, 'year');
            $type_id = CommonHelper::checkSessionPost($request->type_id, $reset, 'type_id');
            $state_id = CommonHelper::checkSessionPost($request->state_id, $reset, 'state_id');
            $date = CommonHelper::checkSessionPost($request->date, $reset, 'date');
        }

        // get years od drop down
        // ----------------------
        $years = $this->leave_repo->getLeaveYear();

        // get lists of off type
        // ---------------------
        $off_types = $this->getLeaveOffTypes();

        // get states
        // ----------
        $states = $this->user_repo->getStateList();

        // get off day
        // -----------
        $off = $this->getOffDayList($year, $type_id, $state_id, $date);

        // get sessions
        // ------------
        $sessions = array('year' => $year, 'type_id' => $type_id, 'state_id' => $state_id, 'date' => $date);
        return View('modules.off-day.index', compact('header', 'years', 'off_types', 'states', 'off', 'sessions'));
    }

    // show calendar
    // -------------
    public function getCalendar($request = null) {
        $header = array(
            'parent'  => 'Leave Application',
            'child'   => 'Calendar',
            'icon'    => 'calendar',
            'title'   => 'Calendar'
        );
        $state_id = null;

        // check request
        // -------------
        if (empty($request)) {

            // check year session
            // ------------------
            if (session()->has('year')) {
                $year = session()->get('year');
            } else {
                $year = date('Y');
            }
        }

        // have request
        // ------------
        else {

            // check year session
            // ------------------
            if (!empty($request->year)) {
                session()->put('year', $request->year);
                $year = session()->get('year');
            } else {
                session()->forget('year');
                $year = date('Y');
            }

            // activate calendar
            // -----------------
            if (!empty($request->btn_active)) {
                $msg = $this->getCalendarActivate($request->year);
                return redirect()->route('mod.calendar.index')->with([
                    'message' => $msg[1],
                    'label'   => 'alert alert-'.$msg[0].' alert-dismissible'
                ]);
            }
        }

        // get all off day
        // ---------------
        $data = $this->getListOffDayAll($year, $state_id);

        // lists states
        // ------------
        $states = $this->getAllStatesCode();

        // get sessions
        // ------------
        $sessions = array('year' => $year, 'state_id' => $state_id);

        // calendar status
        // ---------------
        $status = $this->leave_repo->getCalendarStatus($year);
        return view('modules.calendar.index', compact('header', 'data', 'states', 'sessions', 'status'));
    }

    // activate the calendar
    // ---------------------
    public function getCalendarActivate($year)
    {
        // check if activated
        // ------------------
        $check = $this->getCheckCalActive($year);

        // not activate yet
        // ----------------
        if (empty($check)) {

            // activate the calendar
            // ---------------------
            $this->db_mtn_repo->dbActivateCalendar($year);
            $msg = array('success', 'Calendar successfully Activated.');
        }

        // already active
        // --------------
        else {
            $msg = array('danger', 'Calendar already Activated.');
        }
        return $msg;
    }

    // check whether calendar is activated or not
    // ------------------------------------------
    public function getCheckCalActive($year)
    {
        return \IhrV2\Models\Calendar::where(['year' => $year, 'status' => 1])->first();
    }

    // lists sites
    // -----------
    public function getSiteIndex($request = null) {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Site',
            'icon'   => 'list',
            'title'  => 'Site'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $state_id = CommonHelper::checkSession('state_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $district = CommonHelper::checkSession('district');
            $status_id = 1;
            $search = CommonHelper::checkSession('search');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('state_id', 'region_id', 'phase_id', 'district', 'status_id', 'search'));
                return redirect()->route('mod.site.index');
            }

            // check session
            // -------------
            $state_id = CommonHelper::checkSessionPost($request->state_id, $reset, 'state_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $district = CommonHelper::checkSessionPost($request->district, $reset, 'district');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $search = CommonHelper::checkSessionPost($request->search, $reset, 'search');
        }

        // query sites
        // -----------
        $arr = array($state_id, $region_id, $phase_id, $district, $status_id, $search);
        $sites = $this->getSiteListIndex($arr);

        // populate drop down data
        // -----------------------
        $states = $this->user_repo->getStateList();
        $regions = $this->user_repo->getRegionList();
        $phases = $this->user_repo->getPhaseList();
        $districts = $this->user_repo->getDistrictNameList();
        $status = $this->user_repo->getStatusList();

        // sessions data
        // -------------
        $sessions = array(
            'state_id'  => $state_id,
            'region_id' => $region_id,
            'phase_id'  => $phase_id,
            'district'  => $district,
            'status_id' => $status_id,
            'search'    => $search
        );
        return View('modules.site.index', compact('header', 'sites', 'states', 'regions', 'phases', 'districts', 'sessions', 'status'));
    }

    // lists public holiday
    // --------------------
    public function getPublicIndex($request = null) {
        $header = array(
            'parent'  => 'Maintenance',
            'child'   => 'Public Holiday',
            'icon'    => 'list',
            'title'   => 'Public Holiday'
        );

        // check request
        // -------------
        if (empty($request)) {
            $year = date('Y');
        }

        // have request
        // ------------
        else {
            $year = CommonHelper::checkSessionPost($request->year, 0, 'year');
        }

        // query public holiday
        // --------------------
        $ph = $this->getLeavePublicByYear($year);

        // get lists of month
        // ------------------
        $months = \IhrV2\Models\Month::get()->toArray();

        // store search sessions
        // ---------------------
        $sessions = array('year' => $year);
        return View('modules.public-holiday.index', compact('header', 'ph', 'months', 'sessions'));
    }

    // get leave public by year
    // ------------------------
    public function getLeavePublicByYear($year) {
        $q = \IhrV2\Models\LeavePublic::with(array('LeavePublicState' =>
            function ($h) {
                $h->with('StateName');
                $h->where('status', 1);
            }))
        ->with(array('StatusName'))
        ->whereYear('date', '=', $year)
        ->where('status', 1)
        ->orderBy('date', 'ASC')
        ->get();
        return $q;
    }

    // get leave public 
    // ----------------
    public function getLeavePublicOne($id) {
        $q = \IhrV2\Models\LeavePublic::where(
            [
                'id'     => $id,
                'status' => 1
            ])
        ->with(array('LeavePublicState' =>
            function ($h) {
                $h->with('StateName');
                $h->where('status', 1);
            }))
        ->first()
        ->toArray();
        return $q;
    }

    // get leave public by id
    // ----------------------
    public function getLeavePublicByID($id) {
        $q = \IhrV2\Models\LeavePublic::find($id);
        return $q;
    }

    // cancel public holiday
    // ---------------------
    public function getPublicHolidayCancel($id) {

        // check if already cancel
        // -----------------------
        $public = $this->getLeavePublicOne($id);

        // have record and still active
        // ----------------------------
        if (!empty($public) && $public['status'] == 1) {

            // cancel record
            // -------------
            $this->db_mtn_repo->dbCancelPublicHoliday($id);

            // send email notification
            // -----------------------
            $arr_body = array(
                'public' => $public,
                'updated_by' => auth()->user()->name
            );
            $this->email_repo->getEmailCancelPublic($arr_body);

            // return success
            // --------------
            $msg = array('Public Holiday Successfully Canceled.', 'success');            
        }

        // already cancel
        // --------------
        else {
            $msg = array('Public Holiday already Canceled.', 'danger');
        }
        return $msg;
    }

    // edit public holiday
    // -------------------
    public function getPublicHolidayEdit($i, $id) {

        // get year & date
        // ---------------
        $year = Carbon::createFromFormat('d/m/Y', $i['date'])->format('Y');
        $date = Carbon::createFromFormat('d/m/Y', $i['date'])->format('Y-m-d');

        // update public
        // -------------
        $arr = array('year' => $year, 'date' => $date, 'desc' => $i['desc']);
        $this->db_mtn_repo->dbUpdatePublicHoliday($arr, $id);

        // reset public states (update all status to 0)
        // --------------------------------------------
        $this->db_mtn_repo->dbResetPublicState($id);

        // insert public states
        // --------------------
        if (count($i['state_id']) > 0) {
            foreach ($i['state_id'] as $key => $code) {

                // check if already exist
                // ----------------------
                $check = $this->getChkPublicState($id, $code);

                // insert new public state
                // -----------------------
                if (empty($check)) {
                    $arr['code'] = $code;
                    $this->db_mtn_repo->dbPublicStateInsert($arr, $id);
                }
            }
        }

        // send email notification
        // -----------------------
        $arr_body = array(
            'public' => $this->getLeavePublicOne($id),
            'updated_by' => auth()->user()->name
        );
        $this->email_repo->getEmailUpdatePublic($arr_body);

        // return message
        // --------------
        return array('Public Holiday Successfully Updated!.', 'success');
    }

    // check public holiday state
    // --------------------------
    public function getChkPublicState($id, $code) {
        return \IhrV2\Models\LeavePublicState::where(['leave_public_id' => $id, 'state_id' => $code, 'status' => 1])->first();
    }

    // create public holiday
    // ---------------------
    public function getPublicHolidayCreate($i) {

        // get year & date
        // ---------------
        $year = Carbon::createFromFormat('d/m/Y', $i['date'])->format('Y');
        $date = Carbon::createFromFormat('d/m/Y', $i['date'])->format('Y-m-d');
        $arr = array('year' => $year, 'date' => $date, 'desc' => $i['desc']);

        // save public
        // -----------
        $public_id = $this->db_mtn_repo->dbInsertPublicHoliday($arr);

        // save public states
        // ------------------
        if (count($i['state_id']) > 0) {
            foreach ($i['state_id'] as $key => $value) {
                $data = array(
                    'public_id' => $public_id,
                    'year' => $year,
                    'date' => $date,
                    'state_id' => $value
                );
                $this->db_mtn_repo->dbInsertPublicHolidayState($data);                
            }
        }

        // send email notification
        // -----------------------
        $arr_body = array(
            'public' => $this->getLeavePublicOne($public_id)
        );
        $this->email_repo->getEmailCreatePublic($arr_body);

        // return message
        // --------------
        return array('Public Holiday Successfully Added!', 'success');
    }

    // update region
    // -------------
    public function getUpdateRegion($i, $id) {

        // check user region
        // -----------------
        $check = \IhrV2\Models\RegionUser::where('region_id', $id)
        ->where('user_id', $i['report_to'])
        ->where('status', 1)
        ->first();

        // new record (choose other rm)
        // ----------------------------
        if (empty($check)) {

            // update user region
            // ------------------
            $this->db_mtn_repo->dbUpdateRegionUser($id);

            // insert user region
            // ------------------
            $this->db_mtn_repo->dbInsertRegionUser($i, $id);
        }
        return true;
    }

    // add new off day
    // ---------------
    public function getAddOffDay($i) {

        // initialize variables
        // --------------------
        $insert = 0;
        $wrong = 0;
        $invalid = 0;

        // get start and end date
        // ----------------------
        $start = Carbon::createFromFormat('d/m/Y', $i['off_date_start'])->format('Y-m-d');
        $end = Carbon::createFromFormat('d/m/Y', $i['off_date_end'])->format('Y-m-d');

        // check if date not within year
        // -----------------------------
        if (Carbon::createFromFormat('d/m/Y', $i['off_date_start'])->format('Y') != $i['off_year'] || Carbon::createFromFormat('d/m/Y', $i['off_date_end'])->format('Y') != $i['off_year']) {
            $wrong = 1;
        }

        // check if date from is less than or equal date to
        // ------------------------------------------------
        if (Carbon::parse($start)->gt(Carbon::parse($end))) {
            $invalid = 1;
        }

        // selected dates is valid
        // -----------------------
        if ($wrong == 0 && $invalid == 0) {

            // check date range
            // ----------------
            $duration = $this->leave_repo->DateRange($start, $end);

            // loop apply dates
            // ----------------
            $ids = array();
            foreach ($duration as $date) {

                // insert leave off dates
                // ----------------------
                $id = $this->db_mtn_repo->dbInsertLeaveOffDate($i, $date);
                $insert++;
                $ids[] = $id;

                // check selected state
                // --------------------
                if (count($i['off_state']) > 0) {
                    foreach ($i['off_state'] as $s) {

                        // insert leave off date states
                        // ----------------------------
                        $this->db_mtn_repo->dbInsertLeaveOffDateState($id, $s);
                    }
                }
            }

            // check if have insert
            // --------------------
            if ($insert > 0) {

                // get off day
                // -----------
                $off = $this->getOffDayByArray($ids);

                // get type name
                // -------------
                $type = $this->getOffDayTypeByID($i['off_type']);

                // get state name
                // --------------
                $states = $this->getStateByArray($i['off_state']);

                // set email body
                // --------------
                $arr_body = array(
                    'year'   => $i['off_year'],
                    'type'   => $type,
                    'states' => $states,
                    'off'    => $off
                );

                // send email notification
                // -----------------------
                $this->email_repo->getEmailCreateOffDay($arr_body);
            }

            // return success message
            // ----------------------
            $msg = array('Off Day Successfully Added.', 'success');
        }

        // check error
        // -----------
        if ($wrong == 1) {
            $msg = array('Selected Year of Date is Invalid.', 'danger');
        }
        if ($invalid == 1) {
            $msg = array('Selected Date is Invalid.', 'danger');
        }

        // return message
        // --------------
        return $msg;
    }

    // cancel off day
    // --------------
    public function getOffDayCancel($id) {

        // cancel the record
        // -----------------
        $this->db_mtn_repo->dbUpdateOffDayStatus($id, 2);

        // send email notification
        // -----------------------
        $arr_body = array(
            'off' => $this->getOffDayByID($id)->toArray()
        );
        $this->email_repo->getEmailCancelOffDay($arr_body);

        // return message
        // --------------
        return array('Off Day successfully Canceled!', 'success');
    }

    // get state lists by array
    // ------------------------
    public function getStateByArray($codes) {
        $q = \IhrV2\Models\State::whereIn('code', $codes)->get();
        return $q;
    }

    // get off type by id
    // ------------------
    public function getOffDayTypeByID($id) {
        return \IhrV2\Models\LeaveOffType::find($id);
    }

    // get off day by set of array id
    // ------------------------------
    public function getOffDayByArray($ids) {
        $q = \IhrV2\Models\LeaveOffDate::whereIn('id', $ids)
        ->where('status', 1)
        ->with(array('ListStates'))
        ->get();
        return $q;
    }

    // get of day by id
    // ----------------
    public function getOffDayByID($id) {
        $q = \IhrV2\Models\LeaveOffDate::where('id', $id)
        ->with(array('OffTypeName'))
        ->with(array('ListStates' => function ($s) {
            $s->with('LeaveOffStateName');
        }))        
        ->first();
        return $q;
    }

    // check off day
    // -------------
    public function getChkOffDay($i, $date) {
        $q = \IhrV2\Models\LeaveOffDate::where([
            'year'     => $i['off_year'],
            'group_id' => $i['off_group'],
            'type_id'  => $i['off_type'],
            'status'   => 1
        ])
        ->whereDate('off_date', '=', $date)
        ->count();
        return $q;
    }

    // validate date
    // -------------
    public function validateDate($date) {
        $d = Carbon::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    public function DaysInMonth($m, $y) {
        $numDays = cal_days_in_month(CAL_GREGORIAN, $m, $y);
        return $numDays;
    }

    public function getLeaveOffGroups() {
        return \IhrV2\Models\LeaveOffGroup::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[State Group]', '');
    }

    public function getLeaveOffTypes() {
        return \IhrV2\Models\LeaveOffType::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Type]', '');
    }

    public function getLeaveOffYears() {
        $year = array('' => '[Year]') + array_combine(range(date('Y'), 2008), range(date('Y'), 2008));
        return $year;
    }

    public function getAllStates() {
        return \IhrV2\Models\State::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[State]', '');
    }

    public function getAllStatesCode() {
        $excludes = $this->getStateExclude();
        return \IhrV2\Models\State::whereNotIn('code', $excludes)
        ->orderBy('id', 'ASC')
        ->pluck('name', 'code')
        ->prepend('[State]', '');
    }

    public function getAllGroup() {
        return \IhrV2\Models\Group::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Group]', '');
    }

    // get all sites
    // -------------
    public function getSiteListIndex($arr) {

        // get value from array
        // --------------------
        $state_id = $arr[0];
        $region_id = $arr[1];
        $phase_id = $arr[2];
        $district = $arr[3];
        $status_id = $arr[4];
        $search = $arr[5];

        // search sites
        // ------------
        $q = \IhrV2\Models\Site::with(array('RegionName', 'StateName'))
        ->where(function ($s) use ($state_id) {
            if (!empty($state_id)) {
                $s->where('state_id', $state_id);
            }
        })
        ->where(function ($r) use ($region_id) {
            if (!empty($region_id)) {
                $r->where('region_id', $region_id);
            }
        })
        ->where(function ($p) use ($phase_id) {
            if (!empty($phase_id)) {
                $p->where('phase_id', $phase_id);
            }
        })
        ->where(function ($d) use ($district) {
            if (!empty($district)) {
                $d->where('district', $district);
            }
        })
        ->where(function ($st) use ($status_id) {
            if (!empty($status_id)) {
                $st->where('status', $status_id);
            }
        })
        ->where(function ($q) use ($search) {
            if (!empty($search)) {
                $q->where('code', 'like', "%" . $search . "%");
                $q->orWhere('name', 'like', "%" . $search . "%");
            }
        })
        ->ofSort(array(
            'code'   => 'asc',
            'status' => 'asc'
        ))
            // ->orderBy('code', 'ASC')
        ->paginate(20);
        return $q;
    }

    // get lists of leave off
    // ----------------------
    public function getLeaveOffGroup() {
        return \IhrV2\Models\LeaveOffGroup::get();
    }

    public function getOffGroupWithState() {
        $q = \IhrV2\Models\LeaveOffGroup::with(array('LeaveOffStates' => function ($h) {
            $h->with('StateName');
        }))
        ->get()
        ->toArray();
        return $q;
    }

    // get lists of states
    // -------------------
    public function getLeaveOffState($group_id) {
        $q = \IhrV2\Models\LeaveOffState::where('group_id', $group_id)
        ->with(array('StateName'))
        ->get()
        ->toArray();
        return $q;
    }

    // get all states
    // exclude:
    // ----------------------------------------------
    public function getStateAll() {
        $q = \IhrV2\Models\State::whereNotIn('code', $this->getStateExclude())
        ->get()
        ->toArray();
        return $q;
    }

    // get state name
    // --------------
    public function getStateName($code) {
        $q = \IhrV2\Models\State::where('code', $code)->first();
        return $q;
    }

    public function getOffGroupID($id) {
        return \IhrV2\Models\LeaveOffGroup::find($id);
    }

    // display lists of off day by state
    // ---------------------------------
    public function getOffDateState($year, $state_id) {
        $q = \IhrV2\Models\LeaveOffDate::whereHas('ListStates', function ($i) use ($state_id) {
            $i->where('state_id', $state_id);
        })
        ->where([
            'year'   => $year,
            'status' => 1
        ])
        ->orderBy('off_date', 'asc')
        ->get()
        ->toArray();
        return $q;
    }

    // get leave public states by public id
    // ------------------------------------
    public function getLeavePublicState($public_id) {
        $q = \IhrV2\Models\LeavePublicState::where(['leave_public_id' => $public_id, 'status' => 1])->get();
        return $q;
    }

    // display lists public holiday by state
    // ---------------------------------
    public function getPublicHolidayState($year, $state_id) {
        $q = \IhrV2\Models\LeavePublic::whereHas('LeavePublicState', function ($i) use ($state_id) {
            $i->where('state_id', $state_id);
        })
        ->where([
            'year'   => $year,
            'status' => 1
        ])
        ->whereYear('date', '=', $year)
        ->with(array('LeavePublicState'))
        ->get()
        ->toArray();
        return $q;
    }

    // display public holiday by site
    // ------------------------------
    public function getPublicHolidaySite($date, $state_id) {

        // get year
        // --------
        $year = Carbon::createFromFormat('Y-m-d', $date)->format('Y');

        // get public
        // ----------
        $q = \IhrV2\Models\LeavePublic::whereHas('LeavePublicState',
            function ($i) use ($state_id) {
                $i->where('state_id', $state_id);
            })
        ->where([
            'year'   => $year,
            'status' => 1
        ])
        ->whereYear('date', '=', $year)
        ->whereDate('date', '=', $date)
        ->with(array('LeavePublicState'))
        ->get();
        // ->toArray();
        return $q;
    }

    // display lists of off day dates
    // ------------------------------
    public function getOffDateList($year) {
        $q = \IhrV2\Models\LeaveOffDate::with(array('ListStates', 'OffTypeName'))
        ->where([
            'year'   => $year,
            'status' => 1
        ])
        ->orderBy('off_date', 'asc')
        ->get()
        ->toArray();
        return $q;
    }

    // display lists of off day
    // ------------------------
    public function getOffDayList($year, $type_id, $state_id, $date) 
    {
        $q = \IhrV2\Models\LeaveOffDate::with(array('OffTypeName', 'ActionBy'))
        ->with(array('ListStates' => function ($s) {
            $s->with('LeaveOffStateName');
        })) 

        ->whereHas('ListStates', function ($i) use ($state_id) {
            if (!empty($state_id)) {
                $i->where('state_id', $state_id);
            }
        })

        ->where(function ($y) use ($year) {
            if (!empty($year)) {
                $y->where('year', $year);
            }
        }) 
        ->where(function ($t) use ($type_id) {
            if (!empty($type_id)) {
                $t->where('type_id', $type_id);
            }
        })
        ->where(function ($d) use ($date) {
            if (!empty($date)) {
                $d->where('off_date', $date);
            }
        }) 

        ->where([
            'status' => 1
        ])
        ->orderBy('id', 'desc')
        ->paginate(20);
        return $q;
    }

    // get lists of public holiday
    // ---------------------------
    public function getPublicDateList($year) {
        $q = \IhrV2\Models\LeavePublic::where([
            'year'   => $year,
            'status' => 1
        ])
        ->whereYear('date', '=', $year)
        ->with(array('LeavePublicState'))
        ->get()
        ->toArray();
        return $q;
    }

    // get all days of each month
    // --------------------------
    public function getDateAllMonth($year) {
        $lists = $this->getMonthList();
        foreach ($lists as $month) {
            if ($month->no == '01') {
                $jan[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '02') {
                $feb[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '03') {
                $mar[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '04') {
                $apr[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '05') {
                $may[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '06') {
                $jun[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '07') {
                $jul[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '08') {
                $aug[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '09') {
                $sep[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '10') {
                $oct[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '11') {
                $nov[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
            if ($month->no == '12') {
                $dec[] = $this->leave_repo->getAllDatesInMonth($year . '-' . $month->no);
            }
        }
        $months = array(
            '1'  => $jan,
            '2'  => $feb,
            '3'  => $mar,
            '4'  => $apr,
            '5'  => $may,
            '6'  => $jun,
            '7'  => $jul,
            '8'  => $aug,
            '9'  => $sep,
            '10' => $oct,
            '11' => $nov,
            '12' => $dec
        );
        return $months;
    }

    // get all off day
    // ---------------
    public function getListOffDayAll($year, $state_id) {

        // get all days of each month
        // --------------------------
        $months = $this->getDateAllMonth($year);

        // get all day of off day
        // ----------------------
        $all_dates = $this->getOffDateList($year);

        // get all day of public holiday
        // -----------------------------
        $all_public = $this->getPublicDateList($year);

        // check if public holiday is on the dates
        // ---------------------------------------
        $public1 = array();
        $public2 = array();
        if (count($all_public) > 0) {
            foreach ($all_public as $p) {

                if (!empty($p['leave_public_state'])) {
                    foreach ($p['leave_public_state'] as $s) {

                        // group 1
                        // -------
                        if (in_array($s['state_id'], $this->getGroupState(1))) {
                            $public1[] = $p['date'];
                        }

                        // group 2
                        // -------
                        if (in_array($s['state_id'], $this->getGroupState(2))) {
                            $public2[] = $p['date'];
                        }
                    }
                }
            }
        }

        // initialize array
        // ----------------
        $od_m1 = array();
        $od_am1 = array();
        $odr_m1 = array();
        $odr_am1 = array();
        $odr1 = array();
        $oe1 = array();
        $og1 = array();
        $dates1 = array();
        $od_m2 = array();
        $od_am2 = array();
        $odr_m2 = array();
        $odr_am2 = array();
        $odr2 = array();
        $oe2 = array();
        $og2 = array();
        $dates2 = array();

        // check if off day is on the dates
        // --------------------------------
        foreach ($all_dates as $date) {

            // group 1
            // -------
            if ($date['group_id'] == 1) {
                $dates1[] = $date['off_date'];
                if ($date['type_id'] == 2) {
                    $od_m1[] = $date['off_date'];
                }
                if ($date['type_id'] == 3) {
                    $od_am1[] = $date['off_date'];
                }
                if ($date['type_id'] == 4) {
                    $odr_m1[] = $date['off_date'];
                }
                if ($date['type_id'] == 5) {
                    $odr_am1[] = $date['off_date'];
                }
                if ($date['type_id'] == 6) {
                    $odr1[] = $date['off_date'];
                }
                if ($date['type_id'] == 8) {
                    $oe1[] = $date['off_date'];
                }
                if ($date['type_id'] == 9) {
                    $og1[] = $date['off_date'];
                }
            }

            // group 2
            // -------
            else {
                $dates2[] = $date['off_date'];
                if ($date['type_id'] == 2) {
                    $od_m2[] = $date['off_date'];
                }
                if ($date['type_id'] == 3) {
                    $od_am2[] = $date['off_date'];
                }
                if ($date['type_id'] == 4) {
                    $odr_m2[] = $date['off_date'];
                }
                if ($date['type_id'] == 5) {
                    $odr_am2[] = $date['off_date'];
                }
                if ($date['type_id'] == 6) {
                    $odr2[] = $date['off_date'];
                }
                if ($date['type_id'] == 8) {
                    $oe2[] = $date['off_date'];
                }
                if ($date['type_id'] == 9) {
                    $og2[] = $date['off_date'];
                }
            }
        }

        // get group of off day
        // --------------------
        $groups = $this->getOffGroupWithState();

        // return array
        // ------------
        $data = array(
            'year'    => $year,
            'months'  => $months,
            'groups'  => $groups,
            'dates1'  => $dates1,
            'od_m1'   => $od_m1,
            'od_am1'  => $od_am1,
            'odr_m1'  => $odr_m1,
            'odr_am1' => $odr_am1,
            'odr1'    => $odr1,
            'oe1'     => $oe1,
            'og1'     => $og1,
            'dates2'  => $dates2,
            'od_m2'   => $od_m2,
            'od_am2'  => $od_am2,
            'odr_m2'  => $odr_m2,
            'odr_am2' => $odr_am2,
            'odr2'    => $odr2,
            'oe2'     => $oe2,
            'og2'     => $og2,
            'public1' => $public1,
            'public2' => $public2,
            'types'   => $this->getOffTypeList()->toArray()
        );
        return $data;
    }

    // get all off day by state (site supervisor)
    // ------------------------------------------
    public function getListOffDayState($year, $state_id) {

        // get all days of each month
        // --------------------------
        $months = $this->getDateAllMonth($year);

        // get all day of off day (by session state)
        // -----------------------------------------
        $all_dates = $this->getOffDateState($year, $state_id);

        // get all day of public holiday
        // -----------------------------
        $all_public = $this->getPublicHolidayState($year, $state_id);

        // check if public holiday is on the dates
        // ---------------------------------------
        $public = array();
        if (count($all_public) > 0) {
            foreach ($all_public as $p) {
                $public[] = $p['date'];
            }
        }

        // initialize array
        // ----------------
        $dates = array();
        $od_m = array();
        $od_am = array();
        $odr_m = array();
        $odr_am = array();
        $odr = array();
        $oe = array();
        $og = array();

        // check if off day is on the dates
        // --------------------------------
        foreach ($all_dates as $date) {

            $dates[] = $date['off_date'];
            if ($date['type_id'] == 2) {
                $od_m[] = $date['off_date'];
            }
            if ($date['type_id'] == 3) {
                $od_am[] = $date['off_date'];
            }
            if ($date['type_id'] == 4) {
                $odr_m[] = $date['off_date'];
            }
            if ($date['type_id'] == 5) {
                $odr_am[] = $date['off_date'];
            }
            if ($date['type_id'] == 6) {
                $odr[] = $date['off_date'];
            }
            if ($date['type_id'] == 8) {
                $oe[] = $date['off_date'];
            }
            if ($date['type_id'] == 9) {
                $og[] = $date['off_date'];
            }
        }

        // get state name
        // --------------
        $state = $this->getStateName($state_id);

        // return array
        // ------------
        return array(
            'name'   => $state->name,
            'months' => $months,
            'dates'  => $dates,
            'od_m'   => $od_m,
            'od_am'  => $od_am,
            'odr_m'  => $odr_m,
            'odr_am' => $odr_am,
            'odr'    => $odr,
            'oe'     => $oe,
            'og'     => $og,
            'public' => $public,
            'types'  => $this->getOffTypeList()->toArray()
        );
    }

    public function getOffTypeList() {
        return \IhrV2\Models\LeaveOffType::get();
    }

    public function getMonthList() {
        return \IhrV2\Models\Month::get();
    }

    // display off day of selected site
    // --------------------------------
    public function getOffDaySite($date, $state_id) {
        // get year
        // --------
        $year = Carbon::createFromFormat('Y-m-d', $date)->format('Y');

        // get off day
        // -----------
        $q = \IhrV2\Models\LeaveOffDate::whereHas('ListStates', function ($i) use ($state_id) {
            $i->where('state_id', $state_id);
        })
        ->whereDate('off_date', '=', $date)
        ->whereYear('off_date', '=', $year)
        ->where([
            'year'   => $year,
            'status' => 1
        ])
        ->with(array('OffTypeName'))
        ->with(array('ListStates' => function ($d) {
            $d->with(array('LeaveOffStateName'));
        }))
        ->get();
        return $q;
    }

    // display detail of off day
    // -------------------------
    public function getOffDayDetail($group_id, $date) {
        // get year
        // --------
        $year = Carbon::createFromFormat('Y-m-d', $date)->format('Y');

        // get off day
        // -----------
        $q = \IhrV2\Models\LeaveOffDate::whereDate('off_date', '=', $date)
        ->whereYear('off_date', '=', $year)
        ->where([
            'year'     => $year,
            'group_id' => $group_id,
            'status'   => 1
        ])
        ->with(array('OffTypeName'))
        ->with(array('ListStates' => function ($d) {
            $d->with(array('LeaveOffStateName'));
        }))
        ->get();
        return $q;
    }

    // display detail of public holiday
    // --------------------------------
    public function getPublicDetail($date) 
    {
        // get year
        // --------
        $year = Carbon::createFromFormat('Y-m-d', $date)->format('Y');

        // get public
        // ----------
        $q = \IhrV2\Models\LeavePublic::whereDate('date', '=', $date)
        ->whereYear('date', '=', $year)
        ->where([
            'year'   => $year,
            'status' => 1
        ])
        ->with(array('LeavePublicState'))
        ->orderBy('date', 'asc')
        ->get();
        return $q;
    }

    // get all announcement
    // --------------------
    public function getAnnouncementAll() {
        $q = \IhrV2\Models\Announcement::orderBy('id', 'desc')
        ->with(array('CreatedBy'))
        ->paginate(20);
        return $q;
    }

    // get announcement by id
    // ----------------------
    public function getAnnoByID($id) {
        $q = \IhrV2\Models\Announcement::find($id);
        return $q;
    }

    // process new announcement
    // ------------------------
    public function getProcessNewAnno($i) 
    {
        // save announcement
        // -----------------
        $save = $this->db_mtn_repo->dbInsertNewAnno($i);

        // return message
        // --------------
        return array('Announcement successfully Added', 'success');
    }

    // get site by id
    // --------------
    public function getSiteInfo($id) {
        $q = \IhrV2\Models\Site::where('id', $id)
        ->with(array('PhaseName', 'RegionName', 'StateName'))
        ->first();
        return $q;
    }

    // cancel off day/public holiday
    // -----------------------------
    public function getCancelOffDay($request) 
    {
        dd($request);

        // initialize variables
        // --------------------
        $off = 0;
        $public = 0;

        // check if off day
        // ----------------
        if (!empty($request->off_id)) {
            $off = 1;

            // cancel off day
            // --------------
            $this->db_mtn_repo->dbCancelOffDay($request->off_id);
        }

        // check if public holiday
        // -----------------------
        if (!empty($request->public_id)) {
            $public = 1;

            // cancel public holiday
            // ---------------------
            $this->db_mtn_repo->dbCancelPublicHoliday($request->public_id);
        }
        return true;
    }

    // get lists of fasting
    // --------------------
    public function getFastingList($year) {
        $q = \IhrV2\Models\FastingDate::where('active', 1)
        ->where(function ($s) use ($year) {
            if (!empty($year)) {
                $s->where('year', $year);
            }
        })
        ->with(array('ListStates' => function ($d) {
            $d->with(array('StateName'));
        }))
        ->orderBy('id', 'desc')
        ->get();
        return $q;
    }

    // get fasting info by id
    // ----------------------
    public function getFastingByID($id) {
        $q = \IhrV2\Models\FastingDate::where('id', $id)
        ->with(array('ListStates' => function ($d) {
            $d->with(array('StateName'));
        }))
        ->first();
        return $q;
    }

    // cancel fasting
    // --------------
    public function getFastingCancel($id) {
        $cancel = $this->db_mtn_repo->dbUpdateFasting($id);
        return array('Fasting Info is Canceled.', 'success');
    }

    // save new fasting
    // ----------------
    public function getFastingCreate($data) 
    {
        // initialize variables
        // --------------------
        $wrong = 0;
        $invalid = 0;

        // check if date not within year
        // -----------------------------
        if (Carbon::createFromFormat('Y-m-d', $data['start_date'])->format('Y') != $data['year'] || Carbon::createFromFormat('Y-m-d', $data['end_date'])->format('Y') != $data['year']) {
            $wrong = 1;
        }

        // check if date from is less than or equal date to
        // ------------------------------------------------
        if (Carbon::parse($data['start_date'])->gt(Carbon::parse($data['end_date']))) {
            $invalid = 1;
        }

        // selected dates is valid
        // -----------------------
        if ($wrong == 0 && $invalid == 0) {

            // save fasting
            // ------------
            $id = $this->db_mtn_repo->dbInsertFasting($data);

            // done save fasting
            // -----------------
            if ($id) {

                // check states
                // ------------
                if (count($data['state']) > 0) {
                    foreach ($data['state'] as $y) {

                        // save states
                        // -----------
                        $this->db_mtn_repo->dbInsertFastingState($id, $y);
                    }
                }
            }

            // set email content
            // -----------------
            $arr_body = array(
                'data'       => $data,
                'created_by' => auth()->user()->name
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailFastingCreate($arr_body);

            // return success
            // --------------
            return array('Fasting successfully Added.', 'success');
        }

        // invalid date
        // ------------
        else {
            $arr = array();
            if ($wrong == 1) {
                $arr = array('Selected Date and Year is not same.', 'danger');
            }
            if ($invalid == 1) {
                $arr = array('Selected Date is Invalid.', 'danger');
            }
            return $arr;
        }
    }

    // check fasting year
    // ------------------
    public function getFastingYear($year) {
        $q = \IhrV2\Models\FastingDate::where([
            'year'   => $year,
            'active' => 1
        ])
        ->first();
        return $q;
    }

    // get lists of day in week
    // ------------------------
    public function getDaysofWeek() {
        $arr = array(
            '' => '[Day]',
            1  => 'Monday',
            2  => 'Tuesday',
            3  => 'Wednesday',
            4  => 'Thursday',
            5  => 'Friday',
            6  => 'Saturday',
            7  => 'Sunday'
        );
        return $arr;
    }

    // get lists of time leave (full/half day)
    // ---------------------------------------
    public function getTimeLeave() {
        return \IhrV2\Models\LeaveTimeType::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Time]', '');
    }

    // get change off day
    // ------------------
    public function getChangeOffByUser($uid, $sitecode) {
        $q = \IhrV2\Models\LeaveOffChange::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'status'   => 1
        ])
        ->with(array('LeaveAllOffDate' => function ($h) {
            $h->with('OffTypeName', 'TimeTypeName');
        }))
        ->with(array('CreatedByDetail'))
        ->first();
        return $q;
    }

    // get change off day all
    // ----------------------
    public function getChangeOffByUserAll($uid, $sitecode) {
        $q = \IhrV2\Models\LeaveOffChange::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode
        ])
        ->with(array('LeaveAllOffDate' => function ($h) {
            $h->with('OffTypeName', 'TimeTypeName');
        }))
        ->with(array('CreatedByDetail'))
        ->get();
        return $q;
    }

    // get leave change off day by id
    // ------------------------------
    public function getLeaveOffChangeByID($id) {
        return \IhrV2\Models\LeaveOffChange::find($id);
    }

    // check change off day
    // --------------------
    public function getCheckChangeOffDay($uid, $sitecode) {
        $q = \IhrV2\Models\LeaveOffChange::where([
            'user_id'  => $uid,
            'sitecode' => $sitecode,
            'status'   => 1
        ])
        ->first();
        return $q;
    }

    // save change off day
    // -------------------
    public function getCreateChangeOffDay($data, $uid, $sitecode) 
    {
        // check if already exist
        // ----------------------
        $check = $this->getCheckChangeOffDay($uid, $sitecode);

        // no record yet
        // -------------
        if (empty($check)) {

            // add record
            // ----------
            $id = $this->db_mtn_repo->dbInsertChangeOffDay($data, $uid, $sitecode);

            // check if have attachment
            // ------------------------
            if (!empty($data['file'])) {

                // upload file attachment
                // ----------------------
                $attach = $this->upload_repo->uploadLeaveOffChange($data['file'], $id, $uid);
            }

            // get staff info
            // --------------
            $user = $this->user_repo->getUserByIDSitecode($uid, $sitecode);

            // get rm info
            // -----------
            $rm = $this->user_repo->getRegionManager($user->sitecode);

            // have rm record
            // --------------
            if (!empty($rm)) {

                // setup email title
                // -----------------
                $arr = array(
                    'mgr_name' => $user->name,
                    'sitecode' => $user->sitecode,
                    'rm_email' => $rm->RegionName->RegionManager->RegionManagerDetail->email,
                    'rm_name'  => $rm->RegionName->RegionManager->RegionManagerDetail->name,
                    'attach'   => $attach
                );

                // get change info
                // ---------------
                $change = $this->getLeaveOffChangeByID($id);

                // get site info
                // -------------
                $site = $change->SiteDetail;

                // setup email body
                // ----------------
                $arr_body = array(
                    'mgr_name'   => $user->name,
                    'site_name'  => $site->name,
                    'site_code'  => $site->code,
                    'position'   => $user->UserLatestJob->PositionName->name,
                    'change'     => $change,
                    'created_by' => $change->CreatedByDetail->name
                );

                // send email notification
                // -----------------------
                $this->email_repo->getEmailLeaveOffChange($arr_body, $arr);
            }

            // message success
            // ---------------
            $arr = array('Record successfully Added.', 'success');
        }

        // record already exist
        // --------------------
        else {

            // message failed
            // --------------
            $arr = array('Record already exist', 'danger');
        }
        return $arr;
    }

    // insert date of change off day
    // -----------------------------
    public function getSaveChangeOffDate($data, $uid, $sitecode) 
    {
        // get change info
        // ---------------
        $change = $this->getLeaveOffChangeByID($data['id']);

        // get all dates
        // -------------
        $dates = $this->leave_repo->getDateBetweenDates($change->start_date, $change->end_date, $data['day']);

        // have dates
        // ----------
        if (!empty($dates)) {
            foreach ($dates as $date) {

                // insert off day date
                // -------------------
                $this->db_mtn_repo->dbInsertChangeOffDayDate($data, $date);
            }
        }

        // send email notification
        // -----------------------

        // return message
        // --------------
        return array('Record successfully Added.', 'success');
    }

    // get lists of change off day dates
    // ---------------------------------
    public function getChangeOffDayMonth($id) 
    {
        // get current year
        // ----------------
        $year = date('Y');

        // get all days of each month
        // --------------------------
        $months = $this->getDateAllMonth($year);

        // get all day of off day
        // ----------------------
        $all_dates = $this->getChangeOffDayDate($id);

        // initialize array
        // ----------------
        $dates = array();
        $od_m = array();
        $od_am = array();
        $odr_m = array();
        $odr_am = array();
        $odr = array();
        $oe = array();

        // check if off day is on the dates
        // --------------------------------
        foreach ($all_dates as $date) {

            $dates[] = $date['off_date'];
            if ($date['type_id'] == 2) {
                $od_m[] = $date['off_date'];
            }
            if ($date['type_id'] == 3) {
                $od_am[] = $date['off_date'];
            }
            if ($date['type_id'] == 4) {
                $odr_m[] = $date['off_date'];
            }
            if ($date['type_id'] == 5) {
                $odr_am[] = $date['off_date'];
            }
            if ($date['type_id'] == 6) {
                $odr[] = $date['off_date'];
            }
            if ($date['type_id'] == 8) {
                $oe[] = $date['off_date'];
            }
        }

        // return array
        // ------------
        $data = array(
            'months' => $months,
            'dates'  => $dates,
            'od_m'   => $od_m,
            'od_am'  => $od_am,
            'odr_m'  => $odr_m,
            'odr_am' => $odr_am,
            'odr'    => $odr,
            'oe'     => $oe
        );
        return $data;
    }

    // lists of date
    // -------------
    public function getChangeOffDayDate($id) {
        $q = \IhrV2\Models\LeaveOffChangeDate::where([
            'off_change_id' => $id,
            'status'        => 1
        ])
        ->get();
        return $q;
    }

    // get leave off day by id
    // -----------------------
    public function getLeaveOffDayByID($id) {
        return \IhrV2\Models\LeaveOffDate::find($id);
    }

    // get off day states by off id
    // ----------------------------
    public function getOffDayState($off_id) {
        $q = \IhrV2\Models\LeaveOffDateState::where(['date_id' => $off_id])->get();
        return $q;
    }

    // edit off day
    // ------------
    public function getOffDayEdit($i, $id) 
    {
        // delete off day states
        // ---------------------
        $this->db_mtn_repo->dbResetOffDayState($id);

        // insert public states
        // --------------------
        if (count($i['state_id']) > 0) {
            foreach ($i['state_id'] as $key => $code) {

                // check if already exist
                // ----------------------
                $check = $this->getChkOffState($id, $code);

                // insert new public state
                // -----------------------
                if (empty($check)) {
                    $this->db_mtn_repo->dbInsertOffState($id, $code);
                }
            }
        }
        
        // send email notification
        // -----------------------
        $arr_body = array(
            'off' => $this->getOffDayByID($id)->toArray(),
            'updated_by' => auth()->user()->name
        );
        $this->email_repo->getEmailUpdateOffDay($arr_body);

        // return message
        // --------------
        return array('Off Day Successfully Updated!', 'success');
    }

    // check off day state
    // -------------------
    public function getChkOffState($id, $state_id) {
        return \IhrV2\Models\LeaveOffDateState::where(['date_id' => $id, 'state_id' => $state_id])->first();
    }

    // get lists of announcement
    // -------------------------
    public function getAnnoIndex($request = null) 
    {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Announcement',
            'icon'   => 'list',
            'title'  => 'Announcement'
        );

        // check request
        // -------------
        if (empty($request)) {

            // check session
            // -------------
            $type_id = CommonHelper::checkSession('type_id');
            $status_id = CommonHelper::checkSession('status_id');
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // reset searching
            // ---------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                session()->forget(array('type_id', 'status_id', 'keyword'));
                return redirect()->route('mod.announcement.index');
            }

            // check session
            // -------------
            $type_id = CommonHelper::checkSessionPost($request->type_id, $reset, 'type_id');
            $status_id = CommonHelper::checkSessionPost($request->status_id, $reset, 'status_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
        }

        $types = array('' => '[Group]', 1 => 'Site', 2 => 'Admin');
        $status = array('' => '[Status]', 1 => 'Active', 2 => 'inactive');
        $sessions = array('type_id' => $type_id, 'status_id' => $status_id, 'keyword' => $keyword);
        $anno = $this->getAnnouncementAll();        
        return View('modules.announcement.index', compact('header', 'anno', 'types', 'status', 'sessions'));        
    }

    // get day lists
    // -------------
    public function getDayDropDown()
    {
        $q = \IhrV2\Models\Day::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Days]', '');
        return $q;
    }

    // get day all
    // -----------
    public function getDayAll()
    {
        return \IhrV2\Models\Day::orderBy('id', 'asc')->get();
    }

    // get day by id
    // -------------
    public function getDayByID($id)
    {
        return \IhrV2\Models\Day::find($id);
    }

    // save work hour
    // --------------
    public function getWorkHourCreate($i, $uid, $sitecode)
    {        
        // check record
        // ------------
        $check = $this->getWorkHourInfo($i['contract_id'], $uid, $sitecode);

        // no record yet
        // -------------
        if (empty($check)) {

            // get contract info
            // -----------------
            $contract = $this->user_repo->getUserContractByID($i['contract_id']);
            $date_from = $contract->date_from;
            $date_to = $contract->date_to;

            // save work hour
            // --------------
            $i['days'] = implode(':', $i['day']);
            $i['date_from'] = $date_from;
            $i['date_to'] = $date_to;

            // check if have partner
            // ---------------------
            $i['status_partner'] = (!empty($i['partner'])) ? 1 : 0;

            // insert work hour
            // ----------------
            $id = $this->db_mtn_repo->dbInsertWorkHour($i, $uid, $sitecode);

            // get all off dates
            // -----------------
            $get_dates = array();
            $days = array();
            foreach ($i['day'] as $day) {

                // get off dates
                // -------------
                $get_dates[] = $this->leave_repo->getDateBetweenDates($date_from, $date_to, $day);

                //  get days name
                // --------------
                $days[] = $this->getDayByID($day)->name;
            }

            // set arrays into a single array
            // ------------------------------
            $dates = array_collapse($get_dates);

            // save all off dates
            // ------------------
            if (!empty($dates)) {
                foreach ($dates as $date) {
                    $this->db_mtn_repo->dbInsertWorkOffDate($id, $date);
                }
            }

            // check if have attachment
            // ------------------------    
            $attach = array();        
            if (!empty($i['file'])) {

                // upload file attachment
                // ----------------------
                $attach = $this->upload_repo->uploadFileWorkHour($i['file'], $id, $uid);
            }

            // get user info
            // -------------
            $user = $this->leave_repo->getCheckUserActive($uid, $sitecode);

            // get rm info
            // -----------
            $chk_rm = $this->user_repo->getRegionBySitecode($sitecode);
            $rm_info = $chk_rm['region_manager_info']['region_manager_detail'];

            // receiver info
            // -------------
            $body = array(
                'mgr_name' => $user['user']['name'],
                'mgr_email' => $user['user']['email'],
                'sitecode' => $sitecode,
                'rm_email' => $rm_info['email'],
                'rm_name'  => $rm_info['name'],
                'attach'   => $attach
            );

            // get work hour
            // -------------
            $work_hour = $this->getWorkHourByID($id);

            // set email content
            // -----------------
            $arr_body = array(
                'user' => $user['user'],
                'contract' => $contract,
                'position' => $user['user']['user_latest_job']['position_name']['name'],
                'site_name' => $user['user']['site_name_info']['code'].' '.$user['user']['site_name_info']['name'],
                'work_hour' => $work_hour,
                'days' => $days,
                'created_by' => $work_hour->UserActionBy->name
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailCreateWorkHour($arr_body, $body);

            // return success message
            // ----------------------
            $arr = array('Work Hour & Off Dates successfully Added.', 'success');            
        }

        // return fail message
        // -------------------
        else {
            $arr = array('Record already Exist.', 'danger');
        }
        return $arr;
    }

    // update work hour
    // ----------------
    public function getWorkHourEdit($data, $id)
    {
        // check work hour
        // ---------------
        $wh = $this->getWorkHourByID($id);

        // record exist
        // ------------
        if (!empty($wh)) {

            // save work hour
            // --------------
            $data['days'] = implode(':', $data['day']);
            $this->db_mtn_repo->dbUpdateWorkHour($data, $id);

            // inactive current off day
            // ------------------------
            $this->db_mtn_repo->dbInactiveWorkHourOffDates($id);

            // insert new off dates
            // --------------------
            $get_dates = array();
            $days = array();
            foreach ($data['day'] as $day) {

                // get off dates
                // -------------
                $get_dates[] = $this->leave_repo->getDateBetweenDates($data['start_date'], $data['end_date'], $day);

                //  get days name
                // --------------
                $days[] = $this->getDayByID($day)->name;
            }

            // set arrays into a single array
            // ------------------------------
            $dates = array_collapse($get_dates);

            // save all off dates
            // ------------------
            if (!empty($dates)) {
                foreach ($dates as $date) {
                    $this->db_mtn_repo->dbInsertWorkOffDate($id, $date);
                }
            }

            // get user info
            // -------------
            $user = $this->leave_repo->getCheckUserActive($wh->user_id, $wh->sitecode);

            // get rm info
            // -----------
            $chk_rm = $this->user_repo->getRegionBySitecode($wh->sitecode);
            $rm_info = $chk_rm['region_manager_info']['region_manager_detail'];

            // receiver info
            // -------------
            $body = array(
                'mgr_name' => $user['user']['name'],
                'mgr_email' => $user['user']['email'],
                'sitecode' => $wh->sitecode,
            );

            // set email content
            // -----------------
            $arr_body = array(
                'user' => $user['user'],
                'position' => $user['user']['user_latest_job']['position_name']['name'],
                'site_name' => $user['user']['site_name_info']['code'].' '.$user['user']['site_name_info']['name'],
                'work_hour' => $wh,
                'days' => $days,
                'created_by' => $wh->UserActionBy->name
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailUpdateWorkHour($arr_body, $body);
            $msg = array('Work Hour successfully Updated.', 'success');  
        }

        // record not exist
        // ----------------
        else {
            $msg = array('Record not found.', 'danger');  
        }
        return $msg;
    }

    // get work hour
    // -------------
    public function getWorkHourInfo($contract_id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\WorkHour::where([
            'contract_id' => $contract_id, 
            'user_id' => $uid, 
            'sitecode' => $sitecode, 
            'status' => 1
        ])
        ->first();
        return $q;
    }

    // get work hour by id
    // -------------------
    public function getWorkHourByID($id)
    {
        return \IhrV2\Models\WorkHour::find($id);
    }

    // get work hour by detail
    // -----------------------
    public function getWorkHourDetail($id, $uid, $sitecode)
    {
        $q = \IhrV2\Models\WorkHour::where(['id' => $id, 'user_id' => $uid, 'sitecode' => $sitecode, 'status' => 1])->first();
        return $q;
    }

    // get work hour by specific staff
    // -------------------------------
    public function getWorkHourByStaff($uid, $sitecode)
    {
        $q = \IhrV2\Models\WorkHour::where([
            'user_id' => $uid, 
            'sitecode' => $sitecode, 
            'status' => 1
        ])
        ->with(array('OffDates', 'ContractInfo', 'TheAttachment'))
        ->get();
        return $q;
    }

    // get day list
    // ------------
    public function getDayList($arr)
    {
        return \IhrV2\Models\Day::whereIn('id', $arr)->get();
    }

    // get work hour one
    // -----------------
    public function getWorkHourOne($uid, $sitecode, $contract_id)
    {
        $q = \IhrV2\Models\WorkHour::where([
            'user_id' => $uid, 
            'sitecode' => $sitecode, 
            'contract_id' => $contract_id,
            'status' => 1
        ])
        ->with(array('OffDates'))
        ->first();
        return $q;
    }

    // get lists of email notifications
    // --------------------------------
    public function getEmailNotiList()
    {
        $q = \IhrV2\Models\EmailNotification::with(array('TypeName'))->orderBy('id', 'asc')->paginate(20);
        return $q;
    }

    // get lists of email notification types
    // -------------------------------------
    public function getListEmailNotiTypes()
    {
        return \IhrV2\Models\EmailType::orderBy('id', 'ASC')->pluck('name', 'id')->prepend('[Email Type]', '');
    }

    // get email notification by id
    // ----------------------------
    public function getEmailNotiByID($id)
    {
        return \IhrV2\Models\EmailNotification::find($id);
    }

    // insert email notification
    // -------------------------
    public function getEmailNotiInsert($data)
    {
        $id = $this->db_mtn_repo->dbInsertEmailNoti($data);
        $email = $this->getEmailNotiOne($id);
        $arr_body = array(
            'data' => $email,
            'created_by' => $email->ActionByUser->name
        );
        $this->email_repo->getEmailInsertEmailNoti($arr_body, $email);
        $msg = array('Email Notification Successfully Added.', 'success');             
    }

    // get email notification
    // ----------------------
    public function getEmailNotiOne($id)
    {
        return \IhrV2\Models\EmailNotification::with(array('ActionByUser'))->find($id);
    }

    // get work hour
    // -------------
    public function getWorkHourList($keyword)
    {
        // dd($keyword);
        $q = \IhrV2\Models\WorkHour::orderBy('id', 'desc')
        ->with(array('StaffInfo', 'SiteInfo'))
        ->where(function ($k) use ($keyword) {
            if (!empty($keyword)) {
                $k->whereHas('StaffInfo', function ($i) use ($keyword) {
                    $i->where('name', 'like', '%' . $keyword . '%');
                    $i->orWhere('icno', 'like', '%' . $keyword . '%');
                    $i->orWhere('username', 'like', '%' . $keyword . '%');
                    $i->orWhere('sitecode', 'like', '%' . $keyword . '%');
                });
            }
        })
        ->paginate(20);
        return $q;
    }

    // get lists work hour
    // -------------------
    public function getWorkHourIndex($request = null)
    {
        $header = array(
            'parent' => 'Maintenance',
            'child'  => 'Work Hour',
            'icon'   => 'hourglass',
            'title'  => 'Work Hour'
        );

        // check request / pagination
        // --------------------------
        if (empty($request)) {
            $keyword = CommonHelper::checkSession('keyword');
        }

        // have request
        // ------------
        else {

            // check if reset searching
            // ------------------------
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                $inputs = array('keyword');
                session()->forget($inputs);
                return redirect()->route('mod.work-hour.index');
            }
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');            
        }
        $sessions = array('keyword' => $keyword);
        $work_hour = $this->getWorkHourList($keyword);                
        return view('modules.work-hour.index', compact('header', 'keyword', 'work_hour'));                
    }
    
}


