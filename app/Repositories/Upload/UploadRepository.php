<?php namespace IhrV2\Repositories\Upload;

use File;
use IhrV2\Contracts\Attendance\DbAttendanceInterface;
use IhrV2\Contracts\Leave\DbLeaveInterface;
use IhrV2\Contracts\Letter\DbLetterInterface;
use IhrV2\Contracts\Maintenance\DbMaintenanceInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\User\DbUserInterface;
use IhrV2\Contracts\Training\DbTrainingInterface;
use IhrV2\Contracts\Api\Attendance\DbApiAttendanceInterface;
use Image;

class UploadRepository implements UploadInterface {
    protected $db_leave_repo;
    protected $db_user_repo;
    protected $db_att_repo;
    protected $db_letter_repo;
    protected $db_mtn_repo;
    protected $db_tt_repo;
    protected $db_api_att;

    public function __construct(DbLeaveInterface $db_leave_repo, DbUserInterface $db_user_repo, DbAttendanceInterface $db_att_repo, DbLetterInterface $db_letter_repo, DbMaintenanceInterface $db_mtn_repo, DbTrainingInterface $db_tt_repo, DbApiAttendanceInterface $db_api_att) {
        $this->db_leave_repo = $db_leave_repo;
        $this->db_user_repo = $db_user_repo;
        $this->db_att_repo = $db_att_repo;
        $this->db_letter_repo = $db_letter_repo;
        $this->db_mtn_repo = $db_mtn_repo;
        $this->db_tt_repo = $db_tt_repo;
        $this->db_api_att = $db_api_att;
    }

    // check folder upload
    // -------------------
    public function getCheckFolder($name, $thumb = null) {

        // get current year & month
        // ------------------------
        $year_month = date('Ym');

        // check folder upload & thumbnail
        // -------------------------------
        if (!empty($thumb)) {
            $path = storage_path('app/files/' . $name . '/thumb/' . $year_month);
        } else {
            $path = storage_path('app/files/' . $name . '/' . $year_month);
        }

        // create folder if not exist
        // --------------------------
        if (!File::exists($path)) {
            File::makeDirectory($path, 0775, true);
        }
        return $path;
    }

    // get file info and upload thumbnail
    // ----------------------------------
    public function getProcessFile($file, $uid, $name) 
    {
        // get file info
        // -------------
        $filename = date('YmdHis') . '_' . $uid . '_' . str_random(20);
        $fileext = strtolower($file->getClientOriginalExtension());
        $filesize = $file->getSize();
        $filenew = $filename . '.' . $fileext;

        // check whether the file is image or not
        // --------------------------------------
        $thumb = 0;
        if (substr($file->getMimeType(), 0, 5) == 'image') {
            $thumb = 1;
        }

        // file is image and resize photo
        // ------------------------------
        if ($thumb == 1) {

            // check folder thumb
            // ------------------
            $path_thumb = $this->getCheckFolder($name, 1);

            // set name of thumb
            // -----------------
            $tname = $filename . '_thumb.' . $fileext;
            $img = Image::make($file)->resize(200, null, function ($x) {
                $x->aspectRatio();
                $x->upsize();
            });
            $img->save($path_thumb . '/' . $tname);
            $filethumb = $filename . '_thumb'; // thumb name without ext
        } 

        // not image
        // ---------
        else {
            $filethumb = '';
        }

        // return file info
        // ----------------
        $data = array(
            'filename'  => $filename,
            'fileext'   => $fileext,
            'filesize'  => $filesize,
            'filethumb' => $filethumb,
            'filenew'   => $filenew
        );
        return $data;
    }

    // upload leave attachment
    // -----------------------
    public function uploadFileLeave($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'leave';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check attachment
        // ----------------
        $check = $this->getCheckLeaveAttach($id, auth()->user()->id);

        // attachment empty
        // ----------------
        if (empty($check)) {

            // insert record into db
            // ---------------------
            $this->db_leave_repo->dbInsertLeaveAttachment($id, $uid, $info);
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.leave', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check leave attachment
    // ----------------------
    public function getCheckLeaveAttach($id, $uid)
    {
        $q = \IhrV2\Models\LeaveAttachment::where(['leave_id' => $id, 'user_id' => $uid, 'status' => 1])->first();
        return $q;
    }

    // upload replacement leave attachment
    // -----------------------------------
    public function uploadFileLeaveRep($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'leave';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check attachment
        // ----------------
        $check = $this->getCheckLeaveRepAttach($id, $uid);

        // record empty
        // ------------
        if (empty($check)) {

            // insert record into db
            // ---------------------
            $this->db_leave_repo->dbInsertLeaveRepAttachment($id, $uid, $info);        
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.leave', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check rl request attachment
    // ---------------------------
    public function getCheckLeaveRepAttach($id, $uid)
    {
        $q = \IhrV2\Models\LeaveRepAttachment::where(['leave_rep_id' => $id, 'user_id' => $uid, 'status' => 1])->first();
        return $q;
    }

    // upload reimburse/deduct leave attachment
    // ----------------------------------------
    public function uploadFileLeaveAdd($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'leave';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check attachment
        // ----------------
        $check = $this->getCheckLeaveAddAttach($id, $uid);

        // attachment empty
        // ----------------
        if (empty($check)) {

            // insert record into db
            // ---------------------
            $this->db_leave_repo->dbInsertLeaveAddAttachment($id, $uid, $info);
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.leave', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check attachment leave reimburse/deduct
    // ---------------------------------------
    public function getCheckLeaveAddAttach($id, $uid)
    {
        $q = \IhrV2\Models\LeaveAddAttachment::where(['leave_add_id' => $id, 'user_id' => $uid, 'status' => 1])->first();
        return $q;
    }

    // upload attendance remark attachment
    // -----------------------------------
    public function uploadAttRemarkFile($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'attendance/remark';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check attachment
        // ----------------
        $check = $this->getCheckAttRemAttach($id, $uid);

        // attachment empty yet
        // --------------------
        if (empty($check)) {

            // insert into db
            // --------------
            $this->db_att_repo->dbInsertAttRemarkAttachment($id, $uid, $info);            
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.attendance.remark', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check attendance remark attackment
    // ----------------------------------
    public function getCheckAttRemAttach($id, $uid)
    {
        $q = \IhrV2\Models\AttendanceRemarkAttachment::where(['att_remark_id' => $id, 'user_id' => $uid, 'status' => 1])->first();
        return $q;
    }

    // update file attendance remark time slip
    // ---------------------------------------
    public function uploadAttRemarkOther($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'attendance/remark';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check attachment time slip
        // --------------------------
        $check = $this->getChkAttRemOthAttach($id, $uid);

        // attachment is empty
        // -------------------
        if (empty($check)) {

            // insert into db
            // --------------
            $this->db_att_repo->dbInsertAttOtherAttachment($id, $uid, $info);            
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.attendance.remark', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check attachment attendance time slip
    // -------------------------------------
    public function getChkAttRemOthAttach($id, $uid)
    {
        $q = \IhrV2\Models\AttendanceRemarkAttachment::where(['att_remark_id' => $id, 'user_id' => $uid, 'status' => 1])->first();
        return $q;
    }

    // upload attendance remark by range
    // ---------------------------------
    public function uploadAttRemarkRange($file, $ids, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'attendance/remark';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // loop attendance remark id
        // -------------------------
        if (!empty($ids)) {
            foreach ($ids as $id) {

                // insert into db
                // --------------
                $this->db_att_repo->dbInsertAttRemarkAttachment($id, $uid, $info);
            }
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.attendance.remark', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // upload attendance manual
    // ------------------------
    public function uploadAttManual($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'attendance/manual';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check attachment attendance manual
        // ----------------------------------
        $check = $this->getCheckAttManAttach($id, $uid);

        // attachment empty
        // ----------------
        if (empty($check)) {

            // insert into db
            // --------------
            $this->db_att_repo->dbInsertAttManualAttachment($id, $uid, $info);
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.attendance.manual', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check attachment attendance manual
    // ----------------------------------
    public function getCheckAttManAttach($id, $uid)
    {
        $q = \IhrV2\Models\AttendanceManualAttachment::where(['att_manual_id' => $id, 'user_id' => $uid, 'status' => 1])->first();
        return $q;
    }

    // upload user photo
    // -----------------
    public function uploadUserPhoto($file, $uid, $sitecode) 
    {
        // get path folder
        // ---------------
        $name = 'user';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check if photo exist
        // --------------------
        $check = $this->getCheckUserPhoto($uid, $sitecode);

        // photo is empty
        // --------------
        if (empty($check)) {

            // insert into db
            // --------------
            $photo_id = $this->db_user_repo->dbInsertUserPhoto($uid, $sitecode, $info);
        }

        // photo already exist
        // -------------------
        else {
            $photo_id = $check->id;
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.user', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType(),
            'id'   => $photo_id
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check user photo
    // ----------------
    public function getCheckUserPhoto($uid, $sitecode)
    {
        $q = \IhrV2\Models\UserPhoto::where(['user_id' => $uid, 'sitecode' => $sitecode, 'active' => 1])->first();
        return $q;
    }

    // upload new letter
    // -----------------
    public function uploadLetterNew($file, $letter_id, $uid, $sitecode) 
    {
        // get path folder
        // ---------------
        $name = 'letter';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // check attachment letter
        // -----------------------
        $check = $this->getCheckLetterAttach($letter_id, $uid);

        // attachment empty
        // ----------------
        if (empty($check)) {

            // insert record into db
            // ---------------------
            $this->db_letter_repo->dbInsertLetterAttach($letter_id, $uid, $info);            
        }

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.letter', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // check attachment letter
    // -----------------------
    public function getCheckLetterAttach($id, $uid)
    {
        $q = \IhrV2\Models\LetterAttachment::where(['letter_id' => $id, 'user_id' => $uid, 'status' => 1])->first();
        return $q;
    }

    // upload leave off change
    // -----------------------
    public function uploadLeaveOffChange($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'leave';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // insert record into db
        // ---------------------
        $this->db_mtn_repo->dbInsertOffChangeAttach($id, $uid, $info);

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.leave', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // upload attachment work hour
    // ---------------------------
    public function uploadFileWorkHour($file, $id, $uid) 
    {
        // get path folder
        // ---------------
        $name = 'misc';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // insert record into db
        // ---------------------
        $this->db_mtn_repo->dbInsertWorkHourAttach($id, $uid, $info);

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.misc', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;
    }

    // upload new training
    // -------------------
    public function uploadNewTraining($file, $id, $uid)
    {
        // get path folder
        // ---------------
        $name = 'training';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // insert record into db
        // ---------------------
        $this->db_tt_repo->dbInsertTrainingAttach($id, $uid, $info);

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.training', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;        
    }

    // upload log file mobile attendance
    // ---------------------------------
    public function uploadLogAttGps($file, $uid, $sitecode)
    {
        // get path folder
        // ---------------
        $name = 'attendance/log';
        $path = $this->getCheckFolder($name);

        // process file and thumbnail
        // --------------------------
        $info = $this->getProcessFile($file, $uid, $name);

        // insert record into db
        // ---------------------
        $this->db_api_att->dbInsertAttGpsLog($info, $uid, $sitecode);

        // set array info
        // --------------
        $arr = array(
            'file' => route('lib.file.att.log', array($info['filenew'])),
            'as'   => $info['filenew'],
            'mime' => $file->getMimeType()
        );

        // upload file
        // -----------
        $upload = $file->move($path, $info['filenew']);

        // return file info
        // ----------------
        return $arr;         
    }


}
