<?php
namespace IhrV2\Repositories\Erp;

use Carbon\Carbon;
use Hash;
use IhrV2\Contracts\Erp\DbErpInterface;
use IhrV2\Helpers\ErpHelper;

class DbErpRepository implements DbErpInterface {

    // insert log return
    // -----------------
    public function dbInsertReturnLogApi($msg) {
        $arr = array(
            'message' => $msg
        );
        $sav = new \IhrV2\Models\LogApi($arr);
        $sav->save();
        return true;
    }

    // insert log catch
    // ----------------
    public function dbInsertCatchLogApi($i, $msg) {
        $arr = array(
            'ref_id'  => $i,
            'message' => $msg
        );
        $sav = new \IhrV2\Models\LogApi($arr);
        $sav->save();
        return true;
    }

    // insert user job
    // ---------------
    public function dbInsertUserJobDrm($i) {
        if (!empty($i['Commercement Date'])) {
            $date_join = Carbon::createFromFormat('m/d/Y', $i['Commercement Date'])->format('Y-m-d');
        } else {
            $date_join = date('Y-m-d');
        }
        $arr = array(
            'user_id'     => $i['user_id'],
            'staff_id'    => strtoupper($i['IDStaff']),
            'join_date'   => $date_join,
            'position_id' => $i['IDPosition'],
            'region_id'   => $i['IDRegion'],
            'notes'       => $i['Notes'],
            'status'      => 1,
            'flag'        => 1
        );
        $sav = new \IhrV2\Models\UserJob($arr);
        $sav->save();
        return true;
    }

    // insert user
    // -----------
    public function dbInsertUser($i) {

        // set values
        // ----------
        $staff_id = strtoupper($i['IDStaff']);

        // check user status
        // -----------------
        if (in_array($i['IDStatusEmp'], array(1, 3))) {
            $status = 1;
        } else {
            $status = $i['IDStatusEmp'];
        }

        // insert users
        // ------------
        $arr = array(
            'username'                => $staff_id, // can't used this column to update because it's protected
            'name'                    => $i['PersonName'],
            'staff_id'                => $staff_id, // staff id when login into system
            'prev_staff_id'           => '',
            'email'                   => $i['WorkEmail'],
            'password'                => Hash::make('password'),
            'api_token'               => str_random(60),
            'group_id'                => $i['group_id'],
            'icno'                    => $i['icno'],
            'permanent_street_1'      => $i['Permanent Street 1'],
            'permanent_street_2'      => $i['Permanent Street 2'],
            'permanent_postcode'      => $i['Permanent Postcode'],
            'permanent_city'          => $i['Permanent City'],
            'permanent_state'         => $i['Permanent State'],
            'correspondence_street_1' => $i['Correspondence Street 1'],
            'correspondence_street_2' => $i['Correspondence Street 2'],
            'correspondence_postcode' => $i['Correspondence Postcode'],
            'correspondence_city'     => $i['Correspondence City'],
            'correspondence_state'    => $i['Correspondence State'],
            'telno1'                  => $i['PhoneNo1'],
            'telno2'                  => $i['PhoneNo2'],
            'hpno'                    => $i['MobileNo'],
            'faxno'                   => $i['Fax No'],
            'website'                 => $i['Website'],
            'personal_email'          => $i['Personal Email'],
            'work_email'              => $i['WorkEmail'],
            'gender_id'               => ErpHelper::GenderName($i['Gender']),
            'marital_id'              => ErpHelper::MaritialStatus($i['MaritialStatus']),
            'dob'                     => ErpHelper::checkDate($i['DOB']),
            'pob'                     => $i['POB'],
            'nationality_id'          => ErpHelper::Nationality($i['Nationality']),
            'race_id'                 => ErpHelper::RaceName($i['Race']),
            'religion_id'             => ErpHelper::ReligionName($i['Religion']),
            'sitecode'                => strtoupper($i['sitecode']),
            'epfno'                   => $i['EPF No'],
            'bankname'                => $i['Bank Registered'],
            'bankno'                  => $i['Bank Acc No'],
            'status'                  => $status,
            'action_date'             => date('Y-m-d H:i:s'),
            'mobile'                  => 1,
            'date_created'            => ErpHelper::checkDateTime($i['DateCreated']),
            'date_modified'           => ErpHelper::checkDateTime($i['DateModified'])
        );
        $sav = new \IhrV2\User($arr);
        $sav->save();
        return $sav->id;
    }

    // insert user history
    // -------------------
    public function dbInsertUserHist($i) {
        $arr = array(
            'username'                => $i->username,
            'name'                    => $i->name,
            'staff_id'                => $i->staff_id,
            'prev_staff_id'           => $i->prev_staff_id,
            'email'                   => $i->email,
            'password'                => $i->password,
            'api_token'               => $i->api_token,
            'group_id'                => $i->group_id,
            'icno'                    => $i->icno,
            'permanent_street_1'      => $i->permanent_street_1,
            'permanent_street_2'      => $i->permanent_street_2,
            'permanent_postcode'      => $i->permanent_postcode,
            'permanent_city'          => $i->permanent_city,
            'permanent_state'         => $i->permanent_state,
            'correspondence_street_1' => $i->correspondence_street_1,
            'correspondence_street_2' => $i->correspondence_street_2,
            'correspondence_postcode' => $i->correspondence_postcode,
            'correspondence_city'     => $i->correspondence_city,
            'correspondence_state'    => $i->correspondence_state,
            'telno1'                  => $i->telno1,
            'telno2'                  => $i->telno1,
            'hpno'                    => $i->hpno,
            'hpno2'                   => $i->hpno2,
            'faxno'                   => $i->faxno,
            'website'                 => $i->website,
            'personal_email'          => $i->personal_email,
            'work_email'              => $i->work_email,
            'gender_id'               => $i->gender_id,
            'marital_id'              => $i->marital_id,
            'dob'                     => $i->dob,
            'pob'                     => $i->pob,
            'nationality_id'          => $i->nationality_id,
            'race_id'                 => $i->race_id,
            'religion_id'             => $i->religion_id,
            'partner_name'            => $i->partner_name,
            'partner_phone'           => $i->partner_phone,
            'child_no'                => $i->child_no,
            'sitecode'                => $i->sitecode,
            'epfno'                   => $i->epfno,
            'bankname'                => $i->bankname,
            'bankno'                  => $i->bankno,
            'status'                  => $i->status,
            'action_date'             => $i->action_date,
            'last_login'              => $i->last_login,
            'date_created'            => $i->date_created,
            'date_modified'           => $i->date_modified,
            'imei'                    => $i->imei,
            'device'                  => $i->device,
            'edit'                    => $i->edit,
            'mobile'                  => $i->mobile
        );
        $sav = new \IhrV2\Models\UserHistory($arr);
        $sav->save();
        return true;
    }

    // update user
    // -----------
    public function dbUpdateUser($i, $icno, $staff_id) {
        $q = \IhrV2\User::where([
            'icno'     => $icno,
            'staff_id' => $staff_id
        ])->update(array(
            'name'                    => $i['Full Name'],
            'email'                   => $i['WorkEmail'],
            'permanent_street_1'      => $i['Permanent Street 1'],
            'permanent_street_2'      => $i['Permanent Street 2'],
            'permanent_postcode'      => $i['Permanent Postcode'],
            'permanent_city'          => $i['Permanent City'],
            'permanent_state'         => $i['Permanent State'],
            // 'correspondence_street_1' => $i['Correspondence Street 1'],
            // 'correspondence_street_2' => $i['Correspondence Street 2'],
            // 'correspondence_postcode' => $i['Correspondence Postcode'],
            // 'correspondence_city'     => $i['Correspondence City'],
            // 'correspondence_state'    => $i['Correspondence State'],
            'telno1'                  => $i['PhoneNo1'],
            'telno2'                  => $i['PhoneNo2'],
            'hpno'                    => $i['MobileNo'],
            'faxno'                   => $i['Fax No'],
            'website'                 => $i['Website'],
            'personal_email'          => $i['Personal Email'],
            'work_email'              => $i['WorkEmail'],
            'gender_id'               => ErpHelper::GenderName($i['Gender']),
            // 'marital_id'              => ErpHelper::MaritialStatus($i['MaritialStatus']),
            'dob'                     => ErpHelper::checkDate($i['DOB']),
            'pob'                     => $i['POB'],
            'nationality_id'          => ErpHelper::Nationality($i['Nationality']),
            'race_id'                 => ErpHelper::RaceName($i['Race']),
            'religion_id'             => ErpHelper::ReligionName($i['Religion']),
            'sitecode'                => strtoupper($i['sitecode']),
            'epfno'                   => $i['EPF No'],
            'bankname'                => $i['Bank Registered'],
            'bankno'                  => $i['Bank Acc No'],
            'status'                  => ErpHelper::StatusEmployment($i['StatusEmployment']),
            'action_date'             => date('Y-m-d H:i:s'),
            'date_created'            => ErpHelper::checkDateTime($i['DateCreated']),
            'date_modified'           => ErpHelper::checkDateTime($i['DateModified'])
        ));
        return true;
    }

    // insert user job
    // ---------------
    public function dbInsertUserJob($i, $status) {

        // get region id
        // -------------
        $region_id = $i['IDRegion'];
        // $region_id = ($i['group_id'] == 4) ? $i['IDRegion'] : 0;

        // get join date
        // -------------
        if (!empty($i['Commercement Date'])) {
            $date_join = ErpHelper::checkDate($i['Commercement Date']);
        } else {
            $date_join = date('Y-m-d');
        }

        // check user job status
        // ---------------------
        if (in_array($status, array(1, 3))) {
            $job_status = 1;
        } else {
            $job_status = $status;
        }
        $arr = array(
            'user_id'       => $i['user_id'],
            'staff_id'      => strtoupper($i['IDStaff']),
            'join_date'     => $date_join,
            'position_id'   => $i['IDPosition'],
            'phase_id'      => $i['Phase::PhaseID'],
            'region_id'     => $region_id,
            'notes'         => $i['Notes'],
            'sitecode'      => strtoupper($i['sitecode']),
            'resign_date'   => ErpHelper::checkDate($i['Date Resign']),
            'status'        => $job_status,
            'flag'          => 1,
            'date_modified' => $i['date_modified']
        );
        $sav = new \IhrV2\Models\UserJob($arr);
        $sav->save();
        return true;
    }

    // update user job status
    // ----------------------
    public function dbUpdateUserJob($i, $id) {
        $job = \IhrV2\Models\UserJob::where('id', $id)
            ->update(array(
                'join_date'     => ErpHelper::checkDate($i['Commercement Date']),
                'position_id'   => $i['IDPosition'],
                'phase_id'      => $i['Phase::PhaseID'],
                'region_id'     => $i['IDRegion'],
                'notes'         => $i['Notes'],
                'date_modified' => $i['date_modified']
            ));
        return true;
    }

    // inactive user job
    // -----------------
    public function dbInactiveUserJob($uid, $staff_id, $sitecode) {
        $job = \IhrV2\Models\UserJob::where([
            'user_id'  => $uid,
            'staff_id' => $staff_id,
            'sitecode' => $sitecode,
            'status'   => 1,
            'flag'     => 1
        ])->update(array(
            'status' => 2,
            'flag'   => 0
        ));
        return true;
    }

    // insert user contract
    // --------------------
    public function dbInsertUserContract($i) {
        $arr = array(
            'user_id'            => $i['user_id'],
            'date_from'          => ErpHelper::checkDate($i['Contract Start']),
            'date_to'            => ErpHelper::checkDate($i['Contract End']),
            'status_contract_id' => ErpHelper::JobStatus($i['LevelStaff']),
            'sitecode'           => $i['sitecode'],
            'status'             => 1,
            'flag'               => 1,
            'date_modified'      => $i['date_modified']
        );
        $sav = new \IhrV2\Models\UserContract($arr);
        $sav->save();
        return true;
    }

    // save updated user contract
    // --------------------------
    public function dbUpdateUserContract($i, $id) {
        $contract = \IhrV2\Models\UserContract::where('id', $id)
            ->update(array(
                'date_from'          => ErpHelper::checkDate($i['Contract Start']),
                'date_to'            => ErpHelper::checkDate($i['Contract End']),
                'status_contract_id' => ErpHelper::JobStatus($i['LevelStaff']),
                'date_modified'      => $i['date_modified']
            ));
        return true;
    }

    // insert user contract (api contract)
    // -----------------------------------
    public function dbInsertUserContractApi($i) {
        $arr = array(
            'user_id'            => $i['user_id'],
            'date_from'          => ErpHelper::checkDate($i['Contract Start']),
            'date_to'            => ErpHelper::checkDate($i['Contract End']),
            'status_contract_id' => 1,
            'sitecode'           => trim(strtoupper($i['SiteCode::SiteCode'])),
            'status'             => 1
        );
        $sav = new \IhrV2\Models\UserContract($arr);
        $sav->save();
        return true;
    }

    // insert user contract (api contract)
    // -----------------------------------
    public function dbInsertUserContractSync($i) {
        $arr = array(
            'user_id'            => $i['user_id'],
            'date_from'          => ErpHelper::checkDate($i['ContractStart']),
            'date_to'            => ErpHelper::checkDate($i['ContractEnd']),
            'status_contract_id' => 1,
            'sitecode'           => trim(strtoupper($i['Sitecode'])),
            'status'             => 1,
            'flag'               => 1,
            'date_modified'      => $i['date_modified']
        );
        $sav = new \IhrV2\Models\UserContract($arr);
        $sav->save();
        return true;
    }

    // insert user education
    // ---------------------
    public function dbInsertUserEducation($i) {
        $arr_qua = array(
            'user_id'        => $i['user_id'],
            'name_education' => $i['Qualitification']
        );
        $sav_qua = new \IhrV2\Models\UserEducation($arr_qua);
        $sav_qua->save();
        return true;
    }

    // insert user emergency contact
    // -----------------------------
    public function dbInsertUserEmergencyContact($i) {
        $arr_emer = array(
            'user_id' => $i['user_id'],
            'name'    => $i['Emergency Contact'],
            'telno'   => $i['Emergency Contact No']
        );
        $sav_emer = new \IhrV2\Models\UserEmergency($arr_emer);
        $sav_emer->save();
        return true;
    }

    // update users sitecode
    // ---------------------
    public function dbUpdateUserSitecode($user_id, $sitecode) {
        $update = \IhrV2\User::where('id', $user_id)
            ->update(array(
                'sitecode' => $sitecode
            ));
        return true;
    }

    // update user with staff id
    // -------------------------
    public function dbUpdateUserStaffID($user_id, $old_staff_id, $new_staff_id, $sitecode) {
        $upd_user = \IhrV2\User::where('id', $user_id)
            ->update(array(
                'prev_staff_id' => $old_staff_id,
                'staff_id'      => $new_staff_id,
                'sitecode'      => $sitecode
            ));
        return true;
    }

    // update staff id
    // ---------------
    public function dbUpdateStaffIDGroupID($icno, $prev_staff_id, $i) {
        $upd_user = \IhrV2\User::where('icno', $icno)->update(array(
            'username'      => strtoupper($i['IDStaff']),
            'prev_staff_id' => $prev_staff_id,
            'staff_id'      => strtoupper($i['IDStaff']),
            'group_id'      => $i['group_id'],
            'sitecode'      => strtoupper($i['sitecode']),
            'status'        => 1,
            'updated_at'    => ErpHelper::checkDateTime($i['DateModified'])
        ));
        return true;
    }

    // update user job status
    // ----------------------
    public function dbUpdateJobInactive($staff_id) {
        $upd_job = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->update(array(
                'status' => 2,
                'flag'   => 0
            ));
        return true;
    }

    // update user job status that is active
    // -------------------------------------
    public function dbUpdateJobActive($staff_id) {
        $save = \IhrV2\Models\UserJob::where([
            'staff_id' => $staff_id,
            'status'   => 1
        ])
            ->update(array(
                'status' => 2
            ));
        return true;
    }

    public function dbUpdateUserJobWithSitecode($staff_id, $sitecode) {
        $upd_job = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->where('sitecode', $sitecode)
            ->update(array(
                'status' => 2,
                'flag'   => 1
            ));
        return true;
    }

    // update position id
    // ------------------
    public function dbUpdateUserJobPosition($position_id, $staff_id) {
        $upd_job = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->update(array(
                'position_id' => $position_id
            ));
        return true;
    }

    // update join date
    // ----------------
    public function dbUpdateUserJobJoinDate($join_date, $staff_id) {
        $date = Carbon::createFromFormat('m/d/Y', $join_date)->format('Y-m-d');
        $upd_job = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->update(array(
                'join_date' => $date
            ));
        return true;
    }

    // update phase id
    // ---------------
    public function dbUpdateUserJobPhase($phase_id, $staff_id) {
        $upd_job = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->update(array(
                'phase_id' => $phase_id
            ));
        return true;
    }

    // update status users to inactive
    // -------------------------------
    public function dbUpdateUserStatus($icno) {
        $upd_user = \IhrV2\User::where('icno', $icno)
            ->where('status', 1)
            ->update(array(
                'status' => 2
            ));
        return true;
    }

    // update status users to inactive by icno and staff id
    // ----------------------------------------------------
    public function dbUpdateUserStatusInactive($icno, $staff_id) {
        $upd_user = \IhrV2\User::where('icno', $icno)
            ->where('staff_id', $staff_id)->where('status', 1)
            ->update(array(
                'status' => 2
            ));
        return true;
    }

    // inactive user job with resign date
    // ----------------------------------
    public function dbInactiveJobResign($staff_id, $sitecode, $resign_date) {
        $q = \IhrV2\Models\UserJob::where([
            'staff_id' => $staff_id,
            'sitecode' => $sitecode,
            'status'   => 1,
            'flag'     => 1
        ])->update(array(
            'status'      => 2,
            'flag'        => 1,
            'resign_date' => $resign_date
        ));
        return true;
    }

    // update status
    // -------------
    public function dbUpdateUserJobStatus($status, $staff_id) {
        $upd_job = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->update(array(
                'status' => $status
            ));
        return true;
    }

    public function dbUpdateUserJobDetail($i, $staff_id) {
        $upd_job = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->update(array(
                'position_id' => $i['IDPosition'],
                'phase_id'    => $i['Phase::PhaseID']
            ));
        return true;
    }

    // update user contract status
    // ---------------------------
    public function dbUpdateContractInactive($user_id) {
        $upd_contract = \IhrV2\Models\UserContract::where('user_id', $user_id)
            ->update(array(
                'status' => 2,
                'flag'   => 0
            ));
        return true;
    }

    // update current contract to inactive
    // -----------------------------------
    public function dbUpdateUserContractStatus($uid) {
        $q = \IhrV2\Models\UserContract::where([
            'user_id' => $uid,
            'status'  => 1
        ])
            ->update(array(
                'status' => 2,
                'flag'   => 0
            ));
        return true;
    }

    // update user contract date modified
    // ----------------------------------
    public function dbUpdateContractModified($i, $contract_id, $date_modified) {
        $upd_contract = \IhrV2\Models\UserContract::where('id', $contract_id)
            ->update(array(
                'date_from'     => ErpHelper::checkDate($i['Contract Start']),
                'date_to'       => ErpHelper::checkDate($i['Contract End']),
                'date_modified' => $date_modified
            ));
        return true;
    }

    // insert site
    // -----------
    public function dbInsertSite($i) {

        // check date
        // ----------
        $date_modified = ErpHelper::checkDate($i['DateModified']);
        $start = ErpHelper::checkDate($i['Start']);
        $end = ErpHelper::checkDate($i['End']);

        // set array
        // ---------
        $arr = array(
            'code'          => $i['Site Code'],
            'name'          => trim($i['Site Name']),
            'full_name'     => $i['Full Site Name'],
            'tm_name'       => $i['Phase Name TM'],
            'both_name'     => $i['Phase Name Both'],
            'old_name'      => $i['Old Sitename'],
            'phase_id'      => $i['PhaseID'],
            'region_id'     => $i['IDRegion'],
            'street1'       => $i['Address 1'],
            'street2'       => $i['Address 2'],
            'postal_code'   => $i['Postcode'],
            'city'          => $i['City'],
            'district'      => $i['District'],
            'state_id'      => $i['StateID'],
            'address_label' => $i['Address Label'],
            'email'         => $i['Email Site'],
            'website'       => $i['Website'],
            'latitude'      => trim($i['Latitude']),
            'longitude'     => trim($i['Longitude']),
            'start'         => ($start == '0000-00-00') ? null : $start,
            'end'           => ($end == '0000-00-00') ? null : $end,
            'bandwidth'     => trim($i['Data Bandwidth']),
            'backhaul'      => trim($i['Backhaul']),
            'status'        => 1,
            'date_modified' => ($date_modified == '0000-00-00') ? null : $date_modified
        );
        $sav = new \IhrV2\Models\Site($arr);
        $sav->save();
        return true;
    }

    // insert site history
    // -------------------
    public function dbInsertSiteHistory($i) {
        // set array
        // ---------
        $arr = array(
            'code'          => $i['code'],
            'name'          => $i['name'],
            'full_name'     => $i['full_name'],
            'tm_name'       => $i['tm_name'],
            'both_name'     => $i['both_name'],
            'old_name'      => $i['old_name'],
            'phase_id'      => $i['phase_id'],
            'region_id'     => $i['region_id'],
            'street1'       => $i['street1'],
            'street2'       => $i['street2'],
            'postal_code'   => $i['postal_code'],
            'city'          => $i['city'],
            'district'      => $i['district'],
            'state_id'      => $i['state_id'],
            'address_label' => $i['address_label'],
            'email'         => $i['email'],
            'website'       => $i['website'],
            'latitude'      => $i['latitude'],
            'longitude'     => $i['longitude'],
            'start'         => $i['start'],
            'end'           => $i['end'],
            'bandwidth'     => $i['bandwidth'],
            'backhaul'      => $i['backhaul'],
            'status'        => 1,
            'date_modified' => $i['date_modified']
        );
        $sav = new \IhrV2\Models\SiteHistory($arr);
        $sav->save();
        return true;
    }

    // update site
    // -----------
    public function dbUpdateSite($i) {
        // check date
        // ----------
        $date_modified = ErpHelper::checkDate($i['DateModified']);
        $start = ErpHelper::checkDate($i['Start']);
        $end = ErpHelper::checkDate($i['End']);

        // update site
        // -----------
        $upd = \IhrV2\Models\Site::where('code', $i['Site Code'])
            ->update(array(
                'name'          => trim($i['Site Name']),
                'full_name'     => $i['Full Site Name'],
                'tm_name'       => $i['Phase Name TM'],
                'both_name'     => $i['Phase Name Both'],
                'old_name'      => $i['Old Sitename'],
                'phase_id'      => $i['PhaseID'],
                'region_id'     => $i['IDRegion'],
                'street1'       => $i['Address 1'],
                'street2'       => $i['Address 2'],
                'postal_code'   => $i['Postcode'],
                'city'          => $i['City'],
                'district'      => $i['District'],
                'state_id'      => $i['StateID'],
                'address_label' => $i['Address Label'],
                'website'       => $i['Website'],
                'email'         => $i['Email Site'],
                'latitude'      => trim($i['Latitude']),
                'longitude'     => trim($i['Longitude']),
                'start'         => ($start == '0000-00-00') ? null : $start,
                'end'           => ($end == '0000-00-00') ? null : $end,
                'bandwidth'     => trim($i['Data Bandwidth']),
                'backhaul'      => trim($i['Backhaul']),
                'status'        => ($i['LiveStatus'] == 1) ? 1 : 2,
                'date_modified' => $date_modified
            ));
        return true;
    }

    // insert public holiday
    // ---------------------
    public function dbInsertPublic($i, $year) {
        $data = array(
            'year'          => $year,
            'desc'          => trim($i['Regarding']),
            'date'          => ErpHelper::checkDate($i['DateCuti']),
            'status'        => 1,
            'date_modified' => ErpHelper::checkDateTime($i['DateModified'])
        );
        $sav = new \IhrV2\Models\LeavePublic($data);
        $sav->save();
        return $sav->id;
    }

    // insert public holiday state
    // ---------------------------
    public function dbInsertPublicState($year, $public_id, $arr) {

        // get values
        // ----------
        $date = $arr[0];
        $state_lists = $arr[1];

        // all states
        // ----------
        if ($state_lists == 0) {

            // get all states with excludes states
            // -----------------------------------
            $states = \IhrV2\Models\State::whereNotIn('erp_id', $this->getExcludeState())->get();
            if (!empty($states)) {
                foreach ($states as $state) {
                    $data = array(
                        'leave_public_id' => $public_id,
                        'year'            => $year,
                        'date'            => ErpHelper::checkDate($date),
                        'state_id'        => $state->code,
                        'status'          => 1
                    );
                    \IhrV2\Models\LeavePublicState::insert($data);
                }                
            }
        }

        // specific states
        // ---------------
        else {

            // separate each state
            // -------------------
            $states = preg_split('/\r\n|\r|\n/', $state_lists);

            // check state
            // -----------
            if (count($states) > 0) {
                foreach ($states as $state) {

                    // get state code
                    // --------------
                    $chk = $this->getStateByErpID($state);

                    // insert public state
                    // -------------------
                    $data = array(
                        'leave_public_id' => $public_id,
                        'year'            => $year,
                        'date'            => ErpHelper::checkDate($date),
                        'state_id'        => $chk->code,
                        'status'          => 1
                    );
                    $sav = new \IhrV2\Models\LeavePublicState($data);
                    $sav->save();
                }                
            }
        }
        return true;
    }

    // exclude state public holiday
    // perlis|pulau pinang|WP kuala lumpur|WP labuan|WP putrajaya
    // ----------------------------------------------------------
    public function getExcludeState() {
        return array(1,13,14,16,15);
    }

    // get state id of erp
    // -------------------
    public function getStateByErpID($id) {
        return \IhrV2\Models\State::where('erp_id', $id)->first();
    }

    // update public holiday
    // ---------------------
    public function dbUpdatePublicHoliday($i, $id) {
        $date_modified = Carbon::createFromFormat('m/d/Y H:i:s', $i['DateModified'])->format('Y-m-d H:i:s');
        $q = \IhrV2\Models\LeavePublic::where('id', $id)
            ->update(array(
                'desc'          => trim($i['Regarding']),
                'date_modified' => $date_modified
            ));
        return true;
    }

    // insert phase
    // ------------
    public function dbInsertPhase($i) {
        $data = array(
            'id'        => $i['PhaseID'],
            'name'      => trim($i['Phase Name']),
            'name_tm'   => trim($i['Phase Name TM']),
            'name_both' => trim($i['Phase Name Both']),
            'status'    => 1
        );
        $sav = new \IhrV2\Models\Phase($data);
        $sav->save();
        return true;
    }

    // update phase
    // ------------
    public function dbUpdatePhase($i, $phase_id) {
        $update = \IhrV2\Models\Phase::where('id', $phase_id)
            ->update(array(
                'name'      => trim($i['Phase Name']),
                'name_tm'   => trim($i['Phase Name TM']),
                'name_both' => trim($i['Phase Name Both'])
            ));
        return true;
    }

    // insert position
    // ---------------
    public function dbInsertPosition($i, $group_id) {
        $arr = array(
            'id'       => $i['ID'],
            'name'     => trim($i['Position']),
            'salary'   => '0.00',
            'group_id' => $group_id
        );
        $save = new \IhrV2\Models\Position($arr);
        $save->save();
        return true;
    }

    // update position
    // ---------------
    public function dbUpdatePosition($i, $position_id) {
        $update = \IhrV2\Models\Position::where('id', $position_id)
            ->update(array(
                'name' => trim($i['Position'])
            ));
        return true;
    }

    // insert state
    // ------------
    public function dbInsertState($i) {
        $data = array(
            'id'     => $i['ID'],
            'name'   => trim($i['Name']),
            'erp_id' => $i['IDState']
        );
        $sav = new \IhrV2\Models\State($data);
        $sav->save();
        return true;
    }

    // update state
    // ------------
    public function dbUpdateState($i, $code) {
        $update = \IhrV2\Models\State::where('code', $code)
            ->update(array(
                'name'   => trim($i['Name']),
                'erp_id' => $i['IDState']
            ));
        return true;
    }

    // insert region
    // -------------
    public function dbInsertRegion($i) {
        $data = array(
            'id'       => $i['ID REGION'],
            'name_eng' => trim($i['Region Name'])
        );
        $sav = new \IhrV2\Models\Region($data);
        $sav->save();
        return true;
    }

    // update region
    // -------------
    public function dbUpdateRegion($i, $region_id) {
        $update = \IhrV2\Models\Region::where('id', $region_id)
            ->update(array(
                'name_eng' => trim($i['Region Name'])
            ));
        return true;
    }

    // update resign date at user job
    // ------------------------------
    public function dbUpdateUserResignDate($job_id, $resign_date) {
        $update = \IhrV2\Models\UserJob::where('id', $job_id)
            ->update(array(
                'resign_date' => $resign_date
            ));
        return true;
    }

}
