<?php
namespace IhrV2\Repositories\Erp;

use Carbon\Carbon;
use GuzzleHttp\Client;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Erp\DbErpInterface;
use IhrV2\Contracts\Erp\ErpInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Helpers\ErpHelper;

class ErpRepository implements ErpInterface
{
    protected $db_erp;
    protected $user_repo;
    protected $email_repo;
    protected $client;

    public function __construct(DbErpInterface $db_erp, UserInterface $user_repo, EmailInterface $email_repo, Client $client)
    {
        $this->db_erp = $db_erp;
        $this->user_repo = $user_repo;
        $this->email_repo = $email_repo;
        $this->client = $client;
    }

    // api domain
    // ----------
    public function getDomainErp()
    {
        return env('ERP_DOMAIN');
    }

    // api staff active (all status)
    // -----------------------------
    public function getApiStaffActive($record, $skip)
    {
        $api = "RESTfm/contacts/layout/API_Staff.json?RFMsF1=Location&RFMsV1=site&RFMsF2=CompanyCode&RFMsV2=MSDDI&RFMsF3=Department&RFMsV3=Project&RFMkey=qwertyuio234567sdfgh&RFMmax=$record&RFMskip=$skip";
        return $this->getDomainErp() . $api;
        // return asset('assets/files/API_Staff.json');
    }

    // api staff inactive
    // ------------------
    public function getApiStaffInactive($record, $skip)
    {
        $api = "RESTfm/contacts/layout/API_Staff.json?RFMsF1=site&RFMsV1=site&RFMsF2=CompanyCode&RFMsV2=MSDDI&RFMsF3=Department&RFMsV3=Project&RFMsF4=IDStatusEMP&RFMsV4=2&RFMkey=qwertyuio234567sdfgh&RFMmax=$record&RFMskip=$skip";
        return $this->getDomainErp() . $api;
    }

    // api staff contract
    // ------------------
    public function getApiStaffContract($record, $skip)
    {
        $api = "RESTfm/contacts/layout/API_Contract.json?RFMsF1=Location&RFMsV1=Site&RFMsF2=IDStatusEmp&RFMsV2=1&RFMsF3=Status&RFMsV3=Current&RFMkey=qwertyuio234567sdfgh&RFMmax=$record&RFMskip=$skip";
        // return asset('assets/files/API_Contract.json');
        return $this->getDomainErp() . $api;
    }

    // api staff hr
    // ------------
    public function getApiStaffHR()
    {
        $api = "RESTfm/contacts/layout/API_StaffInfo.json?RFMsF1=IDStatusEMP&RFMsV1=1&RFMsF2=CompanyCode&RFMsV2=MSDDI&RFMsF3=SubDepartment&RFMsV3=Human&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api staff rm
    // ------------
    public function getApiStaffRM()
    {
        $api = "RESTfm/contacts/layout/API_Staff.json?RFMsF1=Location&RFMsV1=branch&RFMsF3=CompanyCode&RFMsV3=MSDDI&RFMsF4=IDPosition&RFMsV4=1&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api staff drm
    // -------------
    public function getApiStaffDRM()
    {
        $api = "RESTfm/contacts/layout/API_Staff.json?RFMsF1=Location&RFMsV1=branch&RFMsF3=CompanyCode&RFMsV3=MSDDI&RFMsF4=IDPosition&RFMsV4=2&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api staff clerk
    // ---------------
    public function getApiStaffClerk()
    {
        $api = "RESTfm/contacts/layout/API_StaffInfo.json?RFMsF1=IDStatusEMP&RFMsV1=1&RFMsF2=CompanyCode&RFMsV2=MSDDI&RFMsF3=JobTitle&RFMsV3=Clerk&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api staff project operation
    // ---------------------------
    public function getApiStaffProject()
    {
        $api = "RESTfm/contacts/layout/API_StaffInfo.json?RFMsF1=IDStatusEMP&RFMsV1=1&RFMsF2=CompanyCode&RFMsV2=MSDDI&RFMsF3=Department&RFMsV3=Project%20Operation&RFMsF4=Location&RFMsV4=HQ&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;        
    }

    // api public holiday
    // ------------------
    public function getApiPublicHoliday($year)
    {
        $api = "RESTfm/Cuti/layout/Public_Holiday.json?RFMsF1=DateCuti&RFMsV1=$year&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api site
    // --------
    public function getApiSite()
    {
        $api = "RESTfm/MSD_PROJECT/layout/API_Project.json?&RFMkey=qwertyuio234567sdfgh&RFMskip=0&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api phase
    // ---------
    public function getApiPhase()
    {
        $api = "RESTfm/MSD_PROJECT/layout/API_Phase.json?&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api position
    // ------------
    public function getApiPosition()
    {
        $api = "RESTfm/contacts/layout/API_Position.json?&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api state
    // ---------
    public function getApiState()
    {
        $api = "RESTfm/MSD_PROJECT/layout/API_State.json?&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // api region
    // ----------
    public function getApiRegion()
    {
        $api = "RESTfm/MSD_PROJECT/layout/API_Region.json?&RFMkey=qwertyuio234567sdfgh&RFMmax=0";
        return $this->getDomainErp() . $api;
    }

    // sync staff active
    // -----------------
    public function getSyncStaffActive($record, $skip)
    {
        // initialize variable
        // -------------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $fail = 0;

        // user & history
        // --------------
        $insert_user = 0;
        $update_user = 0;
        $inactive_user = 0;
        $insert_history = 0;

        // user job
        // --------
        $insert_job = 0;
        $update_job = 0;
        $inactive_job = 0;
        $update_resign = 0;

        // user contract
        // -------------
        $insert_contract = 0;
        $update_contract = 0;
        $inactive_contract = 0;

        // get api path
        // ------------
        $api = $this->getApiStaffActive($record, $skip);

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // record exist
        // ------------
        if (!empty($json['data'])) {

            // loop record
            // -----------
            foreach ($json['data'] as $data) {

                // trim white space
                // ----------------
                $i = array_map('trim', $data);

                // ensure the data is not empty and active/transfer
                // ------------------------------------------------
                if (!empty($i['IDStaff']) &&
                    !empty($i['NRIC']) &&
                    !empty($i['PersonName']) &&
                    !empty($i['SiteCode::SiteCode'])) {

                    // get total record
                    // ----------------
                    $total++;

                    // get sitecode
                    // ------------
                    $sitecode = $i['SiteCode::SiteCode'];

                    // get icno
                    // --------
                    $icno = str_replace("-", "", $i['NRIC']);

                    // get staff id
                    // ------------
                    $staff_id = strtoupper($i['IDStaff']);

                    // get date modified erp
                    // ---------------------
                    $date_modified = Carbon::createFromFormat('m/d/Y H:i:s', $i['DateModified'])->format('Y-m-d H:i:s');

                    // set values into array
                    // ---------------------
                    $i['group_id'] = 3;
                    $i['icno'] = $icno;
                    $i['sitecode'] = $sitecode;
                    $i['date_modified'] = $date_modified;

                    // check staff active
                    // ------------------
                    if ($i['IDStatusEmp'] == 1) {

                        // check icno and staff id (users & user_jobs)
                        // have cases same ic multiple staff id both active
                        // ------------------------------------------------
                        $user = $this->getUserByICNStaffID($icno, $staff_id);

                        // user is empty
                        // -------------
                        if (empty($user)) {

                            // check icno if already exist (match with icno erp)
                            // -------------------------------------------------
                            $chk_icno = $this->getUserICActive($icno);

                            // check the other icno
                            // --------------------
                            if (!empty($chk_icno)) {

                                // have the other icno & need to inactive this one
                                // -----------------------------------------------
                                $this->db_erp->dbUpdateUserStatus($chk_icno->icno);
                                $inactive_user++;

                                // check user job
                                // --------------
                                $job = $this->getUserJobByIC($chk_icno->staff_id);
                                if (!empty($job)) {

                                    // inactive user_jobs
                                    // ------------------
                                    $this->db_erp->dbUpdateJobActive($chk_icno->staff_id);
                                    $update_job++;
                                }
                            }

                            // insert users
                            // ------------
                            $i['user_id'] = $this->db_erp->dbInsertUser($i);
                            $insert_user++;

                            // insert user_jobs
                            // ----------------
                            $this->db_erp->dbInsertUserJob($i, 1);
                            $insert_job++;

                            // // insert user_contracts
                            // // ---------------------
                            // if (!empty($i['Contract Start']) && !empty($i['Contract End'])) {
                            //     $this->db_erp->dbInsertUserContract($i);
                            //     $insert_contract++;
                            // }

                            // insert user_educations
                            // ----------------------
                            if (!empty($i['Qualitification'])) {
                                $this->db_erp->dbInsertUserEducation($i);
                            }

                            // insert user_emergency_contacts
                            // ------------------------------
                            if (!empty($i['Emergency Contact'])) {
                                $this->db_erp->dbInsertUserEmergencyContact($i);
                            }
                        }

                        // icno and staff_id exist
                        // -----------------------
                        else {

                            // set user id
                            // -----------
                            $i['user_id'] = $user->id;

                            // date modified is not empty
                            // --------------------------
                            if (!empty($i['DateModified'])) {

                                // check if date modified is different
                                // -----------------------------------
                                if (Carbon::parse($user->date_modified)->ne(Carbon::parse($date_modified))) {

                                    // insert user history
                                    // -------------------
                                    $this->db_erp->dbInsertUserHist($user);
                                    $insert_history++;

                                    // update users
                                    // ------------
                                    $this->db_erp->dbUpdateUser($i, $user->icno, $user->staff_id);
                                    $update_user++;

                                    // check user jobs
                                    // ---------------
                                    if (!empty($user->UserLatestJob)) {

                                        // get user job
                                        // ------------
                                        $job = $user->UserLatestJob;

                                        // check if date modified job is different
                                        // ---------------------------------------
                                        if (Carbon::parse($job->date_modified)->ne(Carbon::parse($date_modified))) {

                                            // check value job
                                            // ---------------
                                            $check_job = $this->getSyncCheckValueJob($i, $job, $sitecode);

                                            // check if different and have value
                                            // ---------------------------------
                                            if ($check_job == 1) {

                                                // inactive current user job
                                                // -------------------------
                                                $this->db_erp->dbInactiveUserJob($job->user_id, $job->staff_id, $job->sitecode);
                                                $inactive_job++;

                                                // insert new user job
                                                // -------------------
                                                $this->db_erp->dbInsertUserJob($i, 1);
                                                $insert_job++;
                                            }
                                        }
                                    }

                                    // no user job
                                    // -----------
                                    else {

                                        // insert new job
                                        // --------------
                                        $this->db_erp->dbInsertUserJob($i, 1);
                                        $insert_job++;
                                    }

                                    // // check user contracts
                                    // // --------------------
                                    // if (!empty($user->UserLatestContract)) {

                                    //     // get user contract
                                    //     // -----------------
                                    //     $contract = $user->UserLatestContract;

                                    //     // check if date modified is different
                                    //     // -----------------------------------
                                    //     if (Carbon::parse($contract->date_modified)->ne(Carbon::parse($date_modified))) {

                                    //         // get contract date api
                                    //         // ---------------------
                                    //         $start_date = $i['Contract Start'];
                                    //         $end_date = $i['Contract End'];

                                    //         // check value contract
                                    //         // --------------------
                                    //         $check_contract = $this->getSyncCheckValueContract($start_date, $end_date, $contract);

                                    //         // ensure date contract has value and different
                                    //         // --------------------------------------------
                                    //         if ($check_contract == 1) {

                                    //             // inactive current contract
                                    //             // -------------------------
                                    //             $this->db_erp->dbUpdateContractInactive($user->staff_id);
                                    //             $inactive_contract++;

                                    //             // insert new contract
                                    //             // -------------------
                                    //             $this->db_erp->dbInsertUserContract($i, 1);
                                    //             $insert_contract++;
                                    //         }
                                    //     }
                                    // }

                                    // // no contract
                                    // // -----------
                                    // else {

                                    //     // insert new contract
                                    //     // -------------------
                                    //     $this->db_erp->dbInsertUserContract($i, 1);
                                    //     $insert_contract++;
                                    // }
                                }

                                // date modified not different
                                // ---------------------------
                                else {

                                    // check user jobs
                                    // ---------------
                                    if (!empty($user->UserLatestJob)) {

                                        // get user job
                                        // ------------
                                        $job = $user->UserLatestJob;

                                        // check if join date|position|phase|region is empty
                                        // -------------------------------------------------
                                        if (empty($job->join_date) ||
                                            $job->position_id == 0 ||
                                            $job->phase_id == 0 ||
                                            $job->region_id == 0) {

                                            // update user job
                                            // ---------------
                                            $this->db_erp->dbUpdateUserJob($i, $job->id);
                                            $update_job++;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // check staff inactive
                    // --------------------
                    else {

                        // check users that active at IHR
                        // ------------------------------
                        $user = $this->getUserByICNStaffID($icno, $staff_id);

                        // user and job is active
                        // ----------------------
                        if (!empty($user)) {

                            // users status is active
                            // ----------------------
                            if (!empty($user->UserLatestJob)) {

                                // update users status to inactive
                                // -------------------------------
                                $this->db_erp->dbUpdateUserStatusInactive($user->icno, $user->staff_id);
                                $inactive_user++;

                                // get resign date
                                // ---------------
                                $resign_date = ($i['Date Resign']) ? ErpHelper::checkDate($i['Date Resign']) : null;

                                // update user_jobs status to 2
                                // ----------------------------
                                $this->db_erp->dbInactiveJobResign($user->staff_id, $user->sitecode, $resign_date);
                                $inactive_job++;
                            }

                            // users status is inactive
                            // ------------------------
                            else {

                                // check if resign date already set
                                // --------------------------------
                                $resign = $this->getUserJobResignDate($user->id, $user->staff_id);

                                // inactive and resign is not empty yet
                                // ------------------------------------
                                if (!empty($resign) && $resign->resign_date == '0000-00-00' || $resign->resign_date == null) {

                                    // check resign date erp
                                    // ---------------------
                                    if (!empty($i['Date Resign'])) {

                                        // update resign date
                                        // ------------------
                                        $resign_date = ErpHelper::checkDate($i['Date Resign']);
                                        $this->db_erp->dbUpdateUserResignDate($resign->id, $resign_date);
                                        $update_resign++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // no record
        // ---------
        else {
            $fail = 'Fail Read API';
        }

        // return result
        // -------------
        $data = array(
            'start' => $start,
            'total' => $total,
            'skip' => $skip,
            'fail' => $fail,

            // user status
            // -----------
            'insert_user' => $insert_user,
            'update_user' => $update_user,
            'inactive_user' => $inactive_user,
            'insert_history' => $insert_history,

            // job status
            // ----------
            'insert_job' => $insert_job,
            'update_job' => $update_job,
            'inactive_job' => $inactive_job,
            'update_resign' => $update_resign,

            // contract status
            // ---------------
            'insert_contract' => $insert_contract,
            'update_contract' => $update_contract,
            'inactive_contract' => $inactive_contract,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncStaffActive($data);
        return $data;
    }

    // check if resign is not null
    // ---------------------------
    public function getUserJobResignDate($user_id, $staff_id)
    {
        $q = \IhrV2\Models\UserJob::where([
            'user_id' => $user_id,
            'staff_id' => $staff_id,
            'status' => 2,
            'flag' => 1
        ])
        ->first();
        return $q;
    }

    // check value contract
    // --------------------
    public function getSyncCheckValueContract($start, $end, $contract)
    {
        // initial status
        // --------------
        $status = 0;

        // ensure value is not empty
        // -------------------------
        if (!empty($start) && !empty($end)) {

            // date contract have different
            // ----------------------------
            if ($start != $contract->start_date || $end != $contract->end_date) {
                $status = 1;
            }
        }
        return $status;
    }

    // check value job
    // ---------------
    public function getSyncCheckValueJob($i, $job, $sitecode)
    {
        // status pass
        // -----------
        $status_join = 0;
        $status_position = 0;
        $status_phase = 0;
        $status_region = 0;
        $status_sitecode = 0;
        $status_pass = 0;

        // check join date
        // ---------------
        $join_date = ($i['Commercement Date']) ? ErpHelper::checkDate($i['Commercement Date']) : null;
        if (!empty($join_date) && $job->join_date != $join_date) {
            $status_join = 1;
        }

        // check position value
        // --------------------
        $position_id = ($i['IDPosition']) ? $i['IDPosition'] : null;
        if (!empty($position_id) && $job->position_id != $position_id) {
            $status_position = 1;
        }

        // check phase value
        // -----------------
        $phase_id = ($i['Phase::PhaseID']) ? $i['Phase::PhaseID'] : null;
        if (!empty($position_id) && $job->position_id != $position_id) {
            $status_phase = 1;
        }

        // check region value
        // ------------------
        $region_id = ($i['IDRegion']) ? $i['IDRegion'] : null;
        if (!empty($position_id) && $job->position_id != $position_id) {
            $status_region = 1;
        }

        // check sitecode value
        // --------------------
        if (!empty($sitecode) && $job->sitecode != $sitecode) {
            $status_sitecode = 1;
        }

        // check all value
        // ---------------
        if (!empty($join_date) && !empty($position_id) && !empty($phase_id) && !empty($region_id) && !empty($sitecode)) {
            $status_pass = 1; // all have value
        }

        // have all value
        // --------------
        $status = 0;
        if ($status_pass == 1) {

            // there's different
            // -----------------
            if ($status_join == 1 || $status_position == 1 || $status_phase == 1 || $status_region == 1 || $status_sitecode == 1) {
                $status = 1;
            }
        }
        return $status;
    }

    // sync staff contract
    // -------------------
    public function getSyncStaffContract($record, $skip)
    {

        // initialize variables
        // --------------------
        $start = date('Y-m-d H:i:s');
        $today = date('Y-m-d');
        $total = 0;
        $insert = 0;
        $update = 0;
        $total_same = 0;
        $empty = 0;
        $fail = 0;
        $staff = array();
        $duplicate = 0;
        $total_duplicate = 0;

        // call api staff contract
        // -----------------------
        $api = $this->getApiStaffContract($record, $skip);

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // record exist
        // ------------
        if (!empty($json['data'])) {

            // get total record
            // ----------------
            $total = count($json['data']);

            // loop record
            // -----------
            foreach ($json['data'] as $i) {

                // update if active
                // ----------------
                if ($i['IDStatusEmp'] == 1) {

                    // staff id|sitecode|start|end is not empty
                    // ----------------------------------------
                    if (!empty($i['IDStaff']) &&
                        !empty($i['Sitecode']) &&
                        !empty($i['ContractStart']) &&
                        !empty($i['ContractEnd'])) {

                        // define variables
                        // ----------------
                        $staff_id = $i['IDStaff'];
                        $sitecode = $i['Sitecode'];

                        // get date modified erp
                        // ---------------------
                        $date_modified = ErpHelper::checkDateTime($i['DateModified']);

                        // get contract date from erp
                        // --------------------------
                        $date_from = ErpHelper::checkDate($i['ContractStart']);
                        $date_to = ErpHelper::checkDate($i['ContractEnd']);

                        // check user info
                        // ---------------
                        $user = $this->user_repo->getUserByStaffIDSitecode($staff_id, $sitecode);

                        // check staff (if duplicate record)
                        // ---------------------------------
                        if (!in_array($staff_id, $staff)) {
                            $staff[] = $staff_id;
                        } else {
                            $duplicate = 1;
                            $total_duplicate++;
                        }

                        // have user record
                        // ----------------
                        if (!empty($user) && $duplicate != 1) {
                            $same = 0;
                            $i['user_id'] = $user->id;
                            $i['date_modified'] = $date_modified;

                            // check contract
                            // --------------
                            // $contract = $this->user_repo->getContractUserIDDate($user->id, $date_from, $date_to);
                            $contract = $this->user_repo->getContractByIDSitecode($user->id, $user->sitecode);
                            // $contract = $this->user_repo->getUserContractByUserIDStatus($user->id);

                            // contract not exist
                            // ------------------
                            if (empty($contract)) {

                                // check if start and end same
                                // ---------------------------
                                if (Carbon::parse($date_from)->eq(Carbon::parse($date_to))) {
                                    $same = 1;
                                    $total_same++;
                                }

                                // contract end must greater than today
                                // ------------------------------------
                                if (Carbon::parse($date_to)->gt(Carbon::parse($today)) && $same != 1) {

                                    // check if have transfer (same user id but different sitecode)
                                    // ------------------------------------------------------------
                                    $old = $this->user_repo->getUserContractByUserIDStatus($user->id);
                                    if (!empty($old)) {

                                        // set current contract as inactive
                                        // --------------------------------
                                        $this->db_erp->dbUpdateUserContractStatus($user->id);
                                        $update++;
                                    }

                                    // insert new user contract
                                    // ------------------------
                                    $this->db_erp->dbInsertUserContractSync($i);
                                    $insert++;
                                }
                            }

                            // contract exist
                            // --------------
                            else {

                                // check if start and end same
                                // ---------------------------
                                if (Carbon::parse($date_from)->eq(Carbon::parse($date_to))) {
                                    $same = 1;
                                    $total_same++;
                                }

                                // check whether date contract have differences
                                // end contract must greater then today
                                // ------------------------------------
                                $diff = 0;
                                if (Carbon::parse($date_from)->ne(Carbon::parse($contract->date_from)) ||
                                    Carbon::parse($date_to)->ne(Carbon::parse($contract->date_to)) &&
                                    Carbon::parse($date_to)->gt(Carbon::parse($today)) &&
                                    $same != 1) {
                                    $diff = 1;
                                }

                                // date contract is different
                                // --------------------------
                                if ($diff == 1) {

                                    // set current contract as inactive
                                    // --------------------------------
                                    $this->db_erp->dbUpdateUserContractStatus($user->id);
                                    $update++;

                                    // insert new contract
                                    // -------------------
                                    $this->db_erp->dbInsertUserContractSync($i);
                                    $insert++;
                                }
                            }
                        }
                    }

                    // value is empty
                    // --------------
                    else {
                        $empty++;
                    }
                }
            }
        }

        // no record
        // ---------
        else {
            $fail = 'Fail Read API';
        }

        // return result
        // -------------
        $data = array(
            'start' => $start,
            'fail' => $fail,
            'total' => $total,
            'insert' => $insert,
            'update' => $update,
            'same' => $total_same,
            'empty' => $empty,
            'duplicate' => $total_duplicate,
            'record' => $record,
            'skip' => $skip,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncStaffContract($data);
        return $data;
    }

    // sync site
    // ---------
    public function getSyncSite()
    {
        // initialize variables
        // --------------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // call api site
        // -------------
        $api = $this->getApiSite();

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // record exist
        // ------------
        if (!empty($json['data'])) {

            // get total record
            // ----------------
            $total = count($json['data']);

            // loop record
            // -----------
            foreach ($json['data'] as $data) {

                // trim white space
                // ----------------
                $i = array_map('trim', $data);

                // get sitecode
                // ------------
                $sitecode = strtoupper($i['Site Code']);

                // check site by code
                // ------------------
                $query = $this->getSiteByCode($sitecode);

                // record empty
                // ------------
                if (empty($query)) {

                    // insert new site
                    // ---------------
                    $this->db_erp->dbInsertSite($i);
                    $insert++;
                }

                // have record
                // -----------
                else {

                    // check value date modified
                    // -------------------------
                    if (!empty($i['DateModified']) && $i['DateModified'] != '00/00/0000') {

                        // get date modified erp
                        // ---------------------
                        $date_modified = ErpHelper::checkDate($i['DateModified']);

                        // get status erp
                        // --------------
                        $live_status = $i['LiveStatus'];

                        // date modified is different & do not check current inactive
                        // ----------------------------------------------------------
                        // if (Carbon::parse($query->date_modified)->ne(Carbon::parse($date_modified)) && $query->status == 1) {
                        if (Carbon::parse($query->date_modified)->ne(Carbon::parse($date_modified))) {

                            // status erp must active
                            // ----------------------
                            if ($live_status == 1) {

                                // insert current record into history
                                // ----------------------------------
                                $save = $this->db_erp->dbInsertSiteHistory($query);

                                // done insert history
                                // -------------------
                                if ($save) {

                                    // update current site
                                    // -------------------
                                    $this->db_erp->dbUpdateSite($i);
                                    $update++;
                                }
                            }
                        }
                    }
                }
            }
        }

        // no record
        // ---------
        else {
            $fail = 'Fail Read API';
        }

        // set value email
        // ---------------
        $data = array(
            'start' => $start,
            'total' => $total,
            'insert' => $insert,
            'update' => $update,
            'fail' => $fail,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncSite($data);
        return $data;
    }

    // sync public holiday
    // -------------------
    public function getSyncPublicHoliday($year)
    {
        // initialize variables
        // --------------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // set value year
        // --------------
        if (empty($year)) {
            $year = date('Y');
        }

        // get api public holiday
        // -----------------------
        $api = $this->getApiPublicHoliday($year);

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // record exist
        // ------------
        if (!empty($json['data'])) {

            // get total record
            // ----------------
            $total = count($json['data']);

            // date to excludes
            // ----------------
            $excludes = array('2018-05-10', '2018-05-11', '2018-05-13', '2018-05-17', '2018-05-18');

            // loop record
            // -----------
            foreach ($json['data'] as $i) {

                // get date
                // --------
                $date = ErpHelper::checkDate($i['DateCuti']);

                // exclude not related state
                // -------------------------
                if (!in_array($date, $excludes)) {

                    // check public holiday
                    // --------------------
                    $q = $this->getPublicHoliday($i);

                    // insert leave_public
                    // -------------------
                    if (empty($q)) {

                        // insert leave_public
                        // -------------------
                        $public_id = $this->db_erp->dbInsertPublic($i, $year);

                        // insert leave public states
                        // --------------------------
                        $arr = array($i['DateCuti'], $i['ListStateID']);
                        $this->db_erp->dbInsertPublicState($year, $public_id, $arr);
                        $insert++;
                    }

                    // record exist
                    // ------------
                    else {

                        // get date modified
                        // -----------------
                        $erp_modified = Carbon::createFromFormat('m/d/Y H:i:s', $i['DateModified'])->format('Y-m-d');
                        $date_modified = Carbon::createFromFormat('Y-m-d H:i:s', $q->date_modified)->format('Y-m-d');

                        // check if have any changes
                        // -------------------------
                        if (trim($q->desc) != trim($i['Regarding']) || Carbon::parse($date_modified)->ne(Carbon::parse($erp_modified))) {

                            // update public holiday
                            // ---------------------
                            $this->db_erp->dbUpdatePublicHoliday($i, $q->id);
                            $update++;
                        }
                    }
                }
            }
        }

        // no record
        // ---------
        else {
            $fail = 'Fail Read API';
        }

        // set value email
        // ---------------
        $data = array(
            'start' => $start,
            'year' => $year,
            'total' => $total,
            'insert' => $insert,
            'update' => $update,
            'fail' => $fail,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncPublicHoliday($data);
        return $data;
    }

    // sync region
    // -----------
    public function getSyncRegion()
    {

        // initialize variables
        // --------------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // get api
        // -------
        $api = $this->getApiRegion();

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // record exist
        // ------------
        if (!empty($json['data'])) {

            // get total record
            // ----------------
            $total = count($json['data']);

            // check record
            // ------------
            foreach ($json['data'] as $i) {

                // check existing region
                // ---------------------
                $q = $this->getRegionByID($i['ID REGION']);

                // no record yet
                // -------------
                if (empty($q)) {

                    // insert new region
                    // -----------------
                    $this->db_erp->dbInsertRegion($i);
                    $insert++;
                }

                // record exist
                // ------------
                else {

                    // region name is different
                    // ------------------------
                    if ($i['Region Name'] != $q->name_eng) {

                        // update state
                        // ------------
                        $this->db_erp->dbUpdateRegion($i, $q->id);
                        $update++;
                    }
                }
            }
        }

        // record empty
        // ------------
        else {
            $fail = 'Fail Read API';
        }

        // return result
        // -------------
        $data = array(
            'start' => $start,
            'total' => $total,
            'insert' => $insert,
            'update' => $update,
            'fail' => $fail,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncRegion($data);
        return $data;
    }

    // sync state
    // ----------
    public function getSyncState()
    {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // get api
        // -------
        $api = $this->getApiState();

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // record exist
        // ------------
        if (!empty($json['data'])) {
            $total = count($json['data']);
            foreach ($json['data'] as $i) {

                // check state
                // -----------
                $q = $this->getStateByCode($i['ID']);

                // no record yet
                // -------------
                if (empty($q)) {

                    // insert new state
                    // ----------------
                    $this->db_erp->dbInsertState($i);
                    $insert++;
                }

                // record exist
                // ------------
                else {
                    if ($i['Name'] != $q->name || $i['IDState'] != $q->erp_id) {

                        // update state
                        // ------------
                        $this->db_erp->dbUpdateState($i, $q->code);
                        $update++;
                    }
                }
            }
        }

        // record empty
        // ------------
        else {
            $fail = 'Fail Read API';
        }

        // return result
        // -------------
        $data = array(
            'start' => $start,
            'total' => $total,
            'insert' => $insert,
            'update' => $update,
            'fail' => $fail,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncState($data);
        return $data;
    }

    // sync position
    // -------------
    public function getSyncPosition()
    {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // get api
        // -------
        $api = $this->getApiPosition();

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // have record
        // -----------
        if (!empty($json['data'])) {
            $total = count($json['data']);
            foreach ($json['data'] as $i) {

                // check position
                // --------------
                $q = $this->getPositionByID($i['ID']);

                // insert record
                // -------------
                if (empty($q)) {

                    // check group id
                    // --------------
                    if (in_array($i['ID'], array(48, 49, 57, 103))) {
                        // human resource
                        $group_id = 2;
                    } elseif (in_array($i['ID'], array(4, 5, 6, 84))) {
                        // site supervisor
                        $group_id = 3;
                    } elseif (in_array($i['ID'], array(1, 2))) {
                        // region manager
                        $group_id = 4;
                    } else {
                        $group_id = 0;
                    }

                    // insert position
                    // ---------------
                    $this->db_erp->dbInsertPosition($i, $group_id);
                    $insert++;
                }

                // update record
                // -------------
                else {
                    if ($i['Position'] != $q->name) {
                        $this->db_erp->dbUpdatePosition($i, $q->id);
                        $update++;
                    }
                }
            }
        }

        // record empty
        // ------------
        else {
            $fail = 'Fail Read API';
        }

        // return result
        // -------------
        $data = array(
            'start' => $start,
            'total' => $total,
            'insert' => $insert,
            'update' => $update,
            'fail' => $fail,
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncPosition($data);
        return $data;
    }

    // sync phase
    // ----------
    public function getSyncPhase()
    {

        // check & process api
        // -------------------
        try {

            // set variables
            // -------------
            $start = date('Y-m-d H:i:s');
            $total = 0;
            $insert = 0;
            $update = 0;
            $fail = 0;

            // get api
            // -------
            $api = $this->getApiPhase();

            // read request
            // ------------
            $res = $this->client->request('GET', $api);

            // get response
            // ------------
            $json = json_decode($res->getBody(), true);

            // record exist
            // ------------
            if (!empty($json['data'])) {
                $total = count($json['data']);
                foreach ($json['data'] as $i) {

                    // check existing phase
                    // --------------------
                    $q = $this->user_repo->getPhaseByID($i['PhaseID']);
                    if (empty($q)) {

                        // insert new phase
                        // ----------------
                        $this->db_erp->dbInsertPhase($i);
                        $insert++;
                    } else {
                        if ($i['Phase Name'] != $q->name || $i['Phase Name TM'] != $q->name_tm || $i['Phase Name Both'] != $q->name_both) {

                            // update phase
                            // ------------
                            $this->db_erp->dbUpdatePhase($i, $q->id);
                            $update++;
                        }
                    }
                }
            }

            // no record
            // ---------
            else {
                $fail = 'Fail Read API';
            }

            // return result
            // -------------
            $data = array(
                'start' => $start,
                'total' => $total,
                'insert' => $insert,
                'update' => $update,
                'fail' => $fail,
            );

            // send email notification
            // -----------------------
            $this->email_repo->getSyncPhase($data);
            return $data;
        }

        // get error message
        // -----------------
        catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    // sync staff other (management)
    // -----------------------------
    public function getSyncStaffOther($group)
    {
        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $fail = 0;

        // get url API
        // -----------
        if ($group == 2) {
            $api = $this->getApiStaffHR();
            $name = 'HR';
        } 
        else if ($group == 4) {
            $api = $this->getApiStaffRM();
            $name = 'RM';
        } 
        else if ($group == 6) {
            $api = $this->getApiStaffDRM();
            $name = 'DRM';
        } 
        else if ($group == 7) {
            $api = $this->getApiStaffClerk();
            $name = 'Clerk';
        } 
        else if ($group == 10) {
            $api = $this->getApiStaffProject();
            $name = 'Project';            
        } 
        else {
            dd('Group ID Invalid.');
        }

        // read request
        // ------------
        $res = $this->client->request('GET', $api);

        // get response
        // ------------
        $json = json_decode($res->getBody(), true);

        // have record
        // -----------
        if (!empty($json['data'])) {
            $total = count($json['data']);
            foreach ($json['data'] as $i) {

                // check if status is active
                // -------------------------
                if ($i['IDStatusEmp'] == 1
                    && !empty($i['NRIC'])
                    && !empty($i['IDStaff'])
                    && !empty($i['WorkEmail'])) {

                    // set icno
                    // --------
                    $i['icno'] = str_replace("-", "", $i['NRIC']);

                    // check record
                    // ------------
                    $check = $this->getUserOther($group, $i['icno'], $i['IDStaff']);

                    // no record yet
                    // -------------
                    if (empty($check)) {

                        // set group id
                        // ------------
                        $i['group_id'] = $group;
                        $i['sitecode'] = null;

                        // insert user info
                        // ----------------
                        $user_id = $this->db_erp->dbInsertUser($i);

                        // insert user job
                        // ---------------
                        $i['user_id'] = $user_id;
                        $user_job = $this->db_erp->dbInsertUserJobDrm($i);
                        $insert++;
                    }
                }
            }
        }

        // can't read api
        // --------------
        else {
            $fail = 'Fail Read API';
        }

        // return result
        // -------------
        $data = array(
            'start' => $start,
            'total' => $total,
            'insert' => $insert,
            'fail' => $fail,
            'name' => $name
        );

        // send email notification
        // -----------------------
        $this->email_repo->getSyncStaffOther($data);

        // return result
        // -------------
        return $data;
    }

    // get state by code
    // -----------------
    public function getStateByCode($code)
    {
        return \IhrV2\Models\State::where('code', $code)->first();
    }

    // get region by id
    // ----------------
    public function getRegionByID($id)
    {
        return \IhrV2\Models\Region::find($id);
    }

    // check user other
    // ----------------
    public function getUserOther($group_id, $icno, $staff_id)
    {
        $q = \IhrV2\User::where([
            'icno' => $icno,
            'staff_id' => $staff_id,
            'group_id' => $group_id
        ])
        ->first();
        return $q;
    }

    public function getUserJob($user_id)
    {
        return \IhrV2\Models\UserJob::where('user_id', $user_id)->get();
    }

    public function getUserJobByIC($staff_id)
    {
        $q = \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->where('status', 1)
            ->first();
        return $q;
    }

    public function getUserJobActive($user_id, $sitecode)
    {
        return \IhrV2\Models\UserJob::where('user_id', $user_id)
            ->where('sitecode', $sitecode)
            ->where('status', 1)
            ->get();
    }

    public function getUserJobInactive($staff_id, $sitecode)
    {
        return \IhrV2\Models\UserJob::where('staff_id', $staff_id)
            ->where('sitecode', $sitecode)
            ->where('status', 2)
            ->where('flag', 1)
            ->first();
    }

    public function getUserContract($user_id)
    {
        return \IhrV2\Models\UserContract::where('user_id', $user_id)->where('status', 1)->get();
    }

    public function getUserContractStaffID($user_id, $sitecode)
    {
        return \IhrV2\Models\UserContract::where('user_id', $user_id)
            ->where('sitecode', $sitecode)
            ->where('status', 1)
            ->first();
    }

    public function getUserByIC($icno)
    {
        $q = \IhrV2\User::where('icno', $icno)
            ->with(array('UserLatestJob'))
            ->first();
        return $q;
    }

    public function getUserOnlyByIC($icno)
    {
        $q = \IhrV2\User::where('icno', $icno)->where('status', 1)->first();
        return $q;
    }

    public function getUserByICNStaffID($icno, $staff_id)
    {
        $q = \IhrV2\User::where('icno', $icno)
            ->where('staff_id', $staff_id)
            ->with(array('UserLatestJob', 'UserLatestContract')) // status = 1
            ->first();
        return $q;
    }

    public function getUserOnlyByICNStaffID($icno, $staff_id)
    {
        $q = \IhrV2\User::where('icno', $icno)
            ->with(array('UserLatestJob'))
            ->where('staff_id', $staff_id)
            ->where('status', 1)
            ->first();
        return $q;
    }

    public function getUserByICNStaffIDInactive($icno, $staff_id)
    {
        $q = \IhrV2\User::where('icno', $icno)
            ->with(array('UserInactive')) // status = 2
            ->where('staff_id', $staff_id)
            ->first();
        return $q;
    }

    public function getUserByICStaffIDSitecode($icno, $staff_id, $sitecode)
    {
        $q = \IhrV2\User::where('icno', $icno)
            ->with(array('UserLatestJob')) // status = 1
            ->where('staff_id', $staff_id)
            ->where('sitecode', $sitecode)
            ->where('status', 1)
            ->first();
        return $q;
    }

    // check site by code
    // ------------------
    public function getSiteByCode($sitecode)
    {
        return \IhrV2\Models\Site::where('code', $sitecode)->first();
    }

    // check site by code and name
    // ---------------------------
    public function getSiteByCodeName($code, $name)
    {
        $q = \IhrV2\Models\Site::where([
            'code' => $code,
            'name' => $name,
        ])
        ->first();
        return $q;
    }

    // check public holiday
    // --------------------
    public function getPublicHoliday($i)
    {
        // get year and date
        // -----------------
        $year = $i['Year'];
        $desc = $i['Regarding'];
        $date = Carbon::createFromFormat('m/d/Y', $i['DateCuti'])->format('Y-m-d');

        // check record
        // ------------
        $q = \IhrV2\Models\LeavePublic::where([
            'year' => $year,
            'desc' => $desc,
            'status' => 1,
        ])
        ->whereDate('date', '=', $date)
        ->first();
        return $q;
    }

    public function getPositionByID($id)
    {
        return \IhrV2\Models\Position::where('id', $id)->first();
    }

    // get previous staff id for status inactive or Transfer
    // -----------------------------------------------------
    public function getUserICPrev($icno)
    {
        $q = \IhrV2\User::where('icno', $icno)
            ->whereIn('status', array(2, 3))
        // ->where('group_id', 3)
            ->orderBy('id', 'desc')
        // ->take(1)
        // ->get();
            ->first();
        return $q;
    }

    public function getUserIC($icno)
    {
        return \IhrV2\User::where('icno', $icno)->first();
    }

    public function getUserICActive($icno)
    {
        return \IhrV2\User::where([
            'icno' => $icno,
            'status' => 1,
        ])
            ->first();
    }

    public function getCheckLastJob($staff_id, $sitecode)
    {
        $q = \IhrV2\Models\UserJob::where([
            'staff_id' => $staff_id,
            'sitecode' => $sitecode,
            'status' => 2,
        ])
        ->where('resign_date', '=', null)
        ->orderBy('id', 'desc')
        ->first();
        return $q;
    }
}
