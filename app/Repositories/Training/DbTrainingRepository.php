<?php
namespace IhrV2\Repositories\Training;

use IhrV2\Contracts\Training\DbTrainingInterface;
use Carbon\Carbon;

class DbTrainingRepository implements DbTrainingInterface {

	// insert training
	// ---------------
	public function dbInsertTraining($i)
	{
        // save training
        // -------------
        $arr = array(
        	'name' => $i['name'],
            'start_date' => $i['from'],
            'end_date' => $i['to'],
        	'location' => $i['location'],
        	'description' => $i['description'],
        	'created_by' => auth()->user()->id,        	
        	'status' => 1
        );
        $sav = new \IhrV2\Models\Training\Training($arr);
        $sav->save();
        return $sav->id;
	}

    // insert training dates
    // ---------------------
    public function dbInsertTrainingDate($id, $date, $i)
    {
        $arr = array(
            'training_id' => $id,
            'code' => 'TTT#'.$id.'#'.$date.'#'.$i, // TTT#ID#YYYY-MM-DD#SLOT
            'date' => $date, 
            'slot' => $i,
            'status' => 1
        );
        $sav = new \IhrV2\Models\Training\TrainingDate($arr);
        $sav->save();
        return $sav->id;
    }

    // insert training attendees
    // -------------------------
    public function dbInsertTrainingTrainee($id, $date_id, $user)
    {
        $arr = array(
            'training_id' => $id,
            'date_id' => $date_id,
            'user_id' => $user['id'],
            'sitecode' => $user['sitecode'],
            'status' => 1
        );
        $sav = new \IhrV2\Models\Training\TrainingAttendee($arr);
        $sav->save();
        return true;
    }



}

