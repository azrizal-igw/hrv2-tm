<?php
namespace IhrV2\Repositories\Training;

use IhrV2\Contracts\Training\DbTrainingInterface;
use IhrV2\Contracts\Training\TrainingInterface;
use IhrV2\Contracts\Upload\UploadInterface;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\User\UserInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Helpers\CommonHelper;
use Carbon\Carbon;

class TrainingRepository implements TrainingInterface 
{
    protected $db_training;
    protected $upload_repo;
    protected $email_repo;
    protected $user_repo;
    protected $leave_repo;

    public function __construct(DbTrainingInterface $db_training, UploadInterface $upload_repo, EmailInterface $email_repo, UserInterface $user_repo, LeaveInterface $leave_repo) {
        $this->db_training = $db_training;
        $this->upload_repo = $upload_repo;
        $this->email_repo = $email_repo;
        $this->user_repo = $user_repo;
        $this->leave_repo = $leave_repo;
    }

	// get training by id
	// ------------------
	public function getTrainingByID($id)
	{
		return \IhrV2\Models\Training\Training::find($id);
	}

	// get training attendance by user
	// -------------------------------
	public function getTrainingAttByUser($id, $uid, $sitecode)
	{
		$q = \IhrV2\Models\Training\TrainingAttendanceLog::where(['training_id' => $id, 'user_id' => $uid, 'sitecode' => $sitecode])->first();
		return $q;
	}

	// save new training
	// -----------------
	public function getTrainingSave($i)
	{
        // convert date
        // ------------
        $i['from'] = Carbon::createFromFormat('d/m/Y', $i['date_from'])->format('Y-m-d');
        $i['to'] = Carbon::createFromFormat('d/m/Y', $i['date_to'])->format('Y-m-d');

        // check date range
        // ----------------
        if (Carbon::parse($i['from'])->lt(Carbon::parse($i['to']))) {

            // save training
            // -------------
            $id = $this->db_training->dbInsertTraining($i);

            // get date range
            // --------------
            $duration = $this->leave_repo->DateRange($i['from'], $i['to']);

            // get trainees
            // ------------
            $users = array();
            if (!empty($i['chk_id'])) {
                $users = \IhrV2\User::select('id', 'sitecode')->whereIn('id', $i['chk_id'])->get()->toArray();
            }

            // loop duration
            // -------------
            if (!empty($duration)) {
                foreach ($duration as $date) {

                    // loop for 2 slot
                    // ---------------
                    for ($i = 1; $i <= 2; $i++) {

                        // save training dates
                        // -------------------
                        $date_id = $this->db_training->dbInsertTrainingDate($id, $date, $i); 

                        // loop users
                        // ----------
                        if (!empty($users)) {
                            foreach ($users as $user) {

                                // save training attendees
                                // -----------------------
                                $this->db_training->dbInsertTrainingTrainee($id, $date_id, $user);   
                            }
                        }    
                    }             
                }
            }

            // set email content
            // -----------------
            $arr_body = array(
                'training' => $this->getTrainingByID($id),
            );

            // send email notification
            // -----------------------
            $this->email_repo->getEmailTrainingCreate($arr_body);

            // clear session selected staff
            // ----------------------------
            session()->forget('training.chk_id');

            // return output
            // -------------
            return array('success', 'Training Successfully Added.');
        }

        // date range is not allowed
        // -------------------------
        else {
            return array('danger', 'Date Selection is not allowed.');
        }
	}

	// lists of training
	// -----------------
	public function getTrainingIndex($request = null)
	{
        $header = array(
            'parent'  => 'Training Attendance',
            'child'   => 'Training',
            'icon'    => 'earphones-alt',
            'title'   => 'Training'
        );
        $data = $this->getTrainingList();
        return view('modules.training.index', compact('header', 'data'));
	}

	// get training lists
	// ------------------
	public function getTrainingList()
	{
		$q = \IhrV2\Models\Training\Training::with(array('CategoryName'))->orderBy('id', 'desc')->paginate(20);
		return $q;
	}

    // get all active users
    // --------------------
    public function getStaffActiveAll2()
    {
        // query user
        // ----------
        $q = \IhrV2\User::where(['group_id' => 3, 'status' => 1])
        ->with(array('UserLatestJob' => function ($h) {
            $h->with(array('PositionName', 'PhaseName', 'RegionName'));
        }))
        ->with(array('UserPhotoActive' => function ($s) {
            $s->with('PhotoLatestStatus');
        }))
        ->with(array('SiteNameInfo'))
        ->whereNotIn('sitecode', array('X01C001'))
        ->orderBy('sitecode', 'asc')                    
        ->get();
        return $q;
    }

	// get all active users
	// --------------------
	public function getStaffActiveAll($arr, $checked)
	{
		// get variables
		// -------------
		$site_id = $arr['site_id'];
		$phase_id = $arr['phase_id'];
		$region_id = $arr['region_id'];
		$keyword = $arr['keyword'];
        $position_id = $arr['position_id'];        
        $job = array('phase_id' => $phase_id, 'region_id' => $region_id, 'position_id' => $position_id);
        $cond = array('sitecode' => $site_id, 'group_id' => 3, 'status' => 1);

		// query user
		// ----------
		$q = \IhrV2\User::where(['group_id' => 3, 'status' => 1])
        ->with(array('UserLatestJob' => function ($h) {
            $h->with(array('PositionName', 'PhaseName', 'RegionName'));
        }))

        // search by (sitecode/group id/status)
        // ------------------------------------
        ->where(function ($s) use ($cond) {
            foreach ($cond as $column => $key) {
                if (!is_null($key)) {
                    $s->where($column, $key);
                }
            }
        })

        // search by job
        // -------------
        ->where(function ($j) use ($job) {
            $j->whereHas('UserCurrentJob2', function ($i) use ($job) {
                foreach ($job as $column => $key) {
                    if (!is_null($key)) {
                        $i->where($column, $key);
                    }
                }
            });
        })

        // search by keyword
        // -----------------
        ->where(function ($t) use ($keyword) {
            if (!empty($keyword)) {
                $t->where('name', 'like', '%' . $keyword . '%');
                $t->orWhere('icno', 'like', '%' . $keyword . '%');
                $t->orWhere('username', 'like', '%' . $keyword . '%');
                $t->orWhere('sitecode', 'like', '%' . $keyword . '%');
            }
        })
        ->with(array('UserPhotoActive' => function ($s) {
            $s->with('PhotoLatestStatus');
        }))
        ->with(array('SiteNameInfo'))
        ->whereNotIn('sitecode', array('X01C001'))
        ->whereNotIn('id', $checked)
        ->orderBy('sitecode', 'asc')            		
		->get();
		return $q;
	}

    // get lists of staff at the popup
    // -------------------------------
    public function getUserListByIds($ids, $data)
    {
        $q = \IhrV2\User::where(['group_id' => 3, 'status' => 1])
        ->where(function ($k) use ($ids) {
            if (!empty($ids)) {
                $k->whereNotIn('id', $ids);
            }
        })
        ->with(array('UserLatestJob' => function ($h) {
            $h->with('PositionName', 'PhaseName', 'RegionName');
        }))
        ->with(array('UserPhotoActive' => function ($s) {
            $s->with('PhotoLatestStatus');
        }))        
        ->with(array('SiteNameInfo')) 
        ->orderBy('id', 'asc')
        ->take(5)       
        ->get();        
        return $q;
    }

    // get lists of staff by session
    // -----------------------------
    public function getUserListBySession($ids)
    {
        $q = \IhrV2\User::where(['group_id' => 3, 'status' => 1])
        ->where(function ($k) use ($ids) {
            if (!empty($ids)) {
                $k->whereIn('id', $ids);
            }
        })
        ->with(array('UserLatestJob' => function ($h) {
            $h->with('PositionName', 'PhaseName', 'RegionName');
        }))
        ->with(array('UserPhotoActive' => function ($s) {
            $s->with('PhotoLatestStatus');
        }))        
        ->with(array('SiteNameInfo')) 
        ->orderBy('id', 'asc')
        ->take(5)       
        ->get();        
        return $q;
    }

    // query lists of users
    // --------------------
	public function getSelectedUser($ids)
	{
		$q = \IhrV2\User::whereIn('id', $ids)->where(['group_id' => 3, 'status' => 1])->get();
		return $q;
	}

	// add new trainee
	// ---------------
	public function getTrainingCreateUser($request = null)
	{
        $header = array(
            'parent'  => 'Training Attendance',
            'child'   => 'Add',
            'child-a' => route('mod.training.create'),
            'sub'     => 'Trainee',
            'icon'    => 'user-follow',
            'title'   => 'Trainee'
        );

        // no search session
        // -----------------
        $empty = 0;
        if (empty($request)) {
            $empty = 1;
            $site_id = CommonHelper::checkSession('site_id');
            $region_id = CommonHelper::checkSession('region_id');
            $phase_id = CommonHelper::checkSession('phase_id');
            $keyword = CommonHelper::checkSession('keyword');
            $position_id = CommonHelper::checkSession('position_id');            
        }

        // have search session
        // -------------------
        else {
            $reset = 0;
            if (isset($request->reset)) {
                $reset = 1;
                $inputs = array('site_id', 'region_id', 'phase_id', 'keyword', 'position_id');
                session()->forget($inputs);
                return redirect()->route('mod.training.create.select');
            }

            // click button select
            // -------------------
            if (isset($request->select)) {

                // have select any staff
                // ---------------------
            	if (!empty($request->chk_id)) {

                    // store session
                    // -------------
                    session()->push('training.chk_id', $request->chk_id);
	            	return redirect()->route('mod.training.create');
            	}
            }

            // check session
            // -------------
            $site_id = CommonHelper::checkSessionPost($request->site_id, $reset, 'site_id');
            $region_id = CommonHelper::checkSessionPost($request->region_id, $reset, 'region_id');
            $phase_id = CommonHelper::checkSessionPost($request->phase_id, $reset, 'phase_id');
            $keyword = CommonHelper::checkSessionPost($request->keyword, $reset, 'keyword');
            $position_id = CommonHelper::checkSessionPost($request->position_id, $reset, 'position_id');
        }

        // get lists sites
        // ---------------
        $chk = $this->user_repo->getListSiteNCode();
        $sites = $chk['sites'];

        // get lists regions
        // -----------------
        $regions = $this->user_repo->getRegionList();

        // get lists phases
        // ----------------
        $phases = $this->user_repo->getPhaseList();   

        // get lists of positions
        // ----------------------
        $positions = $this->user_repo->getPositionListDistinct(array(3));

        // search sessions
        // ---------------
        $sessions = array(
            'site_id'     => $site_id,
            'phase_id'    => $phase_id,
            'region_id'   => $region_id,
            'keyword'     => $keyword,
            'position_id' => $position_id
        );

        // check session selected staff
        // ----------------------------
        $checked = [];
        if (!empty(session()->has('training.chk_id'))) {
            $checked = array_collapse(session()->get('training')['chk_id']);
        }

        // get lists of active users
        // -------------------------
        $users = [];
        if ($empty == 0) {
            $users = $this->getStaffActiveAll($sessions, $checked);        
        }
        // dd($users);
        return view('modules.training.select', compact('header', 'sites', 'regions', 'phases', 'positions', 'sessions', 'users'));
	}

    // get lists of selected trainees
    // ------------------------------
    public function getTrainingSession()
    {
        $get = $this->getListTrainee($request = null);
        return $get;
    }

    // remove selected trainees
    // ------------------------   
    public function postTrainingRemove($request)
    {
        $get = $this->getListTrainee($request);
        return $get;     
    }

    // get lists trainee training
    // --------------------------
    public function getListTrainee($request)
    {
        // display checked at checkbox
        // ---------------------------
        if (!empty(session()->has('training.chk_id'))) {
            $chk_id = array_collapse(session()->get('training')['chk_id']);
            if (!empty($chk_id)) {
                $checked = 'checked';
            }
            else {
                $checked = '';
            }
        }
        else {
            $checked = '';
        }

        // initialize string
        // -----------------
        $str = "";

        // set table
        // ---------
        $str .= "<table class=\"table table-condensed table-responsive\">";
        $str .= "   <thead>";
        $str .= "       <tr>";
        $str .= "           <th class=\"col-md-1\">No</th>";
        $str .= "           <th class=\"col-md-5\">Name /<br>Position</th>";
        $str .= "           <th class=\"col-md-5\">Site Name /<br>Site Code</th>";
        $str .= "           <th class=\"col-md-1 text-right\"><input type=\"checkbox\" id=\"chk_all\" title=\"Check All\" $checked></th>";
        $str .= "       </tr>";
        $str .= "   </thead>";

        // check session
        // -------------
        $users = array();

        // have session
        // ------------
        if (!empty(session()->has('training.chk_id'))) {

            // have request
            // ------------
            if (!empty($request)) {

                // check at least one checkbox
                // ---------------------------
                if (!empty($request->chk_id)) {
                    
                    // get session
                    // -----------
                    $sessions = session()->get('training')['chk_id'];

                    // loop array session
                    // ------------------
                    foreach ($sessions as $s_key => $s_value) {
                        foreach ($s_value as $i => $j) {

                            // check if selected record is on the session
                            // ------------------------------------------
                            if (in_array($j, $request->chk_id)) {

                                // remove selected value from session
                                // ----------------------------------
                                unset($sessions[$s_key][$i]);
                            }
                        }
                    }

                    // store again session
                    // -------------------
                    session()->put('training.chk_id', $sessions);                
                }
            }

            // convert multi dimensional array into single array
            // -------------------------------------------------            
            $ids = array_collapse(session()->get('training')['chk_id']);

            // query users
            // -----------
            if (!empty($ids)) {
                $users = $this->getUserListBySession($ids)->toArray();

                // loop records
                // ------------
                if (!empty($users)) {
                    $no = 0;
                    foreach ($users as $i) {
                        $no++;
                        $str .= "<tr>";
                        $str .= "   <td>".$no."</td>";
                        if (!empty($i['user_latest_job'])) {
                            $position = $i['user_latest_job']['position_name']['name'];
                        }
                        else {
                            $position = '-';
                        }
                        $str .= "   <td>".$i['name']." /<br>".$position."</td>";
                        if (!empty($i['site_name_info'])) {
                            $sitename = $sitename = $i['sitecode'].' '.$i['site_name_info']['name'];
                        }
                        else {
                            $sitename = $i['sitecode'];
                        }
                        if (!empty($i['user_latest_job'])) {                        
                            $phasename = $i['user_latest_job']['phase_name']['name'];
                        }
                        else {
                            $phasename = '-';
                        }
                        $str .= "   <td>".$sitename." /<br>".$phasename."</td>";
                        $str .= "   <td class=\"text-right\"><input class=\"chk_id\" name=\"chk_id[]\" type=\"checkbox\" value=".$i['id']." checked=\"true\"></td>";
                        $str .= "</tr>";
                    }
                    $str .= "<tr><td colspan=\"4\" class=\"text-center\">Total: ".$no."</td></tr>";
                }                
            }

            // no record
            // ---------
            else {
                $str .= "<tr><td colspan=\"4\">No record</td></tr>";                
            }
        }

        // no session
        // ----------
        else {
            $str .= "<tr><td colspan=\"4\">No record</td></tr>";                
        }
        $str .= "</table>";

        // return output
        // -------------
        return $str;             
    }

    public function getTrainingUser($search)
    {
        // initialize string
        // -----------------
        $str = "";

        // set table
        // ---------
        $str .= "<table class=\"table table-condensed table-responsive table-striped\">";
        $str .= "   <tbody>";
        $str .= "       <tr>";
        $str .= "           <th class=\"col-md-1\">No</th>";
        $str .= "           <th class=\"col-md-4\">Name /<br>IC No /<br>Position</th>";
        $str .= "           <th class=\"col-md-4\">Site Name /<br>Phase /<br>Region</th>";
        $str .= "           <th class=\"col-md-2 text-right\">Photo</th>";
        $str .= "           <th class=\"col-md-1 text-right\"><input type=\"checkbox\" id=\"chk_all\" title=\"Check All\"></th>";
        $str .= "       </tr>";
        $str .= "   </tbody>";

        // check session
        // -------------
        $users = array();
        $ids = array();

        // have session
        // ------------
        if (!empty(session()->has('training.chk_id'))) {

            // have request
            // ------------
            if (!empty($request)) {

                // check at least one checkbox
                // ---------------------------
                if (!empty($request->chk_id)) {
                    
                    // get session
                    // -----------
                    $sessions = session()->get('training')['chk_id'];

                    // loop array session
                    // ------------------
                    foreach ($sessions as $s_key => $s_value) {
                        foreach ($s_value as $i => $j) {

                            // check if selected record is on the session
                            // ------------------------------------------
                            if (in_array($j, $request->chk_id)) {

                                // remove selected value from session
                                // ----------------------------------
                                unset($sessions[$s_key][$i]);
                            }
                        }
                    }

                    // store again session
                    // -------------------
                    session()->put('training.chk_id', $sessions);                
                }
            }

            // convert multi dimensional array into single array
            // -------------------------------------------------            
            $ids = array_collapse(session()->get('training')['chk_id']);
        }

        // query users
        // -----------
        $users = $this->getUserListByIds($ids, $search)->toArray();

        // loop records
        // ------------
        if (!empty($users)) {
            $no = 0;
            foreach ($users as $i) {
                $no++;
                $str .= "<tr>";

                $str .= "   <td>".$no."</td>";

                if (!empty($i['user_latest_job'])) {
                    $position = $i['user_latest_job']['position_name']['name'];
                }
                else {
                    $position = '-';
                }
                $str .= "   <td>".$i['name']." /<br>".$i['icno']." /<br>".$position."</td>";
                if (!empty($i['site_name_info'])) {
                    $sitename = $sitename = $i['sitecode'].' '.$i['site_name_info']['name'];
                }
                else {
                    $sitename = $i['sitecode'];
                }

                if (!empty($i['user_latest_job'])) {                        
                    $phasename = $i['user_latest_job']['phase_name']['name'];
                }
                else {
                    $phasename = '-';
                }
                if (!empty($i['user_latest_job'])) {                        
                    $regionname = $i['user_latest_job']['region_name']['name'];
                }
                else {
                    $regionname = '-';
                }
                $str .= "   <td>".$sitename." /<br>".$phasename." /<br>".$regionname."</td>";
                if (!empty($i['user_photo_active'])) {
                    if ($i['user_photo_active']['photo_latest_status']['status'] == 2) {
                        $photo = $i['user_photo_active']['photo_thumb'].'.'.$i['user_photo_active']['ext'];
                        $url = route('lib.file.user.thumb', array($photo));
                    }
                    else {
                        $url = route('lib.image', array('default.png'));
                    }
                }
                else {
                    $url = route('lib.image', array('default.png'));
                }
                $str .= "   <td align=\"right\"><img src=\"$url\" class=\"img-responsive\" style=\"width: 35%; align: right\"></td>";
                $str .= "   <td class=\"text-right\"><input class=\"chk_id\" name=\"chk_id[]\" type=\"checkbox\" value=".$i['id']."></td>";
                $str .= "</tr>";
            }
            $str .= "<tr><td colspan=\"5\" class=\"text-center\">Total: ".$no."</td></tr>";
        }                

        // return result
        // -------------
        $str .= "</table>";
        return $str;        
    }



}




