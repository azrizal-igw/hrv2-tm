<?php namespace IhrV2\Repositories\Mcmc;

use Carbon\Carbon;
use IhrV2\Contracts\Attendance\AttendanceInterface;
use IhrV2\Contracts\Email\EmailInterface;
use IhrV2\Contracts\Leave\LeaveInterface;
use IhrV2\Contracts\Mcmc\DbMcmcInterface;
use IhrV2\Contracts\Mcmc\McmcInterface;
use IhrV2\Contracts\User\UserInterface;

class McmcRepository implements McmcInterface {
    protected $db_mcmc_repo;
    protected $leave_repo;
    protected $att_repo;
    protected $email_repo;
    protected $user_repo;

    public function __construct(DbMcmcInterface $db_mcmc_repo, LeaveInterface $leave_repo, AttendanceInterface $att_repo, EmailInterface $email_repo, UserInterface $user_repo) {
        $this->db_mcmc_repo = $db_mcmc_repo;
        $this->leave_repo = $leave_repo;
        $this->att_repo = $att_repo;
        $this->email_repo = $email_repo;
        $this->user_repo = $user_repo;
    }

    // get token id & path
    // -------------------
    public function getTokenAndPath() {
        $url = env('AGG_URL');
        $username = env('AGG_USERNAME');
        $password = env('AGG_PASSWORD');
        $path = $url . "HttpAuthentication/?"; // not HttpAttentication!!!!!!!
        $auth = "login?user[UserName]=$username&user[Password]=$password";
        $encode = base64_encode($auth);
        $final = $path . $encode;
        $result = \Httpful\Request::get($final)->send();
        $token = $result->body->xpath('Guid')[0];

        // return token and path
        // ---------------------
        return array('token' => $token, 'path' => $url);
    }

    // retrieve data from the api
    // --------------------------
    public function getCallBack($name) {

        // get token and path
        // ------------------
        $get = $this->getTokenAndPath();

        // execute the api
        // ---------------
        $tkn = $get['token'];
        $url = $get['path'] . 'data/?';
        $value = "select/mcmccims/$name:*?" . '$token=' . "'" . $tkn . "'";
        $encode = base64_encode($value);
        $final = $url . $encode;
        $result = \Httpful\Request::get($final)->expectsJson()->send();
        return $result;
    }

    public function getSiteWithStatePhase() {
        return \IhrV2\Models\Site::with(array('PhaseName', 'StateName'))->get();
    }

    public function getAggSiteBySitecode($sitecode) {
        return \IhrV2\Models\AggSiteDetail::where('REF_ID', $sitecode)->first();
    }

    public function getAggStaffDetailInactive($sitecode, $icno, $position) {
        $q = \IhrV2\Models\AggStaffDetail::where('PI1M_REFID', $sitecode)
            ->where('Staff_IC', $icno)
            ->where('position', $position)
            ->where('status', 'INACTIVE')
            ->first();
        return $q;
    }

    public function getAggStaffDetail($date) {

        // get today created_at and send date is null only
        // -----------------------------------------------
        $q = \IhrV2\Models\AggStaffDetail::whereDate('created_at', '=', $date)
            ->where('Send_Date', '=', null)

            // $q = \IhrV2\Models\AggStaffDetail::where('Send_Date', '=', null)
            // ->where('Error_Message', '=', null)

            // $date = Carbon::yesterday()->format('Y-m-d');
            // $q = \IhrV2\Models\AggStaffDetail::whereDate('created_at', '=', $date)

            ->get();
        return $q;
    }

    public function getAggStaffAttendance($date) {
        $q = \IhrV2\Models\AggStaffAttendance::whereDate('Date', '=', $date)
            ->where('Send_Date', '=', null)
            // $q = \IhrV2\Models\AggStaffAttendance::whereDate('updated_at', '=', $date)
            // ->orWhere('Send_Date', '=', null)
            // ->take(2) // testing records
            ->get();
        return $q;
    }

    public function getAttendanceRemark($i) {
        // dd($i->att_mykad_ic);
        $date = Carbon::parse($i->att_log_time)->format('Y-m-d');
        $q = \IhrV2\Models\AttendanceRemark::where('icno', $i->att_mykad_ic)
            ->where('sitecode', $i->location_id)
            ->whereDate('date', '=', $date)
            ->where('active', 1)
            ->with(array('RemarkName'))
            ->first();
        return $q;
    }

    public function getAggStaffAttendanceByICDate($i) {
        $q = \IhrV2\Models\AggStaffAttendance::where('PI1M_REFID', $i->location_id)
            ->where('Staff_IC', $i->att_mykad_ic)
            ->whereDate('Date', '=', $i->att_date)
            ->first();
        return $q;
    }

    public function getAttendanceProcessTable($date) {
        // $q = \IhrV2\Models\AttendanceProcessTable::whereDate('att_process_date', '=', $date)->get();
        $q = \IhrV2\Models\AttendanceProcessTable::whereDate('att_date', '=', $date)->get();
        return $q;
    }

    public function getAttendanceProcessTableBySitecode($sitecode) {
        $q = \IhrV2\Models\AttendanceProcessTable::where('location_id', $sitecode)->get();
        return $q;
    }

    // get attendance process table by sitecode and date range
    // -------------------------------------------------------
    public function getAttProcessBySdRange($from, $to, $sitecode) {
        $q = \IhrV2\Models\AttendanceProcessTable::where('location_id', $sitecode)
            ->whereBetween('att_date', array($from, $to))
            ->orderBy('att_date', 'asc')
            ->get();
        return $q;
    }

    // get attendance process table by date range
    // ------------------------------------------
    public function getAttProcessByDateRange($from, $to) {
        $q = \IhrV2\Models\AttendanceProcessTable::whereBetween('att_date', array($from, $to))
            ->orderBy('att_date', 'asc')
            ->get();
        return $q;
    }

    // get all public holiday
    // ----------------------
    public function getPublicHolidayList($date, $year) {
        $q = \IhrV2\Models\LeavePublicState::whereDate('date', '=', $date)
            ->where('year', $year)
            ->where('status', 1)
            ->get()
            ->toArray();
        return $q;
    }

    // get one public holiday
    // ----------------------
    public function getPublicHolidayOne($state_id, $date, $year) {
        $q = \IhrV2\Models\LeavePublicState::where(
            [
                'state_id' => $state_id,
                'year'     => $year,
                'status'   => 1
            ])
            ->whereDate('date', '=', $date)
            ->first();
        return $q;
    }

    // get all off day
    // ---------------
    public function getOffDateList($state_id, $types, $date, $year) {
        $q = \IhrV2\Models\LeaveOffDate::whereHas('ListStates',
            function ($q) use ($state_id) {
                $q->where('state_id', $state_id);
            })
            ->whereDate('off_date', '=', $date)
            ->whereIn('type_id', $types)
            ->where([
                'year'   => $year,
                'status' => 1
            ])
            ->groupBy('off_date')
            ->get()
            ->toArray();
        return $q;
    }

    // get off day of specific date
    // ----------------------------
    public function getOffDateListOne($state_id, $types, $date, $year) {
        $q = \IhrV2\Models\LeaveOffDate::whereHas('OffDayState',
            function ($s) use ($state_id) {
                $s->where('state_id', $state_id);
            })
            ->whereDate('off_date', '=', $date)
            ->whereIn('type_id', $types)
            ->where([
                'year'   => $year,
                'status' => 1
            ])
            ->first();
        return $q;
    }

    // get part timer
    // --------------
    public function getPartTimerList() {
        $q = \IhrV2\User::whereHas('UserLatestJob',
            function ($x) {
                $x->where('position_id', 84);
            })
            ->with(array('SiteNameInfo'))
            ->where(['group_id' => 3, 'status' => 1])
            ->where('sitecode', 'NOT LIKE', '%L0%')
            ->whereNotIn('sitecode', array('X01C001'))            
            ->get()
            ->toArray();
        return $q;
    }

    // get approved leave
    // ------------------
    public function getLeaveDateList($date) {
        // $q = \IhrV2\Models\LeaveDate::whereHas('LeaveHistoryOne', function($i) {
        //     $i->where('status', 2);
        //     $i->where('flag', 1);
        // })
        // ->with(array('LeaveApproveAnnual'))
        $q = \IhrV2\Models\LeaveDate::with(array('LeaveApproveAnnual'))
            ->whereDate('leave_date', '=', $date)
            ->where([
                'leave_type' => 1, // available
                'status'     => 1
            ])
            ->get()
            ->toArray();
        return $q;
    }

    public function getRemarkManualList($date) {
        $q = \IhrV2\Models\AttendanceRemark::with(
            array(
                'AttApproveStatus',
                'AttStatusName',
                'AttOtherName'
            ))
            ->whereDate('date', '=', $date)
            ->where('active', 1)
            ->get()
            ->toArray();
        return $q;
    }

    // lists of active site supervisor (active job and contract)
    // ---------------------------------------------------------
    public function getUserStaffActive() {
        $q = \IhrV2\User::with(
            array(
                'UserLatestJob',
                'UserLatestContract',
                'SiteNameInfo'
            ))
            ->where([
                'group_id' => 3,
                'status'   => 1
            ])
            ->where('sitecode', 'NOT LIKE', '%L0%')
            ->whereNotIn('sitecode', array('X01C001'))
            // ->take(10)->get()->toArray();
            ->get()
            ->toArray();
        return $q;
    }

    // get user by sitecode
    // --------------------
    public function getUserAttBySitecode($sitecode) {
        $q = \IhrV2\User::with(
            array(
                'UserLatestJob',
                'UserLatestContract',
                'SiteNameInfo'
            ))
            ->where([
                'sitecode' => $sitecode,
                'group_id' => 3,
                'status'   => 1
            ])
            ->where('sitecode', 'NOT LIKE', '%L0%') // exclude site PJL
            ->whereNotIn('sitecode', array('X01C001'))            
            ->get()
            ->toArray();
        return $q;
    }

    // get who transfer it today only
    // date att_timestamp probably have more than 1 date
    // need to use get() instead first()
    // -------------------------------------------------
    public function getAttendanceTime($i, $date) {
        // $order = ($type == 0) ? 'asc' : 'desc';
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', \DB::raw('MIN(att_log_time) as time_in'), \DB::raw('MAX(att_log_time) as time_out'))
            ->where('att_mykad_ic', $i->icno)
            ->where('location_id', $i->sitecode)
        // ->where('att_record_type', $type) // 0 = in | 1 = out
        // ->whereDate('att_timestamp', '=', $date)
        ->whereDate('att_log_time', '=', $date)
        ->groupBy('att_mykad_ic', 'location_id')
        // ->orderBy('att_log_time', $order)
        ->first();
        return $q;
    }

    public function getAttendanceTimeIn($i, $date) {
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', \DB::raw('MIN(att_log_time) as log_time'))
            ->where('att_mykad_ic', $i->icno)
            ->where('location_id', $i->sitecode)
            ->where('att_record_type', 0)
            ->whereDate('att_log_time', '=', $date)
            ->groupBy('att_mykad_ic', 'location_id')
            ->first();
        return $q;
    }

    public function getAttendanceTimeInSitecode($i) {
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', \DB::raw('MIN(att_log_time) as log_time'))
            ->where('att_mykad_ic', $i->icno)
            ->where('location_id', $i->sitecode)
            ->where('att_record_type', 0)
            ->groupBy('att_mykad_ic', 'location_id')
            ->first();
        return $q;
    }

    public function getAttendanceTimeOut($i, $date) {
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', \DB::raw('MAX(att_log_time) as log_time'))
            ->where('att_mykad_ic', $i->icno)
            ->where('location_id', $i->sitecode)
            ->where('att_record_type', 1)
            ->whereDate('att_log_time', '=', $date)
            ->groupBy('att_mykad_ic', 'location_id')
            ->first();
        return $q;
    }

    public function getAttendanceTimeOutSitecode($i) {
        $q = \IhrV2\Models\Attendance::select('att_mykad_ic', 'location_id', \DB::raw('MAX(att_log_time) as log_time'))
            ->where('att_mykad_ic', $i->icno)
            ->where('location_id', $i->sitecode)
            ->where('att_record_type', 1)
            ->groupBy('att_mykad_ic', 'location_id')
            ->first();
        return $q;
    }

    public function getStateID($sitecode) {
        $state = \IhrV2\Models\Site::where('code', $sitecode)->first();
        return $state->state_id;
    }

    public function getPublicHoliday($date, $state_id) {
        $year = Carbon::createFromFormat('Y-m-d', $date)->format('Y');
        $q = \IhrV2\Models\LeavePublicState::where('year', '=', $year)
            ->whereDate('date', '=', $date)
            ->where('state_id', $state_id)
            ->where('status', 1)
            ->first();
        return $q;
    }

    public function getUserJob($icno) {
        $q = \IhrV2\User::where('icno', $icno)
            ->with(array('UserLatestJob'))
            ->first();
        return $q;
    }

    public function getUserJobActive($icno, $sitecode) {
        $q = \IhrV2\User::with(array('UserLatestJob'))
            ->where([
                'icno'     => $icno,
                'sitecode' => $sitecode,
                'status'   => 1
            ])
            ->first();
        return $q;
    }

    public function getCheckProcessTable($i, $date) {
        $q = \IhrV2\Models\AttendanceProcessTable::where('att_mykad_ic', $i->att_mykad_ic)
            ->where('location_id', $i->location_id)
            ->whereDate('att_date', '=', $date)
            ->first();
        return $q;
    }

    public function getCheckAttProcessTable($icno, $sitecode, $date) {
        $q = \IhrV2\Models\AttendanceProcessTable::where('att_mykad_ic', $icno)
            ->where('location_id', $sitecode)
            ->whereDate('att_date', '=', $date)
            ->first();
        return $q;
    }

    public function getCheckProcessTableEmpty($icno, $sitecode, $date) {
        $q = \IhrV2\Models\AttendanceProcessTable::where('att_mykad_ic', $icno)
            ->where('location_id', $sitecode)
            ->whereDate('att_date', '=', $date)
            ->first();
        return $q;
    }

    public function getCheckRemarkBoth($arr) {

        // initial value
        // -------------
        $remark = null;
        $icno = $arr[0];
        $sitecode = $arr[1];
        $date = $arr[2];
        $in = $arr[3];
        $out = $arr[4];

        // in and out have value
        // ---------------------
        if ($in != '0000-00-00 00:00:00' && $out != '0000-00-00 00:00:00') {
            $remark = 'WORKING';
        }

        // in has value but out empty
        // --------------------------
        else if ($in != '0000-00-00 00:00:00' && $out == '0000-00-00 00:00:00') {
            $remark = 'INCOMPLETE';
        }

        // in empty but out has value
        // --------------------------
        else if ($in == '0000-00-00 00:00:00' && $out != '0000-00-00 00:00:00') {
            $remark = 'INCOMPLETE';
        }

        // in and out is empty
        // -------------------
        else {
            $remark = null;
        }
        return $remark;
    }

    public function getCheckRemark($x, $y, $r = null) {
        if ($x != '0000-00-00 00:00:00' && $y != '0000-00-00 00:00:00') {
            $remark = 'WORKING';
        } else if ($x != '0000-00-00 00:00:00' && $y == '0000-00-00 00:00:00') {
            $remark = 'INCOMPLETE';
        } else if ($x == '0000-00-00 00:00:00' && $y != '0000-00-00 00:00:00') {
            $remark = 'INCOMPLETE';
        }

        // if both time is empty get remark from others
        // --------------------------------------------
        else {
            if (!empty($r)) {
                $remark = $r;
            } else {
                $remark = 'TBU';
            }
        }
        return $remark;
    }

    public function getCheckTime($time) {
        if (empty($time)) {
            $q = '0000-00-00 00:00:00';
        } else {
            $q = $time;
        }
        return $q;
    }

    // get off day lists
    // -----------------
    public function getOffDay($state_id, $types, $date) {

        // get year
        // --------
        $year = Carbon::createFromFormat('Y-m-d', $date)->format('Y');

        // get date off day
        // ----------------
        $q = \IhrV2\Models\LeaveOffDate::whereHas('ListStates',
            function ($x) use ($state_id) {
                $x->where('state_id', $state_id);
            })
            ->where('year', $year)
            ->whereIn('type_id', $types)
            ->whereDate('off_date', '=', $date)
            ->where('status', 1)
            ->first();
        return $q;
    }

    public function getAttendanceDateToProcess() {
        $query = \IhrV2\Models\AggDate::where('staff_attendance', 0)
            ->where('process', 0)
            ->orderBy('date', 'desc')
            ->first();
        return $query;
    }

    public function getAttendanceDateToExport() {
        $query = \IhrV2\Models\AggDate::where('staff_attendance', 1)
            ->where('process', 1)
            ->where('export', 0)
            ->orderBy('date', 'desc')
            ->first();
        return $query;
    }

    public function getAttendanceDateToTransfer() {
        $query = \IhrV2\Models\AggDate::where('staff_attendance', 1)
            ->where('process', 1)
            ->where('export', 1)
            ->where('transfer', 0)
            ->orderBy('date', 'desc')
            ->first();
        return $query;
    }

    public function getCheckAggDate($date) {
        $query = \IhrV2\Models\AggDate::whereDate('date', '=', $date)
            ->first();
        return $query;
    }

    // check all remark attendance
    // off day/public holiday/part timer/approved leave/attendance remark
    // ------------------------------------------------------------------
    public function getRemarkAll($date) {

        // get all part timer
        // ------------------
        $part_timer = array();
        $pt = $this->getPartTimerList();
        if (count($pt) > 0) {
            foreach ($pt as $t) {
                $part_timer[] = $t['icno'];
            }
        }

        // check if have approved leave (annual/unplan/maternity)
        // ------------------------------------------------------
        $user_leave = array();
        $maternity_leave = array();
        $leaves = $this->getLeaveDateList($date);

        if (count($leaves) > 0) {
            foreach ($leaves as $l) {

                // leave is approved
                // -----------------
                if (!empty($l['leave_approve_annual'])) {

                    // annual / unplan leave
                    // ---------------------
                    if (in_array($l['leave_approve_annual']['leave_type_id'], array(1, 13))) {
                        $user_leave[] = $l['user_id'];
                    }

                    // maternity leave
                    // ---------------
                    if ($l['leave_approve_annual']['leave_type_id'] == 8) {
                        $maternity_leave[] = $l['user_id'];
                    }
                }
            }
        }

        // check if have attendance remarks
        // --------------------------------
        $manual_remark = array();
        $manual = $this->getRemarkManualList($date);
        if (count($manual) > 0) {
            foreach ($manual as $m) {

                // remark is approved
                // ------------------
                if (!empty($m['att_approve_status'])) {

                    // get remark name
                    // ---------------
                    if (!empty($m['att_status_name'])) {
                        $remark_name = $m['att_status_name']['name'];
                    }

                    // get other remark name
                    // ---------------------
                    if (!empty($m['att_other_name'])) {
                        $remark_name = $m['att_other_name']['name'];
                    }

                    // get icno and remark name
                    // ------------------------
                    $manual_remark[] = array('icno' => $m['icno'], 'remark' => $remark_name);
                }
            }
        }

        // return value
        // ------------
        return array(
            'part_timer'      => $part_timer,
            'user_leave'      => $user_leave,
            'maternity_leave' => $maternity_leave,
            'manual_remark'   => $manual_remark
        );
    }

    // get attendance in and out
    // -------------------------
    public function getAttInOut($icno, $sitecode, $date, $remark_1st) {

        // initialize variables
        // --------------------
        $insert = 0;
        $update = 0;
        $fail = 0;

        // reset on 1st remark of each user
        // --------------------------------
        $remark = null;
        $remark_2nd = null;

        // check if sitecode and icno is not empty
        // ---------------------------------------
        if (!empty($icno) && !empty($sitecode)) {

            // get in and out
            // --------------
            $att = $this->att_repo->getAttendanceByDate($icno, $sitecode, $date);

            // have record
            // -----------
            if (count($att) > 0) {

                // initial time value
                // ------------------
                $in = null;
                $out = null;

                // get in and out
                // --------------
                foreach ($att as $a) {

                    // check if have time in
                    // ---------------------
                    if ($a['att_record_type'] == 0) {
                        $in = $a['att_in'];
                    }

                    // check if have time out
                    // ----------------------
                    if ($a['att_record_type'] == 1) {
                        $out = $a['att_out'];
                    }
                }

                // check in and out
                // ----------------
                $in = $this->getCheckTime($in);
                $out = $this->getCheckTime($out);

                // set array
                // ---------
                $compare = $this->getCompareTime($in, $out);
                $arr = array($icno, $sitecode, $date, $compare[0], $compare[1]);

                // check remark working or incomplete
                // ----------------------------------
                $remark_2nd = $this->getCheckRemarkBoth($arr);

                // if no remark on 2nd get remark from 1st
                // ---------------------------------------
                if (empty($remark_2nd)) {
                    $remark = $remark_1st;
                }

                // working or incomplete
                // ---------------------
                else {
                    $remark = $remark_2nd;
                }

                // check if record already exist
                // -----------------------------
                $check = $this->getCheckAttProcessTable($icno, $sitecode, $date);

                // insert process table
                // --------------------
                if (empty($check)) {

                    // new record
                    // ----------
                    $this->db_mcmc_repo->dbInsertProcessTableBoth($arr, $remark);
                    $insert++;
                }

                // update process table
                // --------------------
                else {

                    // update process table
                    // --------------------
                    $this->db_mcmc_repo->dbUpdateProcessTableBoth($arr, $remark);
                    $update++;
                }
            }

            // no record attendance
            // --------------------
            else {

                // check remark
                // ------------
                $remark = $remark_1st;

                // check record at process table
                // -----------------------------
                $check = $this->getCheckAttProcessTable($icno, $sitecode, $date);

                // no record yet
                // -------------
                if (empty($check)) {

                    // insert new process data without time in / out
                    // ---------------------------------------------
                    $i = array('icno' => $icno, 'sitecode' => $sitecode);
                    $this->db_mcmc_repo->dbInsertProcessEmptyBoth($i, $date, $remark);
                    $insert++;
                }

                // have record
                // -----------
                else {

                    // update empty process table
                    // --------------------------
                    $this->db_mcmc_repo->dbUpdateProcessEmptyBoth($check, $remark);
                    $update++;
                }
            }
        }

        // icno or sitecode is empty
        // -------------------------
        else {
            $fail++;
        }

        // return values
        // -------------
        $data = array(
            'insert' => $insert,
            'update' => $update,
            'fail'   => $fail
        );
        return $data;
    }

    // compare time in and time out
    // ----------------------------
    public function getCompareTime($in, $out) {

        // if only in have value reset it to zero
        // --------------------------------------
        if ($in != '0000-00-00 00:00:00' && $out == '0000-00-00 00:00:00') {
            $time_in = '0000-00-00 00:00:00';
            $time_out = $out;
        } else {
            $time_in = $in;
            $time_out = $out;
        }
        return array($time_in, $time_out);
    }

    // deep process of process staff attendance
    // ----------------------------------------
    public function getProcessAtt($user, $date) {

        // step to set status attendance (it check all 5 steps)
        // ----------------------------------------------------
        // step 1 - check public holiday
        // step 2 - check off day
        // step 3 - check if part timer
        // step 4 - check if have approved annual/unplan/maternity
        // step 5 - check if have approved attendance remark
        // step 6 - return last remark

        // initialize variables
        // --------------------
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;
        $xsite = 0;

        // get year from date
        // ------------------
        $year = Carbon::parse($date)->format('Y');

        // check all attendance remarks
        // ----------------------------
        $chk_remark = $this->getRemarkAll($date);

        // loop user record
        // ----------------
        foreach ($user as $i) {
            $total++;

            // set remark
            // ----------
            $remark_1st = null;

            // get state id (code)
            // -------------------
            $state_id = null;
            $state = $this->leave_repo->getUserStateID($i['sitecode']);

            // site exist
            // ----------
            if (!empty($state)) {
                $state_id = $state->state_id;
            }

            // have state id
            // -------------
            if (!empty($state_id)) {

                // check site record
                // -----------------
                if (!empty($i['site_name_info'])) {

                    // check public holiday
                    // --------------------
                    $ph = $this->getPublicHolidayOne($state_id, $date, $year);

                    // have record
                    // -----------
                    if (!empty($ph)) {
                        $remark_1st = 'PUBLIC HOLIDAY';
                    }
                }

                // check user job
                // --------------
                if (!empty($i['user_latest_job'])) {

                    // get type id off day
                    // -------------------
                    $m = $this->leave_repo->getOffDayID(4);
                    $am = $this->leave_repo->getOffDayID(5);

                    // check if date is off day manager
                    // --------------------------------
                    if ($i['user_latest_job']['position_id'] == 4) {
                        $od_m = $this->getOffDateListOne($state_id, $m, $date, $year);
                        if (!empty($od_m)) {
                            $remark_1st = 'OFF DAY';
                        }
                    }

                    // check if date is off day assistant manager|it supervisor
                    // --------------------------------------------------------
                    if (in_array($i['user_latest_job']['position_id'], array(5, 6))) {
                        $od_am = $this->getOffDateListOne($state_id, $am, $date, $year);
                        if (!empty($od_am)) {
                            $remark_1st = 'OFF DAY';
                        }
                    }
                }

                // check if part timer
                // -------------------
                if (count($chk_remark['part_timer']) > 0) {
                    if (in_array($i['icno'], $chk_remark['part_timer'])) {
                        $remark_1st = 'PART TIMER';
                    }
                }

                // check if date is approved annual /unplan leave
                // ----------------------------------------------
                if (count($chk_remark['user_leave']) > 0) {
                    if (in_array($i['id'], $chk_remark['user_leave'])) {
                        $remark_1st = 'AL-EL';
                    }
                }

                // check if date is approved maternity leave
                // -----------------------------------------
                if (count($chk_remark['maternity_leave']) > 0) {
                    if (in_array($i['id'], $chk_remark['maternity_leave'])) {
                        $remark_1st = 'MATERNITY LEAVE';
                    }
                }

                // check if date have attendance remarks
                // get status name if approved
                // -------------------------------------
                if (count($chk_remark['manual_remark']) > 0) {
                    foreach ($chk_remark['manual_remark'] as $mr) {
                        if ($i['icno'] == $mr['icno']) {
                            $remark_1st = $mr['remark'];
                        }
                    }
                }

                // if remark still empty
                // ---------------------
                if (empty($remark_1st)) {
                    $remark_1st = 'TBU';
                }

                // check time in & time out
                // ------------------------
                $att = $this->getAttInOut($i['icno'], $i['sitecode'], $date, $remark_1st);

                // track total insert
                // ------------------
                if ($att['insert'] > 0) {
                    $insert += $att['insert'];
                }

                // track total update
                // ------------------
                if ($att['update'] > 0) {
                    $update += $att['update'];
                }

                // track total fail
                // ----------------
                if ($att['fail'] > 0) {
                    $fail += $att['fail'];
                }
            }

            // count the fail
            // --------------
            else {
                $xsite++;
            }
        }

        // return values
        // -------------
        $data = array(
            'total'  => $total,
            'insert' => $insert,
            'update' => $update,
            'fail'   => $fail,
            'xsite'  => $xsite
        );
        return $data;
    }

    // process staff attendance by date
    // --------------------------------
    public function getProAttDate($date) {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;
        $xsite = 0;

        // get all staff site supervisor (active only)
        // -------------------------------------------
        $query = $this->getUserStaffActive();

        // insert attendance_process_table
        // -------------------------------
        if (!empty($query)) {

            // process the record
            // ------------------
            $exe = $this->getProcessAtt($query, $date);

            // update values
            // -------------
            $total = $exe['total'];
            $insert = $exe['insert'];
            $update = $exe['update'];
            $fail = $exe['fail'];
            $xsite = $exe['xsite'];
        }

        // set data
        // --------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'sitecode'   => null,
            'start_date' => $date,
            'end_date'   => $date,
            'total'      => $total,
            'insert'     => $insert,
            'update'     => $update,
            'fail'       => $fail,
            'xsite'      => $xsite
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcProAttendance($data);
        return $data;
    }

    // process staff attendance by sitecode and date
    // ---------------------------------------------
    public function getProAttSiteDate($sitecode, $start_date, $end_date) {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;
        $xsite = 0;

        // convert string to date
        // ----------------------
        $from = Carbon::createFromFormat('Y-m-d', $start_date);
        $to = Carbon::createFromFormat('Y-m-d', $end_date);

        // get all dates
        // -------------
        $duration = $this->leave_repo->DateRange($from, $to);

        // loop all dates
        // --------------
        foreach ($duration as $date) {

            // get all staff site supervisor (active only)
            // -------------------------------------------
            $query = $this->getUserAttBySitecode($sitecode);

            // insert attendance_process_table
            // -------------------------------
            if (!empty($query)) {

                // process the record
                // ------------------
                $exe = $this->getProcessAtt($query, $date);
                $total += $exe['total'];
                $insert += $exe['insert'];
                $update += $exe['update'];
                $fail += $exe['fail'];
                $xsite += $exe['xsite'];
            }
        }

        // return result
        // -------------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'sitecode'   => $sitecode,
            'start_date' => $start_date,
            'end_date'   => $end_date,
            'total'      => $total,
            'insert'     => $insert,
            'update'     => $update,
            'fail'       => $fail,
            'xsite'      => $xsite
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcProAttendance($data);
        return $data;
    }

    // process staff attendance for backdated days
    // -------------------------------------------
    public function getProAttBackdated($days) {

        // get start date
        // --------------
        $start = Carbon::now()->subDays($days + 1)->format('Y-m-d');

        // get end date
        // minus 2 because yesterday already process
        // -----------------------------------------
        $end = Carbon::now()->subDays(2)->format('Y-m-d');

        // process the attendance
        // ----------------------
        $process = $this->getProAttStartEnd($start, $end);

        // export the attendance
        // ---------------------
        $export = $this->getExpAttStartEnd($start, $end);

        // return result
        // -------------
        return true;
    }

    // process staff attendance by date range
    // --------------------------------------
    public function getProAttStartEnd($start_date, $end_date) {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;
        $xsite = 0;

        // convert string to date
        // ----------------------
        $from = Carbon::createFromFormat('Y-m-d', $start_date);
        $to = Carbon::createFromFormat('Y-m-d', $end_date);

        // get all dates
        // -------------
        $duration = $this->leave_repo->DateRange($from, $to);

        // loop all dates
        // --------------
        foreach ($duration as $date) {

            // get all staff site supervisor (active only)
            // -------------------------------------------
            $query = $this->getUserStaffActive();

            // insert attendance_process_table
            // -------------------------------
            if (!empty($query)) {

                // process the record
                // ------------------
                $exe = $this->getProcessAtt($query, $date);

                // update values
                // -------------
                $total += $exe['total'];
                $insert += $exe['insert'];
                $update += $exe['update'];
                $fail += $exe['fail'];
                $xsite += $exe['xsite'];
            }
        }

        // return result
        // -------------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'sitecode'   => null,
            'start_date' => $start_date,
            'end_date'   => $end_date,
            'total'      => $total,
            'insert'     => $insert,
            'update'     => $update,
            'fail'       => $fail,
            'xsite'      => $xsite
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcProAttendance($data);
        return $data;
    }

    // deep process of export staff attendance
    // ---------------------------------------
    public function getExportAtt($i) {

        // initialize variables
        // --------------------
        $insert = 0;
        $update = 0;
        $fail = 0;
        $diff = 0;
        $exclude = 0; // exclude specific site

        // check site to exclude (ulu tebrau & sg tiram ex danawa)
        // -------------------------------------------------------
        // if (in_array($i->location_id, array('J11C001', 'J11C002'))) {
        //     $exclude = 1;
        // }

        // sitecode and icno must have value
        // ---------------------------------
        if (!empty($i->location_id) && !empty($i->att_mykad_ic) && $exclude != 1) {

            // check if record already exist
            // -----------------------------
            $agg = $this->getAggStaffAttendanceByICDate($i);

            // insert record
            // -------------
            if (empty($agg)) {

                // check datetime in
                // -----------------
                if ($i->att_log_time_in == '0000-00-00 00:00:00') {
                    $in = $i->att_date; // fill empty datetime with date only
                } else {
                    $in = $i->att_log_time_in;
                }

                // check datetim out
                // -----------------
                if ($i->att_log_time_out == '0000-00-00 00:00:00') {
                    $out = $i->att_date; // fill empty datetime with date only
                } else {
                    $out = $i->att_log_time_out;
                }

                // insert staff attendance @agg
                // ----------------------------
                $this->db_mcmc_repo->dbInsertAggStaffAttendance($i, $in, $out, $i->remark);
                $insert++;
            }

            // update record
            // -------------
            else {

                // check datetime in
                // -----------------
                if ($i->att_log_time_in == '0000-00-00 00:00:00') {
                    $in = $i->att_date; // fill empty datetime with date only
                } else {
                    $in = $i->att_log_time_in;
                }

                // check datetim out
                // -----------------
                if ($i->att_log_time_out == '0000-00-00 00:00:00') {
                    $out = $i->att_date; // fill empty datetime with date only
                } else {
                    $out = $i->att_log_time_out;
                }

                // check both remark
                // -----------------
                if ($i->remark != $agg->CheckIn_Status) {
                    $diff = 1;
                }

                // ensure the datetime has value
                // -----------------------------
                if ($i->att_log_time_in != '0000-00-00 00:00:00' || $i->att_log_time_out != '0000-00-00 00:00:00') {

                    // check if punch in and punch out
                    // -------------------------------
                    if (Carbon::parse($i->att_log_time_in)->ne(Carbon::parse($agg->CheckIn_Time)) || Carbon::parse($i->att_log_time_out)->ne(Carbon::parse($agg->CheckOut_Time))) {
                        $diff = 1;
                    }
                }

                // record have different
                // ---------------------
                if ($diff == 1) {

                    // update agg staff attendance
                    // ---------------------------
                    $this->db_mcmc_repo->dbUpdateAggStaffAttendance($i, $in, $out, $i->remark);
                    $update++;
                }
            }
        } else {
            $fail++;
        }

        // return values
        // -------------
        $data = array(
            'insert' => $insert,
            'update' => $update,
            'fail'   => $fail
        );
        return $data;
    }

    // export staff attendance by date
    // -------------------------------
    public function getExpAttDate($date) {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // check process table
        // -------------------
        $q = $this->getAttendanceProcessTable($date);

        // have record in process table
        // ----------------------------
        if (!empty($q)) {

            // get total
            // ---------
            $total = count($q);
            foreach ($q as $i) {

                // process export to agg
                // ---------------------
                $export = $this->getExportAtt($i);

                // track total
                // -----------
                if ($export['insert'] > 0) {
                    $insert += $export['insert'];
                }
                if ($export['update'] > 0) {
                    $update += $export['update'];
                }
                if ($export['fail'] > 0) {
                    $fail += $export['fail'];
                }
            }
        }

        // set values
        // ----------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'sitecode'   => null,
            'start_date' => $date,
            'end_date'   => $date,
            'total'      => $total,
            'insert'     => $insert,
            'update'     => $update,
            'fail'       => $fail
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcExpAttendance($data);
        return $data;
    }

    // export staff attendance by sitecode and date
    // --------------------------------------------
    public function getExpAttSiteDate($sitecode, $start_date, $end_date) {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // convert string to date
        // ----------------------
        $from = Carbon::createFromFormat('Y-m-d', $start_date)->format('Y-m-d');
        $to = Carbon::createFromFormat('Y-m-d', $end_date)->format('Y-m-d');

        // check process table
        // -------------------
        $q = $this->getAttProcessBySdRange($from, $to, $sitecode);

        // have record in process table
        // ----------------------------
        if (!empty($q)) {
            $total = count($q);
            foreach ($q as $i) {

                // process export to agg
                // ---------------------
                $export = $this->getExportAtt($i);

                // track total
                // -----------
                if ($export['insert'] > 0) {
                    $insert += $export['insert'];
                }
                if ($export['update'] > 0) {
                    $update += $export['update'];
                }
                if ($export['fail'] > 0) {
                    $fail += $export['fail'];
                }
            }
        }

        // set array
        // ---------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'sitecode'   => $sitecode,
            'start_date' => $start_date,
            'end_date'   => $end_date,
            'total'      => $total,
            'insert'     => $insert,
            'update'     => $update,
            'fail'       => $fail
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcExpAttendance($data);
        return $data;
    }

    // export staff attendance by start and end date
    // ---------------------------------------------
    public function getExpAttStartEnd($start_date, $end_date) {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;
        $fail = 0;

        // convert string to date
        // ----------------------
        $from = Carbon::createFromFormat('Y-m-d', $start_date)->format('Y-m-d');
        $to = Carbon::createFromFormat('Y-m-d', $end_date)->format('Y-m-d');

        // check process record
        // --------------------
        $q = $this->getAttProcessByDateRange($from, $to);

        // have record in process table
        // ----------------------------
        if (!empty($q)) {

            // get total
            // ---------
            $total = count($q);
            foreach ($q as $i) {

                // process export to agg
                // ---------------------
                $export = $this->getExportAtt($i);

                // track total
                // -----------
                if ($export['insert'] > 0) {
                    $insert += $export['insert'];
                }
                if ($export['update'] > 0) {
                    $update += $export['update'];
                }
                if ($export['fail'] > 0) {
                    $fail += $export['fail'];
                }
            }
        }

        // set array
        // ---------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'start_date' => $start_date,
            'end_date'   => $end_date,
            'sitecode'   => null,
            'total'      => $total,
            'insert'     => $insert,
            'update'     => $update,
            'fail'       => $fail
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcExpAttendance($data);
        return $data;
    }

    // export staff
    // ------------
    public function getExpStaff($status, $start, $end) {

        // get start date time
        // -------------------
        $now = date('Y-m-d H:i:s');

        // get staff active
        // ----------------
        if ($status == 1) {
            $q = $this->getStaffActive($start, $end);
        } 

        // get staff inactive
        // ------------------
        else {
            $q = $this->getStaffInactive($start, $end);
        }

        // export staff active
        // -------------------
        $exp = $this->getProExpStaff($q, $status);

        // return result
        // -------------
        $data = array(
            'start'  => $now,
            'total'  => $exp['total'],
            'insert' => $exp['insert'],
            'fail'   => $exp['fail']
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcExpStaff($data, $status);
        return $data;
    }

    // get active staff
    // ----------------
    public function getStaffActive($start, $end) {
        $q = \IhrV2\User::whereHas('UserLatestJob',
            function ($j) {
                $j->whereIn('position_id', array(4, 5, 6, 84));
            })
            ->where('sitecode', 'NOT LIKE', '%L0%') // exclude site PJL
            ->where('sitecode', 'NOT LIKE', 'X01C001') // exclude site PJL
          ->where([
              'group_id' => 3,
              'status'   => 1
          ])
          // ->whereDate('created_at', '>=', $start)
          // ->whereDate('created_at', '<=', $end)
          ->whereBetween('created_at', [$start, $end])          
          ->with(array('UserLatestJob'))
          ->get();
        return $q;
    }

    // get inactive staff
    // ------------------
    public function getStaffInactive($start, $end) {
        $q = \IhrV2\User::whereHas('UserInactiveJob',
            function ($j) {
                $j->whereIn('position_id', array(4, 5, 6, 84));
            })
            ->where('sitecode', 'NOT LIKE', '%L0%') // exclude site PJL
            ->where([
                'group_id' => 3,
                'status'   => 2
            ])
            // ->whereDate('updated_at', '>=', $start)
            // ->whereDate('updated_at', '>=', $end)
            ->whereBetween('updated_at', [$start, $end])
            ->with(array('UserInactiveJob'))
            ->get();
        return $q;
    }

    // deep process to export staff detail
    // -----------------------------------
    public function getProExpStaff($q, $type) {

        // set variables
        // -------------
        $total = 0;
        $insert = 0;
        $fail = 0;
        $invalid = 0;

        // have record
        // -----------
        if (!empty($q)) {

            // get total record
            // ----------------
            $total = count($q);

            // loop record
            // -----------
            foreach ($q as $i) {

                // check users again that active (case of inactive export)
                // -------------------------------------------------------
                $chk = \IhrV2\User::where(['icno' => $i->icno, 'status' => 1])->first();

                // case active or inactive and no active yet
                // -----------------------------------------
                if ($type == 1 || $type == 2 && empty($chk)) {

                    // check mobile no
                    // ---------------
                    if (empty($i->hpno)) {
                        if (!empty($i->telno1)) {
                            $no = $i->telno1;
                        } else {
                            if (!empty($i->telno2)) {
                                $no = $i->telno2;
                            } else {
                                $no = '-';
                                $invalid = 1;
                            }
                        }
                    } else {
                        $no = $i->hpno;
                    }

                    // check email
                    // -----------
                    if (empty($i->email)) {
                        if (!empty($i->work_email)) {
                            $email = $i->work_email;
                        } else {
                            if (!empty($i->personal_email)) {
                                $email = $i->personal_email;
                            } else {
                                $email = '-';
                                $invalid = 1;
                            }
                        }
                    } else {
                        $email = $i->email;
                    }

                    // check staff active
                    // ------------------
                    if ($type == 1) {

                        // check position
                        // --------------
                        if ($i->UserLatestJob) {
                            $position = strtoupper($i->UserLatestJob->PositionName->name);
                        } else {
                            $position = '-';
                            $invalid = 1;
                        }
                    }

                    // check staff inactive
                    // --------------------
                    else {

                        // check position
                        // --------------
                        if ($i->UserInactiveJob) {
                            $position = strtoupper($i->UserInactiveJob->PositionName->name);
                        } else {
                            $position = '-';
                            $invalid = 1;
                        }
                    }

                    // check name
                    // ----------
                    if (empty($i->name)) {
                        $name = '-';
                        $invalid = 1;
                    } else {
                        $name = $i->name;
                    }

                    // check status
                    // ------------
                    if ($i->status == 1) {
                        $status = 'ACTIVE';
                    } else {
                        $status = 'INACTIVE';
                    }

                    // have value
                    // ----------
                    if (!empty($i->sitecode) && !empty($i->icno)) {

                        // insert staff_detail (even already have previous record)
                        // -------------------------------------------------------
                        $this->db_mcmc_repo->dbInsertAggStaffDetail($i->sitecode, $i->icno, $name, $no, $email, $position, $status);
                        $insert++;
                    } 

                    // no value
                    // --------
                    else {
                        $fail++;
                    }
                }
            }
        }

        // return result
        // -------------
        $data = array(
            'total'  => $total,
            'insert' => $insert,
            'fail'   => $fail
        );
        return $data;
    }

    // export site detail
    // ------------------
    public function getExpSite() {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;

        // get site info
        // -------------
        $site = $this->getSiteWithStatePhase()->toArray();
        if (count($site) > 0) {

            // get total site
            // --------------
            $total = count($site);
            foreach ($site as $i) {

                // check state
                // -----------
                $state = null;
                if (!empty($i['state_name'])) {
                    $state = $i['state_name']['name'];
                }

                // check phase
                // -----------
                $phase = null;
                if (!empty($i['phase_name'])) {
                    $phase = $i['phase_name']['name'];
                }

                // check site_detail at aggreagator
                // --------------------------------
                $agg = $this->getAggSiteBySitecode($i['code']);

                // insert new record
                // -----------------
                if (empty($agg)) {
                    $insert++;

                    // insert site detail
                    // ------------------
                    $this->db_mcmc_repo->dbInsertAggSiteDetail($i, $state, $phase);
                    $insert++;
                }

                // update record
                // -------------
                else {
                    $this->db_mcmc_repo->dbUpdateAggSiteDetail($i, $state, $phase, $i['code']);
                    $update++;
                }
            }
        }

        // return result
        // -------------
        $data = array(
            'start'  => $start,
            'end'    => date('Y-m-d H:i:s'),
            'total'  => $total,
            'insert' => $insert,
            'update' => $update
        );

        // send email notification
        // -----------------------
        $this->email_repo->getMcmcExpSite($data);
        return $data;
    }

    // check attendance fail transfer
    // ------------------------------
    public function getChkAttFail($start, $end) 
    {
        // set variables
        // -------------
        $total = 0;

        // check mycomm staff attendance
        // -----------------------------
        $fail = $this->getAggAttFail($start, $end)->toArray();
        if (!empty($fail)) {
            $total = count($fail);
        }

        // set email body
        // --------------
        $data = array(
            'total' => $total,
            'start' => $start,
            'end'   => $end,
            'fail'  => $fail
        );

        // send email notification
        // -----------------------
        $this->email_repo->getEmailAggAttFail($data);
        return $data;
    }

    // check attendance fail transfer
    // ------------------------------
    public function getAggAttFail($start, $end) {
        $q = \IhrV2\Models\AggStaffAttendance::where(
            [
                'Send_Date' => null
            ])
            ->whereDate('Date', '>=', $start)
            ->whereDate('Date', '<=', $end)
            ->whereNotNull('Error_Message')
            ->orderBy('id', 'asc')
            ->get();
        return $q;
    }

    // reset exported staff attendance by date range
    // ---------------------------------------------
    public function getResetAttStartEnd($date_start, $date_end, $sitecode = null) {

        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;

        // get staff attendance
        // --------------------
        $q = \IhrV2\Models\AggStaffAttendance::whereBetween('Date', array($date_start, $date_end))
            ->where(function ($s) use ($sitecode) {
                if (!empty($sitecode)) {
                    $s->where('PI1M_REFID', $sitecode);
                }
            })
            ->count();

        // get total
        // ---------
        $total = $q;

        // have record
        // -----------
        if ($q > 0) {

            // update send_date and error_message
            // ----------------------------------
            $update = $this->db_mcmc_repo->dbUpdateAttDateRange($date_start, $date_end, $sitecode);
        }

        // return result
        // -------------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'date_start' => $date_start,
            'date_end'   => $date_end,
            'total'      => $total
        );

        // send email notification
        // -----------------------
        $this->email_repo->getAggAttReset($data);
        return $data;
    }

    // reset exported staff attendance by date
    // ---------------------------------------
    public function getResetAttDate($date) 
    {
        // set variables
        // -------------
        $start = date('Y-m-d H:i:s');
        $total = 0;
        $insert = 0;
        $update = 0;

        // get staff attendance
        // --------------------
        $q = \IhrV2\Models\AggStaffAttendance::whereDate('Date', '=', $date)
            ->count();

        // get total
        // ---------
        $total = $q;

        // have record
        // -----------
        if ($q > 0) {

            // update send_date and error_message
            // ----------------------------------
            $update = $this->db_mcmc_repo->dbUpdateAttDate($date);
        }

        // return result
        // -------------
        $data = array(
            'start'      => $start,
            'end'        => date('Y-m-d H:i:s'),
            'date_start' => $date,
            'date_end'   => $date,
            'total'      => $total
        );

        // send email notification
        // -----------------------
        $this->email_repo->getAggAttReset($data);
        return $data;
    }

}
