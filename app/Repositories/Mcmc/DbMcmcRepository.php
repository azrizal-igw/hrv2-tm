<?php namespace IhrV2\Repositories\Mcmc;

use Carbon\Carbon;
use IhrV2\Contracts\Mcmc\DbMcmcInterface;

class DbMcmcRepository implements DbMcmcInterface {

    // insert staff detail
    // -------------------
    public function dbInsertAggStaffDetail($sitecode, $icno, $name, $no, $email, $position, $status) {

        // trim white space
        // ----------------
        $contact_no = preg_replace('/\s+/', '', $no);
        $arr = array(
            'PI1M_REFID'       => trim(strtoupper($sitecode)),
            'SERVICE_PROVIDER' => 'TM',
            'Staff_IC'         => trim($icno),
            'Staff_Name'       => trim(strtoupper($name)),
            'Contact_Number'   => trim($contact_no),
            'Contact_Email'    => trim(strtoupper($email)),
            'Position'         => trim($position),
            'Status'           => $status,
            'Send_Date'        => null, // new record / updated record
            'Error_Message'    => null
        );
        $sav = new \IhrV2\Models\AggStaffDetail($arr);
        $sav->save();
        return true;
    }

    // update send date staff detail
    // -----------------------------
    public function dbUpdateSendDateAggStaffDetail($id) {
        $q = \IhrV2\Models\AggStaffDetail::where('id', $id)->update(array('Send_Date' => date('Y-m-d H:i:s')));
        return true;
    }

    // update error message staff detail
    // ---------------------------------
    public function dbUpdateErrorAggStaffDetail($i, $err) {
        $q = \IhrV2\Models\AggStaffDetail::where('Staff_IC', $i->Staff_IC)
            ->where('PI1M_REFID', $i->PI1M_REFID)
            ->update(array('Error_Message' => $err));
        return true;
    }

    // insert site detail
    // ------------------
    public function dbInsertAggSiteDetail($i, $state, $phase) {
        $arr = array(
            'REF_ID'           => $i['code'],
            'SITE_NAME'        => $i['full_name'],
            'STATE'            => $state,
            'ADDRESS1'         => $i['street1'],
            'ADDRESS2'         => $i['street2'],
            'POSTCODE'         => $i['postal_code'],
            'CITY'             => $i['city'],
            'SERVICE_PROVIDER' => 'TM',
            'PHASE'            => $phase,
            'BANDWIDTH'        => $i['bandwidth'],
            'TECHNOLOGY'       => $i['backhaul']
        );
        $sav = new \IhrV2\Models\AggSiteDetail($arr);
        $sav->save();
        return true;
    }

    // update site detail
    // ------------------
    public function dbUpdateAggSiteDetail($i, $state, $phase, $sitecode) {
        $val = array(
            'SITE_NAME'        => $i['full_name'],
            'STATE'            => $state,
            'ADDRESS1'         => $i['street1'],
            'ADDRESS2'         => $i['street1'],
            'POSTCODE'         => $i['postal_code'],
            'CITY'             => $i['city'],
            'SERVICE_PROVIDER' => 'TM',
            'PHASE'            => $phase,
            'BANDWIDTH'        => $i['bandwidth'],
            'TECHNOLOGY'       => $i['backhaul']
        );
        $q = \IhrV2\Models\AggSiteDetail::where('REF_ID', $sitecode)->update(array($val));
        return true;
    }

    // update send date attendance
    // ---------------------------
    public function dbUpdateSendDateAttendance($i) {
        $q = \IhrV2\Models\AggStaffAttendance::where('Staff_IC', $i->Staff_IC)
            ->where('PI1M_REFID', $i->PI1M_REFID)
            ->whereDate('Date', '=', $i->Date)
            ->update(array('Send_Date' => date('Y-m-d H:i:s')));
        return true;
    }

    // update error message attendance
    // -------------------------------
    public function dbUpdateErrorAttendance($att, $err) {
        $q = \IhrV2\Models\AggStaffAttendance::where('Staff_IC', $att->Staff_IC)
            ->where('PI1M_REFID', $att->PI1M_REFID)
            ->whereDate('Date', '=', $att->Date)
            ->update(array('Error_Message' => $err));
        return true;
    }

    // update error message empty attendance
    // -------------------------------------
    public function dbUpdateErrorEmptyAttendance($att) {
        $q = \IhrV2\Models\AggStaffAttendance::where('Staff_IC', $att->Staff_IC)
            ->where('PI1M_REFID', $att->PI1M_REFID)
            ->whereDate('Date', '=', $att->Date)
            ->update(array('Error_Message' => 'Either Sitecode or ICNO is empty'));
        return true;
    }

    // insert staff attendance
    // -----------------------
    public function dbInsertAggStaffAttendance($i, $in, $out, $remark) {
        $arr = array(
            'PI1M_REFID'       => strtoupper($i->location_id),
            'SERVICE_PROVIDER' => 'TM',
            'Staff_IC'         => strtoupper($i->att_mykad_ic),
            'CheckIn_Date'     => $i->att_date,
            'CheckIn_Time'     => $in,
            'CheckOut_Time'    => $out,
            'CheckIn_Status'   => strtoupper($remark),
            'Date'             => $i->att_date,
            'Send_Date'        => null,
            'Error_Message'    => null
        );
        $sav = new \IhrV2\Models\AggStaffAttendance($arr);
        $sav->save();
        return true;
    }

    // reset staff attendance by date range
    // ------------------------------------
    public function dbUpdateAttDateRange($start, $end, $sitecode) {
        $q = \IhrV2\Models\AggStaffAttendance::whereBetween('Date', array($start, $end))
            ->where(function ($s) use ($sitecode) {
                if (!empty($sitecode)) {
                    $s->where('PI1M_REFID', $sitecode);
                }
            })
            ->update(array(
                'Send_Date'     => null,
                'Error_Message' => null
            ));
        return true;
    }

    // reset staff attendance by date
    // ------------------------------
    public function dbUpdateAttDate($date) {
        $q = \IhrV2\Models\AggStaffAttendance::whereDate('Date', '=', $date)
            ->update(array(
                'Send_Date'     => null,
                'Error_Message' => null
            ));
        return true;
    }

    // update staff attendance
    // -----------------------
    public function dbUpdateAggStaffAttendance($i, $in, $out, $remark) {
        $q = \IhrV2\Models\AggStaffAttendance::where('Staff_IC', $i->att_mykad_ic)
            ->where('PI1M_REFID', $i->location_id)
            ->whereDate('Date', '=', $i->att_date)
            ->update(array(
                'CheckIn_Time'   => $in,
                'CheckOut_Time'  => $out,
                'CheckIn_Status' => $remark,
                'Send_Date'      => null,
                'Error_Message'  => null
            ));
        return true;
    }

    // insert process table with in and out
    // ------------------------------------
    public function dbInsertProcessTableBoth($i, $remark) {
        $arr = array(
            'att_mykad_ic'     => $i[0],
            'att_password'     => null,
            'location_id'      => $i[1],
            'att_record_type'  => null,
            'att_input_type'   => null,
            'att_log_time_in'  => $i[3],
            'att_log_time_out' => $i[4],
            'att_userstamp'    => null,
            'ip_add'           => null,
            'att_timestamp'    => null,
            'att_sys_status'   => null,
            'remark'           => $remark,
            'att_date'         => $i[2],
            'att_process_date' => Carbon::now()->format('Y-m-d'),
            'created_at'       => Carbon::now(),
            'updated_at'       => Carbon::now()
        );
        $sav = new \IhrV2\Models\AttendanceProcessTable($arr);
        $sav->save();
        return true;
    }

    // update process table
    // --------------------
    public function dbUpdateProcessTableBoth($arr, $remark) {
        $q = \IhrV2\Models\AttendanceProcessTable::where(
            [
                'att_mykad_ic' => $arr[0],
                'location_id'  => $arr[1]
            ])
            ->whereDate('att_date', '=', Carbon::parse($arr[2])->format('Y-m-d'))
            ->update(array(
                'att_log_time_in'  => $arr[3],
                'att_log_time_out' => $arr[4],
                'remark'           => $remark,
                'att_process_date' => Carbon::now()->format('Y-m-d'),
                'updated_at'       => Carbon::now()
            ));
        return true;
    }

    // insert process table
    // --------------------
    public function dbInsertProcessTable($i, $date, $type, $remark) {
        $in = ($type == 0) ? $i->log_time : '0000-00-00 00:00:00';
        $out = ($type == 1) ? $i->log_time : '0000-00-00 00:00:00';
        $arr = array(
            'att_mykad_ic'     => $i->att_mykad_ic,
            'att_password'     => null,
            'location_id'      => strtoupper($i->location_id),
            'att_record_type'  => null,
            'att_input_type'   => null,
            'att_log_time_in'  => $in,
            'att_log_time_out' => $out,
            'att_userstamp'    => null,
            'ip_add'           => null,
            'att_timestamp'    => null,
            'att_sys_status'   => null,
            'remark'           => $remark,
            'att_date'         => $date,
            'att_process_date' => date('Y-m-d'),
            'created_at'       => date('Y-m-d H:i:s'),
            'updated_at'       => date('Y-m-d H:i:s')
        );
        $sav = new \IhrV2\Models\AttendanceProcessTable($arr);
        $sav->save();
        return true;
    }

    // update process table
    // --------------------
    public function dbUpdateProcessTable($i, $remark, $type) {
        if ($type == 0) {
            $col = 'att_log_time_in';
        } else if ($type == 1) {
            $col = 'att_log_time_out';
        }
        $q = \IhrV2\Models\AttendanceProcessTable::where('att_mykad_ic', $i->att_mykad_ic)
            ->where('location_id', $i->location_id)
            ->whereDate('att_date', '=', Carbon::parse($i->log_time)->format('Y-m-d'))
            ->update(array(
                $col         => $i->log_time,
                'remark'     => $remark,
                'updated_at' => date('Y-m-d H:i:s')
            ));
        return true;
    }

    // update process table remark
    // ---------------------------
    public function dbUpdateProcessTableRemark($i, $remark) {
        $q = \IhrV2\Models\AttendanceProcessTable::where('att_mykad_ic', $i->att_mykad_ic)
            ->where('location_id', $i->location_id)
            ->whereDate('att_date', '=', Carbon::parse($i->att_date)->format('Y-m-d'))
            ->update(array(
                'remark'     => $remark,
                'updated_at' => date('Y-m-d H:i:s')
            ));
        return true;
    }

    // update process table in-complete
    // --------------------------------
    public function dbUpdateProcessTableInComplete($icno, $sitecode, $date, $remark) {
        $q = \IhrV2\Models\AttendanceProcessTable::where('att_mykad_ic', $icno)
            ->where('location_id', $sitecode)
            ->whereDate('att_date', '=', Carbon::parse($date)->format('Y-m-d'))
            ->update(array(
                'remark'     => $remark,
                'updated_at' => date('Y-m-d H:i:s')
            ));
        return true;
    }

    // insert process empty
    // --------------------
    public function dbInsertProcessEmptyBoth($i, $date, $remark) {
        $arr = array(
            'att_mykad_ic'     => $i['icno'],
            'att_password'     => null,
            'location_id'      => $i['sitecode'],
            'att_record_type'  => null,
            'att_input_type'   => null,
            'att_log_time_in'  => '0000-00-00 00:00:00',
            'att_log_time_out' => '0000-00-00 00:00:00',
            'att_userstamp'    => null,
            'ip_add'           => null,
            'att_timestamp'    => null,
            'att_sys_status'   => null,
            'remark'           => $remark,
            'att_date'         => $date,
            'att_process_date' => Carbon::now()->format('Y-m-d'),
            'created_at'       => Carbon::now(),
            'updated_at'       => Carbon::now()
        );
        $sav = new \IhrV2\Models\AttendanceProcessTable($arr);
        $sav->save();
        return true;
    }

    // update process empty
    // --------------------
    public function dbUpdateProcessEmptyBoth($i, $remark) {
        $arr = array(
            'remark'           => $remark,
            'att_process_date' => Carbon::now()->format('Y-m-d'),
            'updated_at'       => Carbon::now()
        );
        $q = \IhrV2\Models\AttendanceProcessTable::where('att_mykad_ic', $i->att_mykad_ic)
            ->where('location_id', $i->location_id)
            ->whereDate('att_date', '=', $i->att_date)
            ->update($arr);
        return true;
    }

    public function dbUpdateAggDate($type_id, $date, $status) {
        $type = ($type_id == 1) ? 'staff_attendance' : 'staff_detail';
        if ($status == 1) {
            $op = 'process';
        } else if ($status == 2) {
            $op = 'export';
        } else if ($status == 3) {
            $op = 'transfer';
        }
        $upd = \IhrV2\Models\AggDate::where('date', $date)
            ->update(array(
                $type => 1,
                $op   => 1
            ));
        return true;
    }

    public function dbInsertAggDate($date) {
        $arr = array(
            'date'             => $date,
            'staff_attendance' => 0,
            'staff_detail'     => 0,
            'process'          => 0,
            'export'           => 0,
            'transfer'         => 0
        );
        $sav = new \IhrV2\Models\AggDate($arr);
        $sav->save();
        return true;
    }

    public function dbUpdateStatusNullAttendance($i, $remark) {
        $q = \IhrV2\Models\AggStaffAttendance::where('PI1M_REFID', $i->PI1M_REFID)
            ->where('Staff_IC', $i->Staff_IC)
            ->whereDate('Date', '=', Carbon::parse($i->Date)->format('Y-m-d'))
            ->update(array(
                'CheckIn_Status' => $remark,
                'Send_Date'      => null,
                'Error_Message'  => null,
                'updated_at'     => date('Y-m-d H:i:s')
            ));
        return true;
    }

    // reset staff attendance
    // ----------------------
    public function dbResetStaffAtt($start, $end) {
        $q = \IhrV2\Models\AggStaffAttendance::whereBetween('Date', [$start, $end])
            ->update(array(
                'Send_Date'     => null,
                'Error_Message' => null
            ));
        return true;
    }

    // update status_mcmc
    // ------------------
    public function dbUpdateStatusMcmc($icno, $sitecode, $status) {
        $q = \IhrV2\User::where(
            [
                'icno'     => $icno,
                'sitecode' => $sitecode,
                'status'   => $status
            ])
            ->update(array(
                'status_mcmc' => 1
            ));
        return true;
    }

}
