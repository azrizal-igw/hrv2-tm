<?php

namespace IhrV2;

use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

// use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable {
    
    // public $timestamps = false;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'username',
        'name',
        'staff_id',
        'prev_staff_id',
        'email',
        'password',
        'api_token',
        'group_id',
        'is_admin',
        'icno',
        'permanent_street_1',
        'permanent_street_2',
        'permanent_postcode',
        'permanent_city',
        'permanent_state',
        'correspondence_street_1',
        'correspondence_street_2',
        'correspondence_postcode',
        'correspondence_city',
        'correspondence_state',
        'telno1',
        'telno2',
        'hpno',
        'hpno2',
        'faxno',
        'website',
        'personal_email',
        'work_email',
        'gender_id',
        'marital_id',
        'height',
        'weight',
        'dob',
        'pob',
        'nationality_id',
        'race_id',
        'religion_id',
        'partner_name',
        'partner_phone',
        'child_no',
        'sitecode',
        'itaxno',
        'epfno',
        'socsono',
        'bankname',
        'bankno',
        'status',
        'remember_token',
        'sid',
        'action_date',
        'last_login',
        'date_created',
        'date_modified',
        'imei',
        'device',
        'edit',
        'mobile',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    // public function roles()
    // {
    //     return $this->belongsToMany(Role::class);
    // }

    // public function hasRole($role) //
    // {
    //     if (is_string($role)) {
    //         return $this->roles->contains('name', $role);
    //     }
    //     return !! $role->intersect($this->roles)->count();
    // }

    // public function assign($role)
    // {
    //     if (is_string($role)) {
    //         return $this->roles()->save(
    //             Role::whereName($role)->firstOrFail()
    //         );
    //     }
    //     return $this->roles()->save($role);
    // }

    /**
     * Always hash the password when we save it to the database
     */
    // public function setPasswordAttribute($value) {
    //     $this->attributes['password'] = Hash::make($value);
    // }

    // always encrypt value password when user enter the password
    // public function setPasswordAttribute($value)
    // {
    //     $this->attributes['password'] = bcrypt($value);
    // }

    public function getEmailForPasswordReset() {
        return $this->personal_email;
    }

    public function scopeIsActive($q) {
        $q->where('status', 1);
    }

    public function scopeIsActiveAndIDDesc($q) {
        $q->where('status', 1)->orderBy('id', 'DESC');
    }

    // belongs to
    // ----------
    public function GroupName() {
        return $this->belongsTo('IhrV2\Models\Group', 'group_id');
    }

    public function GenderName() {
        return $this->belongsTo('IhrV2\Models\Gender', 'gender_id');
    }

    public function MaritalName() {
        return $this->belongsTo('IhrV2\Models\MaritalStatus', 'marital_id');
    }

    public function RaceName() {
        return $this->belongsTo('IhrV2\Models\Race', 'race_id');
    }

    public function ReligionName() {
        return $this->belongsTo('IhrV2\Models\Religion', 'religion_id');
    }

    public function NationalityName() {
        return $this->belongsTo('IhrV2\Models\Nationality', 'nationality_id');
    }

    public function SiteNameInfo() {
        return $this->belongsTo('IhrV2\Models\Site', 'sitecode', 'code');
    }

    public function StatusName() {
        return $this->belongsTo('IhrV2\Models\UserStatus', 'status');
    }

    // has one
    // -------
    public function UserLatestJob() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id')->where('status', 1)->where('flag', 1);
    }

    public function UserLatestJob2() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id')->where('status', 1);
    }

    public function UserCurrentJob() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id')->orderBy('id', 'desc');
    }

    public function UserCurrentJob2() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id')->where('flag', 1);
    }

    public function UserCurrentContract2() {
        return $this->hasOne('IhrV2\Models\UserContract', 'user_id')->where('flag', 1);
    }    

    public function UserLatestContract() {
        // return $this->hasOne('IhrV2\Models\UserContract', 'user_id')->where('status', 1);
        return $this->hasOne('IhrV2\Models\UserContract', 'user_id')->where('status', 1)->where('flag', 1);
    }

    public function UserLatestContract2() {
        return $this->hasOne('IhrV2\Models\UserContract', 'user_id')->where('status', 1)->where('flag', 1);
    }

    public function UserLastContract() {
        return $this->hasOne('IhrV2\Models\UserContract', 'user_id')->orderBy('id', 'desc');
    }

    public function UserInactive() {
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id')->where('status', 2);
    }

    // inactive job & contract
    // -----------------------
    public function UserInactiveJob() {
        // return $this->hasOne('IhrV2\Models\UserJob', 'user_id')->where('status', 2);
        return $this->hasOne('IhrV2\Models\UserJob', 'user_id')->where('status', 2)->where('flag', 1);
    }

    public function UserInactiveContract() {
        return $this->hasOne('IhrV2\Models\UserContract', 'user_id')->where('status', 2)->where('flag', 1);
    }

    // user photo
    // ----------
    public function UserPhotoActive() {
        return $this->hasOne('IhrV2\Models\UserPhoto', 'user_id')->where('active', 1);
    }

    public function UserPhotoApproved() {
        return $this->hasOne('IhrV2\Models\UserPhotoHistory', 'user_id')->where('status', 2)->where('flag', 1);
    }

    // has many
    // --------
    public function UserJob() {
        return $this->hasMany('IhrV2\Models\UserJob', 'user_id');
    }

    public function UserContract() {
        return $this->hasMany('IhrV2\Models\UserContract', 'user_id');
    }

    public function UserEducation() {
        return $this->hasMany('IhrV2\Models\UserEducation', 'user_id')->orderBy('id', 'DESC');
    }

    public function UserEducationOne() {
        return $this->hasOne('IhrV2\Models\UserEducation', 'user_id')->orderBy('id', 'DESC');
    }

    public function UserEmergency() {
        return $this->hasMany('IhrV2\Models\UserEmergency', 'user_id')->orderBy('id', 'DESC');
    }

    // display status color
    // --------------------
    public function getStatusColorAttribute() {
        if ($this->status == 1) {
            return '<span class="text-primary">' . strtoupper($this->StatusName->name) . '</span>';
        } else {
            return '<span class="text-danger">' . strtoupper($this->StatusName->name) . '</span>';
        }
    }

}
