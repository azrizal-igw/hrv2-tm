<?php

namespace IhrV2\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{



    
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }




    /**
     * Register the application services.
     *
     * @return void
     */
    // public function register()
    // {
    //     $this->app->bind(UserInterface::class, UserEloquent::class);
    // }


    public function register() {
        $models = array(
            'User\User',
            'User\DbUser',
            'Leave\Leave',
            'Leave\DbLeave',
            'Attendance\Attendance',
            'Attendance\DbAttendance',
            'Maintenance\Maintenance',
            'Maintenance\DbMaintenance',
            'Erp\Erp',
            'Erp\DbErp',
            'Mcmc\Mcmc',
            'Mcmc\DbMcmc',
            'Letter\Letter',
            'Letter\DbLetter',
            'Agg\Agg',
            'Agg\DbAgg',
            'Email\Email',
            'Upload\Upload',
            'Training\Training',
            'Training\DbTraining',
            'Download\Download',
            'Report\Report',
            'Api\Auth\ApiAuth',
            'Api\Auth\DbApiAuth',            
            'Api\Attendance\ApiAttendance',
            'Api\Attendance\DbApiAttendance',
            'Api\Training\ApiTraining',
            'Api\Training\DbApiTraining'            
        );

        foreach ($models as $model) {
            $this->app->bind("IhrV2\Contracts\\{$model}Interface", "IhrV2\Repositories\\{$model}Repository");
        }


    }




}





