<?php

namespace IhrV2\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use IhrV2\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'IhrV2\Model' => 'IhrV2\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        // $gate->define('admin-access', function($user){
        //     // dd($user->group_id);
        //     return $user->group_id == 1; // admin
        // });
 
        // $gate->define('hr-access', function($user){
        //     return $user->group_id == 2; // hr
        // });

        // $gate->define('sv-access', function($user){
        //     return $user->group_id == 3; // sv
        // });

        // $gate->define('rm-access', function($user){
        //     return $user->group_id == 4; // rm
        // });

        // $gate->define('drm-access', function($user){
        //     return $user->group_id == 6; // drm
        // });

        // dd(0);

        // $gate->define('customer-access', function($user){
        //     return $user->role == 'customer' || $user->role == 'admin' || $user->role == 'manager';
        // });

        // foreach ($this->getPermissions() as $permission) {
        //     $gate->define($permission->name, function($user) use ($permission) {
        //         return $user->hasRole($permission->roles);
        //     });
        // }
    }



    // protected function getPermissions()
    // {
    //     return Permission::with('roles')->get();
    // }




}



