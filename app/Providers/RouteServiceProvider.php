<?php

namespace IhrV2\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider {
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */

    protected $namespace = 'IhrV2\Http\Controllers';
    protected $apiNamespace = 'IhrV2\Http\Controllers\Api';
    protected $modNamespace = 'IhrV2\Http\Controllers\Mod';
    protected $svNamespace = 'IhrV2\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router) {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router) {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router) {

        /*
        |--------------------------------------------------------------------------
        | Default Router
        |--------------------------------------------------------------------------
         */

        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
            require app_path('Http/Routes/Auth.php');
            require app_path('Http/Routes/Library.php');
            require app_path('Http/Routes/Ajax/Block.php');
            require app_path('Http/Routes/Api.php');
        });

        /*
        |--------------------------------------------------------------------------
        | API Router (need api_token value)
        |--------------------------------------------------------------------------
         */

        $router->group([
            'apiNamespace' => $this->namespace, 'middleware' => 'auth:api',
        ], function ($router) {
            require app_path('Http/Routes/Api/Agg.php');
            require app_path('Http/Routes/Api/Attendance.php');
            require app_path('Http/Routes/Api/Sync.php');
            require app_path('Http/Routes/Api/Leave.php');
            require app_path('Http/Routes/Api/Migration.php');
            require app_path('Http/Routes/Api/User.php');
            require app_path('Http/Routes/Api/Update.php');
            require app_path('Http/Routes/Api/Mobile.php');
        });

        /*
        |--------------------------------------------------------------------------
        | Site Supervisor Router
        |--------------------------------------------------------------------------
         */

        $router->group([
            'svNamespace' => $this->namespace, 'middleware' => ['web', 'auth', 'sv'],
        ], function ($router) {
            require app_path('Http/Routes/Group/Site.php');
        });

        /*
        |--------------------------------------------------------------------------
        | Backend Router
        |--------------------------------------------------------------------------
         */

        $router->group([
            'modNamespace' => $this->namespace, 'middleware' => ['web', 'auth'],
        ], function ($router) {
            require app_path('Http/Routes/Mod/Agg.php');
            require app_path('Http/Routes/Mod/Sync.php');
            require app_path('Http/Routes/Mod/Ajax.php');
            require app_path('Http/Routes/Mod/User.php');
            require app_path('Http/Routes/Mod/Leave.php');
            require app_path('Http/Routes/Mod/Attendance.php');
            require app_path('Http/Routes/Mod/Maintenance.php');
            require app_path('Http/Routes/Mod/Letter.php');
            require app_path('Http/Routes/Mod/Update.php');
            require app_path('Http/Routes/Mod/Training.php');
            require app_path('Http/Routes/Mod/Report.php');
        });

    }
}
