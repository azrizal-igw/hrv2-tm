$(document).ready(function(){

	$(document).on('click','#btn_back',function() {
		window.location.href = $(this).data('href');
	});	
	
    $(document).on('click','#btn_url',function() {
        window.location.href = $(this).data('href');
    }); 
    
	$(document).on('click','#btn_save',function() {
		var answer = confirm('Do you want to save this record?');
		if (answer == true) {
			$("#btn_save").attr("disabled", true).html("Please wait...");
			$(this).parents('form').submit();
		}
		else {
			return false;
		} 
	});

    $(document).on('click','.btn_click',function() {
        var opt = $(this).attr('id');
        var answer = confirm('Do you want to ' + opt + ' this record?');
        if (answer == true) {
            $(".btn_click").attr("disabled", true).html("Please wait...");
            $(this).parents('form').submit();
        }
        else {
            return false;
        } 
    });

    $(document).on('click','#btn_cancel',function() {
        var answer = confirm('Do you want to cancel this record?');
        if (answer == true) {
            $("#btn_cancel").attr("disabled", true);
            var id = $(this).attr('data-id');
            var val = $(this).attr('data-value');
            $('input[name="' + id + '"]').val(val);
            $(this).parents('form').submit();
        }
        else {
            return false;
        } 
    });

    $(document).on('keyup','#word_count',function() {
        var words = this.value.match(/\S+/g).length;
        $('#display_count').text(words);
    });  

    $(document).on('click','.click_more',function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#' + id).hide();
        $('#' + id + '_content').show();
    });

    $(document).on('click','#staff',function() {
        var id = $(this).attr('alt');
        $('#status_id').val(id);
        $( "#block-user" ).submit();
    }); 

    $(document).on('click','#openBtn',function() {
        $('#myModal').modal({
            show: true
        })
    }); 

    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $(document).on('click','#detail a',function(e) {
        e.preventDefault();
        var url = $(this).attr("data-url");
        var href = this.hash;
        var pane = $(this);        
        $(href).load(url,function(result){      
            pane.tab('show');
        });
    });
});

function getLoadTab() {
    $('#att').load($('.active a').attr("data-url"), function (result) {
        $('.active a').tab('show');
    });
}

function getDatePicker(id) {
    $('#' + id).datetimepicker({
        pickTime: false
    });      
}

function getTimePicker(id) {
    $('#' + id).datetimepicker({
        pickDate: false
    });      
}

function getAjaxBlock(type) {
    var alt;
    switch (type) {
        case 1: alt = 'announcement'; break;
        case 2: alt = 'staff-info'; break;
        case 3: alt = 'leave-info'; break;
        case 4: alt = 'rl-info'; break;
        case 5: alt = 'attendance'; break;
        case 6: alt = 'calendar'; break;
    }
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/ajax/block/" + alt,
        async: true,
        success: function (response, status, jqXHR) {
            arr = response['block'];
            $('.block_' + type).append(arr);                         
            $('.loader_' + type).hide();
        }
    });    
}

function getAjaxClose() {
    var alt = [1,2,3,4,5,6];
    $.each(alt, function(i, val){
        $('.block_' + val).append('Not applicable.');                         
        $('.loader_' + val).hide();
    });
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        html: "true", 
        placement: "bottom", 
        title: OdTips
    });
    $('[data-toggle="tooltip_att"]').tooltip({
        html: "true", 
        placement: "bottom", 
        title: AttTips
    });
    $('[data-toggle="popover_att"]').popover({ 
        trigger: "manual", 
        html: true, 
        animation: false,
        placement: "bottom",
        title: $(this).attr("data-title"),
        content: AttTips 
    })
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    })
    .on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });
})

function AttTips()
{
    return $(this).attr("data-content");
}

function OdTips()
{
    var date = $(this).attr("data-attribute");
    var type = $(this).attr("data-type");
    var day = $(this).attr("data-day");
    if (type != '') {
        var table = type + '<br>Date: '+ date + '<br>Day: '+ day;        
    }
    else {
        var table = 'Date: '+ date + '<br>Day: '+ day;
    }
    return table;
}

function DisplayMap(lat, long, loc)
{    
    // add a tile layer to our map
    // ---------------------------
    var url = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
    var attr = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    osm = L.tileLayer(url, {
        maxZoom: 18,
        attribution: attr
    });

    // initialize the map
    // ------------------
    var map = new L.map('mapId').setView([lat, long], 14).addLayer(osm);

    // map.invalidateSize();
    setTimeout(function () { map.invalidateSize() }, 100);

    // set map marker
    // --------------
    marker = L.marker([lat, long]).addTo(map);


    // click event
    // -----------
    map.on('click', onMapClick);

    // markers storage
    // ---------------
    var markers = [];

    // script for adding marker on map click
    // -------------------------------------
    function onMapClick(e) {
        var geojsonFeature = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Point",
                "coordinates": [e.latlng.lat, e.latlng.lng]
            }
        }

        // remove previous marker
        // ----------------------
        if (markers.length > 0) {
            map.removeLayer(markers.pop());
        }

    }     
}

$(function () {
  $('[data-toggle="popover"]').popover()
})



