$(document).ready(function() {

    $(document).on('click','#submitCreate',function() {
        var registerForm = $("#formRegister");
        var formData = registerForm.serialize();
        // console.log(formData);
        $.ajax({
            type: "POST",
            url: "/" + $('#path').val(),
            data: formData,
            dataType: "json",
            success: function(data) {
                $('#popupMessage').css('display', 'block');
                location.reload();                
            },
            error: function(data) {
                if (data.status === 422) {
                    var errors = data.responseJSON;
                    $.each(errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                } 
                else {
                    alert('Incorrect credentials. Please try again.')
                }
            }            
        });
    }); 

    $(document).on('click','#modal-popup',function(e) {
        e.preventDefault();
        $(".divLoading").addClass('show');
        var url = $(this).attr('data-url');
        $.get(url, function(data) {
            $(data).modal();
            $(".divLoading").removeClass('show').addClass('hide');
        });
    })

    $(document).on('click','#btnRoute',function() {
        url = $(this).attr("data-url");
        window.location.href = url;
    })

    $(document).on('click','#chk_all',function() {
        if($(this).is(':checked')) {
            $('.chk_id').prop('checked', true);                            
        } 
        else {
            $('.chk_id').prop('checked', false);                  
        }       
    }); 
});

function getModDataList(url, page) 
{
    $.ajax({
        type: "GET",
        url: url + "?page=" + page,
        cache: false,
        success: function(response, status, jqXHR) {
            // console.log(response);
            $('#table-prev').html(response['list']);
            $('#table-count').html(response['total']);
        },
        error: function(e){
            console.log(e);
        },        
    }); 
}

function getModDataAll(url) 
{
    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(response, status, jqXHR) {
            $('#table-staff').html(response['list']);
        },
        error: function(e){
            console.log(e);
        },        
    }); 
}

function getCheckBoxValue(url) 
{
    $.ajax({
        type: "POST",
        url: url,
        data: $("#form-data").serialize(),
        cache: false,
        // dataType: "json",
        // contentType: 'application/json',
        success: function(response, status, jqXHR) {
            console.log(response);
            $('#table-attendees').parent().html(response['list']);
            // $(".modal.in").modal("hide");
            $("#myModal").modal('hide'); 

        },  
        complete: function(response, status, jqXHR) {                                       
        },                      
        error: function(e){
            console.log(e);
        },        
    }); 
}

function getModPopupUpdate(url) 
{
    var answer = confirm('Are you sure want to continue this process?');
    if (answer == true) {
        $.ajax({
            type: "POST",
            url: url,
            data: $("#form-data").serialize(),
            cache: false,
            success: function(response, status, jqXHR) {
                $(".modal.in").modal("hide"); 
                $('#table-prev').parent().html(response['list']);
                $('#table-count').parent().html(response['total']);                                           
            },  
            complete: function(response, status, jqXHR) {                                       
            },                      
            error: function(e){
                console.log(e);
            },        
        }); 
    }
    else {
        return false;
    }
}

function getModDeleteSelected(url) 
{
    var answer = confirm('Are you sure want to remove the selected record?');
    if (answer == true) {
        $.ajax({
            type: "POST",
            url: url,
            data: $("#form-data").serialize(),
            cache: false,
            success: function(response, status, jqXHR) {
                // console.log(response);
                $('#table-staff').css('overflow-y', 'scroll');
                $('#table-staff').css('height', '415px');
                $('#table-staff').addClass('well');
                $('#table-staff').html(response['list']);
            },  
            complete: function(response, status, jqXHR) {                                       
            },                      
            error: function(e){
                console.log(e);
            },        
        }); 
    }
    else {
        return false;
    }
}

function getModAjaxBlock(type) 
{
    var alt;
    switch (type) {
        case 1: alt = 'announcement'; break;
        case 2: alt = 'staff-info'; break;
        case 3: alt = 'leave-info'; break;
        case 4: alt = 'rl-info'; break;
        case 5: alt = 'attendance'; break;
        case 6: alt = 'calendar'; break;
    }
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/mod/ajax/block/" + alt,
        async: true,
        success: function (response, status, jqXHR) {
            if (type == 2) {
                arr = getBlockStaffInfo(response['block']);
            }
            else if (type == 3) {
                arr = getBlockLeaveInfo(response['status'], response['types']);                
            }
            else if (type == 4) {
                arr = getBlockRLInfo(response['block']);                
            }
            else if (type == 5) {
                arr = getBlockAttInfo(response['block']);                
            }                            
            else {
                arr = response['block'];
            }
            $('.block_' + type).append(arr);                         
            $('.loader_' + type).hide();
        }
    });    
}

function getBlockStaffInfo(arr) 
{
    var arrColors = ['#1661B8', '#D8B517', '#F379AB'];
    new Morris.Bar({
        element: 'staff-info',
        data: [
            { y: arr[0]['status_l'], a: arr[0]['total']},
            { y: 'M', a: arr[2]['total']},
            { y: 'AM', a: arr[3]['total']}
        ],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Total'],
        barColors: function (row, series, type) {
            return arrColors[row.x];
        }, 
        hideHover: 'auto',
        resize: true,
    });    
}

function getBlockLeaveInfo(status, types) 
{
    var data1 = [];
    var color1 = [];
    $.each(status, function(key, val){
        data1.push({'label': val['leave_status_name']['name'], 'value' : val['total']}); 
        color1.push('#' + val['leave_status_name']['color_code']);
    });
    new Morris.Donut({
        element: 'leave-chart1',
        data: data1,
        resize: true,
        stacked: true, 
        grid: false,
        colors: color1
    });
    var data2 = [];
    var color2 = [];
    $.each(types, function(key, val){
        data2.push({'label': val['leave_type_name']['name'], 'value' : val['total']}); 
        color2.push('#' + val['leave_type_name']['color_code']);
    });      
    new Morris.Donut({
        element: 'leave-chart2',
        data: data2,
        resize: true,
        stacked: true, 
        grid: false,
        colors: color2
    });      
}

function getBlockRLInfo(arr) 
{
    var data = [];
    var color = [];
    $.each(arr, function(key, val){
        data.push({'label': val['leave_rep_status_name']['name'], 'value' : val['total']}); 
        color.push('#' + val['leave_rep_status_name']['color_code']);
    });    
    new Morris.Donut({
        element: 'rl-info',
        data: data,
        resize: true,
        stacked: true, 
        grid: false,
        colors: color
    });  
}

function getBlockAttInfo(arr) 
{
    if (arr.length > 0) {
        var data = [];
        $.each(arr, function(key, val){
            data.push({'state': val['state'], 'in' : val['in'], 'out' : val['out']}); 
        });        
        new Morris.Line({
            element: 'att-info',
            data: data,
            lineColors: ['#819C79', '#fc8710'],
            xkey: 'state',
            ykeys: ['in','out'],
            labels: ['IN', 'OUT'],
            xLabels: 'day',
            parseTime: false,
            xLabelAngle: 45,
            resize: true
        });        
    }
    else {
        return 'No record';
    }

}

