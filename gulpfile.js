var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


//  var paths = {
// 	'jquery': 'bower_components/jquery/dist/',
// 	'bootstrap': 'bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap',
// 	'fontawesome': 'bower_components/font-awesome-sass/assets/stylesheets/font-awesome/'
// }

elixir(function(mix) {

    mix.sass([
        'web-fonts.scss',
        // 'simple-line-icons.scss'
        // 'font-awesome.scss'
    ], 'public/assets/css/web-fonts.css');

    // mix.copy('bower_components/font-awesome-sass/assets/fonts', 'public/assets/fonts');
    // mix.copy('bower_components/simple-line-icons/fonts', 'public/assets/fonts');
    // mix.copy('bower_components/jquery', 'public/assets/jquery');
    // mix.copy('bower_components/bootstrap-sass-official/assets/fonts/bootstrap', 'public/assets/fonts');



});


