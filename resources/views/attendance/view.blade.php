@extends('layouts/backend')

@section('content')

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin=""></script>



@if ($data['date'] < $data['today'])

	<div class="panel panel-primary">
		<div class="panel-body">
			@include('attendance/includes/view/att')
		</div>
	</div>

	@if (count($data['remarks']) > 0)
		@include('attendance/includes/view/remark')
		@include('attendance/includes/view/history')
	@endif

	@if (!empty($data['other']))
		<div class="panel panel-primary">
			<div class="panel-body">
				@include('attendance/includes/view/timeslip')
			</div>
		</div>
	@endif

	@if ($data['att']['in'] != 'Empty' && 
		$data['att']['out'] != 'Empty' &&
		empty($data['other']))
		@include('attendance/includes/view/form/timeslip')
	@endif

	@if ($data['on'] == 1 && $data['date'] >= $data['start'])
		@include('attendance/includes/view/form/remark')
	@endif

@else
	<div class="panel panel-primary">
		<div class="panel-body nopadding">			
	        <div class="row">
	            <div class="col-md-12">Selected Date is invalid. Cannot process current/future Date.</div>
	        </div>
		</div>
	</div>	
@endif


@stop


