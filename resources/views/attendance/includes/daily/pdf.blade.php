					<table class="table table-striped table-condensed table-responsive table-hover" width="100%">
						<thead>
							<tr style="color: #ffffff; background-color: #4e5154;">
								<th>Date</th>
								<th class="text-center">Time In</th>
								<th class="text-center">Time Out</th>
								<th class="text-center">Total Time</th>
								<th class="text-center">Total MCMC</th>
								<th class="text-left">Remarks</th>
							</tr>
						</thead>

						@if (count($data['all_dates']) > 0)
							<?php
								$start = null;
								$end = null;					
								$hours  = null;
								$minutes = null;
								$seconds = null;
								$mcmc_hours = null;
								$mcmc_minutes = null;
								$mcmc_seconds = null;
							?>				
							@foreach ($data['all_dates'] as $x => $y)
								<?php
									$date = \Carbon\Carbon::parse($y)->format('Y-m-d');
									$on = 0; 
									$apply_remark = 0;
									$approve_remark = 0;
									$checkbox = 0;
									$limit = array();
								?>
								@if (array_key_exists($y, $data['limit']))
									<?php 
										$limit['time_in'] = $data['limit'][$y]['time_in']; 
										$limit['time_out'] = $data['limit'][$y]['time_out'];
									?>
								@endif

								@if (\Carbon\Carbon::parse($y)->format('Y-m-d') == $data['today'])
									<?php $active = "class=success"; ?>
								@else
									<?php $active = ''; ?>
								@endif

								<tr {{ $active }}>	
									<td>
										{{ \Carbon\Carbon::parse($y)->format('Y-m-d') }}
									</td>
									<?php 
										$in = 0; 
										$out = 0;
										$have_in = 0; 
										$have_out = 0;
										$less = 0;  
									?>							
									@if (in_array(\Carbon\Carbon::parse($y)->format('Y-m-d'), $data['dates']))
										@foreach ($data['att'] as $t)
											@if (\Carbon\Carbon::parse($t->att_date)->format('Y-m-d') == \Carbon\Carbon::parse($y)->format('Y-m-d'))

												@if ($t->att_record_type == 0 && $have_in == 0)
													<?php 
														$in = 1; 
														$late_in = '';
														$start = $t->att_in;
														$have_in = 1;
														$status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_in'], $data['mobile']); 
														$title = \Carbon\Carbon::parse($t->att_in)->format('Y-m-d g:i:s A'); 
													?>		
													@if (\Carbon\Carbon::parse($t->att_in)->format('H:i:s') > $limit['time_in']) 
														<?php $late_in = 'text-danger' ?>
													@endif
													<td class="text-center">
														<span class="{{ $late_in }}">{{ \Carbon\Carbon::parse($t->att_in)->format('g:i:s A') }}</span>&nbsp;<i class="{{ $status['icon'] }} text-{{ $status['color'] }}" data-content="{{ $status['content'] }}" data-toggle="popover_att" data-title="{{ $title }}"></i>
													</td>
												@endif

												@if ($t->att_record_type == 1 && $have_out == 0)
													<?php 
														$out = 1; 
														$early_out = '';
														$end = $t->att_out; 
														$have_out = 1;													
														$status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_out'], $data['mobile']);
														$title = \Carbon\Carbon::parse($t->att_out)->format('Y-m-d g:i:s A');
													?>
													@if ($in == 0)
														<td class="text-center">-</td>
													@endif
													@if (\Carbon\Carbon::parse($t->att_out)->format('H:i:s') < $limit['time_out'])
														<?php $early_out = 'text-danger' ?>
													@endif											
													<td class="text-center">
														<span class="{{ $early_out }}">{{ \Carbon\Carbon::parse($t->att_out)->format('g:i:s A') }}</span>&nbsp;<i class="{{ $status['icon'] }} text-{{ $status['color'] }}" data-content="{{ $status['content'] }}" data-toggle="popover_att" data-title="{{ $title }}"></i>
													</td>
												@endif

											@endif
										@endforeach

										@if ($in == 1 && $out == 0)
											<td class="text-center">-</td>
										@endif								

									@else
										<td class="text-center">-</td>
										<td class="text-center">-</td>
									@endif

									@if ($in == 1 && $out == 1)
										<?php
											if ($start < $end) {
												$get = AttendanceHelper::getCompareDateTime($start, $end, $limit);
												$hours += $get['hour'];
												$minutes += $get['minute'];
												$seconds += $get['second'];	
												$total_time = $get['time'];									
											}
											else {
												$less = 1;
												$total_time = '-';
											}
										?>
										<td class="text-center">{!! $total_time !!}</td>
									@else
										<td class="text-center">-</td>
									@endif

									@if ($in == 1 && $out == 1)
										<?php
											if ($start < $end) {
												$mcmc = AttendanceHelper::getCompareMcmc($start, $end, $limit);
												$mcmc_hours += $mcmc['hour'];
												$mcmc_minutes += $mcmc['minute'];
												$mcmc_seconds += $mcmc['second'];
												$total_mcmc = $mcmc['time'];
											}
											else {
												$less = 1;
												$total_mcmc = '-';
											}
										?>
										<td class="text-center">{!! $total_mcmc !!}</td>
									@else
										<td class="text-center">-</td>
									@endif

									<td>
										<?php $empty = 0; $remark = '-'; ?>
										@if ($date <= $data['today'])
											@if ($in == 1 && $out == 1 && $less != 1)
												<?php $remark = 'Working'; ?>
											@else											
												@if (in_array($date, $data['leave_dates']))
													@foreach ($data['leave_details'] as $ld)
														@if ($date == $ld['date'])
															<?php $remark = $ld['name']; ?>
														@endif
													@endforeach
												@elseif (in_array($date, $data['leave_half']))
													<?php 
														$counts = array_count_values($data['leave_half']);
														$half = $counts[$date];
													?>
													@if ($half >= 2)
														<?php $remark = 'Leave 2X'; ?>
													@endif
												@elseif (in_array($date, $data['public_dates']))
													<?php $remark = 'Public Holiday'; ?>
												@elseif (in_array($date, $data['off_dates']))
													<?php $remark = 'Off Day'; ?>								
												@endif												
											@endif
										@endif
										@if (in_array($date, $data['approve_dates']) && $remark == '-')
											@foreach ($data['remark_details'] as $rd)
												@if ($date == $rd['date'])
													<?php $remark = $rd['name']; ?>
												@endif
											@endforeach
										@endif
										@if (in_array($date, $data['rm_approve_dates']) && $remark == '-')
											@foreach ($data['remark_details'] as $rd)
												@if ($date == $rd['date'])
													<?php $remark = $rd['name']; ?>												
												@endif
											@endforeach
										@endif								
										{{ $remark }}
									</td>
								</tr>
							@endforeach
						@endif

						<tr style="color: #ffffff; background-color: #4e5154;">
							<td colspan="3">Total Time In a Month</td>
							<td class="text-center">
								<?php
									$get_total = AttendanceHelper::getTotalHours($hours, $minutes, $seconds);
								?>
								{{ $get_total['total_hours'] }}:{{ $get_total['total_minutes'] }}:{{ $get_total['total_seconds'] }}
							</td>
							<td class="text-center">
								<?php
									$mcmc_total = AttendanceHelper::getTotalHours($mcmc_hours, $mcmc_minutes, $mcmc_seconds);
								?>
								{{ $mcmc_total['total_hours'] }}:{{ $mcmc_total['total_minutes'] }}:{{ $mcmc_total['total_seconds'] }}
							</td>
							<td></td>
						</tr>

					</table>