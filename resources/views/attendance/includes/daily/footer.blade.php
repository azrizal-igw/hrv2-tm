@if (auth()->user()->group_id == 3)
<div class="well">
	<strong>Remarks: </strong>
	<br>1) MCMC Time is from 9:00AM to 6:00PM (Peninsular) and 8:00AM to 5:00PM (Sabah & Sarawak).
	<br>2) Please ensure type of attendance is choosen correctly when Punch The Attendance or the record will display empty.
	<br>3) Double check Site Code and IC No if record still empty.
	<br>4) Fail to Punch Attendance will effect the Total Time. 
	<br>5) Please ensure total time is 9 hours.
	<br>6) If have Time Slip (Out for a while) make sure the status is 'Working' 
	<br>7) Any request remark is for previous date. Current/Future date is not allowed.
	<br>8) Tick the checkbox if same event more than 1 date.
	<br>9) Remark only 3 days backward. Please fill as soon as possible.
</div>
@else
<div class="well">
	<strong>Remarks: </strong>
	<br>1) MCMC Time is from 9:00AM to 6:00PM (Peninsular) and 8:00AM to 5:00PM (Sabah & Sarawak).
	<br>2) Please ensure type of attendance is choosen correctly when Punch The Attendance or the record will display empty.
	<br>3) Double check Site Code and IC No if record still empty.
</div>
@endif

<div class="well">
	<p class="bg-primary" style="padding: 5px;"><strong>Attendance Status</strong></p>
	<i class="fa fa-check-circle text-success"></i> Direct Transfer.
	<br><i class="fa fa-pencil"></i> Attendance Manual.
	<br><i class="fa fa-qrcode text-success"></i> QR Code CDCC.
	<br><i class="fa fa-map-marker text-warning"></i> Mobile GPS.
	<br><i class="fa fa-exclamation-triangle text-danger"></i> Date Time Attendance and Date Time Transfer is different.

	<br><br>
	<p class="bg-primary" style="padding: 5px;"><strong>Color Indicator</strong></p>
	Green - OK
	<br>Yellow - Accepted but not Frequent
	<br>Black - Not Recommended
	<br>Red - Will be Questioned
</div>

<div class="well">
	<strong>Legends: </strong>
	<br><i class="glyphicon glyphicon-stop yellow"></i>&nbsp;Working
	<br><i class="glyphicon glyphicon-stop grey"></i>&nbsp;Off Day
	<br><i class="glyphicon glyphicon-stop blue"></i>&nbsp;Approved Leave
	<br><i class="glyphicon glyphicon-stop black"></i>&nbsp;Public Holiday
	<br><i class="glyphicon glyphicon-stop aqua"></i>&nbsp;Apply Remark
	<br><i class="glyphicon glyphicon-stop green-leaf"></i>&nbsp;Approved Remark
	<br><i class="glyphicon glyphicon-stop purple"></i>&nbsp;Remark By RM
	<br><i class="glyphicon glyphicon-stop red"></i>&nbsp;Warning
</div>



<style type="text/css">
a.alt_l {
	color: #666666;
}
</style>


