			<table class="table table-striped table-condensed table-responsive table-hover">

				<thead>
					<tr class="bg-default">
						<th>Date</th>
						<th class="text-center">Time In</th>
						<th class="text-center">Time Out</th>
						<th class="text-center">Total Time</th>
						<th class="text-center">Total MCMC</th>
						<th class="text-center">Remarks</th>
						<th class="text-center">#</th>						
						<th class="text-right">Actions</th>
					</tr>
				</thead>

				@if (count($data['all_dates']) > 0)
					<?php
						$start = null;
						$end = null;					
						$hours  = null;
						$minutes = null;
						$seconds = null;
						$mcmc_hours = null;
						$mcmc_minutes = null;
						$mcmc_seconds = null;
					?>				
					@foreach ($data['all_dates'] as $x => $y)
						<?php
							$date = \Carbon\Carbon::parse($y)->format('Y-m-d');
							$on = 0; 
							$apply_remark = 0;
							$approve_remark = 0;
							$checkbox = 0;
							$limit = array();
						?>
						@if (array_key_exists($y, $data['limit']))
							<?php 
								$limit['time_in'] = $data['limit'][$y]['time_in']; 
								$limit['time_out'] = $data['limit'][$y]['time_out'];
							?>
						@endif

						@if (\Carbon\Carbon::parse($y)->format('Y-m-d') == $data['today'])
							<?php $active = "class=success"; ?>
						@else
							<?php $active = ''; ?>
						@endif

						<tr {{ $active }}>	
							<td>
								{{ \Carbon\Carbon::parse($y)->format('Y-m-d') }}
							</td>
							<?php 
								$in = 0; 
								$out = 0;
								$have_in = 0; 
								$have_out = 0;
								$less = 0;  
							?>		

							@if (in_array(\Carbon\Carbon::parse($y)->format('Y-m-d'), $data['dates']))
								@foreach ($data['att'] as $t)
									@if (\Carbon\Carbon::parse($t->att_date)->format('Y-m-d') == \Carbon\Carbon::parse($y)->format('Y-m-d'))

										@if ($t->att_record_type == 0 && $have_in == 0)
											<?php 
												$in = 1; 
												$late_in = '';
												$start = $t->att_in;
												$have_in = 1;
												$status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_in'], $data['mobile']);
												$title = \Carbon\Carbon::parse($t->att_in)->format('Y-m-d g:i:s A'); 
											?>		
											@if (\Carbon\Carbon::parse($t->att_in)->format('H:i:s') > $limit['time_in']) 
												<?php $late_in = 'text-danger' ?>
											@endif
											<td class="text-center">
												<span class="{{ $late_in }}">{{ \Carbon\Carbon::parse($t->att_in)->format('g:i:s A') }}</span>&nbsp;<i class="{{ $status['icon'] }} text-{{ $status['color'] }}" data-content="{{ $status['content'] }}" data-toggle="popover_att" data-title="{{ $title }}"></i>
											</td>
										@endif

										@if ($t->att_record_type == 1 && $have_out == 0)
											<?php 
												$out = 1; 
												$early_out = '';
												$end = $t->att_out; 
												$have_out = 1;											
												$status = AttendanceHelper::checkAttStatus($t, $t->att_record_type, $limit['time_out'], $data['mobile']);
												$title = \Carbon\Carbon::parse($t->att_out)->format('Y-m-d g:i:s A');				
											?>
											@if ($in == 0)
												<td class="text-center">-</td>
											@endif
											@if (\Carbon\Carbon::parse($t->att_out)->format('H:i:s') < $limit['time_out'])
												<?php $early_out = 'text-danger' ?>
											@endif											
											<td class="text-center">
												<span class="{{ $early_out }}">{{ \Carbon\Carbon::parse($t->att_out)->format('g:i:s A') }}</span>&nbsp;<i class="{{ $status['icon'] }} text-{{ $status['color'] }}" data-content="{{ $status['content'] }}" data-toggle="popover_att" data-title="{{ $title }}"></i>
											</td>
										@endif

									@endif
								@endforeach

								@if ($in == 1 && $out == 0)
									<td class="text-center">-</td>
								@endif								

							@else
								<td class="text-center">-</td>
								<td class="text-center">-</td>
							@endif



							@if ($in == 1 && $out == 1)
								<?php
									if ($start < $end) {
										$get = AttendanceHelper::getCompareDateTime($start, $end, $limit);
										$hours += $get['hour'];
										$minutes += $get['minute'];
										$seconds += $get['second'];	
										$total_time = $get['time'];									
									}
									else {
										$less = 1;
										$total_time = '-';
									}
								?>
								<td class="text-center">{!! $total_time !!}</td>
							@else
								<td class="text-center">-</td>
							@endif

							@if ($in == 1 && $out == 1)
								<?php
									if ($start < $end) {
										$mcmc = AttendanceHelper::getCompareMcmc($start, $end, $limit);
										$mcmc_hours += $mcmc['hour'];
										$mcmc_minutes += $mcmc['minute'];
										$mcmc_seconds += $mcmc['second'];
										$total_mcmc = $mcmc['time'];
									}
									else {
										$less = 1;
										$total_mcmc = '-';
									}
								?>
								<td class="text-center">{!! $total_mcmc !!}</td>
							@else
								<td class="text-center">-</td>
							@endif

							<td class="text-center">
								@if ($date <= $data['today'])
									@if ($in == 1 && $out == 1 && $less != 1)
										<i class="glyphicon glyphicon-stop yellow" title="Working"></i>
									@else
										@if (in_array($date, $data['public_dates']))
											<i class="glyphicon glyphicon-stop black" title="Public Holiday"></i>									
										@elseif (in_array($date, $data['off_dates']))
											<i class="glyphicon glyphicon-stop grey" title="Off Day"></i>
										@elseif (in_array($date, $data['leave_dates']))
											<i class="glyphicon glyphicon-stop blue" title="Approved Leave"></i>
										@elseif (in_array($date, $data['leave_half']))
											<?php 
												$counts = array_count_values($data['leave_half']);
												$half = $counts[$date];
											?>
											@if ($half >= 2)
												<i class="glyphicon glyphicon-stop blue" title="Approved Leave"></i>
											@else
												<i class="glyphicon glyphicon-stop red"></i>
											@endif
										@else
											<?php $on = 1; ?>
											<i class="glyphicon glyphicon-stop red"></i>
										@endif
									@endif
								@else
									<i class="glyphicon glyphicon-stop white"></i>
								@endif

								@if (in_array($date, $data['pending_dates']))
									<?php $checkbox = 1; ?>
									<i class="glyphicon glyphicon-stop aqua" title="Apply Remark"></i>
								@else
									<i class="glyphicon glyphicon-stop white"></i>
								@endif

								@if (in_array($date, $data['approve_dates']))
									<?php $approve_remark = 1; $checkbox = 1; $col3_color = 'green-leaf'; $col3_title = 'Approved Remark'; ?>
								@elseif (in_array($date, $data['rm_approve_dates']))
									<?php $approve_remark = 1; $checkbox = 1; $col3_color = 'purple'; $col3_title = 'Remark By RM'; ?>
								@else
									<?php $col3_color = 'white'; $col3_title = ''; ?>
								@endif
								<i class="glyphicon glyphicon-stop {{ $col3_color }}" title="{{ $col3_title }}"></i>
							</td>

							<td class="text-center">
								@if (auth()->user()->group_id == 3)
									@if ($date < $data['today'] && $date >= $data['start'])
										@if ($on == 1)
											@if ($checkbox != 1)	
												{{ Form::checkbox('chk_date[]', $date, null, ['class' => 'chk-date']) }}
											@endif
										@endif
									@endif
								@else
									@if ($on == 1)
										@if ($checkbox != 1 && $less != 1)	
											{{ Form::checkbox('chk_date[]', $date, null, ['class' => 'chk-date']) }}
										@endif
									@endif
								@endif
							</td>

							<td class="text-right">
								@if ($date < $data['today'])

									@if (auth()->user()->group_id == 3)
										@if ($on == 1)
											@if ($approve_remark == 1)
												<a href="{{ route('sv.attendance.daily.view', array($date)) }}" class="btn btn-sm btn-default" title="View"><i class="icon-magnifier-add"></i></a>
											@else
												@if ($date < $data['start'])
													@if (in_array($date, $data['other_dates']))
														@if ($less != 1)
															<?php $icon = 'doc'; $title = 'View'; $color = 'primary'; ?>
														@else
															<?php $icon = 'doc'; $title = 'View'; $color = 'danger'; ?>
														@endif
													@else
														<?php $icon = 'lock'; $title = 'Remark is Locked'; $color = 'maroon'; ?>
													@endif
													<a href="{{ route('sv.attendance.daily.view', array($date)) }}" class="btn btn-sm btn-{{ $color }}" title="{{ $title }}"><i class="icon-{{ $icon }}"></i></a>
												@else
													@if ($less != 1)
														<?php $title = 'Put Remark'; $icon = 'note'; ?>
													@else
														<?php $title = 'Invalid Time'; $icon = 'minus'; ?>													
													@endif
													<a href="{{ route('sv.attendance.daily.view', array($date)) }}" class="btn btn-sm btn-danger" title="{{ $title }}"><i class="icon-{{ $icon }}"></i></a>
												@endif
											@endif
										@else
											@if (in_array($date, $data['other_dates']))
												<?php $icon = 'doc'; $color = 'primary'; ?>
											@else
												<?php $icon = 'magnifier-add'; $color = 'default'; ?>
											@endif	
											<a href="{{ route('sv.attendance.daily.view', array($date)) }}" class="btn btn-sm btn-{{ $color }}" title="View"><i class="icon-{{ $icon }}"></i></a>																				
										@endif
							
									@else
										@if ($on == 1)
											@if ($approve_remark == 1)
												<a href="{{ route('mod.attendance.user.view', array($arr[0], $arr[1], $arr[2], $date)) }}" class="btn btn-sm btn-default" title="View"><i class="icon-magnifier-add"></i></a>
											@else
												@if (in_array($date, $data['other_dates']))
													<?php $icon = 'doc'; $title = 'View'; $color = 'primary'; ?>
												@else
													@if ($less != 1)
														<?php $icon = 'note'; $title = 'Set Remark'; $color = 'danger'; ?>
													@else
														<?php $icon = 'minus'; $title = 'Invalid Time'; $color = 'danger'; ?>
													@endif
												@endif
												<a href="{{ route('mod.attendance.user.view', array($arr[0], $arr[1], $arr[2], $date)) }}" class="btn btn-sm btn-{{ $color }}" title="{{ $title }}"><i class="icon-{{ $icon }}"></i></a>
											@endif
										@else
											@if (in_array($date, $data['other_dates']))
												<?php $icon = 'doc'; $title = 'View'; $color = 'primary'; ?>
											@else
												<?php $icon = 'magnifier-add'; $title = 'View'; $color = 'default'; ?>
											@endif
											<a href="{{ route('mod.attendance.user.view', array($arr[0], $arr[1], $arr[2], $date)) }}" class="btn btn-sm btn-{{ $color }}" title="{{ $title }}"><i class="icon-{{ $icon }}"></i></a>										
										@endif
									@endif

								@else
									{{ '&nbsp;' }}
								@endif									
							</td>

						</tr>
					@endforeach
				@endif

				<tr style="color: #ffffff; background-color: #4e5154;">
					<td colspan="3">Total Time In a Month</td>
					<td class="text-center">
						<?php
							$get_total = AttendanceHelper::getTotalHours($hours, $minutes, $seconds);
						?>
						{{ $get_total['total_hours'] }}:{{ $get_total['total_minutes'] }}:{{ $get_total['total_seconds'] }}
					</td>
					<td class="text-center">
						<?php
							$mcmc_total = AttendanceHelper::getTotalHours($mcmc_hours, $mcmc_minutes, $mcmc_seconds);
						?>
						{{ $mcmc_total['total_hours'] }}:{{ $mcmc_total['total_minutes'] }}:{{ $mcmc_total['total_seconds'] }}
					</td>
					<td></td>
					<td class="text-center">
						{{ Form::button('<i class="icon-note"></i>', array('class' => 'btn btn-sm btn-danger', 'type' => 'submit', 'name' => 'btn_remark', 'value' => 'remark', 'title' => 'Put Remark of Selected Date')) }}
					</td>
					<td></td>
				</tr>

			</table>