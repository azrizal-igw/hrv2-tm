@if (auth()->user()->group_id == 3)


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
	<div class="col-md-12">
		<div class="well">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-3">
								{{ Form::select('year', $years, $sessions['year'], array('class' => 'form-control')) }}
							</div>

							<div class="col-md-3">
								{{ Form::selectMonth('month', $sessions['month'], ['class' => 'form-control', 'title' => 'Month']) }}
							</div>		
						</div>   
					<br>    
						{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}
					</div>          
				</div>
		</div>
	</div>            
</div>
{{ Form::close() }}


@else


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
	<div class="col-md-6">
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							{{ Form::select('year', $years, $sessions['year'], array('class' => 'form-control', 'title' => 'Year')) }}
						</div>
						<div class="col-md-6">
							{{ Form::selectMonth('month', $sessions['month'], ['class' => 'form-control', 'title' => 'Month']) }}
						</div>		
					</div>   
				<br>    
				{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn_search')) }}
				</div>          
			</div>
		</div>
	</div> 

	<div class="col-md-6">
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							{{ Form::select('month_from', $months, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-6">
							{{ Form::select('month_to', $months, null, array('class' => 'form-control')) }}							
						</div>		
					</div>   
				<br>    
				{{ Form::button('<i class="fa fa-file-pdf-o"></i>', array('class' => 'btn btn-danger', 'type' => 'submit', 'name' => 'btn_pdf', 'value' => 'pdf', 'title' => 'Download PDF')) }}
				{{ Form::button('<i class="fa fa-file-word-o"></i>', array('class' => 'btn btn-primary', 'type' => 'submit', 'name' => 'btn_word', 'value' => 'word', 'title' => 'Download Word')) }}											
				{{ Form::button('<i class="fa fa-file-excel-o"></i>', array('class' => 'btn btn-success', 'type' => 'submit', 'name' => 'btn_excel', 'value' => 'excel', 'title' => 'Download Excel')) }}							
				</div>          
			</div>
		</div>
	</div>   	           
</div>
{{ Form::close() }}


@endif

