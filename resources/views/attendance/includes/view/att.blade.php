		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="3"><strong>Attendance Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Date</td>
						<td class="col-md-7">{{ $data['date'] }}</td>
						<td></td>
					</tr>
					<tr>
						<td>Time In</td>
						<td>
							{!! $data['att']['in'] !!} 
						</td>
						<td class="text-right">

						</td>
					</tr>
					<tr>
						<td>Time Out</td>
						<td>
							{!! $data['att']['out'] !!}
						</td>
						<td class="text-right">

						</td>						
					</tr>

					
					@if (!empty($data['off_day']))
						<tr>
							<td>Off/Rest Day</td>
							<td>
								@if (!empty($data['off_day']['off_type_name']))
									{{ $data['off_day']['off_type_name']['name'] }}
								@else
									{{ '-' }}
								@endif
							</td>
							<td></td>
						</tr>
					@endif

					@if (!empty($data['public']))
						<tr>
							<td>Public Holiday</td>
							<td>
								@if (!empty($data['public']['public_name']))
									{{ $data['public']['public_name']['desc'] }}
								@else
									{{ '-' }}
								@endif
							</td>
							<td></td>
						</tr>
					@endif

					<?php $status_half = 0; ?>
					@if (!empty($data['leave']->LeaveApproveOne))
						<tr>
							<td>Apply Leave</td>
							<td>
								{{ $data['leave']->LeaveInfo->LeaveTypeName->name }} 
								@if ($data['leave']->LeaveInfo->is_half_day == 1) 
									<?php $status_half = 1; ?>
									{{ '(Half Day)' }}
								@endif								
							</td>
							<td></td>							
						</tr>
					@endif

					@if (auth()->user()->group_id != 3)
						@if ($data['remarks'])
							@if (!empty($data['remarks']->AttApplyRemark))
								<tr>
									<td>Request Remark</td>
									<td>
										{{ $data['remarks']->AttStatusName->name }}
									</td>
									<td></td>
								</tr>
							@else
								<tr><td>Request Remark</td><td>-</td></tr>
							@endif

							@if ($data['remarks']->AttActiveStatus->status == 3)
								<tr>
									<td>Updated Remark</td>
									<td>
										{{ $data['remarks']->AttOtherName->name }}
									</td>
									<td></td>									
								</tr>
							@else
								<tr><td>Updated Remark</td><td>-</td><td></td></tr>
							@endif

							<tr>
								<td>Notes</td>
								<td>
									{{ $data['remarks']->notes }}
								</td>
								<td></td>
							</tr>
							<tr>
								<td>Status Req. Remark</td>
								<td>
									@if ($data['remarks']->AttActiveStatus)
										@if ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 1)
											<?php $color = 'text-danger'; ?>
										@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 2)
											<?php $color = 'text-primary'; ?>
										@else
											<?php $color = 'text-info'; ?>									
										@endif
										<span class="{{ $color }}">{{ $data['remarks']->AttActiveStatus->AttRemarkStatusName->name }}</span>
									@else
										{{ '-' }}
									@endif
								</td>
								<td></td>
							</tr>
							<tr>
								<td>Attachment</td>
								<td>
									@if ($data['remarks']->AttActiveAttachment)													
										<?php
											$attachment = $data['remarks']->AttActiveAttachment->filename . '.' . $data['remarks']->AttActiveAttachment->ext;
											$photo = route('lib.file.attendance.remark', array($attachment));
											$thumb = $data['remarks']->AttActiveAttachment->thumb_name . '.' . $data['remarks']->AttActiveAttachment->ext;
											$thumb_url = route('lib.file.attendance.remark.thumb', array($thumb));
										?>
										<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
									@else
										{{ '-' }}
									@endif
								</td>
								<td></td>
							</tr>						
							<tr>
								<td colspan="2">
									@if ($data['remarks']->AttActiveStatus)
										@if ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 1)
											{{ 'Note: This Request is waiting for Approval from Region Manager (RM).' }}
										@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 2)
											{{ 'Note: This remark is approved.' }}
										@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 3)
											{{ 'Note: This remark set by RM.' }}
										@endif
									@else
										{{ '-' }}
									@endif
								</td>
								<td></td>
							</tr>
						@endif
					@endif
				</table>

				@if ($status_half == 1)
					<p><strong>Notes:</strong>
						<br>1) If apply Half Day make sure have Punch In & Punch Out.
					</p>
				@endif

			</div>
		</div>

