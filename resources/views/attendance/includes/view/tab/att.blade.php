<div class="tab-pane" id="att">

	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Punch Info</h4>
				</div>	

				<div class="panel-body nopadding">
					<table class="table table-condensed table-responsive table-striped">						
						<tr>
							<td class="col-md-3">Method</td>
							<td class="col-md-9">{{ $data['method'] }}</td>
						</tr>

						<tr>
							<td>Type</td>
							<td>
								@if ($data['att']->att_record_type == 0)
								{{ 'IN' }}
								@else
								{{ 'OUT' }} 
								@endif
							</td>
						</tr>

						<tr>
							<td>Punch</td>
							<td>{{ \Carbon\Carbon::parse($data['att']->att_log_time)->format('Y-m-d g:i:s A') }}</td>
						</tr>

						<tr>
							<td>Transfer</td>
							<td>{{ \Carbon\Carbon::parse($data['att']->att_timestamp)->format('Y-m-d g:i:s A') }}</td>
						</tr>
						<tr>
							<td>IP Address</td>
							<td>{{ $data['att']->ip_add }}</td>
						</tr>
					</table>	             
				</div>

			</div>
		</div>
	</div>
</div>


