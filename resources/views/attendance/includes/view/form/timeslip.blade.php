	{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
	<div class="row">
		<div class="col-lg-12">

			<div class="panel panel-default">
				<div class="panel-body">
					<legend>Set Time Slip</legend>
					<div class="form-group">
						@if ($errors->has('att_notes'))
							<p class="col-lg-12 text-danger">{{ $errors->first('att_notes') }}</p>
						@endif   	
						<div class="required"> 					
							<label class="col-lg-3 control-label">Notes</label>
						</div>
						<div class="col-lg-4">
							{{ Form::textarea('att_notes', old('att_notes'), ['class' => 'form-control', 'size' => '30x3']) }}  
						</div>
					</div>

					<div class="form-group" id="i2">
						@if ($errors->has('att_file'))
							<p class="col-lg-12 text-danger">{{ $errors->first('att_file') }}</p>
						@endif  
						<div class="required"> 					
							<label class="col-lg-3 control-label" for="textArea">Attachment</label>
						</div>
						<div class="col-lg-9">
							<label>
								{{ Form::file('att_file') }}
							</label>
						</div>
					</div>


					<br>
					<strong>Remarks: </strong>	
					<br>1) Use this form to prove any info regarding the attendance.
				</div>
				<div class="panel-footer">  
					<div class="row">
						<div class="col-md-12">
							{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
							{{ Form::hidden('type', 2)}}
						</div>
					</div>
				</div>				
			</div>

		</div>
	</div>	
	{{ Form::close() }}