	{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
	<div class="row">
		<div class="col-lg-12">

			<div class="panel panel-default">
				<div class="panel-body">
					<legend>Set Remark</legend>
					<div class="form-group">
						@if ($errors->has('att_status'))
							<p class="col-lg-12 text-danger">{{ $errors->first('att_status') }}</p>
						@endif   	
						<div class="required"> 					
							<label class="col-lg-3 control-label">Remark Status</label>
						</div>
						<div class="col-lg-4">
							{{ Form::select('att_status', $status, old('att_status'), array('class' => 'form-control', 'id' => 'selectStatus')) }}  
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('att_notes'))
							<p class="col-lg-12 text-danger">{{ $errors->first('att_notes') }}</p>
						@endif   	
						<div class="required"> 					
							<label class="col-lg-3 control-label">Notes</label>
						</div>
						<div class="col-lg-4">
							{{ Form::textarea('att_notes', old('att_notes'), ['class' => 'form-control', 'size' => '30x3']) }}  
						</div>
					</div>

					<div class="form-group" id="i2">
						@if ($errors->has('att_file'))
							<p class="col-lg-12 text-danger">{{ $errors->first('att_file') }}</p>
						@endif  
						<div class="required"> 					
							<label class="col-lg-3 control-label" for="textArea">Attachment</label>
						</div>
						<div class="col-lg-9">
							<label>
								{{ Form::file('att_file') }}
							</label>
						</div>
					</div>
					<hr>

					<br>
					<strong>Remarks: </strong>	
					<br>1) Use this request if have Training/Switch Off Day/Can't punch the attendance (2)
					<br>2) Can't Punch - Blackout/No Internet/Punch Attendance but not appear in the system. Select TBU if this.
					<br>3) TBU = To Be Update.
					<br>4) Submitted request will send to RM for verification.
					<br>5) File attachment must be in following types jpeg/jpg/png and below 2MB.
					<br>6) Get a Print Screen from System Attendance if Punch the Attendance.
					<br>7) If none are related please apply Leave. 				
				</div>

				<div class="panel-footer">  
					<div class="row">
						<div class="col-md-12">
							{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
							{{ Form::hidden('type', 1) }}
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	{{ Form::close() }}