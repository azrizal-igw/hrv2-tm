			<div class="row">
				<div class="col-md-12">	
					<table class="table table-condensed table-responsive table-striped table-hover">
						<thead>
							<tr class="bg-inverse">
								<td colspan="2"><strong>Time Slip</strong></td>
							</tr>
						</thead>
						<tr>
							<td class="col-md-3">Apply Date</td>
							<td class="col-md-9">{{ $data['other']['action_date'] }}</td>
						</tr>
						<tr>
							<td>Notes</td>
							<td>{{ $data['other']['notes'] }}</td>
						</tr>	
						<tr>
							<td>Attachment</td>
							<td>
								@if ($data['other']->AttAttachment)													
									<?php
										$attachment = $data['other']->AttAttachment->filename . '.' . $data['other']->AttAttachment->ext;
										$photo = route('lib.file.attendance.remark', array($attachment));
										$thumb = $data['other']->AttAttachment->thumb_name . '.' . $data['other']->AttAttachment->ext;
										$thumb_url = route('lib.file.attendance.remark.thumb', array($thumb));
									?>
									<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
								@else
									{{ '-' }}
								@endif								
							</td>
						</tr>
					</table>
				</div>
			</div>