<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">

      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
      <div class="modal-content">

         <div class="modal-body">
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="2"><strong>Punch @if ($type_id == 0) {{ 'In' }} @else {{ 'Out' }} @endif </strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="col-md-3">Date</td>
                     <td class="col-md-9">{{ $date }}</td>
                  </tr>
                  <tr>
                     <td>Time</td>
                     <td>
                     @if ($type_id == 0)
                        {!! $att->att_in !!}
                     @else
                        {!! $att->att_out !!}
                     @endif
                     </td>
                  </tr>
                  <tr>
                     <td>Transfer</td>
                     <td>{{ $att->att_timestamp }}</td>
                  </tr>
                  <tr>
                     <td>IP Address</td>
                     <td>{{ $att->ip_add }}</td>
                  </tr>
                  <tr>
                     <td>Status</td>
                     <td>{{ $att->att_sys_status }}</td>
                  </tr>
               </tbody>
            </table>
         </div>

      </div>
      {{ Form::close() }}

   </div>
</div>

