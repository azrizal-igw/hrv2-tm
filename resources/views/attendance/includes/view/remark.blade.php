	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table table-striped table-condensed table-responsive table-hover">
						<thead>
							<tr class="bg-inverse">
								<td colspan="2"><strong>Remark Info</strong></td>
							</tr>
						</thead>
						@if (!empty($data['remarks']->AttApplyRemark))
							<tr>
								<td class="col-md-3">Request Remark</td>
								<td class="col-md-9">
									{{ $data['remarks']->AttStatusName->name }}
								</td>
							</tr>
						@else
							<tr><td>Request Remark</td><td>-</td></tr>
						@endif


						@if ($data['remarks']->AttActiveStatus->status == 3)
							<tr>
								<td>Updated Remark</td>
								<td>
									{{ $data['remarks']->AttOtherName->name }}
								</td>
							</tr>
						@endif
						
						<tr>
							<td>Notes</td>
							<td>
								{{ $data['remarks']->notes }}
							</td>
						</tr>
						<tr>
							<td>Status Req. Remark</td>
							<td>
								@if ($data['remarks']->AttActiveStatus)
									@if ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 1)
										<?php $color = 'text-danger'; ?>
									@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 2)
										<?php $color = 'text-primary'; ?>
									@else
										<?php $color = 'text-info'; ?>									
									@endif
									<span class="{{ $color }}">{{ $data['remarks']->AttActiveStatus->AttRemarkStatusName->name }}</span>
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
						<tr>
							<td>Attachment</td>
							<td>
								@if ($data['remarks']->AttActiveAttachment)													
									<?php
										$attachment = $data['remarks']->AttActiveAttachment->filename . '.' . $data['remarks']->AttActiveAttachment->ext;
										$photo = route('lib.file.attendance.remark', array($attachment));
										$thumb = $data['remarks']->AttActiveAttachment->thumb_name . '.' . $data['remarks']->AttActiveAttachment->ext;
										$thumb_url = route('lib.file.attendance.remark.thumb', array($thumb));
									?>
									<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
								@else
									{{ '-' }}
								@endif																
							</td>
						</tr>						
						<tr>
							<td colspan="2">
								@if ($data['remarks']->AttActiveStatus)
									@if ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 1)
										{{ 'Note: This Request is waiting for Approval from Region Manager (RM).' }}
									@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 2)
										{{ 'Note: This remark is approved.' }}
									@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 3)
										{{ 'Note: This remark set by RM.' }}
									@endif
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>						
					</table>
				</div>
			</div>
		</div>
	</div>
