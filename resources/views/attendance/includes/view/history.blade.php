	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table table-striped table-condensed table-responsive table-hover">
						<thead>
							<tr class="bg-inverse">
								<td colspan="5"><strong>Status Remark</strong></td>
							</tr>
						</thead>						
						<tr class="active">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Status</th>
							<th class="col-md-2">Action Date</th>
							<th class="col-md-3">Action By</th>
							<th class="col-md-4">Remarks</th>
						</tr>
						@if (count($data['remarks']) > 0)
							<?php $no = 0; ?>
							@foreach ($data['remarks']->AttAllStatus as $r)
								<?php $no++; ?>
								<tr>
									<td>{{ $no }}</td>
									<td>{{ $r->AttRemarkStatusName->name }}</td>
									<td>{{ $r->action_date }}</td>
									<td>{{ $r->AttRemarkActionBy->name }}</td>
									<td>{{ $r->action_remark }}</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="5">No Request Record.</td>
							</tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
