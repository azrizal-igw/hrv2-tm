@extends('layouts/backend')

@section('content')

<!-- @include('includes/leaflet') -->

@include('attendance/includes/daily/search')

{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}             
<div class="row">
	<div class="col-sm-12">
		<div class="well">

			@include('attendance/includes/daily/table')

		</div>
	</div>
</div>
{{ Form::close() }}

@include('attendance/includes/daily/footer')

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-body">
            <div id="mapId"></div>
         </div>
      </div>

   </div>
</div>

<style>
#mapId {
  height: 200px;
}
</style>

@stop



