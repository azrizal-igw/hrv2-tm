@extends('layouts/backend')

@section('content')





{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-heading">
		<h4 class="panel-title">Select Leave Type</h4>
	</div>	
	<div class="panel-body nopadding">			
		<div class="form-group">
			@if ($errors->has('leave_type_id'))
				<p class="col-lg-12 text-danger">{{ $errors->first('leave_type_id') }}</p>
			@endif      
			<div class="col-lg-3">
				{{ Form::select('leave_type_id', $leave_types, null, array('class' => 'form-control')) }}
			</div>			
		</div>	
		*Please	read Remarks & Notes carefully before select the Leave Type.
	</div> 
	<div class="panel-footer">
		{{ Form::button('Select&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger']) }}
	</div>
</div>
{{ Form::close() }}  





<div class="well">
	<strong>Remarks:</strong>
	<br>1) Annual Leave - Any Leave. 
	<br>2) Medical Leave - Sick. Get MC from doctor.
	<br>3) Compassionate Leave - Somebody nearest is pass away. 
	<br><i>(Siblings/Father/Mother/In Law(Father/Mother)/Husband/Wife/Son/Daughter/Grandmother/Grandfather)</i>
	<br>4) Calamity Leave - Flood/Lanslide/Any Related.
	<br>5) Umrah Leave - Perform Umrah.
	<br>6) Replacement Leave - Given when work on previous date. RL Request already approved.
	<br>7) Haji Leave - Perform Hajj.
	<br>8) Maternity Leave - Give Birth.
	<br>9) Paternity Leave - Wife give birth.
	<br>10) Marriage Leave - Going to married.
	<br>11) Hospitalization - Admit to Hospital.
	<br>12) Unpaid Leave - Leave to deduct the salary.
	<br>13) Unplan Leave - Partner is on leave. Use if really emergency. (Allow only 3 times)
	<br>14) Prolong Illness Leave - Hospitalization exceed 2 months.
	<br>15) Unrecorded Leave - Priority to volunteer teams such as 'RELA' or 'Askar Wataniah' (which are subject to the Act only)<br><br>

	<strong>Notes:</strong>
	<br>1) All Leave must apply within the current Contract.
	<br>2) If got long leave (Maternity/Hospitalization) do not wait until the leave is over. 
	<br>3) Apply leave as soon as possible. The end of contract might near.
	<br>4) Any changes on Approved Leave must use 'Apply Cancel' 
	<br>5) Using any 'Borang' to change the Approved Leave is not allowed. Use this system to change the date.
	<br>6) If leave is approve please make sure it's true and valid.
	<br>7) Leave Balance is not carry forward when renewing the Contract.
	<br>8) Compassionate/Married/Paternity Leave must in sequence date. Cannot used multiple time of same event.
	<br>9) Check Leave Summary to know Total Entitled, Taken and Balance.
</div>



@stop