@extends('layouts/backend')

@section('content')

{{ Form::open(array('class' => 'form-inline', 'id' => 'form-list')) }} 
<div class="panel panel-primary">
	<div class="panel-body">

   		@include('leave/includes/view/leave')

   		@include('leave/includes/view/date')

   		@include('leave/includes/view/status')

   		@include('leave/includes/view/history')

	</div>

	<div class="panel-footer">
   		@include('leave/includes/view/footer')	
	</div>
	<div class="modal fade" id="leave-modal"></div>

</div>

<div class="well">
   <strong>Remarks</strong>
   <br>1) Rejected By Off Day might occur if Fall on Partner AL/RL Leave. Please double check with Partner.
   <br>2) If Apply Cancel (Cancel on Approved Leave) make sure the Date is not Outdated.
</div>

{{ Form::close() }}





<script type="text/javascript">
$(document).ready(function(){

	// save leave
    $(document).on('click','#leave-update',function(){
    	if ($('#remark').val() == "") {
    		alert('Please insert Remark.');
    	}
    	else {
	        var answer = confirm('Are you sure want to cancel this leave?');
	        if (answer == true) {
	            $('#form-list').submit(); 
	        }
	        else {
	            return false;
	        } 
	    }
    });	    

    // display popup modal
	$(document).on('click','#popup-modal',function() {  
		var type = $(this).attr('alt');		
		str =  '';   		
		str += '<div class="modal-dialog">';
		str += '	<div class="modal-content">';
		str += '		<div class="modal-header">';
		str += '			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		str += '			<legend>Remarks</legend>';
		str += '		</div>';
		str += '		<div class="modal-body">';
		str += '			<p><textarea name="remark" rows="4" cols="30" id="remark" placeholder="Insert Remark Here"></textarea></p><input type="hidden" name="type" value="' + type + '" />';
		str += '		</div>';
		str += '		<div class="modal-footer">';
		str += '			<div class="btn-group">';
		str += '				<button type="button" class="btn btn-primary" id="leave-update">Submit</button>';
		str += '				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
		str += '			</div>';
		str += '		</div>';
		str += '	</div>';
		str += '</div>';
		$('#leave-modal').html(str);                                                    
		$('#leave-modal').modal();   
	}); 

});  
</script>




@stop



