		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Staff Name</td>
						<td class="col-md-9">{{ $detail->LeaveUserDetail->name }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $detail->LeaveUserDetail->icno ? $detail->LeaveUserDetail->icno : '-' }}</td>
					</tr>
					<tr>
						<td>Staff ID</td>
						<td>{{ $detail->LeaveUserDetail->staff_id ? $detail->LeaveUserDetail->staff_id : '-' }}</td>
					</tr>					
					<tr>
						<td>Position</td>
						<td>
						@if ($detail->LeaveUserDetail->UserLatestJob)
							{{ $detail->LeaveUserDetail->UserLatestJob->PositionName->name }}
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>
					<tr>
						<td>Site Name</td>
						<td>
						@if ($detail->LeaveSiteName)
							{{ $detail->LeaveSiteName->code }} - {{ $detail->LeaveSiteName->name }}
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>
					<tr>
						<td>Reporting Officer</td>
						<td>{{ ($detail->LeaveReportToName) ? $detail->LeaveReportToName->name : '-' }}</td>
					</tr>					
				</table>
			</div>
		</div>