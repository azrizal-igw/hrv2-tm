		<div class="row">
			<div class="col-md-12">	
				<table class="table table-striped table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Status</th>
							<th class="col-md-2">Action Date</th>
							<th class="col-md-3">Action By</th>
							<th class="col-md-4">Remarks</th>
						</tr>
					</thead>
					@if (count($detail->LeavePrevHistory) > 0)
					<?php $no = 0; ?>
						@foreach ($detail->LeavePrevHistory as $i)
						<?php $no++; ?>
						<tr>
							<td>{{ $no }}</td>
							<td>{{ $i->LeaveStatusName->name }}</td>
							<td>{{ $i->action_date }}</td>
							<td>{{ $i->LeaveActionByName->name }}</td>
							<td>{{ $i->action_remark }}</td>
						</tr>
						@endforeach
					@else
						<tr>
							<td colspan="5">No previous record</td>
						</tr>
					@endif
				</table>
			</div>
		</div>