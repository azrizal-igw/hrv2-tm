		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Status Info</strong></td>
						</tr>
					</thead>				
					<tr>
						<td class="col-md-3">Status</td>
						<td class="col-md-9">
							@if ($detail->LeaveLatestHistory)
								<span class="text-{!! $detail->LeaveLatestHistory->availability !!}">{{ $detail->LeaveLatestHistory->LeaveStatusName->name }}</span>
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Action Date</td>
						<td>{{ $detail->LeaveLatestHistory->action_date }}</td>
					</tr>	
					<tr>
						<td>Action By</td>
						<td>{{ $detail->LeaveLatestHistory->LeaveActionByName->name }}</td>
					</tr>	

					@if ($detail->LeaveLatestHistory->status != 1)					            
						<tr>
							<td>Remarks</td>
							<td>{{ $detail->LeaveLatestHistory->action_remark }}</td>
						</tr>
					@endif
					
					<tr>
						<td colspan="2">
							{!! $detail->LeaveLatestHistory->LeaveStatusName->note !!}						
						</td>
					</tr>
				</table>
			</div>
		</div>