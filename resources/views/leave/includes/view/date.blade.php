		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Apply Date</strong></td>
						</tr>
					</thead>				
					<tr>
						<td class="col-md-3">Total Day</td>
						<td class="col-md-9">
							@if ($detail->is_half_day == 1)
								{{ '0.5' }}
							@else
								{{ count($detail->LeaveDateAll) }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Available Day</td>
						<td>
							@if ($detail->is_half_day == 1)
								@if (count($detail->LeaveDateAvail) > 0)
									{{ '0.5' }}
								@else
									{{ 'Empty' }}
								@endif
							@else
								@if (count($detail->LeaveDateAvail) > 0)
									{{ count($detail->LeaveDateAvail) }}
								@else
									{{ 'Empty' }}
								@endif
							@endif
						</td>
					</tr>	
					<tr>
						<td>Date</td>
						<td>
							@if (!in_array($detail->leave_type_id, array(5,7,8,11,12)))
								@if ($detail->LeaveDateAll)
									@foreach ($detail->LeaveDateAll as $i)
										@if ($i->leave_type == 1)
											<?php $label = 'check-circle text-success'; $title = 'Available'; ?>										
										@elseif ($i->leave_type == 7)
											<?php $label = 'times-circle text-danger'; $title = 'Public Holiday'; ?>
										@else
											<?php $label = 'minus-circle text-danger'; $title = 'Off Day M/AM'; ?>
										@endif
										<i class="fa fa-{{ $label }}" title="{{ $title }}"></i>&nbsp;{{ '['.$i->leave_date.']' }}
									@endforeach
								@else
									{{ '-' }}
								@endif
							@else
								<i class="fa fa-check-circle text-success" title="Available"></i>&nbsp;[{{ $detail->date_from }}]&nbsp;to&nbsp;<i class="fa fa-check-circle text-success" title="Available"></i>&nbsp;[{{ $detail->date_to }}]
							@endif							
						</td>
					</tr>
					<tr>
						<td colspan="2">
							Note: <i class="fa fa-times-circle text-danger"></i>&nbsp;Public Holiday&nbsp;<i class="fa fa-minus-circle text-danger"></i>&nbsp;Off Day M/AM&nbsp;<i class="fa fa-check-circle text-success"></i>&nbsp;Available
						</td>
					</tr>				            
				</table>
			</div>
		</div>
