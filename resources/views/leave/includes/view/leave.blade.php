		<div class="row">
			<div class="col-md-12">	
				<table class="table table-hover table-condensed table-responsive table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Leave Details</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Date Apply</td>
						<td class="col-md-9">{{ $detail->date_apply }}</td>
					</tr>
					<tr>
						<td>Leave Type</td>
						<td>{{ $detail->LeaveTypeName->name }}</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>{{ $detail->desc }}</td>
					</tr>
					<tr>
						<td>Start Date</td>
						<td>{{ $detail->date_from }}</td>
					</tr>
					<tr>
						<td>End Date</td>
						<td>{{ $detail->date_to }}</td>
					</tr>
					<tr>
						<td>Is Half Day</td>
						<td>
						@if ($detail->is_half_day == 1)
							{{ 'Yes' }}
						@else
							{{ 'No' }}
						@endif							
						</td>
					</tr>					

					<tr>
						<td>Current Attachment</td>
						<td>
						@if ($detail->LeaveLatestAttachment)
							<?php
								$attachment = $detail->LeaveLatestAttachment->filename . '.' . $detail->LeaveLatestAttachment->ext;
								$photo = route('lib.file.leave', array($attachment));
								$thumb = $detail->LeaveLatestAttachment->thumb_name . '.' . $detail->LeaveLatestAttachment->ext;
								$thumb_url = route('lib.file.leave.thumb', array($thumb));
							?>
							<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>

					@if (auth()->user()->group_id != 3)
						@if ($total['status'] == 1)
							<tr>
								<td>Total Annual (AL)</td>
								<td><i class="fa fa-book text-default" title="Entitled"></i> [Entitled: {{ $total['entitled'] }}] <i class="fa fa-check-circle text-success" title="Taken"></i> [Taken: {{ $total['taken'] }}] <i class="fa fa-pie-chart text-primary" title="Balance"></i> [Balance: {{ $total['balance'] }}] </td>
							</tr>
						@endif
					@endif
				</table>
			</div>
		</div>