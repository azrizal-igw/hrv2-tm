		@if (auth()->user()->group_id == 3) 


		<div class="row">
			<div class="col-md-6"> 
				<a href="{{ route('sv.leave.attachment.edit', array($detail->id)) }}" class="btn btn-primary" title="Edit Attachment"><i class="icon-paper-clip"></i>&nbsp;Edit</a>
			</div>
			<div class="col-md-6 text-right">
				@if ($detail->LeaveLatestHistory->LeaveStatusName->id == 1) 
					{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Cancel Leave', 'alt' => 5)) }}
				@elseif ($detail->LeaveLatestHistory->LeaveStatusName->id == 2)
					{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Apply Cancel', 'alt' => 4)) }}
				@endif
			</div>
		</div>		

		@else

		<div class="row">
			<div class="col-md-6"> 
				<a href="{{ route('mod.leave.attachment', array($detail->id, $detail->user_id, $detail->sitecode)) }}" class="btn btn-primary" title="Edit Attachment"><i class="icon-paper-clip"></i>&nbsp;Edit</a>
				@if ($detail->leave_type_id == 12 && $detail->LeaveLatestHistory->status == 2)
					<button type="button" class="btn btn-warning" title="Download PDF" onclick="window.location='{{ route("mod.leave.print.pdf", array($detail->id, $detail->user_id, $detail->sitecode)) }}'"><i class="icon-printer"></i>&nbsp;Print</button>
				@endif
			</div>
			<div class="col-md-6 text-right">

				<!-- status leave pending -->
				@if ($detail->LeaveLatestHistory->LeaveStatusName->id == 1) 

					<!-- have available day -->
					@if (count($detail->LeaveDate) > 0)
						{{ Form::button('<i class="icon-check"></i>&nbsp;Approve', array('class' => 'btn btn-success', 'id' => 'popup-modal', 'title' => 'Approve Leave', 'alt' => 2, 'data-name' => 'Approve')) }}				
						{{ Form::button('<i class="icon-close"></i>&nbsp;Reject', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Reject Leave', 'alt' => 3, 'data-name' => 'Reject')) }}
					@endif

				<!-- status leave approved -->
				@elseif ($detail->LeaveLatestHistory->LeaveStatusName->id == 2)
					{{ Form::button('<i class="icon-trash"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Cancel Leave', 'alt' => 8, 'data-name' => 'Cancel')) }}

				<!-- status leave apply cancel -->
				@elseif ($detail->LeaveLatestHistory->LeaveStatusName->id == 4)
					{{ Form::button('<i class="icon-check"></i>&nbsp;Approve Cancel', array('class' => 'btn btn-success', 'id' => 'popup-modal', 'title' => 'Approve Cancel', 'alt' => 6, 'data-name' => 'Approve Cancel')) }}
					{{ Form::button('<i class="icon-close"></i>&nbsp;Reject Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Reject Cancel', 'alt' => 7, 'data-name' => 'Reject Cancel')) }}					
				@endif

			</div>
		</div>

		@endif


