

   @if (auth()->user()->group_id == 3)

      <table class="table table-condensed table-bordered">
         <tr class="bg-inverse">
            <td colspan="3">Staff Info</td>
         </tr>
         <tr>
            <td>NAME: <strong>{{ strtoupper(Auth::user()->name) }}</strong></td>
            <td>CONTRACT: <strong>{{ strtoupper(Carbon\Carbon::parse($contract->date_from)->format('d M Y')) }}</strong> - <strong>{{ strtoupper(Carbon\Carbon::parse($contract->date_to)->format('d M Y')) }}</strong></td>
            <td>WORK DURATION: <strong>{{ count($duration) }}</strong></td>
         </tr>
         <tr>
            <td>POSITION: <strong>{{ strtoupper($position) }}</strong></td>      
            <td>SITE: <strong>{!! strtoupper($sitename) !!}</strong></td>
            <td>JOIN DATE: <strong>{{ strtoupper(Carbon\Carbon::parse(session()->get('job')['join_date'])->format('d M Y')) }}</strong></td>
         </tr>   
      </table>

   @else

      <table class="table table-condensed table-bordered">
         <tr class="bg-inverse">
            <td colspan="3">Staff Info</td>
         </tr>
         <tr>
            <td>NAME: <strong>{{ strtoupper($info['name']) }}</strong></td>
            <td>CONTRACT: <strong>{{ strtoupper(Carbon\Carbon::parse($info['date_from'])->format('d M Y')) }}</strong> - <strong>{{ strtoupper(Carbon\Carbon::parse($info['date_to'])->format('d M Y')) }}</strong></td>
            <td>WORK DURATION: <strong>{{ count($duration) }}</strong></td>
         </tr>
         <tr>
            <td>POSITION: <strong>{{ strtoupper($info['position']) }}</strong></td>      
            <td>SITE: <strong>{{ strtoupper($info['sitename']) }}&nbsp;({{ strtoupper($info['sitecode']) }})</strong></td>
            <td>JOIN DATE: <strong>{{ strtoupper(Carbon\Carbon::parse($info['join_date'])->format('d M Y')) }}</strong></td>
         </tr>   
      </table>

   @endif