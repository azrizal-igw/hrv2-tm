   <table class="table table-condensed table-bordered table-responsive">
      <tr class="bg-inverse">
         <td>LEAVE TYPE</td>
         <td align="center">ENTITLED GIVEN</td>
         <td align="center">REIMBURSE LEAVE (+)</td>
         <td align="center">DEDUCT LEAVE (-)</td>
         <td align="center">LEAVE TAKEN</td>
         <td align="center">BALANCE</td>      
      </tr> 
      
      @if (count($lists) > 0)
         @foreach ($lists as $i)

            @if ($i['id'] == 6)
               @if ($i['entitled'] > 0)
                  <?php
                     $bg_entitled = '#1589FF';
                  ?>
               @endif
            @else
               <?php
                  $bg_entitled = '#F9F9F9';
               ?>
            @endif

            @if ($i['reimburse'] > 0)
               <?php
                  $bg_reimburse = '#00cc99';
               ?>
            @else
               <?php
                  $bg_reimburse = '#F9F9F9';
               ?>
            @endif

            @if ($i['deduct'] > 0)
               <?php
                  $bg_deduct = '#ff5050';
               ?>
            @else
               <?php
                  $bg_deduct = '#F9F9F9';
               ?>
            @endif

            @if ($i['taken'] > 0)
               <?php
                  $bg_taken = '#54c841';
               ?>
            @else
               <?php
                  $bg_taken = '#F9F9F9';
               ?>
            @endif

            <tr>
               <td>
                  {{ $i['name'] }}
               </td>

               @if (auth()->user()->group_id == 3)
                  <?php
                     $entitled = route('sv.leave.list', array($i['id'], 2));
                     $reimburse = route('sv.leave.list', array($i['id'], 4));
                     $deduct = route('sv.leave.list', array($i['id'], 3));
                     $taken = route('sv.leave.list', array($i['id'], 1));
                  ?>
               @else
                  <?php
                     $entitled = route('mod.leave.user.list', array($info['uid'], $info['sitecode'], $i['id'], 2, $contract_id));
                     $reimburse = route('mod.leave.user.list', array($info['uid'], $info['sitecode'], $i['id'], 4, $contract_id));
                     $deduct = route('mod.leave.user.list', array($info['uid'], $info['sitecode'], $i['id'], 3, $contract_id));
                     $taken = route('mod.leave.user.list', array($info['uid'], $info['sitecode'], $i['id'], 1, $contract_id));
                  ?>
               @endif

               <td class="text-center" style="background: {{ $bg_entitled }}">
                  @if ($i['entitled'] > 0 && $i['id'] == 6)
                     <a href="#" data-url="{{ $entitled }}" data-toggle="modal" id="list"><strong>{{ $i['entitled'] }}</strong></a>
                  @elseif ($i['id'] == 13)
                     {{ '-' }}
                  @else
                     {{ $i['entitled'] }}
                  @endif
               </td>

               <td class="text-center" style="background: {{ $bg_reimburse }}">
                  @if ($i['reimburse'] > 0)
                     <a href="#" data-url="{{ $reimburse }}" data-toggle="modal" id="list"><strong>{{ round($i['reimburse'], 1) }}</strong></a>
                  @elseif ($i['id'] == 13)
                     {{ '-' }}                     
                  @else
                     {{ $i['reimburse'] }}
                  @endif
               </td>

               <td class="text-center" style="background: {{ $bg_deduct }}">
                  @if ($i['deduct'] > 0)
                     <a href="#" data-url="{{ $deduct }}" data-toggle="modal" id="list"><strong>{{ round($i['deduct'], 1) }}</strong></a>
                  @elseif ($i['id'] == 13)
                     {{ '-' }}                     
                  @else
                     {{ $i['deduct'] }}
                  @endif
               </td>

               <td class="text-center" style="background: {{ $bg_taken }}">
                  @if ($i['taken'] > 0)
                     <a href="#" data-url="{{ $taken }}" data-toggle="modal" id="list"><strong>{{ round($i['taken'], 1) }}</strong></a>
                  @else
                     {{ round($i['taken'], 1) }}
                  @endif
               </td>

               <td class="text-center">
                  @if ($i['id'] == 13)
                     {{ '-'}}
                  @else
                     {{ $i['balance'] }}
                  @endif
               </td>

            </tr>
         @endforeach
      @endif
   </table>