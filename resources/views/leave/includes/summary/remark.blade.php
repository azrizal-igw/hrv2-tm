<div class="well">
   <strong>Remarks:</strong>
   <br><i class="glyphicon glyphicon-stop summary-taken"></i> Total Leave Taken
   <br><i class="glyphicon glyphicon-stop summary-entitled"></i> Total Entitled RL Leave
   <br><i class="glyphicon glyphicon-stop summary-reimburse"></i> Total Reimburse Leave (+)
   <br><i class="glyphicon glyphicon-stop summary-deduct"></i> Total Deduct Leave (-)
</div>