<div class="row">
   <div class="col-md-12">
      <div class="well">
         {{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
            <div class="row">
               <div class="col-md-12">

                  <div class="form-group">
                     <div class="col-md-3">
                     @if ($errors->has('contract_id'))
                        <p class="text-danger">{{ $errors->first('contract_id') }}</p>
                     @endif
                     {{ Form::select('contract_id', $contracts, $contract_id, array('class' => 'form-control')) }}
                     </div>
                  </div>

                  {{ Form::button('Search Contract', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}
               </div>          
            </div>
         {{ Form::close() }}
      </div>
   </div>            
</div>