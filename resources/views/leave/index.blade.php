@extends('layouts/backend')

@section('content')

<div class="row">
    <div class="col-sm-12">

        <div class="well">

            @php 
            $exist = 0; 
            @endphp

            @if (count($types) > 0)
            <table class="table table-striped table-condensed table-responsive">
                @foreach ($types as $t)
                <thead>
                    <tr><td colspan="10" align="center"><h6><strong>{{ $t->LeaveTypeName->name }}</strong></h6></td></tr>
                    <tr class="bg-primary">
                        <th>No</th>
                        <th class="text-center">Date Apply</th>
                        <th class="text-center">Start Date</th>
                        <th class="text-center">End Date</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">Available</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action Date</th>
                        <th class="text-right"><i class="icon-picture" title="Attachment"></i></th>
                        <th class="actions text-right">Actions</th>
                    </tr>
                </thead>

                @if (count($leaves) > 0)
                    @php
                    $exist = 1;
                    $no = 0;
                    $total = 0;
                    $pending = 0;
                    $approved = 0;
                    $rejected = 0;
                    $cancel = 0;
                    $taken = 0;
                    @endphp

                    @foreach ($leaves as $i)
                        @if ($i->leave_type_id == $t->leave_type_id)

                            @php
                            $no++;
                            $total = count($i->LeaveDateAll);
                            $avail = count($i->LeaveDate);
                            @endphp

                            @if ($i->is_half_day == 1) 
                                @php $tot = 0.5; @endphp
                                @if ($avail > 0) 
                                    @php $a = 0.5; @endphp
                                @else
                                    @php $a = 0; @endphp
                                @endif
                            @else
                                @php $tot = $total; @endphp
                                @if ($avail > 0) 
                                    @php $a = $avail; @endphp
                                @else
                                    @php $a = 0; @endphp
                                @endif
                            @endif

                            <tr>
                                <td>{{ $no }}</td>
                                <td class="text-center">
                                    @if (date('Y-m-d') == \Carbon\Carbon::parse($i->date_apply)->format('Y-m-d'))
                                        {{ 'Today' }}
                                    @else
                                        {{ \Carbon\Carbon::parse($i->date_apply)->format('d/m/Y') }}
                                    @endif
                                </td>

                                <td class="text-center">
                                    {{ \Carbon\Carbon::parse($i->date_from)->format('d/m/Y') }}
                                </td>

                                <td class="text-center">
                                    {{ \Carbon\Carbon::parse($i->date_to)->format('d/m/Y') }}
                                </td>

                                <td class="text-center">
                                    {{ $tot }}
                                </td>

                                <td class="text-center">
                                    {{ $a }}
                                </td>

                                <td class="text-center">
                                    @if ($i->LeaveLatestHistory->status == 1)
                                        <?php $color = '#ff0000';?>
                                    @elseif (in_array($i->LeaveLatestHistory->status, array(2,4,7)))
                                        <?php $color = '#177EE5'; $taken += $a;?>
                                    @else
                                        <?php $color = '#666666';?>
                                    @endif
                                    <span style="color: {{ $color }}">
                                        {{ $i->LeaveLatestHistory->LeaveStatusName->name }}
                                    </span>
                                </td>

                                <td class="text-center">
                                    @if (date('Y-m-d') == \Carbon\Carbon::parse($i->LeaveLatestHistory->action_date)->format('Y-m-d'))
                                        {{ 'Today' }}
                                    @else
                                        {{ \Carbon\Carbon::parse($i->LeaveLatestHistory->action_date)->format('d/m/Y') }}
                                    @endif
                                </td>

                                <td class="text-right">
                                    @if ($i->LeaveLatestAttachment)
                                        <?php
                                        $attachment = $i->LeaveLatestAttachment->filename . '.' . $i->LeaveLatestAttachment->ext;
                                        ?>
                                        <a href="{{ route('lib.file.leave', array($attachment)) }}" target="_blank" style="text-decoration: none; color: #666666;" title="View Attachment"><i class="icon-paper-clip"></i></a>
                                    @else
                                        {{ '-' }}
                                    @endif
                                </td>

                                <td class="text-right">
                                    <a href="{{ route('sv.leave.view', array($i->id)) }}" class="btn btn-success btn-sm" title="View Leave"><i class="icon-magnifier"></i></a>
                                </td>
                            </tr>
                        @endif
                    @endforeach

                @if ($no > 0)
                    <tr class="success">
                        <td colspan="5" class="text-left">Total Taken</td>
                        <td class="text-center"><strong>{{ $taken }}</strong></td>
                        <td colspan="4"></td>
                    </tr>
                @endif

                @else
                    <tr><td colspan="10">No record</td></tr>
                @endif

                <tr><td colspan="10">&nbsp;</td></tr>
                @endforeach
            </table>

            @else
                {{ 'No Record' }}
            @endif

        </div>


        @if ($exist == 1)
            <div class="well">
                <strong>Leave Status</strong>
                <br><i class="glyphicon glyphicon-stop red-light"></i> Pending - Site Supervisor Apply the Leave.
                <br><i class="glyphicon glyphicon-stop blue"></i> Approved - Region Manager (RM) Approve the Apply Leave.
                <br><i class="glyphicon glyphicon-stop maroon-light"></i> Rejected - RM Reject the Apply Leave.
                <br><i class="glyphicon glyphicon-stop grey-light"></i> Cancel - Site Supervisor Cancel the Leave before RM Approve/Reject the Leave.
                <br><i class="glyphicon glyphicon-stop brown-light"></i> Apply Cancel - Leave is Approved but Site Supervisor Cancel the Leave. Wait RM to Approve/Reject the Request.
                <br><i class="glyphicon glyphicon-stop green-light"></i> Approved Cancel - RM Approve the Apply Cancel.
                <br><i class="glyphicon glyphicon-stop green-dark"></i> Rejected Cancel - RM Reject the Apply Cancel.
                <br><br>
                <strong>Remarks:</strong>
                <br>1) Status 'Apply Cancel' or 'Rejected Cancel' still count as Taken.
                <br>2) Please Apply new Leave if request is Rejected (Case of RM ask for attachment/Wrong Info).
                <br>3) Double check with RM if there's no action.
                <br>4) Unplan Leave can apply only 3 times.
            </div>
        @endif


    </div>
</div>



@stop