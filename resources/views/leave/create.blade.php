@extends('layouts/backend')

@section('content')



<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">





<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Staff Info</legend>
				<div class="row">
					<dl class="dl">
						<dt class="col-lg-3">Date Apply</dt>
						<dd class="col-lg-9">{{ date('d/m/Y') }}</dd>
						<dt class="col-lg-3">Name</dt>
						<dd class="col-lg-9">{{ auth()->user()->name }}</dd>
						<dt class="col-lg-3">Position</dt>
						<dd class="col-lg-9">{{ session()->get('position_name') }}</dd>
						<dt class="col-lg-3">Site Name</dt>
						<dd class="col-lg-9">{{ auth()->user()->sitecode }} - {{ session()->get('site_name') }}</dd>
						<dt class="col-lg-3">Reporting Officer</dt>
						<dd class="col-lg-9">{{ session()->get('rm_info')['name'] }}</dd>
					</dl>
				</div>
				<br>

				<legend>Leave Info</legend>
				<div class="row">
					<dl class="dl">
						<dt class="col-lg-3">Leave Name</dt>
						<dd class="col-lg-9">{{ $check['leave_type']->name.' ('.$check['leave_type']->code.')' }}</dd>
						<dt class="col-lg-3">Total</dt>
						<dd class="col-lg-9">
							<i class="fa fa-book text-default" title="Entitled"></i> [Entitled: {{ round($check['leave_total'], 1) }}] <i class="fa fa-check-circle text-success" title="Taken"></i> [Taken: 
							@if (is_float($check['leave_taken']))
								{{ $check['leave_taken'] }}
							@else
								{{ floatval($check['leave_taken']) }}
							@endif
							] <i class="fa fa-pie-chart text-primary" title="Balance"></i> [Balance: {{ $check['leave_balance'] }}]
						</dd>

					</dl>
				</div>

			</div>
		</div>
	</div>
</div>





{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">
		
		<div class="panel panel-default">
			<div class="panel-body">

				<fieldset>
					<legend>New Leave</legend>
					<div class="form-group">
						@if ($errors->has('desc'))
							<p class="col-lg-12 text-danger">{{ $errors->first('desc') }}</p>
						@endif  					
						<div class="required"> 
							<label class="col-lg-3 control-label" for="textArea">Description</label>
						</div>
						<div class="col-lg-4">
							{{ Form::textarea('desc', old('desc'), ['class' => 'form-control', 'size' => '30x3']) }}
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('date_from'))
							<p class="col-lg-12 text-danger">{{ $errors->first('date_from') }}</p>
						@endif  					
						<div class="required"> 
							<label class="col-lg-3 control-label" for="select">Start Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="pick_start_date">
								{{ Form::text('date_from', old('date_from'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'date1', 'readonly')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>	
						</div>
					</div>

					@if ($check['to'] != 1)
					<div class="form-group">
						@if ($errors->has('date_to'))
							<p class="col-lg-12 text-danger">{{ $errors->first('date_to') }}</p>
						@endif  					
						<div class="required"> 
							<label class="col-lg-3 control-label" for="select">End Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="pick_end_date">
								{{ Form::text('date_to', old('date_to'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'date2', 'readonly')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>	
						</div>
					</div>							
					@endif

					<div class="form-group">
						<label class="col-lg-3 control-label" for="select">Half Day</label>
						<div class="col-lg-3">
							<div class="checkbox">
								<label>
									{{ Form::checkbox('is_half_day', 1) }}&nbsp;Please tick if required
								</label>
							</div>							
						</div>
					</div>

					<div class="form-group" id="i2">
						@if ($errors->has('leave_file'))
							<p class="col-lg-12 text-danger">{{ $errors->first('leave_file') }}</p>
						@endif  					
						<label class="col-lg-3 control-label" for="textArea">Attachment</label>
						<div class="col-lg-9">
							<label>
								{{ Form::file('leave_file') }}
							</label>
						</div>
					</div>
				</fieldset>
			</div>

			<div class="panel-footer">  

				<div class="row">
					<div class="col-md-6"> 
						{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('sv.leave.select')]) }} 
					</div>
					<div class="col-md-6 text-right">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
					</div>
				</div>

			</div>
		</div>

	</div>
</div>
{{ Form::hidden('leave_type_id', Session::get('leave_type_id')) }} 		
{{ Form::close() }} 





<div class="well">
	<strong>Remarks: </strong>					
	<br>1) Please ensure the correct info before submittng this leave. 
	<br>2) End Date for Haji/Umrah/Maternity/Paternity is calculate automatically. 
	<br>3) File attachment must be in following types jpeg/jpg/png and below 2MB.
	<br>4) Leave MC/Compassionate/Calamity/Hospitalization/Paternity/Maternity/Umrah/Haji/Married/Unplan must have attachment.
	<br>5) Unplan Leave take balance from Annual Leave.
	<br>6) Request Date must not fall on Partner Leave.	
</div>





<script type="text/javascript">
$(function () {
	getDatePicker('pick_start_date');
	getDatePicker('pick_end_date');     
});
</script>





@stop


