@extends('layouts/backend')

@section('content')




<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row col-wrap">	

			<div class="col-sm-6 col">
				<div class="row">
					<div class="col-md-12">

						<table class="table table-condensed table-responsive">
							<thead>
								<tr class="bg-danger">
									<td colspan="4"><i class="icon-event"></i>&nbsp;<strong>Public Holiday</strong></td>
								</tr>
							</thead>
							<tr class="active">
								<th class="col-md-1">No</th>
								<th class="col-md-5">Description</th>
								<th class="col-md-3">Date</th>
								<th class="col-md-3 text-left">States</th>
							</tr>							
							<tbody>
							@if (count($public) > 0)
								<?php $no_p = 0; ?>
								@foreach ($public as $p)
									<?php $no_p++; ?>
									<tr>
										<td>{{ $no_p }}</td>
										<td>{{ $p->desc }}</td>
										<td>{{ $p->date }}</td>
										<td class="text-left">
											@if (count($p->LeavePublicState) > 0)
												@if (count($p->LeavePublicState) == 16)
													{{ 'All' }}
												@else
													@if ($p->LeavePublicState)
														@foreach ($p->LeavePublicState as $s)
															{{ $s->StateName->name }}<br>
														@endforeach
													@else
														{{ '-' }}
													@endif
												@endif
											@endif
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="4">No record</td>
								</tr>
							@endif								
							</tbody>					
						</table>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col">
				<div class="row">
					<div class="col-md-12">

						<table class="table table-condensed table-responsive">
							<thead>
								<tr class="bg-warning">
									<td colspan="4"><i class="icon-event"></i>&nbsp;<strong>Off/Rest Day</strong></td>
								</tr>
							</thead>
							<tr class="active">
								<th class="col-md-1">No</th>
								<th class="col-md-5">Description</th>
								<th class="col-md-3">Date</th>
								<th class="col-md-3 text-left">States</th>
							</tr>
							<tbody>
							@if (count($off) > 0)
								<?php $no_o = 0; ?>
								@foreach ($off as $o)
									<?php $no_o++; ?>
									<tr>
										<td>{{ $no_o }}</td>
										<td>
											@if ($o->OffTypeName)
												{{ $o->OffTypeName->name }}
											@else
												{{ '-' }}
											@endif								
										</td>
										<td>{{ $o->off_date }}</td>
										<td class="text-left">
											@if (!empty($o->ListStates))
												@foreach ($o->ListStates as $k)
													{{ $k->LeaveOffStateName->name }}<br>
												@endforeach
											@else
												{{ '-' }}
											@endif										
										</td>
									</tr>								
								@endforeach
							@else
								<tr>
									<td colspan="3">No record</td>
								</tr>
							@endif									
							</tbody>						
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>


<div class="well">
	<strong>Remarks:</strong>
	<br>1) Off Day is 1st day (Saturday/Friday) meanwhile Rest Day is 2nd day (Saturday/Sunday)
	<br>2) If need to work on Off/Rest Day replace it with RL Request & RL Leave.
</div>



@stop

