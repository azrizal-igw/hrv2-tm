@extends('layouts/backend')


@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">                 
						<div class="col-md-2">
							{{ Form::select('year', $years, $sessions['year'], array('class' => 'form-control', 'title' => 'Year')) }}
						</div>                                                              
					</div> 

					<br>    
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}
				</div>          
			</div>

		</div>
	</div>            
</div>
{{ Form::close() }}




<?php
	$dates = $data['dates'];
	$od_m = array($data['od_m'], $data['types'][0]['name'], $data['types'][0]['color']);
	$od_am = array($data['od_am'], $data['types'][1]['name'], $data['types'][1]['color']);
	$odr_m = array($data['odr_m'], $data['types'][2]['name'], $data['types'][2]['color']);
	$odr_am = array($data['odr_am'], $data['types'][3]['name'], $data['types'][3]['color']);
	$odr = array($data['odr'], $data['types'][4]['name'], $data['types'][4]['color']);
	$oe = array($data['oe'], $data['types'][5]['name'], $data['types'][5]['color']);
	$og = array($data['og'], $data['types'][6]['name'], $data['types'][6]['color']);
	$public = array($data['public'], 'Cuti Umum', 'red');
	$today = date('Y-m-d');
?>




<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-bordered">
			<thead>
				<tr class="bg-primary">
					<td colspan="2">{{ strtoupper($state->name) }}</td>
				</tr>
			</thead>
			@for ($d = 1; $d <= 12; $d++)
				<tr>
					<td class="col-md-2">
						{{ strtoupper(\Carbon\Carbon::createFromFormat('!m', $d)->format('F')) }}
					</td>
					<td class="col-md-10">
						@foreach ($data['months'] as $k => $v)							
							@if ($k == $d)
								@foreach ($v[0] as $m)
									<?php
										$day = \Carbon\Carbon::parse($m)->format('l');
										$no = \Carbon\Carbon::parse($m)->format('j');
										$color = null;
										$title = null;										
									?>
									@if (in_array($m, $dates))

										@if (in_array($m, $public[0]))
											<?php $color = 'black'; $title = ''; ?>
										@endif

										@if (in_array($m, $od_m[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $od_m[2]; $title = $od_m[1]; ?>
											@endif										
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $od_am[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $od_am[2]; $title = $od_am[1]; ?>
											@endif										
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $odr_m[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $odr_m[2]; $title = $odr_m[1]; ?>
											@endif											
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $odr_am[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $odr_am[2]; $title = $odr_am[1]; ?>
											@endif											
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $oe[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $oe[2]; $title = $oe[1]; ?>
											@endif
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $og[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $og[2]; $title = $og[1]; ?>
											@endif
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $odr[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $odr[2]; $title = $odr[1]; ?>
											@endif										
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>
										@else
											<i class="glyphicon glyphicon-stop white-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="" data-day="{{ $day }}" data-no="{{ $no }}"></i>
										@endif

									@else
										@if (in_array($m, $public[0]))
											<?php $color = $public[2]; $title = $public[1]; ?>
											<a href="{{ route('sv.leave.calendar.date', array($m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>
										@else
											<i class="glyphicon glyphicon-stop white-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="" data-day="{{ $day }}" data-no="{{ $no }}"></i>
										@endif
									@endif

								@endforeach
							@endif
						@endforeach

					</td>
				</tr>
			@endfor

		</table>
	</div>
</div>




<div class="well">
	<strong>Legends: </strong>
	@if (!empty($data['types']))
		@foreach ($data['types'] as $t)
			<br><i class="glyphicon glyphicon-stop {{ $t['color'] }}"></i>&nbsp;{{ $t['name'] }}
		@endforeach
	@endif
	<br><i class="glyphicon glyphicon-stop red"></i>&nbsp;Cuti Umum  
</div>




@stop