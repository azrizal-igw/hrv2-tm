      <li class="">
         <a href="#" aria-expanded="true"><i class="icon-clock"></i>&nbsp;Attendance <span class="glyphicon arrow"></span></a>
         <ul aria-expanded="false" class="collapse" style="height: 0px;">
            <li><a href="{{ route('mod.attendance.site') }}"><i class="icon-share"></i>&nbsp;Site</a></li>
            <li><a href="{{ route('mod.attendance.remark') }}"><i class="icon-speech"></i>&nbsp;Remark</a></li>
            <li><a href="{{ route('mod.attendance.timeslip') }}"><i class="icon-doc"></i>&nbsp;Time Slip</a></li>
            <li><a href="{{ route('mod.attendance.mobile') }}"><i class="icon-screen-smartphone"></i>&nbsp;Mobile</a></li>
            <li class="">
               <a href="#" aria-expanded="false"><i class="icon-pencil"></i>&nbsp;Manual »</a>
               <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li><a href="{{ route('mod.attendance.manual.create') }}"><i class="icon-plus"></i>&nbsp;Add</a></li>
                  <li><a href="{{ route('mod.attendance.manual') }}"><i class="icon-list"></i>&nbsp;View</a></li>
               </ul>
            </li>
         </ul>
      </li>