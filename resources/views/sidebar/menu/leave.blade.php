      <li class="">
         <a href="#" aria-expanded="true"><i class="icon-login"></i>&nbsp;Leave Application <span class="glyphicon arrow"></span></a>
         <ul aria-expanded="false" class="collapse" style="height: 0px;">
            <li><a href="{{ route('mod.leave.index') }}"><i class="icon-grid"></i>&nbsp;Leave</a></li>
            <li><a href="{{ route('mod.leave.user.index') }}"><i class="icon-settings"></i>&nbsp;Manage Leave</a></li>
            <li><a href="{{ route('mod.leave.replacement.index') }}"><i class="icon-note"></i>&nbsp;RL Request</a></li>
            <li><a href="{{ route('mod.calendar.index') }}"><i class="icon-calendar"></i>&nbsp;Calendar</a></li>
         </ul>
      </li>