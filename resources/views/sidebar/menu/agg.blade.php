      <li class="">
         <a href="#" aria-expanded="true"><i class="icon-shuffle"></i>&nbsp;Aggregator <span class="glyphicon arrow"></span></a>
         <ul aria-expanded="false" class="collapse" style="height: 0px;">

            <li class="">
               <a href="#" aria-expanded="false"><i class="icon-people"></i>&nbsp;Staff »</a>
               <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li><a href="{{ route('mod.agg.staff.export') }}"><i class="icon-action-redo"></i>&nbsp;Export/Transfer</a></li>
                  <li><a href="{{ route('mod.agg.staff.list') }}"><i class="icon-check"></i>&nbsp;Check Transfer</a></li>
                  <li><a href="{{ route('mod.agg.staff.select') }}"><i class="icon-screen-desktop"></i>&nbsp;Call API</a></li>
                  <li><a href="{{ route('mod.agg.staff.manual.search') }}"><i class="icon-note"></i>&nbsp;Manual Send</a></li>
                  {{-- <li><a href="{{ route('mod.agg.staff.manual.list') }}"><i class="icon-list"></i>&nbsp;Lists Manual</a></li> --}}
               </ul>
            </li>

            <li class="">
               <a href="#" aria-expanded="false"><i class="icon-share"></i>&nbsp;Site »</a>
               <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li><a href="{{ route('mod.agg.site.sync') }}"><i class="icon-refresh"></i>&nbsp;Sync</a></li>
               </ul>
            </li>

            <li class="">
               <a href="#" aria-expanded="false"><i class="icon-clock"></i>&nbsp;Attendance »</a>
               <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li><a href="{{ route('mod.agg.attendance.select') }}"><i class="icon-wrench"></i>&nbsp;Process/Export/Reset</a></li>
                  <li><a href="{{ route('mod.agg.attendance.check') }}"><i class="icon-check"></i>&nbsp;Check Transfer</a></li>
               </ul>
            </li>

            <li><a href="{{ route('mod.agg.transfer') }}"><i class="icon-action-redo"></i>&nbsp;Transfer</a></li>
            
         </ul>
      </li>