      <li class="">
         <a href="#" aria-expanded="false"><i class="icon-earphones-alt"></i>&nbsp;Training <span class="glyphicon arrow "></span></a>
         <ul aria-expanded="false" class="collapse" style="height: 0px;">  
            <li><a href="{{ route('mod.training.index') }}"><i class="icon-list"></i>&nbsp;Training</a></li>
            <li><a href="{{ route('mod.training.create') }}"><i class="icon-plus"></i>&nbsp;Create</a></li>
         </ul>
      </li>