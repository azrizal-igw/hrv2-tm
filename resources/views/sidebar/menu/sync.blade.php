      <li class="">
         <a href="#" aria-expanded="true"><i class="icon-refresh"></i>&nbsp;Sync ERP <span class="glyphicon arrow"></span></a>
         <ul aria-expanded="false" class="collapse" style="height: 0px;">
			<li><a href="{{ route('mod.sync.staff') }}"><i class="icon-people"></i>&nbsp;Staff</a></li>
			<li><a href="{{ route('mod.sync.site') }}"><i class="icon-share"></i>&nbsp;Site</a></li>
			<li><a href="{{ route('mod.sync.public-holiday') }}"><i class="icon-event"></i>&nbsp;Public Holiday</a></li>            
         </ul>
      </li>            