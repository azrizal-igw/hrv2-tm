      <li class="">
         <a href="#" aria-expanded="false"><i class="icon-people"></i>&nbsp;Staff Administration <span class="glyphicon arrow "></span></a>
         <ul aria-expanded="false" class="collapse" style="height: 0px;">
            <li><a href="{{ route('mod.user.index') }}"><i class="icon-people"></i>&nbsp;Staff</a></li>
            @if (in_array(auth()->user()->group_id, array(1,2,4,7)))            
               <li><a href="{{ route('mod.user.photo') }}"><i class="icon-picture"></i>&nbsp;Photo</a></li>
            @endif
            @if (in_array(auth()->user()->group_id, array(1,2,5)))
               <li><a href="{{ route('mod.user.other') }}"><i class="icon-people"></i>&nbsp;Management</a></li>
            @endif
            @if (in_array(auth()->user()->group_id, array(1,2,4,5)))
               <li><a href="{{ route('mod.letter.index') }}"><i class="icon-envelope"></i>&nbsp;Letter</a></li>
            @endif
            @if (auth()->user()->group_id == 1)
               <li><a href="{{ route('mod.user.select') }}"><i class="icon-user-follow"></i>&nbsp;Add Staff</a></li>
            @endif
         </ul>
      </li>