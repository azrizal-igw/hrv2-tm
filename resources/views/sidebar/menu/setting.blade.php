      <li class="">
         <a href="#" aria-expanded="true"><i class="icon-wrench"></i>&nbsp;Settings <span class="glyphicon arrow"></span></a>
         <ul aria-expanded="true" class="collapse ">

            <li><a href="{{ route('mod.site.index') }}"><i class="icon-share"></i>&nbsp;Site</a></li>
            <li><a href="{{ route('mod.public-holiday.index') }}"><i class="icon-list"></i>&nbsp;Public Holiday</a></li>

            <li class="">
               <a href="#" aria-expanded="false"><i class="icon-close"></i>&nbsp;Off Day »</a>
               <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li><a href="{{ route('mod.off-day.index') }}"><i class="icon-close"></i>&nbsp;Off Day</a></li>
                  <li><a href="{{ route('mod.off-day.create') }}"><i class="icon-plus"></i>&nbsp;Add</a></li>
               </ul>
            </li>

            <li><a href="{{ route('mod.email.notification') }}"><i class="icon-envelope-open"></i>&nbsp;Email Notification</a></li>
            <li><a href="{{ route('mod.work-hour.index') }}"><i class="icon-hourglass"></i>&nbsp;Work Hour</a></li>

            @if (auth()->user()->is_admin == 1)
               <li class="">
                  <a href="#" aria-expanded="false"><i class="icon-feed"></i>&nbsp;Announcement »</a>
                  <ul aria-expanded="false" class="collapse" style="height: 0px;">
                     <li><a href="{{ route('mod.announcement.index') }}"><i class="icon-list"></i>&nbsp;View</a></li>
                     <li><a href="{{ route('mod.announcement.create') }}"><i class="icon-plus"></i>&nbsp;Add</a></li>
                  </ul>
               </li>

               <li class="">
                  <a href="#" aria-expanded="false"><i class="icon-pin"></i>&nbsp;Fasting »</a>
                  <ul aria-expanded="false" class="collapse" style="height: 0px;">
                     <li><a href="{{ route('mod.fasting.index') }}"><i class="icon-list"></i>&nbsp;View</a></li>
                     <li><a href="{{ route('mod.fasting.create') }}"><i class="icon-plus"></i>&nbsp;Add</a></li>
                  </ul>
               </li>
            @endif

         </ul>
      </li>