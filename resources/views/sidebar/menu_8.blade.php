      <li class="">
         <a href="#" aria-expanded="false"><i class="icon-clock"></i>&nbsp;Attendance <span class="glyphicon arrow "></span></a>
         <ul aria-expanded="false" class="collapse" style="height: 0px;">  
            <li><a href="{{ route('mod.attendance.site') }}"><i class="icon-share"></i>&nbsp;Site</a></li>
            <li><a href="{{ route('mod.attendance.mobile') }}"><i class="icon-screen-smartphone"></i>&nbsp;Mobile</a></li>            
         </ul>
      </li>

      @include('sidebar/menu/training')

