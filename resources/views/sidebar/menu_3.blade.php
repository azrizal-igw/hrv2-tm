      @if (in_array(session()->get('job')['position_id'], array(4,5,6)))
         <li class="">
            <a href="#" aria-expanded="true"><i class="icon-login"></i>&nbsp;Leave Application <span class="glyphicon arrow"></span></a>
            <ul aria-expanded="true" class="collapse ">
               <li class="">
                  <a href="#" aria-expanded="false"><i class="icon-login"></i>&nbsp;Leave »</a>
                  <ul aria-expanded="false" class="collapse" style="height: 0px;">
                     <li><a href="{{ route('sv.leave.summary') }}"><i class="icon-calculator"></i>&nbsp;Leave Summary</a></li>
                     <li><a href="{{ route('sv.leave.select') }}"><i class="icon-plus"></i>&nbsp;Add Leave</a></li>
                     <li><a href="{{ route('sv.leave.index') }}"><i class="icon-grid"></i>&nbsp;All Leave</a></li>
                  </ul>
               </li>
               <li class="">
                  <a href="#" aria-expanded="false"><i class="icon-note"></i>&nbsp;RL Request »</a>
                  <ul aria-expanded="false" class="collapse" style="height: 0px;">
                     <li><a href="{{ route('sv.leave.replacement.create') }}"><i class="icon-plus"></i>&nbsp;Add Request</a></li>
                     <li><a href="{{ route('sv.leave.replacement.index') }}"><i class="icon-list"></i>&nbsp;All Request</a></li>
                  </ul>
               </li>
               <li><a href="{{ route('sv.leave.calendar') }}"><i class="icon-calendar"></i>&nbsp;Calendar</a></li>             
            </ul>
         </li>
      @endif

      <li class="">
         <a href="#" aria-expanded="true"><i class="icon-clock"></i>&nbsp;Attendance <span class="glyphicon arrow"></span></a>
         <ul aria-expanded="true" class="collapse ">
            <li class="">
               <a href="{{ route('sv.attendance.daily') }}"><i class="icon-clock"></i>&nbsp;Daily Attendance</a>
            </li>
         </ul>
      </li>  

