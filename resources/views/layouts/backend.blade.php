<!DOCTYPE html>
<html lang="en">

<head>
   @include('includes/head')
</head>

<body>

   <div class="clearfix"> 
      <nav class="sidebar-nav">

         @include('includes/profile')

         <ul class="metismenu" id="menu2">
            <li class="">
               <a href="#" aria-expanded="false"><i class="icon-screen-desktop"></i> Home Page <span class="glyphicon arrow"></span></a>
               <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li><a href="{{ route('home') }}"><i class="icon-home"></i>&nbsp;Home</a></li>                 
               </ul>
            </li>
            @if (auth()->user()->is_admin == 1 && auth()->user()->id == 1) <!-- administrator -->
               @include('sidebar/menu_1') 
            @else
               @include('sidebar/menu_'.auth()->user()->group_id) 
            @endif
         </ul>

      </nav>
   </div>

   @include('includes/navigation')

   <section class="dashboard-content">

      @include('includes/breadcrumb')

      @if(Session::has('message'))
      <div class="alert alert-{{ Session::get('label') }} alert-dismissable" id="flashMessage">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         {{ Session::get('message') }}
      </div>
      @endif  

      @yield('content')

      <div class="divLoading"></div>  

      <div class="row">
         @include('includes/footer')
      </div>

   </section>

   <script src="{{ asset('/assets/js/backend.js') }}"></script>

   @include('includes/script')

</body>
</html>


