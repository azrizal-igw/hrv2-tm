<!DOCTYPE html>
<html lang="en">




<head>
   @include('includes/head')
</head>




<body>


    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    IHR V2
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Integrated HR System</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                          {{ Auth::user()->name }} <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <?php
                           if (auth()->user()->gender_id == 2) {
                              $icon = 'user-female';
                           }
                           else {
                              $icon = 'user';
                           }
                        ?>
                        <li><a href="{{ URL::route('auth.profile') }}"><i class="icon-{{ $icon }}"></i>&nbsp;View Profile</a></li>
                        @if (in_array(auth()->user()->group_id, array(1,2,4,5)))
                           <li><a href="{{ URL::to( '/assets/files/User_Manual_IHRV2.docx') }}" target="_blank"><i class="icon-book-open"></i>&nbsp;User Manual</a></li>  
                        @endif             
                        <li><a href="{{ URL::route('auth.password') }}"><i class="icon-pencil"></i>&nbsp;Change Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-off"></i>&nbsp;&nbsp;Logout</a></li>
                      </ul>
                  </li>
                </ul>
            </div>
        </div>
    </nav>






    <div class="container">
        <div class="row">


            <div class="col-md-3">
               <nav class="sidebar-nav">
                  <div class="profile-side">
                     <div class="details clear">

                        <span class="name">{{ auth()->user()->name }}</span>
                     </div>
                  </div>


               </nav>
            </div>



            <div class="col-md-9">



            </div>


        </div>
    </div>

    @include('includes/script')



</body>
</html>


