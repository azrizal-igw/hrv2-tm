   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="">
   <meta name="author" content="">

   <title>IHR V2</title>

   <!-- Javascript -->
   <script src="/assets/js/jquery.min.js"></script>
   <script src="/assets/js/bootstrap.min.js"></script>
   <script src="/assets/js/moment.js"></script>

   <!-- CSS -->
   <link href="/assets/css/bootstrap.css" rel="stylesheet">

   <!-- Fonts -->
   <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  -->
   <link href="/assets/css/font.css" rel="stylesheet">
   <link href="/assets/css/style.css" rel="stylesheet">

   <!-- Icons -->
   <link href="/assets/css/simple-line-icons.css" rel="stylesheet" type="text/css" >
   <link href="/assets/css/font-awesome.css" rel="stylesheet">
