         <div class="profile-side">
            <div class="details clear">
               <span class="images">

                  @if (auth()->user()->group_id == 3)
                     @if (session()->has('photo'))
                        @if (session()->get('photo')['photo_approved'])
                           <?php $photo = session()->get('photo')['photo_thumb'].'.'.session()->get('photo')['ext']; ?>
                           <a href="{{ route('auth.profile') }}"><img src="{{ route('lib.file.user.thumb', array($photo)) }}" class="img-responsive" title="View Profile"></a>
                        @else
                           <a href="{{ route('auth.profile') }}"><img src="{{ route('lib.image', array('profile.png')) }}" title="View Profile"></a>           
                        @endif                    
                     @else
                        <a href="{{ route('auth.photo') }}" title="Upload Photo"><img src="{{ route('lib.image', array('profile.png')) }}"> </a>               
                     @endif
                  @else
                     <img src="{{ route('lib.image', array('profile.png')) }}">                
                  @endif
               
               </span>
               <span class="name">{{ auth()->user()->name }}</span>
            </div>

            <div class="details">
               Position: {{ session()->get('position_name') }}
               @if (auth()->user()->group_id == 3)
                  <br>Site: {{ session()->get('site_name') }}
                  <br>Reporting Officer: {{ session()->get('rm_info')['name'] }}                  
               @elseif (in_array(auth()->user()->group_id, array(4,6,7)))
                  <br>Region: {{ session()->get('region_name') }}
               @endif
            </div>
         </div>
         <div class="modal fade" id="modal-profile"></div> 