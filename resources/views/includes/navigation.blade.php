   <div class="header-top">

      <nav class="navbar navbar-default">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="{{ route('home') }}">IHR System V2</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right">





                  @if (auth()->user()->group_id == 3)
                     <?php $n_data = []; $n_total = 0; ?>
                     @if (session()->has('photo'))
                        @if (!in_array(session()->get('photo')['photo_latest_status']['status'], array(1,2)))
                           <?php $n_data[] = array('title' => 'Upload Photo', 'route' => route('auth.photo')); $n_total++; ?>
                        @endif
                     @else
                        <?php $n_data[] = array('title' => 'Upload Photo', 'route' => route('auth.photo')); $n_total++; ?>
                     @endif
                     @if (auth()->user()->edit == 0)
                        <?php $n_data[] = array('title' => 'Edit Info', 'route' => route('auth.profile.edit')); $n_total++; ?>
                     @endif

                     @if ($n_total > 0)
                        <?php $n_color = 'danger'; $n_sup = "<sup class=\"text-danger\">$n_total</sup>"; ?>
                     @else
                        <?php $n_color = 'default'; $n_sup = ''; ?>
                     @endif

                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell text-{{ $n_color }}"></i>{!! $n_sup !!}&nbsp;&nbsp;Notification <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           @if (count($n_data) > 0)
                              @foreach ($n_data as $i)
                                 <li>
                                    <a href="{{ $i['route'] }}"><i class='fa fa-exclamation-circle text-danger'></i>&nbsp;&nbsp;{{ $i['title'] }}</a>
                                 </li>
                              @endforeach
                           @else
                              <li><a href="#">{{ 'No record' }}</a></li>                                                           
                           @endif

                        </ul>
                     </li>
                  @endif                  



                  @if (auth()->user()->group_id == 3)
                     @if (count(session()->get('letters')) > 0)
                        <?php 
                           $total = count(session()->get('letters')); 
                           $color = 'danger';
                        ?>
                     @else
                        <?php 
                           $total = 0;
                           $color = 'default';
                        ?>
                     @endif
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-envelope text-{{ $color }}"></i>
                           @if ($total != 0)
                              <sup class="text-danger">{{ $total }}</sup>
                           @endif
                           &nbsp;Letters <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                           <li>
                              @if (!empty(session()->get('letters')))
                                 @foreach (session()->get('letters') as $i)
                                    <?php $attachment = $i['letter_attachment']['filename'] . '.' . $i['letter_attachment']['ext'];?>
                                    <a href="#" data-url="{{ route('sv.letter.popup', array($i['id'])) }}" data-toggle="modal"><i class='fa fa-envelope-square'></i>&nbsp;&nbsp;{{ $i['letter_date'] }} - {{ CommonHelper::ordinal($i['reminder_no']) }} {{ $i['letter_type_name']['name'] }}</a>
                                 @endforeach
                              @else
                                 <a href="#">{{ 'No record' }}</a>
                              @endif
                           </li>
                        </ul>
                     </li>
                  @endif





                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i>&nbsp;&nbsp;Settings <span class="caret"></span></a>
                     <ul class="dropdown-menu">
                        <li><a href="{{ URL::route('auth.profile') }}"><i class='fa fa-user'></i>&nbsp;&nbsp;View Profile</a></li>
                        @if (auth()->user()->group_id == 3)
                           <li><a href="{{ route('auth.profile.edit') }}"><i class='fa fa-edit'></i>&nbsp;&nbsp;Edit Info</a></li>
                           <li><a href="{{ route('auth.photo') }}"><i class='fa fa-camera-retro'></i>&nbsp;&nbsp;Upload Photo</a></li>
                        @endif
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ URL::route('auth.password') }}"><i class='fa fa-key'></i>&nbsp;&nbsp;Change Password</a></li>
                     </ul>
                  </li>
                  <li><a href="{{ url('/logout') }}"><i class='fa fa-sign-out'></i>&nbsp;&nbsp;Logout</a></li>
               </ul>
            </div>
         </div>
      </nav>

   </div>



<script type="text/javascript">
$(document).ready(function(){
   $('[data-toggle="modal"]').click(function(e) {
      e.preventDefault();
      $(".divLoading").addClass('show');
      var url = $(this).attr('data-url');
      $.get(url, function(data) {
         $(data).modal();
         $(".divLoading").removeClass('show').addClass('hide');
      });
   });
});
</script>




