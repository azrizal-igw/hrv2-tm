@extends('layouts/backend')



@section('content')


<?php
   $approve = 0;
?>


<div class="row">
   <div class="col-sm-12">
      <table class="table table-striped table-condensed table-responsive">
         <thead>
            <tr class="bg-primary">
               <th class="col-md-1">No</th>
               <th class="col-md-1 text-center">Month</th>
               <th class="col-md-2 text-center">Total Day</th>
               <th class="col-md-2">Instructed By</th>
               <th class="col-md-2">Date Apply</th>
               <th class="col-md-2 text-center">Status</th>
               <th class="col-md-1 text-center"><i class="icon-picture" title="Attachment"></i></th>
               <th class="col-md-1 text-right">Actions</th>
            </tr>
         </thead>    

         @if (count($leaves) > 0)
            <?php $no = $leaves->firstItem() - 1; ?>
            @foreach ($leaves as $i)
               <?php 
                  $no++;                       
               ?>
               <tr>
                  <td>{{ $no }}</td>

                  <td class="text-center">
                     {{ \Carbon\Carbon::parse($i->year_month)->format('Y-m') }}
                  </td>

                  <td class="text-center">
                     {{ $i->no_day }}
                  </td>

                  <td>
                     {{ $i->instructed_by }}
                  </td>

                  <td>
                     {{ $i->date_apply }}
                  </td>

                  <td class="text-center">                     
                     @if ($i->LeaveRepLatestHistory->status == 1)
                        <?php $color = '#ff0000'; ?>
                     @elseif ($i->LeaveRepLatestHistory->status == 2)
                        <?php $color = '#177EE5'; $approve += $i->no_day; ?>                             
                     @else
                        <?php $color = '#666666'; ?>
                     @endif
                     <span style="color: {{ $color }}">{{ $i->LeaveRepLatestHistory->LeaveRepStatusName->name }}</span>
                  </td>

                  <td class="text-center">
                     @if ($i->LeaveRepLatestAttachment)
                        <?php
                           $attachment = $i->LeaveRepLatestAttachment->filename.'.'.$i->LeaveRepLatestAttachment->ext;
                        ?> 
                        <a href="{{ route('lib.file.leave', array($attachment)) }}" target="_blank" style="text-decoration: none; color: #666666;" title="View Attachment"><i class="icon-paper-clip"></i></a>
                     @else
                        {{ '-' }}
                     @endif
                  </td>

                  <td class="text-right">
                     <a href="{{ route('sv.leave.replacement.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
                  </td>
               </tr>
            @endforeach
         @else
            <tr>
               <td colspan="8">No record</td>
            </tr>
         @endif
      </table>



      <div class="well">
         <div class="paging text-center">  
            {{ $leaves->render() }}  
            <p>Total Entitled: {{ $approve }}</p>
         </div>
      </div>






      <div class="well">
         <strong>Request Status</strong>
         <br><i class="glyphicon glyphicon-stop red-light"></i> Pending - Site Supervisor Apply the Request.
         <br><i class="glyphicon glyphicon-stop blue"></i> Approved - Region Manager (RM) Approve the Request.
         <br><i class="glyphicon glyphicon-stop maroon-light"></i> Rejected - RM Reject the Request.
         <br><i class="glyphicon glyphicon-stop grey-light"></i> Cancel - Site Supervisor Cancel the Request before RM Approve/Reject the Request. 
         <br><i class="glyphicon glyphicon-stop brown-light"></i> Apply Cancel - Request is Approved but Site Supervisor Cancel the Request. Wait RM to Approve/Reject the Request.
         <br><i class="glyphicon glyphicon-stop green-light"></i> Approved Cancel - RM Approve the Apply Cancel. 
         <br><i class="glyphicon glyphicon-stop green-dark"></i> Rejected Cancel - RM Reject the Apply Cancel.
         <br><br>
         <strong>Remarks:</strong>
         <br>1) Status 'Apply Cancel' or 'Rejected Cancel' still count as Entitled.
         <br>2) Please Apply new Leave if request is Rejected (Case of RM ask for attachment/Wrong Info).
         <br>3) Double check with RM if there's no action.  
      </div>



   </div>
</div>





@stop

