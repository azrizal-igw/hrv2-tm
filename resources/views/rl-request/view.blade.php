@extends('layouts/backend')

@section('content')





{{ Form::open(array('class' => 'form-inline', 'id' => 'form-list')) }} 
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-hover table-condensed table-responsive">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Replacement Details</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Date Apply</td>
						<td class="col-md-9">{{ $detail->date_apply }}</td>
					</tr>
					<tr>
						<td>Total Day</td>
						<td>{{ $detail->no_day }}</td>
					</tr>
					<tr>
						<td>Year & Month</td>
						<td>{{ \Carbon\Carbon::parse($detail->year_month)->format('Y-m') }}</td>
					</tr>
					<tr>
						<td>Instructed By</td>
						<td>{{ $detail->instructed_by }}</td>
					</tr>
					<tr>
						<td>Location</td>
						<td>{{ $detail->location }}</td>
					</tr>
					<tr>
						<td>Reason</td>
						<td>{{ $detail->reason }}</td>
					</tr>
					<tr>
						<td>Notes</td>
						<td>{{ $detail->notes }}</td>
					</tr>
					<tr>
						<td>Current Attachment</td>
						<td>
						@if ($detail->LeaveRepLatestAttachment)
							<?php
								$attachment = $detail->LeaveRepLatestAttachment->filename . '.' . $detail->LeaveRepLatestAttachment->ext;
								$photo = route('lib.file.leave', array($attachment));
								$thumb = $detail->LeaveRepLatestAttachment->thumb_name . '.' . $detail->LeaveRepLatestAttachment->ext;
								$thumb_url = route('lib.file.leave.thumb', array($thumb));
							?>
							<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>

				</table>
			</div>
		</div>




		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Status Info</strong></td>
						</tr>
					</thead>				
					<tr>
						<td class="col-md-3">Status</td>
						<td class="col-md-9"><strong>{{ $detail->LeaveRepLatestHistory->LeaveRepStatusName->name }}</strong></td>
					</tr>
					<tr>
						<td>Action Date</td>
						<td>{{ $detail->LeaveRepLatestHistory->action_date }}</td>
					</tr>

					<tr>
						<td>Action By</td>
						<td>
							@if ($detail->LeaveRepLatestHistory->LeaveActionByName)
								{{ $detail->LeaveRepLatestHistory->LeaveActionByName->name }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>	

					@if ($detail->LeaveRepLatestHistory->status != 1)				            
						<tr>
							<td>Remarks</td>
							<td>
								@if ($detail->LeaveRepLatestHistory->action_remark)
									{{ $detail->LeaveRepLatestHistory->action_remark }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
					@endif
					
					<tr>
						<td colspan="2">
							@if ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 1) 
								{{ "Note: This RL Request is waiting for Approval from Region Manager (RM)."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 2)
								{{ "Note: This RL Request is Approved."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 3)
								{{ "Note: This RL Request is Rejected. Please contact Regional Officer for Further Information."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 4)
								{{ "Note: This RL Request already Approve but Canceled by Site Supervisor. Awaits Approval from Regional Officer."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 5)
								{{ "Note: This RL Request already Cancel."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 6)
								{{ "Note: This RL Request is Approved for Apply Cancel."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 7)
								{{ "Note: This RL Request is Rejected for Apply Cancel."}}
							@else
								{{ "Note: Unknown status" }}
							@endif							
						</td>
					</tr>
				</table>
			</div>
		</div>






		<table class="table table-striped table-condensed table-responsive table-hover">
			<thead>
				<tr class="bg-inverse">
					<th>No</th>
					<th>Status</th>
					<th>Action Date</th>
					<th>Action By</th>														
					<th>Remarks</th>
				</tr>
			</thead>
			@if (count($detail->LeaveRepPrevHistory) > 0)
			<?php $no = 0; ?>
				@foreach ($detail->LeaveRepPrevHistory as $i)
				<?php $no++; ?>
				<tr>
					<td>{{ $no }}</td>
					<td>{{ $i->LeaveRepStatusName->name }}</td>
					<td>{{ $i->action_date }}</td>
					<td>{{ $i->LeaveActionByName->name }}</td>
					<td>{{ ($i->action_remark) ? $i->action_remark : '-' }}</td>
				</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">No previous record</td>
				</tr>
			@endif				
		</table>





	</div>


	<div class="panel-footer">
		<div class="row">
			<div class="col-md-6"> 
				<a href="{{ route('sv.leave.replacement.attachment.edit', array($detail->id)) }}" class="btn btn-primary" title="Edit Attachment"><i class="icon-paper-clip"></i>&nbsp;Edit</a>
			</div>
			<div class="col-md-6 text-right">
				@if ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 1) 
					{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Cancel', 'alt' => 5)) }}
				@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 2)
					{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Apply Cancel', 'alt' => 4)) }}
				@endif
			</div>
		</div>
	</div>
	<div class="modal fade" id="leave-modal"></div>


</div>
{{ Form::close() }}






<script type="text/javascript">
$(document).ready(function(){


	// save leave
    $(document).on('click','#leave-update',function(){
    	if ($('#remark').val() == "") {
    		alert('Please insert Remark.');
    	}
    	else {
	        var answer = confirm('Are you sure want to Cancel this RL Request?');
	        if (answer == true) {
	            $('#form-list').submit(); 
	        }
	        else {
	            return false;
	        } 
	    }
    });	    


    // display popup modal
	$(document).on('click','#popup-modal',function() {  
		var type = $(this).attr('alt');		
		str =  '';   		
		str += '<div class="modal-dialog">';
		str += '	<div class="modal-content">';
		str += '		<div class="modal-header">';
		str += '			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		str += '			<legend>Remarks</legend>';
		str += '		</div>';
		str += '		<div class="modal-body">';
		str += '			<p><textarea name="remark" rows="4" cols="30" id="remark" placeholder="Insert Remark Here"></textarea></p><input type="hidden" name="type" value="' + type + '" />';
		str += '		</div>';
		str += '		<div class="modal-footer">';
		str += '			<div class="btn-group">';
		str += '				<button type="button" class="btn btn-primary" id="leave-update">Submit</button>';
		str += '				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
		str += '			</div>';
		str += '		</div>';
		str += '	</div>';
		str += '</div>';
		$('#leave-modal').html(str);                                                    
		$('#leave-modal').modal();   
	}); 



});  
</script>






@stop



