@extends('layouts/backend')


@section('content')



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Current Attachment</strong></td>
						</tr>
					</thead>

					@if (!empty($detail))
						<tbody>
							<tr>
								<td class="col-md-3">Date Upload</td>
								<td class="col-md-9">{{ $detail->created_at }}</td>
							</tr>
							<tr>
								<td>Type</td>
								<td>{{ $detail->ext }}</td>
							</tr>
							<tr>
								<td>Size</td>
								<td>{{ CommonHelper::bytesToHuman($detail->size) }}</td>
							</tr>
							<tr>
								<td>Upload By</td>
								<td>{{ $detail->UserDetail->name }}</td>
							</tr>
							<tr>
								<td>Attachment</td>
								<td>
									<?php 
										$attachment = $detail->filename.'.'.$detail->ext;
										$photo = route('lib.file.leave', array($attachment)); 
										$thumb = $detail->thumb_name.'.'.$detail->ext;
										$thumb_url = route('lib.file.leave.thumb', array($thumb)); 
									?>
									<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
								</td>
							</tr>
						</tbody>
					@else
						<tbody>
							<tr>
								<td colspan="2">No record</td>
							</tr>
						</tbody>
					@endif

				</table>
			</div>
		</div>



		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Size</th>
							<th class="col-md-3">Date Upload</th>
							<th class="col-md-4">Upload By</th>
							<th class="col-md-1 text-center">Type</th>
							<th class="col-md-1 text-right">Actions</th>
						</tr>
					</thead>
					<tbody>
						@if (!empty($inactive))
							<?php $no = 0; ?>
							@foreach ($inactive as $i)
								<?php 
									$no++; 
									$attachment = $i->filename.'.'.$i->ext;
									$photo = route('lib.file.leave', array($attachment));
								?>
								<tr>
									<td>{{ $no }}</td>
									<td>{{ CommonHelper::bytesToHuman($i->size) }}</td>
									<td>{{ $i->created_at }}</td>
									<td>{{ $i->UserDetail->name }}</td>
									<td class="text-center">{{ $i->ext }}</td>
									<td class="text-right">
										<a href="{{ $photo }}" target="_blank" class="btn btn-success btn-sm" title="View Attachment"><i class="icon-magnifier"></i></a>
									</td>
								</tr>
							@endforeach
							<tr>
								<td colspan="6" class="text-center">Total Previous Attachment: <strong>{{ $no }}</strong></td>
							</tr>
						@else
							<tr>
								<td colspan="6">No previous record</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="panel panel-primary">
	<div class="panel-body">
		<legend>Upload New</legend>
		<div class="row">           
			<div class="col-md-12"> 
				<div class="form-group">
					<div class="col-md-12">       
						@if ($errors->has('leave_file'))
							<p class="text-danger">{{ $errors->first('leave_file') }}</p>
						@endif                             
						{{ Form::file('leave_file') }}                                                    
					</div>
				</div>    
				<br><strong>Remarks: </strong>					
				<br>1) File attachment must be in following types jpeg/jpg/png and below 2MB.
				<br>2) Ex: image.png
				<br>3) Upload limit is 5. 				
				<br>4) If request is rejected, please apply new request.
			</div>       
		</div>  
	</div>

	<div class="panel-footer">  
		<div class="row">
			<div class="col-md-6">
				{{ Form::button('<i class="icon-arrow-up-circle"></i>&nbsp;Upload',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'upload']) }} 
			</div>
		</div>
	</div>
</div>		
{{ Form::close() }} 



@stop


