@extends('layouts/backend')

@section('content')




<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-hover table-condensed table-responsive">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Details</strong></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="col-md-3">Date Apply</td>
							<td class="col-md-9">{{ date('d/m/Y') }}</td>
						</tr>
						<tr>
							<td>Name</td>
							<td>{{ auth()->user()->name }}</td>
						</tr>
						<tr>
							<td>Position</td>
							<td>{{ session()->get('position_name') }}</td>
						</tr>
						<tr>
							<td>Site Name</td>
							<td>{{ auth()->user()->sitecode }} - {{ session()->get('site_name') }}</td>
						</tr>
						<tr>
							<td>Reporting Officer</td>
							<td>{{ session()->get('rm_info')['name'] }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>





{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">

			<div class="panel-body">
				<fieldset>
					<legend>RL Details</legend>
					<div class="form-group">
						@if ($errors->has('no_day'))
							<p class="col-lg-12 text-danger">{{ $errors->first('no_day') }}</p>
						@endif   	
						<div class="required"> 					
							<label class="col-lg-3 control-label">No. of Days</label>
						</div>
						<div class="col-lg-2">
							{{ Form::select('no_day', $arr[2], old('no_day'), array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="form-group">	
						@if ($errors->has('month'))
							<p class="col-lg-12 text-danger">{{ $errors->first('month') }}</p>
						@endif   
						@if ($errors->has('year'))
							<p class="col-lg-12 text-danger">{{ $errors->first('year') }}</p>
						@endif   															
						<div class="required"> 
							<label class="col-lg-3 control-label" for="textArea">Month & Year</label>
						</div>
						<div class="col-lg-4">
			               <div class="row">
			                  <div class="col-lg-6">{{ Form::select('month', $arr[1], date('m'), array('class' => 'form-control')) }}</div>  
			                  <div class="col-lg-6">{{ Form::select('year', $arr[0], date('Y'), array('class' => 'form-control')) }}</div>
			               </div>							
						</div>
					</div>

					<div class="form-group">	
						@if ($errors->has('date_work'))
							<p class="col-lg-12 text-danger">{{ $errors->first('date_work') }}</p>
						@endif  					
						<div class="required"> 
							<label class="col-lg-3 control-label" for="select">Work/Start Date</label>
						</div>
						<div class="col-lg-2">
							<div class="input-group date" id="pick_work_date">
								{{ Form::text('date_work', old('date_work'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'date1', 'readonly')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>	
						</div>
					</div>

					<div class="form-group">					
						<label class="col-lg-3 control-label" for="selectIns">Instructed By</label>
						<div class="col-lg-4">
							{{ Form::text('instructed_by', old('instructed_by'), array('class'=>'form-control', 'id' => 'selectIns', 'size' => 40)) }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label" for="selectLoc">Location</label>
						<div class="col-lg-4">
							{{ Form::text('location', old('location'), array('class'=>'form-control', 'id' => 'selectLoc', 'size' => 40)) }}
						</div>
					</div>

					<div class="form-group"> 
						<label class="col-lg-3 control-label" for="textArea">Reason</label>
						<div class="col-lg-4">
							{{ Form::textarea('reason', old('reason'), ['class' => 'form-control', 'size' => '30x3']) }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3 control-label" for="textArea">Notes</label>
						<div class="col-lg-4">
							{{ Form::textarea('notes', old('notes'), ['class' => 'form-control', 'size' => '30x3']) }}
						</div>
					</div>

					<div class="form-group" id="i2">
						@if ($errors->has('rep_file'))
							<p class="col-lg-12 text-danger">{{ $errors->first('rep_file') }}</p>
						@endif  		
						<div class="required"> 				
							<label class="col-lg-3 control-label" for="textArea">Attachment</label>
						</div>
						<div class="col-lg-4">
							<label>
								{{ Form::file('rep_file') }}
							</label>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="panel-footer">        
				{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
			</div>
		</div>
	</div>
</div>
{{ Form::close() }} 



<div class="well">
	<strong>Remarks: </strong>					
	<br>1) Use this request if previously work on your off day/public holiday.
	<br>2) If Approved, make sure Apply Replacement Leave before Contract is ended. It will not carry forward.
	<br>3) Attachment is compulsary. 
	<br>4) File Attachment must be in following types jpeg/jpg/png and below 2MB.	
	<br>5) If no Attachment, discuss with Region Manager which file to put or create any empty picture.
	<br>6) Only 1 request in a day. Other request please apply next day.
	<br>7) Work/Start Date must within Date Contract.
</div>


<script type="text/javascript">
$(function () {
	getDatePicker('pick_work_date');
});
</script>



@stop




