Off Day is Canceled!
<br><br>


Year: {{ $off['year'] }}<br>
Type: {{ $off['off_type_name']['name'] }}<br>
Date: {{ $off['off_date'] }}
<br><br>


Lists of States:<br>
@if (!empty($off['list_states']))
	@foreach ($off['list_states'] as $i)
		{{ $i['leave_off_state_name']['name'] }}<br>
	@endforeach
@endif
<br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>
