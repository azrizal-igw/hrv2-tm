Off Day is Added!
<br><br>

Year: {{ $year }}<br>
Type: {{ $type->name }}<br>
State:
@if (!empty($states))
	@foreach ($states as $s)
		[{{ $s->name }}]
	@endforeach
@else
	{{ '-' }}
@endif
<br><br>


Lists of Dates:<br>
@if (!empty($off))
	@foreach ($off as $i)
		Date: {{ $i->off_date }}<br>
	@endforeach
@else
	{{ '-' }}
@endif
<br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>
