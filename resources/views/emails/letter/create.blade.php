@if ($all == 1)

	Dear Site Supervisor,
	<br><br>


	There are new letter from MSD Management that need your considerations.
	<br><br>


@else
	There are new letter uploaded from IHR!
	<br><br>
@endif


Name: {{ $name }}<br>
Position: {{ $position }}<br>
Site Name: {{ $sitename }}
<br><br>


Letter Date: {{ $letter->letter_date }}<br>
Report By: {{ $report_by }}<br>	
Notes: {{ $letter->notes }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

