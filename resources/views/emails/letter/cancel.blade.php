Dear Site Supervisor,
<br><br>


{{ $letter_name }} is Canceled!
<br><br>


Letter Details<br>
Date: {{ $letter->letter_date }}<br>
Reminder No: {{ $letter->reminder_no }}<br>
Notes: {{ $letter->notes }}
<br><br>


Canceled By: {{ $updated_by }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

