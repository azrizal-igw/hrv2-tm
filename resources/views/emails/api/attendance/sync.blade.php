There are new synchronize from Mobile Attendance!
<br><br>


Sync Details<br>
Date: {{ $date }}<br>
Total: {{ $total }}<br>
Insert: {{ $insert }}


@if ($fail['total'] > 0)
	<br><br>Fail Details<br>
	Total: {{ $fail['total'] }}<br>
	@if (!empty($fail['data']))
		@foreach ($fail['data'] as $f)
			<?php   
				$type = ($f['type_id'] == 0 ? 'IN' : 'OUT');
			?>
			Log: {{ $f['datetime'] }} - {{ $type }}<br>
		@endforeach
	@endif
	<br>
@else
	<br><br>
@endif


Thank you.
<br><br>

