Dear Manager/Assistant Manager,
<br><br>


Your RL request is {{ $status }}!
<br><br>


Request Details<br>
Date Apply: {{ $rl->date_apply }}<br>
Total Day: {{ $rl->no_day }}<br>
Month & Year: {{ Carbon\Carbon::parse($rl->year_month)->format('F').' '.Carbon\Carbon::parse($rl->year_month)->format('Y') }}<br>
Work/Start Date: {{ $rl->date_work }}<br>
Instructed By: {{ $rl->instructed_by }}<br>
Location: {{ $rl->location }}<br>
Reason: {{ $rl->reason }}<br>
Notes: {{ $rl->notes }}<br><br>


RM Remarks: {{ $remark }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

