Dear Regional Manager,
<br><br>


There are new request for Replacement!
<br><br>


Staff Details<br>
Position: {{ $position_name }}<br>
Site Name: {{ $site_name }}
<br><br>


Request Details<br>
Date Apply: {{ $rl->date_apply }} <br>
Total Day: {{ $rl->no_day }}<br>
Month & Year: {{ Carbon\Carbon::parse($rl->year_month)->format('F').' '.Carbon\Carbon::parse($rl->year_month)->format('Y') }}<br>
Work/Start Date: {{ $rl->date_work }}<br>
Instructed By: {{ $rl->instructed_by }}<br>
Location: {{ $rl->location }}<br>
Reason: {{ $rl->reason }}<br>
Notes: {{ $rl->notes }}
<br><br>



Please process it as soon as possible. Remarks will label as 'Approved/Rejected By Email'.
<br><br>



Approve<br>
{{ $approve }}
<br><br>



Reject<br>
{{ $reject }}
<br><br>



Thank you.
<br><br>

