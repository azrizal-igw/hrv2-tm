Dear Manager/Assistant Manager,
<br><br>


Your apply cancel is {{ $status }}!
<br><br>


RL Details<br>
Date Apply: {{ $rl->date_apply }} <br>
Total Day: {{ $rl->no_day }}<br>
Month & Year: {{ $rl->year_month }}<br>
Instructed By: {{ $rl->instructed_by }}<br>
Location: {{ $rl->location }}<br>
Reason: {{ $rl->reason }}<br>
Notes: {{ $rl->notes }}<br><br>


Cancel Details<br>
Date Apply: {{ $history->action_date }}<br>
Remark: {{ $history->action_remark }}<br><br>


RM Remarks: {{ $remark }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

