Dear Site Supervisor,
<br><br>


There are new backdated leave from IHR!
<br><br>


Staff Details<br>
Name: {{ $name }}<br>
Position: <?php echo $position_name; ?><br>
Site Name: <?php echo $site_name; ?>
<br><br>


Leave Details<br>
Selected Contract: {{ $contract }} <br>
Leave Name: <?php echo $leave_name; ?><br>
Date From: <?php echo $leave->date_from; ?><br>
Date To: <?php echo $leave->date_to; ?><br>
Duration: <?php echo $duration; ?><br>
Description: <?php echo $leave->desc; ?>
<br><br>


Date Apply: <?php echo $leave->date_apply; ?><br>
Created By: <?php echo $created_by ?>
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

