Dear Regional Manager,
<br><br>


There are apply cancel leave from IHR!
<br><br>


Staff Details<br>
Position: <?php echo $position_name; ?><br>
Site Name: <?php echo $site_name; ?>
<br><br>


Leave Details<br>
Date Apply: <?php echo $leave->date_apply; ?> <br>
Leave Type: {{ $leave_name }}<br>
Date From: <?php echo $leave->date_from; ?><br>
Date To: <?php echo $leave->date_to; ?><br>
@if ($leave->is_half_day == 1)
	Available Day: 0.5<br>
@else
	Available Day: <?php echo $available; ?><br>
@endif
Description: <?php echo $leave->desc; ?>
<br><br>


Cancel Details<br>
Action Date:  {{ date('Y-m-d H:i:s') }}<br>
Remarks: {{ $remark }}
<br><br>


Please process it as soon as possible.
<br><br>


Link<br>
<?php echo $url; ?>
<br><br>


Thank you.
<br><br>

