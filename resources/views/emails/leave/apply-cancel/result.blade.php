Dear Manager/Assistant Manager,
<br><br>


Your apply cancel is <?php echo $cancel_status; ?>!
<br><br>


Leave Details<br>
Date Apply: <?php echo $leave->date_apply; ?><br>
Leave Type: <?php echo $leave_name; ?><br>
Date From: <?php echo $leave->date_from; ?><br>
Date To: <?php echo $leave->date_to; ?><br>
@if ($leave->is_half_day == 1)
	Available Day: 0.5<br>
@else
	Available Day: <?php echo $available; ?><br>
@endif
Description: <?php echo $leave->desc; ?><br><br>


Cancel Details<br>
Date Apply: <?php echo $history->action_date ?><br>
Remark: <?php echo $history->action_remark ?><br><br>


RM Remarks: <?php echo $remark; ?>
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

