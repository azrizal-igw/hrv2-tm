Process Staff Attendance is Completed!
<br><br>


Process Details<br>
Start Time: {{ $start }}<br>
End Time: {{ $end }}<br>


@if (!empty($sitecode))
	Sitecode: {{ $sitecode }}<br>
@endif


Start Date: {{ $start_date }}<br>
End Date: {{ $end_date }}


@if (!empty($total))
	<br>
	Total: {{ $total }}<br>
	Insert: {{ $insert }}<br>
	Update: {{ $update }}<br>
	Fail: {{ $fail }}<br>
	No Site: {{ $xsite }}<br><br>
@else
	<br><br>
@endif


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>
