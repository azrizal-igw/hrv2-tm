Export Staff Attendance is Completed!
<br><br>


Process Details<br>
Start Time: {{ $start }}<br>
End Time: {{ $end }}<br><br>
Start Date: {{ $start_date }}<br>
End Date: {{ $end_date }}<br>

@if (!empty($sitecode))
	Sitecode: {{ $sitecode }}<br>
@endif

Total: {{ $total }}<br>
Insert: {{ $insert }}<br>
Update: {{ $update }}<br>
Fail: {{ $fail }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

