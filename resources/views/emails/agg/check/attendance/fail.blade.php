Failed Transfer Attendance
<br><br>


Attendance Date: {{ $start }} to {{ $end }}<br>
Total: {{ $total }}<br><br>

<table class="table">
	<tr>
		<td>No</td>
		<td>Date</td>
		<td>Sitecode</td>
		<td>No IC</td>
		<td>Status</td>
		<td>Error</td>
	</tr>
	@if (!empty($fail))
		<?php $no = 0;?>
		@foreach ($fail as $i)
			<?php $no++;?>
			<tr>
				<td>{{ $no }}</td>
				<td>{{ $i['CheckIn_Date'] }}</td>
				<td>{{ $i['PI1M_REFID'] }}</td>
				<td>{{ $i['Staff_IC'] }}</td>
				<td>{{ $i['CheckIn_Status'] }}</td>
				<td>{{ $i['Error_Message'] }}</td>
			</tr>
		@endforeach
	@else
		<tr>
			<td colspan="6">No record</td>
		</tr>
	@endif
</table><br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

