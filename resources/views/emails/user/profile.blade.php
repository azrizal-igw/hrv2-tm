Staff Profile is Updated!
<br><br>


Staff Details<br>
Name: {{ $name }}<br>
IC No: {{ $icno }}<br>
Position: {{ $position }}<br>
Site Name: {{ $site_code }}&nbsp;{{ $site_name }}<br><br>


Changes<br>
Marital Status: {{ $marital->name }}<br>
Mobile No 2: {{ $new['hpno2'] }}<br>
Partner Name: {{ $new['partner_name'] }}<br>
Partner Phone: {{ $new['partner_phone'] }}<br>
Child No: {{ $new['child_no'] }}<br><br>


Correspondence Address<br>
Street 1: {{ $new['correspondence_street_1'] }}<br>
Street 2: {{ $new['correspondence_street_2'] }}<br>
Postcode: {{ $new['correspondence_postcode'] }}<br>
City: {{ $new['correspondence_city'] }}<br>
State: {{ $state->name }}
<br><br>


Thank you.
<br><br>

