User Job is Canceled!
<br><br>


Staff Details<br>
Name: {{ $job->UserDetail->name }}<br>
Site Name:
@if (!empty($job->SiteName))
	{{ $job->SiteName->code }} {{ $job->SiteName->name }}
@else
	{{ '-' }}
@endif
<br><br>


Job Details<br>
Staff ID: {{ $job->staff_id }}<br>
Join Date: {{ $job->join_date }}<br>
Position: {{ $job->PositionName->name }}<br>
Phase: {{ $job->PhaseName->name }}<br>
Region: {{ $job->RegionName->name }}<br>
Canceled By: {{ $job->UpdatedBy->name }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>
