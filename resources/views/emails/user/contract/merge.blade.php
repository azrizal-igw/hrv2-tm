User Contract is Merged!
<br><br>


Staff Details<br>
Name: {{ $contract->UserDetail->name }}<br>
Site Name:
@if (!empty($contract->SiteName))
	{{ $contract->SiteName->code }} {{ $contract->SiteName->name }}
@else
	{{ '-' }}
@endif
<br><br>


Contract Details<br>
Start Date: {{ $contract->date_from }}<br>
End Date: {{ $contract->date_to }}<br>
Merged By: {{ auth()->user()->name }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>
