Dear Site Supervisor,
<br><br>


There are new attendance manual from region!
<br><br>


<?php
	if ($manual->type_id == 0) {
		$type = 'In';
	}
	else {
		$type = 'Out';
	}
?>


Staff Details<br>
Name: <?php echo $user->name ?><br>
Site Name: <?php echo $site->name ?>&nbsp;<?php echo $site->code ?><br>
Position: <?php echo $position; ?>
<br><br>


Attendance Details<br>
Punch: <?php echo $type; ?><br>
Datetime: <?php echo $manual->att_log_time ?><br>
Remark: <?php echo $notes; ?> 
<br><br>


Apply Details<br>
Apply Date: <?php echo $manual->action_date; ?><br>
Created By: <?php echo $created->name; ?>
<br><br>


Note: This process is not recommended and site supervisor must punch in/out accordingly.
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

