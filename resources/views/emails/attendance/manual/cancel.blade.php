Attendance Manual is Canceled!
<br><br>


Staff Details<br>
Name: <?php echo $manual->UserInfo->name ?><br>
Site Name: <?php echo $manual->UserSite->name ?>&nbsp;<?php echo $manual->UserSite->code ?><br>
Position: <?php echo $manual->UserJob->PositionName->name; ?>
<br><br>


Attendance Details<br>
Type: 
@if ($manual->type_id == 0)
	{{ 'IN' }}
@else
	{{ 'OUT' }}
@endif<br>
Datetime: <?php echo $manual->att_log_time ?>
<br><br>


Cancel Details<br>
Action Date: <?php echo $manual->updated_at; ?><br>
Action By: <?php echo $manual->UserUpdatedBy->name; ?>
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

