Attendance Mobile is Canceled!
<br><br>


Attendance Details<br>
Latitude: {{ $mobile->latitude }}<br>
Longitude: {{ $mobile->longitude }}<br>
QR Code: {{ $mobile->qr_code }}<br>
Date Time: {{ $mobile->datetime }}<br>
Punch:  
@if ($mobile->in_out == 0)
	{{ 'IN' }}
@else
	{{ 'OUT' }}
@endif
<br>
Location: {{ $mobile->location }}<br>
Remark: {{ $mobile->remark }}<br>
Status: {{ $mobile->AttGpsStatus->name }}
<br><br>


Action By: {{ $updated_by->name }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

