Punch {{ $type }} Attendance
<br><br>


@if (!empty($region))
	Region: {{ $region }}<br>
@endif
Date: {{ $date }}<br>
Time: {{ $start }} to {{ $end }}<br>
Total: {{ $total }}
<br><br>


@if (!empty($att))
	<?php $invalid_ic = 0; $invalid_site = 0; ?>
	@foreach ($att as $i)
		@if (!empty($i['user_attendance_detail']))
			<?php $name = $i['user_attendance_detail']['name']; $rmk_name = null;?>
		@else
			<?php 
				$name = '-'; 
				$rmk_name = '<span style="color: #FF0000;">(IC No. not Exist)</span>'; 
				$invalid_ic++;
			?>
		@endif
		@if (!empty($i['user_attendance_site']))
			<?php $sname = $i['user_attendance_site']['name']; $rmk_sname = null;?>
		@else
			<?php 
				$sname = '-'; 
				$rmk_sname = '<span style="color: #FF0000;">(Sitecode not Exist)</span>';
				$invalid_site++;
			?>
		@endif
		IC No: {{ $i['att_mykad_ic'] }} {!! $rmk_name !!}<br>
		Name: {{ $name }}<br>
		Site Name: {{ $i['location_id'] }} {{ $sname }} {!! $rmk_sname !!}<br>
		Punch {{ $type }}: {{ Carbon\Carbon::parse($i['att_log_time'])->format('g:i:s a') }}<br>
		Transfer: {{ Carbon\Carbon::parse($i['att_timestamp'])->format('g:i:s a') }}<br>
		IP Address: {{ $i['ip_add'] }}<br><br>
	@endforeach
@endif


Invalid IC No: {{ $invalid_ic }}<br>
Invalid Sitecode: {{ $invalid_site }}<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

