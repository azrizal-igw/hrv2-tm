Attendance Summary
<br><br>

Region: {{ $region }}<br>
Date: {{ $date }}<br>
Staff: {{ $total }}
<br><br>

<?php $no = 0; $total_in = 0; $total_out = 0; $total_incom = 0; $total_abs = 0; ?>

@if (!empty($staff))
	@foreach ($staff as $i)
		<?php $no++; $in = '<span style="color: #FF0000;">Empty</span>'; $out = '<span style="color: #FF0000;">Empty</span>'; $have_in = 0; $have_out = 0; $remark = 0; ?>
		#: {{ $no }}<br>
		Site Name: {{ $i['sitecode'] }} {{ $i['sitename'] }}<br>
		Name: {{ $i['name'] }}<br>
		IC No: {{ $i['icno'] }}<br>
		Position: {{ $i['position'] }}<br>




		@if ($i['att']['in'] != 'Empty')
			<?php $in = $i['att']['in']; $total_in++; $have_in = 1; ?>
		@endif
		@if ($i['att']['out'] != 'Empty')
			<?php $out = $i['att']['out']; $total_out++; $have_out = 1; ?>
		@endif




		@if (!empty($i['remark'])) 
			<?php $remark = 1; ?>
		@else
			@if ($have_in == 1 && $have_out == 0 || $have_in == 0 && $have_out == 1)
				<?php $total_incom++; $remark = 2; ?>
			@endif

			@if ($have_in == 0 && $have_out == 0)	
				<?php $total_abs++; $remark = 3; ?>
			@endif		
		@endif



		In: {!! $in !!}<br>
		Out: {!! $out !!}<br>
		Remark: 
		@if ($remark == 1)
			{{ $i['remark'] }}
		@elseif ($remark == 2)
			<span style="color: #FF0000;">{{ 'In-Complete' }}</span>
		@elseif ($remark == 3)
			<span style="color: #FF0000;">{{ 'No Attendance' }}</span>			
		@else
			{{ 'Working' }}
		@endif<br>



		<br>
	@endforeach
@endif

Total Punch In: {{ $total_in }}<br>
Total Punch Out: {{ $total_out }}<br>
Total In-Complete: {{ $total_incom }}<br>
Total No Attendance: {{ $total_abs }}<br>
Total Leave: {{ $total_leave }}<br>
Total Off Day: {{ $total_off_day }}<br>
Total Public Holiday: {{ $total_public }}<br><br>

Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

