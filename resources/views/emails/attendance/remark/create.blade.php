Dear Regional Manager/Deputy Region Manager,
<br><br>


There are new request for Attendance Remark!
<br><br>


Staff Details<br>
Position: <?php echo $position_name; ?><br>
Site Name: <?php echo $site_name; ?>
<br><br>


Request Details<br>
Apply Date: <?php echo $remark->action_date; ?><br>
Attendance Date: <?php echo $remark->date; ?><br>
Time In: <?php echo $in; ?><br>
Time Out: <?php echo $out; ?><br>
Request Remarks: <?php echo $remark_status ?><br>
Notes: <?php echo $remark->notes; ?>
<br><br>


Please process the request as soon as possible.
<br><br>


Link<br>
<?php echo $url; ?>
<br><br>


Thank you.
<br><br>

