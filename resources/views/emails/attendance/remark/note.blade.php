Dear Manager/Assistant Manager,
<br><br>


Your remark note is updated!
<br><br>


Request Details<br>
Apply Date: {{ $remark->action_date }}<br>
Attendance Date: {{ $remark->date }}<br>
Time In: {{  $in }}<br>
Time Out: {{ $out }}<br>
Remarks: {{ $status }}
<br><br>


New Notes: {{ $remark->notes }}<br>
Updated By: {{ $updated_by }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

