Dear Manager/Assistant Manager,
<br><br>


Your attendance remark is {{ $status }}!
<br><br>


Request Details<br>
Apply Date: <?php echo $att_remark->action_date; ?><br>
Attendance Date: <?php echo $att_remark->date; ?><br>
Time In: <?php echo $in; ?><br>
Time Out: <?php echo $out; ?><br>
Request Remarks: <?php echo $att_remark_status ?><br>
Notes: <?php echo $att_remark->notes; ?>
<br><br>


@if ($status == 'Updated')
	Updated Remarks: <?php echo $other_remark ?><br>
	RM Remarks: <?php echo $remark; ?>
	<br><br>
@else
	RM Remarks: <?php echo $remark; ?>
	<br><br>
@endif


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

