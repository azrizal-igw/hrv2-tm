Dear Regional Manager/Deputy Region Manager,
<br><br>


There are new request for Range Remark!
<br><br>


Staff Details<br>
Position: <?php echo $position_name; ?><br>
Site Name: <?php echo $site_name; ?>
<br><br>


Remark Details<br>
Apply Date: <?php echo date('Y-m-d H:i:s'); ?><br>
Status: <?php echo $remark['status']->name ?><br>
Attendance Date:<br> 
<?php 
	if (!empty($dates)) {
		foreach ($dates as $date) {
			echo $date.'<br>';
		}
	}
?>
Notes: <?php echo $remark['notes']; ?>
<br><br>


Please process the request as soon as possible.
<br><br>


Link<br>
<?php 
	if (!empty($url)) {
		foreach ($url as $u) {
			echo $u.'<br>';
		}
	}
?>
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

