Attendance Time Slip is Canceled!
<br><br>

Time Slip Details<br>
Apply Date: {{ $timeslip->action_date }}<br>
Attendance Date: {{ $timeslip->date }}<br>
Notes: {{ $timeslip->notes }}<br>
Canceled By: {{ $timeslip->AttUpdatedByInfo->name }}
<br><br>

Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

