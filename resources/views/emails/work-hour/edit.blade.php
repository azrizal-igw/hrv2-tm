There are updated record for work hour!
<br><br>


Staff Details<br>
Name: {{ $user['name'] }}<br>
IC No: {{ $user['icno'] }}<br>
Position: {{ $position }}<br>
Site Name: {{ $site_name }}
<br><br>

Work Hour<br>
Start Date: {{ $work_hour['date_from'] }}<br>
End Date: {{ $work_hour['date_to'] }}<br>
Time In: {{ $work_hour['time_in'] }}<br>
Time Out: {{ $work_hour['time_out'] }}<br>
Off Day (Every Week): 
@if (!empty($days))
	<?php $no = 0; ?>
	@foreach ($days as $d)
		<?php $no++; ?>
		<br>{{ $no }}) {{ $d }}
	@endforeach 
@else
	{{ '-' }}
@endif
<br>
Reason: {{ $work_hour['reason'] }}
<br><br>

Created By: {{ $created_by }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

