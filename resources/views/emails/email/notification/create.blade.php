New Email Notification is Created!
<br><br>


Email Notification Details<br>
Name: {{ $data['name'] }}<br>
Email: {{ $data['email'] }}<br>
Type: {{ $data['type_id'] }}<br>
Created By: {{ $created_by }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

