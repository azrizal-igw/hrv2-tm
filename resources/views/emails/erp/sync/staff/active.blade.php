Staff Active is Synced!
<br><br>


Sync Details<br>
Start Process: <?php echo $start; ?><br>
End Process: <?php echo date('Y-m-d H:i:s'); ?><br>
Total: <?php echo $total; ?><br>
Skip: <?php echo $skip; ?><br>
Fail: <?php echo $fail; ?>
<br><br>


New Staff: <?php echo $insert_user; ?><br>
Update Staff: <?php echo $update_user; ?><br>
Inactive Staff: <?php echo $inactive_user; ?><br>
Insert History: <?php echo $insert_history; ?><br><br>

New Job: <?php echo $insert_job; ?><br>
Update Job: <?php echo $update_job; ?><br>
Inactive Job: <?php echo $inactive_job; ?><br>
Update Resign Date: <?php echo $update_resign; ?><br><br>

New Contract: <?php echo $insert_contract; ?><br>
Update Contract: <?php echo $update_contract; ?><br>
Inactive Contract: <?php echo $inactive_contract; ?>
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

