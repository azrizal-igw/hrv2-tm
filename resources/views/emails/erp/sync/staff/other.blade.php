Staff {{ $name }} is Synced!
<br><br>


Sync Details<br>
Start Process: {{ $start }}<br>
End Process: {{ date('Y-m-d H:i:s') }}<br>
Total: {{ $total }}<br>
Insert: {{ $insert }}<br>
Fail: {{ $fail }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

