New Fasting Date & Time is Created!
<br><br>


Fasting Details<br>
Year: {{ $data['year'] }}<br>
Start Date: {{ $data['start_date'] }}<br>
End Date: {{ $data['end_date'] }}<br>
Time In: {{ $data['start_time'] }}<br>
Time Out: {{ $data['end_time'] }}<br>
Created By: {{ $created_by }}
<br><br>


Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

