Public Holiday is Updated!
<br><br>

Year: {{ $public['year'] }}<br>
Description: {{ $public['desc'] }}<br>
Date: {{ $public['date'] }}<br>
Updated By: {{ $updated_by }}
<br><br>

States:<br>
@if (count($public['leave_public_state']) > 0)
	@foreach ($public['leave_public_state'] as $i)
		{{ $i['state_name']['name'] }}<br>
	@endforeach
@endif
<br>

Please check at IHR System
<br>
http://hrv2.msd.net.my
<br><br>


Thank you.
<br><br>

