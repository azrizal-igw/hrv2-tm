@extends('layouts.backend')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Profile</h4>
            </div>
            <div class="panel-body nopadding">

                @if (auth()->user()->group_id == 3)
                    @if ($data['approved'] == 1)
                        <a href="{{ route('lib.file.user', array($data['photo']->photo.'.'.$data['photo']->ext)) }}" target="_blank" title="Click to Enlarge"><img src="{{ route('lib.file.user.thumb', array($data['photo']->photo_thumb.'.'.$data['photo']->ext)) }}" class="img-thumbnail img-responsive"></a>
                    @else
                        <a href="{{ route('auth.photo') }}" title="Set Photo"><img src="{{ route('lib.image', array('default.png')) }}" class="img-thumbnail img-circle img-responsive"></a> 
                    @endif
                @else
                    <img src="{{ route('lib.image', array('default.png')) }}" class="img-thumbnail img-circle img-responsive">
                @endif
                <br><br>                

                <div class="row col-wrap">          
                    <div class="col-sm-6 col">
                        <div class="row">
                            <div class="col-md-12">

                                <table class="table table-condensed table-responsive">
                                    <thead>
                                        <tr class="bg-success">
                                            <td colspan="2"><i class="icon-user"></i>&nbsp;<strong>Personal Info</strong></td>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                        <tr>
                                            <td>Staff ID</td>
                                            <td>{{ auth()->user()->staff_id }}</td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-4">Full Name</td>
                                            <td class="col-md-8">{{ auth()->user()->name }}</td>
                                        </tr>
                                        <tr>
                                            <td>IC No</td>
                                            <td>{{ auth()->user()->icno }}</td>
                                        </tr>
                                        <tr>
                                            <td>Age</td>
                                            <td>
                                                <?php $age = date('Y') - date('Y', strtotime(auth()->user()->dob)); ?>
                                                {{ $age }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No</td>
                                            <td>{{ auth()->user()->hpno }}</td>
                                        </tr>
                                        <tr>
                                            <td>Mobile No 2</td>
                                            <td>{{ auth()->user()->hpno2 }}</td>
                                        </tr>                                        
                                        <tr>
                                            <td>Email</td>
                                            <td>{{ auth()->user()->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Marital Status</td>
                                            <td>
                                                @if ($marital)
                                                    {{ $marital->name }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nationality</td>
                                            <td>
                                                @if ($nationality)
                                                    {{ $nationality->eng }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Race</td>
                                            <td>
                                                @if ($race)
                                                    {{ $race->name }}
                                                @else
                                                    {{ '-' }}
                                                @endif                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Religion</td>
                                            <td>
                                                @if ($religion)
                                                    {{ $religion->name }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>

                                    </tbody>                                                                                                               
                                </table>
                            </div>
                        </div>
                    </div>



                    <div class="col-sm-6 col">
                        <div class="row">
                            <div class="col-md-12"> 
                                <table class="table table-condensed table-responsive">
                                    <thead>
                                        <tr class="bg-danger">
                                            <td colspan="2"><i class="icon-home"></i>&nbsp;<strong>Correspondence Address</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="col-md-4">Street 1</td>
                                            <td class="col-md-8">
                                                @if (auth()->user()->correspondence_street_1)
                                                    {{ auth()->user()->correspondence_street_1 }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Street 2</td>
                                            <td class="col-md-8">
                                                @if (auth()->user()->correspondence_street_2)
                                                    {{ auth()->user()->correspondence_street_2 }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Postcode</td>
                                            <td class="col-md-8">
                                                @if (auth()->user()->correspondence_postcode)
                                                    {{ auth()->user()->correspondence_postcode }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td class="col-md-8">
                                                @if (auth()->user()->correspondence_city)
                                                    {{ auth()->user()->correspondence_city }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td class="col-md-8">
                                                @if ($corr_state)
                                                    {{ $corr_state->name }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col">
                        <div class="row">
                            <div class="col-md-12"> 
                                <table class="table table-condensed table-responsive">
                                    <thead>
                                        <tr class="bg-warning">
                                            <td colspan="2"><i class="icon-people"></i>&nbsp;<strong>Spouse</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="col-md-4">Name</td>
                                            <td class="col-md-8">
                                                @if (auth()->user()->partner_name)
                                                    {{ auth()->user()->partner_name }}
                                                @else
                                                    {{ '-' }}
                                                @endif                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td>
                                                @if (auth()->user()->partner_phone)
                                                    {{ auth()->user()->partner_phone }}
                                                @else
                                                    {{ '-' }}
                                                @endif                                                 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Total Child</td>
                                            <td>
                                                @if (auth()->user()->child_no)
                                                    {{ auth()->user()->child_no }}
                                                @else
                                                    {{ '-' }}
                                                @endif                                                 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>                    
                </div>      


            </div>
        </div>
    </div>
</div>



@stop