@extends('layouts.backend')


@section('content')



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-hover table-condensed table-responsive">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Current Photo</strong></td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td colspan="2">								
								@if ($data['empty'] == 1)
									{{ 'Empty' }}
								@else
									@if ($data['pending'] == 1)
										{{ 'Photo is waiting approval from RM.' }}
									@elseif ($data['approved'] == 1)
										<a href="{{ route('lib.file.user', array($data['photo']->photo.'.'.$data['photo']->ext)) }}" target="_blank" title="Click to Enlarge"><img src="{{ route('lib.file.user.thumb', array($data['photo']->photo_thumb.'.'.$data['photo']->ext)) }}" class="img-thumbnail img-responsive"></a>
									@elseif ($data['rejected'] == 1)
										{{ 'Photo is rejected. Please upload again.' }}
									@endif
								@endif	
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>





		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Action Date</th>
							<th class="col-md-3">Action By</th>
							<th class="col-md-3">Remarks</th>
							<th class="col-md-2">Status</th>
							<th class="col-md-1 text-right">Photo</th>
						</tr>
					</thead>
					@if (count($all) > 0)
						<?php $no = 0; ?>								
						@foreach ($all as $i)
							<?php 
								$no++; 
							?>
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $i->action_date }}</td>
								<td>
									@if ($i->PhotoLatestStatus)
										{{ $i->PhotoLatestStatus->PhotoActionBy->name }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td>
									@if (!empty($i->PhotoLatestStatus->remark))
										{{ $i->PhotoLatestStatus->remark }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td>
									@if ($i->PhotoLatestStatus)
										<?php
											if ($i->PhotoLatestStatus->status == 1) {
												$color = 'danger';
											}
											else if ($i->PhotoLatestStatus->status == 2) {
												$color = 'primary';
											}
											else if (in_array($i->PhotoLatestStatus->status, array(3,4))) {
												$color = 'muted';
											}								
										?>
										<span class="text-{{ $color }}">{{ $i->PhotoLatestStatus->PhotoStatusName->name }}</span>
									@else
										{{ '-' }}
									@endif
								</td>
								<td class="text-right"><a href="{{ route('lib.file.user', array($i->photo.'.'.$i->ext)) }}" target="_blank" class="btn btn-primary btn-sm" title="View Photo"><i class="icon-magnifier-add"></i></a></td>
							</tr>
						@endforeach
					@else
						<tr>
							<td colspan="6">No record</td>
						</tr>
					@endif
				</table>
			</div>
		</div>

	</div>
</div>




@if ($data['empty'] == 1 || $data['rejected'] == 1 && $data['pending'] != 1)
{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="panel panel-primary">
	<div class="panel-body">
		<legend>Upload Photo</legend>
		<div class="row">           
			<div class="col-md-12"> 
				<div class="form-group">
					<div class="col-md-12">       
						@if ($errors->has('photo_file'))
							<p class="text-danger">{{ $errors->first('photo_file') }}</p>
						@endif
						{{ Form::file('photo_file') }} 
					</div>
				</div>    
				<strong>Remarks: </strong>					
				<br>1) File photo must be in following types jpeg/jpg/png and below 100KB.
				<br>2) Use latest photo and not older than 3 months.
				<br>3) Background: 1 color. (Preferebly Blue/White).
				<br>4) The Dimension must follow the size of Passport.
				<br>5) Saved photo will be reviewed by RM for verification.	
			</div>       
		</div>  
	</div>

	<div class="panel-footer">  
		<div class="row">
			<div class="col-md-6">
				{{ Form::button('Upload&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'upload']) }} 
			</div>
		</div>
	</div>
</div>		
{{ Form::close() }} 
@endif





@stop

