@extends('layouts/backend')



@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'role'=>'form')) }}


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h4 class="panel-title">Info</h4>
			</div>
			<div class="panel-body nopadding">


				<div class="row col-wrap">			
					<div class="col-sm-6 col">
						<div class="row">
							<div class="col-md-12">

								<legend>Status</legend>
								<div class="form-group required">
									<label class="col-sm-4 control-label">Marital Status</label>
									<div class="col-sm-8">
										{{ Form::select('marital_id', $marital_status, auth()->user()->marital_id, array('class' => 'form-control')) }}
										@if ($errors->has('marital_id'))
											<p class="text-danger">{{ $errors->first('marital_id') }}</p>
										@endif
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Mobile No. 2</label>
									<div class="col-sm-8">
										{{ Form::text('hpno2', auth()->user()->hpno2, array('class'=>'form-control', 'maxlength' => 50)) }} 
									</div>
								</div>
								<legend>Spouse</legend>
								<div class="form-group">
									<label class="col-sm-4 control-label">Name</label>
									<div class="col-sm-8">
										{{ Form::text('partner_name', auth()->user()->partner_name, array('class'=>'form-control', 'maxlength' => 50)) }} 
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Phone</label>
									<div class="col-sm-8">
										{{ Form::text('partner_phone', auth()->user()->partner_phone, array('class'=>'form-control', 'maxlength' => 50)) }} 
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Total Child</label>
									<div class="col-sm-8">
										{{ Form::text('child_no', auth()->user()->child_no, array('class'=>'form-control', 'maxlength' => 2)) }} 
									</div>
								</div>

							</div>
						</div>
					</div>

					<div class="col-sm-6 col">
						<div class="row">
							<div class="col-md-12">	
								<legend>Correspondence Address</legend>
								<div class="form-group required">
									<label class="col-sm-4 control-label">Street 1</label>
									<div class="col-sm-8">
										{{ Form::text('correspondence_street_1', auth()->user()->correspondence_street_1, array('class'=>'form-control', 'maxlength' => 100)) }}
										@if ($errors->has('correspondence_street_1'))
											<p class="text-danger">{{ $errors->first('correspondence_street_1') }}</p>
										@endif										
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-4 control-label">Street 2</label>
									<div class="col-sm-8">
										{{ Form::text('correspondence_street_2', auth()->user()->correspondence_street_2, array('class'=>'form-control', 'maxlength' => 100)) }}
										@if ($errors->has('correspondence_street_2'))
											<p class="text-danger">{{ $errors->first('correspondence_street_2') }}</p>
										@endif										
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-4 control-label">Postcode</label>
									<div class="col-sm-8">
										{{ Form::text('correspondence_postcode', auth()->user()->correspondence_postcode, array('class'=>'form-control', 'maxlength' => 100)) }}
										@if ($errors->has('correspondence_postcode'))
											<p class="text-danger">{{ $errors->first('correspondence_postcode') }}</p>
										@endif										
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-4 control-label">City</label>
									<div class="col-sm-8">
										{{ Form::text('correspondence_city', auth()->user()->correspondence_city, array('class'=>'form-control', 'maxlength' => 100)) }}
										@if ($errors->has('correspondence_city'))
											<p class="text-danger">{{ $errors->first('correspondence_city') }}</p>
										@endif										
									</div>
								</div>

								<div class="form-group required">
									<label class="col-sm-4 control-label">State</label>
									<div class="col-sm-8">
										{{ Form::select('correspondence_state', $states, auth()->user()->correspondence_state, array('class' => 'form-control')) }}
										@if ($errors->has('correspondence_state'))
											<p class="text-danger">{{ $errors->first('correspondence_state') }}</p>
										@endif										
									</div>
								</div>																																
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel-footer" align="left">
				{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save'])  }}
			</div>

		</div>
	</div>
</div>




<div class="well">
	<strong>Notes:</strong>
	<br>1) HR need updated record from Site Supervisor for future reference.
	<br>2) Please fill up all the required fields.
	<br>3) Submitted record will send to HR.
</div>

{{ Form::close() }}


@stop


