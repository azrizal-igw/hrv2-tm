@extends('layouts.backend')

@section('content')



<link rel="stylesheet" href="/assets/css/morris.css">
<script src="/assets/js/raphael-min.js"></script>
<script src="/assets/js/morris.min.js"></script>
<link href="/assets/css/backend.css" rel="stylesheet">


@if (in_array($group_id, array(1,2,3,4,7)))
    <?php $height = 'block_height_chart'; $style = 'height: 150px;'; ?>
@else
    <?php $height = ''; $style = ''; ?>
@endif


<div class="row">
    <div class="col-md-4">
        <div class="panel panel-warning {{ $height }}">
            <div class="panel-heading">
                <div class="pull-left">MESSAGES</div>
                <div class="clearfix"></div>                
            </div>
            <div class="panel-body">
                <div class="block block_1" style="{{ $style }}">
                    <div class="loader loader_1"></div>
                </div>
            </div>             
        </div>        
    </div>

    <div class="col-md-4">
        <div class="panel panel-success {{ $height }}">
            <div class="panel-heading">                
                <div class="pull-left">STAFF APPLICATION</div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div class="block block_2" id="staff-info" style="{{ $style }}">
                    <div class="loader loader_2"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel panel-primary {{ $height }}">
            <div class="panel-heading">
                <div class="pull-left">LEAVE APPLICATION {{ date('Y') }}</div>
                <div class="clearfix"></div>
            </div>
            @if (auth()->user()->group_id != 3)                
                <div class="panel-body">
                    <div class="block block_3" id="leave-info" style="{{ $style }}">
                        <div class="col-md-6">
                            <div id="leave-chart1" style="{{ $style }}"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="leave-chart2" style="{{ $style }}"></div>
                        </div>            
                        <div class="loader loader_3"></div>                    
                    </div>                    
                </div>
            @else
                <div class="panel-body">
                    <div class="block block_3">
                        <div class="loader loader_3"></div>
                    </div>          
                </div>
            @endif
        </div>  
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info {{ $height }}">
            <div class="panel-heading">
                <div class="pull-left">REPLACEMENT (RL) REQUEST {{ date('Y') }}</div>
                <div class="clearfix"></div>                
            </div>
            <div class="panel-body">
                <div class="block block_4" id="rl-info" style="{{ $style }}">
                    <div class="loader loader_4"></div>
                </div>               
            </div>          
        </div>        
    </div>

    <div class="col-md-4">
        <div class="panel panel-info {{ $height }}">
            <div class="panel-heading panel-grey">        
                <div class="pull-left">ATTENDANCE {{ strtoupper(date('j F Y')) }}</div>
                <div class="clearfix"></div>    
            </div>
            <div class="panel-body">
                <div class="block block_5" id="att-info" style="{{ $style }}">
                    <div class="loader loader_5"></div>
                </div>                  
            </div>          
        </div>        
    </div>

    <div class="col-md-4">
        <div class="panel panel-danger {{ $height }}">
            <div class="panel-heading">                
                <div class="pull-left">CALENDAR {{ strtoupper(date('F')) }}</div>
                <div class="clearfix"></div> 
            </div>
            <div class="panel-body">
                <div class="block block_6" style="{{ $style }}">
                    <div class="loader loader_6"></div>
                </div>     
            </div>            
        </div>        
    </div>
</div>





@if (in_array($group_id, array(1,2,4,7)))

    <script src="{{ asset('/assets/js/backend.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        getModAjaxBlock(1);
        getModAjaxBlock(2);
        getModAjaxBlock(3);
        getModAjaxBlock(4);
        getModAjaxBlock(5);
        getModAjaxBlock(6);    
    });    
    </script>

@elseif ($group_id == 3) <!-- site supervisor -->
    <script type="text/javascript">
    $(document).ready(function(){
        getAjaxBlock(1);
        getAjaxBlock(2);
        getAjaxBlock(3);
        getAjaxBlock(4);
        getAjaxBlock(5);
        getAjaxBlock(6);
    });    
    </script>

@else
    <script type="text/javascript">
    $(document).ready(function(){
        getAjaxClose();
    });    
    </script>
@endif







@endsection


