@extends('layouts/backend')


@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">
					@if ($errors->has('type_id'))
						<p class="text-danger">{{ $errors->first('type_id') }}</p>
					@endif
					<div class="row">
						<div class="col-md-2">
							{{ Form::select('month', $months, old('month'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">
							{{ Form::select('year', $years, old('year'), array('class' => 'form-control')) }}
						</div>							
						<div class="col-md-2">
							{{ Form::select('status', $status, old('status'), array('class' => 'form-control')) }}							
						</div>
					</div>
					<br>
					{{ Form::button('Download', array('class' => 'btn btn-success', 'type' => 'submit', 'name' => 'btn-excel', 'id' => 'download')) }}
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}




@stop
