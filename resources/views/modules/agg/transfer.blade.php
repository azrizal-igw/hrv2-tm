@extends('layouts/backend')

@section('content')



<div class="row">
	<div class="col-md-12">
		<div class="well">

			{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
			<div class="row">
				<div class="col-md-12">

					<div class="form-group">
						@if ($errors->has('type_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('type_id') }}</p>
						@endif               
						<div class="required"> 
							<label class="col-lg-2 control-label">Type</label>
						</div>
						<div class="col-lg-2">
							{{ Form::select('type_id', $types, null, array('class' => 'form-control')) }}
						</div>
					</div>

					<br>
					{{ Form::button('Transfer',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'transfer']) }}
				</div>
			</div>
			{{ Form::close() }}

		</div>
	</div>            
</div>


@stop


