@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">
					<div class="row">
						@if ($errors->has('status_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('status_id') }}</p>
						@endif						
						<div class="col-md-2">
							{{ Form::select('status_id', $status, null, array('class' => 'form-control', 'id' => 'site_id')) }}
						</div>
					</div>
					<br>
					{{ Form::button('Sync', array('class' => 'btn btn-primary btn_click', 'type' => 'submit','name' => 'btn-search', 'id' => 'sync')) }}
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}



<div class="well">
	<strong>Remarks</strong>
	<br>1) This process is to update site detail from IHR to Aggregator (mycommSiteDetail).
	<br>2) Make sure sync from ERP is already latest.
</div>

@if (Session::has('text'))
	<div class="well">
		<strong>Results:</strong><br>
		{!! Session::get('text') !!}
	</div>
@endif



@stop


