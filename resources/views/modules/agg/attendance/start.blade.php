@extends('layouts/backend')


@section('content')



<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>{{ $by_name }}</legend>
				<fieldset>

					@if ($by_id == 2)
						<div class="form-group">
							@if ($errors->has('sitecode'))
								<p class="col-lg-12 text-danger">{{ $errors->first('sitecode') }}</p>
							@endif               
							<div class="required"> 
								<label class="col-lg-2 control-label">Leave Type</label>
							</div>
							<div class="col-lg-6">
								{{ Form::select('sitecode', $sites, old('sitecode'), array('class' => 'form-control')) }}
							</div>
						</div>
					@endif   

					<div class="form-group">
						@if ($errors->has('start_date'))
							<p class="col-lg-12 text-danger">{{ $errors->first('start_date') }}</p>
						@endif               
						<div class="required"> 
							<label class="col-lg-2 control-label" for="select">Start Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="start_date">
								{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>   
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('end_date'))
							<p class="col-lg-12 text-danger">{{ $errors->first('end_date') }}</p>
						@endif               
						<div class="required"> 
							<label class="col-lg-2 control-label" for="select">End Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="end_date">
								{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>   
						</div>
					</div>

				</fieldset>
			</div>

			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-6">
						{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('mod.agg.attendance.select')]) }}
					</div>

					<div class="col-md-6 text-right"> 
						{{ Form::button('Start&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'proceed']) }} 
						{{ Form::hidden('type_id', $type_id) }}
						{{ Form::hidden('by_id', $by_id) }}
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}



<script type="text/javascript">
	$(function () {
		getDatePicker('start_date');
		getDatePicker('end_date');                 
	});
</script>




@stop