@extends('layouts/backend')


@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-body nopadding">		
		<fieldset>
		<div class="form-group">
			@if ($errors->has('type_id'))
				<p class="col-lg-12 text-danger">{{ $errors->first('type_id') }}</p>
			@endif      
			<div class="col-lg-3">
				{{ Form::select('type_id', $types, null, array('class' => 'form-control')) }}
			</div>			
		</div>	

		<div class="form-group">
			<div class="col-xs-12">
				@if ($errors->has('by_id'))
					<p class="text-danger">{{ $errors->first('by_id') }}</p>
				@endif
				@if (count($by_lists) > 0)
					@foreach ($by_lists as $x => $y)
						<div class="radio">
							<label><input name="by_id" value="{{ $x }}" type="radio">{{ $y }}</label>
						</div>
					@endforeach
				@endif
			</div>
		</div>

		</fieldset>
		*<strong>Notes</strong>:
		<br>1) Process is to get latest Punch In/Out and status Attendance of each Staff
		<br>2) The process might take longer than expected. Depending on how much record it process.
		<br>3) Export is to transfer the record Attendance to Agg database.
		<br>4) Reset is to update Send Date and Error Message to null. It will transfer after an hour.
	</div> 

	<div class="panel-footer">
		{{ Form::button('Select&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-primary']) }}   		
	</div>

</div>
{{ Form::close() }}  



@stop

