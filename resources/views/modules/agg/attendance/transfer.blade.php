@extends('layouts/backend')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="well">

            {{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
	            <div class="row">
	                <div class="col-md-6"> 
	                    {{ Form::button('Execute', array('class' => 'btn btn-primary btn_click', 'type' => 'submit', 'id' => 'execute')) }}
	                </div>
	            </div>
	            <div id="token" alt="{{ csrf_token() }}"></div>
            {{ Form::close() }}

        </div>
    </div>            
</div>

@stop

