@extends('layouts/backend')

@section('content')


<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Attendance Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Site Name</td>
						<td class="col-md-9">{{ $detail->PI1M_REFID }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>
							{{ $detail->Staff_IC }}
						</td>
					</tr>
					<tr>
						<td>Date</td>
						<td>
							{{ $detail->CheckIn_Date }}
						</td>
					</tr>
					<tr>
						<td>Time In</td>
						<td>{{ $detail->CheckIn_Time }}</td>
					</tr>
					<tr>
						<td>Time Out</td>
						<td>{{ $detail->CheckOut_Time }}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>{{ $detail->CheckIn_Status }}</td>
					</tr>
					<tr>
						<td>Send Date</td>
						<td>{{ $detail->Send_Date }}</td>
					</tr>
					<tr>
						<td>Error Message</td>
						<td>{{ $detail->Error_Message }}</td>
					</tr>
				</table>
			</div>
		</div>

	</div>
</div>




@stop



