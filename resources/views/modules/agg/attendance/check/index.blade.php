@extends('layouts/backend')

@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							{{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							<div class="input-group date" id="pick_date">
								{{ Form::text('date', $sessions['date'], array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'date_to', 'placeholder' => 'Date')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}




<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No /<br>Site Name</th>
					<th class="col-md-3">Date /<br>Punch In /<br>Punch Out</th>
					<th class="col-md-3">Status /<br>Send Date /<br>Error Message</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>


	      	@if (count($att) > 0)
	            <?php $no = $att->firstItem() - 1;?>
	            @foreach ($att as $i)
	            	<?php $no++;?>

					<tbody>
						<tr>
							<td>{{ $no }}</td>
							<td>
								{{ $i->UserDetail->name }} /<br>
								{{ $i->Staff_IC }} /<br>
								{{ $i->SiteDetail->code }} {{ $i->SiteDetail->name }}
							</td>
							<td>
								{{ $i->CheckIn_Date }} /<br>
								{{ $i->CheckIn_Time }} /<br>
								{{ $i->CheckOut_Time }}
							</td>
							<td>
								{{ $i->CheckIn_Status }} /<br>
								@if (!empty($i->Send_Date))
									{{ $i->Send_Date }}
								@else
									{{ '-' }}
								@endif
								/<br>
								@if (!empty($i->Error_Message))
									{{ $i->Error_Message }}
								@else
									{{ '-' }}
								@endif
							</td>
							<td class="text-right">
								<a href="{{ route('mod.agg.attendance.check.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View Detail"><i class="icon-magnifier-add"></i></a>
							</td>
						</tr>
					</tbody>

	            @endforeach
	        @else

	        	<tbody>
	        		<tr>
	        			<td colspan="5">No record</td>
	        		</tr>
	        	</tbody>
	        @endif


		</table>


		<div class="well">
			<div class="paging text-center">
			{{ $att->render() }}
			<p>{{ 'Total: '.$att->total() }}</p>
			</div>
		</div>


	</div>
</div>


<script type="text/javascript">
$(function () {
	getDatePicker('pick_date');
});
</script>


@stop


