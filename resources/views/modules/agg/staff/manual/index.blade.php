@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $status, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('export_id', $export, null, array('class' => 'form-control')) }}
						</div>	                  
					</div>

					<div class="row">
						<div class="col-md-12">
							{{ Form::text('keyword', null, array('class'=>'form-control', 'placeholder' => 'Search Name/IC No', 'size' => 40)) }}
						</div>
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>            
</div>
{{ Form::close() }}




<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No. /<br>Position</th>
					<th class="col-md-3">Sitecode /<br>Contact No /<br>Email</th>
					<th class="col-md-3">Date Apply /<br>Status /<br>Export to AGG</th>
					<th class="col-md-1 text-right"></th>
				</tr>
			</thead>
			@if (count($manual) > 0)
			<?php $no = $manual->firstItem() - 1;?>
			@foreach ($manual as $i)
			<?php 
			$no++;                       
			?>
			<tr>					
				<td>{{ $no }}</td>

				<td>
					{{ $i->Staff_Name }} /<br>{{ $i->Staff_IC }} /<br>{{ $i->Position }}
				</td>

				<td>
					{{ $i->PI1M_REFID }} /<br>{{ $i->Contact_Number }} /<br>{{ $i->Contact_Email }}
				</td>

				<td>
					{{ $i->Action_Date }} /<br>
					{{ $i->Status }} /<br>
					@if ($i->Export_Status == 1)
					{{ 'SEND' }}
					@else
					{{ 'NOT YET' }}
					@endif
				</td>

				<td class="text-right">
					<a href="{{ route('mod.agg.staff.manual.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
				</td>
			</tr>
			@endforeach
			@else
			<tr><td colspan="5">No record</td></tr>
			@endif
		</table>


		<div class="well">
			<div class="paging text-center">  
				{{ $manual->render() }}      
				<p>{{ 'Total: '.$manual->total() }}</p>
			</div>        	
		</div>

	</div>
</div>





@stop

