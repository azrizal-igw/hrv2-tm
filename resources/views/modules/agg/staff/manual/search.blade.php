@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-heading">
		<h4 class="panel-title">Staff Info</h4>
	</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('IC No.') }}
						@if ($errors->has('icno'))
							<p class="text-danger">{{ $errors->first('icno') }}</p>
						@endif
						{{ Form::text('icno', old('icno'), array('class'=>'form-control', 'size' => 40, 'maxlength' => 12)) }}
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="panel-footer">
		<div class="row">
			<div class="col-md-6">
				{{ Form::button('Search',['type' => 'submit', 'class' => 'btn btn-success', 'id' => 'search']) }}
			</div>
		</div>
	</div>	
</div>


<div class="well">
	<strong>Remarks: </strong>
	<br>1) Use this form if need to update specific Staff Detail to MyComm (Ex: Staff Movement/Staff Transfer)
	<br>2) Need to give 1 hour of each process if update same IC No. Ex: keyin inactive at 8am, later 9am keyin active.
</div>



{{ Form::close() }}





@stop

