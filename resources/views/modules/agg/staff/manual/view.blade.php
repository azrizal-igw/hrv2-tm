@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Name</td>
						<td class="col-md-9">{{ $detail->Staff_Name }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>
							{{ $detail->Staff_IC }}
						</td>
					</tr>
					<tr>
						<td>Mobile No</td>
						<td>
							{{ $detail->Contact_Number }}
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{ $detail->Contact_Email }}</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>{{ $detail->Position }}</td>
					</tr>
					<tr>
						<td>Sitecode</td>
						<td>{{ $detail->PI1M_REFID }}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>{{ $detail->Status }}</td>
					</tr>
				</table>
			</div>
		</div>




		@if ($status == 1)
			<div class="row">
				<div class="col-md-12">	
					<table class="table table-condensed table-responsive table-hover table-striped">
						<thead>
							<tr class="bg-primary">
								<td colspan="2"><strong>Export Info</strong></td>
							</tr>
						</thead>
						<tr>
							<td class="col-md-3">Date</td>
							<td class="col-md-9">{{ $detail->Export_Date }}</td>
						</tr>
						<tr>
							<td>Action By</td>
							<td>{{ $detail->UserExportBy->name }}</td>
						</tr>
						<tr>
							<td>Status</td>
							<td>
								@if ($detail->Export_Status == 1)
									{{ 'SEND' }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
					</table>
					{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-danger', 'id' => 'btn_back', 'data-href' => route('mod.agg.staff.manual.list')]) }}
				</div>
			</div>			
		@endif


	</div>

	@if ($status == 0)
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-6"> 
					{{ Form::button('<i class="icon-action-redo"></i>&nbsp;Export to Agg DB',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'export']) }} 
				</div>
			</div>
		</div>
	@endif

</div>
{{ Form::close() }}





@stop