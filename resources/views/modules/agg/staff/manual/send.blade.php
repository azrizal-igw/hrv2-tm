@extends('layouts/backend')

@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-body nopadding">
		@if ($errors->has('chk_id'))
			<p class="text-danger">{{ $errors->first('chk_id') }}</p>
		@endif
		<table class="table table-condensed table-responsive">
			<thead>
				<tr class="bg-primary">
					<th class="col-md-1">No</th>
					<th class="col-md-3">Site Name</th>
					<th class="col-md-10">Staff Details</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>
			<tbody>
				@if (count($users) > 0)
					<?php $no = 0; ?>
					@foreach ($users as $i)
						<?php $no++; ?>
						<tr>
							<td>{{ $no }}</td>
							<td>
								{{ $i->sitecode }} {{ $i->SiteNameInfo->name }}
							</td>
							<td>
								<div class="row">
									<div class="col-md-6">
										<table class="table table-responsive">
											<tr>
												<td class="col-md-6">
													<?php
														if ($i->status == 1) {
															$status_name = 'ACTIVE';
														}
														else {
															$status_name = 'INACTIVE';
														}
													?>
													<input class="form-control" name="hpno[{{ $i->id }}]" type="text" value="{{ $i->hpno }}"><br>
													<input class="form-control" name="email[{{ $i->id }}]" type="text" value="{{ $i->email }}"><br>
													<select class="form-control" size="4" name="position_name[{{ $i->id }}]">
														@foreach ($positions as $x => $y)
															@if (strtoupper($i->UserCurrentJob2->PositionName->name) == $y)
																<?php $selected = 'selected'; ?>
															@else
																<?php $selected = ''; ?>
															@endif
															<option value="{{ $x }}" {{ $selected }}>{{ $y }}</option>
														@endforeach
													</select><br>
													<select class="form-control" size="4" name="status_name[{{ $i->id }}]">
														@foreach ($status as $x => $y)
															@if ($status_name == $y)
																<?php $selected = 'selected'; ?>
															@else
																<?php $selected = ''; ?>
															@endif
															<option value="{{ $x }}" {{ $selected }}>{{ $y }}</option>
														@endforeach
													</select><br>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</td>
							<td class="text-right">
								<input name="chk_id" type="radio" value="{{ $i->id }}">
							</td>
						</tr>
					@endforeach
				@endif				
			</tbody>
		</table>
	</div> 

	<div class="panel-footer">
		<div class="row">
			<div class="col-md-6"> 
				{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('mod.agg.staff.manual.send')]) }} 
			</div>
			<div class="col-md-6 text-right">
				{{ Form::button('Send&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'select']) }}
				{{ Form::hidden('icno', Session::get('icno')) }} 
			</div>
		</div>				
	</div>

</div>
{{ Form::close() }}  

@stop



