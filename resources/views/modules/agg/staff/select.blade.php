@extends('layouts/backend')

@section('content')



<div class="row">
	<div class="col-sm-12">

		<table class="table table-striped table-condensed table-responsive table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">ID</th>
					<th class="col-md-4">Name /<br>IC No. /<br>Position</th>
					<th class="col-md-4">Sitecode /<br>Contact No. /<br>Email</th>
					<th class="col-md-3">Created /<br>Updated /<br>Status</th>
				</tr>
			</thead>


			@if (count($json['USP_IM.V_PI1M_CMS_STAFF_DETAILS']) > 0)
				<?php $no = 0; ?>
				@foreach ($json['USP_IM.V_PI1M_CMS_STAFF_DETAILS'] as $i)
					<?php $no++; ?>
					<tr>
						<td>{{ $no }}</td>
						<td>
							{{ $i['STAFF_NAME'] }} /<br>{{ $i['STAFF_IC'] }} /<br>{{ $i['POSITION'] }}
						</td>
						<td>
							{{ $i['PI1M_REFID'] }} /<br>{{ $i['CONTACT_NUMBER'] }} /<br>{{ $i['CONTACT_EMAIL'] }}
						</td>
						<td>
							{{ $i['DATE_CREATED'] }} /<br>
							{{ $i['UPDATED'] }} /<br>
							@if ($i['STATUS'] == 'ACTIVE')
								<span class="text-primary">{{ $i['STATUS'] }}</span>
							@else
								<span class="text-danger">{{ $i['STATUS'] }}</span>
							@endif
						</td>
					</tr>

				@endforeach
			@endif

		</table>

	</div>
</div>



@stop


