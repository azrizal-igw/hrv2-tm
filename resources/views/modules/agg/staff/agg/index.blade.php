@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('position_id', $positions, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $status, null, array('class' => 'form-control')) }}
						</div>	                  
					</div>

					<div class="row">
						<div class="col-md-12">
							{{ Form::text('keyword', null, array('class'=>'form-control', 'placeholder' => 'Search Name/IC No', 'size' => 40)) }}
						</div>
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>            
</div>
{{ Form::close() }}






<div class="row">
	<div class="col-sm-12">

		<table class="table table-striped table-condensed table-responsive table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No. /<br>Position</th>
					<th class="col-md-3">Site Name /<br>Contact No. /<br>Email</th>
					<th class="col-md-3">Created /<br>Send Date /<br>Status</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>

			@if (count($users) > 0)
				<?php $no = $users->firstItem() - 1;?>
				@foreach ($users as $i)
					<?php
						$no++;
					?>
					<tr>
						<td>{{ $no }}</td>
						<td>
							{{ $i->Staff_Name }} /<br>{{ $i->Staff_IC }} /<br>{{ $i->Position }}
						</td>
						<td>
							@if (!empty($i->SiteDetail))
							{{ $i->SiteDetail->code }} {{ $i->SiteDetail->name }} 
							@else
							{{ '-' }}
							@endif
							/<br>{{ $i->Contact_Number }} /<br>{{ $i->Contact_Email }}
						</td>
						<td>
							{{ $i->created_at }} /<br>
							@if (!empty($i->Send_Date))
							{{ $i->Send_Date }} 
							@else
							{{ '-' }}
							@endif
							/<br>{{ $i->Status }}
						</td>
						<td class="text-right">
							<a href="{{ route('mod.agg.staff.list.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View Detail"><i class="icon-magnifier-add"></i></a>
						</td>						
					</tr>
				@endforeach
			@endif
		</table>

		<div class="well">
			<div class="paging text-center">
				{{ $users->render() }}
				<p>{{ 'Total: '.$users->total() }}</p>
			</div>
		</div>

	</div>
</div>





@stop


