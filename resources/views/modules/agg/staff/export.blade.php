@extends('layouts/backend')

@section('content')



<div class="progress" style="display: none;">
	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
	<span class="sr-only">45% Complete</span>
	</div>
</div>


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">


<div class="row">
	<div class="col-md-12">
		<div class="well">

			{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
			<div class="row">
				<div class="col-md-12">



					<div class="form-group">
						@if ($errors->has('status_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('status_id') }}</p>
						@endif               
						<div class="required"> 
							<label class="col-lg-2 control-label">Status</label>
						</div>
						<div class="col-lg-2">
							{{ Form::select('status_id', $status, null, array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('start'))
							<p class="col-lg-12 text-danger">{{ $errors->first('start') }}</p>
						@endif               
						<div class="required"> 
							<label class="col-lg-2 control-label" for="select">Start Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="start">
								{{ Form::text('start', date('Y-m-d'), array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>   
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('end'))
							<p class="col-lg-12 text-danger">{{ $errors->first('end') }}</p>
						@endif               
						<div class="required"> 
							<label class="col-lg-2 control-label" for="select">End Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="end">
								{{ Form::text('end', date('Y-m-d'), array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>   
						</div>
					</div>

					<br>
					{{ Form::button('Export',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'export']) }}
				</div>
			</div>
			{{ Form::close() }}

		</div>
	</div>            
</div>



<script type="text/javascript">
	$(function () {
		getDatePicker('start');
		getDatePicker('end');                 
	});
</script>


@stop


