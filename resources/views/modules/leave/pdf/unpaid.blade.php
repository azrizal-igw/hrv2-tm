<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 14 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"Arial Black";
	panose-1:2 11 10 4 2 1 2 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Footer Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-link:Footer;}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
 /* Page Definitions */
 @page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=EN-US>

<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:18.0pt;line-height:115%;font-family:"Arial Black","sans-serif";
color:red'>PUSAT INTERNET 1MALAYSIA (PI1M)</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><u><span
style='font-size:12.0pt;line-height:115%;font-family:"Arial Black","sans-serif";
color:black'>Borang Permohonan Cuti Tanpa Gaji</span></u></b></p>

<p class=MsoNormal align=center style='text-align:center'><span
style='position:absolute;z-index:251658240;left:0px;margin-left:-9px;
margin-top:61px;width:654px;height:2px'><img width=654 height=2
src="BORANG%20CUTI%20TANPA%20GAJI%20PUSAT%20INTERNET%201MALAYSIA_files/image001.png"></span><span
style='font-size:8.0pt;line-height:115%;font-family:"Arial Black","sans-serif";
color:black'>(Sila penuhi ruangan yang berkaitan. Borang ini perlu disertakan
dengan surat permohonan rasmi dan mendapat kelulusan Pengurus Wilayah masing-masing.
Permohonan cuti adalah sekurang-kurangnya tempoh satu (1) bulan (Berdasarkan
situasi dan pertimbangan pihak pengurusan)</span></p>

<p class=MsoNormal align=center style='text-align:center'><span
style='font-size:8.0pt;line-height:115%;font-family:"Arial Black","sans-serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:9.0pt;line-height:115%;font-family:
"Arial Black","sans-serif";color:black'>1) Nama    : <u>{{ $leave->LeaveUserInfo->name }}</u></span></p>

<p class=MsoNormal><span style='font-size:9.0pt;line-height:115%;font-family:
"Arial Black","sans-serif";color:black'>2) Jawatan : 

@if (!empty($leave->LeaveUserInfo))
  <u>{{ $leave->LeaveUserInfo->UserCurrentJob->PositionName->name }}</u>
@else
  {{ '-' }}
@endif
</span></p>

<p class=MsoNormal><span style='font-size:9.0pt;line-height:115%;font-family:
"Arial Black","sans-serif";color:black'>3) Lokasi Bertugas : <u>{{ $leave->LeaveInfo->LeaveSiteName->code }}&nbsp;{{ $leave->LeaveInfo->LeaveSiteName->name }}</u></span></p>

<p class=MsoNormal><span style='font-size:9.0pt;line-height:115%;font-family:
"Arial Black","sans-serif";color:black'>4) Jumlah Cuti Yang Dipohon : <u>{{ round($leave->date_value, 1) }}</u></span></p>

<p class=MsoNormal><span style='font-size:9.0pt;line-height:115%;font-family:
"Arial Black","sans-serif";color:black'>5) Tarikh Permohonan : <u>{{ $leave->LeaveInfo->date_apply }}</u></span></p>

<p class=MsoNormal><span style='font-size:9.0pt;line-height:115%;font-family:
"Arial Black","sans-serif";color:black'>5) Jenis cuti tanpa gaji (Sila tanda
pada ruangan ) :</span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:5.4pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border:solid black 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Urusan
  majlis perkahwinan keluarga atau saudara mara</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Kematian
  saudara mara</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Urusan
  sekolah anak-anak</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Pemeriksaan
  kesihatan di klinik atau hospital</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Mengikuti
  pembelajaran Jarak Jauh</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Melancong</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Mengasuh
  anak</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Mengikuti program
  luar (Sukan, sukarelawan, unit beruniform dll)</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Masalah kesihatan
  dan sakit yang melarat</span></p>
  </td>
 </tr>
 <tr>
  <td width=42 valign=top style='width:31.5pt;border:solid black 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt'><i class="icon-user"></i><p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:9.0pt;line-height:150%;color:black'>&nbsp;</span></p>
  </td>
  <td width=552 valign=top style='width:5.75in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  150%'><span style='font-size:10.0pt;line-height:150%;color:black'>Lain-lain
  (nyatakan) : {{ $leave->LeaveInfo->desc }}</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:9.0pt;color:black'>&nbsp;</span></p>

<br><br>
<p class=MsoNormal><b><span
style='font-size:12.0pt;line-height:115%;color:black'>Kegunaan Bahagian
Pengurusan</span></b></p>

<p class=MsoNormal><b><span style='font-size:12.0pt;line-height:115%;
color:black'>Jumlah cuti tanpa gaji yang telah diambil tahun ini : <u>{{ round($total['value'], 1) }}</u></span></b></p>

<p class=MsoNormal><b><span style='font-size:12.0pt;line-height:115%;
color:black'>Diluluskan Oleh : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Disahkan
Oleh :</span></b></p>

<p class=MsoNormal><b><span style='font-size:12.0pt;line-height:115%;
color:black'>…………………………………….                                    …………………………………………………</span></b></p>

<p class=MsoNormal style='margin-left:221.25pt'><b><span style='color:black'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahagian Sumber Manusia</span></b></p>

<br><br><br><br><br>
<p class=MsoNormal><b><span style='font-size:12.0pt;line-height:115%;
color:black'>MSD Digital Intelligence Sdn Bhd</span></b></p>

</div>

</body>

</html>
