@extends('layouts/backend')


@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							{{ Form::select('leave_type_id', $leave_types, $sessions['leave_type_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('leave_status_id', $leave_status, $sessions['leave_status_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('half_day_id', $half_day, $sessions['half_day_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('position_id', $positions, $sessions['position_id'], array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
						</div>
						<div class="col-md-3">
							<div class="input-group date" id="pick_start_date">
								{{ Form::text('start_date', $sessions['start_date'], array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'date_from', 'placeholder' => 'Start Date')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="input-group date" id="pick_end_date">
								{{ Form::text('end_date', $sessions['end_date'], array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'date_to', 'placeholder' => 'End Date')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}




<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered table-condensed table-responsive">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>Site Name /<br>Position</th>
					<th class="col-md-3">Start /<br>End /<br>Total Day</th>
					<th class="col-md-3">Date Apply /<br>Leave Type /<br>Status</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>
			@if (count($leaves) > 0)
				<?php $no = $leaves->firstItem() - 1;?>
				@foreach ($leaves as $i)
					<?php
						$no++;
					?>
					<tr>
						<td>{{ $no }}</td>
						<td>
							{{ $i->LeaveUserDetail->name }}
							/<br>
							@if ($i->LeaveSiteName)
								{{ $i->LeaveSiteName->code }}&nbsp;{{ $i->LeaveSiteName->name }}
							@else
								{{ '-' }}
							@endif
							/<br>
							@if ($i->LeaveUserJob)
								{{ $i->LeaveUserJob->PositionName->name }}
							@else
								{{ '-' }}
							@endif
						</td>
						<td>
							{{ $i->date_from }}
							/<br>
							{{ $i->date_to }}
							/<br>
							@if ($i->is_half_day == 1)
								{{ '0.5'}}
							@else
								@if ($i->LeaveDateAvail)
									{{ count($i->LeaveDateAvail) }}
								@else
									{{ '-' }}
								@endif
							@endif
						</td>
						<td>
							{{ $i->date_apply }} /<br>
							{{ $i->LeaveTypeName->name }} /<br>
							@if ($i->LeaveLatestHistory)
								<span class="text-{!! $i->LeaveLatestHistory->availability !!}">{{ $i->LeaveLatestHistory->LeaveStatusName->name }}</span>
							@else
								{{ '-' }}
							@endif
						</td>
						<td class="text-right">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-primary" title="View Details" onclick="window.location='{{ route("mod.leave.view", array($i->id, $i->user_id, $i->sitecode)) }}'"><i class="icon-magnifier-add"></i></button>
								<button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span></button>
								<ul class="dropdown-menu pull-right">
									<li><a href="{{ route('mod.leave.view', array($i->id, $i->user_id, $i->sitecode)) }}"><i class="icon-doc"></i>&nbsp;View Details</a></li>
									<li><a href="{{ route('mod.leave.user.summary', array($i->user_id, $i->sitecode)) }}"><i class="icon-calculator"></i>&nbsp;Leave Summary</a></li>
									<li><a href="{{ route('mod.leave.user.leave', array($i->user_id, $i->sitecode)) }}"><i class="icon-grid"></i>&nbsp;Staff Leave</a></li>
									<li><a href="{{ route('mod.leave.user.replacement', array($i->user_id, $i->sitecode)) }}"><i class="icon-list"></i>&nbsp;RL Request</a></li>
									<li><a href="{{ route('mod.attendance.user', array($i->LeaveUserDetail->icno, $i->sitecode)) }}"><i class="icon-clock"></i>&nbsp;Attendance</a></li>
									<li><a href="{{ route('mod.leave.user.reimburse.deduct', array($i->user_id, $i->sitecode)) }}"><i class="icon-note"></i>&nbsp;Reimburse/Deduct</a></li>
									<li><a href="{{ route('mod.user.view', array($i->user_id, $i->LeaveUserDetail->api_token)) }}"><i class="icon-user"></i>&nbsp;Staff Detail</a></li>
								</ul>
							</div>
						</td>
					</tr>
				@endforeach
			@endif
		</table>

		<div class="well">
			<div class="paging text-center">
				{{ $leaves->render() }}
				<p>{{ 'Total: '.$leaves->total() }}</p>
			</div>
		</div>

	</div>
</div>




<div class="well">
	<strong>Remarks: </strong>
	<br>1) Pending - Site Supervisor Apply the Leave.
	<br>2) Approved - Regional Manager Approve the Apply Leave.
	<br>3) Rejected - Regional Manager Reject the Apply Leave.
	<br>4) Cancel - Site Supervisor Cancel the Leave before RM Approve/Reject the Leave.
	<br>5) Apply Cancel - Leave is Approved but Site Supervisor Cancel the Leave. RM need to Approve/Reject the Request.
	<br>6) Approved Cancel - RM Approve the Apply Cancel (5).
	<br>7) Rejected Cancel - RM Reject the Apply Cancel (5).

	<br><br><strong>Notes</strong>
	<br>1) If record still not appear probably the leave is from old system. Click at 'Staff Leave' to display all the leave.
</div>




<script type="text/javascript">
$(function () {
	getDatePicker('pick_start_date');
	getDatePicker('pick_end_date');
});
</script>





@stop


