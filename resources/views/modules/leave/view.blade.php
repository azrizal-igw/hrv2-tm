@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-inline', 'id' => 'form-list', 'files' => true)) }} 
<div class="panel panel-primary">
	<div class="panel-body">

   		@include('leave/includes/view/staff')

   		@include('leave/includes/view/leave')

   		@include('leave/includes/view/date')

   		@include('leave/includes/view/status')

   		@include('leave/includes/view/history')

	</div>

	<div class="panel-footer">

   		@include('leave/includes/view/footer')

	</div>
	<div class="modal fade" id="leave-modal"></div>
</div>
{{ Form::close() }}



<script type="text/javascript">
$(document).ready(function(){

	// save leave
    $(document).on('click','#leave-update', function(){
    	if ($('#remark').val() == "") {
    		alert('Please insert Remark.');
    	}
    	else {
	        var answer = confirm('Are you sure want to process this leave?');
	        if (answer == true) {
	            $('#form-list').submit(); 
	        }
	        else {
	            return false;
	        } 
	    }
    });	    

    // display popup modal
	$(document).on('click','#popup-modal',function() {  
		var type = $(this).attr('alt');
		var name = $(this).attr('data-name');		
		str =  '';   		
		str += '<div class="modal-dialog">';
		str += '	<div class="modal-content">';
		str += '		<div class="modal-header">';
		str += '			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		str += '			<legend>' + name + ' Leave</legend>';
		str += '		</div>';
		str += '		<div class="modal-body">';
		str += '			<p><textarea name="remark" rows="4" cols="30" id="remark" placeholder="Insert Remark Here"></textarea></p><input type="hidden" name="type" value="' + type + '" />';
		str += '		</div>';
		str += '		<div class="modal-footer">';
		str += '			<div class="btn-group">';
		str += '				<button type="button" class="btn btn-primary" id="leave-update">Submit</button>';
		str += '				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
		str += '			</div>';
		str += '		</div>';
		str += '	</div>';
		str += '</div>';
		$('#leave-modal').html(str);                                                    
		$('#leave-modal').modal();  
	}); 

});  
</script>




@stop



