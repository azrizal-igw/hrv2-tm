@extends('layouts/backend')


@section('content')





<div class="row">
	<div class="col-md-12">
		<div class="well">

			{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
			<div class="row">
				<div class="col-md-7">

					<div class="row">
						<div class="col-md-12">
							{{ Form::select('site', $sites, $sessions['site'], array('class' => 'form-control')) }}
						</div>
					</div>    

					<div class="row">
						<div class="col-md-5">{{ Form::select('leave_type', $leave_types, $sessions['leave_type'], array('class' => 'form-control')) }}</div>                    
						<div class="col-md-4">{{ Form::select('month', $months, $sessions['month'], array('class' => 'form-control', 'title' => 'Month of Start Date')) }}</div>                    
                  		<div class="col-md-3">{{ Form::select('year', $years, $sessions['year'], array('class' => 'form-control', 'title' => 'Year of Start Date')) }}</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Username', 'size' => 40)) }}
						</div>
					</div>
					
					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}
				</div>
			</div>
      		{{ Form::hidden('leave_id', null, array('id' => 'leave_id')) }}
			{{ Form::close() }}

		</div>
	</div>            
</div>




<div class="row">
	<div class="col-sm-12">


		<div class="well">
			<table class="table table-striped table-condensed table-responsive">
				<thead>
					<tr class="bg-default">
						<th>No</th>
						<th>Start Date /<br>Leave Type & Status</th>
						<th>Name /<br>Staff ID</th>
						<th>Site Code /<br>Site Name</th>
						<th class="actions text-right">Actions</th>
					</tr>
				</thead>
				@if (count($leaves) > 0)
					<?php $no = $leaves->firstItem() - 1;?>
					@foreach ($leaves as $i)
						<?php 
							$no++;                       
						?>
						<tr>
							<td>{{ $no }}</td>
							<td>{{ $i->date_from }} /<br>{{ $i->LeaveTypeName->name }} & {{ $i->LeaveLatestHistory->LeaveStatusName->name }}</td>
							<td>{{ $i->LeaveUserDetail->name }} /<br>{{ $i->LeaveUserDetail->username }}</td>
							<td>{{ $i->LeaveSiteName->code }} /<br>{{ $i->LeaveSiteName->address }}</td>
							<td class="text-right"><a href="{{ route('mod.leave.view', array($i->id, $i->user_id)) }}" class="btn btn-primary btn-sm" title="View Leave"><i class="icon-magnifier"></i></a></td>
						</tr>
					@endforeach
				@else
					<tr><td colspan="5">No record</td></tr>
				@endif

			</table>

			<div class="paging text-center">  
				{{ $leaves->render() }}      
				<br>
				<p>{{ 'Total: '.$leaves->total() }}</p>
			</div>			
		</div>

	</div>
</div>



<script type="text/javascript">
$(document).ready(function(){

   $(document).on('click','#btn_id',function() {
      var answer = confirm('Do you want to delete this record?');
      if (answer == true) {
         $('#leave_id').val($(this).attr('alt'));
      }
      else {
         return false;
      } 
   });

});
</script>


@stop


