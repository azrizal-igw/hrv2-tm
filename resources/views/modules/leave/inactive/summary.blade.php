@extends('layouts/backend')


@section('content')


@if (!empty($user->UserCurrentJob) && !empty($user->UserLastContract))


<div class="well">
   <table class="table table-condensed table-bordered">
      <tr class="bg-inverse">
         <td colspan="3">Staff Info</td>
      </tr>
      <tr>
         <td>NAME: <strong>{{ strtoupper($user->name) }}</strong></td>
         <td>SITE: <strong>{{ strtoupper($user->SiteNameInfo->name) }}&nbsp;({{ strtoupper($user->SiteNameInfo->code) }})</strong></td>
         <td>WORK DURATION: <strong>{{ count($duration) }}</strong></td>
      </tr>
      <tr>
         <td>POSITION: <strong>{{ strtoupper($user->UserCurrentJob->PositionName->name) }}</strong></td>
         <td>CONTRACT FROM: <strong>{{ strtoupper(Carbon\Carbon::parse($user->UserLastContract->date_from)->format('d M Y')) }}</strong></td>
         <td>CONTRACT TO: <strong>{{ strtoupper(Carbon\Carbon::parse($user->UserLastContract->date_to)->format('d M Y')) }}</strong></td>
      </tr>   
   </table>





   <table class="table table-condensed table-bordered">
      <tr class="bg-inverse">
         <td>LEAVE TYPE</td>
         <td align="center">ENTITLED GIVEN</td>
         <td align="center">REIMBURSE LEAVE (+)</td>
         <td align="center">DEDUCT LEAVE (-)</td>
         <td align="center">LEAVE TAKEN</td>
         <td align="center">BALANCE</td>      
      </tr> 
      


      @if (count($lists) > 0)
         @foreach ($lists as $i)

            @if ($i['id'] == 6)
               @if ($i['entitled'] > 0)
                  <?php
                     $bg_entitled = '#1589FF';
                  ?>
               @endif
            @else
               <?php
                  $bg_entitled = '#F9F9F9';
               ?>
            @endif

            @if ($i['reimburse'] > 0)
               <?php
                  $bg_reimburse = '#00cc99';
               ?>
            @else
               <?php
                  $bg_reimburse = '#F9F9F9';
               ?>
            @endif

            @if ($i['deduct'] > 0)
               <?php
                  $bg_deduct = '#ff5050';
               ?>
            @else
               <?php
                  $bg_deduct = '#F9F9F9';
               ?>
            @endif

            @if ($i['taken'] > 0)
               <?php
                  $bg_taken = '#54c841';
               ?>
            @else
               <?php
                  $bg_taken = '#F9F9F9';
               ?>
            @endif

            <tr>
               <td>{{ $i['name'] }}</td>

               <td class="text-center" style="background: {{ $bg_entitled }}">
                  @if ($i['entitled'] > 0 && $i['id'] == 6)
                     <a href="#" data-url="" data-toggle="modal" id="list"><strong>{{ $i['entitled'] }}</strong></a>
                  @elseif ($i['id'] == 13)
                     {{ '-' }}
                  @else
                     {{ $i['entitled'] }}
                  @endif
               </td>

               <td class="text-center" style="background: {{ $bg_reimburse }}">
                  @if ($i['reimburse'] > 0)
                     <a href="#" data-url="" data-toggle="modal" id="list"><strong>{{ $i['reimburse'] }}</strong></a>
                  @elseif ($i['id'] == 13)
                     {{ '-' }}                     
                  @else
                     {{ $i['reimburse'] }}
                  @endif
               </td>

               <td class="text-center" style="background: {{ $bg_deduct }}">
                  @if ($i['deduct'] > 0)
                     <a href="#" data-url="" data-toggle="modal" id="list"><strong>{{ $i['deduct'] }}</strong></a>
                  @elseif ($i['id'] == 13)
                     {{ '-' }}                     
                  @else
                     {{ $i['deduct'] }}
                  @endif
               </td>

               <td class="text-center" style="background: {{ $bg_taken }}">
                  @if ($i['taken'] > 0)
                     <a href="#" data-url="" data-toggle="modal" id="list"><strong>{{ round($i['taken'], 1) }}</strong></a>
                  @else
                     {{ round($i['taken'], 1) }}
                  @endif
               </td>

               <td class="text-center">
                  @if ($i['id'] == 13)
                     {{ '-'}}
                  @else
                     {{ $i['balance'] }}
                  @endif
               </td>

            </tr>
         @endforeach
      @endif
   </table>
</div>





@else
<table class="table table-condensed table-bordered">
   <tr class="bg-inverse">
      <td colspan="3">Staff Info</td>
   </tr>
   <tr>
      <td colspan="3">Either Job Info or Contract Info is not exist.</td>
   </tr>   
</table>
@endif




<script type="text/javascript">
$(document).ready(function(){
   // $('[data-toggle="modal"]').click(function(e) {
   //    e.preventDefault();
   //    $(".divLoading").addClass('show');
   //    var url = $(this).attr('data-url');
   //    $.get(url, function(data) {
   //       $(data).modal();
   //       $(".divLoading").removeClass('show').addClass('hide');
   //    });
   // });
});
</script>



<style type="text/css">
#list a {
   color: #FFFFFF;
   text-decoration: underline;
}   
a:link {
   color: #FFFFFF;
}
a:visited {
   color: #FFFFFF;
}
a:hover {
   color: #FFFFFF;
}
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
</style>




@stop