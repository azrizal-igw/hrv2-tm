@extends('layouts/backend')

@section('content')




<div class="row">
   <div class="col-sm-12">
      <table class="table table-striped table-condensed table-responsive table-bordered">
         <thead>
            <tr class="active">
               <th class="col-md-1">No</th>
               <th class="col-md-2">Month Year /<br>Total Day</th>
               <th class="col-md-6">Instructed By /<br>Location</th>
               <th class="col-md-2">Date Apply /<br>Status</th>
               <th class="col-md-1 text-right">Actions</th>
            </tr>
         </thead>    

         @if (count($leaves) > 0)
            <?php $no = $leaves->firstItem() - 1; $approve = 0; ?>
            @foreach ($leaves as $i)
               <?php 
                  $no++;                       
               ?>
               <tr>
                  <td>{{ $no }}</td>
                  <td>
                     {{ \Carbon\Carbon::parse($i->year_month)->format('F Y') }} /<br>{{ $i->no_day }}
                  </td>
                  <td>
                     {{ ($i->instructed_by) ? $i->instructed_by : '-' }} /<br>{{ ($i->location) ? $i->location : '-' }}
                  </td>
                  <td>
                     @if ($i->date_apply != '0000-00-00 00:00:00')
                        @if (date('Y-m-d') == \Carbon\Carbon::parse($i->date_apply)->format('Y-m-d'))
                           {{ 'Today' }}
                        @else
                           {{ \Carbon\Carbon::parse($i->date_apply)->format('d/m/Y') }}
                        @endif
                     @else
                        {{ '0000-00-00' }}
                     @endif
                     / <br>                     
                     @if ($i->LeaveRepLatestHistory->status == 1)
                        <?php $color = '#ff0000'; ?>
                     @elseif ($i->LeaveRepLatestHistory->status == 2)
                        <?php $color = '#177EE5'; $approve +=$i->no_day; ?>                             
                     @else
                        <?php $color = '#666666'; ?>
                     @endif
                     <span style="color: {{ $color }}">{{ $i->LeaveRepLatestHistory->LeaveRepStatusName->name }}</span>
                  </td>
                  <td class="text-right">
                     <a href="{{ route('mod.leave.replacement.view', array($i->id, $user->id, $user->sitecode)) }}" class="btn btn-primary btn-sm" title="View Replacement Leave"><i class="icon-magnifier-add"></i></a>
                  </td>
               </tr>
            @endforeach
         @else
            <tr>
               <td colspan="6">No record</td>
            </tr>
         @endif
      </table>



      <div class="well">
         <div class="paging text-center">  
            {{ $leaves->render() }}      
            <p>{{ 'Total: '.$leaves->total() }}</p>
         </div>
      </div>








      <div class="well">
         <strong>Remarks: </strong>          
         <br>1) All request for current contract only.         
      </div>


   </div>
</div>





@stop

