@extends('layouts/backend')

@section('content')



@if ($status == 1)


<div class="row">
   <div class="col-sm-12">
      <div class="well">

         @if (count($types) > 0)
            <table class="table table-striped table-condensed table-responsive">
            @foreach ($types as $t)
               <thead>
                  <tr><td colspan="9" align="center"><h6><strong>{{ $t->LeaveTypeName->name }}</strong></h6></td></tr>
                  <tr class="bg-primary">
                     <th>No</th>
                     <th class="text-center">Date Apply</th>
                     <th class="text-center">Start Date</th>
                     <th class="text-center">End Date</th>
                     <th class="text-center">Total</th>
                     <th class="text-center">Available</th>
                     <th class="text-center">Status</th>
                     <th class="text-center">Action Date</th>
                     <th class="actions text-right">Actions</th>
                  </tr>
               </thead>

               @if (count($leaves) > 0)
                  <?php 
                     $no = 0; 
                     $total = 0;
                     $pending = 0;
                     $approved = 0;
                     $rejected = 0;
                     $cancel = 0;
                     $taken = 0;
                  ?>    
                  @foreach ($leaves as $i)                     
                     @if ($i->leave_type_id == $t->leave_type_id)
                        <?php 
                           $no++;
                           $total = count($i->LeaveDateAll);
                           $avail = count($i->LeaveDate);                               

                           // check if half day
                           // -----------------
                           if ($i->is_half_day == 1) {
                              $tot = 0.5;
                              if ($avail > 0) {
                                 $a = 0.5;
                              }
                              else {
                                 $a = 0;
                              }
                           }
                           else {
                              $tot = $total;
                              if ($avail > 0) {
                                 $a = $avail;
                              }
                              else {
                                 $a = 0;
                              }
                           }
                        ?>

                        <tr>
                           <td>{{ $no }}</td>
                           <td class="text-center">
                              @if (date('Y-m-d') == \Carbon\Carbon::parse($i->date_apply)->format('Y-m-d'))
                                 {{ 'Today' }}
                              @else
                                 {{ \Carbon\Carbon::parse($i->date_apply)->format('d/m/Y') }}
                              @endif
                           </td>

                           <td class="text-center">{{ \Carbon\Carbon::parse($i->date_from)->format('d/m/Y') }}</td>

                           <td class="text-center">
                              {{ \Carbon\Carbon::parse($i->date_to)->format('d/m/Y') }}
                           </td>

                           <td class="text-center">
                              {{ $tot }}
                           </td>

                           <td class="text-center">
                              {{ $a }}
                           </td>

                           <td class="text-center">
                              @if ($i->LeaveLatestHistory->status == 1)
                                 <?php $color = '#ff0000'; ?>
                              @elseif ($i->LeaveLatestHistory->status == 2)
                                 <?php $color = '#177EE5'; $taken += $a; ?>                             
                              @else
                                 <?php $color = '#666666'; ?>
                              @endif
                              <span style="color: {{ $color }}">{{ $i->LeaveLatestHistory->LeaveStatusName->name }}</span>
                           </td>

                           <td class="text-center">
                              @if (date('Y-m-d') == \Carbon\Carbon::parse($i->LeaveLatestHistory->action_date)->format('Y-m-d'))
                                 {{ 'Today' }}
                              @else
                                 {{ \Carbon\Carbon::parse($i->LeaveLatestHistory->action_date)->format('d/m/Y') }}
                              @endif
                           </td>

                           <td class="text-right">
                              <a href="{{ route('mod.leave.view', array($i->id, $user->id, $user->sitecode)) }}" class="btn btn-success btn-sm" title="View Leave"><i class="icon-magnifier"></i></a>
                           </td>
                        </tr>
                     @endif
                  @endforeach

                  @if ($no > 0)
                     <tr class="success">
                        <td colspan="5" class="text-left">Total Taken</td>
                        <td class="text-center"><strong>{{ $taken }}</strong></td>
                        <td colspan="3"></td>
                     </tr>
                  @endif
               @else
                  <tr><td colspan="9">No record</td></tr>
               @endif
               <tr><td colspan="9">&nbsp;</td></tr>

            @endforeach
            </table>
         @else
            {{ 'No record' }}
         @endif

      </div>

   </div>
</div>


@else
   <div class="row">
      <div class="col-sm-12">
         <div class="well">
            {{ 'Contract is not set. Please contact HR.' }}
         </div>
      </div>
   </div>
@endif

@stop