<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">

      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'form-data')) }}
      <div class="modal-content">

         <div class="modal-body">
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-primary">
                     <td colspan="2"><strong>Work Hour Info</strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="col-md-3">Contract</td>
                     <td class="col-md-9">
                        {{ \Carbon\Carbon::parse($work->ContractInfo->date_from)->format('j M Y') }} - {{ \Carbon\Carbon::parse($work->ContractInfo->date_from)->format('j M Y') }}
                     </td>
                  </tr>
                  <tr>
                     <td>Time In</td>
                     <td>{{ $work->time_in }}</td>
                  </tr>
                  <tr>
                     <td>Time Out</td>
                     <td>{{ $work->time_out }}</td>
                  </tr>
                  <tr>
                     <td>Off Days (Every Week)</td>
                     <td>
                        @foreach ($work->day_name as $d)
                           {{ $d->name }}<br>
                        @endforeach
                     </td>
                  </tr>
                  <tr>
                     <td>Reason</td>
                     <td>
                        {{ $work->reason }}
                     </td>
                  </tr>
                  <tr>
                     <td>Has M/AM</td>
                     <td>
                        @if ($work->partner == 1)
                           {{ 'Yes' }}
                        @else
                           {{ 'No' }}
                        @endif
                     </td>
                  </tr>
                  <tr>
                     <td>Created By</td>
                     <td>{{ $work->UserActionBy->name }}</td>
                  </tr>
                  <tr>
                     <td>Created Date</td>
                     <td>{{ $work->created_at }}</td>
                  </tr>
                  <tr>
                     <td>Attachment</td>
                     <td>
                        @if ($work->TheAttachment)
                           <?php
                              $attachment = $work->TheAttachment->filename . '.' . $work->TheAttachment->ext;
                              $photo = route('lib.file.misc', array($attachment));
                              $thumb = $work->TheAttachment->thumb_name . '.' . $work->TheAttachment->ext;
                              $thumb_url = route('lib.file.misc.thumb', array($thumb));
                           ?>
                           <a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
                        @else
                           {{ '-' }}
                        @endif                                                
                     </td>
                  </tr>
               </tbody>
            </table>

         </div>
      </div>
      {{ Form::close() }}




   </div>
</div>



