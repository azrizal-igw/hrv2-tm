@extends('layouts/backend')

@section('content')

<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css.old/bootstrap-datetimepicker.css') }}" rel="stylesheet">




<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-condensed table-responsive table-hover">
			<thead>
				<tr class="bg-inverse">
					<th class="col-md-1">No</th>
					<th class="col-md-2 text-left">Contract Date</th>
					<th class="col-md-2 text-center">Time In</th>
					<th class="col-md-2 text-center">Time Out</th>
					<th class="col-md-2 text-left">Off Day</th>
					<th class="col-md-1 text-center">Status</th>
					<th class="col-md-1 text-center"><i class="icon-picture" title="Attachment"></i></th>
					<th class="col-md-1 text-right">Actions</th>
				</tr>
			</thead>
			<tbody>
				@if (count($work_hour) > 0)
					<?php $no = 0; ?>
					@foreach ($work_hour as $i)
						<?php $no++; ?>
						<tr>
							<td>{{ $no }}</td>
							<td class="text-left">
								{{ \Carbon\Carbon::parse($i->ContractInfo->date_from)->format('j M Y') }} - {{ \Carbon\Carbon::parse($i->ContractInfo->date_to)->format('j M Y') }}
							</td>
							<td class="text-center">{{ $i->time_in }}</td>
							<td class="text-center">{{ $i->time_out }}</td>
							<td>
								@foreach ($i->day_name as $d)
									{{ $d->name }}<br>
								@endforeach
							</td>
							<td class="text-center">{!! $i->avail !!}</td>
							<td class="text-center">							
								@if ($i->TheAttachment)
									<?php
										$attachment = $i->TheAttachment->filename . '.' . $i->TheAttachment->ext;
										$photo = route('lib.file.misc', array($attachment));
									?>
									<a href="{{ $photo }}" target="_blank"><i class="icon-paper-clip"></i></a>
								@else
									{{ '-' }}
								@endif
							</td>
							<td class="text-right">
								<a href="#" id="modal-popup" data-url="{{ route('mod.leave.user.work-hour.view.popup', array($i->id, $i->user_id, $i->sitecode)) }}" class="btn btn-success btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
							</td>
						</tr>
					@endforeach
				@else
					<tr><td colspan="8">No record</td></tr>
				@endif

			</tbody>
		</table>
	</div>
</div>



{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Work Hour Info</legend>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Contract') }}
						@if ($errors->has('contract_id'))
							<p class="text-danger">{{ $errors->first('contract_id') }}</p>
						@endif
						{{ Form::select('contract_id', $contracts, old('contract_id'), array('class' => 'form-control')) }}
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Time In') }}
						@if ($errors->has('time_in'))
							<p class="text-danger">{{ $errors->first('time_in') }}</p>
						@endif
						<div class="input-group date" id="pick_time_in">
							{{ Form::text('time_in', old('time_in'), array('class' => 'form-control', 'data-date-format' => 'HH:mm', 'readonly')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Time Out') }}
						@if ($errors->has('time_out'))
							<p class="text-danger">{{ $errors->first('time_out') }}</p>
						@endif
						<div class="input-group date" id="pick_time_out">
							{{ Form::text('time_out', old('time_out'), array('class' => 'form-control', 'data-date-format' => 'HH:mm', 'readonly')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Off Day (Every Week)') }}
						@if ($errors->has('day'))
							<p class="text-danger">{{ $errors->first('day') }}</p>
						@endif						
						@if (!empty($days))
							@foreach ($days as $d)
								<br>{{ Form::checkbox('day[]', $d['id'], null, ['class' => 'check-day']) }}&nbsp;{{ $d['name'] }}
							@endforeach
						@else

						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4 required">
						{{ Form::label('Reason') }}
						@if ($errors->has('reason'))
							<p class="text-danger">{{ $errors->first('reason') }}</p>
						@endif
						{{ Form::textarea('reason', old('reason'), ['class' => 'form-control', 'size' => '30x3']) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4">
						{{ Form::label('Has M/AM') }}
						@if ($errors->has('partner'))
							<p class="text-danger">{{ $errors->first('partner') }}</p>
						@endif
						{{ Form::checkbox('partner', 1) }}
					</div>
				</div>



                <div class="form-group">
                    <div class="col-md-6 required">
                        {{ Form::label('Attachment') }}
                        @if ($errors->has('file'))
                            <p class="text-danger">{{ $errors->first('file') }}</p>
                        @endif
                        {{ Form::file('file') }}
                    </div>
                </div>

			</div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'button', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }}
                    </div>
                </div>
            </div>


		</div>
	</div>
</div>




{{ Form::close() }}



<script type="text/javascript">
$(function () {
	getTimePicker('pick_time_in');
	getTimePicker('pick_time_out');
});
</script>





@stop


