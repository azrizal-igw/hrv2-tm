@extends('layouts/backend')

@section('content')



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Details</strong></td>
						</tr>
					</thead>

					<tr>
						<td class="col-md-3">Name</td>
						<td class="col-md-9">{{ $info['name'] }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $info['icno'] }}</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>{{ $info['position'] }}</td>
					</tr>
					<tr>
						<td>Site Name</td>
						<td>{{ $info['sitecode'] }} {{ $info['sitename'] }}</td>
					</tr>
					<tr>
						<td>Contract</td>
						<td>{{ Carbon\Carbon::parse($info['date_from'])->format('d M Y') }} - {{ Carbon\Carbon::parse($info['date_to'])->format('d M Y') }}</td>
					</tr>
					<tr>
						<td>Reporting Officer</td>
						<td>{{ $info['rm'] }}</td>
					</tr>										
				</table>
			</div>
		</div>



		<div class="row">
			<div class="col-md-12">	
				<table class="table table-striped table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Apply Date</th>
							<th class="col-md-2">Created By</th>
							<th class="col-md-1 text-center">Total</th>
							<th class="col-md-5">Reason</th>
							<th class="actions text-right col-md-1">Actions</th>
						</tr>
					</thead>
					@if (count($reimburse) > 0)
						<?php $no = 0; $total_all = 3; ?>
						@foreach ($reimburse as $e)
							<?php 
								$no++; 
								$total_all += $e->total;
							?>
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $e->action_date }}</td>
								<td>
									@if (!empty($e->UserActionBy))
										{{ $e->UserActionBy->name }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td class="text-center">{{ $e->total }}</td>
								<td>{{ $e->reason }}</td>
								<td class="text-right">
									<a href="{{ route('mod.leave.unplan.limit.view', array($e->id, $info['uid'], $info['sitecode'])) }}" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
								</td>
							</tr>
						@endforeach
						<tr>
							<td colspan="6">Total Limit: {{ $total_all }}</td>
						</tr>
					@else
						<tr>
							<td colspan="6">No record</td>
						</tr>
					@endif
				</table>
			</div>
		</div>

	</div>
</div>





{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">             
	<div class="col-md-12">  

		<div class="panel panel-primary">
		    <div class="panel-body">
               
            	<legend>Reimburse Limit</legend>
				<div class="form-group">
					@if ($errors->has('total'))
						<p class="text-danger col-lg-12">{{ $errors->first('total') }}</p>
					@endif 					 	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Total Reimburse</label>
					</div>
					<div class="col-lg-2">
						{{ Form::select('total', $total, old('total'), array('class' => 'form-control')) }} 
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('reason'))
						<p class="text-danger col-lg-12">{{ $errors->first('reason') }}</p>
					@endif 					 	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Reason</label>
					</div>
					<div class="col-lg-4">
						{{ Form::textarea('reason', old('reason'), ['class' => 'form-control', 'size' => '30x5']) }}
					</div>
				</div>
            </div>

			<div class="panel-footer" align="left">
				{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }}
				{{ Form::hidden('contract_id', $info['contract_id']) }}   
			</div>
		</div>   

	</div>	
</div> 
{{ Form::close() }}  




<div class="well">
	<strong>Notes:</strong><br>
	1) Use this form to Reimburse Unplan Leave Limit.<br>
	2) Default Limit is 3.
</div>




@stop