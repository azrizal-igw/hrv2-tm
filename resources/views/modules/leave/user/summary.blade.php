@extends('layouts/backend')


@section('content')


@include('leave/includes/summary/search')


<div class="well">


   @include('leave/includes/summary/staff')


   @include('leave/includes/summary/table')


</div>


@include('leave/includes/summary/remark')


<style type="text/css">
#list a {
   color: #FFFFFF;
   text-decoration: underline;
}   
a:link {
   color: #FFFFFF;
}
a:visited {
   color: #FFFFFF;
}
a:hover {
   color: #FFFFFF;
}
</style>


@stop

