@extends('layouts/backend')


@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}      
<div class="panel panel-primary">
	<div class="panel-heading">Reimburse/Deduct Info</div>
	<div class="panel-body">


		<div class="row">             
			<div class="col-md-12">                 


				<div class="form-group">
					<div class="col-md-4">          
						<div class="required">        
						{{ Form::label('selectType', 'Types') }}   
						@if ($errors->has('type_id'))
							<p class="text-danger">{{ $errors->first('type_id') }}</p>
						@endif       
						{{ Form::select('type_id', $types, $leave_extra->type_id, array('class' => 'form-control', 'id' => 'selectType')) }}
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4">          
						<div class="required">        
						{{ Form::label('selectLeaveType', 'Type of Leave') }}   
						@if ($errors->has('leave_type_id'))
							<p class="text-danger">{{ $errors->first('leave_type_id') }}</p>
						@endif       
						{{ Form::select('leave_type_id', $leave_types, $leave_extra->leave_type_id, array('class' => 'form-control', 'id' => 'selectLeaveType')) }}
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4">          
						<div class="required">        
						{{ Form::label('selectTotal', 'No of Days') }}   
						@if ($errors->has('total'))
							<p class="text-danger">{{ $errors->first('total') }}</p>
						@endif       
						{{ Form::text('total', $leave_extra->total, array('class'=>'form-control')) }}
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4">          
						<div class="required">        
						{{ Form::label('selectReason', 'Reason') }}   
						@if ($errors->has('reason'))
							<p class="text-danger">{{ $errors->first('reason') }}</p>
						@endif       
						{{ Form::textarea('reason', $leave_extra->reason, ['class' => 'form-control', 'size' => '30x5']) }}
						</div>
					</div>
				</div>




			</div>
		</div>


	</div>
	<div class="panel-footer" align="left">
		{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save'])  }}
	</div>


</div>
{{ Form::close() }}


@stop

