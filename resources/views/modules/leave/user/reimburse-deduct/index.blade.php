@extends('layouts/backend')


@section('content')



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Details</strong></td>
						</tr>
					</thead>

					<tr>
						<td class="col-md-3">Name</td>
						<td class="col-md-9">{{ $info['name'] }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $info['icno'] }}</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>{{ $info['position'] }}</td>
					</tr>
					<tr>
						<td>Site Name</td>
						<td>{{ $info['sitecode'] }} {{ $info['sitename'] }}</td>
					</tr>
					<tr>
						<td>Contract</td>
						<td>{{ Carbon\Carbon::parse($info['date_from'])->format('d M Y') }} - {{ Carbon\Carbon::parse($info['date_to'])->format('d M Y') }}</td>
					</tr>
					<tr>
						<td>Reporting Officer</td>
						<td>{{ $info['rm'] }}</td>
					</tr>										
				</table>
			</div>
		</div>



		<div class="row">
			<div class="col-md-12">	
				<table class="table table-striped table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Leave Type</th>
							<th class="col-md-2">Type</th>
							<th class="col-md-1 text-center">Total</th>
							<th class="col-md-5">Reason</th>
							<th class="actions text-right col-md-1">Actions</th>
						</tr>
					</thead>
					@if (count($ext) > 0)
						<?php $no = 0; ?>
						@foreach ($ext as $e)
							<?php 
								$no++; 
								$color = ($e->type_id == 1) ? 'primary' : 'danger';
							?>
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $e->LeaveTypeName->name }}</td>
								<td>
									<span class="text-{{ $color }}">{{ ($e->type_id == 1) ? 'Reimburse' : 'Deduct' }}</span>
								</td>
								<td class="text-center">{{ number_format($e->total, 1) }}</td>
								<td>{{ $e->reason }}</td>
								<td class="text-right">
									<a href="#" data-url="{{ route('mod.leave.user.reimburse.deduct.view', array($e->id, $info['uid'], $info['sitecode'])) }}" data-toggle="modal" id="list" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
								</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td colspan="6">No record Reimburse/Deduct</td>
						</tr>
					@endif
				</table>
			</div>
		</div>

	</div>
</div>





{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">             
	<div class="col-md-12">  

		<div class="panel panel-primary">
		    <div class="panel-body">
               
            	<legend>Create New</legend>
				<div class="form-group">
					@if ($errors->has('type_id'))
						<p class="text-danger col-lg-12">{{ $errors->first('type_id') }}</p>
					@endif 					 	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Type</label>
					</div>
					<div class="col-lg-3">
						{{ Form::select('type_id', $types, old('type_id'), array('class' => 'form-control', 'id' => 'selectType')) }} 
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('leave_type_id'))
						<p class="text-danger col-lg-12">{{ $errors->first('leave_type_id') }}</p>
					@endif 					 	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Leave Type</label>
					</div>
					<div class="col-lg-3">
						{{ Form::select('leave_type_id', $leave_types, old('leave_type_id'), array('class' => 'form-control', 'id' => 'selectLeaveType')) }}
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('total'))
						<p class="text-danger col-lg-12">{{ $errors->first('total') }}</p>
					@endif 					 	
					<div class="required"> 					
						<label class="col-lg-3 control-label">No of Days</label>
					</div>
					<div class="col-lg-3">
						{{ Form::select('total', $days, old('total'), array('class' => 'form-control', 'id' => 'selectLeaveType')) }}
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('reason'))
						<p class="text-danger col-lg-12">{{ $errors->first('reason') }}</p>
					@endif 					 	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Reason</label>
					</div>
					<div class="col-lg-4">
						{{ Form::textarea('reason', null, ['class' => 'form-control', 'size' => '30x5']) }}
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('leave_file'))
						<p class="col-lg-12 text-danger">{{ $errors->first('leave_file') }}</p>
					@endif  
					<div class="required"> 					
						<label class="col-lg-3 control-label">Attachment</label>
					</div>				
					<div class="col-lg-4">
						<label>
							{{ Form::file('leave_file') }}
						</label>
					</div>
				</div>

            </div>



			<div class="panel-footer" align="left">
				{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }}   
			</div>
		</div>   

	</div>	
</div> 
{{ Form::close() }}  



<div class="well">
	<strong>Notes:</strong><br>
	1) Use this form to increase/decrease the leave.<br>
	2) Successfully will send the notification to Site Supervisor.
</div>



@stop

