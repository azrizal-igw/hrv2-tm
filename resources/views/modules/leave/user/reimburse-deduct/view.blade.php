<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">

      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
      <div class="modal-content">

         <div class="modal-body">
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="2"><strong>{{ $type_name }} Info</strong></td>
                  </tr>
               </thead>
               <tbody>
					<tr>
						<td class="col-md-3">Apply Date</td>
						<td class="col-md-9">{{ $rd->created_at }}</td>
					</tr>
					<tr>
						<td>Leave Type</td>
						<td>{{ $rd->LeaveTypeName->name }}</td>
					</tr>
					<tr>
						<td>Total</td>
						<td>{{ $rd->total }}</td>
					</tr>
					<tr>
						<td>Reason</td>
						<td>{{ $rd->reason }}</td>
					</tr>
					<tr>
						<td>Action Date</td>
						<td>{{ $rd->created_at }}</td>
					</tr>
					<tr>
						<td>Action By</td>
						<td>{{ $rd->ActionByDetail->name }}</td>
					</tr>
					<tr>
						<td>Attachment</td>
						<td>
	                        @if ($rd->LeaveAddAttachment)                                     
	                           <?php
	                              $attachment = $rd->LeaveAddAttachment->filename . '.' . $rd->LeaveAddAttachment->ext;
	                              $photo = route('lib.file.leave', array($attachment));
	                              $thumb = $rd->LeaveAddAttachment->thumb_name . '.' . $rd->LeaveAddAttachment->ext;
	                              $thumb_url = route('lib.file.leave.thumb', array($thumb));
	                           ?>
	                           <a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
	                        @else
	                           {{ '-' }}
	                        @endif					
						</td>
					</tr>
               </tbody>
            </table>
         </div>



      </div>
      {{ Form::close() }}




   </div>
</div>


