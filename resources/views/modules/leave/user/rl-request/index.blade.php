@extends('layouts/backend')

@section('content')




<div class="row">
   <div class="col-md-12">
      <div class="well">
         {{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
            <div class="row">
               <div class="col-md-12">

                  <div class="row">
                     @if ($errors->has('contract_id'))
                        <p class="col-lg-12 text-danger">{{ $errors->first('contract_id') }}</p>
                     @endif                      
                     <div class="col-md-3">
                        {{ Form::select('contract_id', $contracts, $contract_id, array('class' => 'form-control')) }}
                     </div>      
                  </div>   

               <br>    
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}
               </div>          
            </div>
         {{ Form::close() }}
      </div>
   </div>            
</div>





<div class="row">
   <div class="col-sm-12">
      <table class="table table-striped table-condensed table-responsive">
         <thead>
            <tr class="bg-primary">
               <th class="col-md-1">No</th>
               <th class="col-md-1 text-center">Month</th>
               <th class="col-md-2 text-center">Total Day</th>
               <th class="col-md-2">Instructed By</th>
               <th class="col-md-2">Date Apply</th>
               <th class="col-md-2 text-center">Status</th>
               <th class="col-md-1 text-center"><i class="icon-picture" title="Attachment"></i></th>
               <th class="col-md-1 text-right">Actions</th>
            </tr>
         </thead>    


         @if (count($leaves) > 0)
            <?php $no = $leaves->firstItem() - 1; $approve = 0; ?>
            @foreach ($leaves as $i)
               <?php 
                  $no++;                       
               ?>
               <tr>
                  <td>{{ $no }}</td>

                  <td class="text-center">
                     {{ \Carbon\Carbon::parse($i->year_month)->format('Y-m') }}
                  </td>

                  <td class="text-center">
                     {{ $i->no_day }}
                  </td>

                  <td>
                     {{ $i->instructed_by }}
                  </td>

                  <td>
                     {{ $i->date_apply }}
                  </td>

                  <td class="text-center">                     
                     @if ($i->LeaveRepLatestHistory->status == 1)
                        <?php $color = '#ff0000'; ?>
                     @elseif ($i->LeaveRepLatestHistory->status == 2)
                        <?php $color = '#177EE5'; $approve += $i->no_day; ?>                             
                     @else
                        <?php $color = '#666666'; ?>
                     @endif
                     <span style="color: {{ $color }}">{{ $i->LeaveRepLatestHistory->LeaveRepStatusName->name }}</span>
                  </td>

                  <td class="text-center">
                     @if ($i->LeaveRepLatestAttachment)
                        <?php
                           $attachment = $i->LeaveRepLatestAttachment->filename.'.'.$i->LeaveRepLatestAttachment->ext;
                        ?> 
                        <a href="{{ route('lib.file.leave', array($attachment)) }}" target="_blank" style="text-decoration: none; color: #666666;" title="View Attachment"><i class="icon-paper-clip"></i></a>
                     @else
                        {{ '-' }}
                     @endif
                  </td>

                  <td class="text-right">
                     <a href="{{ route('mod.leave.replacement.view', array($i->id, $uid, $sitecode)) }}" class="btn btn-primary btn-sm" title="View Replacement Leave"><i class="icon-magnifier-add"></i></a>
                  </td>
               </tr>
            @endforeach
         @else
            <tr>
               <td colspan="8">No record</td>
            </tr>
         @endif
      </table>



      <div class="well">
         <div class="paging text-center">  
            {{ $leaves->render() }}      
            <p>{{ 'Total: '.$leaves->total() }}</p>
         </div>
      </div>








      <div class="well">
         <strong>Remarks: </strong>          
         <br>1) All request for current contract only.         
      </div>


   </div>
</div>





@stop

