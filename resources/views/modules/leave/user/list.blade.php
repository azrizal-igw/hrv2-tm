<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5><span class="label label-primary">{{ $name }} - {{ $leave_type->name }}</span></h5>
         </div>
         <div class="modal-body">

            @if ($type == 1)
               @if (!empty($leaves))
               <table class="table table-bordered table-condensed table-responsive">
                  <tr class="bg-inverse">
                     <td class="col-md-1">No</td>
                     <td class="text-center col-md-1">Date Apply</td>
                     <td class="text-center col-md-1">Start</td>
                     <td class="text-center col-md-1">End</td>
                     <td class="text-center col-md-1">Total</td>
                     <td class="text-center col-md-1">Available</td>
                     <td class="col-md-5">Description</td>
                     <td class="text-right col-md-1">Actions</td>
                  </tr>                  
                  <?php $no = 0; $taken = 0; ?>
                  @foreach ($leaves as $i)
                     @if (in_array($i['leave_latest_history']['status'], array(2,4,7)))                 
                        <?php 
                           $no++; 

                          // get date all and date available
                          // -------------------------------
                          $total = count($i['leave_date_all']);
                          $avail = count($i['leave_date']);

                          // check if half day
                          // -----------------
                           if ($i['is_half_day'] == 1) {
                              $tot = 0.5;
                              if ($avail > 0) {
                                 $a = 0.5;
                              }
                              else {
                                 $a = 0;
                              }
                           }
                           else {
                              $tot = $total;
                              if ($avail > 0) {
                                 $a = $avail;
                              }
                              else {
                                 $a = 0;
                              }
                           }
                           $taken += $a;
                        ?>
                        <tr>
                           <td>{{ $no }}</td>
                           <td class="text-center">{{ \Carbon\Carbon::parse($i['date_apply'])->format('d/m/Y') }}</td>
                           <td class="text-center">{{ \Carbon\Carbon::parse($i['date_from'])->format('d/m/Y') }}</td>
                           <td class="text-center">{{ \Carbon\Carbon::parse($i['date_to'])->format('d/m/Y') }}</td>
                           <td class="text-center">{{ $tot }}</td>
                           <td class="text-center">{{ $a }}</td>
                           <td>{{ $i['desc'] }}</td>
                           <td class="text-right"><a href="{{ route('mod.leave.view', array($i['id'], $uid, $sitecode)) }}" class="btn btn-success btn-sm" title="View Leave"><i class="icon-magnifier"></i></a></td>
                        </tr>
                     @endif
                  @endforeach
                  <tr class="active">
                     <td colspan="5"><strong>Total Taken</strong></td>
                     <td class="text-center"><strong>{{ $taken }}</strong></td>
                     <td colspan="2"></td>
                  </tr>
               </table>         
               @endif
               


            @elseif ($type == 2)
               @if (!empty($leaves))
               <table class="table table-bordered table-condensed table-responsive">
                  <tr class="bg-inverse">
                     <td class="col-md-1">No</td>
                     <td class="text-center col-md-1">Date Apply</td>
                     <td class="text-center col-md-1">Year Month</td>
                     <td class="text-center col-md-1">Total</td>
                     <td class="col-md-1">Instructed By</td>
                     <td class="col-md-6">Reason</td>
                     <td class="text-right col-md-1">Actions</td>
                  </tr> 
                  <?php $no = 0; $entitled = 0; ?>
                  @foreach ($leaves as $i)
                     <?php 
                     $no++; 
                     $entitled += $i['no_day'];
                     ?>
                     <tr>
                        <td>{{ $no }}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($i['date_apply'])->format('d/m/Y') }}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($i['year_month'])->format('Y-m') }}</td>
                        <td class="text-center">{{ $i['no_day'] }}</td>
                        <td class="text-left">{{ $i['instructed_by'] }}</td>
                        <td class="text-left">{{ $i['reason'] }}</td>
                        <td class="text-right"><a href="{{ route('mod.leave.replacement.view', array($i['id'], $uid, $sitecode)) }}" class="btn btn-success btn-sm" title="View Leave"><i class="icon-magnifier"></i></a></td>
                     </tr>
                  @endforeach
                  <tr>
                     <td colspan="3">Total Entitled</td>
                     <td class="text-center bg-inverse"><strong>{{ $entitled }}</strong></td>
                     <td colspan="3"></td>
                  </tr>
               </table>
               @endif




            @elseif ($type == 3 || $type == 4)
               @if (!empty($leaves))
               <table class="table table-bordered table-condensed table-responsive">
                  <tr class="bg-inverse">
                     <td class="col-md-1">No</td>
                     <td class="text-center col-md-1">Date Apply</td>
                     <td class="text-center col-md-1">Total Day</td>
                     <td class="col-md-9">Reason</td>
                  </tr> 
                  <?php $no = 0; $tot = 0; ?>
                  @foreach ($leaves as $i)
                     <?php 
                     $no++; 
                     $tot += $i['total'];
                     ?>
                     <tr>
                        <td>{{ $no }}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($i['action_date'])->format('d/m/Y') }}</td>
                        <td class="text-center">{{ round($i['total'], 1) }}</td>
                        <td class="text-left">{{ $i['reason'] }}</td>
                     </tr>
                  @endforeach
                  <tr class="active">
                     <td colspan="2"><strong>Total @if ($type == 3) {{ 'Deduct'}} @else {{ 'Reimburse' }} @endif</strong></td>
                     <td class="text-center"><strong>{{ $tot }}</strong></td>
                     <td colspan="1"></td>
                  </tr>
               </table>
               @endif


            @endif




         </div>

      </div>
   </div>
</div>


