@extends('layouts/backend')

@section('content')



<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">


<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Previous Contract</strong></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="col-md-3">Start Date</td>
							<td class="col-md-9">{{ $contract->date_from }}</td>
						</tr>
						<tr>
							<td>End Date</td>
							<td>{{ $contract->date_to }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>





{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">

				<fieldset>
					<legend>Backdated Leave</legend>
					<div class="form-group">
						@if ($errors->has('leave_type_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('leave_type_id') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label">Leave Type</label>
						</div>
						<div class="col-lg-3">
							{{ Form::select('leave_type_id', $leave_types, null, array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('date_from'))
							<p class="col-lg-12 text-danger">{{ $errors->first('date_from') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label" for="select">Start Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="pick_start_date">
								{{ Form::text('date_from', old('date_from'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'date1', 'readonly')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('no_day'))
							<p class="col-lg-12 text-danger">{{ $errors->first('no_day') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label">Total Day</label>
						</div>
						<div class="col-lg-3">{{ Form::text('no_day', old('no_day'), array('class'=>'form-control', 'size' => 40, 'maxlength' => 2)) }}
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('desc'))
							<p class="col-lg-12 text-danger">{{ $errors->first('desc') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label" for="textArea">Description</label>
						</div>
						<div class="col-lg-4">
							{{ Form::textarea('desc', old('desc'), ['class' => 'form-control', 'size' => '30x3']) }}
						</div>
					</div>

					<div class="form-group" id="i2">
						@if ($errors->has('leave_file'))
							<p class="col-lg-12 text-danger">{{ $errors->first('leave_file') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label" for="textArea">Attachment</label>
						</div>
						<div class="col-lg-9">
							<label>
								{{ Form::file('leave_file') }}
							</label>
						</div>
					</div>
				</fieldset>
			</div>

			<div class="panel-footer">

				<div class="row">
					<div class="col-md-6">
						{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('mod.leave.user.select', array($user['uid'], $user['sitecode']))]) }}
					</div>
					<div class="col-md-6 text-right">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }}
						{{ Form::hidden('chk_contract', Session::get('chk_contract')) }}

					</div>
				</div>

			</div>
		</div>

	</div>
</div>
{{ Form::close() }}


<div class="well">
	<strong>Notes:</strong>
	<br>1) End Date is calculate automatically.
	<br>2) Total Day for Maternity Leave is 60.
</div>


<script type="text/javascript">
$(function () {
	getDatePicker('pick_start_date');
});
</script>


@stop
