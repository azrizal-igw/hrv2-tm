@extends('layouts/backend')

@section('content')



<div class="row">
	<div class="col-md-12">
		<div class="well">

			{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
			<div class="row">
				<div class="col-md-12">


					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('position_id', $positions, $sessions['position_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
						</div>
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>
			{{ Form::close() }}

		</div>
	</div>
</div>



{{ Form::open(array('class' => 'form-horizontal')) }}
<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No. /<br>Position</th>
					<th class="col-md-4">Site Name /<br>Region /<br>Phase</th>
					<th class="col-md-2">Join Date /<br>Status /<br>Last Login</th>
					<th class="col-md-1 text-right">Actions</th>
				</tr>
			</thead>
			@if (count($users) > 0)
				<?php $no = $users->firstItem() - 1;?>
				@foreach ($users as $i)
					<?php $no++;?>
					<tr>
						<td>{{ $no }}</td>

						<td>
							{{ $i->name }} /<br>
							{{ $i->icno }} /<br>
							@if ($i->UserCurrentJob2)
								{{ $i->UserCurrentJob2->PositionName->name }}
							@else
								{{ '-' }}
							@endif
						</td>

						<td>
							@if ($i->SiteNameInfo)
								{{ $i->SiteNameInfo->code.' '.$i->SiteNameInfo->name }}
							@else
								{{ '-' }}
							@endif
							/<br>
							@if ($i->UserCurrentJob2)
								@if ($i->UserCurrentJob2->RegionName)
									{{ $i->UserCurrentJob2->RegionName->name }}
								@else
									{{ '-' }}
								@endif
							@else
								{{ '-' }}
							@endif
							/<br>
							@if ($i->UserCurrentJob2)
								@if ($i->UserCurrentJob2->PhaseName)
									{{ $i->UserCurrentJob2->PhaseName->name }}
								@else
									{{ '-' }}
								@endif
							@else
								{{ '-' }}
							@endif							
						</td>

						<td>
							@if ($i->status == 1)
								<?php $leaves = 'mod.leave.user.leave'; $summary = 'mod.leave.user.summary'; $rl = 'mod.leave.user.replacement';?>
							@else
								<?php $leaves = 'mod.leave.inactive.leave'; $summary = 'mod.leave.inactive.summary'; $rl = 'mod.leave.inactive.rl-request';?>
							@endif
							@if ($i->UserCurrentJob2)
								{{ $i->UserCurrentJob2->join_date }}
							@else
								{{ '-' }}
							@endif
							/<br>
							{!! $i->status_color !!}
							/<br>
							@if (!empty($i->last_login))
								{{ $i->last_login }}
							@else
								{{ '-' }}
							@endif
						</td>

						<td class="text-right">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-primary" title="All Leaves" onclick="window.location='{{ route($leaves, array($i->id, $i->sitecode)) }}'"><i class="icon-magnifier-add"></i></button>
								<button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu pull-right">
									<li><a href="{{ route($leaves, array($i->id, $i->sitecode)) }}" title="All Leaves"><i class="icon-grid"></i>&nbsp;All Leaves</a></li>
									<li><a href="{{ route($summary, array($i->id, $i->sitecode)) }}" title="Leave Summary"><i class="icon-calculator"></i>&nbsp;Leave Summary</a></li>
									<li><a href="{{ route($rl, array($i->id, $i->sitecode)) }}" title="Replacement Request"><i class="icon-list"></i>&nbsp;RL Request</a></li>
									<li><a href="{{ route('mod.leave.user.select', array($i->id, $i->sitecode)) }}" title="Apply Leave"><i class="icon-plus"></i>&nbsp;Add Backdated Leave</a></li>
									<li><a href="{{ route('mod.leave.user.reimburse.deduct', array($i->id, $i->sitecode)) }}"><i class="icon-note"></i>&nbsp;Reimburse/Deduct Entitled</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="{{ route('mod.user.view', array($i->id, $i->api_token)) }}"><i class="icon-user"></i>&nbsp;Staff Detail</a></li>
									<li><a href="{{ route('mod.attendance.user', array($i->icno, $i->sitecode)) }}"><i class="icon-clock"></i>&nbsp;Attendance</a></li>
									<li role="separator" class="divider"></li>									
									<li><a href="{{ route('mod.leave.user.work-hour.view', array($i->id, $i->sitecode)) }}"><i class="icon-hourglass"></i>&nbsp;Change Work Hour</a></li>
									{{-- <li><a href="{{ route('mod.change.sitecode', array($i->id, $i->sitecode)) }}"><i class="icon-reload"></i>&nbsp;Change Sitecode</a></li> --}}
								</ul>
							</div>
						</td>

					</tr>
				@endforeach
			@else
				<tr><td colspan="4">No record</td></tr>
			@endif
		</table>

		<div class="well">
			<div class="paging text-center">
				{{ $users->render() }}
				<p>{{ 'Total: '.$users->total() }}</p>
			</div>
		</div>

	</div>
</div>
{{ Form::close() }}



<div class="well">
	<strong>Notes:</strong><br>
	1) Backdated leave is for Maternity/Hospitalization only. Use to Add Leave for Previous contract.<br>
	2) Make sure the Staff is Active.
</div>


<style type="text/css">
	.top-buffer { margin-bottom:5px; }
</style>

@stop


