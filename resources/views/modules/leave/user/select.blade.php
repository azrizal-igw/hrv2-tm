@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-body nopadding">
		<legend>Select Inactive Contract</legend>	
		<table class="table table-condensed table-responsive table-hover table-striped">
			<thead>
				<tr class="bg-primary">
					<td class="col-md-1">No</td>
					<td class="col-md-2 text-center">Start Date</td>
					<td class="col-md-2 text-center">End Date</td>
					<td class="col-md-2 text-center">Total Leave</td>
					<td class="col-md-2 text-center">Status</td>
					<td class="col-md-3"></td>
				</tr>
			</thead>
			<tbody>
				@if (count($contracts) > 0)
					<?php $no = 0; ?>
					@foreach ($contracts as $i)
						@if ($i->status == 2)
							<?php $no++; ?>
							<tr>
								<td>{{ $no }}</td>

								<td class="text-center">
									{{ $i->date_from }}
								</td>

								<td class="text-center">
									{{ $i->date_to }}
								</td>

								<td class="text-center">
									@if (!empty($i->LeaveApplication))
										{{ count($i->LeaveApplication) }}
									@else
										{{ '-' }}
									@endif
								</td>

								<td class="text-center">
									@if ($i->status == 1) 
										<span class="text-primary">{{ 'Active' }}</span>
									@else
										<span class="text-danger">{{ 'Inactive' }}</span>
									@endif
								</td>
								
								<td class="text-right">
									{{ Form::radio('chk_contract', $i->id, false) }}
								</td>
							</tr>
						@endif
					@endforeach
				@else
					<tr>
						<td colspan="5">No record</td>
					</tr>
				@endif
				
			</tbody>
		</table>
	</div> 
	<div class="panel-footer">
		{{ Form::button('Select&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'select']) }}   		
	</div>

</div>
{{ Form::close() }}  






@stop