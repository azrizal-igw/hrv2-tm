<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">


      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
      <div class="modal-content">
         <div class="modal-body">
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="2"><strong>RL Request Details</strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="col-md-3">Date Apply</td>
                     <td class="col-md-9">{{ $detail->date_apply }}</td>
                  </tr>
                  <tr>
                     <td>Total Day</td>
                     <td>{{ $detail->no_day }}</td>
                  </tr>
                  <tr>
                     <td>Month & Year</td>
                     <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $detail->year_month)->format('F') }}&nbsp;{{ \Carbon\Carbon::createFromFormat('Y-m-d', $detail->year_month)->format('Y') }}</td>
                  </tr>
                  <tr>
                     <td>Instructed By</td>
                     <td>{{ $detail->instructed_by ? $detail->instructed_by : '-' }}</td>
                  </tr>
                  <tr>
                     <td>Location</td>
                     <td>{{ $detail->location ? $detail->location : '-' }}</td>
                  </tr>
                  <tr>
                     <td>Reason</td>
                     <td>{{ $detail->reason ? $detail->reason : '-' }}</td>
                  </tr>
                  <tr>
                     <td>Notes</td>
                     <td>{{ $detail->notes ? $detail->notes : '-' }}</td>
                  </tr>
                  <tr>
                     <td>Current Attachment</td>
                     <td>
                     @if ($detail->LeaveRepLatestAttachment)
                        <?php
                           $attachment = $detail->LeaveRepLatestAttachment->filename . '.' . $detail->LeaveRepLatestAttachment->ext;
                           $photo = route('lib.file.leave', array($attachment));
                           $thumb = $detail->LeaveRepLatestAttachment->thumb_name . '.' . $detail->LeaveRepLatestAttachment->ext;
                           $thumb_url = route('lib.file.leave.thumb', array($thumb));
                        ?>
                        <a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
                     @else
                        {{ '-' }}
                     @endif
                     </td>
                  </tr>

               </tbody>
            </table>
         </div>

         <div class="panel-footer">
            <div class="row">
               <div class="col-md-12">
                     {{ Form::button('<i class="icon-magnifier"></i>&nbsp;View',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btnRoute', 'title' => 'View', 'data-url' => route('mod.leave.replacement.view', array($detail->id, $detail->user_id, $detail->sitecode))]) }}
               </div>
            </div>
         </div>         
      </div>
      {{ Form::close() }}

   </div>
</div>



