@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-inline', 'id' => 'form-list')) }} 
<div class="panel panel-primary">
	<div class="panel-body">



		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>				
					<tr>
						<td class="col-md-3">Staff Name</td>
						<td class="col-md-9">{{ $detail->LeaveRepUserDetail->name ? $detail->LeaveRepUserDetail->name : '-' }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $detail->LeaveRepUserDetail->icno ? $detail->LeaveRepUserDetail->icno : '-' }}</td>
					</tr>
					<tr>
						<td>Staff ID</td>
						<td>{{ $detail->LeaveRepUserDetail->staff_id ? $detail->LeaveRepUserDetail->staff_id : '-' }}</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>
						@if ($detail->LeaveRepUserDetail->UserLatestJob)
							{{ $detail->LeaveRepUserDetail->UserLatestJob->PositionName->name }}
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>
					<tr>
						<td>Site Name</td>
						<td>
						@if ($detail->LeaveRepSiteName)
							{{ $detail->LeaveRepSiteName->code }} - {{ $detail->LeaveRepSiteName->name }}
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>
					<tr>
						<td>Reporting Officer</td>
						<td>{{ ($detail->LeaveRepReportToName) ? $detail->LeaveRepReportToName->name : '-' }}</td>
					</tr>					
				</table>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12">	
				<table class="table table-hover table-condensed table-responsive table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Replacement Details</strong></td>
						</tr>
					</thead>				
					<tr>
						<td class="col-md-3">Date Apply</td>
						<td class="col-md-9">{{ $detail->date_apply }}</td>
					</tr>
					<tr>
						<td>Total Day</td>
						<td>{{ $detail->no_day }}</td>
					</tr>
					<tr>
						<td>Month & Year</td>
						<td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $detail->year_month)->format('F') }}&nbsp;{{ \Carbon\Carbon::createFromFormat('Y-m-d', $detail->year_month)->format('Y') }}</td>
					</tr>
					<tr>
						<td>Instructed By</td>
						<td>{{ $detail->instructed_by ? $detail->instructed_by : '-' }}</td>
					</tr>
					<tr>
						<td>Location</td>
						<td>{{ $detail->location ? $detail->location : '-' }}</td>
					</tr>
					<tr>
						<td>Date Work</td>
						<td>
							@if ($detail->date_work)
								{{ $detail->date_work }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Reason</td>
						<td>{{ $detail->reason ? $detail->reason : '-' }}</td>
					</tr>
					<tr>
						<td>Notes</td>
						<td>{{ $detail->notes ? $detail->notes : '-' }}</td>
					</tr>

					<tr>
						<td>Current Attachment</td>
						<td>
						@if ($detail->LeaveRepLatestAttachment)
							<?php
								$attachment = $detail->LeaveRepLatestAttachment->filename . '.' . $detail->LeaveRepLatestAttachment->ext;
								$photo = route('lib.file.leave', array($attachment));
								$thumb = $detail->LeaveRepLatestAttachment->thumb_name . '.' . $detail->LeaveRepLatestAttachment->ext;
								$thumb_url = route('lib.file.leave.thumb', array($thumb));
							?>
							<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>


					
				</table>
			</div>
		</div>



		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Status Info</strong></td>
						</tr>
					</thead>				
					<tr>
						<td class="col-md-3">Status</td>
						<td class="col-md-9">
							@if ($detail->LeaveRepLatestHistory->status == 1 || $detail->LeaveRepLatestHistory->status == 4)
								<?php $color = 'danger'; ?>
							@elseif ($detail->LeaveRepLatestHistory->status == 2 || $detail->LeaveRepLatestHistory->status == 6)
								<?php $color = 'primary'; ?>	
							@elseif ($detail->LeaveRepLatestHistory->status == 3 || $detail->LeaveRepLatestHistory->status == 5 || $detail->LeaveRepLatestHistory->status == 7)
								<?php $color = 'muted'; ?>									
							@else
								<?php $color = 'info'; ?>
							@endif						
							<span class="text-{{ $color }}">{{ $detail->LeaveRepLatestHistory->LeaveRepStatusName->name }}</span>
						</td>
					</tr>
					<tr>
						<td>Action Date</td>
						<td>{{ $detail->LeaveRepLatestHistory->action_date }}</td>
					</tr>	
					<tr>
						<td>Action By</td>
						<td>{{ $detail->LeaveRepLatestHistory->LeaveActionByName->name }}</td>
					</tr>	

					@if ($detail->LeaveRepLatestHistory->status != 1)		            
						<tr>
							<td>Remarks</td>
							<td>{{ $detail->LeaveRepLatestHistory->action_remark }}</td>
						</tr>
					@endif
					
					<tr>
						<td colspan="2">
							@if ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 1) 
								{{ "Note: This leave is waiting for Approval from Region Manager (RM)."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 2)
								{{ "Note: This Request is Approved."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 3)
								{{ "Note: This leave is Rejected. Please contact Regional Officer for Further Information."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 4)
								{{ "Note: This leave already approve but Canceled by Site Supervisor. Awaits Approval from Regional Officer."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 5)
								{{ "Note: This leave already cancel by Site Supervisor."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 6)
								{{ "Note: This leave is Approved for Apply Cancel."}}
							@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 7)
								{{ "Note: This leave is Rejected for Apply Cancel."}}
							@else
								{{ "Note: Unknown status" }}
							@endif							
						</td>
					</tr>					
				</table>
			</div>
		</div>



		<div class="row">
			<div class="col-md-12">	
				<table class="table table-striped table-condensed table-responsive table-hover">		
					<thead>
						<tr class="bg-inverse">
							<th>No</th>
							<th>Status</th>
							<th>Action Date</th>
							<th>Action By</th>														
							<th>Remarks</th>
						</tr>
					</thead>
					@if (count($detail->LeaveRepPrevHistory) > 0)
					<?php $no = 0; ?>
						@foreach ($detail->LeaveRepPrevHistory as $i)
						<?php $no++; ?>
						<tr>
							<td>{{ $no }}</td>
							<td>{{ $i->LeaveRepStatusName->name }}</td>
							<td>{{ $i->action_date }}</td>
							<td>{{ $i->LeaveActionByName->name }}</td>
							<td>{{ $i->action_remark }}</td>
						</tr>
						@endforeach
					@else
						<tr>
							<td colspan="5">No previous record</td>
						</tr>
					@endif				
				</table>
			</div>
		</div>




	</div>

	<div class="panel-footer">
		<div class="row">
			<div class="col-md-6"> 
				<a href="{{ route('mod.leave.replacement.attachment.edit', array($detail->id, $detail->user_id, $detail->sitecode)) }}" class="btn btn-primary" title="Edit Attachment"><i class="icon-paper-clip"></i>&nbsp;Edit</a>
			</div>
			<div class="col-md-6 text-right">
				@if ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 1) 
					{{ Form::button('<i class="icon-check"></i>&nbsp;Approve', array('class' => 'btn btn-success', 'id' => 'popup-modal', 'title' => 'Approve Request', 'alt' => 2, 'data-name' => 'Approve')) }}
					{{ Form::button('<i class="icon-close"></i>&nbsp;Reject', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Reject Request', 'alt' => 3, 'data-name' => 'Reject')) }}					
				@elseif ($detail->LeaveRepLatestHistory->LeaveRepStatusName->id == 4)
					{{ Form::button('<i class="icon-check"></i>&nbsp;Approve', array('class' => 'btn btn-success', 'id' => 'popup-modal', 'title' => 'Approve Cancel', 'alt' => 6, 'data-name' => 'Approve Cancel')) }}
					{{ Form::button('<i class="icon-close"></i>&nbsp;Reject', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Reject Cancel', 'alt' => 7, 'data-name' => 'Reject Cancel')) }}	
				@endif
			</div>
		</div>
	</div>
	<div class="modal fade" id="leave-modal"></div>
</div>



<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-danger">
							<td colspan="7"><strong>RL Request History</strong></td>
						</tr>
					</thead>	
					<tbody>
						<tr>
							<th>No</th>
							<th>Date Apply</th>
							<th>Day</th>
							<th>Year Month</th>
							<th>Reason</th>
							<th>Status</th>
							<th class="text-right">#</th>
						</tr>
						@if (!empty($history))
							<?php $no_h = 0; ?>
							@foreach ($history as $h)
								<?php $no_h++; ?>
								<tr>
									<td class="col-md-1">{{ $no_h }}</td>
									<td class="col-md-2">{{ $h->date_apply }}</td>
									<td class="col-md-1">{{ $h->no_day }}</td>
									<td class="col-md-1">{{ \Carbon\Carbon::parse($h->year_month)->format('Y-m') }}</td>
									<td class="col-md-5">{{ $h->reason }}</td>
									<td class="col-md-1">
										@if (!empty($h->LeaveRepLatestHistory))
											<span class="text-{{ $h->LeaveRepLatestHistory->availability }}">{{ $h->LeaveRepLatestHistory->LeaveRepStatusName->name }}</span>
										@else
											{{ '-' }}
										@endif
									</td>
									<td class="col-md-1 text-right">
										<a href="#" data-url="{{ route('mod.leave.replacement.view.popup', array($h->id, $user['uid'], $user['sitecode'])) }}" data-toggle="modal" id="list" class="btn btn-success btn-sm" title="View Request"><i class="icon-magnifier-add"></i></a>
									</td>
								</tr>
							@endforeach
							<tr><td colspan="7" class="text-center">Total: <strong>{{ $no_h }}</strong></td></tr>
						@else
							<tr>
								<td colspan="7">No record</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
{{ Form::close() }}




<script type="text/javascript">
$(document).ready(function(){

	// save leave
    $(document).on('click','#leave-update',function(){
    	if ($('#remark').val() == "") {
    		alert('Please insert Remark.');
    	}
    	else {
	        var answer = confirm('Are you sure want to process this RL request?');
	        if (answer == true) {
	            $('#form-list').submit(); 
	        }
	        else {
	            return false;
	        } 
	    }
    });	    

    // display popup modal
	$(document).on('click','#popup-modal',function() {  
		var type = $(this).attr('alt');		
		var name = $(this).attr('data-name');		
		str =  '';   		
		str += '<div class="modal-dialog">';
		str += '	<div class="modal-content">';
		str += '		<div class="modal-header">';
		str += '			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		str += '			<legend>' + name + ' RL Request</legend>';
		str += '		</div>';
		str += '		<div class="modal-body">';
		str += '			<p><textarea name="remark" rows="4" cols="30" id="remark" placeholder="Insert Remark Here"></textarea></p><input type="hidden" name="type" value="' + type + '" />';
		str += '		</div>';
		str += '		<div class="modal-footer">';
		str += '			<div class="btn-group">';
		str += '				<button type="button" class="btn btn-primary" id="leave-update">Submit</button>';
		str += '				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
		str += '			</div>';
		str += '		</div>';
		str += '	</div>';
		str += '</div>';
		$('#leave-modal').html(str);                                                    
		$('#leave-modal').modal();   
	}); 



});  
</script>



@stop



