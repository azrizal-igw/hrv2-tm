@extends('layouts/backend')

@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">



<div class="row">
   <div class="col-md-12">
      <div class="well">

         {{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
         <div class="row">
            <div class="col-md-12">

               <div class="row">
                  <div class="col-md-6">
                     {{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-6">
                     {{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('leave_status_id', $leave_status, $sessions['leave_status_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     <div class="input-group date" id="pick_apply_date">
                        {{ Form::text('apply_date', $sessions['apply_date'], array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'apply_date', 'placeholder' => 'Apply Date')) }}
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                     </div>
                  </div>
               </div>

               <br>
               <input type="submit" value="Search" class="btn btn-warning" name="btn-search" />&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>
         </div>
         {{ Form::close() }}

      </div>
   </div>
</div>





<div class="row">
   <div class="col-sm-12">
      <table class="table table-striped table-condensed table-responsive table-bordered">
         <thead>
            <tr class="active">
               <th class="col-md-1">No</th>
               <th class="col-md-4">Name /<br>IC No. /<br>Position</th>
               <th class="col-md-4">Region & Phase /<br>Site Name /<br>Year & Month</th>
               <th class="col-md-2">Apply Date /<br>Total day /<br>Leave Status</th>
               <th class="col-md-1 actions text-right">Actions</th>
            </tr>
         </thead>
         @if (count($leaves) > 0)
            <?php $no = $leaves->firstItem() - 1;?>
            @foreach ($leaves as $i)
               <?php
                  $no++;
               ?>
               <tr>
                  <td>{{ $no }}</td>

                  <td>
                     @if ($i->LeaveRepUserDetail)
                        {{ $i->LeaveRepUserDetail->name }}
                     @else
                        {{ '-' }}
                     @endif
                     /<br>
                     @if ($i->LeaveRepUserDetail)
                        {{ $i->LeaveRepUserDetail->icno }}
                     @else
                        {{ '-' }}
                     @endif
                     /<br>
                     @if ($i->LeaveRepUserJob)
                        {{ $i->LeaveRepUserJob->PositionName->name }}
                     @else
                        {{ '-' }}
                     @endif
                  </td>

                  <td>
                     @if ($i->LeaveRepUserJob)
                        {{ strtoupper($i->LeaveRepUserJob->RegionName->name) }} - {{ $i->LeaveRepUserJob->PhaseName->name }}
                     @else
                        {{ '-' }}
                     @endif
                     /<br>
                     @if ($i->LeaveRepSiteName)
                        {{ $i->LeaveRepSiteName->code.'&nbsp;'.$i->LeaveRepSiteName->name }}
                     @else
                        {{ '-' }}
                     @endif                    
                     /<br>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $i->year_month)->format('Y') }} {{ \Carbon\Carbon::createFromFormat('Y-m-d', $i->year_month)->format('F') }}
                  </td>

                  <td>
                     {{ $i->date_apply }}
                     /<br>{{ $i->no_day }}
                     @if ($i->LeaveRepLatestHistory->status == 1 || $i->LeaveRepLatestHistory->status == 4)
                        <?php $color = 'danger';?>
                     @elseif ($i->LeaveRepLatestHistory->status == 2 || $i->LeaveRepLatestHistory->status == 6)
                        <?php $color = 'primary';?>
                     @elseif ($i->LeaveRepLatestHistory->status == 3 || $i->LeaveRepLatestHistory->status == 5 || $i->LeaveRepLatestHistory->status == 7)
                        <?php $color = 'muted';?>
                     @else
                        <?php $color = 'info';?>
                     @endif
                     /<br><span class="text-{{ $color }}">{{ $i->LeaveRepLatestHistory->LeaveRepStatusName->name }}</span>
                  </td>

                  <td class="text-right">
                     <a href="{{ route('mod.leave.replacement.view', array($i->id, $i->user_id, $i->sitecode)) }}" class="btn btn-primary btn-sm" title="View Request"><i class="icon-magnifier-add"></i></a>
                  </td>
               </tr>
            @endforeach
         @endif
      </table>


      <div class="well">
         <div class="paging text-center">
            {{ $leaves->render() }}
            <br>
            <p>{{ 'Total: '.$leaves->total() }}</p>
         </div>

      </div>


   </div>
</div>


<script type="text/javascript">
$(function () {
   getDatePicker('pick_apply_date');
});
</script>



@stop

