@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">

               <div class="row">
                  <div class="col-md-3">{{ Form::select('state_id', $states, $sessions['state_id'], array('class' => 'form-control')) }}</div>
                  <div class="col-md-3">{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}</div>
                  <div class="col-md-3">{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}</div>
                  <div class="col-md-3">{{ Form::select('district', $districts, $sessions['district'], array('class' => 'form-control')) }}</div>                  
               </div>

               <div class="row">
                  <div class="col-md-3">
                     {{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-9">{{ Form::text('search', $sessions['search'], array('class'=>'form-control', 'placeholder' => 'Search Code/Name')) }}
                  </div>                  
               </div>

               <br>
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>
         </div>

      </div>
   </div>
</div>
{{ Form::close() }}




<div class="row">
   <div class="col-sm-12">
      <table class="table table-striped table-condensed table-responsive table-bordered">
         <thead>
            <tr class="active">
               <th>No</th>
               <th>Name /<br>Code</th>
               <th>City /<br>District</th>
               <th>Region /<br>State</th>
               <th>Status</th>
               <th class="text-right">Actions</th>
            </tr>
         </thead>
         @if (count($sites) > 0)
            <?php $no = $sites->firstItem() - 1;?>
            @foreach ($sites as $i)
               <?php $no++;?>
               <tr>
                  <td>{{ $no }}</td>
                  <td><a href="{{ route('mod.site.view', array($i->id)) }}">{{ $i->name }}</a><br>{{ $i->code }}</td>
                  <td>{{ ($i->city) ? $i->city : '-' }}<br>{{ ($i->district) ? $i->district : '-' }}</td>
                  <td>{{ ($i->RegionName) ? $i->RegionName->name : '-' }}<br>{{ ($i->StateName) ? $i->StateName->name : '-' }}</td>
                  <td>
                     {!! $i->availability !!}
                  </td>
                  <td class="text-right">
                     <a href="{{ route('mod.site.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View Detail"><i class="icon-magnifier-add"></i></a>
                  </td>
               </tr>
            @endforeach
         @else
            <tr><td colspan="6">No record</td></tr>
         @endif
      </table>



      <div class="well">
         <div class="paging text-center">
            {{ $sites->render() }}
            <p>{{ 'Total: '.$sites->total() }}</p>
         </div>
      </div>


   </div>
</div>


@stop