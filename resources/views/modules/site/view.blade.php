@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal')) }}
<div class="panel panel-primary">

	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Site Info</strong></td>
						</tr>
					</thead>					
					<tbody>
						<tr>
							<td class="col-md-3">Name</td>
							<td class="col-md-9">{{ $detail->name }}</td>
						</tr>
						<tr>
							<td>Code</td>
							<td>{{ $detail->code }}</td>
						</tr>
						<tr>
							<td>Full Name</td>
							<td>{{ $detail->full_name }}</td>
						</tr>
						<tr>
							<td>TM Name</td>
							<td>{{ $detail->tm_name }}</td>
						</tr>
						<tr>
							<td>Both Name</td>
							<td>{{ $detail->both_name }}</td>
						</tr>
						<tr>
							<td>Old Name</td>
							<td>{{ $detail->old_name }}</td>
						</tr>
						<tr>
							<td>Phase</td>
							<td> 
								@if ($detail->PhaseName)
									{{ $detail->PhaseName->name }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
						<tr>
							<td>Region</td>
							<td>
								@if ($detail->RegionName)
									{{ $detail->RegionName->name }}
								@else
									{{ '-' }}
								@endif								
							</td>
						</tr>						
					</tbody>
				</table>



				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Address Info</strong></td>
						</tr>
					</thead>					
					<tbody>
						<tr>
							<td class="col-md-3">Street 1</td>
							<td class="col-md-9">{{ $detail->street1 }}</td>
						</tr>
						<tr>
							<td>Street 2</td>
							<td>{{ $detail->street2 }}</td>
						</tr>
						<tr>
							<td>Postal Code</td>
							<td>{{ $detail->postal_code }}</td>
						</tr>
						<tr>
							<td>City</td>
							<td>{{ $detail->city }}</td>
						</tr>
						<tr>
							<td>District</td>
							<td>{{ $detail->district }}</td>
						</tr>
						<tr>
							<td>State</td>
							<td>
								@if ($detail->StateName)
									{{ $detail->StateName->name }}
								@else
									{{ '-' }}
								@endif								
							</td>
						</tr>
						<tr>
							<td>Address Label</td>
							<td>{{ $detail->address_label }}</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>{{ $detail->email }}</td>
						</tr>
						<tr>
							<td>Website</td>
							<td>{{ $detail->website }}</td>
						</tr>
					</tbody>
				</table>




				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Other Info</strong></td>
						</tr>
					</thead>					
					<tbody>
						<tr>
							<td>Start</td>
							<td>{{ $detail->start }}</td>
						</tr>
						<tr>
							<td>End</td>
							<td>{{ $detail->end }}</td>
						</tr>						
						<tr>
							<td class="col-md-3">Latitude</td>
							<td class="col-md-9">{{ $detail->latitude }}</td>
						</tr>
						<tr>
							<td>Longitude</td>
							<td>{{ $detail->longitude }}</td>
						</tr>
						<tr>
							<td>Bandwidth</td>
							<td>{{ $detail->bandwidth }}</td>
						</tr>
						<tr>
							<td>Backhaul</td>
							<td>{{ $detail->backhaul }}</td>
						</tr>
						<tr>
							<td>Status</td>
							<td>
								{!! $detail->availability !!}
							</td>
						</tr>
						<tr>
							<td>Date Modified</td>
							<td>{{ $detail->date_modified }}</td>
						</tr>
					</tbody>
				</table>




			</div>
		</div>
	</div>

	<div class="panel-footer">
		<div class="row">
			<div class="col-md-6"> 
				{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'cancel']) }} 
			</div>
		</div>
	</div>
</div>
{{ Form::close() }}



@stop


