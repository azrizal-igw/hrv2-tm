@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
   <div class="panel-heading">
      <h4 class="panel-title">Site Info</h4>
   </div>   
   <div class="panel-body">
      <div class="row col-wrap">


         <div class="col-sm-6 col">
            <div class="row">
               <div class="col-md-12">

                  <div class="form-group">
                     <label class="col-sm-4 control-label">Code</label>
                     <div class="col-sm-8">
                        {{ Form::text('code', old('code'), array('class'=>'form-control')) }}
                        @if ($errors->has('code'))
                           <span class="glyphicon glyphicon-warning-sign" title="{{ $errors->first('code') }}"></span>
                        @endif
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-sm-4 control-label">Name</label>
                     <div class="col-sm-8">
                        {{ Form::text('name', old('name'), array('class'=>'form-control')) }}
                        @if ($errors->has('name'))
                           <span class="glyphicon glyphicon-warning-sign" title="{{ $errors->first('name') }}"></span>
                        @endif
                     </div>
                  </div>

               </div>
            </div>
         </div>



         <div class="col-sm-6 col">
            <div class="row">
               <div class="col-md-12"> 

                  <div class="form-group">
                     <label class="col-sm-4 control-label">Email</label>
                     <div class="col-sm-8">
                        {{ Form::text('email', old('email'), array('class'=>'form-control')) }}
                        @if ($errors->has('email'))
                           <span class="glyphicon glyphicon-warning-sign" title="{{ $errors->first('email') }}"></span>
                        @endif
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div>      

   </div>      
</div>
{{ Form::close() }}  




@stop