@extends('layouts/backend')

@section('content')



<div class="panel panel-default">
   <div class="panel-inverse">Site Info</div>
   <div class="panel-body">


      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}      
      <div class="row">             
         <div class="col-md-12">                 




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectCode', 'Code') }}   
                  @if ($errors->has('code'))
                     <p class="text-danger">{{ $errors->first('code') }}</p>
                  @endif      
                  {{ Form::text('code', $detail->code, array('class'=>'form-control', 'id' => 'selectCode', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectName', 'Name') }}   
                  @if ($errors->has('name'))
                     <p class="text-danger">{{ $errors->first('name') }}</p>
                  @endif      
                  {{ Form::text('name', $detail->name, array('class'=>'form-control', 'id' => 'selectName', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>





            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectFullName', 'Full Name') }}   
                  @if ($errors->has('full_name'))
                     <p class="text-danger">{{ $errors->first('full_name') }}</p>
                  @endif      
                  {{ Form::text('full_name', $detail->full_name, array('class'=>'form-control', 'id' => 'selectFullName', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>



            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectPhase', 'Phase') }}   
                  @if ($errors->has('phase_id'))
                     <p class="text-danger">{{ $errors->first('phase_id') }}</p>
                  @endif      
                  {{ Form::select('phase_id', $phases, $detail->phase_id, array('class' => 'form-control', 'id' => 'selectPhase')) }}                     
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectRegion', 'Region') }}   
                  @if ($errors->has('region_id'))
                     <p class="text-danger">{{ $errors->first('region_id') }}</p>
                  @endif      
                  {{ Form::select('region_id', $regions, $detail->region_id, array('class' => 'form-control', 'id' => 'selectRegion')) }}                     
                  </div>
               </div>
            </div>





            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectStreet1', 'Street 1') }}   
                  @if ($errors->has('street1'))
                     <p class="text-danger">{{ $errors->first('street1') }}</p>
                  @endif      
                  {{ Form::text('street1', $detail->street1, array('class'=>'form-control', 'id' => 'selectStreet1', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectStreet2', 'Street 2') }}   
                  @if ($errors->has('street2'))
                     <p class="text-danger">{{ $errors->first('street2') }}</p>
                  @endif      
                  {{ Form::text('street2', $detail->street2, array('class'=>'form-control', 'id' => 'selectStreet2', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectPostalCode', 'Postal Code') }}   
                  @if ($errors->has('postal_code'))
                     <p class="text-danger">{{ $errors->first('postal_code') }}</p>
                  @endif      
                  {{ Form::text('postal_code', $detail->postal_code, array('class'=>'form-control', 'id' => 'selectPostalCode', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectCity', 'City') }}   
                  @if ($errors->has('city'))
                     <p class="text-danger">{{ $errors->first('city') }}</p>
                  @endif      
                  {{ Form::text('city', $detail->city, array('class'=>'form-control', 'id' => 'selectCity', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectDistrict', 'District') }}   
                  @if ($errors->has('district'))
                     <p class="text-danger">{{ $errors->first('district') }}</p>
                  @endif      
                  {{ Form::select('district', $districts, $detail->district, array('class' => 'form-control', 'id' => 'selectDistrict')) }}                   
                  </div>
               </div>
            </div>





            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectState', 'State') }}   
                  @if ($errors->has('state_id'))
                     <p class="text-danger">{{ $errors->first('state_id') }}</p>
                  @endif      
                  {{ Form::select('state_id', $states, $detail->state_id, array('class' => 'form-control', 'id' => 'selectState')) }}                    
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectAddressLabel', 'Address Label') }}   
                  @if ($errors->has('address_label'))
                     <p class="text-danger">{{ $errors->first('address_label') }}</p>
                  @endif      
                  {{ Form::text('address_label', $detail->address_label, array('class'=>'form-control', 'id' => 'selectAddressLabel', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>





            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectEmail', 'Email') }}   
                  @if ($errors->has('email'))
                     <p class="text-danger">{{ $errors->first('email') }}</p>
                  @endif      
                  {{ Form::text('email', $detail->email, array('class'=>'form-control', 'id' => 'selectEmail', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>





            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectWebsite', 'Website') }}   
                  @if ($errors->has('website'))
                     <p class="text-danger">{{ $errors->first('website') }}</p>
                  @endif      
                  {{ Form::text('website', $detail->website, array('class'=>'form-control', 'id' => 'selectWebsite', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>





            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectLatitute', 'Latitude') }}   
                  @if ($errors->has('latitude'))
                     <p class="text-danger">{{ $errors->first('latitude') }}</p>
                  @endif      
                  {{ Form::text('latitude', $detail->latitude, array('class'=>'form-control', 'id' => 'selectLatitute', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>



            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectLongitude', 'Longitude') }}   
                  @if ($errors->has('longitude'))
                     <p class="text-danger">{{ $errors->first('longitude') }}</p>
                  @endif      
                  {{ Form::text('longitude', $detail->longitude, array('class'=>'form-control', 'id' => 'selectLongitude', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>




            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectBandwidth', 'Bandwidth') }}   
                  @if ($errors->has('bandwidth'))
                     <p class="text-danger">{{ $errors->first('bandwidth') }}</p>
                  @endif      
                  {{ Form::text('bandwidth', $detail->bandwidth, array('class'=>'form-control', 'id' => 'selectBandwidth', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>


            <div class="form-group">
               <div class="col-md-4">          
                  <div class="required">        
                  {{ Form::label('selectBackhaul', 'Backhaul') }}   
                  @if ($errors->has('backhaul'))
                     <p class="text-danger">{{ $errors->first('backhaul') }}</p>
                  @endif      
                  {{ Form::text('backhaul', $detail->backhaul, array('class'=>'form-control', 'id' => 'selectBackhaul', 'size' => 40)) }}                     
                  </div>
               </div>
            </div>



         </div>
      </div>


   </div>
   <div class="panel-footer" align="left">
      {{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'btn_save'])  }}
   </div>
</div>





@stop