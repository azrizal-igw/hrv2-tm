@extends('layouts/backend')


@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('region_id', $regions, null, array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, null, array('class' => 'form-control')) }}
						</div>
					</div>


					<div class="row">
						<div class="col-md-12">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
						</div>
					</div>


					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}




<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No /<br>Position /<br></th>
					<th class="col-md-3">Site Name /<br>Phase Name /<br>Date</th>
					<th class="col-md-3">Apply By /<br>Action Date /<br>Punch</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>


			@if (count($att_manual) > 0)
				<?php $no = $att_manual->firstItem() - 1;?>
				@foreach ($att_manual as $i)
					<?php $no++;?>
					<tr>
						<td>{{ $no }}</td>
						<td>
							@if (!empty($i->UserInfo))
								{{ $i->UserInfo->name }}
							@else
								{{ '-' }}
							@endif
							/ <br>
							{{ $i->icno }}
							/ <br>
							@if (!empty($i->UserInfo->UserCurrentJob2))
								{{ $i->UserInfo->UserCurrentJob2->PositionName->name }}
							@else
								{{ '-' }}
							@endif
						</td>

						<td>
							@if ($i->UserSite)
								{{ $i->UserSite->code }}&nbsp;{{ $i->UserSite->name }}
							@else
								{{ '-' }}
							@endif
							/ <br>
							@if (!empty($i->UserInfo->UserCurrentJob2))
								{{ $i->UserInfo->UserCurrentJob2->PhaseName->name }}
							@else
								{{ '-' }}
							@endif
							/ <br>
							{{ $i->att_log_time }}
						</td>

						<td>
							@if (!empty($i->UserActionBy))
								{{ $i->UserActionBy->name }}
							@else
								{{ '-' }}
							@endif
							/ <br>
							{{ $i->action_date }}
							/ <br>
							@if ($i->type_id == 0)
								{{ 'IN' }}
							@elseif ($i->type_id == 1)
								{{ 'OUT' }}
							@else
								{{ '-' }}
							@endif
						</td>

						<td class="text-right">
							<a href="{{ route('mod.attendance.manual.view', array($i->id, $i->icno, $i->sitecode)) }}" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
						</td>

					</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">No record</td>
				</tr>
			@endif

		</table>


		<div class="well">
			<div class="paging text-center">
			{{ $att_manual->render() }}
			<p>{{ 'Total: '.$att_manual->total() }}</p>
			</div>
		</div>

	</div>
</div>




@stop


