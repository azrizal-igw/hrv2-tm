@extends('layouts/backend')


@section('content')




<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css.old/bootstrap-datetimepicker.css') }}" rel="stylesheet">




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Attendance Info</legend>




				<div class="form-group">
					<div class="col-md-4 required">
						{{ Form::label('selectIC', 'IC No.') }}
						@if ($errors->has('icno'))
						<p class="text-danger">{{ $errors->first('icno') }}</p>
						@endif
						{{ Form::text('icno', old('icno'), array('class'=>'form-control', 'id' => 'selectIC', 'size' => 40, 'maxlength' => 12)) }}
					</div>
				</div>



				<div class="form-group">
					<div class="col-md-4 required">
						{{ Form::label('selectSitecode', 'Sitecode') }}
						@if ($errors->has('sitecode'))
						<p class="text-danger">{{ $errors->first('sitecode') }}</p>
						@endif
						{{ Form::text('sitecode', old('sitecode'), array('class'=>'form-control', 'id' => 'selectSitecode', 'size' => 40, 'maxlength' => 7)) }}
					</div>
				</div>



				<div class="form-group">
					<div class="col-md-1 required">
						{{ Form::label('selectType', 'In/Out') }}
						@if ($errors->has('type_id'))
						<p class="text-danger">{{ $errors->first('type_id') }}</p>
						@endif
						{{ Form::select('type_id', array(0 => 'IN', 1 => 'OUT'), old('type_id'), array('class' => 'form-control', 'id' => 'selectType')) }}
					</div>
				</div>



				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('selectDateTime', 'Date Time') }}
						@if ($errors->has('datetime'))
							<p class="text-danger">{{ $errors->first('datetime') }}</p>
						@endif
						<div class="input-group date" id="pick_start_date">
							{{ Form::text('datetime', old('datetime'), array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD HH:mm:ss', 'id' => 'date1')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>



				<div class="form-group">
					<div class="col-md-4 required">
						{{ Form::label('selectRemark', 'Remark') }}
						@if ($errors->has('remark'))
							<p class="text-danger">{{ $errors->first('remark') }}</p>
						@endif
						{{ Form::textarea('remark', old('remark'), ['class' => 'form-control', 'size' => '30x3']) }}
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-4">
						{{ Form::label('Attachment') }}
						@if ($errors->has('att_file'))
							<p class="text-danger">{{ $errors->first('att_file') }}</p>
						@endif
						{{ Form::file('att_file') }}
					</div>
				</div>
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-md-6">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }}
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}


<div class="well">
	<strong>Remarks: </strong>
	<br>1) Please fill all the required fields.
	<br>2) Attachment must image format. (jpg/png)
</div>

<script type="text/javascript">
$(function () {
	$('#pick_start_date').datetimepicker();
});
</script>


@stop