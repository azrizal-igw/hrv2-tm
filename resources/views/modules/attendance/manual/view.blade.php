@extends('layouts/backend')


@section('content')



{{ Form::open(array('class' => 'form-horizontal')) }} 
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Name</td>
						<td class="col-md-9">{{ $manual->UserDetail->name }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>
							{{ $manual->icno }}
						</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>
							@if (!empty($manual->UserDetail->UserCurrentJob))
								{{ $manual->UserDetail->UserCurrentJob->PositionName->name }}
							@else
								{{ '-' }}
							@endif							
						</td>
					</tr>
					<tr>
						<td>Sitecode</td>
						<td>
							{{ $manual->sitecode }}
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<td colspan="2"><strong>Manual Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td>Date Apply</td>
						<td>{{ $manual->action_date }}</td>
					</tr>
					<tr>
						<td>Action By</td>
						<td>{{ $manual->UserActionBy->name }}</td>
					</tr>
					<tr>
						<td class="col-md-3">Date Time</td>
						<td class="col-md-9">
							{{ $manual->att_log_time }}
						</td>
					</tr>
					<tr>
						<td>Type</td>
						<td>
							@if ($manual->type_id == 0)
								{{ 'IN' }}
							@elseif ($manual->type_id == 1)
								{{ 'OUT' }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Remark</td>						
						<td>
							@if (!empty($manual->remark))
								{{ $manual->remark }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Attachment</td>
						<td>
						@if (!empty($manual->AttManualAttachment))
							<?php
								$attachment = $manual->AttManualAttachment->filename.'.'.$manual->AttManualAttachment->ext;
							?>
							<a href="{{ route('lib.file.attendance.manual', array($attachment)) }}" target="_blank"><i class="icon-paper-clip"></i></a>							
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="panel-footer">
		<div class="row">
			<div class="col-md-12"> 
				{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger btn_click', 'id' => 'cancel', 'title' => 'Cancel', 'alt' => 2)) }}
				{{ Form::hidden('id', $manual->id) }}
				{{ Form::hidden('type_id', $manual->type_id) }}
			</div>
		</div>
	</div>	
</div>
{{ Form::close() }}




@stop


