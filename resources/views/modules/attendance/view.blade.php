@extends('layouts/backend')


@section('content')


<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin=""></script>


@if ($data['date'] <= $data['today'])


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">


		@include('attendance/includes/view/att')


	</div>




	@if ($errors->has('remark'))
		<div class="row">
			<div class="col-md-12">
				<span class="col-lg-12 text-danger">{{ $errors->first('remark') }}</span>
			</div>
		</div>
	@endif  	





	@if (!empty($data['remarks']))
		@if ($data['remarks']->AttActiveStatus)
			@if ($data['remarks']->AttActiveStatus->status == 1)
			<!-- pending remark	 -->
			<div class="panel-footer">
				<div class="row">
					<div class="col-md-6"> 
						<a href="{{ route('mod.attendance.user.edit', array($data['user'][0], $data['user'][1], $data['user'][2], $data['date'])) }}" class="btn btn-primary" title="Edit"><i class="icon-note"></i>&nbsp;Edit if Other Remark</a>
					</div>
					<div class="col-md-6 text-right">
						{{ Form::button('<i class="icon-check"></i>&nbsp;Approve', array('class' => 'btn btn-success', 'id' => 'popup-modal', 'title' => 'Approve Remark', 'alt' => 2)) }}
						{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Cancel Remark', 'alt' => 4)) }}
					</div>						
				</div>
			</div>

			@elseif (in_array($data['remarks']->AttActiveStatus->status, array(2,3)))
			<!-- approved/updated remark -->
			<div class="panel-footer">
				<div class="row">
					<div class="col-md-6"> 
						{{ Form::button('<i class="icon-note"></i>&nbsp;Edit Notes', array('class' => 'btn btn-primary', 'id' => 'popup-modal', 'title' => 'Edit Notes', 'alt' => 6)) }}
					</div>
					<div class="col-md-6 text-right">
						{{ Form::button('<i class="icon-close"></i>&nbsp;Reject', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Reject Remark', 'alt' => 5)) }}
					</div>						
				</div>
			</div>				
			@endif

			{{ Form::hidden('id', $data['remarks']->id) }}
		@endif
	@endif
	<div class="modal fade" id="att-modal"></div>

</div>
{{ Form::close() }}





@if (count($data['remarks']) > 0)
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table table-striped table-condensed table-responsive table-hover">
						<thead>
							<tr class="bg-inverse">
								<td colspan="5"><i class="icon-info"></i>&nbsp;<strong>Status Remark</strong></td>
							</tr>
						</thead>						
						<tr class="active">
							<th class="col-md-1">No</th>
							<th class="col-md-2">Status</th>
							<th class="col-md-2">Action Date</th>
							<th class="col-md-3">Action By</th>
							<th class="col-md-4">Remarks</th>
						</tr>
						<?php $no = 0; ?>
						@foreach ($data['remarks']->AttAllStatus as $r)
							<?php $no++; ?>
							<tr>
								<td>{{ $no }}</td>
								<td>
									{{ $r->AttRemarkStatusName->name }}
								</td>
								<td>{{ $r->action_date }}</td>
								<td>{{ $r->AttRemarkActionBy->name }}</td>
								<td>{{ $r->action_remark }}</td>
							</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
@endif








@if (!empty($data['other']))
	<div class="panel panel-primary">
		<div class="panel-body">

			<div class="row">
				<div class="col-md-12">	
					<table class="table table-condensed table-responsive table-hover">
						<thead>
							<tr class="bg-inverse">
								<td colspan="2"><i class="icon-doc"></i>&nbsp;<strong>Time Slip</strong></td>
							</tr>
						</thead>
						<tr>
							<td class="col-md-3">Apply Date</td>
							<td class="col-md-9">{{ $data['other']['action_date'] }}</td>
						</tr>
						<tr>
							<td>Notes</td>
							<td>{{ $data['other']['notes'] }}</td>
						</tr>	
						<tr>
							<td>Attachment</td>
							<td>
								@if ($data['other']->AttAttachment)													
									<?php
										$attachment = $data['other']->AttAttachment->filename . '.' . $data['other']->AttAttachment->ext;
										$photo = route('lib.file.attendance.remark', array($attachment));
										$thumb = $data['other']->AttAttachment->thumb_name . '.' . $data['other']->AttAttachment->ext;
										$thumb_url = route('lib.file.attendance.remark.thumb', array($thumb));
									?>
									<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
					</table>
				</div>
			</div>

		</div>
	</div>
@endif




@if ($data['on'] == 1)
{{ Form::open(array('class' => 'form-horizontal', 'files' => true)) }}

<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Set Remark</legend>
				<div class="form-group">
					@if ($errors->has('att_status'))
						<p class="col-lg-12 text-danger">{{ $errors->first('att_status') }}</p>
					@endif   	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Remark Status</label>
					</div>
					<div class="col-lg-4">
						{{ Form::select('att_status', $status, old('att_status'), array('class' => 'form-control', 'id' => 'selectStatus')) }}  
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('att_notes'))
						<p class="col-lg-12 text-danger">{{ $errors->first('att_notes') }}</p>
					@endif   	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Notes</label>
					</div>
					<div class="col-lg-4">
						{{ Form::textarea('att_notes', old('att_notes'), ['class' => 'form-control', 'size' => '30x3']) }}  
					</div>
				</div>

				<div class="form-group" id="i2">
					@if ($errors->has('att_file'))
						<p class="col-lg-12 text-danger">{{ $errors->first('att_file') }}</p>
					@endif  
					<label class="col-lg-3 control-label" for="textArea">Attachment</label>
					<div class="col-lg-9">
						<label>
							{{ Form::file('att_file') }}
						</label>
					</div>
				</div>
				{{ Form::hidden('type', 0) }}

				<br><strong>Notes:</strong>
				<br>1) This Attendance Remark will save as Remark By RM.
				<br>2) Case of punch the Attendance but not appear in system/Wrong IC/Sitecode ask UMS Support 1st to check.
				<br>3) If problem (2) still persist use Attendance Manual.
				<br>4) File attachment must be in following types jpeg/jpg/png and below 2MB. 
			</div>

			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-12">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
					</div>
				</div>
			</div>

		</div>
	</div>
</div>





{{ Form::close() }}

@endif





<div class="well">
	<strong>Remarks Status: </strong>
	<br>1) WORKING - Time In and Time Out must not Empty.
	<br>2) MC - Applicable if apply the Leave (Medical Leave).
	<br>3) TRAINING - If have Training/Outstation.
	<br>4) OFF DAY - Applicable if the Date falls on Off Day.
	<br>5) AL-EL - Applicable if apply the Leave (Annual Leave).
	<br>6) PART TIMER - Applicable if Staff Position is Part Timer.
	<br>7) TBU (To Be Update) can use for cases like Blackout/No Internet/Wrong IC/Sitecode. Please follow-up if choose this.
	<br>8) INCOMPLETE - Set if Staff Fail to Punch the Attendance.
	<br>9) PUBLIC HOLIDAY - Applicable if Date falls on Public Holiday.
</div>


<script type="text/javascript">
$(document).ready(function(){

	// save leave
    $(document).on('click','#att-update', function(){
    	if ($('#remark').val() == "") {
    		alert('Please insert Notes.');
    	}
    	else {
	        var answer = confirm('Are you sure want to process this remark?');
	        if (answer == true) {
	            $('#form-list').submit(); 
	        }
	        else {
	            return false;
	        } 
	    }
    });	    

    // display popup modal
	$(document).on('click','#popup-modal',function() {  
		var type = $(this).attr('alt');
		str =  '';   		
		str += '<div class="modal-dialog">';
		str += '	<div class="modal-content">';
		str += '		<div class="modal-header">';
		str += '			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		str += '			<legend>' + $(this).attr('title') + '</legend>';
		str += '		</div>';
		str += '		<div class="modal-body">';
		str += '			<p><textarea name="remark" rows="4" cols="30" id="remark" placeholder="Insert Notes Here"></textarea></p><input type="hidden" name="type" value="' + type + '" />';
		str += '			<strong>Notes:</strong>';
		str += '			<br>1) Approve Remark - The Attendance Remark is valid.';
		str += '			<br>2) Cancel Remark - Used when Site Supervisor put wrong info/attachment on the Attendance Remark. Ask them to keyin again/No need to apply/Apply Leave.';
		str += '		</div>';
		str += '		<div class="modal-footer">';
		str += '			<div class="btn-group">';
		str += '				<button type="button" class="btn btn-primary" id="att-update">Submit</button>';
		str += '				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
		str += '			</div>';
		str += '		</div>';
		str += '	</div>';
		str += '</div>';
		$('#att-modal').html(str);                                                    
		$('#att-modal').modal();  
	}); 
});  
</script>





@else
<div class="panel panel-primary">
	<div class="panel-body nopadding">			
        <div class="row">
            <div class="col-md-12">Selected Date is invalid. Cannot process future Date.</div>
        </div>
	</div>
</div>	
@endif







@stop