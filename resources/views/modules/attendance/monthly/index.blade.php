@extends('layouts/backend')


@section('content')


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>


<div class="row">
	<div class="col-md-12">
		<div class="well">

			{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-10">
							{{ Form::select('site_id', $sites, null, array('class' => 'form-control', 'id' => 'site_id')) }}
						</div>
						<div class="col-md-2">
							{{ Form::selectYear('year', $start, date('Y'), $sessions['year'], ['class' => 'form-control', 'title' => 'Year']) }}
						</div>															
					</div>
					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search', 'id' => 'btn-search')) }}
				</div>
			</div>
			{{ Form::close() }}

		</div>
	</div>            
</div>


@if ($sessions['site_id'])
	{!! $calendar->calendar() !!}
	{!! $calendar->script() !!}
@endif


<script type="text/javascript">
$('#calendar').fullCalendar({
    dayClick: function() {
        alert('a day has been clicked!');
    }
});	
</script>
<br>

@stop