@extends('layouts/backend')

@section('content')

@include('attendance/includes/daily/search')

{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}             
<div class="row">
	<div class="col-sm-12">
		<div class="well">

			@include('attendance/includes/daily/table')

		</div>
	</div>
</div>
{{ Form::close() }}

@include('attendance/includes/daily/footer')

<style>
#mapid {
  height: 200px;
}
</style>

@stop



