<style type="text/css">
#body {
    font: normal 12px Verdana, Arial, sans-serif;
}
#header {
	text-align: center;
}
#title {
	text-align: center;
	font-weight: bold;
}
.page-break {
    page-break-after: always;
}
</style>

<?php $no = 0; $total = count($datas['datas']); ?>

@if (!empty($datas['datas']))

	@foreach ($datas['datas'] as $data)
		<?php $no++; ?>

		<div id="header"><img src="{{ url('assets/images/logo-msd.png') }}" /></div>

		<div id="title">{{ 'Attendance' }} {{ \Carbon\Carbon::createFromFormat('n', $data['month'])->format('F') }} {{ $data['year'] }}</div>
		<br>

		{{ $datas['name'] }}
		<br>

		{{ $datas['site'] }}
		<br>
		<br>

		<div class="row" id="body">
			<div class="col-sm-12">
				<div class="well">

					@include('attendance/includes/daily/pdf')

				</div>
			</div>
		</div>

		@if ($no != $total)
			<div class="page-break"></div>
		@endif

	@endforeach 

@endif




