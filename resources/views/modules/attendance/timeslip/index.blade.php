@extends('layouts/backend')


@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
						</div>
					</div>


	               <div class="row">
						<div class="col-md-6">
							{{ Form::text('keyword', null, array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
						</div>
						<div class="col-md-3">
							<div class="input-group date" id="pick_att_date">
								{{ Form::text('att_date', $sessions['att_date'], array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'apply_date', 'placeholder' => 'Attendance Date')) }}
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $status, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>
	               </div>


					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}



<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No /<br>Position</th>
					<th class="col-md-4">Site Name /<br>Region /<br>Phase</th>
					<th class="col-md-2">Apply Date /<br>Date /<br>Status</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>
			@if (count($timeslip) > 0)
				<?php $no = $timeslip->firstItem() - 1;?>
				@foreach ($timeslip as $i)
					<?php $no++;$cancel = 0;?>
					<tr>
						<td>{{ $no }}</td>

						<td>
							{{ $i->AttUserInfo->name }} /<br>
							{{ $i->icno }} /<br>
							{{ $i->AttUserJob->PositionName->name }} /<br>

						</td>

						<td>
							{{ $i->AttSiteInfo->code }}&nbsp;{{ $i->AttSiteInfo->name }} /<br>
							{{ $i->AttUserJob->RegionName->name }} /<br>
							{{ $i->AttUserJob->PhaseName->name }}

						</td>

						<td>
							{{ $i->action_date }} /<br>
							{{ $i->date }} /<br>
							{!! $i->status !!}
						</td>

						<td class="text-right">
							<a href="#" data-url="{{ route('mod.attendance.timeslip.view', array($i->id, $i->user_id, $i->sitecode)) }}" data-toggle="modal" id="list" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
						</td>

					</tr>
				@endforeach
			@else
				<tr><td colspan="5">No record.</td></tr>
			@endif
		</table>

		<div class="well">
			<div class="paging text-center">
				{{ $timeslip->render() }}
				<p>{{ 'Total: '.$timeslip->total() }}</p>
			</div>
		</div>
	</div>
</div>





<script type="text/javascript">
$(function () {
   getDatePicker('pick_att_date');
});
</script>



@stop