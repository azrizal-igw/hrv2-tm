<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">

      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
      <div class="modal-content">

         <div class="modal-body">
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="2"><strong>Time Slip</strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td>Name</td>
                     <td>{{ $timeslip->AttUserInfo->name }}</td>
                  </tr>
                  <tr>
                     <td>Position</td>
                     <td>{{ $timeslip->AttUserJob->PositionName->name }}</td>
                  </tr>
                  <tr>
                     <td>Site Name</td>
                     <td>{{ $timeslip->sitecode }} {{ $timeslip->AttSiteInfo->name }}</td>
                  </tr>
                  <tr>
                     <td class="col-md-3">Attendance Date</td>
                     <td class="col-md-9">{{ $timeslip->date }}</td>
                  </tr>
                  <tr>
                     <td>Notes</td>
                     <td>{{ $timeslip->notes }}</td>
                  </tr>
                  <tr>
                     <td>Apply Date</td>
                     <td>{{ $timeslip->action_date }}</td>
                  </tr>
                  <tr>
                     <td>Attachment</td>
                     <td>

                        @if ($timeslip->AttAttachment)                                     
                           <?php
                              $attachment = $timeslip->AttAttachment->filename . '.' . $timeslip->AttAttachment->ext;
                              $photo = route('lib.file.attendance.remark', array($attachment));
                              $thumb = $timeslip->AttAttachment->thumb_name . '.' . $timeslip->AttAttachment->ext;
                              $thumb_url = route('lib.file.attendance.remark.thumb', array($thumb));
                           ?>
                           <a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
                        @else
                           {{ '-' }}
                        @endif

                     </td>
                  </tr>
               </tbody>
            </table>
         </div>

         @if ($timeslip->active == 1)
            <div class="panel-footer">
               <div class="row">
                  <div class="col-md-12">
                        {{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger btn_click', 'id' => 'cancel', 'title' => 'Cancel Time Slip', 'alt' => 4)) }}
                  </div>
               </div>
            </div>
         @endif

      </div>
      {{ Form::close() }}

   </div>
</div>




