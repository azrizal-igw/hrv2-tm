@extends('layouts/backend')


@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
						</div>
					</div>

	                <div class="row">
						<div class="col-md-3">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
						</div>	  

						<div class="col-md-3">
							<div class="input-group date" id="pick_att_date">
							{{ Form::text('att_date', $sessions['att_date'], array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'att_date', 'placeholder' => 'Attendance Date')) }}
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
						<div class="col-md-3">
							{{ Form::select('punch_id', $punch, $sessions['punch_id'], array('class' => 'form-control')) }}
						</div>	
						<div class="col-md-3">
							{{ Form::select('type_id', $types, $sessions['type_id'], array('class' => 'form-control')) }}
						</div>

	                </div>


					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}



<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No /<br>Position</th>
					<th class="col-md-4">Site Name /<br>Region /<br>Phase</th>
					<th class="col-md-2">Date Time /<br>In/Out /<br>Type</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>
			<tbody>
				@if (count($att_gps) > 0)
					<?php $no = $att_gps->firstItem() - 1;?>
					@foreach ($att_gps as $i)
						<?php $no++; ?>
							<tr>
								<td>{{ $no }}</td>
								<td>
									{{ $i->AttGpsUser->name }} / <br>
									{{ $i->AttGpsUser->icno }} /<br>
									@if (!empty($i->AttGpsUserJob))
										{{ $i->AttGpsUserJob->PositionName->name }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td>
									@if (!empty($i->AttGpsSite))
										{{ $i->AttGpsSite->code }} {{ $i->AttGpsSite->name }}
									@else
										{{ '-' }}
									@endif
									/<br>
									@if ($i->AttGpsUserJob)
										{{ $i->AttGpsUserJob->RegionName->name }}
									@else
										{{ '-' }}
									@endif 
									/<br>
									@if ($i->AttGpsUserJob)
										{{ $i->AttGpsUserJob->PhaseName->name }}
									@else
										{{ '-' }}
									@endif 									
								</td>
								<td>
									{{ $i->datetime }} 
									/<br>
									@if ($i->in_out == 0) 
										{{ 'IN' }}
									@else
										{{ 'OUT' }}
									@endif 
									/<br>
									@if (empty($i->qr_code))
										{{ 'GPS' }}
									@else
										{{ 'QRCODE' }}
									@endif
								</td>
								<td class="text-right">
									<a href="{{ route('mod.attendance.mobile.view', array($i->id, $i->user_id, $i->sitecode)) }}" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
								</td>								
							</tr>	
					@endforeach
				@else
					<tr>
						<td colspan="5">No record</td>
					</tr>
				@endif							
			</tbody>
		</table>

		<div class="well">
			<div class="paging text-center">
				{{ $att_gps->render() }}
				<p>{{ 'Total: '.$att_gps->total() }}</p>
			</div>
		</div>

	</div>
</div>


<script type="text/javascript">
$(function () {
   getDatePicker('pick_att_date');
});
</script>


@stop


