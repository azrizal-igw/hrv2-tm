@extends('layouts/backend')

@section('content')



<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-condensed table-responsive table-hover">
			<thead>
				<tr class="bg-inverse">
					<th class="col-md-1">No</th>
					<th class="col-md-1 text-left">Version</th>
					<th class="col-md-2 text-left">Build No.</th>
					<th class="col-md-1 text-center">Server 1</th>
					<th class="col-md-1 text-center">Server 2</th>
					<th class="col-md-1 text-center">Server 3</th>
					<th class="col-md-1 text-center">Server 4</th>
					<th class="col-md-1 text-center">Server 5</th>
					<th class="col-md-2 text-center">Status</th>
					<th class="col-md-1 text-right">Actions</th>
				</tr>
			</thead>
			<tbody>
				@if (!empty($version))
					<?php $no = 0; ?>
					@foreach ($version as $i)
						<?php $no++; ?>
						<tr>
							<td>{{ $no }}</td>
							<td>{{ $i->version }}</td>
							<td>{{ $i->build_no }}</td>
							<td class="text-center">
								@if ($i->server1)
									<a href="{{ $i->server1 }}" target="_blank"><i class="icon-link"></i></a>
								@else
									{{ '-' }}
								@endif
							</td>
							<td class="text-center">
								@if ($i->server2)
									<a href="{{ $i->server2 }}" target="_blank"><i class="icon-link"></i></a>
								@else
									{{ '-' }}
								@endif
							</td>
							<td class="text-center">
								@if ($i->server3)
									<a href="{{ $i->server3 }}" target="_blank"><i class="icon-link"></i></a>
								@else
									{{ '-' }}
								@endif
							</td>
							<td class="text-center">
								@if ($i->server4)
									<a href="{{ $i->server4 }}" target="_blank"><i class="icon-link"></i></a>
								@else
									{{ '-' }}
								@endif
							</td>

							<td class="text-center">
								@if ($i->server5)
									<a href="{{ $i->server5 }}" target="_blank"><i class="icon-link"></i></a>
								@else
									{{ '-' }}
								@endif
							</td>
							<td class="text-center">{!! $i->availability !!}</td>
							<td class="text-right">
								<a href="{{ route('mod.attendance.mobile.version.edit', array($i->id)) }}" class="btn btn-danger btn-sm" title="Edit"><i class="icon-note"></i></a>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>


{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>App Version</legend>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Version') }}
						@if ($errors->has('version'))
							<p class="text-danger">{{ $errors->first('version') }}</p>
						@endif
						{{ Form::text('version', old('version'), array('class'=>'form-control')) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Build No.') }}
						@if ($errors->has('build_no'))
							<p class="text-danger">{{ $errors->first('build_no') }}</p>
						@endif
						{{ Form::text('build_no', old('build_no'), array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 1') }}
						{{ Form::text('server1', old('server1'), array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 2') }}
						{{ Form::text('server2', old('server2'), array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 3') }}
						{{ Form::text('server3', old('server3'), array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 4') }}
						{{ Form::text('server4', old('server4'), array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 5') }}
						{{ Form::text('server5', old('server5'), array('class'=>'form-control')) }}
					</div>
				</div>


			</div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'button', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }}
                    </div>
                </div>
            </div>


		</div>
	</div>
</div>




{{ Form::close() }}



@stop


