@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>App Version</legend>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Version') }}
						@if ($errors->has('version'))
							<p class="text-danger">{{ $errors->first('version') }}</p>
						@endif
						{{ Form::text('version', $data->version, array('class'=>'form-control')) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Build No.') }}
						@if ($errors->has('build_no'))
							<p class="text-danger">{{ $errors->first('build_no') }}</p>
						@endif
						{{ Form::text('build_no', $data->build_no, array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 1') }}
						{{ Form::text('server1', $data->server1, array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 2') }}
						{{ Form::text('server2', $data->server2, array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 3') }}
						{{ Form::text('server3', $data->server3, array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 4') }}
						{{ Form::text('server4', $data->server4, array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Server 5') }}
						{{ Form::text('server5', $data->server5, array('class'=>'form-control')) }}
					</div>
				</div>


			</div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'button', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }}
                    </div>
                </div>
            </div>


		</div>
	</div>
</div>
{{ Form::close() }}




@stop

