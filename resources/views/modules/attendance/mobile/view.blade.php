@extends('layouts/backend')


@section('content')


<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin=""></script>


{{ Form::open(array('class' => 'form-horizontal')) }} 
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Name</td>
						<td class="col-md-9">{{ $mobile->AttGpsUser->name }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $mobile->AttGpsUser->icno }}</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>
							@if ($mobile->AttGpsUserJob)
								{{ $mobile->AttGpsUserJob->PositionName->name }}
							@else
								{{ '-' }}
							@endif						
						</td>
					</tr>
					<tr>
						<td>Sitecode</td>
						<td>
							@if ($mobile->AttGpsSite)
								{{ $mobile->AttGpsSite->code }} {{ $mobile->AttGpsSite->name }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<td colspan="2"><strong>Attendance Info</strong></td>
						</tr>
					</thead>					
					<tr>
						<td>QR Code</td>
						<td>
							@if (!empty($mobile->qr_code))
								{{ $mobile->qr_code }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Latitude</td>
						<td>{{ $mobile->latitude }}</td>
					</tr>
					<tr>
						<td>Longitude</td>
						<td>{{ $mobile->longitude }}</td>
					</tr>
					<tr>
						<td>Map</td>
						<td>
							<div id="mapid"></div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3">Date Time</td>
						<td class="col-md-9">{{ $mobile->datetime }}</td>
					</tr>
					<tr>
						<td>Type</td>
						<td>
							@if ($mobile->in_out == 0)
								{{ 'IN' }}
							@else
								{{ 'OUT' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Location</td>
						<td>{{ $mobile->location }}</td>
					</tr>
					<tr>
						<td>Remark</td>
						<td>
							@if ($mobile->remark)
								{{ $mobile->remark }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Flag</td>
						<td>{{ $mobile->AttGpsStatus->name }}</td>
					</tr>
					<tr>
						<td>Imei</td>
						<td>{{ $mobile->imei }}</td>
					</tr>
					<tr>
						<td>Version</td>
						<td>{{ $mobile->version }}</td>
					</tr>
					<tr>
						<td>Build No.</td>
						<td>{{ $mobile->build_no }}</td>
					</tr>
					<tr>
						<td>Model</td>
						<td>{{ $mobile->model }}</td>
					</tr>
					<tr>
						<td>Transfer</td>
						<td>
							@if ($mobile->transfer == 1)
								{{ 'Online' }}
							@else
								{{ 'Offline' }}
							@endif
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>


	@if (auth()->user()->is_admin == 1)
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-12"> 
					{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger btn_click', 'id' => 'cancel', 'title' => 'Cancel')) }}
					{{ Form::hidden('id', $mobile->id) }}
				</div>
			</div>
		</div>
	@endif	

</div>
{{ Form::close() }}



<style type="text/css">
#mapid { 
	height: 380px; 
}	
</style>


<?php
	$lat = $mobile->latitude;
	$long = $mobile->longitude;
	$location = $mobile->location;
?>

<script type="text/javascript">
	var mymap = L.map('mapid').setView([<?php echo $lat; ?>, <?php echo $long; ?>], 13);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaHJ2MiIsImEiOiJjanBrbmJubTMwNTY5NDhxeHFlc2hheGhtIn0.G0Cl9rPkl552LHlr5cdDVQ', {
	    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	    maxZoom: 18,
	    id: 'mapbox.streets',
	    accessToken: 'your.mapbox.access.token'
	}).addTo(mymap);

	marker = L.marker([<?php echo $lat; ?>, <?php echo $long; ?>]).addTo(mymap);
	marker.on('mouseover', function(e){
		e.target.bindPopup('<?php echo $location; ?>').openPopup();
	});      
	marker.on('mouseout', function(e){  
		e.target.closePopup();    
	});

	// var popup = L.popup();
	// function onMapClick(e) {
	// 	popup
	// 		.setLatLng(e.latlng)
	// 		.setContent("You clicked the map at " + e.latlng.toString())
	// 		.openOn(mymap);
	// }

	// mymap.on('click', onMapClick);


</script>

@stop


