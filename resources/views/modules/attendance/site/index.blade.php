@extends('layouts/backend')


@section('content')





<div class="row">
	<div class="col-md-12">
		<div class="well">

			{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control', 'id' => 'site_id')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control', 'id' => 'region_id')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control', 'id' => 'phase_id')) }}
						</div>																											
					</div>

					<div class="row">
						<div class="col-md-9">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Site Name/Code', 'size' => 40)) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $staff_status, $sessions['status_id'], array('class' => 'form-control', 'id' => 'phase_id')) }}
						</div>													
					</div>
					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search', 'id' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>
			{{ Form::close() }}

		</div>
	</div>            
</div>





<div class="row">
	<div class="col-sm-12">
        <table class="table table-striped table-condensed table-responsive table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Site Name</th>
                    <th>Region / Phase</th>
                    <th>Staff</th>
                </tr>
            </thead>
            <tbody>
				@if (count($lists) > 0)
					<?php $no = $lists->firstItem() - 1;?>
					@foreach ($lists as $i)
						<?php 
							$no++; 
						?>
						<tr>
							<td>{{ $no }}</td>
							<td>{{ $i->name }} /<br>{{ $i->code }}</td>
							<td>
								@if (!empty($i->RegionName))
									{{ $i->RegionName->name }}
								@else
									{{ '-' }}
								@endif
								/<br>
								@if (!empty($i->PhaseName))
									{{ $i->PhaseName->name }}
								@else
									{{ '-' }}
								@endif								
							</td>

							<td>
				                @if (count($i->UserJobSite) > 0)

									<div class="row">
										<div class="col-md-12">				                
						                	@foreach ($i->UserJobSite as $j)

						                		@if (empty($sessions['status_id']))
													<div class="row" style="margin-bottom: 3px;">
														<div class="col-md-5">
															{{ $j->PositionName->name }}
														</div>
														<div class="col-md-5">
															{{ $j->UserDetail->name }} /<br>
															{{ $j->UserDetail->icno }} /<br>
															{{ $j->UserDetail->staff_id }} /<br>
															{!! $j->availability !!}
														</div>
														<div class="col-md-2 text-right">
															<a href="{{ route('mod.attendance.user', array($j->UserDetail->icno, $j->sitecode)) }}" class="btn btn-primary btn-sm" title="View Attendance"><i class="icon-magnifier-add"></i></a>
														</div>
													</div>
						                		@else
							                		@if ($sessions['status_id'] == $j->status)
														<div class="row" style="margin-bottom: 3px;">
															<div class="col-md-5">
																{{ $j->PositionName->name }}
															</div>
															<div class="col-md-5">
																{{ $j->UserDetail->name }} /<br>
																{{ $j->UserDetail->icno }} /<br>
																{{ $j->UserDetail->staff_id }} /<br>
																{!! $j->availability !!}
															</div>
															<div class="col-md-2 text-right">
																<a href="{{ route('mod.attendance.user', array($j->UserDetail->icno, $j->sitecode)) }}" class="btn btn-primary btn-sm" title="View Attendance"><i class="icon-magnifier-add"></i></a>
															</div>
														</div>
													@endif
												@endif

						                	@endforeach
						                </div>
						            </div>
				                @else
			                    	{{ '-' }}
				                @endif
							</td>

						</tr>
		            @endforeach
		        @endif
            </tbody>
        </table>

        <div class="well">
			<div class="paging text-center">  
				{{ $lists->render() }}      
				<p>{{ 'Total: '.$lists->total() }}</p>
			</div>        	
        </div>



	</div>
</div>





@stop


