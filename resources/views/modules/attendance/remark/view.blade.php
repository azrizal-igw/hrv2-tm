@extends('layouts/backend')


@section('content')





{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Attendance Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Date</td>
						<td class="col-md-9">{{ $data['date'] }}</td>
					</tr>
					<tr>
						<td>Time In</td>
						<td>
							{!! $data['att']['in'] !!}
						</td>
					</tr>
					<tr>
						<td>Time Out</td>
						<td>
							{!! $data['att']['out'] !!}
						</td>
					</tr>

					@if (!empty($data['off_day']))
						<tr>
							<td>Remark</td>
							<td>
								@if (!empty($data['off_day']['off_type_name']))
									{{ $data['off_day']['off_type_name']['name'] }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
					@endif

					@if (!empty($data['public']))
						<tr>
							<td>Remark</td>
							<td>
								@if (!empty($data['public']['public_name']))
									{{ $data['public']['public_name']['desc'] }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
					@endif

					<?php $status_half = 0; ?>
					@if (!empty($data['leave']->LeaveApproveOne))
						<tr>
							<td>Apply Leave</td>
							<td>
								{{ $data['leave']->LeaveInfo->LeaveTypeName->name }} 
								@if ($data['leave']->LeaveInfo->is_half_day == 1) 
									<?php $status_half = 1; ?>
									{{ '(Half Day)' }}
								@endif								
							</td>
						</tr>
					@endif





					@if (!empty($data['remarks']->AttApplyRemark))
						<tr>
							<td>Request Remark</td>
							<td>
								{{ $data['remarks']->AttStatusName->name }}
							</td>
						</tr>
					@else
						<tr><td>Request Remark</td><td>-</td></tr>
					@endif


					@if ($data['remarks']->AttActiveStatus->status == 3)
						<tr>
							<td>Updated Remark</td>
							<td>
								{{ $data['remarks']->AttOtherName->name }}
							</td>
						</tr>
					@else
						<tr><td>Updated Remark</td><td>-</td></tr>
					@endif



					<tr>
						<td>Notes</td>
						<td>
							{{ $data['remarks']->notes }}
						</td>
					</tr>
					<tr>
						<td>Status Req. Remark</td>
						<td>
							@if ($data['remarks']->AttActiveStatus)
								@if ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 1)
									<?php $color = 'text-danger'; ?>
								@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 2)
									<?php $color = 'text-primary'; ?>
								@else
									<?php $color = 'text-info'; ?>									
								@endif
								<span class="{{ $color }}">{{ $data['remarks']->AttActiveStatus->AttRemarkStatusName->name }}</span>
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Attachment</td>
						<td>
							@if ($data['remarks']->AttActiveAttachment)													
								<?php
									$attachment = $data['remarks']->AttActiveAttachment->filename . '.' . $data['remarks']->AttActiveAttachment->ext;
									$photo = route('lib.file.attendance.remark', array($attachment));
									$thumb = $data['remarks']->AttActiveAttachment->thumb_name . '.' . $data['remarks']->AttActiveAttachment->ext;
									$thumb_url = route('lib.file.attendance.remark.thumb', array($thumb));
								?>
								<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>						
					<tr>
						<td colspan="2">
							@if ($data['remarks']->AttActiveStatus)
								@if ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 1)
									{{ 'Note: This Request is waiting for Approval from Region Manager (RM).' }}
								@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 2)
									{{ 'Note: This remark is approved.' }}
								@elseif ($data['remarks']->AttActiveStatus->AttRemarkStatusName->id == 3)
									{{ 'Note: This remark set by RM.' }}
								@endif
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>

				</table>
				@if ($status_half == 1)
					<p><strong>Notes:</strong>
						<br>1) If apply Half Day make sure have Punch In & Punch Out.
					</p>
				@endif



			</div>
		</div>
	</div>







	@if ($errors->has('remark'))
		<div class="row">
			<div class="col-md-12">
				<span class="col-lg-12 text-danger">{{ $errors->first('remark') }}</span>
			</div>
		</div>
	@endif  	





	@if (!empty($data['remarks']))
		@if ($data['remarks']->AttActiveStatus)
			@if ($data['remarks']->AttActiveStatus->status == 1)
			<!-- pending remark	 -->
			<div class="panel-footer">
				<div class="row">
					<div class="col-md-6"> 
						<a href="{{ route('mod.attendance.remark.edit', array($remark_id, $data['user'][0], $data['user'][1], $data['user'][2])) }}" class="btn btn-primary" title="Edit"><i class="icon-note"></i>&nbsp;Edit if Other Remark</a>
					</div>
					<div class="col-md-6 text-right">
						{{ Form::button('<i class="icon-check"></i>&nbsp;Approve', array('class' => 'btn btn-success', 'id' => 'popup-modal', 'title' => 'Approve Remark', 'alt' => 2)) }}
						{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Cancel Remark', 'alt' => 4)) }}
					</div>						
				</div>
			</div>

			@elseif (in_array($data['remarks']->AttActiveStatus->status, array(2,3)))
			<!-- approved remark	 -->
			<div class="panel-footer">
				<div class="row">
					<div class="col-md-6"> 
						{{ Form::button('<i class="icon-note"></i>&nbsp;Edit Notes', array('class' => 'btn btn-primary', 'id' => 'popup-modal', 'title' => 'Edit Notes', 'alt' => 6)) }}
					</div>
					<div class="col-md-6 text-right">
						{{ Form::button('<i class="icon-close"></i>&nbsp;Reject', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Reject Remark', 'alt' => 5)) }}
					</div>						
				</div>
			</div>				
			@endif

			{{ Form::hidden('id', $data['remarks']->id) }}
			{{ Form::hidden('date', $data['remarks']->date) }}
		@endif
	@endif
	<div class="modal fade" id="att-modal"></div>




</div>
{{ Form::close() }}








<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<table class="table table-striped table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-inverse">
							<td colspan="5"><i class="icon-info"></i>&nbsp;<strong>Status Remark</strong></td>
						</tr>
					</thead>						
					<tr class="active">
						<th class="col-md-1">No</th>
						<th class="col-md-2">Status</th>
						<th class="col-md-2">Action Date</th>
						<th class="col-md-3">Action By</th>
						<th class="col-md-4">Remarks</th>
					</tr>

					@if (count($data['remarks']) > 0)
						<?php $no = 0; ?>
						@foreach ($data['remarks']->AttAllStatus as $r)
							<?php $no++; ?>
							<tr>
								<td>{{ $no }}</td>
								<td>
									{{ $r->AttRemarkStatusName->name }}
								</td>
								<td>{{ $r->action_date }}</td>
								<td>{{ $r->AttRemarkActionBy->name }}</td>
								<td>{{ $r->action_remark }}</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td colspan="5">No Request Record.</td>
						</tr>
					@endif				
				</table>
			</div>
		</div>
	</div>
</div>







@if (!empty($data['other']))
	<div class="panel panel-primary">
		<div class="panel-body">

			<div class="row">
				<div class="col-md-12">	
					<table class="table table-condensed table-responsive table-hover">
						<thead>
							<tr class="bg-primary">
								<td colspan="2"><strong>Time Slip</strong></td>
							</tr>
						</thead>
						<tr>
							<td class="col-md-3">Apply Date</td>
							<td class="col-md-9">{{ $data['other']['action_date'] }}</td>
						</tr>
						<tr>
							<td>Notes</td>
							<td>{{ $data['other']['notes'] }}</td>
						</tr>	
						<tr>
							<td>Attachment</td>
							<td>
								@if ($data['other']->AttAttachment)
									<?php
										$attachment = $data['other']->AttAttachment->filename.'.'.$data['other']->AttAttachment->ext;
									?>
									<a href="{{ route('lib.file.attendance.remark', array($attachment)) }}" target="_blank">Download</a>
								@else
									{{ '-' }}
								@endif									
							</td>
						</tr>
					</table>
				</div>
			</div>

		</div>
	</div>
@endif






<div class="well">
	<strong>Remarks Status: </strong>
	<br>1) WORKING - Time In and Time Out must not Empty.
	<br>2) MC - Applicable if apply the Leave (Medical Leave).
	<br>3) TRAINING - If have Training/Outstation.
	<br>4) OFF DAY - Applicable if the Date falls on Off Day.
	<br>5) AL-EL - Applicable if apply the Leave (Annual Leave).
	<br>6) PART TIMER - Applicable if Staff Position is Part Timer.
	<br>7) TBU (To Be Update) can use for cases like Blackout/No Internet/Wrong IC/Sitecode. Please follow-up if choose this.
	<br>8) INCOMPLETE - Set if Staff Fail to Punch the Attendance.
	<br>9) PUBLIC HOLIDAY - Applicable if Date falls on Public Holiday.
</div>




<script type="text/javascript">
$(document).ready(function(){

	// save leave
    $(document).on('click','#att-update', function(){
    	if ($('#remark').val() == "") {
    		alert('Please insert Notes.');
    	}
    	else {
	        var answer = confirm('Are you sure want to process this remark?');
	        if (answer == true) {
	            $('#form-list').submit(); 
	        }
	        else {
	            return false;
	        } 
	    }
    });	    

    // display popup modal
	$(document).on('click','#popup-modal',function() {  
		var type = $(this).attr('alt');
		str =  '';   		
		str += '<div class="modal-dialog">';
		str += '	<div class="modal-content">';
		str += '		<div class="modal-header">';
		str += '			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		str += '			<legend>' + $(this).attr('title') + '</legend>';
		str += '		</div>';
		str += '		<div class="modal-body">';
		str += '			<p><textarea name="remark" rows="4" cols="30" id="remark" placeholder="Insert Notes Here"></textarea></p><input type="hidden" name="type" value="' + type + '" />';
		str += '			<strong>Notes:</strong>';
		str += '			<br>1) Approve Remark - The Attendance Remark is valid.';
		str += '			<br>2) Cancel Remark - Used when Site Supervisor put wrong info/attachment on the Attendance Remark. Ask them to keyin again/No need to apply/Apply Leave.';
		str += '		</div>';
		str += '		<div class="modal-footer">';
		str += '			<div class="btn-group">';
		str += '				<button type="button" class="btn btn-primary" id="att-update">Submit</button>';
		str += '				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
		str += '			</div>';
		str += '		</div>';
		str += '	</div>';
		str += '</div>';
		$('#att-modal').html(str);                                                    
		$('#att-modal').modal();  
	}); 
});  
</script>








@stop