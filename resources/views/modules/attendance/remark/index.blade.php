@extends('layouts/backend')


@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
						</div>
					</div>
	                <div class="row">
						<div class="col-md-6">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
						</div>	               	
						<div class="col-md-3">
							{{ Form::select('remark_id', $remarks, $sessions['remark_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							{{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
						</div>

	               	</div>
					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}





<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No /<br>Position</th>
					<th class="col-md-4">Site Name /<br>Phase /<br>Attendance Date</th>
					<th class="col-md-2">Apply Date /<br>Req. Remark /<br>Status</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>
			@if (count($att_remarks) > 0)
				<?php $no = $att_remarks->firstItem() - 1;?>
				@foreach ($att_remarks as $i)
					<?php $no++; ?>
					<tr>
						<td>{{ $no }}</td>

						<td>
							@if (!empty($i->AttUserInfo))
								{{ $i->AttUserInfo->name }} 
							@else
								{{ '-' }}
							@endif
							/<br>
							{{ $i->icno }} /<br>
							@if (!empty($i->AttUserJob))
								{{ $i->AttUserJob->PositionName->name }}
							@else
								{{ '-' }}
							@endif
						</td>

						<td>
							@if (!empty($i->AttSiteInfo))
								{{ $i->AttSiteInfo->code }} {{ $i->AttSiteInfo->name }}
							@else
								{{ '-' }}
							@endif
							/<br>
							@if (!empty($i->AttUserJob))
								{{ $i->AttUserJob->PhaseName->name }} 
							@else
								{{ '-' }}
							@endif
							/<br>
							{{ $i->date }}
						</td>

						<td>
							{{ $i->action_date }} 
							/<br>
							@if (!empty($i->AttStatusName ))
								{{ $i->AttStatusName->name }}
							@else
								{{ '-' }}
							@endif
							/<br>
							@if (!empty($i->AttActiveStatus))
								{!! $i->AttActiveStatus->status_color !!}
							@else
								{{ '-' }}
							@endif
						</td>

						<td class="text-right">
							<a href="{{ route('mod.attendance.remark.view', array($i->id, $i->user_id, $i->icno, $i->sitecode)) }}" class="btn btn-primary btn-sm" title="View Remark"><i class="icon-magnifier-add"></i></a>
						</td>

					</tr>
				@endforeach
			@else
				<tr><td colspan="5">No record.</td></tr>
			@endif
		</table>

		<div class="well">
			<div class="paging text-center">
				{{ $att_remarks->render() }}
				<p>{{ 'Total: '.$att_remarks->total() }}</p>
			</div>
		</div>
	</div>
</div>


@stop