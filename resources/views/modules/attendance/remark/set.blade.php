@extends('layouts/backend')

@section('content')



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Attendance Info</strong></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="col-md-2">Date</td>
							<td class="col-md-10">
								@if ($dates)
									@foreach ($dates as $date)
										<i class="icon-check"></i>&nbsp;{{ $date }}
									@endforeach
								@endif								
							</td>
						</tr>
						<tr>
							<td>Total Day</td>
							<td>
								{{ $total }}
							</td>
						</tr>						
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Set Remark</legend>			

				<div class="form-group">
					@if ($errors->has('att_status'))
						<p class="col-lg-12 text-danger">{{ $errors->first('att_status') }}</p>
					@endif   	
					<div class="required"> 					
						<label class="col-lg-2 control-label">Remark Status</label>
					</div>
					<div class="col-lg-4">
						{{ Form::select('att_status', $status, $input['att_status'], array('class' => 'form-control', 'id' => 'selectStatus')) }}  
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('att_notes'))
						<p class="col-lg-12 text-danger">{{ $errors->first('att_notes') }}</p>
					@endif   	
					<div class="required"> 					
						<label class="col-lg-2 control-label">Notes</label>
					</div>
					<div class="col-lg-4">
						{{ Form::textarea('att_notes', $input['att_notes'], ['class' => 'form-control', 'size' => '30x3']) }}  
					</div>
				</div>

				<div class="form-group" id="i2">
					@if ($errors->has('att_file'))
						<p class="col-lg-12 text-danger">{{ $errors->first('att_file') }}</p>
					@endif  
					<div class="required"> 					
						<label class="col-lg-2 control-label" for="textArea">Attachment</label>
					</div>
					<div class="col-lg-4">
						<label>
							{{ Form::file('att_file') }}
						</label>
					</div>
				</div>
				<hr>

				<br>
				<strong>Remarks: </strong>	
				<br>1) Use this form if Same Remark is more than 1 date.
				<br>2) File attachment must be in following types jpeg/jpg/png and below 2MB. 
			</div>

			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-6">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }}
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}


@stop

