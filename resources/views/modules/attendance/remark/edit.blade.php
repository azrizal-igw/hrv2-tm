@extends('layouts/backend')


@section('content')





{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>
					<legend>Edit Remark</legend>
					<div class="form-group">
						@if ($errors->has('att_status'))
							<p class="col-lg-12 text-danger">{{ $errors->first('att_status') }}</p>
						@endif   	
						<div class="required"> 					
							<label class="col-lg-2 control-label">Remark Status</label>
						</div>
						<div class="col-lg-4">
							{{ Form::select('att_status', $remark_status, old('att_status'), array('class' => 'form-control', 'id' => 'selectStatus')) }}  
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('att_notes'))
							<p class="col-lg-12 text-danger">{{ $errors->first('att_notes') }}</p>
						@endif   	
						<div class="required"> 					
							<label class="col-lg-2 control-label">Notes</label>
						</div>
						<div class="col-lg-4">
							{{ Form::textarea('att_notes', old('att_notes'), ['class' => 'form-control', 'size' => '30x3']) }}  
						</div>
					</div>
				</fieldset>
			</div>
			{{ Form::hidden('id', $remark->id) }}
			{{ Form::hidden('date', $remark->date) }}

			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-12">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
					</div>
				</div>
			</div>

		</div>


	</div>
</div>
{{ Form::close() }}

<div class="well">
	<strong>Remarks Status: </strong>
	<br>1) WORKING - Time In and Time Out must not Empty.
	<br>2) MC - Applicable if apply the Leave (Medical Leave).
	<br>3) TRAINING - If have Training/Outstation.
	<br>4) OFF DAY - Applicable if the Date falls on Off Day.
	<br>5) AL-EL - Applicable if apply the Leave (Annual Leave).
	<br>6) PART TIMER - Applicable if Staff Position is Part Timer.
	<br>7) TBU (To Be Update) can use for cases like Blackout/No Internet/Wrong IC/Sitecode. Please follow-up if choose this.
	<br>8) INCOMPLETE - Set if Staff Fail to Punch the Attendance.
	<br>9) PUBLIC HOLIDAY - Applicable if Date falls on Public Holiday.
</div>


@stop