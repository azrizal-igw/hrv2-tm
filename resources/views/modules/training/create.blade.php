@extends('layouts/backend')

@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'form-data')) }}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Add Training</h4>
            </div>
            <div class="panel-body nopadding">


                <div class="row col-wrap">
                    <div class="col-sm-6 col">
                        <div class="row">
                            <div class="col-md-12">

                            	<div class="well">

	                            	<legend>Training</legend>
									<div class="form-group">
										@if ($errors->has('name'))
											<p class="col-lg-12 text-danger">{{ $errors->first('name') }}</p>
										@endif
										<div class="required">
											<label class="col-lg-4 control-label" for="textArea">Name</label>
										</div>
										<div class="col-lg-8">
											{{ Form::text('name', old('name'), array('class'=>'form-control')) }}
										</div>
									</div>

									<div class="form-group">
										@if ($errors->has('date_from'))
											<p class="col-lg-12 text-danger">{{ $errors->first('date_from') }}</p>
										@endif  					
										<div class="required"> 
											<label class="col-lg-4 control-label" for="select">Start Date</label>
										</div>
										<div class="col-lg-6">
											<div class="input-group date" id="pick_start_date">
												{{ Form::text('date_from', old('date_from'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'date1', 'readonly')) }}
												<span class="input-group-addon"><i class="icon-calendar"></i>
											</div>	
										</div>
									</div>

									<div class="form-group">
										@if ($errors->has('date_to'))
											<p class="col-lg-12 text-danger">{{ $errors->first('date_to') }}</p>
										@endif  					
										<div class="required"> 
											<label class="col-lg-4 control-label" for="select">End Date</label>
										</div>
										<div class="col-lg-6">
											<div class="input-group date" id="pick_end_date">
												{{ Form::text('date_to', old('date_to'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'date2', 'readonly')) }}
												<span class="input-group-addon"><i class="icon-calendar"></i>
											</div>	
										</div>
									</div>

									<div class="form-group">
										@if ($errors->has('location'))
											<p class="col-lg-12 text-danger">{{ $errors->first('location') }}</p>
										@endif
										<div class="required">
											<label class="col-lg-4 control-label" for="textArea">Location</label>
										</div>
										<div class="col-lg-8">
											{{ Form::text('location', old('location'), array('class'=>'form-control')) }}
										</div>
									</div>

									<div class="form-group">
										@if ($errors->has('description'))
											<p class="col-lg-12 text-danger">{{ $errors->first('description') }}</p>
										@endif
										<div class="required">
											<label class="col-lg-4 control-label">Description</label>
										</div>
										<div class="col-lg-8">
											{{ Form::textarea('description', old('description'), ['class' => 'form-control', 'size' => '30x3']) }}
										</div>
									</div>
                            	</div>

                            </div>
						</div>
					</div>

                    <div class="col-sm-6 col">
                        <div class="row">
                            <div class="col-md-12">
                            	<div class="well" id="table-staff" style="overflow-y: scroll; height:415px;"></div>
                            </div>
						</div>
					</div>

				</div>
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-md-6">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'btn_save']) }}
					</div>
					<div class="col-md-6 text-right">
						<a href="#" data-url="{{ route('mod.training.popup') }}" data-toggle="modal" id="list" class="btn btn-success" title="Add Site Supervisor"><i class="icon-plus"></i>&nbsp;Add</a>
            			<?php 
            				$route = route('mod.training.remove'); 
            			?>
						<button type="button" class="btn btn-danger" onclick="getModDeleteSelected('{{ $route }}')" title="Remove Selected"><i class="icon-close"></i>&nbsp;Delete</button>
					</div>					
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}





<script type="text/javascript">	
$(function () {
	getDatePicker('pick_start_date');
	getDatePicker('pick_end_date');  	
	getModDataAll("{{ route('mod.training.session') }}");    
});
</script>




@stop



