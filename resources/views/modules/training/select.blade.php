@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}



<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">   


               <div class="row">
                  <div class="col-md-6">
                     {{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
                  </div>                                     
               </div>

               <div class="row">
                  <div class="col-md-9">
                     {{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Sitecode', 'size' => 40)) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('position_id', $positions, $sessions['position_id'], array('class' => 'form-control')) }}
                  </div>                  
               </div>

               <br>               
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;
               <input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>          
         </div>

      </div>
   </div>            
</div>



<?php $no = 0; ?>

<div class="row">
	<div class="col-sm-12">

        <div class="panel panel-primary">
            <div class="panel-body nopadding">

				<table class="table table-striped table-condensed table-responsive">
					<thead>
						<tr>
							<th class="col-md-1">No</th>
							<th class="col-md-3">Name /<br>IC No.</th>
							<th class="col-md-3">Position /<br>Region</th>
							<th class="col-md-3">Site Name /<br> Phase</th>
               				<th class="col-md-1">Photo</th>
							<th class="col-md-1 text-right">
								<input type="checkbox" id="chk_all" title="Check All">
							</th>
						</tr>
					</thead>

					<tbody>
						@if (!empty($users))
							@foreach ($users as $i)
								<?php $no++; ?>
								<tr>
									<td>{{ $no }}</td>
									<td>
										{{ $i->name }} /<br>{{ $i->icno }}
									</td>

									<td>
										@if (!empty($i->UserLatestJob))
											{{ $i->UserLatestJob->PositionName->name }}
										@else
											{{ '-' }}
										@endif
										/<br>
										@if (!empty($i->UserLatestJob))
											{{ $i->UserLatestJob->RegionName->name }}
										@else
											{{ '-' }}
										@endif                              
									</td>

									<td>
										@if (!empty($i->SiteNameInfo))
											{{ $i->SiteNameInfo->code }} {{ $i->SiteNameInfo->name }}
										@else
											{{ '-' }}
										@endif
										/<br>
										@if (!empty($i->UserLatestJob))
											{{ $i->UserLatestJob->PhaseName->name }}
										@else
											{{ '-' }}
										@endif 
									</td>
									<td>
										@if ($i->UserPhotoActive)
											@if (in_array($i->UserPhotoActive->PhotoLatestStatus->status, array(2)))
												<?php
												$photo = $i->UserPhotoActive->photo_thumb.'.'.$i->UserPhotoActive->ext;
												?>
												<img src="{{ route('lib.file.user.thumb', array($photo)) }}" class="img-responsive" title="View Photo" style="width: 50%;">
											@else
												<img src="{{ route('lib.image', array('default.png')) }}" class="img-responsive" title="No Photo" style="width: 50%;">
											@endif
										@else
											<img src="{{ route('lib.image', array('default.png')) }}" class="img-responsive" title="No Photo" style="width: 50%;">
										@endif										
									</td>
									<td class="text-right">
										{{ Form::checkbox('chk_id[]', $i->id, null, ['class' => 'chk_id']) }}
									</td>
								</tr>
							@endforeach
						@else
						<tr><td colspan="5">No record</td></tr>
						@endif				
					</tbody>	
				</table>
			</div>

			@if ($no > 0)
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
	               			{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('mod.training.create')]) }}
						</div>						
						<div class="col-md-6 text-right">
	               			<input type="submit" value="Select" class="btn btn-danger" name="select"/>
						</div>
					</div>
				</div>
			@else
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
	               			{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('mod.training.create')]) }}
						</div>
					</div>
				</div>
			@endif

		</div>
	</div>
</div>


{{ Form::close() }}



@stop



