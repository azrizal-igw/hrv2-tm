<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">

      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'form-popup')) }}

      <div class="modal-content">

         <div class="modal-body" style="overflow-y: scroll; height:410px;">

            <div class="row">
               <div class="col-md-12">
                  <div class="well">

                     <div class="row">
                        <div class="col-md-12">   


                           <div class="row">
                              <div class="col-md-12">
                                 {{ Form::select('site_id', $sites, null, array('class' => 'form-control')) }}
                              </div>                 
                           </div>

                           <div class="row">
                              <div class="col-md-6">
                                 {{ Form::select('region_id', $regions, null, array('class' => 'form-control')) }}
                              </div>
                              <div class="col-md-6">
                                 {{ Form::select('phase_id', $phases, null, array('class' => 'form-control')) }}
                              </div>                 
                           </div>

                           <div class="row">
                              <div class="col-md-12">
                                 {{ Form::text('keyword', null, array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Sitecode', 'size' => 40)) }}
                              </div>
                           </div>

                           <br>               
                           {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;
                           <input type="submit" value="Reset" class="btn btn-default" name="reset" />
                        </div>          
                     </div>

                  </div>
               </div>            
            </div>

            <div id="table-staff"></div>

         </div>
         <div class="panel-footer">
            <div class="row">
               <div class="col-md-12 text-right">
                  <button type="button" class="btn btn-success" onclick="" title="Select"><i class="icon-check"></i>&nbsp;Select</button>
               </div>
            </div>
         </div>
      </div>


      <script type="text/javascript">  
         $(function () {
            var formData = $('form#form-popup').serialize();
            $.ajax({
               type: "POST",
               url: "{{ route('mod.training.popup.user') }}",
               data: formData,
               dataType: "json",              
               cache: false,
               success: function(response, status, jqXHR) {
                  console.log(response['list']);
                  $('#myModal #table-staff').html(response['list']);
               },
               error: function(e){
                  console.log(e);
               },        
            }); 
         });
      </script>


      {{ Form::close() }}

   </div>
</div>




