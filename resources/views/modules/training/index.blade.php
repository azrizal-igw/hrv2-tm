@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-12">{{ Form::text('search', null, array('class'=>'form-control', 'placeholder' => 'Search Code/Name')) }}
						</div>                  
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}




<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-condensed table-responsive table-bordered">
			<thead>
				<tr class="active">
					<th>No</th>
					<th>Name /<br>Code</th>
					<th>Category /<br>Location</th>
					<th>Date Time /<br>Trainer</th>
					<th>Day /<br>Slot</th>
					<th>Status</th>
					<th class="text-right">Actions</th>
				</tr>
			</thead>
			<tbody>

			@if (count($data) > 0)
				<?php $no = $data->firstItem() - 1;?>
				@foreach ($data as $i)
					<?php $no++;?>
					<tr>
						<td>{{ $no }}</td>
						<td>
							{{ $i->name }} /<br>{{ $i->code }}
						</td>
						<td>
							@if (!empty($i->CategoryName))
								{{ $i->CategoryName->name }}
							@else
								{{ '-' }}
							@endif
							/<br>
							{{ $i->location }}
						</td>
						<td>
							{{ $i->datetime }} /<br>{{ $i->trainer }}
						</td>
						<td>
							{{ $i->day }} /<br>{{ $i->slot }}
						</td>
						<td>
							{!! $i->availability !!}
						</td>
						<td class="text-right">
							<a href="#" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
							<a href="#" data-url="{{ route('mod.training.popup.code', array($i->id)) }}" data-toggle="modal" id="list" class="btn btn-success btn-sm" title="Scan QR Code"><i class="fa fa-qrcode"></i></a>
						</td>
					</tr>
				@endforeach
			@endif
			</tbody>
		</table>

		<div class="well">
			<div class="paging text-center">
			{{ $data->render() }}
			<p>{{ 'Total: '.$data->total() }}</p>
			</div>
		</div>		
	</div>
</div>



@stop


