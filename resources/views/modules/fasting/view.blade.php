@extends('layouts/backend')


@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Fasting Info</strong></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="col-md-3">Year</td>
							<td class="col-md-9">{{ $detail->year }}</td>
						</tr>
						<tr>
							<td>Start Date</td>
							<td>{{ $detail->start_date }}</td>
						</tr>
						<tr>
							<td>End Date</td>
							<td>{{ $detail->end_date }}</td>
						</tr>
						<tr>
							<td>Start Time</td>
							<td>{{ $detail->start_time }}</td>
						</tr>
						<tr>
							<td>End Time</td>
							<td>{{ $detail->end_time }}</td>
						</tr>
						<tr>
							<td>State</td>
							<td>
								@if (!empty($detail->ListStates))
									@foreach ($detail->ListStates as $state)
										[{{ $state->StateName->name }}]
									@endforeach
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>						
						<tr>
							<td>Action Date</td>
							<td>{{ $detail->action_date }}</td>
						</tr>
						<tr>
							<td>Action By</td>
							<td>{{ $detail->ActionByUser->name }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="panel-footer">
		<div class="row">
			<div class="col-md-12"> 
				{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger btn_click', 'id' => 'cancel', 'title' => 'Cancel')) }}
			</div>
		</div>
	</div>	
</div>
{{ Form::close() }}



@stop