@extends('layouts/backend')


@section('content')



<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css.old/bootstrap-datetimepicker.css') }}" rel="stylesheet">




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>

					<div class="form-group">
						<div class="col-md-3 required">          
							{{ Form::label('selectDateTime', 'Year') }}  						
							@if ($errors->has('year'))
								<p class="text-danger">{{ $errors->first('year') }}</p>
							@endif       
							<div>
								{{ Form::select('year', array('' => "[Select Year]") + array_combine(range(2016, date('Y') + 1), range(2016, date('Y') + 1)), old('year'), array('class' => 'form-control')) }}
							</div>
						</div>	
					</div>	


					<div class="form-group">
						<div class="col-md-3 required">          
							{{ Form::label('selectDateTime', ' Start Date') }}  
							@if ($errors->has('start_date'))
								<p class="text-danger">{{ $errors->first('start_date') }}</p>
							@endif  						
							<div class="input-group date" id="pick_start_date">
								{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>	
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3 required">          
							{{ Form::label('selectDateTime', 'End Date') }}  
							@if ($errors->has('end_date'))
								<p class="text-danger">{{ $errors->first('end_date') }}</p>
							@endif  						
							<div class="input-group date" id="pick_end_date">
								{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>	
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3 required">          
							{{ Form::label('selectDateTime', 'Start Time') }}  
							@if ($errors->has('start_time'))
								<p class="text-danger">{{ $errors->first('start_time') }}</p>
							@endif  						
							<div class="input-group date" id="pick_start_time">
								{{ Form::text('start_time', old('start_time'), array('class' => 'form-control', 'data-date-format' => 'HH:mm:ss')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>	
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3 required">          
							{{ Form::label('selectDateTime', 'End Time') }}  
							@if ($errors->has('end_time'))
								<p class="text-danger">{{ $errors->first('end_time') }}</p>
							@endif  						
							<div class="input-group date" id="pick_end_time">
								{{ Form::text('end_time', old('end_time'), array('class' => 'form-control', 'data-date-format' => 'HH:mm:ss')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i>
							</div>	
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('state'))
							<p class="col-lg-12 text-danger">{{ $errors->first('state') }}</p>
						@endif               
						<div class="col-md-12">
							{{ Form::label(null, 'Select State') }}<br>
							<input type="checkbox" id="state-chk-all">&nbsp;Check All
							<div class="checkbox">
								@if (!empty($states))
									@foreach ($states as $i)
										<label class="checkbox-inline">
										{{ Form::checkbox('state[]', $i['code'], null, ['class' => 'check-state']) }}&nbsp;{{ $i['name'] }}
										</label><br>                                          
									@endforeach
								@endif                     
							</div>
						</div>
					</div>

				</fieldset>
			</div>

			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-12">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
					</div>
				</div>
			</div>

		</div>

	</div>
</div>
{{ Form::close() }} 

<script type="text/javascript">
$(function () {
	getDatePicker('pick_start_date');
	getDatePicker('pick_end_date');     
	getTimePicker('pick_start_time');     
	getTimePicker('pick_end_time');     
});


$('#state-chk-all').click(function(){
  if($(this).is(':checked')) {
     $('.check-state').prop('checked', true);                            
  } 
  else {
     $('.check-state').prop('checked', false);                  
  }       
});
</script>

@stop

