@extends('layouts/backend')


@section('content')





{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">                            
               <div class="row">                 
                  <div class="col-md-2">{{ Form::selectYear('year', '2016', date('Y') + 1, $sessions['year'], ['class' => 'form-control']) }}
                  </div>
               </div>

               <br>               
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;
               <input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>          
         </div>

      </div>
   </div>            
</div>
{{ Form::close() }}





<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Year</th>
					<th class="text-left">Start Date</th>
					<th class="text-left">End Date</th>
					<th class="text-left">Start Time</th>
					<th class="text-left">End Time</th>
					<th class="text-left">States</th>
					<th class="text-right">Actions</th>
				</tr>
			</thead>

			@if (count($fast) > 0)
				<?php $no = 0; ?>
				@foreach ($fast as $i)
					<?php $no++; ?>
					<tbody>
						<tr>
							<td>{{ $no }}</td>
							<td>{{ $i->year }}</td>
							<td class="text-left">{{ $i->start_date }}</td>
							<td class="text-left">{{ $i->end_date }}</td>
							<td class="text-left">{{ $i->start_time }}</td>
							<td class="text-left">{{ $i->end_time }}</td>
							<td>							
								@if (!empty($i->ListStates))
									@foreach ($i->ListStates as $state)
										{{ $state->StateName->name }}<br>
									@endforeach
								@else
									{{ '-' }}
								@endif
							</td>
							<td class="text-right">
								<a href="{{ route('mod.fasting.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
							</td>
						</tr>
					</tbody>
				@endforeach
			@else
				<tbody>
					<tr>
						<td colspan="8">No record</td>
					</tr>					
				</tbody>
			@endif
		</table>


		<div class="well">
			<div class="paging text-center">
				<p>{{ 'Total: '.count($fast) }}</p>
			</div>				
		</div>

	</div>
</div>

@stop


