@extends('layouts/backend')

@section('content')

{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Email Notification Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Name</td>
						<td class="col-md-9">XXXX</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>
							XXXXX
						</td>
					</tr>
				</table>
			</div>
		</div>

	</div>
</div>
{{ Form::close() }}




@stop


