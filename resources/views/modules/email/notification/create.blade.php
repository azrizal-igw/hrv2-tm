@extends('layouts/backend')

@section('content')

{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<fieldset>

					<div class="form-group">
						<div class="col-md-3 required">
							{{ Form::label('Name') }}
							@if ($errors->has('name'))
								<p class="text-danger">{{ $errors->first('name') }}</p>
							@endif
							{{ Form::text('name', old('name'), array('class'=>'form-control', 'size' => 40)) }}
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3 required">
							{{ Form::label('Email') }}
							@if ($errors->has('email'))
								<p class="text-danger">{{ $errors->first('email') }}</p>
							@endif
							{{ Form::text('email', old('email'), array('class'=>'form-control', 'size' => 40)) }}
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3 required">
							{{ Form::label('Type') }}
							@if ($errors->has('type_id'))
								<p class="text-danger">{{ $errors->first('type_id') }}</p>
							@endif
							{{ Form::select('type_id', $types, null, array('class' => 'form-control')) }}
						</div>
					</div>

				</fieldset>
			</div>

			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-12">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
					</div>
				</div>
			</div>

		</div>

	</div>
</div>
{{ Form::close() }} 

@stop


