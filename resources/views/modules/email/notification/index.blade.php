@extends('layouts/backend')

@section('content')

{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">

               <div class="row">
                  <div class="col-md-12">{{ Form::text('search', null, array('class'=>'form-control', 'placeholder' => 'Search Name/Email')) }}
                  </div>                  
               </div>

               <br>
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>
         </div>

      </div>
   </div>
</div>
{{ Form::close() }}

<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-condensed table-responsive table-bordered">
			<thead>
				<tr class="active">
					<th>No</th>
					<th>Name</th>
					<th>Email</th>
					<th>Type</th>
					<th>Status</th>
					<th class="text-right">Actions</th>
				</tr>
			</thead>
			@if (count($emails) > 0)
				<?php $no = $emails->firstItem() - 1;?>
				@foreach ($emails as $i)
					<?php $no++;?>
					<tr>
						<td>{{ $no }}</td>
						<td>
							{{ $i->name }}
						</td>
						<td>{{ $i->email }}</td>
						<td>
							{{ $i->TypeName->name }}
						</td>
						<td>{{ $i->status }}</td>
						<td class="text-right">
							<a href="{{ route('mod.email.notification.edit', array($i->id)) }}" class="btn btn-primary btn-sm" title="Edit"><i class="icon-note"></i></a>
							<a href="#" class="btn btn-danger btn-sm" title="Delete"><i class="icon-trash"></i></a>
						</td>
					</tr>
				@endforeach
			@else
				<tr><td colspan="6">No record</td></tr>
			@endif			
		</table>

		<div class="well">
			<div class="paging text-center">
				{{ $emails->render() }}
				<p>{{ 'Total: '.$emails->total() }}</p>
			</div>
		</div>

	</div>
</div>

@stop


