@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Announcement Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Apply Date</td>
						<td class="col-md-9">{{ $detail->apply_date }}</td>
					</tr>
					<tr>
						<td>Created By</td>
						<td>
							@if (!empty($detail->CreatedBy))
								{{ $detail->CreatedBy->name }}
							@else
								{{ '-' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Message</td>
						<td>{{ $detail->message }}</td>
					</tr>
					<tr>
						<td>Type</td>
						<td>{!! $detail->type_name !!}</td>
					</tr>
					<tr>
						<td>Order</td>
						<td>{{ $detail->order }}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>{!! $detail->availability !!}</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="panel-footer">
		{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger btn_click', 'id' => 'cancel', 'title' => 'Cancel')) }}
	</div>

</div>
{{ Form::close() }}





@stop


