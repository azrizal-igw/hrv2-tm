@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Announcement</legend>

				<div class="form-group">
					<div class="col-md-2 required">          
						{{ Form::label('selectType', 'Type') }}   
						@if ($errors->has('type_id'))
							<p class="text-danger">{{ $errors->first('type_id') }}</p>
						@endif      
						{{ Form::select('type_id', array(1 => 'Site Supervisor', 2 => 'Admin'), old('type_id'), array('class' => 'form-control', 'id' => 'selectType')) }}                   
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 required">          
						{{ Form::label('selectOrder', 'Order') }}   
						@if ($errors->has('order'))
							<p class="text-danger">{{ $errors->first('order') }}</p>
						@endif      
						{{ Form::text('order', old('order'), array('class'=>'form-control', 'id' => 'selectOrder', 'size' => 40)) }}                     
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4 required">          
						{{ Form::label('selectMessage', 'Message') }}   
						@if ($errors->has('message'))
							<p class="text-danger">{{ $errors->first('message') }}</p>
						@endif      
						{{ Form::text('message', old('message'), array('class'=>'form-control', 'id' => 'selectMessage', 'size' => 40)) }}                     
					</div>
				</div>
			</div>

			<div class="panel-footer">
				{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }}
			</div>
		</div>


	</div>
</div>
{{ Form::close() }}



@stop


