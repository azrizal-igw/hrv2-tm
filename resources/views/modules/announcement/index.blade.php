@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
	<div class="col-md-12">
		<div class="well">
			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-3">
							{{ Form::select('type_id', $types, $sessions['type_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
						</div>                  						
						<div class="col-md-6">{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Message')) }}
						</div>
					</div> 

					<br>    
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}
				</div>          
			</div>
		</div>
	</div>            
</div>
{{ Form::close() }}





<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-condensed table-responsive table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-8">Message / Group</th>
					<th class="text-left col-md-1">Status</th>                 
					<th class="text-left col-md-1">Order</th>                 
					<th class="text-right col-md-1">Actions</th>                
				</tr>
			</thead>

			<tbody>
				@if (count($anno) > 0)
					<?php $no = $anno->firstItem() - 1;?>				
					@foreach ($anno as $i)
						<?php $no++; ?>
						<tr>
							<td>
								{{ $no }}
							</td>

							<td>
								{{ $i->message }} /<br>{!! $i->type_name !!}
							</td>
							<td>
								{!! $i->availability !!}
							</td>

							<td class="text-left">
								{{ Form::text('order', $i->order, array('class'=>'form-control', 'size' => 1, 'vertical-align' => 'center')) }}  
							</td>

							<td class="text-right">
								<a href="{{ route('mod.announcement.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View Detail"><i class="icon-magnifier-add"></i></a>
							</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="5">No record</td>
					</tr>
				@endif
			</tbody>
		</table>

		<div class="well">
			<div class="paging text-center">  
				{{ $anno->render() }}      
				<p>{{ 'Total: '.$anno->total() }}</p>
			</div>
		</div>


	</div>
</div>


@stop


