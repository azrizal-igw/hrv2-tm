@extends('layouts/backend')

@section('content')



<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Details</strong></td>
						</tr>
					</thead>

					<tr>
						<td class="col-md-3">Name</td>
						<td class="col-md-9">{{ $info['name'] }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $info['icno'] }}</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>{{ $info['position'] }}</td>
					</tr>
					<tr>
						<td>Site Name</td>
						<td>{{ $info['sitecode'] }} {{ $info['sitename'] }}</td>
					</tr>
					<tr>
						<td>Contract</td>
						<td>{{ Carbon\Carbon::parse($info['date_from'])->format('d M Y') }} - {{ Carbon\Carbon::parse($info['date_to'])->format('d M Y') }}</td>
					</tr>
					<tr>
						<td>Reporting Officer</td>
						<td>{{ $info['rm_name'] }}</td>
					</tr>										
				</table>
			</div>
		</div>



		<?php $exist = 0; ?>
		@if (!empty($types))
			@foreach ($types as $i)

				@if ($i->id == 1)
					<?php $bg = 'red'; ?>
				@elseif ($i->id == 2)
					<?php $bg = 'grey'; ?>
				@else
					<?php $bg = 'inverse'; ?>
				@endif

				@if (count($letters) > 0)
					<?php $exist = 1; $num = 0; ?>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped table-condensed table-responsive table-hover">
								<thead>
									<tr><td colspan="8" align="center"><h6><strong>{{ $i->name }}</strong></h6></td></tr>
									<tr class="bg-{{ $bg }}">
										<th class="col-md-1">No</th>
										<th class="col-md-2">Letter Date</th>
										<th class="col-md-1">Reminder</th>
										<th class="col-md-3">Upload By</th>
										<th class="col-md-2 text-left">Apply Date</th>
										<th class="col-md-1 text-center">Status</th>
										<th class="col-md-1 text-right"><i class="icon-picture" title="Attachment"></i></th>
										<th class="col-md-1 text-right">Actions</th>
									</tr>
								</thead>

								@foreach ($letters as $l)
									@if ($l->letter_type_id == $i->id)
										<?php $num++; ?>

										<tbody>
											<tr>
												<td>{{ $num }}</td>
												<td>
													{{ $l->letter_date }}
												</td>
												<td>
													{{ CommonHelper::ordinal($l->reminder_no) }}
												</td>
												<td>
													@if (!empty($l->LetterActionBy))
														{{ $l->LetterActionBy->name }}
													@else
														{{ '-' }}
													@endif
												</td>
												
												<td class="text-left">
													{{ $l->action_date }}
												</td>
												
												<td class="text-center">
													{!! $l->availability !!}
												</td>

												<td class="text-right">
													@if (!empty($l->LetterAttachment))
														<?php
															$attachment = $l->LetterAttachment->filename.'.'.$l->LetterAttachment->ext;
														?> 													
														<a href="{{ route('lib.file.letter', array($attachment)) }}" target="_blank" style="text-decoration: none; color: #666666;" title="View Letter"><i class="icon-paper-clip"></i></a>
													@else
														{{ '-' }}
													@endif
												</td>

												<td class="text-right">
													<a href="{{ route('mod.letter.user.view', array($l->id, $info['uid'], $info['sitecode'])) }}" class="btn btn-success btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
												</td>
											</tr>
										</tbody>
									@endif
								@endforeach	

								@if ($num == 0)
									<tr>
										<td colspan="8">No record</td>
									</tr>
								@else
									<tr>
										<td colspan="8" class="text-center">Total: <strong>{{ $num }}</strong></td>
									</tr>
								@endif

							</table>
						</div>
					</div>
				@endif

			@endforeach
		@endif

		@if ($exist == 0) 
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped table-condensed table-responsive table-hover">
						<thead>
							<tr class="bg-inverse">
								<td>No Letter</td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		@endif

	</div>
</div>




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>New Letter</legend>

				<div class="form-group">
					@if ($errors->has('letter_type_id'))
						<p class="col-lg-12 text-danger">{{ $errors->first('letter_type_id') }}</p>
					@endif   	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Letter Type</label>
					</div>
					<div class="col-lg-3">
						{{ Form::select('letter_type_id', $letter_types, old('letter_type_id'), array('class' => 'form-control')) }} 
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('reminder_no'))
						<p class="col-lg-12 text-danger">{{ $errors->first('reminder_no') }}</p>
					@endif   	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Reminder No</label>
					</div>
					<div class="col-lg-3">
						{{ Form::select('reminder_no', $no, old('reminder_no'), array('class' => 'form-control')) }} 
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('letter_date'))
						<p class="col-lg-12 text-danger">{{ $errors->first('letter_date') }}</p>
					@endif  					
					<div class="required"> 
						<label class="col-lg-3 control-label" for="select">Letter Date</label>
					</div>
					<div class="col-lg-3">
						<div class="input-group date" id="pick_release_date">
							{{ Form::text('letter_date', old('letter_date'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'date1', 'readonly')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>	
					</div>
				</div>


				<div class="form-group">
					@if ($errors->has('notes'))
						<p class="col-lg-12 text-danger">{{ $errors->first('notes') }}</p>
					@endif   	
					<div class="required"> 					
						<label class="col-lg-3 control-label">Notes</label>
					</div>
					<div class="col-lg-4">
						{{ Form::textarea('notes', old('notes'), ['class' => 'form-control', 'size' => '30x3']) }}  
					</div>
				</div>

				<div class="form-group">
					@if ($errors->has('letter_file'))
						<p class="col-lg-12 text-danger">{{ $errors->first('letter_file') }}</p>
					@endif  
					<div class="required"> 					
						<label class="col-lg-3 control-label" for="textArea">Attachment</label>
					</div>
					<div class="col-lg-9">
						<label>
							{{ Form::file('letter_file') }}
						</label>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-3 control-label" for="textArea">Email Notification</label>
					<label class="col-lg-9">
						{{ Form::checkbox('email', null, true) }}
					</label>
				</div>


				<br>
				<strong>Remarks: </strong>	
				<br>1) Use this form upload the Letter for Site Supervisor
				<br>2) Please describe at notes about reason and action need to be taken.
				<br>3) Uncheck 'Email Notification' if Site Supervisor already receive the letter.
				<br>4) Any 'Active' letter will visible to Site Supervisor.
				<br>5) Site Supervisor/HR/RM/DRM will get the notification once letter is created.
				<br>6) File attachment must pdf/doc/docx and below 2mb.


			</div>
			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-12">
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }} 
						{{ Form::hidden('report_to', $info['rm_id'])}}
						{{ Form::hidden('region_id', $info['region_id'])}}
					</div>
				</div>
			</div>				
		</div>

	</div>
</div>
{{ Form::close() }}




<script type="text/javascript">
$(function () {
	getDatePicker('pick_release_date');
});
</script>



@stop


