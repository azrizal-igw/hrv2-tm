@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="col-md-3">Name</td>
							<td class="col-md-9">{{ $letter->LetterUserDetail->name }}</td>
						</tr>
						<tr>
							<td>IC No.</td>
							<td>{{ $letter->LetterUserDetail->icno }}</td>
						</tr>
						<tr>
							<td>Site Name</td>
							<td>
								@if ($letter->LetterSiteName)
									{{ $letter->LetterSiteName->code }} {{ $letter->LetterSiteName->name }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
						<tr>
							<td>Reporting Officer</td>
							<td>
								@if ($letter->LetterReportTo)
									{{ $letter->LetterReportTo->name }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Letter Info</strong></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="col-md-3">Letter Type</td>
							<td class="col-md-9"><strong>{{ $letter->LetterTypeName->name }}</strong></td>
						</tr>
						<tr>
							<td>Letter Date</td>
							<td>
								{{ $letter->letter_date }}
							</td>
						</tr>
						<tr>
							<td>Reminder No</td>
							<td>
								{{ CommonHelper::ordinal($letter->reminder_no) }}
							</td>
						</tr>
						<tr>
							<td>Action Date</td>
							<td>
								{{ $letter->action_date }}
							</td>
						</tr>
						<tr>
							<td>Action By</td>
							<td>
								{{ $letter->LetterActionBy->name }}
							</td>
						</tr>
						<tr>
							<td>Notes</td>
							<td>
								{{ $letter->notes }}
							</td>
						</tr>
						<tr>
							<td>Status</td>
							<td>
								{!! $letter->availability !!}
							</td>
						</tr>
						<tr>
							<td>Updated By</td>
							<td>
								@if (!empty($letter->LetterUpdatedBy))
									{{ $letter->LetterUpdatedBy->name }}
								@else
									{{ '-' }}
								@endif
							</td>
						</tr>
						<tr>
							<td>Attachment</td>
							<td>
								@if (!empty($letter->LetterAttachment))
									<?php
$attachment = $letter->LetterAttachment->filename . '.' . $letter->LetterAttachment->ext;
?>
									<a href="{{ route('lib.file.letter', array($attachment)) }}" target="_blank" style="text-decoration: none; color: #666666;" title="View Letter"><i class="icon-paper-clip"></i></a>
								@else
									{{ '-' }}
								@endif

							</td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>
	</div>

	@if ($letter->status == 1)
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-6">
					{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'cancel']) }}
				</div>
<!-- 				<div class="col-md-6 text-right">
					{{ Form::button('<i class="icon-paper-plane"></i>&nbsp;Resend Email',['type' => 'submit', 'class' => 'btn btn-primary btn_click', 'id' => 'resend']) }}
				</div> -->
			</div>
		</div>
	@endif

</div>
{{ Form::close() }}



@stop


