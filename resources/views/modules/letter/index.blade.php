@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
						</div>
					</div>


					<div class="row">
						<div class="col-md-3">
							{{ Form::select('position_id', $positions, $sessions['position_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('letter_type_id', $letter_types, $sessions['letter_type_id'], array('class' => 'form-control')) }}
						</div>

						<div class="col-md-3">
							{{ Form::select('reminder_no', $reminder_lists, $sessions['reminder_no'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
						</div>
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}




<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No /<br>Position</th>
					<th class="col-md-4">Region /<br>Site Name /<br>Reminder No</th>
					<th class="col-md-2">Apply Date /<br>Letter Type /<br>Status</th>
					<th class="col-md-1"></th>
				</tr>
			</thead>
			@if (count($letters) > 0)
				<?php $no = $letters->firstItem() - 1;?>
				@foreach ($letters as $i)
					<?php $no++;?>
					<tbody>
						<tr>
							<td>{{ $no }}</td>

							<td>
								@if (!empty($i->LetterUserDetail))
									{{ $i->LetterUserDetail->name }} /<br>
									{{ $i->LetterUserDetail->icno }} /<br>
									@if ($i->LetterUserJob)
										{{ $i->LetterUserJob->PositionName->name }}
									@else
										{{ '-' }}
									@endif
								@else
									{{ '-' }}
								@endif
							</td>

							<td>
								@if (!empty($i->LetterUserJob))
									{{ $i->LetterUserJob->RegionName->name }}
								@else
									{{ '-' }}
								@endif
								/<br>
								@if (!empty($i->LetterSiteName))
									{{ $i->LetterSiteName->code.' '.$i->LetterSiteName->name }}
								@else
									{{ '-' }}
								@endif
								/<br>
								{{ CommonHelper::ordinal($i->reminder_no) }}

							</td>

							<td>
								{{ $i->action_date }} /<br>
								@if (!empty($i->LetterTypeName))
									{{ $i->LetterTypeName->name }}
								@else
									{{ '-' }}
								@endif
								/<br>
								{!! $i->availability !!}
							</td>

							<td class="text-right">
								<a href="{{ route('mod.letter.user.view', array($i->id, $i->user_id, $i->sitecode)) }}" class="btn btn-primary btn-sm" title="View Letter"><i class="icon-magnifier-add"></i></a>
							</td>
						</tr>
					</tbody>
				@endforeach
			@else
				<tbody>
					<tr>
						<td colspan="5">No record</td>
					</tr>
				</tbody>
			@endif
		</table>




		<div class="well">
			<div class="paging text-center">
				{{ $letters->render() }}
				<p>{{ 'Total: '.$letters->total() }}</p>
			</div>
		</div>

	</div>
</div>














@stop



