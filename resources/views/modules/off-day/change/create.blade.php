@extends('layouts/backend')

@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css.old/bootstrap-datetimepicker.css') }}" rel="stylesheet">




<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed table-responsive table-hover">
                            <thead>
                                <tr class="bg-primary">
                                    <td colspan="2"><strong>Staff Info</strong></td>
                                </tr>
                            </thead>

                            <tr>
                                <td class="col-md-3">Name</td>
                                <td class="col-md-9">{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td>IC No.</td>
                                <td>{{ $user->icno }}</td>
                            </tr>
                            <tr>
                                <td>Position</td>
                                <td>{{ $user->UserLatestJob->PositionName->name }}</td>
                            </tr>
                            <tr>
                                <td>Site Name</td>
                                <td>{{ $user->SiteNameInfo->code }} {{ $user->SiteNameInfo->name }}</td>
                            </tr>
                            <tr>
                                <td>Contract</td>
                                <td>{{ $user->UserLatestContract->date_from }} to {{ $user->UserLatestContract->date_to }}</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Off Date Info</legend>
				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('selectStartDate', 'Start Date') }}
						@if ($errors->has('start_date'))
							<p class="text-danger">{{ $errors->first('start_date') }}</p>
						@endif
						<div class="input-group date" id="pick_start_date">
							{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'selectStartDate', 'readonly')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('selectEndDate', 'End Date') }}
						@if ($errors->has('end_date'))
							<p class="text-danger">{{ $errors->first('end_date') }}</p>
						@endif
						<div class="input-group date" id="pick_end_date">
							{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'id' => 'selectStartDate', 'readonly')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 required">
						{{ Form::label('Reason') }}
						@if ($errors->has('reason'))
							<p class="text-danger">{{ $errors->first('reason') }}</p>
						@endif
						{{ Form::textarea('reason', old('reason'), ['class' => 'form-control', 'size' => '30x3']) }}
					</div>
				</div>

                <div class="form-group">
                    <div class="col-md-6 required">
                        {{ Form::label('Attachment') }}
                        @if ($errors->has('file'))
                            <p class="text-danger">{{ $errors->first('file') }}</p>
                        @endif
                        {{ Form::file('file') }}
                    </div>
                </div>

			</div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'button', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }}
                    </div>
                </div>
            </div>


		</div>
	</div>
</div>




{{ Form::close() }}





<script type="text/javascript">
$(function () {
	getDatePicker('pick_start_date');
	getDatePicker('pick_end_date');
});
</script>




@stop


