@extends('layouts/backend')

@section('content')




@if (!empty($change))
	<?php $found = 1; ?>
@else
	<?php $found = 0; ?>
@endif




<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">




                        <table class="table table-condensed table-responsive table-hover">
                            <thead>
                                <tr class="bg-primary">
                                    <td colspan="2"><strong>Staff Info</strong></td>
                                </tr>
                            </thead>

                            <tr>
                                <td class="col-md-3">Name</td>
                                <td class="col-md-9">{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td>IC No.</td>
                                <td>{{ $user->icno }}</td>
                            </tr>
                            <tr>
                                <td>Position</td>
                                <td>{{ $user->UserLatestJob->PositionName->name }}</td>
                            </tr>
                            <tr>
                                <td>Site Name</td>
                                <td>{{ $user->SiteNameInfo->code }} {{ $user->SiteNameInfo->name }}</td>
                            </tr>
                            <tr>
                                <td>Contract</td>
                                <td>{{ $user->UserLatestContract->date_from }} to {{ $user->UserLatestContract->date_to }}</td>
                            </tr>
                        </table>




                        @if ($found == 1)
							<table class="table table-striped table-condensed table-responsive table-hover">
	                            <thead>
	                                <tr class="bg-primary">
	                                    <td colspan="12"><strong>Change Info</strong></td>
	                                </tr>
	                            </thead>							
								<thead>
									<tr>
										<th class="col-md-1">No</th>
										<th class="col-md-2">Start Date /<br>End Date</th>
										<th class="col-md-5">Reason /<br>Status</th>
										<th class="col-md-3">Apply Date /<br>Created</th>
										<th class="col-md-1 text-right"><i class="icon-picture" title="Attachment"></i></th>
									</tr>
									<tr>
										<td>1</td>
										<td>2018-04-01 /<br>2018-04-30</td>
										<td>Pada 14/04/2018 (Sabtu) Saya bertugas melakukan persiapan hari bersama komuniti dan 15/04/2018 (Ahad) menjayakan program hari bersama komuniti yang bertempat di PI1M Felda Pasoh 4. Bercuti ganti pada : 20 April 2018 (Jumaat) dan 21 April 2018 (Sabtu) /<br>Active</td>
										<td>2018-04-20 11:12:11 /<br>Nik Mohd Azman Bin Nik Ahmad</td>
										<td class="text-right"><i class="icon-paper-clip"></i></td>
									</tr>
								</thead>
							</table>    
						@endif


                    </div>
                </div>

            </div>


            @if ($found == 1)
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
							{{ Form::button('<i class="icon-plus"></i>&nbsp;Renew',['type' => 'submit', 'class' => 'btn btn-primary', 'id' => 'btn_save']) }}
						</div>					
						<div class="col-md-6 text-right">
							{{ Form::button('<i class="icon-calendar"></i>&nbsp;Calendar',['type' => 'submit', 'class' => 'btn btn-success', 'id' => 'btn_save']) }}
						</div>
					</div>
				</div>
			@else
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
							{{ Form::button('<i class="icon-plus"></i>&nbsp;Add',['type' => 'button', 'class' => 'btn btn-success']) }}
						</div>					
					</div>
				</div>
			@endif

        </div>
    </div>
</div>






@if ($found == 1)






	{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
	<div class="row">
		<div class="col-lg-12">

			<div class="panel panel-default">
				<div class="panel-body">
					<legend>Set Off Date</legend>


					<div class="form-group">
						@if ($errors->has('day'))
							<p class="col-lg-12 text-danger">{{ $errors->first('day') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label" for="select">Day</label>
						</div>
						<div class="col-lg-2">
							{{ Form::select('day', $days, null, array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('time_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('time_id') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label" for="select">Time</label>
						</div>
						<div class="col-lg-2">
							{{ Form::select('time_id', $time_types, null, array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="form-group">
						@if ($errors->has('type_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('type_id') }}</p>
						@endif
						<div class="required">
							<label class="col-lg-3 control-label">Type</label>
						</div>
						<div class="col-lg-4">
							{{ Form::select('type_id', $off_types, null, array('class' => 'form-control')) }}
						</div>
					</div>

				</div>

				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
                    		{{ Form::hidden('id', $change->id) }}
							{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }}
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	{{ Form::close() }}
	<div class="well">
		<strong>Notes:</strong>
		<br>1) Setting the Off Date will effect on every Week. Depending on duration of Start and End date.
	</div>
@endif



<script type="text/javascript">
function getButtonClick() {
	alert(1);
}
</script>


@stop


