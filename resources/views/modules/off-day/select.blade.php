@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-body nopadding">		
		<legend>Year & Group</legend>
		<fieldset>



		<div class="form-group">
			@if ($errors->has('off_year'))
				<p class="col-lg-12 text-danger">{{ $errors->first('off_year') }}</p>
			@endif       
			<div class="col-lg-2">
				{{ Form::select('off_year', array('' => "[Year]") + array_combine(range(2016, date('Y') + 1), range(2016, date('Y') + 1)), old('off_year'), array('class' => 'form-control')) }}
			</div>			
		</div>	


		<div class="form-group">
			<div class="col-xs-12">
				@if ($errors->has('off_group'))
					<p class="text-danger">{{ $errors->first('off_group') }}</p>
				@endif  
				@if (count($off_groups) > 0)
					@foreach ($off_groups as $i)
						<div class="radio">
							<label><input id="" name="off_group" value="{{ $i->id }}" type="radio">{{ $i->name }}</label>
						</div>
					@endforeach
				@endif

			</div>
		</div>
		</fieldset>
	</div> 
	
	<div class="panel-footer">
		{{ Form::button('Select&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger']) }}   		
	</div>

</div>
{{ Form::close() }}  





@stop