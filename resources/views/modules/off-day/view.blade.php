@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal')) }}
<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row col-wrap">	

			<div class="col-sm-6 col">
				<div class="row">
					<div class="col-md-12">

						<table class="table table-condensed table-responsive">
							<thead>
								<tr class="bg-success">
									<td colspan="2"><i class="icon-event"></i>&nbsp;<strong>Off/Rest Day Info</strong></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="col-md-3">Year</td>
									<td class="col-md-9">{{ $off['year'] }}</td>
								</tr>
								<tr>
									<td>Date</td>
									<td>{{ $off['off_date'] }}</td>
								</tr>
								<tr>
									<td>Off Type</td>
									<td>{{ $off['off_type_name']['name'] }}</td>
								</tr>
							</tbody>					
						</table>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col">
				<div class="row">
					<div class="col-md-12">

						<table class="table table-condensed table-responsive">
							<thead>
								<tr class="bg-warning">
									<td colspan="2"><i class="icon-list"></i>&nbsp;<strong>States</strong></td>
								</tr>
							</thead>
							@if (!empty($off['list_states']))
								@foreach ($off['list_states'] as $i)
									<tr>
										<td colspan="2">{{ $i['leave_off_state_name']['name'] }}</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="2">No record</td>
								</tr>
							@endif							
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="panel-footer">  
		<div class="row">
			<div class="col-md-6"> 
				{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel',['type' => 'button', 'class' => 'btn btn-danger', 'id' => 'btn_cancel']) }}
			</div>				
			<div class="col-md-6 text-right"> 
				{{ Form::button('<i class="icon-note"></i>&nbsp;Edit',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_url', 'data-href' => route('mod.off-day.edit', array($off['id']))]) }} 
			</div>		
		</div>
	</div>

</div>
{{ Form::close() }}


@stop


