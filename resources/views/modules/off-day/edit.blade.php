@extends('layouts/backend')

@section('content')



<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">



{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed table-responsive table-hover">
                            <thead>
                                <tr class="bg-primary">
                                    <td colspan="2"><strong>Off/Rest Day</strong></td>
                                </tr>
                            </thead>
                            <tr>
                                <td class="col-md-2">Date</td>
                                <td class="col-md-10">{{ $off->off_date }}</td>
                            </tr>
                            <tr>
                            	<td>Type</td>
                            	<td>{{ $off->OffTypeName->name }}</td>
                            </tr>
                        </table>
                    </div>
                </div>

				<fieldset> 
					<div class="form-group">
						@if ($errors->has('state_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('state_id') }}</p>
						@endif 					
						<div class="col-md-12">
							{{ Form::label(null, 'Select State') }}<br>
							<input type="checkbox" id="state-chk-all">&nbsp;Check All
							<div class="checkbox">
								@if (!empty($states))
									@foreach ($states as $i)
										@if (in_array($i['code'], $off_states))
											<?php $checked = 'checked'; ?>
										@else
											<?php $checked = ''; ?>
										@endif	
										<label class="checkbox-inline">
											<input type="checkbox" {{ $checked }} name="state_id[]" value="{{ $i['code'] }}" class="check-state">&nbsp;{{ $i['name'] }}
										</label><br>
									@endforeach
								@endif							
							</div>
						</div>
					</div>
				</fieldset>
			</div>

			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-6"> 
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'update']) }} 
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}



<script type="text/javascript">
$(function () {
    $('#public_date').datetimepicker({
        pickTime: false
    });              
});

$('#state-chk-all').click(function(){
    if($(this).is(':checked')) {
        $('.check-state').prop('checked', true);                            
    } 
    else {
        $('.check-state').prop('checked', false);                  
    }       
}); 
</script>


@stop

