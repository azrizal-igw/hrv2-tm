@extends('layouts/backend')

@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
   <div class="col-lg-12">

      <div class="panel panel-default">
         <div class="panel-body">
            <legend>Year {{ $off_year }}</legend>
            <fieldset>

               <div class="form-group">
                  @if ($errors->has('off_type'))
                  <p class="col-lg-12 text-danger">{{ $errors->first('off_type') }}</p>
                  @endif               
                  <div class="required"> 
                     <label class="col-lg-3 control-label">Leave Type</label>
                  </div>
                  <div class="col-lg-3">
                     {{ Form::select('off_type', $off_types, old('off_type'), array('class' => 'form-control')) }}
                  </div>
               </div>     

               <div class="form-group">
                  @if ($errors->has('off_date_start'))
                  <p class="col-lg-12 text-danger">{{ $errors->first('off_date_start') }}</p>
                  @endif               
                  <div class="required"> 
                     <label class="col-lg-3 control-label" for="select">Start Date</label>
                  </div>
                  <div class="col-lg-3">
                     <div class="input-group date" id="select_off_date_start">
                        {{ Form::text('off_date_start', old('off_date_start'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY')) }}
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                     </div>   
                  </div>
               </div>

               <div class="form-group">
                  @if ($errors->has('off_date_end'))
                  <p class="col-lg-12 text-danger">{{ $errors->first('off_date_end') }}</p>
                  @endif               
                  <div class="required"> 
                     <label class="col-lg-3 control-label" for="select">End Date</label>
                  </div>
                  <div class="col-lg-3">
                     <div class="input-group date" id="select_off_date_end">
                        {{ Form::text('off_date_end', old('off_date_end'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY')) }}
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                     </div>   
                  </div>
               </div>

               <div class="form-group">
                  @if ($errors->has('off_state'))
                  <p class="col-lg-12 text-danger">{{ $errors->first('off_state') }}</p>
                  @endif               
                  <div class="col-md-12">
                     {{ Form::label(null, 'Select State') }}<br>
                     <input type="checkbox" id="state-chk-all">&nbsp;Check All
                     <div class="checkbox">
                        @if (!empty($states))
                           @foreach ($states as $i)
                           <label class="checkbox-inline">
                              {{ Form::checkbox('off_state[]', $i['state_id'], null, ['class' => 'check-state']) }}&nbsp;{{ $i['state_name']['name'] }}
                           </label><br>                                          
                           @endforeach
                        @endif                     
                     </div>
                  </div>
               </div>

    
            </fieldset>
         </div>

         <div class="panel-footer">  
            <div class="row">

               <div class="col-md-6">
                  {{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('mod.off-day.select')]) }}
               </div>

               <div class="col-md-6 text-right"> 
                  {{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
                  {{ Form::hidden('off_group', $off_group) }}
                  {{ Form::hidden('off_year', $off_year) }}
               </div>
            </div>
         </div>

      </div>
   </div>
</div>
{{ Form::close() }}




<script type="text/javascript">
   $(function () {
      getDatePicker('select_off_date_start');
      getDatePicker('select_off_date_end');                 
   });

   $('#state-chk-all').click(function(){
      if($(this).is(':checked')) {
         $('.check-state').prop('checked', true);                            
      } 
      else {
         $('.check-state').prop('checked', false);                  
      }       
   }); 
</script>




@stop

