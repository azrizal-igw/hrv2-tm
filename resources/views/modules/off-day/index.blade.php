@extends('layouts/backend')

@section('content')

<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">

               <div class="row">
                  <div class="col-md-3">
                     {{ Form::select('year', $years, $sessions['year'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('type_id', $off_types, $sessions['type_id'], array('class' => 'form-control')) }}
                  </div>

                  <div class="col-md-3">
                     {{ Form::select('state_id', $states, $sessions['state_id'], array('class' => 'form-control')) }}
                  </div> 

                  <div class="col-md-3">
                     <div class="input-group date" id="pick_date">
                        {{ Form::text('date', $sessions['date'], array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD', 'placeholder' => 'Date')) }}
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                     </div>
                  </div>

               </div>    

               <br>
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>
         </div>

      </div>
   </div>            
</div>
{{ Form::close() }}



<div class="row">
   <div class="col-sm-12">
      <table class="table table-condensed table-responsive table-bordered table-striped">
         <thead>
            <tr>
               <th class="col-md-1">No</th>
               <th class="col-md-6">Date</th>
               <th class="col-md-2">Type</th>
               <th class="col-md-2">State</th>
               <th class="col-md-1 text-right">Actions</th>
            </tr>
         </thead>
         @if (count($off) > 0)
            <?php $no = $off->firstItem() - 1;?>
            @foreach ($off as $i)
               <?php 
                  $no++;                       
               ?>
                  <tr>
                     <td>{{ $no }}</td>
                     <td>
                        {{ $i->off_date }}                   
                     </td>
                     <td>
                        @if (!empty($i->OffTypeName))
                           {{ $i->OffTypeName->name }}
                        @else
                           {{ '-' }}
                        @endif   
                     </td>
                     <td>
                        @if (!empty($i->ListStates))
                           @foreach ($i->ListStates as $s)
                              {{ $s->LeaveOffStateName->name }}<br>
                           @endforeach
                        @else
                           {{ '-' }}
                        @endif
                     </td>
                     <td class="text-right">
                        <a href="{{ route('mod.off-day.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View"><i class="icon-magnifier-add"></i></a>
                     </td>
                  </tr>
            @endforeach
         @else
            <tr>
               <td colspan="5">No reocrd.</td>
            </tr>
         @endif
      </table>

      <div class="well">            
         <div class="paging text-center">  
            {{ $off->render() }}      
            <p>{{ 'Total: '.$off->total() }}</p>
         </div>
      </div>

   </div>
</div>



<script type="text/javascript">
$(function () {
   getDatePicker('pick_date');
});
</script>


@stop