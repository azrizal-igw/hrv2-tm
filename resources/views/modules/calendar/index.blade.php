@extends('layouts/backend')

@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
   <div class="col-md-12">
      <div class="well">
         <div class="row">
            <div class="col-md-12">
				<div class="row">                 
					<div class="col-md-2">
						{{ Form::selectYear('year', 2016, date('Y') + 1, $sessions['year'], ['class' => 'form-control']) }}
					</div>
				</div> 
               <br>    
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}
            </div>          
         </div>

      </div>
   </div>            
</div>

@if (empty($status))
	<div class="well text-left">
		{{ Form::button('Activate', array('class' => 'btn btn-primary', 'type' => 'submit', 'name' => 'btn_active', 'value' => 'activate', 'title' => 'Activate the Calendar')) }}
		<br><br>Notes: Before Activate the Calendar please make sure all Off Day and Public Holiday already recorded.
	</div>
@endif
{{ Form::close() }}


@foreach ($data['groups'] as $i)
<?php
	$group_id = $i['id'];
	if ($i['id'] == 1) {
		$dates = $data['dates1'];
		$od_m = array($data['od_m1'], $data['types'][0]['name'], $data['types'][0]['color']);
		$od_am = array($data['od_am1'], $data['types'][1]['name'], $data['types'][1]['color']);
		$odr_m = array($data['odr_m1'], $data['types'][2]['name'], $data['types'][2]['color']);
		$odr_am = array($data['odr_am1'], $data['types'][3]['name'], $data['types'][3]['color']);
		$odr = array($data['odr1'], $data['types'][4]['name'], $data['types'][4]['color']);
		$oe = array($data['oe1'], $data['types'][5]['name'], $data['types'][5]['color']);
		$og = array($data['og1'], $data['types'][6]['name'], $data['types'][6]['color']);
		$public = array($data['public1'], 'Cuti Umum', 'red');
	}
	else {
		$dates = $data['dates2'];
		$od_m = array($data['od_m2'], $data['types'][0]['name'], $data['types'][0]['color']);
		$od_am = array($data['od_am2'], $data['types'][1]['name'], $data['types'][1]['color']);
		$odr_m = array($data['odr_m2'], $data['types'][2]['name'], $data['types'][2]['color']);
		$odr_am = array($data['odr_am2'], $data['types'][3]['name'], $data['types'][3]['color']);
		$odr = array($data['odr2'], $data['types'][4]['name'], $data['types'][4]['color']);
		$oe = array($data['oe2'], $data['types'][5]['name'], $data['types'][5]['color']);
		$og = array($data['og2'], $data['types'][6]['name'], $data['types'][6]['color']);
		$public = array($data['public2'], 'Cuti Umum', 'red');
	}
	$sname = array();
	if (count($i['leave_off_states']) > 0) {
		foreach ($i['leave_off_states'] as $state) {
			$sname[] = $state['state_name']['name'];
		}
	}
	$states = implode('/', $sname);
?>
<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-bordered">
			<thead>
				<tr class="bg-primary">
					<td colspan="2">{{ strtoupper($states) }}</td>
				</tr>
			</thead>
			@for ($d = 1; $d <= 12; $d++)
				<tr>
					<td class="col-md-2">{{ strtoupper(\Carbon\Carbon::createFromFormat('!m', $d)->format('F')) }}</td>
					<td class="col-md-10">

						@foreach ($data['months'] as $k => $v)							
							@if ($k == $d)

								@foreach ($v[0] as $m)
									<?php
										$day = \Carbon\Carbon::parse($m)->format('l');
										$no = \Carbon\Carbon::parse($m)->format('j');
										$color = null;
										$title = null;
									?>
									@if (in_array($m, $dates))

										@if (in_array($m, $public[0]))
											<?php $color = 'black'; $title = ''; ?>
										@endif

										@if (in_array($m, $od_m[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $od_m[2]; $title = $od_m[1]; ?>
											@endif
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $od_am[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $od_am[2]; $title = $od_am[1]; ?>
											@endif
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $odr_m[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $odr_m[2]; $title = $odr_m[1]; ?>
											@endif											
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $odr_am[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $odr_am[2]; $title = $odr_am[1]; ?>
											@endif											
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $oe[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $oe[2]; $title = $oe[1]; ?>
											@endif
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $og[0]))
											@if (empty($color) && empty($title)) 
												<?php $color = $og[2]; $title = $og[1]; ?>
											@endif
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@elseif (in_array($m, $odr[0]))
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop grey-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="Manager & Assistant Manager RL WL" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>

										@else
											<i class="glyphicon glyphicon-stop white-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="" data-day="{{ $day }}" data-no="{{ $no }}"></i>
										@endif
										
									@else

										@if (in_array($m, $public[0]))
											<?php $color = $public[2]; $title = $public[1]; ?>
											<a href="{{ route('mod.calendar.view', array($group_id, $m)) }}"><i class="glyphicon glyphicon-stop {{ $color }}-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="{{ $title }}" data-day="{{ $day }}" data-no="{{ $no }}"></i></a>	
										@else
											<i class="glyphicon glyphicon-stop white-big" data-toggle="tooltip" data-attribute="{{ $m }}" data-type="" data-day="{{ $day }}" data-no="{{ $no }}"></i>
										@endif
									@endif


								@endforeach

							@endif

						@endforeach

					</td>
				</tr>
			@endfor

		</table>
	</div>
</div>
@endforeach

<div class="well">
	<strong>Legends: </strong>
	@if (!empty($data['types']))
		@foreach ($data['types'] as $t)
			<br><i class="glyphicon glyphicon-stop {{ $t['color'] }}"></i>&nbsp;{{ $t['name'] }}
		@endforeach
	@endif
    <br><i class="glyphicon glyphicon-stop red"></i>&nbsp;Cuti Umum  
</div>

@stop


