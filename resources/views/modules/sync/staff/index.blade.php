@extends('layouts/backend')


@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">
					@if ($errors->has('type_id'))
						<p class="text-danger">{{ $errors->first('type_id') }}</p>
					@endif
					<div class="row">
						<div class="col-md-2">
							{{ Form::select('type_id', $lists, old('type_id'), array('class' => 'form-control')) }}
						</div>
						<div class="col-md-2">{{ Form::text('record', old('record'), array('class'=>'form-control', 'placeholder' => 'Total Record')) }}</div>
						<div class="col-md-2">{{ Form::text('skip', old('skip'), array('class'=>'form-control', 'placeholder' => 'Skip')) }}</div>
					</div>
					<br>
					{{ Form::button('Start Sync', array('class' => 'btn btn-primary btn_click', 'type' => 'submit', 'name' => 'btn-search', 'id' => 'process')) }}
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}


<div class="well">
	<strong>Remarks:</strong>
	<br>1) Record from ERP might big and take time to load, so use total record and skip to sync it by batch.
	<br>2) Ex: 1st -> Total: 500, Skip 0 (From 0 to 500) | 2nd Total: 500, Skip 500 (From 500 to 1000).
	<br>3) Use small amount of total to fasten the process.
</div>


@if (Session::has('text'))
<div class="well">
	<strong>Summary</strong><br>
	{!! Session::get('text') !!}
</div>
@endif



@stop
