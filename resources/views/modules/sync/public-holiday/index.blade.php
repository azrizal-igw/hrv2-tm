@extends('layouts/backend')


@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">                            
                <div class="row">    
					@if ($errors->has('year'))
						<p class="text-danger col-md-12">{{ $errors->first('year') }}</p>
					@endif    
					<div class="col-md-2">{{ Form::selectYear('year', '2016', date('Y') + 1, date('Y'), ['class' => 'form-control']) }}
					</div>
				</div>
				<br>               
				{{ Form::button('Start Sync', array('class' => 'btn btn-primary btn_click', 'type' => 'submit', 'name' => 'btn-search', 'id' => 'process')) }}
            	</div>          
         </div>

      </div>
   </div>            
</div>
{{ Form::close() }}



@if (Session::has('text'))
	<div class="well">
		<strong>Summary</strong><br>
		{!! Session::get('text') !!}
	</div>
@endif



@stop

