@extends('layouts/backend')

@section('content')





{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">                            
               <div class="row">                 
                  <div class="col-md-2">{{ Form::selectYear('year', '2016', date('Y') + 1, $sessions['year'], ['class' => 'form-control']) }}
                  </div>
               </div>

               <br>               
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}
            </div>          
         </div>

      </div>
   </div>            
</div>
{{ Form::close() }}






<div class="row">
   <div class="col-sm-12">
      <table class="table table-condensed table-responsive table-bordered table-striped">
         <thead>
            <tr>
               <th>Date</th>
               <th>Description</th>
               <th>Applicable To</th>
               <th>Actions</th>
            </tr>
         </thead>
         @foreach ($months as $m)
            <tr class="">
               <td colspan="5" class="bg-success"><strong>{{ $m['name'] }}</strong></td>
            </tr>
            @if (count($ph) > 0)
               @foreach ($ph as $i)
                  @if (\Carbon\Carbon::parse($i->date)->format('m') == $m['no'])
                     <tr>
                        <td>{{ $i->date }}</td>
                        <td>
                           {{ $i->desc }}
                        </td>
                        <td align="left">
                           @if (count($i->LeavePublicState) > 0)
                              @if (count($i->LeavePublicState) == 16)
                                 {{ 'All' }}
                              @else
                                 @if ($i->LeavePublicState)
                                    @foreach ($i->LeavePublicState as $s)
                                       {{ $s->StateName->name }}<br>
                                    @endforeach
                                 @else
                                    {{ '-' }}
                                 @endif
                              @endif
                           @endif
                        </td>
                        <td class="text-right">
                           <a href="{{ route('mod.public-holiday.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View"><i class="icon-magnifier-add"></i></a>
                        </td>
                     </tr>
                  @endif
               @endforeach
               <tr><td colspan="4">&nbsp;</td></tr>
            @else
               <tr><td colspan="4">No record</td></tr>
            @endif
         @endforeach
      </table>

   </div>
</div>







@stop