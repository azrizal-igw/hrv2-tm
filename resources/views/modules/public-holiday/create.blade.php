@extends('layouts/backend')

@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css.old/bootstrap-datetimepicker.css') }}" rel="stylesheet">




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">

				<fieldset>
					<legend>Public Holiday</legend>



					
					<div class="form-group">
						@if ($errors->has('date'))
							<p class="col-lg-12 text-danger">{{ $errors->first('date') }}</p>
						@endif  					
						<div class="required"> 
							<label class="col-lg-3 control-label" for="select">Date</label>
						</div>
						<div class="col-lg-3">
							<div class="input-group date" id="public_date">
								{{ Form::text('date', old('date'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'readonly')) }}
								<span class="input-group-addon"><i class="icon-calendar"></i></span>
							</div>	
						</div>
					</div>


					<div class="form-group">
						@if ($errors->has('desc'))
							<p class="col-lg-12 text-danger">{{ $errors->first('desc') }}</p>
						@endif  					
						<div class="required"> 
							<label class="col-lg-3 control-label">Description</label>
						</div>
						<div class="col-lg-4">
							{{ Form::text('desc', old('desc'), array('class'=>'form-control')) }}
						</div>
					</div>		



					<div class="form-group">
						@if ($errors->has('state_id'))
							<p class="col-lg-12 text-danger">{{ $errors->first('state_id') }}</p>
						@endif 					
						<div class="col-md-12">
							{{ Form::label(null, 'Select State') }}<br>
							<input type="checkbox" id="state-chk-all">&nbsp;Check All
							<div class="checkbox">
								@if (!empty($states))
									@foreach ($states as $i)
										<label class="checkbox-inline">
											<input type="checkbox" name="state_id[]" value="{{ $i['code']  }}" class="check-state">&nbsp;{{ $i['name'] }}
										</label><br>														
									@endforeach
								@endif							
							</div>
						</div>
					</div>
				</fieldset>

			</div>
			<div class="panel-footer">  
				<div class="row">
					<div class="col-md-6"> 
						{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }} 
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}



<script type="text/javascript">
$(function () {
    $('#public_date').datetimepicker({
        pickTime: false
    });              
});

$('#state-chk-all').click(function(){
    if($(this).is(':checked')) {
        $('.check-state').prop('checked', true);                            
    } 
    else {
        $('.check-state').prop('checked', false);                  
    }       
}); 
</script>

@stop


