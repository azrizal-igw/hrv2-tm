@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal')) }}
<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row col-wrap">	

			<div class="col-sm-6 col">
				<div class="row">
					<div class="col-md-12">

						<table class="table table-condensed table-responsive">
							<thead>
								<tr class="bg-success">
									<td colspan="2"><i class="icon-event"></i>&nbsp;<strong>Public Info</strong></td>
								</tr>
							</thead>
							<tr>
								<td class="col-md-3">Year</td>
								<td class="col-md-9">{{ $public['year'] }}</td>
							</tr>
							<tr>
								<td>Date</td>
								<td>{{ $public['date'] }}</td>
							</tr>
							<tr>
								<td>Description</td>
								<td>{{ $public['desc'] }}</td>
							</tr>														
						</table>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col">
				<div class="row">
					<div class="col-md-12">

						<table class="table table-condensed table-responsive">
							<thead>
								<tr class="bg-warning">
									<td colspan="2"><i class="icon-list"></i>&nbsp;<strong>States</strong></td>
								</tr>
							</thead>
							@if (!empty($public['leave_public_state']))
								@foreach ($public['leave_public_state'] as $i)
									<tr>
										<td colspan="2">{{ $i['state_name']['name'] }}</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="2">No record</td>
								</tr>
							@endif
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="panel-footer">  
		<div class="row">
			<div class="col-md-6"> 
				{{ Form::button('<i class="icon-close"></i>&nbsp;Cancel',['type' => 'button', 'class' => 'btn btn-danger', 'id' => 'btn_cancel', 'data-href' => route('mod.public-holiday.view', array($public['id']))]) }}
			</div>				
			<div class="col-md-6 text-right"> 
				{{ Form::button('<i class="icon-note"></i>&nbsp;Edit',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_url', 'data-href' => route('mod.public-holiday.edit', array($public['id']))]) }} 
			</div>
		
		</div>
	</div>

</div>
{{ Form::close() }}


@stop


