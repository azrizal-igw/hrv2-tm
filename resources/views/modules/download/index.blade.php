@extends('layouts/backend')


@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-body nopadding">
		<fieldset>
		<div class="form-group">
			<div class="col-lg-3">
				{{ Form::select('type_id', array(1 => 'Masterlist Sites'), null, array('class' => 'form-control')) }}
			</div>
		</div>

		</fieldset>
	</div>

	<div class="panel-footer">
		{{ Form::button('Download',['type' => 'submit', 'class' => 'btn btn-primary']) }}
	</div>

</div>
{{ Form::close() }}




@stop
