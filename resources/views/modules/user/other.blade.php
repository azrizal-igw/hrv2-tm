@extends('layouts/backend')



@section('content')




<div class="row">
   <div class="col-md-12">
      <div class="well">

         {{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}  
         <div class="row">
            <div class="col-md-12">

               <div class="row">                 
                  <div class="col-md-6">
                     {{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID', 'size' => 40)) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('group_id', $thegroups, $sessions['group_id'], array('class' => 'form-control')) }}
                  </div>   
                  <div class="col-md-3">
                     {{ Form::select('position_id', $positions, $sessions['position_id'], array('class' => 'form-control')) }}
                  </div>                   
               </div> 

               <br>
               <input type="submit" value="Search" class="btn btn-warning" name="btn-search" />&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>
         </div>
         {{ Form::close() }}

      </div>
   </div>            
</div>



<div class="row">
	<div class="col-sm-12">

		<table class="table table-striped table-condensed table-responsive table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No.</th>
					<th class="col-md-3">Staff ID /<br>Position</th>
					<th class="col-md-3">Group /<br>Status</th>
					<th class="col-md-1 text-right">Actions</th>
				</tr>
			</thead>
			@if (count($others) > 0)
				<?php $no = $others->firstItem() - 1;?>
				@foreach ($others as $i)
					<?php 
						$no++;                       
					?>
					<tr>
						<td>{{ $no }}</td>
						<td>
							{{ $i->name }} 
							/<br>{{ $i->icno }}
						</td>
						<td>
							{{ $i->staff_id }} 
							/<br>
							@if ($i->UserCurrentJob2)
								@if ($i->UserCurrentJob2->PositionName)
									{{ $i->UserCurrentJob2->PositionName->name }}
								@else
									{{ '-' }}
								@endif
							@else
								{{ '-' }}
							@endif
						</td>
						<td>
							@if ($i->GroupName)
								{{ $i->GroupName->name }} 
							@else
								{{ '-' }}
							@endif
							/<br>{{ $i->StatusName->name }}
						</td>
						<td class="text-right">
							<div class="btn-group">
							<button type="button" class="btn btn-sm btn-primary view" title="View Details" onclick="window.location='{{ route("mod.user.view", array($i->id, $i->api_token)) }}'"><i class="icon-user"></i></button>
							<button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="caret"></span></button>
							<ul class="dropdown-menu pull-right">
							<li><a href="{{ route('mod.user.password', array($i->id, $i->api_token)) }}"><i class="icon-lock"></i>&nbsp;Change Password</a></li>
							</ul>
							</div> 							
						</td>
					</tr>
				@endforeach
			@endif
		</table>


		<div class="well">            
			<div class="paging text-center">  
	            {{ $others->render() }}      
	            <p>{{ 'Total: '.$others->total() }}</p>
			</div>
		</div>


	</div>
</div>




@stop


