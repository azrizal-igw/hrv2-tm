@extends('layouts/backend')

@section('content')



<script type="text/javascript">
$(document).ready(function(){

	$('#detail a').click(function (e) {
		e.preventDefault();
	  
		var url = $(this).attr("data-url");
	  	var href = this.hash;
	  	var pane = $(this);
		
		// ajax load from data-url
		$(href).load(url,function(result){      
		    pane.tab('show');
		});
	});

	// load initial tab
	$('#profile').load($('.active a').attr("data-url"), function (result) {
		$('.active a').tab('show');
	});

})
</script>




<div class="row">
	<div class="col-sm-12">
		<div class="well">

			<div class="panel-body">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" id="detail">
					<li class="active"><a data-url="{{ route('mod.user.view.personal', array($detail->id, $detail->api_token)) }}" href="#profile"><i class="icon-user"></i>&nbsp;Profile</a></li>
					<li class=""><a data-url="{{ route('mod.user.view.job', array($detail->id, $detail->api_token)) }}" href="#job"><i class="icon-briefcase"></i>&nbsp;Job</a></li>
					
					@if ($detail->group_id == 3)						
						<li class=""><a data-url="{{ route('mod.user.view.contract', array($detail->id, $detail->api_token)) }}" href="#contract"><i class="icon-note"></i>&nbsp;Contract</a></li>
					@endif

					<li class=""><a data-url="{{ route('mod.user.view.family', array($detail->id, $detail->api_token)) }}" href="#family"><i class="icon-people"></i>&nbsp;Family</a></li>
					<li class=""><a data-url="{{ route('mod.user.view.education', array($detail->id, $detail->api_token)) }}" href="#education"><i class="icon-graduation"></i>&nbsp;Education</a></li>
					<li class=""><a data-url="{{ route('mod.user.view.emergency', array($detail->id, $detail->api_token)) }}" href="#emergency"><i class="icon-phone"></i>&nbsp;Emergency Contact</a></li>
					<li class=""><a data-url="{{ route('mod.user.view.photo', array($detail->id, $detail->api_token)) }}" href="#photo"><i class="icon-picture"></i>&nbsp;Photo</a></li>  									          
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="profile"></div>
					<div class="tab-pane" id="job"></div>
					<div class="tab-pane" id="contract"></div>
					<div class="tab-pane" id="family"></div>
					<div class="tab-pane" id="education"></div>
					<div class="tab-pane" id="emergency"></div>
					<div class="tab-pane" id="photo"></div>					
				</div>

			</div>                 


		</div>
	</div>
</div>







@stop