@extends('layouts/backend')

@section('content')



<div class="panel panel-primary">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-2">Name</td>
						<td class="col-md-10">{{ $user->name }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $user->icno }}</td>
					</tr>
					<tr>
						<td>Position</td>
						<td>{{ $user->UserCurrentJob2->PositionName->name }}</td>
					</tr>
					<tr>
						<td>Site Name</td>
						<td>{{ $user->sitecode }} {{ $user->SiteNameInfo->name }}</td>
					</tr>
				</table>
			</div>
		</div>

	</div>
</div>



<div class="row">
   <div class="col-md-12">
      <div class="well">

      	<strong>Notes:</strong>
      	<br>1) If have data offline, please sync it first before continuing reset the device ID. Otherwise record from previous device could not sync later on.

      </div>
   </div>            
</div>



<div class="well">
	Current Device ID: 
	@if (!empty($user->imei))
		<?php $status = 1; ?>
		{{ $user->imei }}
	@else
		<?php $status = 0; ?>
		{{ 'Empty' }}
	@endif
</div>


@if ($status == 1)
{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}             
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">                                          
				{{ Form::button('Reset', array('class' => 'btn btn-primary btn_click', 'type' => 'submit', 'name' => 'btn-search', 'id' => 'reset')) }}
            	</div>          
         </div>

      </div>
   </div>            
</div>
{{ Form::close() }}
@endif




@stop


