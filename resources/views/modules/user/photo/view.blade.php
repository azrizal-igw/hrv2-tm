@extends('layouts/backend')


@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12"> 

                <a href="{{ route('lib.file.user', array($photo->photo.'.'.$photo->ext)) }}" target="_blank" title="Click to Enlarge"><img src="{{ route('lib.file.user.thumb', array($photo->photo_thumb.'.'.$photo->ext)) }}" class="img-thumbnail img-responsive"></a>

      </div>
    </div>
  </div>




  @if ($photo->PhotoLatestStatus->status == 1)
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-6"> 
          {{ Form::button('<i class="icon-check"></i>&nbsp;Approve', array('class' => 'btn btn-success', 'id' => 'popup-modal', 'title' => 'Approve Remark', 'alt' => 2)) }}
        </div>
        <div class="col-md-6 text-right">
          {{ Form::button('<i class="icon-close"></i>&nbsp;Reject', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Approve Remark', 'alt' => 3)) }}
        </div>
      </div>
    </div>
  @elseif ($photo->PhotoLatestStatus->status == 2)
    <div class="panel-footer">
      <div class="row">
        <div class="col-md-6"> 
          {{ Form::button('<i class="icon-close"></i>&nbsp;Cancel', array('class' => 'btn btn-danger', 'id' => 'popup-modal', 'title' => 'Cancel & Rejected', 'alt' => 4)) }}
        </div>
      </div>
    </div>
  @endif
  <div class="modal fade" id="photo-modal"></div> 
</div>





<div class="row">
  <div class="col-md-12">
    <?php $no = 0; ?>               
    <table class="table table-striped table-condensed table-responsive table-hover">
      <thead>
        <tr class="bg-inverse">
          <th class="col-md-1">No</th>
          <th class="col-md-2">Action Date</th>
          <th class="col-md-4">Action By</th>
          <th class="col-md-3">Remarks</th>
          <th class="col-md-2">Status</th>
        </tr>
      </thead>
      @if (count($history) > 0)
        <?php $no = 0; ?>
        @foreach ($history as $i) 
          <?php 
            $no++; 
          ?>
          <tr>
            <td>{{ $no }}</td>
            <td>{{ $i->action_date }}</td>
            <td>
              @if ($i->PhotoActionBy)
                {{ $i->PhotoActionBy->name }}
              @else
                {{ '-' }}
              @endif
            </td>
            <td>
              @if (!empty($i->remark))
                {{ $i->remark }}
              @else
                {{ '-' }}
              @endif
            </td>
            <td>
              <?php
                if ($i->status == 1) {
                  $color = 'danger';
                }
                else if ($i->status == 2) {
                  $color = 'primary';
                }
                else {
                  $color = 'muted';
                }             
              ?>
              <span class="text-{{ $color }}">{{ $i->PhotoStatusName->name }}</span>
            </td>
          </tr>
        @endforeach
      @else
        <tr>
          <td colspan="5">No record.</td>
        </tr>
      @endif
    </table>
  </div>
</div>
{{ Form::close() }}




<script type="text/javascript">
$(document).ready(function(){

  // save leave
    $(document).on('click','#photo-update', function(){
      if ($('#remark').val() == "") {
        alert('Please insert Remark.');
      }
      else {
          var answer = confirm('Are you sure want to process this photo?');
          if (answer == true) {
              $('#form-list').submit(); 
          }
          else {
              return false;
          } 
      }
    });     

    // display popup modal
  $(document).on('click','#popup-modal',function() {  
    var type = $(this).attr('alt');
    str =  '';      
    str += '<div class="modal-dialog">';
    str += '  <div class="modal-content">';
    str += '    <div class="modal-header">';
    str += '      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    str += '      <legend>Remarks</legend>';
    str += '    </div>';
    str += '    <div class="modal-body">';
    str += '      <p><textarea name="remark" rows="4" cols="30" id="remark" placeholder="Insert Remark Here"></textarea></p><input type="hidden" name="type" value="' + type + '" />';
    str += '    </div>';
    str += '    <div class="modal-footer">';
    str += '      <div class="btn-group">';
    str += '        <button type="button" class="btn btn-primary" id="photo-update">Submit</button>';
    str += '        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
    str += '      </div>';
    str += '    </div>';
    str += '  </div>';
    str += '</div>';
    $('#photo-modal').html(str);                                                    
    $('#photo-modal').modal();  
  }); 

});  
</script>


@stop