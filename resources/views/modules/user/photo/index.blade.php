@extends('layouts/backend')

@section('content')




{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
	<div class="col-md-12">
		<div class="well">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-6">
							{{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
						</div>
						<div class="col-md-3">
							{{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							{{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID', 'size' => 40)) }}
						</div>
					</div>

					<br>
					{{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit','name' => 'btn-search')) }}&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
				</div>
			</div>

		</div>
	</div>
</div>
{{ Form::close() }}



<div class="row">
	<div class="col-sm-12">
		<table class="table table-condensed table-responsive table-striped table-bordered">
			<thead>
				<tr class="active">
					<th class="col-md-1">No</th>
					<th class="col-md-4">Name /<br>IC No. /<br>Staff ID</th>
					<th class="col-md-4">Region /<br>Site Name /<br>Position</th>
					<th class="col-md-2">Date Apply /<br>Type /<br>Status</th>
					<th class="col-md-1 text-right">Photo</th>
				</tr>
			</thead>
			@if (count($photos) > 0)
				<?php $no = $photos->firstItem() - 1;?>
				@foreach ($photos as $i)
					<?php $no++;?>
					<tr>
						<td>{{ $no }}</td>

						<td>
							{{ $i->UserDetail->name }} /<br>
							{{ $i->UserDetail->icno }} /<br>
							{{ $i->UserDetail->staff_id }}
						</td>

						<td>
							@if ($i->UserJob)
								{{ $i->UserJob->RegionName->name }}
							@else
								{{ '-' }}
							@endif
							/<br>
							{{ $i->SiteDetail->code }} {{ $i->SiteDetail->name }} /<br>
							@if ($i->UserJob)
								{{ $i->UserJob->PositionName->name }}
							@else
								{{ '-' }}
							@endif
						</td>

						<td>
							{{ $i->action_date }} /<br>
							{{ $i->ext }} /<br>
							{!! $i->PhotoLatestStatus->availability !!}
						</td>

						<td class="text-right">
							@if ($i->PhotoLatestStatus)
								@if (in_array($i->PhotoLatestStatus->status, array(1,2)))
									<?php $id = $i->id; $photo = $i->photo_thumb . '.' . $i->ext;?>
									<a href="{{ route('mod.user.photo.view', array($i->id, $i->user_id, $i->sitecode)) }}"><img src="{{ route('lib.file.user.thumb', array($photo)) }}" class="img-responsive" title="View Photo" style="width: 50%; float:right;"></a>
								@else
									<img src="{{ route('lib.image', array('default.png')) }}" class="img-responsive" style="width: 50%; float:right;">
								@endif
							@else
								<img src="{{ route('lib.image', array('default.png')) }}" class="img-responsive" style="width: 50%; float:right;">
							@endif
						</td>

					</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">No record</td>
				</tr>
			@endif
		</table>


		<div class="well">
			<div class="paging text-center">
				{{ $photos->render() }}
				<p>{{ 'Total: '.$photos->total() }}</p>
			</div>
		</div>

	</div>
</div>



@stop

