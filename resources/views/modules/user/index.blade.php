@extends('layouts/backend')

@section('content')



{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">

               <div class="row">
                  <div class="col-md-6">
                     {{ Form::select('site_id', $sites, $sessions['site_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('region_id', $regions, $sessions['region_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('phase_id', $phases, $sessions['phase_id'], array('class' => 'form-control')) }}
                  </div>                  
               </div>

               <div class="row">
                  <div class="col-md-3">
                     {{ Form::select('group_id', $thegroups, $sessions['group_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('position_id', $positions, $sessions['position_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('status_id', $status, $sessions['status_id'], array('class' => 'form-control')) }}
                  </div>
                  <div class="col-md-3">
                     {{ Form::select('contract_id', $contracts, $sessions['contract_id'], array('class' => 'form-control')) }}
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-12">
                     {{ Form::text('keyword', $sessions['keyword'], array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode', 'size' => 40)) }}
                  </div>
               </div>

               <br>
               <input type="submit" value="Search" class="btn btn-warning" name="btn-search" />&nbsp;&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>
         </div>
         
      </div>
   </div>
</div>





<div class="row">
   <div class="col-sm-12">

      <table class="table table-striped table-condensed table-responsive table-bordered">
         <thead>
            <tr class="active">
               <th class="col-md-1">No</th>
               <th class="col-md-4">Name /<br>IC No /<br>Staff ID</th>
               <th class="col-md-3">Region & Phase /<br>Site Name /<br>Contract</th>
               <th class="col-md-2">Group /<br>Position /<br>Status</th>
               <th class="col-md-1">Photo</th>
               <th class="col-md-1 text-right">Actions</th>
            </tr>
         </thead>
         @if (count($users) > 0)
            <?php $no = $users->firstItem() - 1;?>
            @foreach ($users as $i)
               <?php
                  $no++;
               ?>
               <tr>
                  <td>
                     {{ $no }}
                  </td>

                  <td>
                     <a href="{{ route('mod.user.view', array($i->id, $i->api_token)) }}">{{ $i->name }}</a>
                     /<br>{{ $i->icno }}
                     /<br>{{ $i->staff_id }}
                  </td>

                  <td>
                     @if ($i->UserCurrentJob2)
                        @if ($i->UserCurrentJob2->RegionName)
                           {{ strtoupper($i->UserCurrentJob2->RegionName->name) }}
                        @else
                           <span class="text-danger">EMPTY</span>
                        @endif
                         - 
                        @if ($i->UserCurrentJob2->PhaseName)
                           {{ $i->UserCurrentJob2->PhaseName->name }}
                        @else
                           <span class="text-danger">EMPTY</span>
                        @endif
                     @else
                        <span class="text-danger">EMPTY</span> - <span class="text-danger">EMPTY</span>
                     @endif

                     /<br>@if ($i->sitecode) {{ $i->sitecode }} @else {{ '-' }} @endif
                     @if ($i->SiteNameInfo)
                        {{ $i->SiteNameInfo->name }}
                     @endif

                     /<br>
                     @if ($i->UserCurrentJob2)
                        @if ($i->UserCurrentJob2->position_id != 84)
                           @if ($i->UserLatestContract)
                              {!! $i->UserLatestContract->status_contract !!}
                           @else
                              <span class="text-danger">{{ 'EMPTY' }}</span>
                           @endif
                        @else
                           {{ '-' }}
                        @endif
                     @else

                     @endif
                  </td>

                  <td>
                     {{ $i->GroupName->name }}

                     /<br>
                     @if ($i->UserCurrentJob2)
                        @if ($i->UserCurrentJob2->PositionName)
                           {{ $i->UserCurrentJob2->PositionName->name }}
                        @else
                           {{ '-' }}
                        @endif
                     @else
                      {{ '-' }}
                     @endif                     
                     /<br>
                     @if ($i->status != 0)
                        {!! $i->status_color !!}
                     @else
                        {{ '-' }}
                     @endif
                  </td>

                  <td>
                     @if ($i->UserPhotoActive)
                        @if (in_array($i->UserPhotoActive->PhotoLatestStatus->status, array(1,2)))
                           <?php
                              $id = $i->UserPhotoActive->id;
                              $photo = $i->UserPhotoActive->photo_thumb.'.'.$i->UserPhotoActive->ext;
                           ?>
                           <a href="{{ route('mod.user.photo.view', array($id, $i->id, $i->sitecode)) }}"><img src="{{ route('lib.file.user.thumb', array($photo)) }}" class="img-responsive" title="View Photo" style="width: 50%;"></a>
                           @if ($i->UserPhotoActive->PhotoLatestStatus->status == 1) 
                              <span class="text-danger">{{ 'Pending' }}</span>
                           @endif
                        @else
                           <img src="{{ route('lib.image', array('default.png')) }}" class="img-responsive" title="No Photo" style="width: 50%;">
                        @endif
                     @else
                        <img src="{{ route('lib.image', array('default.png')) }}" class="img-responsive" title="No Photo" style="width: 50%;">
                     @endif
                  </td>

                  <td class="text-right">
                     <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary view" title="View Details" onclick="window.location='{{ route("mod.user.view", array($i->id, $i->api_token)) }}'"><i class="icon-user"></i></button>
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu pull-right">
                           <li><a href="{{ route('mod.user.view', array($i->id, $i->api_token)) }}"><i class="icon-magnifier"></i>&nbsp;View Details</a></li>
                           <li><a href="{{ route('mod.letter.user.index', array($i->id, $i->sitecode)) }}"><i class="icon-envelope"></i>&nbsp;Staff Letter</a></li>
                           <li><a href="#"><i class="icon-user-unfollow"></i>&nbsp;Set No Partner</a></li>
                           <li role="separator" class="divider"></li>                           
                           <li><a href="{{ route('mod.user.reset', array($i->id, $i->api_token)) }}"><i class="icon-reload"></i>&nbsp;Reset Device</a></li>
                           <li><a href="{{ route('mod.user.password', array($i->id, $i->api_token)) }}"><i class="icon-lock"></i>&nbsp;Change Password</a></li>
                        </ul>
                     </div>
                  </td>

               </tr>
            @endforeach
         @else
            <tr><td colspan="6">No record</td></tr>
         @endif

      </table>


      <div class="well">
         <div class="paging text-center">
            {{ $users->render() }}
            <p>{{ 'Total: '.$users->total() }}</p>
         </div>
      </div>


   </div>
</div>

{{ Form::close() }}


@stop


