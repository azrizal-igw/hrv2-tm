<!-- education -->
<div class="tab-pane" id="education">

	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Education</h4>
				</div>	
      			
				<div class="panel-body nopadding">
					<table class="table table-condensed table-responsive">
						<thead>
							<tr class="active">
								<th>No</th>
								<th>Year From</th>
								<th>Year To</th>
								<th>Institution</th>
								<th>Result</th>
								<th class="actions text-right">Actions</th>
							</tr>
						</thead>
						@if (count($detail) > 0)
							<?php $no = 0; ?>											
							@foreach ($detail as $education)
							<?php $no++; ?>												
							<tr>
								<td>{{ $no }}</td>
								<td>
									@if ($education->year_from)
										{{ $education->year_from }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td>
									@if ($education->year_to)
										{{ $education->year_to }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td>
									@if ($education->name_education)
										{{ $education->name_education }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td>
									@if ($education->result)
										{{ $education->result }}
									@else
										{{ '-' }}
									@endif
								</td>
								<td class="text-right">

								</td>
							</tr>
							@endforeach
						@else
							<tr><td colspan="6">No record</td></tr>
						@endif
					</table>		             
				</div>	






			</div>
		</div>
	</div>
</div>



