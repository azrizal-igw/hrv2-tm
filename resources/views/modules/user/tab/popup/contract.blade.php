<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">

      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'form-data')) }}
      <div class="modal-content">

         <div class="modal-body">
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="2"><strong>Contract Info</strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="col-md-3">Start Date</td>
                     <td class="col-md-9">
                        {{ $contract->date_from }}
                     </td>
                  </tr>
                  <tr>
                     <td>End Date</td>
                     <td>{{ $contract->date_to }}</td>
                  </tr>
                  <tr>
                     <td>Days</td>
                     <td>{!! $contract->duration !!}</td>
                  </tr>
                  <tr>
                     <td>Site Name</td>
                     <td>{{ $contract->SiteName->code }} {{ $contract->SiteName->name }}</td>
                  </tr>
                  <tr>
                     <td>Apply Leave</td>
                     <td>
                        <?php $disabled = false;?>
                        @if ($contract->LeaveApplication)
                           @if (count($contract->LeaveApplication) > 0)
                              <?php $disabled = true;?>
                           @endif
                           {{ count($contract->LeaveApplication) }}
                        @else
                           {{ '-' }}
                        @endif
                     </td>
                  </tr>
               </tbody>
            </table>

            @if (count($duplicate) > 0)
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="6"><strong>Duplicate Contract</strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr class="active">
                     <th class="col-md-1">No</th>
                     <th class="col-md-2 text-center">Year</th>
                     <th class="col-md-2 text-center">Start Date</th>
                     <th class="col-md-2 text-center">End Date</th>
                     <th class="col-md-2 text-center">Days</th>
                     <th class="col-md-3 text-center">Leave</th>
                  </tr>

                  <?php $no = 0;?>
                  @foreach ($duplicate as $i)
                     <?php $no++;?>
                     <tr>
                        <td>{{ $no }}</td>
                        <td class="text-center">
                           @if ($i->date_from != '0000-00-00')
                              {{ \Carbon\Carbon::parse($i->date_from)->format('Y') }}
                           @else
                              {{ '-' }}
                           @endif
                        </td>
                        <td class="text-center">
                           @if ($i->date_from != '0000-00-00')
                              {{ \Carbon\Carbon::parse($i->date_from)->format('j M Y') }}
                           @else
                              {{ $i->date_from }}
                           @endif
                        </td>
                        <td class="text-center">
                           @if ($i->date_to != '0000-00-00')
                              {{ \Carbon\Carbon::parse($i->date_to)->format('j M Y') }}
                           @else
                              {{ $i->date_to }}
                           @endif
                        </td>
                        <td class="text-center">{!! $i->duration !!}</td>
                        <td class="text-center">
                           @if ($i->LeaveApplication)
                              @if (count($i->LeaveApplication) > 0)
                                 {{ count($i->LeaveApplication) }}
                              @else
                                 {{ '-' }}
                              @endif
                           @else
                              {{ '-' }}
                           @endif
                           {{ Form::hidden('contract_id[]', $i->id) }}
                        </td>
                     </tr>
                  @endforeach

               </tbody>
            </table>
            @endif
         </div>


         @if (count($duplicate) > 0)
            <?php $route = route('mod.user.view.popup.contract', array($user['id'], $user['uid'], $user['token'])); ?>
            <div class="panel-footer">
               <div class="row">
                  <div class="col-md-12">
                        <button type="button" class="btn btn-primary" onclick="getModPopupUpdate('{{ $route }}')" title="Merge Contract"><i class="icon-size-actual"></i>&nbsp;Merge</button>
                  </div>
               </div>
            </div>
         @endif


      </div>
      {{ Form::close() }}




   </div>
</div>



