<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-body">

            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="2"><strong>Letter Info</strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="col-md-3">Letter Type</td>
                     <td class="col-md-9"><strong>{{ $letter->LetterTypeName->name }}</strong></td>
                  </tr>
                  <tr>
                     <td>Letter Date</td>
                     <td>
                        {{ $letter->letter_date }}
                     </td>
                  </tr>
                  <tr>
                     <td>Reminder No</td>
                     <td>
                        {{ CommonHelper::ordinal($letter->reminder_no) }}
                     </td>
                  </tr>
                  <tr>
                     <td>Action Date</td>
                     <td>
                        {{ $letter->action_date }}
                     </td>
                  </tr>
                  <tr>
                     <td>Action By</td>
                     <td>
                        {{ $letter->LetterActionBy->name }}
                     </td>
                  </tr>
                  <tr>
                     <td>Notes</td>
                     <td>
                        {{ $letter->notes }}
                     </td>
                  </tr>
                  <tr>
                     <td>Status</td>
                     <td>
                        {!! $letter->availability !!}
                     </td>
                  </tr>
                  <tr>
                     <td>Updated By</td>
                     <td>
                        @if (!empty($letter->LetterUpdatedBy))
                           {{ $letter->LetterUpdatedBy->name }}
                        @else
                           {{ '-' }}
                        @endif
                     </td>
                  </tr>
                  <tr>
                     <td>Attachment</td>
                     <td>
                        @if (!empty($letter->LetterAttachment))
                           <?php
                              $attachment = $letter->LetterAttachment->filename.'.'.$letter->LetterAttachment->ext;
                           ?>                                        
                           <a href="{{ route('lib.file.letter', array($attachment)) }}" target="_blank" style="text-decoration: none; color: #666666;" title="View Letter"><i class="icon-paper-clip"></i></a>
                        @else
                           {{ '-' }}
                        @endif
                        
                     </td>
                  </tr>
               </tbody>
            </table>

         </div>

      </div>
   </div>
</div>


