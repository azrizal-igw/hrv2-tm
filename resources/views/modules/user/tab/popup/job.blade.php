<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog modal-lg" role="document">

      {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
      <div class="modal-content">

         <div class="modal-body">
            <table class="table table-condensed table-responsive">
               <thead>
                  <tr class="bg-inverse">
                     <td colspan="2"><strong>Job Info</strong></td>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="col-md-3">Join Date</td>
                     <td class="col-md-9">
                        {{ \Carbon\Carbon::parse($job->join_date)->format('j M Y') }}
                     </td>
                  </tr>
                  <tr>
                     <td>Position</td>
                     <td>
                        @if ($job->PositionName) 
                           {{ $job->PositionName->name }} 
                        @else 
                           {{ '-' }} 
                        @endif                                                                              
                     </td>
                  </tr>

                  @if ($job->UserDetail->group_id == 3)
                     <tr>
                        <td>Phase</td>
                        <td>
                           @if ($job->PhaseName)
                              {{ $job->PhaseName->name }}
                           @else
                              {{ '-' }}
                           @endif                                          
                        </td>
                     </tr>
                     <tr>
                        <td>Region</td>
                        <td>
                           @if ($job->RegionName)
                              {{ $job->RegionName->name }}
                           @else
                              {{ '-' }}
                           @endif                                          
                        </td>
                     </tr>                                  
                  @endif

                  @if (!empty($job->sitecode))
                     <tr>
                        <td>Site Name</td>
                        <td>
                           @if ($job->SiteName)
                              {{ $job->SiteName->code }}&nbsp;{{ $job->SiteName->name }}
                           @else
                              {{ '-' }}
                           @endif                                       
                        </td>
                     </tr>
                  @endif   
                  <tr>
                     <td>End Date</td>
                     <td>
                        @if ($job->resign_date && $job->resign_date != '0000-00-00')
                           {{ \Carbon\Carbon::parse($job->resign_date)->format('j M Y') }}
                        @else
                           {{ '-' }}
                        @endif
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>


         <div class="panel-footer">  
            <div class="row">
               <div class="col-md-12">
                  {{ Form::button('<i class="icon-close"></i>&nbsp;Cancel',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'cancel', 'title' => 'Cancel Job']) }}
               </div>
            </div>
         </div>


      </div>
      {{ Form::close() }}




   </div>
</div>



