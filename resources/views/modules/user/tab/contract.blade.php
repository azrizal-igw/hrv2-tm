<!-- contract -->
<div class="tab-pane" id="contract">

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Contract</h4>
				</div>
				<div class="panel-body nopadding">

					<div class="row col-wrap">			
						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">

									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-success">
												<td colspan="2"><i class="icon-note"></i>&nbsp;<strong>Current Contract</strong></td>
											</tr>
										</thead>
										@if (!empty($contract))
											<tr>
												<td>Status</td>
												<td>
													{!! $contract->status_contract !!}
												</td>
											</tr>
											<tr>
												<td class="col-md-4">Contract Date</td>
												<td class="col-md-8">
													@if ($contract->date_from != '0000-00-00')
														{{ \Carbon\Carbon::parse($contract->date_from)->format('j M Y') }}
													@else
														{{ $contract->date_from }}
													@endif
													 - 
													@if ($contract->date_to != '0000-00-00')
														{{ \Carbon\Carbon::parse($contract->date_to)->format('j M Y') }}
													@else
														{{ $contract->date_to }}
													@endif	
													<i class="fa fa-check-circle text-success" title="Current Contract"></i>														
												</td>
											</tr>
											<tr>
												<td>Days</td>
												<td>{!! $contract->duration !!}</td>
											</tr>
											<tr>
												<td>Remaining Days</td>
												<td>{!! $contract->remain_day !!}</td>
											</tr>
											<tr>
												<td>Site Name</td>
												<td>{{ $contract->sitecode }} {{ $contract->SiteName->name }}</td>
											</tr>
											<tr>
												<td>Entitled AL</td>
												<td>{{ $entitled }}</td>
											</tr>
											<tr>
												<td>Leave Taken AL</td>
												<td>
													@if ($taken)
														{{ round($taken->total, 1) }}
													@else
														{{ '-' }}
													@endif
												</td>
											</tr>
										@else
											<tr>
												<td colspan="2">No record</td>
											</tr>
										@endif
									</table>

									@if (count($duplicate) > 1)
										<?php $no_d = 0; ?>
										{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
											<table class="table table-condensed table-responsive">
												<thead>
													<tr class="bg-danger">
														<td colspan="5"><i class="icon-docs"></i>&nbsp;<strong>Active Contract</strong></td>
													</tr>
												</thead>
												<tr class="active">
													<th class="col-md-1">No</th>
													<th class="col-md-3">Site Code</th>
													<th class="col-md-3">Start</th>
													<th class="col-md-3">End</th>
													<th class="col-md-2 text-right">Actions</th>
												</tr>
												@foreach ($duplicate as $d)
													<?php $no_d++; ?>
													<tr>
														<td>{{ $no_d }}</td>
														<td>{{ $d->sitecode }}</td>
														<td>
															@if ($d->date_from != '0000-00-00')
																{{ \Carbon\Carbon::parse($d->date_from)->format('j M Y') }}
															@else
																{{ $d->date_from }}
															@endif															
														</td>
														<td>
															@if ($d->date_to != '0000-00-00')
																{{ \Carbon\Carbon::parse($d->date_to)->format('j M Y') }}
															@else
																{{ $d->date_to }}
															@endif															
														</td>
														<td class="text-right">{{ Form::radio('duplicate_id', $d->id) }}</td>
													</tr>
												@endforeach
												<tr><td colspan="5">Note: Found <strong>{{ $no_d }}</strong> Active Contract. Please confirm the Current Contract.</td></tr>
												<tr><td colspan="5" class="text-right"><button type="button" class="btn btn-sm btn-danger btn_click" id="set" title="Set Default"><i class="icon-check"></i>&nbsp;Set</button></td></tr>										
											</table>
										{{ Form::close() }}
									@endif

								</div>
							</div>
						</div>

						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">
									<div id="table-prev"></div>	
								</div>
							</div>
						</div>
					</div>


					<div class="row col-wrap">			
						<div class="col-sm-12 col">
							<div class="row">
								<div class="col-md-12">

									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-info">
												<td colspan="5" class="text-center"><i class="icon-exclamation"></i>&nbsp;<strong>Diciplinary Record</strong></td>
											</tr>
										</thead>										
										<tr class="active">
											<th class="col-md-1">No</th>
											<th class="col-md-3">Letter Date /<br>Letter Name</th>
											<th class="col-md-4">Apply Date /<br>Upload By</th>
											<th class="col-md-2">Reminder /<br>Status</th>
											<th class="col-md-2 text-right">Actions</th>
										</tr>
										@if (count($letter) > 0)
											<?php $no = 0; ?>
											@foreach ($letter as $i)
												<?php $no++; ?>
												<tr>
													<td>{{ $no }}</td>
													<td>
														{{ $i->letter_date }} /<br>
														@if (!empty($i->LetterTypeName))
															{{ $i->LetterTypeName->name }}
														@else
															{{ '-' }}
														@endif
													</td>
													<td>
														{{ $i->action_date }} /<br>
														@if (!empty($i->LetterActionBy))
															{{ $i->LetterActionBy->name }}
														@else
															{{ '-' }}
														@endif
													</td>
													<td>
														{{ CommonHelper::ordinal($i->reminder_no) }} /<br>
														{!! $i->availability !!}
													</td>
													<td class="text-right">
														@if (!empty($i->LetterAttachment))
															<?php
																$attachment = $i->LetterAttachment->filename.'.'.$i->LetterAttachment->ext;
															?> 													
															<a href="#" data-url="{{ route('mod.user.view.popup.letter', array($i->id)) }}" id="modal-popup" class="btn btn-primary btn-sm" title="View Letter"><i class="icon-magnifier-add"></i></a>&nbsp;<a href="{{ route('lib.file.letter', array($attachment)) }}" target="_blank" title="Attachment" class="btn btn-primary btn-sm"><i class="icon-paper-clip"></i></a>
														@else
															<a href="#" class="btn btn-success btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
														@endif											
													</td>
												</tr>
											@endforeach
											<tr>
												<td colspan="5" class="text-center">Total: {{ $no }}</td>
											</tr>
										@else
											<tr>
												<td colspan="5">No record</td>
											</tr>
										@endif
									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>





<script type="text/javascript">
	getModDataList("{{ route('mod.user.contract.prev.api', array($uid, $token)) }}", 1);
</script>

<style type="text/css">
#list a {
   color: #FFFFFF;
   text-decoration: underline;
}  
a:link {
   color: #FFFFFF;
}
a:visited {
   color: #FFFFFF;
}
a:hover {
   color: #FFFFFF;
}
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
</style>
