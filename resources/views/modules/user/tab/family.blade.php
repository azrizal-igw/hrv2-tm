<div class="tab-pane" id="family">

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Family</h4>
				</div>
				<div class="panel-body nopadding">


					<div class="row col-wrap">			
						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">


									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-success">
												<td colspan="2"><i class="icon-people"></i>&nbsp;<strong>Spouse</strong></td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="col-md-3">Name</td>
												<td class="col-md-9">
													@if ($user->partner_name)
														{{ $user->partner_name }}
													@else
														{{ '-' }}
													@endif
												</td>
											</tr>
											<tr>
												<td>Phone</td>
												<td>
													@if ($user->partner_phone)
														{{ $user->partner_phone }}
													@else
														{{ '-' }}
													@endif													
												</td>
											</tr>
										</tbody>
									</table>


								</div>
							</div>
						</div>

						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">	
									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-warning">
												<td colspan="2"><i class="icon-emotsmile"></i>&nbsp;<strong>Children</strong></td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Total</td>
												<td class="text-right">
													@if ($user->child_no)
														{{ $user->child_no }}
													@else
														{{ '-' }}
													@endif
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
</div>

