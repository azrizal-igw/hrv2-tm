<div class="tab-pane" id="picture">


	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Photo</h4>
				</div>	

				@if (!empty($photo))
					<div class="panel-body nopadding">
						<div class="form-group">						
							<a href="{{ route('lib.file.user', array($photo->photo.'.'.$photo->ext)) }}" target="_blank" title="Click to Enlarge"><img src="{{ route('lib.file.user.thumb', array($photo->photo_thumb.'.'.$photo->ext)) }}" class="img-thumbnail img-responsive"></a>
						</div>
					</div> 
				@else
					@if (in_array(auth()->user()->group_id, array(1,2)))
						{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
							<div class="panel-body nopadding">
								<div class="form-group">
									<div class="col-md-12">       
										@if ($errors->has('photo_file'))
											<p class="text-danger">{{ $errors->first('photo_file') }}</p>
										@endif
										{{ Form::file('photo_file') }} 
										<br>
										{{ Form::button('Upload&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'upload']) }} 
									</div>
								</div>
								<strong>Remarks: </strong>					
								<br>1) File photo must be in following types jpeg/jpg/png and below 100KB.
								<br>2) Use latest photo and not older than 3 months.
								<br>3) Background: 1 color. (Preferebly Blue/White).
								<br>4) The Dimension must follow the size of Passport.							
							</div>
						{{ Form::close() }}
					@else
						<div class="panel-body nopadding">
							<div class="form-group">
								<div class="col-md-12">  
									No record
								</div>
							</div>
						</div>									 					
					@endif

				@endif

			</div>
		</div>
	</div>

</div>   



