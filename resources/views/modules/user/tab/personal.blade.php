<!-- personal -->
<div class="tab-pane active" id="personal">

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Profile</h4>
				</div>
				<div class="panel-body nopadding">

					@if (!empty($photo) && !empty($photo->PhotoApproved))
						<a href="{{ route('lib.file.user', array($photo->photo.'.'.$photo->ext)) }}" target="_blank" title="Click to Enlarge"><img src="{{ route('lib.file.user.thumb', array($photo->photo_thumb.'.'.$photo->ext)) }}" class="img-thumbnail img-responsive"></a>
					@else
						<img src="{{ route('lib.image', array('default.png')) }}" class="img-thumbnail img-circle img-responsive"> 
					@endif
					<br><br>

					<div class="row col-wrap">			
						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">

									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-success">
												<td colspan="2"><i class="icon-credit-card"></i>&nbsp;<strong>Personal Info</strong></td>
											</tr>
										</thead>
										<tr>
											<td>Status</td>
											<td>{!! $personal->status_color !!}</td>
										</tr>
										<tr>
											<td>Staff ID</td>
											<td>{{ $personal->staff_id }}</td>
										</tr>
										<tr>
											<td class="col-md-4">Full Name</td>
											<td class="col-md-8">{{ $personal->name }}</td>
										</tr>
										<tr>
											<td>IC No</td>
											<td>{{ $personal->icno }}</td>
										</tr>
										<tr>
											<td>Mobile No</td>
											<td>{{ $personal->hpno }}</td>
										</tr>
										<tr>
											<td>Mobile No 2</td>
											<td>{{ $personal->hpno2 }}</td>
										</tr>										
										<tr>
											<td>Telephone No 1</td>
											<td>{{ $personal->telno }}</td>
										</tr>
										<tr>
											<td>Telephone No 2</td>
											<td>{{ $personal->telno }}</td>
										</tr>										
										<tr>
											<td>Gender</td>
											<td>
												@if ($personal->GenderName)
													{{ $personal->GenderName->eng }}
												@else
													{{ '-' }}
												@endif
											</td>
										</tr>
										<tr>
											<td>Work Email</td>
											<td>{{ $personal->work_email }}</td>
										</tr>										
										<tr>
											<td>Personal Email</td>
											<td>{{ $personal->personal_email }}</td>
										</tr>
										<tr>
											<td>Date of Birth</td>
											<td>{{ $personal->dob }}</td>
										</tr>
										<tr>
											<td>Place of Birth</td>
											<td>{{ $personal->pob }}</td>
										</tr>

										<tr>
											<td>Age</td>
											<td>{{ $age }}</td>
										</tr>
										<tr>
											<td>Marital Status</td>
											<td>
												@if ($personal->MaritalName)
													{{ $personal->MaritalName->name }}
												@else
													{{ '-' }}
												@endif
											</td>
										</tr>
										<tr>
											<td>Height</td>
											<td>{{ $personal->height }}</td>
										</tr>
										<tr>
											<td>Weight</td>
											<td>{{ $personal->weight }}</td>
										</tr>
										<tr>
											<td>Race</td>
											<td>
												@if ($personal->RaceName)
													{{ $personal->RaceName->name }}
												@else
													{{ '-' }}
												@endif
											</td>
										</tr>
										<tr>
											<td>Religion</td>
											<td>
												@if ($personal->ReligionName)
													{{ $personal->ReligionName->name }}
												@else
													{{ '-' }}
												@endif
											</td>
										</tr>
										<tr>
											<td>Nationality</td>
											<td>
												@if ($personal->NationalityName)
													{{ $personal->NationalityName->eng }}
												@else
													{{ '-' }}
												@endif
											</td>
										</tr>


										<tr>
											<td>Last Login</td>
											<td>
												@if (!empty($personal->last_login))
													{{ $personal->last_login }}
												@else
													{{ '-' }}
												@endif
											</td>
										</tr>																				
									</table>



								</div>
							</div>
						</div>



						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">	
									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-success">
												<td colspan="2"><i class="icon-home"></i>&nbsp;<strong>Permanent Address</strong></td>
											</tr>
										</thead>
										<tr>
											<td class="col-md-4">Permanent Street 1</td>
											<td class="col-md-8">{{ $personal->permanent_street_1 }}</td>
										</tr>
										<tr>
											<td>Permanent Street 2</td>
											<td>{{ $personal->permanent_street_2 }}</td>
										</tr>
										<tr>
											<td>Permanent Postcode</td>
											<td>{{ $personal->permanent_postcode }}</td>
										</tr>
										<tr>
											<td>Permanent City</td>
											<td>{{ $personal->permanent_city }}</td>
										</tr>
										<tr>
											<td>Permanent State</td>
											<td>{{ $personal->permanent_state }}</td>
										</tr>
									</table>

									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-success">
												<td colspan="2"><i class="icon-home"></i>&nbsp;<strong>Correspondence Address</strong></td>
											</tr>
										</thead>
										<tr>
											<td class="col-md-4">Correspondence Street 1</td>
											<td class="col-md-12">{{ $personal->correspondence_street_1 }}</td>
										</tr>
										<tr>
											<td>Correspondence Street 2</td>
											<td>{{ $personal->correspondence_street_2 }}</td>
										</tr>
										<tr>
											<td>Correspondence Postcode</td>
											<td>{{ $personal->correspondence_postcode }}</td>
										</tr>
										<tr>
											<td>Correspondence City</td>
											<td>{{ $personal->correspondence_city }}</td>
										</tr>
                                        <tr>
                                            <td>Correspondence State</td>
                                            <td>
                                                @if ($corr_state)
                                                    {{ $corr_state->name }}
                                                @else
                                                    {{ '-' }}
                                                @endif
                                            </td>
                                        </tr>
									</table>

									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-success">
												<td colspan="2"><i class="icon-briefcase"></i>&nbsp;<strong>Work Info</strong></td>
											</tr>
										</thead>
										<tr>
											<td class="col-md-4">Website</td>
											<td class="col-md-8">{{ $personal->website }}</td>
										</tr>										
										<tr>
											<td>Income Tax No.</td>
											<td>{{ $personal->itaxno }}</td>
										</tr>
										<tr>
											<td>EPF No.</td>
											<td>{{ $personal->epfno }}</td>
										</tr>
										<tr>
											<td>Socso No.</td>
											<td>{{ $personal->socsono }}</td>
										</tr>
										<tr>
											<td>Bank Name</td>
											<td>{{ $personal->bankname }}</td>
										</tr>
										<tr>
											<td>Bank Account No.</td>
											<td>{{ $personal->bankno }}</td>
										</tr>
									</table>


								</div>
							</div>
						</div>
					</div>		


				</div>
			</div>
		</div>
	</div>
</div>