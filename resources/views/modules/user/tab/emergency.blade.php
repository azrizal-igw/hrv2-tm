<div class="tab-pane" id="emergency">

	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Emergency Contact</h4>
				</div>	
      			
				<div class="panel-body nopadding">
					<table class="table table-condensed table-responsive">
						<thead>
							<tr class="active">
								<th>No</th>
								<th>Name</th>
								<th>Relationship</th>
								<th>Address</th>
								<th>Tel No.</th>
								<th class="actions text-right">Actions</th>
							</tr>
						</thead>
						@if (count($detail) > 0)
							<?php $no = 0; ?>												
							@foreach ($detail as $i)
							<?php $no++; ?>												
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $i->name }}</td>
								<td>{{ $i->relation }}</td>					           
								<td>{{ $i->address }}</td>			              
								<td>{{ $i->telno }}</td>	              
								<td class="text-right">
								</td>
							</tr>
							@endforeach
						@else
							<tr><td colspan="6">No record</td></tr>
						@endif
					</table>		             
				</div>






			</div>
		</div>
	</div>
</div>
