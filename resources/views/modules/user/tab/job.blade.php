<!-- job -->
<div class="tab-pane" id="job">

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Job</h4>
				</div>
				<div class="panel-body nopadding">


					<div class="row col-wrap">			
						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">


									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-success">
												<td colspan="2"><i class="icon-briefcase"></i>&nbsp;<strong>Current Job</strong></td>
											</tr>
										</thead>
										@if ($job)
											<tr>
												<td>Status</td>
												<td>
													{!! $job->status_color !!}
												</td>
											</tr>
											<tr>
												<td class="col-md-4">Staff ID</td>
												<td class="col-md-8">{{ $job->staff_id }}</td>
											</tr>
											<tr>
												<td>Join Date</td>
												<td>{{ \Carbon\Carbon::parse($job->join_date)->format('j M Y') }}</td>
											</tr>
											<tr>
												<td>Position</td>
												<td>
													@if ($job->PositionName) 
														{{ $job->PositionName->name }} 
													@else 
														{{ '-' }} 
													@endif																										
												</td>
											</tr>

											@if ($job->UserDetail->group_id == 3)
												<tr>
													<td>Phase</td>
													<td>
														@if ($job->PhaseName)
															{{ $job->PhaseName->name }}
														@else
															{{ '-' }}
														@endif														
													</td>
												</tr>
												<tr>
													<td>Region</td>
													<td>
														@if ($job->RegionName)
															{{ $job->RegionName->name }}
														@else
															{{ '-' }}
														@endif														
													</td>
												</tr>												
											@endif

											@if (!empty($job->sitecode))
												<tr>
													<td>Site Name</td>
													<td>
														@if ($job->SiteName)
															{{ $job->SiteName->code }}&nbsp;{{ $job->SiteName->name }}
														@else
															{{ '-' }}
														@endif													
													</td>
												</tr>
											@endif
											<tr>
												<td>Resign Date</td>
												<td>
													@if ($job->resign_date && $job->resign_date != '0000-00-00')
														{{ \Carbon\Carbon::parse($job->resign_date)->format('j M Y') }}
													@else
														{{ '-' }}
													@endif
												</td>
											</tr>
										@else
											<tr>
												<td colspan="2">No record</td>
											</tr>
										@endif
									</table>


								</div>
							</div>
						</div>

						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">	
									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-danger">
												<td colspan="4"><i class="icon-clock"></i>&nbsp;<strong>{{ count($prev) }} Movement</strong></td>
											</tr>
										</thead>
										<tr class="active">
											<th class="col-md-1">No</th>
											<th class="col-md-4">Position</th>
											<th class="col-md-5">Site Name</th>
											<th class="col-md-2 text-right">Actions</th>
										</tr>										
										@if (count($prev) > 0)
											<?php $no = 0; ?>
											@foreach ($prev as $i)
												<?php $no++; ?>
												<tr>
													<td>{{ $no }}</td>
													<td>
														@if ($i->PositionName) 
															{{ $i->PositionName->name }} 
														@else 
															{{ '-' }} 
														@endif															
													</td>
													<td>
														@if ($i->SiteName)
															{{ $i->SiteName->code }}&nbsp;{{ $i->SiteName->name }}
														@else
															{{ '-' }}
														@endif															
													</td>
													<td class="text-right">
														<a href="#" data-url="{{ route('mod.user.view.popup.job', array($i->id, $i->user_id, $token)) }}" data-toggle="modal" id="list" class="btn btn-success btn-sm" title="View Details"><i class="icon-magnifier-add"></i></a>
													</td>
												</tr>
											@endforeach
										@else
											<tr>
												<td colspan="4">No record</td>
											</tr>
										@endif
									</table>
								</div>
							</div>
						</div>
					</div>


					<div class="row col-wrap">			
						<div class="col-sm-6 col">
							<div class="row">
								<div class="col-md-12">

									<table class="table table-condensed table-responsive">
										<thead>
											<tr class="bg-info">
												<td colspan="4"><i class="icon-directions"></i>&nbsp;<strong>{{ count($transfer) }} Transfer</strong></td>
											</tr>
										</thead>
										<tr class="active">
											<th class="col-md-1">No</th>
											<th class="col-md-3">Staff ID</th>
											<th class="col-md-2">Status</th>											
											<th class="col-md-6">Site Name</th>
										</tr>
										@if (count($transfer) > 0)
											<?php $no = 0; ?>
											@foreach ($transfer as $i)
												<?php $no++; ?>
												<tr>
													<td>{{ $no }}</td>
													<td>
														@if ($i->staff_id)
															<a href="{{ route('mod.user.view', array($i->id, $i->api_token)) }}" style="color:#ff7f0e">{{ $i->staff_id }}</a>
														@else
															{{ '-' }}
														@endif
													</td>
													<td>{!! $i->status_color !!}</td>
													<td>{{ $i->SiteNameInfo->code }} {{ $i->SiteNameInfo->name }}</td>
												</tr>
											@endforeach
										@else
											<tr>
												<td colspan="4">No record</td>
											</tr>
										@endif
									</table>

								</div>
							</div>
						</div>

					</div>


				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
   $('[data-toggle="modal"]').click(function(e) {
      e.preventDefault();
      $(".divLoading").addClass('show');
      var url = $(this).attr('data-url');
      $.get(url, function(data) {
         $(data).modal();
         $(".divLoading").removeClass('show').addClass('hide');
      });
   });
});
</script>


<style type="text/css">
#list a {
   color: #FFFFFF;
   text-decoration: underline;
}  
a:link {
   color: #FFFFFF;
}
a:visited {
   color: #FFFFFF;
}
a:hover {
   color: #FFFFFF;
}
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
</style>