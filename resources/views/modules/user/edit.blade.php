@extends('layouts/backend')


@section('content')



<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">



{{ Form::open(array('class' => 'form-horizontal', 'role'=>'form')) }}
<div class="panel panel-primary">
	<div class="panel-heading">Edit Profile</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">

				<div class="form-group">
					<div class="col-md-3">
						<div class="required">
							{{ Form::label('selectIC', 'IC No.') }}
							@if ($errors->has('icno'))
								<p class="text-danger">{{ $errors->first('icno') }}</p>
							@endif
							{{ Form::text('icno', $detail->icno, array('class'=>'form-control', 'id' => 'selectIC', 'size' => 40)) }}
						</div>
					</div>
				</div>

				<p><strong>Remarks: </strong>
					<br>1) Used this form if IC No is registered wrong and already update at ERP.
					<br>2) Make sure Sync Staff Active is not trigger yet.
				</p>



			</div>
		</div>
	</div>

	<div class="panel-footer">
		{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger btn_click', 'id' => 'update']) }}
	</div>
</div>
{{ Form::hidden('id', $detail->id) }}
{{ Form::close() }}





@stop



