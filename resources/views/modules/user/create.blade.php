@extends('layouts/backend')

@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
<div class="panel panel-primary">
	<div class="panel-heading">
		<h4 class="panel-title">Staff Info</h4>
	</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Name') }}
						@if ($errors->has('name'))
							<p class="text-danger">{{ $errors->first('name') }}</p>
						@endif
						{{ Form::text('name', old('name'), array('class'=>'form-control', 'size' => 40)) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('NRIC') }}
						@if ($errors->has('icno'))
						<p class="text-danger">{{ $errors->first('icno') }}</p>
						@endif
						{{ Form::text('icno', old('icno'), array('class'=>'form-control', 'size' => 40, 'maxlength' => 12)) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Staff ID') }}
						@if ($errors->has('staff_id'))
							<p class="text-danger">{{ $errors->first('staff_id') }}</p>
						@endif
						{{ Form::text('staff_id', old('staff_id'), array('class'=>'form-control', 'size' => 40)) }}
						{{ Form::hidden('group_id', $group_id) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Email') }}
						@if ($errors->has('email'))
						<p class="text-danger">{{ $errors->first('email') }}</p>
						@endif
						{{ Form::text('email', old('email'), array('class'=>'form-control', 'size' => 40)) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Mobile No.') }}
						@if ($errors->has('hpno'))
						<p class="text-danger">{{ $errors->first('hpno') }}</p>
						@endif
						{{ Form::text('hpno', old('hpno'), array('class'=>'form-control', 'size' => 40)) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Position') }}
						@if ($errors->has('position_id'))
							<p class="text-danger">{{ $errors->first('position_id') }}</p>
						@endif
						{{ Form::select('position_id', $positions, null, array('class' => 'form-control')) }}
					</div>
				</div>

				@if ($group_id == 4)
					<div class="form-group">
						<div class="col-md-3 required">
							{{ Form::label('Region') }}
							@if ($errors->has('region_id'))
								<p class="text-danger">{{ $errors->first('region_id') }}</p>
							@endif
							{{ Form::select('region_id', $regions, null, array('class' => 'form-control')) }}
						</div>
					</div>
				@endif

			</div>
		</div>
	</div>



	<div class="panel-footer">
		<div class="row">
			<div class="col-md-6">
				{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>&nbsp;',['type' => 'submit', 'class' => 'btn btn-danger', 'id' => 'btn_save']) }}
			</div>
		</div>
	</div>




</div>
{{ Form::close() }}






@stop





