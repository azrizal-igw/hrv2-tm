@extends('layouts/backend')

@section('content')





<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">





<div class="panel panel-primary">
   <div class="panel-heading">Job Info</div>
   <div class="panel-body">






         {{ Form::open(array('class' => 'form-horizontal', 'role' => 'form')) }}
         <div class="row">             
            <div class="col-md-12">                 






				<div class="form-group">
					<div class="col-md-4">          
						<div class="required">        
						{{ Form::label('selectStaffID', 'Staff ID') }}   
						@if ($errors->has('staff_id'))
							<p class="text-danger">{{ $errors->first('staff_id') }}</p>
						@endif       
						{{ Form::text('staff_id', old('staff_id'), array('class'=>'form-control')) }}
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-4">  
		                @if ($errors->has('join_date'))
		                    <div class="text-danger">{{ $errors->first('join_date') }}</div>
		                @endif 					        
						<div class="required">{{ Form::label('selectStartDate', 'Join Date') }}</div>
						<div class="input-group date" id="pick_register_date">
							{{ Form::text('join_date', old('join_date'), array('class' => 'form-control', 'data-date-format' => 'DD/MM/YYYY', 'id' => 'selectStartDate')) }}
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>						
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4">          
						<div class="required">        
						{{ Form::label('selectPosition', 'Position') }}   
						@if ($errors->has('position_id'))
							<p class="text-danger">{{ $errors->first('position_id') }}</p>
						@endif       
						{{ Form::select('position_id', $positions, old('position_id'), array('class' => 'form-control', 'id' => 'selectPosition')) }}
						</div>
					</div>
				</div>





				@if ($detail->group_id == 3)
					<div class="form-group">
						<div class="col-md-4">          
							<div class="required">        
							{{ Form::label('selectPhase', 'Phase') }}   
							@if ($errors->has('phase_id'))
								<p class="text-danger">{{ $errors->first('phase_id') }}</p>
							@endif       
							{{ Form::select('phase_id', $phases, old('phase_id'), array('class' => 'form-control', 'id' => 'selectPhase')) }}
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-4">          
							<div class="required">        
							{{ Form::label('selectSite', 'Site') }}   
							@if ($errors->has('sitecode'))
								<p class="text-danger">{{ $errors->first('sitecode') }}</p>
							@endif       
							{{ Form::select('sitecode', $sites, old('sitecode'), array('class' => 'form-control', 'id' => 'selectSite')) }}
							</div>
						</div>
					</div>



				@elseif ($detail->group_id == 4)	
					<div class="form-group">
						<div class="col-md-4">          
							<div class="required">        
							{{ Form::label('selectRegion', 'Region') }}   
							@if ($errors->has('region_id'))
								<p class="text-danger">{{ $errors->first('region_id') }}</p>
							@endif       
							{{ Form::select('region_id', $regions, old('region_id'), array('class' => 'form-control', 'id' => 'selectRegion')) }}
							</div>
						</div>
					</div>					
				@endif



				<br>
				<p><strong>Remarks: </strong>
				<br>1) This process is not recommended because latest staff info provided from ERP.
				<br>2) Staff ID will be used to login the system.
				<br>3) Previous Job will be history.
				</p>




            </div>               
         </div>   

		
		{{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'submit', 'class' => 'btn btn-danger']) }}      
		{{ Form::hidden('group_id', $detail->group_id) }}   
		{{ Form::close() }}  


   </div>      
</div>




<script type="text/javascript">
$(function () {
    $('#pick_register_date').datetimepicker({
        pickTime: false
    });         
});
</script>





@stop