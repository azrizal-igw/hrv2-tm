@extends('layouts/backend')

@section('content')


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-search')) }}
<div class="row">
   <div class="col-md-12">
      <div class="well">

         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-12">{{ Form::text('keyword', null, array('class'=>'form-control', 'placeholder' => 'Search Name/IC No/Staff ID/Sitecode')) }}
                  </div>                  
               </div>

               <br>
               {{ Form::button('Search', array('class' => 'btn btn-warning', 'type' => 'submit', 'name' => 'btn-search')) }}&nbsp;<input type="submit" value="Reset" class="btn btn-default" name="reset" />
            </div>
         </div>

      </div>
   </div>
</div>
{{ Form::close() }}



<div class="row">
   <div class="col-sm-12">
      <table class="table table-striped table-condensed table-responsive table-bordered">
         <thead>
            <tr class="active">
               <th>No</th>
               <th>Name /<br>Site Name</th>
               <th>Duration /<br>Time</th>
               <th>Partner /<br>Off Days</th>
               <th>Status</th>
               <th class="text-right">Actions</th>
            </tr>         	
			@if (count($work_hour) > 0)
				<?php $no = $work_hour->firstItem() - 1;?>
				@foreach ($work_hour as $i)
					<?php $no++;?>         	
					<tr>
						<td>{{ $no }}</td>
						<td>
							@if (!empty($i->StaffInfo))
								{{ $i->StaffInfo->name }}
							@else
								{{ '-' }}
							@endif
							/<br>
							{{ $i->sitecode }} 
							@if (!empty($i->SiteInfo))
								{{ $i->SiteInfo->name }}
							@else
								{{ '-' }}
							@endif
						</td>
						<td>
							{{ $i->date_from }} - {{ $i->date_to }} /<br>
							{{ $i->time_in }} - {{ $i->time_out }}
						</td>
						<td>
							{!! $i->partner_status !!} /<br>
							@if (!empty($i->day_name))
								@foreach ($i->day_name as $d)
									{{ $d->name }}<br>
								@endforeach
							@else
								{{ '-' }}
							@endif
						</td>
						<td>
							{!! $i->avail !!}
						</td>
						<td class="text-right">
							<a href="{{ route('mod.work-hour.view', array($i->id)) }}" class="btn btn-primary btn-sm" title="View"><i class="icon-magnifier-add"></i></a>
							<a href="{{ route('mod.work-hour.edit', array($i->id)) }}" class="btn btn-danger btn-sm" title="Edit"><i class="icon-note"></i></a>							
						</td>
					</tr>
				@endforeach
			@endif
         </thead>
      </table>



      <div class="well">
         <div class="paging text-center">
            {{ $work_hour->render() }}
            <p>{{ 'Total: '.$work_hour->total() }}</p>
         </div>
      </div>


   </div>
</div>



@stop


