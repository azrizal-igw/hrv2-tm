@extends('layouts/backend')


@section('content')

<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Staff Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Staff Name</td>
						<td class="col-md-9">{{ $work_hour->StaffInfo->name }}</td>
					</tr>
					<tr>
						<td>IC No.</td>
						<td>{{ $work_hour->StaffInfo->icno ? $work_hour->StaffInfo->icno : '-' }}</td>
					</tr>
					<tr>
						<td>Staff ID</td>
						<td>{{ $work_hour->StaffInfo->staff_id ? $work_hour->StaffInfo->staff_id : '-' }}</td>
					</tr>					
					<tr>
						<td>Position</td>
						<td>
						@if ($work_hour->StaffInfo->UserLatestJob)
							{{ $work_hour->StaffInfo->UserLatestJob->PositionName->name }}
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>
					<tr>
						<td>Site Name</td>
						<td>
						@if ($work_hour->SiteInfo)
							{{ $work_hour->SiteInfo->code }} - {{ $work_hour->SiteInfo->name }}
						@else
							{{ '-' }}
						@endif
						</td>
					</tr>

				</table>
			</div>
		</div>
	</div>
</div>


{{ Form::open(array('class' => 'form-horizontal', 'id' => 'form-list')) }}
<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Work Hour Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Date From</td>
						<td class="col-md-9">{{ $work_hour->date_from }}</td>
					</tr>
					<tr>
						<td>Date To</td>
						<td>
							{{ $work_hour->date_to }}
						</td>
					</tr>
					<tr>
						<td>Time In</td>
						<td>
							{{ $work_hour->time_in }}
						</td>
					</tr>
					<tr>
						<td>Time Out</td>
						<td>{{ $work_hour->time_out }}</td>
					</tr>
					<tr>
						<td>Off Days (Every Week)</td>
						<td>
							@foreach ($work_hour->day_name as $d)
							{{ $d->name }}<br>
							@endforeach
						</td>
					</tr>
					<tr>
						<td>Reason</td>
						<td>
							{{ $work_hour->reason }}
						</td>
					</tr>
					<tr>
						<td>Has M/AM</td>
						<td>
							@if ($work_hour->partner == 1)
							{{ 'Yes' }}
							@else
							{{ 'No' }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>{!! $work_hour->avail !!}</td>
					</tr>
					<tr>
						<td>Created By</td>
						<td>{{ $work_hour->UserActionBy->name }}</td>
					</tr>
					<tr>
						<td>Created Date</td>
						<td>{{ $work_hour->created_at }}</td>
					</tr>					
				</table>
			</div>
		</div>
	</div>
</div>



<div class="panel panel-primary">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">	
				<table class="table table-condensed table-responsive table-hover table-striped">
					<thead>
						<tr class="bg-primary">
							<td colspan="2"><strong>Attachment Info</strong></td>
						</tr>
					</thead>
					<tr>
						<td class="col-md-3">Attachment</td>
						<td class="col-md-9">
							@if ($work_hour->TheAttachment)
								<?php
									$attachment = $work_hour->TheAttachment->filename . '.' . $work_hour->TheAttachment->ext;
									$photo = route('lib.file.misc', array($attachment));
									$thumb = $work_hour->TheAttachment->thumb_name . '.' . $work_hour->TheAttachment->ext;
									$thumb_url = route('lib.file.misc.thumb', array($thumb));
								?>
								<a href="{{ $photo }}" target="_blank"><img src="{{ $thumb_url }}"></a>
							@else
								{{ '-' }}
							@endif                                                
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="panel-footer">  
		<div class="row">
			<div class="col-md-6"> 
				{{ Form::button('<i class="icon-arrow-left"></i>&nbsp;Back',['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btn_back', 'data-href' => route('mod.work-hour.index')]) }} 
			</div>
			<div class="col-md-6 text-right">
				{{ Form::button('Edit&nbsp;<i class="icon-note"></i>',['type' => 'button', 'class' => 'btn btn-danger', 'id' => 'btn_url', 'data-href' => route('mod.work-hour.edit', array($work_hour->id))]) }} 
			</div>
		</div>
	</div>



</div>
{{ Form::close() }}


@stop

