@extends('layouts/backend')


@section('content')


<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<link href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">


{{ Form::open(array('class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-default">
			<div class="panel-body">
				<legend>Work Hour Info</legend>


				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Start Date') }}
						@if ($errors->has('start_date'))
							<p class="text-danger">{{ $errors->first('start_date') }}</p>
						@endif
						<div class="input-group date" id="pick_start_date">
							{{ Form::text('start_date', $work_hour->date_from, array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('End Date') }}
						@if ($errors->has('end_date'))
							<p class="text-danger">{{ $errors->first('end_date') }}</p>
						@endif
						<div class="input-group date" id="pick_end_date">
							{{ Form::text('end_date', $work_hour->date_to, array('class' => 'form-control', 'data-date-format' => 'YYYY-MM-DD')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Time In') }}
						@if ($errors->has('time_in'))
							<p class="text-danger">{{ $errors->first('time_in') }}</p>
						@endif
						<div class="input-group date" id="pick_time_in">
							{{ Form::text('time_in', $work_hour->time_in, array('class' => 'form-control', 'data-date-format' => 'HH:mm', 'readonly')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 required">
						{{ Form::label('Time Out') }}
						@if ($errors->has('time_out'))
							<p class="text-danger">{{ $errors->first('time_out') }}</p>
						@endif
						<div class="input-group date" id="pick_time_out">
							{{ Form::text('time_out', $work_hour->time_out, array('class' => 'form-control', 'data-date-format' => 'HH:mm', 'readonly')) }}
							<span class="input-group-addon"><i class="icon-calendar"></i>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3">
						{{ Form::label('Off Day (Every Week)') }}
						@if ($errors->has('day'))
							<p class="text-danger">{{ $errors->first('day') }}</p>
						@endif
						<br>
						@if (!empty($days))
							@foreach ($days as $d)
								@if (in_array($d['id'], $exp_days))
									<?php $checked = 'checked'; ?>
								@else
									<?php $checked = ''; ?>
								@endif	
								<label class="checkbox-inline">
									<input type="checkbox" {{ $checked }} name="day[]" value="{{ $d['id'] }}" class="check-day">&nbsp;{{ $d['name'] }}
								</label><br>
							@endforeach
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4 required">
						{{ Form::label('Reason') }}
						@if ($errors->has('reason'))
							<p class="text-danger">{{ $errors->first('reason') }}</p>
						@endif
						{{ Form::textarea('reason', $work_hour->reason, ['class' => 'form-control', 'size' => '30x3']) }}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-4">
						{{ Form::label('Has M/AM') }}
						@if ($errors->has('partner'))
							<p class="text-danger">{{ $errors->first('partner') }}</p>
						@endif
						@if ($work_hour->partner == 1)
							{{ Form::checkbox('partner', 1, true) }}
						@else
							{{ Form::checkbox('partner', 1, false) }}
						@endif
					</div>
				</div>

			</div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::button('Save&nbsp;<i class="icon-arrow-right"></i>',['type' => 'button', 'class' => 'btn btn-danger btn_click', 'id' => 'save']) }}
                    </div>
                </div>
            </div>


		</div>
	</div>
</div>
{{ Form::close() }}


<script type="text/javascript">
$(function () {
	getDatePicker('pick_start_date');
	getDatePicker('pick_end_date');
	getTimePicker('pick_time_in');
	getTimePicker('pick_time_out');	     
});
</script>


@stop

